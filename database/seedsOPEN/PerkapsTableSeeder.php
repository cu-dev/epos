<?php

use Illuminate\Database\Seeder;

class PerkapsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('perkaps')->delete();
        
        \DB::table('perkaps')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => '000001/KMK/PR/03/2018',
                'nama' => 'Perlengkapan Awal Toko KMK',
                'harga' => '16412700.000',
                'nominal' => NULL,
                'residu' => NULL,
                'umur' => 0,
                'status' => 1,
                'no_transaksi' => '-',
                'keterangan' => 'Perlengkapan Awal Toko KMK',
                'created_at' => '2018-03-01 02:03:34',
                'updated_at' => '2018-03-01 02:03:34',
            ),
            1 => 
            array (
                'id' => 2,
                'kode' => '0001/KMK/PL/03/2018',
                'nama' => 'Perlatan Awal Toko KMK',
                'harga' => '55917400.000',
                'nominal' => '55917400.000',
                'residu' => '0.000',
                'umur' => 5,
                'status' => 1,
                'no_transaksi' => '-',
                'keterangan' => 'Perlatan Awal Toko KMK',
                'created_at' => '2018-03-01 00:00:00',
                'updated_at' => '2018-03-29 12:58:22',
            ),
        ));
        
        
    }
}