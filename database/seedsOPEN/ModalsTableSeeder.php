<?php

use Illuminate\Database\Seeder;

class ModalsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('modals')->delete();
        
        \DB::table('modals')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nominal' => '2000000000.000',
                'keterangan' => 'penambahan Modal',
                'created_at' => '2018-03-01 13:58:50',
                'updated_at' => '2018-03-01 13:58:50',
            ),
        ));
        
        
    }
}