<?php

use Illuminate\Database\Seeder;

class PersenPajaksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('persen_pajaks')->delete();
        
        \DB::table('persen_pajaks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'persen' => '1.000',
                'user_id' => 1,
                'created_at' => '2018-03-24 21:14:14',
                'updated_at' => '2018-03-24 21:14:15',
            ),
        ));
        
        
    }
}