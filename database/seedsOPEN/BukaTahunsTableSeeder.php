<?php

use Illuminate\Database\Seeder;

class BukaTahunsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('buka_tahuns')->delete();
        
        \DB::table('buka_tahuns')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-03-29 12:58:21',
                'updated_at' => '2018-03-29 12:58:21',
            ),
        ));
        
        
    }
}