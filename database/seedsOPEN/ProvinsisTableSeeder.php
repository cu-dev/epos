<?php

use Illuminate\Database\Seeder;

class ProvinsisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinsis')->delete();
        
        \DB::table('provinsis')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'Jawa Tengah',
                'created_at' => '2018-03-01 00:23:39',
                'updated_at' => '2018-03-01 00:23:39',
            ),
        ));
        
        
    }
}