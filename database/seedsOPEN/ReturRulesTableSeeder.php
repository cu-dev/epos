<?php

use Illuminate\Database\Seeder;

class ReturRulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('retur_rules')->delete();
        
        \DB::table('retur_rules')->insert(array (
            0 => 
            array (
                'id' => 1,
                'syarat' => 3,
                'user_id' => 1,
                'created_at' => '2018-02-28 18:57:51',
                'updated_at' => '2018-02-28 18:57:51',
            ),
        ));
        
        
    }
}