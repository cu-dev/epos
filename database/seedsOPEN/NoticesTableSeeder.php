<?php

use Illuminate\Database\Seeder;

class NoticesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('notices')->delete();
        
        \DB::table('notices')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transaksi_pembelian_id' => 1,
                'aktif' => 1,
                'created_at' => '2018-03-30 14:22:25',
                'updated_at' => '2018-03-30 14:22:25',
            ),
            1 => 
            array (
                'id' => 2,
                'transaksi_pembelian_id' => 2,
                'aktif' => 1,
                'created_at' => '2018-03-30 15:08:58',
                'updated_at' => '2018-03-30 15:08:58',
            ),
            2 => 
            array (
                'id' => 3,
                'transaksi_pembelian_id' => 3,
                'aktif' => 1,
                'created_at' => '2018-03-30 15:21:03',
                'updated_at' => '2018-03-30 15:21:03',
            ),
            3 => 
            array (
                'id' => 4,
                'transaksi_pembelian_id' => 4,
                'aktif' => 1,
                'created_at' => '2018-03-30 15:50:40',
                'updated_at' => '2018-03-30 15:50:40',
            ),
            4 => 
            array (
                'id' => 5,
                'transaksi_pembelian_id' => 5,
                'aktif' => 1,
                'created_at' => '2018-03-30 16:05:37',
                'updated_at' => '2018-03-30 16:05:37',
            ),
            5 => 
            array (
                'id' => 6,
                'transaksi_pembelian_id' => 6,
                'aktif' => 1,
                'created_at' => '2018-03-30 16:29:14',
                'updated_at' => '2018-03-30 16:29:14',
            ),
            6 => 
            array (
                'id' => 7,
                'transaksi_pembelian_id' => 7,
                'aktif' => 1,
                'created_at' => '2018-03-30 16:37:31',
                'updated_at' => '2018-03-30 16:37:31',
            ),
            7 => 
            array (
                'id' => 8,
                'transaksi_pembelian_id' => 8,
                'aktif' => 1,
                'created_at' => '2018-03-30 16:42:51',
                'updated_at' => '2018-03-30 16:42:51',
            ),
            8 => 
            array (
                'id' => 9,
                'transaksi_pembelian_id' => 9,
                'aktif' => 1,
                'created_at' => '2018-03-30 17:45:58',
                'updated_at' => '2018-03-30 17:45:58',
            ),
            9 => 
            array (
                'id' => 10,
                'transaksi_pembelian_id' => 10,
                'aktif' => 1,
                'created_at' => '2018-03-30 18:39:03',
                'updated_at' => '2018-03-30 18:39:03',
            ),
            10 => 
            array (
                'id' => 11,
                'transaksi_pembelian_id' => 11,
                'aktif' => 1,
                'created_at' => '2018-03-30 19:27:48',
                'updated_at' => '2018-03-30 19:27:48',
            ),
            11 => 
            array (
                'id' => 12,
                'transaksi_pembelian_id' => 12,
                'aktif' => 1,
                'created_at' => '2018-03-30 19:55:42',
                'updated_at' => '2018-03-30 19:55:42',
            ),
            12 => 
            array (
                'id' => 13,
                'transaksi_pembelian_id' => 13,
                'aktif' => 1,
                'created_at' => '2018-03-30 20:45:16',
                'updated_at' => '2018-03-30 20:45:16',
            ),
            13 => 
            array (
                'id' => 14,
                'transaksi_pembelian_id' => 14,
                'aktif' => 1,
                'created_at' => '2018-03-30 21:45:06',
                'updated_at' => '2018-03-30 21:45:06',
            ),
            14 => 
            array (
                'id' => 15,
                'transaksi_pembelian_id' => 15,
                'aktif' => 1,
                'created_at' => '2018-03-31 11:40:15',
                'updated_at' => '2018-03-31 11:40:15',
            ),
            15 => 
            array (
                'id' => 16,
                'transaksi_pembelian_id' => 16,
                'aktif' => 1,
                'created_at' => '2018-03-31 13:37:05',
                'updated_at' => '2018-03-31 13:37:05',
            ),
            16 => 
            array (
                'id' => 17,
                'transaksi_pembelian_id' => 17,
                'aktif' => 1,
                'created_at' => '2018-03-31 13:56:49',
                'updated_at' => '2018-03-31 13:56:49',
            ),
            17 => 
            array (
                'id' => 18,
                'transaksi_pembelian_id' => 18,
                'aktif' => 1,
                'created_at' => '2018-03-31 14:39:20',
                'updated_at' => '2018-03-31 14:39:20',
            ),
            18 => 
            array (
                'id' => 19,
                'transaksi_pembelian_id' => 19,
                'aktif' => 1,
                'created_at' => '2018-03-31 14:56:22',
                'updated_at' => '2018-03-31 14:56:22',
            ),
            19 => 
            array (
                'id' => 20,
                'transaksi_pembelian_id' => 20,
                'aktif' => 1,
                'created_at' => '2018-03-31 15:20:54',
                'updated_at' => '2018-03-31 15:20:54',
            ),
            20 => 
            array (
                'id' => 21,
                'transaksi_pembelian_id' => 21,
                'aktif' => 1,
                'created_at' => '2018-03-31 17:20:46',
                'updated_at' => '2018-03-31 17:20:46',
            ),
            21 => 
            array (
                'id' => 22,
                'transaksi_pembelian_id' => 22,
                'aktif' => 1,
                'created_at' => '2018-03-31 19:06:49',
                'updated_at' => '2018-03-31 19:06:49',
            ),
            22 => 
            array (
                'id' => 23,
                'transaksi_pembelian_id' => 23,
                'aktif' => 1,
                'created_at' => '2018-03-31 19:40:20',
                'updated_at' => '2018-03-31 19:40:20',
            ),
            23 => 
            array (
                'id' => 24,
                'transaksi_pembelian_id' => 24,
                'aktif' => 1,
                'created_at' => '2018-03-31 19:52:13',
                'updated_at' => '2018-03-31 19:52:13',
            ),
            24 => 
            array (
                'id' => 25,
                'transaksi_pembelian_id' => 25,
                'aktif' => 1,
                'created_at' => '2018-03-31 19:59:54',
                'updated_at' => '2018-03-31 19:59:54',
            ),
            25 => 
            array (
                'id' => 26,
                'transaksi_pembelian_id' => 26,
                'aktif' => 1,
                'created_at' => '2018-03-31 22:56:10',
                'updated_at' => '2018-03-31 22:56:10',
            ),
            26 => 
            array (
                'id' => 27,
                'transaksi_pembelian_id' => 27,
                'aktif' => 1,
                'created_at' => '2018-03-31 23:26:45',
                'updated_at' => '2018-03-31 23:26:45',
            ),
            27 => 
            array (
                'id' => 28,
                'transaksi_pembelian_id' => 28,
                'aktif' => 1,
                'created_at' => '2018-03-31 23:36:15',
                'updated_at' => '2018-03-31 23:36:15',
            ),
            28 => 
            array (
                'id' => 29,
                'transaksi_pembelian_id' => 29,
                'aktif' => 1,
                'created_at' => '2018-04-14 13:23:09',
                'updated_at' => '2018-04-14 13:23:09',
            ),
            29 => 
            array (
                'id' => 30,
                'transaksi_pembelian_id' => 30,
                'aktif' => 1,
                'created_at' => '2018-04-14 14:04:12',
                'updated_at' => '2018-04-14 14:04:12',
            ),
        ));
        
        
    }
}