<?php

use Illuminate\Database\Seeder;

class BangunansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bangunans')->delete();
        
        \DB::table('bangunans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'umur' => 20,
                'harga' => '941000100.000',
                'nominal' => '941000100.000',
                'residu' => '0.000',
                'created_at' => '2018-03-01 02:01:11',
                'updated_at' => '2018-03-29 12:58:21',
            ),
        ));
        
        
    }
}