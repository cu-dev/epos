<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'Admin',
                'password' => '$2y$10$.Gewd0a/umgQU6uEZwS3Ve//CNeG/YVUWyFhtHyuumZzDieNCViHK',
                'nama' => 'Admin',
                'email' => 'kencanamulyakarangduwur@gmail.com',
                'alamat' => 'Jl Puring-Petanahan No. 3 Karangduwur',
                'telepon' => '087863300022',
                'tanggal_lahir' => '2018-03-01',
                'foto' => 'default.png',
                'level_id' => 2,
                'status' => 1,
                'cetak_nota' => 1,
                'remember_token' => 'WIp2F3g2n81DZzjqRPlLQ0Ar5BYMyPrQs7VT4z4vj7pVu20OoB1Iip72ynpY',
                'created_at' => '2018-03-01 00:30:49',
                'updated_at' => '2018-04-01 12:48:43',
            ),
            1 => 
            array (
                'id' => 3,
                'username' => 'Eceran',
                'password' => '$2y$10$CexI7q0eqZGQs6SixcQcDexYZU1dSjdka3emP5kZJ/UwBCqPogKIG',
                'nama' => 'Dummy Eceran',
                'email' => 'eceran@gmial.com',
                'alamat' => 'dsfgn',
                'telepon' => '3510',
                'tanggal_lahir' => '2018-03-07',
                'foto' => 'default.png',
                'level_id' => 3,
                'status' => 1,
                'cetak_nota' => 1,
                'remember_token' => NULL,
                'created_at' => '2018-03-19 20:14:58',
                'updated_at' => '2018-03-19 20:14:58',
            ),
            2 => 
            array (
                'id' => 4,
                'username' => 'Grosir',
                'password' => '$2y$10$8kY2v6k8btkzoNumfzOle.dY8bjskoWYx83h5PDrpAO1Im8Svpoxq',
                'nama' => 'Dummy Grosir',
                'email' => 'grosir@gmail.com',
                'alamat' => 'adfasd',
                'telepon' => '54310',
                'tanggal_lahir' => '2018-03-22',
                'foto' => 'default.png',
                'level_id' => 4,
                'status' => 1,
                'cetak_nota' => 1,
                'remember_token' => NULL,
                'created_at' => '2018-03-19 20:15:22',
                'updated_at' => '2018-03-19 20:15:22',
            ),
            3 => 
            array (
                'id' => 5,
                'username' => 'Gudang',
                'password' => '$2y$10$k9xR/TW0Kuw/zatcqo.jG.aZao7x2sCduOyrvw9Z4djjc/dq3hArm',
                'nama' => 'Dummy Gudang',
                'email' => 'gudang@mail.com',
                'alamat' => 'Diaasdasd',
                'telepon' => '514501',
                'tanggal_lahir' => '2018-03-22',
                'foto' => 'default.png',
                'level_id' => 5,
                'status' => 1,
                'cetak_nota' => 1,
                'remember_token' => NULL,
                'created_at' => '2018-03-19 20:16:03',
                'updated_at' => '2018-03-19 20:16:03',
            ),
        ));
        
        
    }
}