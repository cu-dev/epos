<?php

use Illuminate\Database\Seeder;

class CatatansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('catatans')->delete();
        
        \DB::table('catatans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'jenis_catatan' => 'faktur',
                'catatan' => 'SEMOGA',
                'created_at' => '2017-12-21 02:19:03',
                'updated_at' => '2018-02-22 20:34:35',
            ),
            1 => 
            array (
                'id' => 2,
                'jenis_catatan' => 'faktur',
                'catatan' => 'NANTI',
                'created_at' => '2017-12-21 02:19:19',
                'updated_at' => '2018-02-22 20:34:35',
            ),
            2 => 
            array (
                'id' => 3,
                'jenis_catatan' => 'faktur',
                'catatan' => 'SEJAHTERA',
                'created_at' => '2017-12-21 02:21:26',
                'updated_at' => '2018-02-22 20:34:36',
            ),
            3 => 
            array (
                'id' => 4,
                'jenis_catatan' => 'faktur_kredit',
                'catatan' => 'aku mau makan',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:14',
            ),
            4 => 
            array (
                'id' => 5,
                'jenis_catatan' => 'faktur_kredit',
                'catatan' => 'sop di warung',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:14',
            ),
            5 => 
            array (
                'id' => 6,
                'jenis_catatan' => 'faktur_kredit',
                'catatan' => 'sebelah sana',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:15',
            ),
            6 => 
            array (
                'id' => 7,
                'jenis_catatan' => 'surat_jalan',
                'catatan' => 'ku tak bisa',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:56',
            ),
            7 => 
            array (
                'id' => 8,
                'jenis_catatan' => 'deposito',
                'catatan' => 'bersanding',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:56',
            ),
            8 => 
            array (
                'id' => 9,
                'jenis_catatan' => 'deposito',
                'catatan' => 'dengannya',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2017-12-21 04:36:56',
            ),
            9 => 
            array (
                'id' => 10,
                'jenis_catatan' => 'deposito',
                'catatan' => 'TERIMA KASIH',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            10 => 
            array (
                'id' => 11,
                'jenis_catatan' => 'struk',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            11 => 
            array (
                'id' => 12,
                'jenis_catatan' => 'struk',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            12 => 
            array (
                'id' => 13,
                'jenis_catatan' => 'struk',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            13 => 
            array (
                'id' => 14,
                'jenis_catatan' => 'pengambilan',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            14 => 
            array (
                'id' => 15,
                'jenis_catatan' => 'pengambilan',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            15 => 
            array (
                'id' => 16,
                'jenis_catatan' => 'pengambilan',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            16 => 
            array (
                'id' => 17,
                'jenis_catatan' => 'retur',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            17 => 
            array (
                'id' => 18,
                'jenis_catatan' => 'retur',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            18 => 
            array (
                'id' => 19,
                'jenis_catatan' => 'retur',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            19 => 
            array (
                'id' => 20,
                'jenis_catatan' => 'po_beli',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            20 => 
            array (
                'id' => 21,
                'jenis_catatan' => 'po_beli',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
            21 => 
            array (
                'id' => 22,
                'jenis_catatan' => 'po_beli',
                'catatan' => 'TERIMA KASIH BRO',
                'created_at' => '2017-12-21 02:23:00',
                'updated_at' => '2018-02-22 20:34:48',
            ),
        ));
        
        
    }
}