<?php

use Illuminate\Database\Seeder;

class SaldosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('saldos')->delete();
        
        \DB::table('saldos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode_akun' => 100000,
                'debet' => '2746307883.580',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:24',
            ),
            1 => 
            array (
                'id' => 2,
                'kode_akun' => 110000,
                'debet' => '670335068.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:24',
            ),
            2 => 
            array (
                'id' => 3,
                'kode_akun' => 111100,
                'debet' => '54329300.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:24',
            ),
            3 => 
            array (
                'id' => 4,
                'kode_akun' => 111200,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            4 => 
            array (
                'id' => 5,
                'kode_akun' => 111310,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            5 => 
            array (
                'id' => 6,
                'kode_akun' => 111320,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            6 => 
            array (
                'id' => 7,
                'kode_akun' => 111330,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            7 => 
            array (
                'id' => 8,
                'kode_akun' => 112000,
                'debet' => '235910663.390',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            8 => 
            array (
                'id' => 9,
                'kode_akun' => 113000,
                'debet' => '16412700.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            9 => 
            array (
                'id' => 10,
                'kode_akun' => 114100,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:25',
            ),
            10 => 
            array (
                'id' => 11,
                'kode_akun' => 114200,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            11 => 
            array (
                'id' => 12,
                'kode_akun' => 114300,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            12 => 
            array (
                'id' => 13,
                'kode_akun' => 114500,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            13 => 
            array (
                'id' => 14,
                'kode_akun' => 115100,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            14 => 
            array (
                'id' => 15,
                'kode_akun' => 115200,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            15 => 
            array (
                'id' => 16,
                'kode_akun' => 116000,
                'debet' => '23590510.610',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            16 => 
            array (
                'id' => 17,
                'kode_akun' => 120000,
                'debet' => '1916064709.580',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            17 => 
            array (
                'id' => 18,
                'kode_akun' => 121000,
                'debet' => '924000000.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            18 => 
            array (
                'id' => 19,
                'kode_akun' => 122000,
                'debet' => '941000100.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:26',
            ),
            19 => 
            array (
                'id' => 20,
                'kode_akun' => 123000,
                'debet' => '55917400.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            20 => 
            array (
                'id' => 21,
                'kode_akun' => 124000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            21 => 
            array (
                'id' => 22,
                'kode_akun' => 125100,
                'debet' => '0.000',
                'kredit' => '3920833.750',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            22 => 
            array (
                'id' => 23,
                'kode_akun' => 125200,
                'debet' => '0.000',
                'kredit' => '931956.670',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            23 => 
            array (
                'id' => 24,
                'kode_akun' => 125300,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            24 => 
            array (
                'id' => 25,
                'kode_akun' => 200000,
                'debet' => '0.000',
                'kredit' => '258807990.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            25 => 
            array (
                'id' => 26,
                'kode_akun' => 210000,
                'debet' => '0.000',
                'kredit' => '258807990.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            26 => 
            array (
                'id' => 27,
                'kode_akun' => 211000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:27',
            ),
            27 => 
            array (
                'id' => 28,
                'kode_akun' => 212100,
                'debet' => '0.000',
                'kredit' => '258807990.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            28 => 
            array (
                'id' => 29,
                'kode_akun' => 212300,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            29 => 
            array (
                'id' => 30,
                'kode_akun' => 213000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            30 => 
            array (
                'id' => 31,
                'kode_akun' => 214000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            31 => 
            array (
                'id' => 32,
                'kode_akun' => 215100,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            32 => 
            array (
                'id' => 33,
                'kode_akun' => 215200,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            33 => 
            array (
                'id' => 34,
                'kode_akun' => 216000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            34 => 
            array (
                'id' => 35,
                'kode_akun' => 220000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:28',
            ),
            35 => 
            array (
                'id' => 36,
                'kode_akun' => 221000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            36 => 
            array (
                'id' => 37,
                'kode_akun' => 300000,
                'debet' => '0.000',
                'kredit' => '2000000000.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            37 => 
            array (
                'id' => 38,
                'kode_akun' => 310000,
                'debet' => '0.000',
                'kredit' => '-12500106.420',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            38 => 
            array (
                'id' => 39,
                'kode_akun' => 320000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            39 => 
            array (
                'id' => 40,
                'kode_akun' => 330000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            40 => 
            array (
                'id' => 41,
                'kode_akun' => 400000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            41 => 
            array (
                'id' => 42,
                'kode_akun' => 411000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            42 => 
            array (
                'id' => 43,
                'kode_akun' => 412000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:29',
            ),
            43 => 
            array (
                'id' => 44,
                'kode_akun' => 420000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            44 => 
            array (
                'id' => 45,
                'kode_akun' => 430000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            45 => 
            array (
                'id' => 46,
                'kode_akun' => 431000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            46 => 
            array (
                'id' => 47,
                'kode_akun' => 440000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            47 => 
            array (
                'id' => 48,
                'kode_akun' => 450000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            48 => 
            array (
                'id' => 49,
                'kode_akun' => 470000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            49 => 
            array (
                'id' => 50,
                'kode_akun' => 480000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            50 => 
            array (
                'id' => 51,
                'kode_akun' => 500000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:30',
            ),
            51 => 
            array (
                'id' => 52,
                'kode_akun' => 511000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            52 => 
            array (
                'id' => 53,
                'kode_akun' => 512000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            53 => 
            array (
                'id' => 54,
                'kode_akun' => 531000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            54 => 
            array (
                'id' => 55,
                'kode_akun' => 532000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            55 => 
            array (
                'id' => 56,
                'kode_akun' => 541000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            56 => 
            array (
                'id' => 57,
                'kode_akun' => 542000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            57 => 
            array (
                'id' => 58,
                'kode_akun' => 543000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            58 => 
            array (
                'id' => 59,
                'kode_akun' => 551000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:31',
            ),
            59 => 
            array (
                'id' => 60,
                'kode_akun' => 552000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            60 => 
            array (
                'id' => 61,
                'kode_akun' => 553000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            61 => 
            array (
                'id' => 62,
                'kode_akun' => 561000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            62 => 
            array (
                'id' => 63,
                'kode_akun' => 562000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            63 => 
            array (
                'id' => 64,
                'kode_akun' => 563000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            64 => 
            array (
                'id' => 65,
                'kode_akun' => 563100,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            65 => 
            array (
                'id' => 66,
                'kode_akun' => 564000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            66 => 
            array (
                'id' => 67,
                'kode_akun' => 571000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            67 => 
            array (
                'id' => 68,
                'kode_akun' => 572000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:32',
            ),
            68 => 
            array (
                'id' => 69,
                'kode_akun' => 573000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            69 => 
            array (
                'id' => 70,
                'kode_akun' => 580000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            70 => 
            array (
                'id' => 71,
                'kode_akun' => 593000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            71 => 
            array (
                'id' => 72,
                'kode_akun' => 600000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            72 => 
            array (
                'id' => 73,
                'kode_akun' => 575000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            73 => 
            array (
                'id' => 74,
                'kode_akun' => 217000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            74 => 
            array (
                'id' => 75,
                'kode_akun' => 591000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            75 => 
            array (
                'id' => 76,
                'kode_akun' => 592000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            76 => 
            array (
                'id' => 77,
                'kode_akun' => 544000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            77 => 
            array (
                'id' => 78,
                'kode_akun' => 490000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:33',
            ),
            78 => 
            array (
                'id' => 79,
                'kode_akun' => 574000,
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-31 00:00:00',
                'updated_at' => '2018-04-01 00:39:34',
            ),
        ));
        
        
    }
}