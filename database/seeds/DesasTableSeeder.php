<?php

use Illuminate\Database\Seeder;

class DesasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('desas')->delete();
        
        \DB::table('desas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kecamatan_id' => 1,
                'nama' => 'Adikarto',
                'created_at' => '2017-04-28 14:39:00',
                'updated_at' => '2017-04-28 14:39:00',
            ),
            1 => 
            array (
                'id' => 2,
                'kecamatan_id' => 1,
                'nama' => 'Adiluhur',
                'created_at' => '2017-04-29 14:39:00',
                'updated_at' => '2017-04-29 14:39:00',
            ),
            2 => 
            array (
                'id' => 3,
                'kecamatan_id' => 1,
                'nama' => 'Adimulyo',
                'created_at' => '2017-04-30 14:39:00',
                'updated_at' => '2017-04-30 14:39:00',
            ),
            3 => 
            array (
                'id' => 4,
                'kecamatan_id' => 1,
                'nama' => 'Arjomulyo',
                'created_at' => '2017-05-01 14:39:00',
                'updated_at' => '2017-05-01 14:39:00',
            ),
            4 => 
            array (
                'id' => 5,
                'kecamatan_id' => 1,
                'nama' => 'Arjosari',
                'created_at' => '2017-05-02 14:39:00',
                'updated_at' => '2017-05-02 14:39:00',
            ),
            5 => 
            array (
                'id' => 6,
                'kecamatan_id' => 1,
                'nama' => 'Banyuroto',
                'created_at' => '2017-05-03 14:39:00',
                'updated_at' => '2017-05-03 14:39:00',
            ),
            6 => 
            array (
                'id' => 7,
                'kecamatan_id' => 1,
                'nama' => 'Bonjok',
                'created_at' => '2017-05-04 14:39:00',
                'updated_at' => '2017-05-04 14:39:00',
            ),
            7 => 
            array (
                'id' => 8,
                'kecamatan_id' => 1,
                'nama' => 'Candi Wulan',
                'created_at' => '2017-05-05 14:39:00',
                'updated_at' => '2017-05-05 14:39:00',
            ),
            8 => 
            array (
                'id' => 9,
                'kecamatan_id' => 1,
                'nama' => 'Caruban',
                'created_at' => '2017-05-06 14:39:00',
                'updated_at' => '2017-05-06 14:39:00',
            ),
            9 => 
            array (
                'id' => 10,
                'kecamatan_id' => 1,
                'nama' => 'Joho',
                'created_at' => '2017-05-07 14:39:00',
                'updated_at' => '2017-05-07 14:39:00',
            ),
            10 => 
            array (
                'id' => 11,
                'kecamatan_id' => 1,
                'nama' => 'Kemujan',
                'created_at' => '2017-05-08 14:39:00',
                'updated_at' => '2017-05-08 14:39:00',
            ),
            11 => 
            array (
                'id' => 12,
                'kecamatan_id' => 1,
                'nama' => 'Mangunharjo',
                'created_at' => '2017-05-09 14:39:00',
                'updated_at' => '2017-05-09 14:39:00',
            ),
            12 => 
            array (
                'id' => 13,
                'kecamatan_id' => 1,
                'nama' => 'Meles',
                'created_at' => '2017-05-10 14:39:00',
                'updated_at' => '2017-05-10 14:39:00',
            ),
            13 => 
            array (
                'id' => 14,
                'kecamatan_id' => 1,
                'nama' => 'Pekuwon',
                'created_at' => '2017-05-11 14:39:00',
                'updated_at' => '2017-05-11 14:39:00',
            ),
            14 => 
            array (
                'id' => 15,
                'kecamatan_id' => 1,
                'nama' => 'Sekarteja',
                'created_at' => '2017-05-12 14:39:00',
                'updated_at' => '2017-05-12 14:39:00',
            ),
            15 => 
            array (
                'id' => 16,
                'kecamatan_id' => 1,
                'nama' => 'Sidamukti',
                'created_at' => '2017-05-13 14:39:00',
                'updated_at' => '2017-05-13 14:39:00',
            ),
            16 => 
            array (
                'id' => 17,
                'kecamatan_id' => 1,
                'nama' => 'Sidamulyo',
                'created_at' => '2017-05-14 14:39:00',
                'updated_at' => '2017-05-14 14:39:00',
            ),
            17 => 
            array (
                'id' => 18,
                'kecamatan_id' => 1,
                'nama' => 'Sugihwaras',
                'created_at' => '2017-05-15 14:39:00',
                'updated_at' => '2017-05-15 14:39:00',
            ),
            18 => 
            array (
                'id' => 19,
                'kecamatan_id' => 1,
                'nama' => 'Tambakharjo',
                'created_at' => '2017-05-16 14:39:00',
                'updated_at' => '2017-05-16 14:39:00',
            ),
            19 => 
            array (
                'id' => 20,
                'kecamatan_id' => 1,
                'nama' => 'Tegalsari',
                'created_at' => '2017-05-17 14:39:00',
                'updated_at' => '2017-05-17 14:39:00',
            ),
            20 => 
            array (
                'id' => 21,
                'kecamatan_id' => 1,
                'nama' => 'Temanggal',
                'created_at' => '2017-05-18 14:39:00',
                'updated_at' => '2017-05-18 14:39:00',
            ),
            21 => 
            array (
                'id' => 22,
                'kecamatan_id' => 1,
                'nama' => 'Tepakyang',
                'created_at' => '2017-05-19 14:39:00',
                'updated_at' => '2017-05-19 14:39:00',
            ),
            22 => 
            array (
                'id' => 23,
                'kecamatan_id' => 1,
                'nama' => 'Wajasari',
                'created_at' => '2017-05-20 14:39:00',
                'updated_at' => '2017-05-20 14:39:00',
            ),
            23 => 
            array (
                'id' => 24,
                'kecamatan_id' => 2,
                'nama' => 'Bojongsari',
                'created_at' => '2017-05-21 14:39:00',
                'updated_at' => '2017-05-21 14:39:00',
            ),
            24 => 
            array (
                'id' => 25,
                'kecamatan_id' => 2,
                'nama' => 'Jatimulyo',
                'created_at' => '2017-05-22 14:39:00',
                'updated_at' => '2017-05-22 14:39:00',
            ),
            25 => 
            array (
                'id' => 26,
                'kecamatan_id' => 2,
                'nama' => 'Kalijaya',
                'created_at' => '2017-05-23 14:39:00',
                'updated_at' => '2017-05-23 14:39:00',
            ),
            26 => 
            array (
                'id' => 27,
                'kecamatan_id' => 2,
                'nama' => 'Kaliputih',
                'created_at' => '2017-05-24 14:39:00',
                'updated_at' => '2017-05-24 14:39:00',
            ),
            27 => 
            array (
                'id' => 28,
                'kecamatan_id' => 2,
                'nama' => 'Kalirancang',
                'created_at' => '2017-05-25 14:39:00',
                'updated_at' => '2017-05-25 14:39:00',
            ),
            28 => 
            array (
                'id' => 29,
                'kecamatan_id' => 2,
                'nama' => 'Kambangsari',
                'created_at' => '2017-05-26 14:39:00',
                'updated_at' => '2017-05-26 14:39:00',
            ),
            29 => 
            array (
                'id' => 30,
                'kecamatan_id' => 2,
                'nama' => 'Karangkembang',
                'created_at' => '2017-05-27 14:39:00',
                'updated_at' => '2017-05-27 14:39:00',
            ),
            30 => 
            array (
                'id' => 31,
                'kecamatan_id' => 2,
                'nama' => 'Karangtanjung',
                'created_at' => '2017-05-28 14:39:00',
                'updated_at' => '2017-05-28 14:39:00',
            ),
            31 => 
            array (
                'id' => 32,
                'kecamatan_id' => 2,
                'nama' => 'Kemangguhan',
                'created_at' => '2017-05-29 14:39:00',
                'updated_at' => '2017-05-29 14:39:00',
            ),
            32 => 
            array (
                'id' => 33,
                'kecamatan_id' => 2,
                'nama' => 'Krakal',
                'created_at' => '2017-05-30 14:39:00',
                'updated_at' => '2017-05-30 14:39:00',
            ),
            33 => 
            array (
                'id' => 34,
                'kecamatan_id' => 2,
                'nama' => 'Sawangan',
                'created_at' => '2017-05-31 14:39:00',
                'updated_at' => '2017-05-31 14:39:00',
            ),
            34 => 
            array (
                'id' => 35,
                'kecamatan_id' => 2,
                'nama' => 'Seliling',
                'created_at' => '2017-06-01 14:39:00',
                'updated_at' => '2017-06-01 14:39:00',
            ),
            35 => 
            array (
                'id' => 36,
                'kecamatan_id' => 2,
                'nama' => 'Surotrunan',
                'created_at' => '2017-06-02 14:39:00',
                'updated_at' => '2017-06-02 14:39:00',
            ),
            36 => 
            array (
                'id' => 37,
                'kecamatan_id' => 2,
                'nama' => 'Tanuharjo',
                'created_at' => '2017-06-03 14:39:00',
                'updated_at' => '2017-06-03 14:39:00',
            ),
            37 => 
            array (
                'id' => 38,
                'kecamatan_id' => 2,
                'nama' => 'Tlogowulung',
                'created_at' => '2017-06-04 14:39:00',
                'updated_at' => '2017-06-04 14:39:00',
            ),
            38 => 
            array (
                'id' => 39,
                'kecamatan_id' => 2,
                'nama' => 'Wonokromo',
                'created_at' => '2017-06-05 14:39:00',
                'updated_at' => '2017-06-05 14:39:00',
            ),
            39 => 
            array (
                'id' => 40,
                'kecamatan_id' => 3,
                'nama' => 'Ambalkebrek',
                'created_at' => '2017-06-06 14:39:00',
                'updated_at' => '2017-06-06 14:39:00',
            ),
            40 => 
            array (
                'id' => 41,
                'kecamatan_id' => 3,
                'nama' => 'Ambalkliwonan',
                'created_at' => '2017-06-07 14:39:00',
                'updated_at' => '2017-06-07 14:39:00',
            ),
            41 => 
            array (
                'id' => 42,
                'kecamatan_id' => 3,
                'nama' => 'Ambalresmi',
                'created_at' => '2017-06-08 14:39:00',
                'updated_at' => '2017-06-08 14:39:00',
            ),
            42 => 
            array (
                'id' => 43,
                'kecamatan_id' => 3,
                'nama' => 'Ambarwinangun',
                'created_at' => '2017-06-09 14:39:00',
                'updated_at' => '2017-06-09 14:39:00',
            ),
            43 => 
            array (
                'id' => 44,
                'kecamatan_id' => 3,
                'nama' => 'Banjarsari',
                'created_at' => '2017-06-10 14:39:00',
                'updated_at' => '2017-06-10 14:39:00',
            ),
            44 => 
            array (
                'id' => 45,
                'kecamatan_id' => 3,
                'nama' => 'Benerkulon',
                'created_at' => '2017-06-11 14:39:00',
                'updated_at' => '2017-06-11 14:39:00',
            ),
            45 => 
            array (
                'id' => 46,
                'kecamatan_id' => 3,
                'nama' => 'Benerwetan',
                'created_at' => '2017-06-12 14:39:00',
                'updated_at' => '2017-06-12 14:39:00',
            ),
            46 => 
            array (
                'id' => 47,
                'kecamatan_id' => 3,
                'nama' => 'Blengorkulon',
                'created_at' => '2017-06-13 14:39:00',
                'updated_at' => '2017-06-13 14:39:00',
            ),
            47 => 
            array (
                'id' => 48,
                'kecamatan_id' => 3,
                'nama' => 'Blengorwetan',
                'created_at' => '2017-06-14 14:39:00',
                'updated_at' => '2017-06-14 14:39:00',
            ),
            48 => 
            array (
                'id' => 49,
                'kecamatan_id' => 3,
                'nama' => 'Dukuhrejasari',
                'created_at' => '2017-06-15 14:39:00',
                'updated_at' => '2017-06-15 14:39:00',
            ),
            49 => 
            array (
                'id' => 50,
                'kecamatan_id' => 3,
                'nama' => 'Entak',
                'created_at' => '2017-06-16 14:39:00',
                'updated_at' => '2017-06-16 14:39:00',
            ),
            50 => 
            array (
                'id' => 51,
                'kecamatan_id' => 3,
                'nama' => 'Gondanglegi',
                'created_at' => '2017-06-17 14:39:00',
                'updated_at' => '2017-06-17 14:39:00',
            ),
            51 => 
            array (
                'id' => 52,
                'kecamatan_id' => 3,
                'nama' => 'Kaibon',
                'created_at' => '2017-06-18 14:39:00',
                'updated_at' => '2017-06-18 14:39:00',
            ),
            52 => 
            array (
                'id' => 53,
                'kecamatan_id' => 3,
                'nama' => 'Kaibonpetangkuran',
                'created_at' => '2017-06-19 14:39:00',
                'updated_at' => '2017-06-19 14:39:00',
            ),
            53 => 
            array (
                'id' => 54,
                'kecamatan_id' => 3,
                'nama' => 'Kembangsawit',
                'created_at' => '2017-06-20 14:39:00',
                'updated_at' => '2017-06-20 14:39:00',
            ),
            54 => 
            array (
                'id' => 55,
                'kecamatan_id' => 3,
                'nama' => 'Kenayajayan',
                'created_at' => '2017-06-21 14:39:00',
                'updated_at' => '2017-06-21 14:39:00',
            ),
            55 => 
            array (
                'id' => 56,
                'kecamatan_id' => 3,
                'nama' => 'Kradenan',
                'created_at' => '2017-06-22 14:39:00',
                'updated_at' => '2017-06-22 14:39:00',
            ),
            56 => 
            array (
                'id' => 57,
                'kecamatan_id' => 3,
                'nama' => 'Lajer',
                'created_at' => '2017-06-23 14:39:00',
                'updated_at' => '2017-06-23 14:39:00',
            ),
            57 => 
            array (
                'id' => 58,
                'kecamatan_id' => 3,
                'nama' => 'Pagedangan',
                'created_at' => '2017-06-24 14:39:00',
                'updated_at' => '2017-06-24 14:39:00',
            ),
            58 => 
            array (
                'id' => 59,
                'kecamatan_id' => 3,
                'nama' => 'Pasarsenen',
                'created_at' => '2017-06-25 14:39:00',
                'updated_at' => '2017-06-25 14:39:00',
            ),
            59 => 
            array (
                'id' => 60,
                'kecamatan_id' => 3,
                'nama' => 'Peneket',
                'created_at' => '2017-06-26 14:39:00',
                'updated_at' => '2017-06-26 14:39:00',
            ),
            60 => 
            array (
                'id' => 61,
                'kecamatan_id' => 3,
                'nama' => 'Plempukankembaran',
                'created_at' => '2017-06-27 14:39:00',
                'updated_at' => '2017-06-27 14:39:00',
            ),
            61 => 
            array (
                'id' => 62,
                'kecamatan_id' => 3,
                'nama' => 'Prasutan',
                'created_at' => '2017-06-28 14:39:00',
                'updated_at' => '2017-06-28 14:39:00',
            ),
            62 => 
            array (
                'id' => 63,
                'kecamatan_id' => 3,
                'nama' => 'Pucangan',
                'created_at' => '2017-06-29 14:39:00',
                'updated_at' => '2017-06-29 14:39:00',
            ),
            63 => 
            array (
                'id' => 64,
                'kecamatan_id' => 3,
                'nama' => 'Sidareja',
                'created_at' => '2017-06-30 14:39:00',
                'updated_at' => '2017-06-30 14:39:00',
            ),
            64 => 
            array (
                'id' => 65,
                'kecamatan_id' => 3,
                'nama' => 'Sidoluhur',
                'created_at' => '2017-07-01 14:39:00',
                'updated_at' => '2017-07-01 14:39:00',
            ),
            65 => 
            array (
                'id' => 66,
                'kecamatan_id' => 3,
                'nama' => 'Sidomukti',
                'created_at' => '2017-07-02 14:39:00',
                'updated_at' => '2017-07-02 14:39:00',
            ),
            66 => 
            array (
                'id' => 67,
                'kecamatan_id' => 3,
                'nama' => 'Sidomulyo',
                'created_at' => '2017-07-03 14:39:00',
                'updated_at' => '2017-07-03 14:39:00',
            ),
            67 => 
            array (
                'id' => 68,
                'kecamatan_id' => 3,
                'nama' => 'Singosari',
                'created_at' => '2017-07-04 14:39:00',
                'updated_at' => '2017-07-04 14:39:00',
            ),
            68 => 
            array (
                'id' => 69,
                'kecamatan_id' => 3,
                'nama' => 'Sinungreja',
                'created_at' => '2017-07-05 14:39:00',
                'updated_at' => '2017-07-05 14:39:00',
            ),
            69 => 
            array (
                'id' => 70,
                'kecamatan_id' => 3,
                'nama' => 'Sumberjati',
                'created_at' => '2017-07-06 14:39:00',
                'updated_at' => '2017-07-06 14:39:00',
            ),
            70 => 
            array (
                'id' => 71,
                'kecamatan_id' => 3,
                'nama' => 'Surobayan',
                'created_at' => '2017-07-07 14:39:00',
                'updated_at' => '2017-07-07 14:39:00',
            ),
            71 => 
            array (
                'id' => 72,
                'kecamatan_id' => 4,
                'nama' => 'Argopeni',
                'created_at' => '2017-07-08 14:39:00',
                'updated_at' => '2017-07-08 14:39:00',
            ),
            72 => 
            array (
                'id' => 73,
                'kecamatan_id' => 4,
                'nama' => 'Argosari',
                'created_at' => '2017-07-09 14:39:00',
                'updated_at' => '2017-07-09 14:39:00',
            ),
            73 => 
            array (
                'id' => 74,
                'kecamatan_id' => 4,
                'nama' => 'Ayah',
                'created_at' => '2017-07-10 14:39:00',
                'updated_at' => '2017-07-10 14:39:00',
            ),
            74 => 
            array (
                'id' => 75,
                'kecamatan_id' => 4,
                'nama' => 'Banjararjo',
                'created_at' => '2017-07-11 14:39:00',
                'updated_at' => '2017-07-11 14:39:00',
            ),
            75 => 
            array (
                'id' => 76,
                'kecamatan_id' => 4,
                'nama' => 'Bulurejo',
                'created_at' => '2017-07-12 14:39:00',
                'updated_at' => '2017-07-12 14:39:00',
            ),
            76 => 
            array (
                'id' => 77,
                'kecamatan_id' => 4,
                'nama' => 'Candirenggo',
                'created_at' => '2017-07-13 14:39:00',
                'updated_at' => '2017-07-13 14:39:00',
            ),
            77 => 
            array (
                'id' => 78,
                'kecamatan_id' => 4,
                'nama' => 'Demangsari',
                'created_at' => '2017-07-14 14:39:00',
                'updated_at' => '2017-07-14 14:39:00',
            ),
            78 => 
            array (
                'id' => 79,
                'kecamatan_id' => 4,
                'nama' => 'Jatijajar',
                'created_at' => '2017-07-15 14:39:00',
                'updated_at' => '2017-07-15 14:39:00',
            ),
            79 => 
            array (
                'id' => 80,
                'kecamatan_id' => 4,
                'nama' => 'Jintung',
                'created_at' => '2017-07-16 14:39:00',
                'updated_at' => '2017-07-16 14:39:00',
            ),
            80 => 
            array (
                'id' => 81,
                'kecamatan_id' => 4,
                'nama' => 'Kalibangkang',
                'created_at' => '2017-07-17 14:39:00',
                'updated_at' => '2017-07-17 14:39:00',
            ),
            81 => 
            array (
                'id' => 82,
                'kecamatan_id' => 4,
                'nama' => 'Kalipoh',
                'created_at' => '2017-07-18 14:39:00',
                'updated_at' => '2017-07-18 14:39:00',
            ),
            82 => 
            array (
                'id' => 83,
                'kecamatan_id' => 4,
                'nama' => 'Karangduwur',
                'created_at' => '2017-07-19 14:39:00',
                'updated_at' => '2017-07-19 14:39:00',
            ),
            83 => 
            array (
                'id' => 84,
                'kecamatan_id' => 4,
                'nama' => 'Kedungweru',
                'created_at' => '2017-07-20 14:39:00',
                'updated_at' => '2017-07-20 14:39:00',
            ),
            84 => 
            array (
                'id' => 85,
                'kecamatan_id' => 4,
                'nama' => 'Mangunweni',
                'created_at' => '2017-07-21 14:39:00',
                'updated_at' => '2017-07-21 14:39:00',
            ),
            85 => 
            array (
                'id' => 86,
                'kecamatan_id' => 4,
                'nama' => 'Pasir',
                'created_at' => '2017-07-22 14:39:00',
                'updated_at' => '2017-07-22 14:39:00',
            ),
            86 => 
            array (
                'id' => 87,
                'kecamatan_id' => 4,
                'nama' => 'Srati',
                'created_at' => '2017-07-23 14:39:00',
                'updated_at' => '2017-07-23 14:39:00',
            ),
            87 => 
            array (
                'id' => 88,
                'kecamatan_id' => 4,
                'nama' => 'Tlogosari',
                'created_at' => '2017-07-24 14:39:00',
                'updated_at' => '2017-07-24 14:39:00',
            ),
            88 => 
            array (
                'id' => 89,
                'kecamatan_id' => 4,
                'nama' => 'Watukelir',
                'created_at' => '2017-07-25 14:39:00',
                'updated_at' => '2017-07-25 14:39:00',
            ),
            89 => 
            array (
                'id' => 90,
                'kecamatan_id' => 5,
                'nama' => 'Balorejo',
                'created_at' => '2017-07-26 14:39:00',
                'updated_at' => '2017-07-26 14:39:00',
            ),
            90 => 
            array (
                'id' => 91,
                'kecamatan_id' => 5,
                'nama' => 'Bonjokkidul',
                'created_at' => '2017-07-27 14:39:00',
                'updated_at' => '2017-07-27 14:39:00',
            ),
            91 => 
            array (
                'id' => 92,
                'kecamatan_id' => 5,
                'nama' => 'Bonjoklor',
                'created_at' => '2017-07-28 14:39:00',
                'updated_at' => '2017-07-28 14:39:00',
            ),
            92 => 
            array (
                'id' => 93,
                'kecamatan_id' => 5,
                'nama' => 'Bonorowo',
                'created_at' => '2017-07-29 14:39:00',
                'updated_at' => '2017-07-29 14:39:00',
            ),
            93 => 
            array (
                'id' => 94,
                'kecamatan_id' => 5,
                'nama' => 'Mrentul',
                'created_at' => '2017-07-30 14:39:00',
                'updated_at' => '2017-07-30 14:39:00',
            ),
            94 => 
            array (
                'id' => 95,
                'kecamatan_id' => 5,
                'nama' => 'Ngasinan',
                'created_at' => '2017-07-31 14:39:00',
                'updated_at' => '2017-07-31 14:39:00',
            ),
            95 => 
            array (
                'id' => 96,
                'kecamatan_id' => 5,
                'nama' => 'Patukrejo',
                'created_at' => '2017-08-01 14:39:00',
                'updated_at' => '2017-08-01 14:39:00',
            ),
            96 => 
            array (
                'id' => 97,
                'kecamatan_id' => 5,
                'nama' => 'Pujodadi',
                'created_at' => '2017-08-02 14:39:00',
                'updated_at' => '2017-08-02 14:39:00',
            ),
            97 => 
            array (
                'id' => 98,
                'kecamatan_id' => 5,
                'nama' => 'Rowosari',
                'created_at' => '2017-08-03 14:39:00',
                'updated_at' => '2017-08-03 14:39:00',
            ),
            98 => 
            array (
                'id' => 99,
                'kecamatan_id' => 5,
                'nama' => 'Sirnoboyo',
                'created_at' => '2017-08-04 14:39:00',
                'updated_at' => '2017-08-04 14:39:00',
            ),
            99 => 
            array (
                'id' => 100,
                'kecamatan_id' => 5,
                'nama' => 'Tlogorejo',
                'created_at' => '2017-08-05 14:39:00',
                'updated_at' => '2017-08-05 14:39:00',
            ),
            100 => 
            array (
                'id' => 101,
                'kecamatan_id' => 6,
                'nama' => 'Adiwarno',
                'created_at' => '2017-08-06 14:39:00',
                'updated_at' => '2017-08-06 14:39:00',
            ),
            101 => 
            array (
                'id' => 102,
                'kecamatan_id' => 6,
                'nama' => 'Banyumudal',
                'created_at' => '2017-08-07 14:39:00',
                'updated_at' => '2017-08-07 14:39:00',
            ),
            102 => 
            array (
                'id' => 103,
                'kecamatan_id' => 6,
                'nama' => 'Buayan',
                'created_at' => '2017-08-08 14:39:00',
                'updated_at' => '2017-08-08 14:39:00',
            ),
            103 => 
            array (
                'id' => 104,
                'kecamatan_id' => 6,
                'nama' => 'Geblug',
                'created_at' => '2017-08-09 14:39:00',
                'updated_at' => '2017-08-09 14:39:00',
            ),
            104 => 
            array (
                'id' => 105,
                'kecamatan_id' => 6,
                'nama' => 'Jatiroto',
                'created_at' => '2017-08-10 14:39:00',
                'updated_at' => '2017-08-10 14:39:00',
            ),
            105 => 
            array (
                'id' => 106,
                'kecamatan_id' => 6,
                'nama' => 'Jladri',
                'created_at' => '2017-08-11 14:39:00',
                'updated_at' => '2017-08-11 14:39:00',
            ),
            106 => 
            array (
                'id' => 107,
                'kecamatan_id' => 6,
                'nama' => 'Jogomulyo',
                'created_at' => '2017-08-12 14:39:00',
                'updated_at' => '2017-08-12 14:39:00',
            ),
            107 => 
            array (
                'id' => 108,
                'kecamatan_id' => 6,
                'nama' => 'Karangbolong',
                'created_at' => '2017-08-13 14:39:00',
                'updated_at' => '2017-08-13 14:39:00',
            ),
            108 => 
            array (
                'id' => 109,
                'kecamatan_id' => 6,
                'nama' => 'Karangsari',
                'created_at' => '2017-08-14 14:39:00',
                'updated_at' => '2017-08-14 14:39:00',
            ),
            109 => 
            array (
                'id' => 110,
                'kecamatan_id' => 6,
                'nama' => 'Mergosono',
                'created_at' => '2017-08-15 14:39:00',
                'updated_at' => '2017-08-15 14:39:00',
            ),
            110 => 
            array (
                'id' => 111,
                'kecamatan_id' => 6,
                'nama' => 'Nogoraji',
                'created_at' => '2017-08-16 14:39:00',
                'updated_at' => '2017-08-16 14:39:00',
            ),
            111 => 
            array (
                'id' => 112,
                'kecamatan_id' => 6,
                'nama' => 'Pakuran',
                'created_at' => '2017-08-17 14:39:00',
                'updated_at' => '2017-08-17 14:39:00',
            ),
            112 => 
            array (
                'id' => 113,
                'kecamatan_id' => 6,
                'nama' => 'Purbowangi',
                'created_at' => '2017-08-18 14:39:00',
                'updated_at' => '2017-08-18 14:39:00',
            ),
            113 => 
            array (
                'id' => 114,
                'kecamatan_id' => 6,
                'nama' => 'Rangkah',
                'created_at' => '2017-08-19 14:39:00',
                'updated_at' => '2017-08-19 14:39:00',
            ),
            114 => 
            array (
                'id' => 115,
                'kecamatan_id' => 6,
                'nama' => 'Rogodadi',
                'created_at' => '2017-08-20 14:39:00',
                'updated_at' => '2017-08-20 14:39:00',
            ),
            115 => 
            array (
                'id' => 116,
                'kecamatan_id' => 6,
                'nama' => 'Rogodono',
                'created_at' => '2017-08-21 14:39:00',
                'updated_at' => '2017-08-21 14:39:00',
            ),
            116 => 
            array (
                'id' => 117,
                'kecamatan_id' => 6,
                'nama' => 'Semampir',
                'created_at' => '2017-08-22 14:39:00',
                'updated_at' => '2017-08-22 14:39:00',
            ),
            117 => 
            array (
                'id' => 118,
                'kecamatan_id' => 6,
                'nama' => 'Sikayu',
                'created_at' => '2017-08-23 14:39:00',
                'updated_at' => '2017-08-23 14:39:00',
            ),
            118 => 
            array (
                'id' => 119,
                'kecamatan_id' => 6,
                'nama' => 'Tugu',
                'created_at' => '2017-08-24 14:39:00',
                'updated_at' => '2017-08-24 14:39:00',
            ),
            119 => 
            array (
                'id' => 120,
                'kecamatan_id' => 6,
                'nama' => 'Wonodadi',
                'created_at' => '2017-08-25 14:39:00',
                'updated_at' => '2017-08-25 14:39:00',
            ),
            120 => 
            array (
                'id' => 121,
                'kecamatan_id' => 7,
                'nama' => 'Ambalkumolo',
                'created_at' => '2017-08-26 14:39:00',
                'updated_at' => '2017-08-26 14:39:00',
            ),
            121 => 
            array (
                'id' => 122,
                'kecamatan_id' => 7,
                'nama' => 'Ampih',
                'created_at' => '2017-08-27 14:39:00',
                'updated_at' => '2017-08-27 14:39:00',
            ),
            122 => 
            array (
                'id' => 123,
                'kecamatan_id' => 7,
                'nama' => 'Arjowinangun',
                'created_at' => '2017-08-28 14:39:00',
                'updated_at' => '2017-08-28 14:39:00',
            ),
            123 => 
            array (
                'id' => 124,
                'kecamatan_id' => 7,
                'nama' => 'Ayamputih',
                'created_at' => '2017-08-29 14:39:00',
                'updated_at' => '2017-08-29 14:39:00',
            ),
            124 => 
            array (
                'id' => 125,
                'kecamatan_id' => 7,
                'nama' => 'Banjurmukadan',
                'created_at' => '2017-08-30 14:39:00',
                'updated_at' => '2017-08-30 14:39:00',
            ),
            125 => 
            array (
                'id' => 126,
                'kecamatan_id' => 7,
                'nama' => 'Banjurpasar',
                'created_at' => '2017-08-31 14:39:00',
                'updated_at' => '2017-08-31 14:39:00',
            ),
            126 => 
            array (
                'id' => 127,
                'kecamatan_id' => 7,
                'nama' => 'Bocor',
                'created_at' => '2017-09-01 14:39:00',
                'updated_at' => '2017-09-01 14:39:00',
            ),
            127 => 
            array (
                'id' => 128,
                'kecamatan_id' => 7,
                'nama' => 'Brecong',
                'created_at' => '2017-09-02 14:39:00',
                'updated_at' => '2017-09-02 14:39:00',
            ),
            128 => 
            array (
                'id' => 129,
                'kecamatan_id' => 7,
                'nama' => 'Buluspesantren',
                'created_at' => '2017-09-03 14:39:00',
                'updated_at' => '2017-09-03 14:39:00',
            ),
            129 => 
            array (
                'id' => 130,
                'kecamatan_id' => 7,
                'nama' => 'Indrosari',
                'created_at' => '2017-09-04 14:39:00',
                'updated_at' => '2017-09-04 14:39:00',
            ),
            130 => 
            array (
                'id' => 131,
                'kecamatan_id' => 7,
                'nama' => 'Jogopaten',
                'created_at' => '2017-09-05 14:39:00',
                'updated_at' => '2017-09-05 14:39:00',
            ),
            131 => 
            array (
                'id' => 132,
                'kecamatan_id' => 7,
                'nama' => 'Kloposawit',
                'created_at' => '2017-09-06 14:39:00',
                'updated_at' => '2017-09-06 14:39:00',
            ),
            132 => 
            array (
                'id' => 133,
                'kecamatan_id' => 7,
                'nama' => 'Maduretno',
                'created_at' => '2017-09-07 14:39:00',
                'updated_at' => '2017-09-07 14:39:00',
            ),
            133 => 
            array (
                'id' => 134,
                'kecamatan_id' => 7,
                'nama' => 'Rantewringin',
                'created_at' => '2017-09-08 14:39:00',
                'updated_at' => '2017-09-08 14:39:00',
            ),
            134 => 
            array (
                'id' => 135,
                'kecamatan_id' => 7,
                'nama' => 'Sangubanyu',
                'created_at' => '2017-09-09 14:39:00',
                'updated_at' => '2017-09-09 14:39:00',
            ),
            135 => 
            array (
                'id' => 136,
                'kecamatan_id' => 7,
                'nama' => 'Setrojenar',
                'created_at' => '2017-09-10 14:39:00',
                'updated_at' => '2017-09-10 14:39:00',
            ),
            136 => 
            array (
                'id' => 137,
                'kecamatan_id' => 7,
                'nama' => 'Sidomoro',
                'created_at' => '2017-09-11 14:39:00',
                'updated_at' => '2017-09-11 14:39:00',
            ),
            137 => 
            array (
                'id' => 138,
                'kecamatan_id' => 7,
                'nama' => 'Tambakrejo',
                'created_at' => '2017-09-12 14:39:00',
                'updated_at' => '2017-09-12 14:39:00',
            ),
            138 => 
            array (
                'id' => 139,
                'kecamatan_id' => 7,
                'nama' => 'Tanjungrejo',
                'created_at' => '2017-09-13 14:39:00',
                'updated_at' => '2017-09-13 14:39:00',
            ),
            139 => 
            array (
                'id' => 140,
                'kecamatan_id' => 7,
                'nama' => 'Tanjungsari',
                'created_at' => '2017-09-14 14:39:00',
                'updated_at' => '2017-09-14 14:39:00',
            ),
            140 => 
            array (
                'id' => 141,
                'kecamatan_id' => 7,
                'nama' => 'Waluyo',
                'created_at' => '2017-09-15 14:39:00',
                'updated_at' => '2017-09-15 14:39:00',
            ),
            141 => 
            array (
                'id' => 142,
                'kecamatan_id' => 8,
                'nama' => 'Banjarsari',
                'created_at' => '2017-09-16 14:39:00',
                'updated_at' => '2017-09-16 14:39:00',
            ),
            142 => 
            array (
                'id' => 143,
                'kecamatan_id' => 8,
                'nama' => 'Gombong',
                'created_at' => '2017-09-17 14:39:00',
                'updated_at' => '2017-09-17 14:39:00',
            ),
            143 => 
            array (
                'id' => 144,
                'kecamatan_id' => 8,
                'nama' => 'Kalitengah',
                'created_at' => '2017-09-18 14:39:00',
                'updated_at' => '2017-09-18 14:39:00',
            ),
            144 => 
            array (
                'id' => 145,
                'kecamatan_id' => 8,
                'nama' => 'Kedungpuji',
                'created_at' => '2017-09-19 14:39:00',
                'updated_at' => '2017-09-19 14:39:00',
            ),
            145 => 
            array (
                'id' => 146,
                'kecamatan_id' => 8,
                'nama' => 'Kemukus',
                'created_at' => '2017-09-20 14:39:00',
                'updated_at' => '2017-09-20 14:39:00',
            ),
            146 => 
            array (
                'id' => 147,
                'kecamatan_id' => 8,
                'nama' => 'Klopogodo',
                'created_at' => '2017-09-21 14:39:00',
                'updated_at' => '2017-09-21 14:39:00',
            ),
            147 => 
            array (
                'id' => 148,
                'kecamatan_id' => 8,
                'nama' => 'Panjangsari',
                'created_at' => '2017-09-22 14:39:00',
                'updated_at' => '2017-09-22 14:39:00',
            ),
            148 => 
            array (
                'id' => 149,
                'kecamatan_id' => 8,
                'nama' => 'Patemon',
                'created_at' => '2017-09-23 14:39:00',
                'updated_at' => '2017-09-23 14:39:00',
            ),
            149 => 
            array (
                'id' => 150,
                'kecamatan_id' => 8,
                'nama' => 'Semanding',
                'created_at' => '2017-09-24 14:39:00',
                'updated_at' => '2017-09-24 14:39:00',
            ),
            150 => 
            array (
                'id' => 151,
                'kecamatan_id' => 8,
                'nama' => 'Semondo',
                'created_at' => '2017-09-25 14:39:00',
                'updated_at' => '2017-09-25 14:39:00',
            ),
            151 => 
            array (
                'id' => 152,
                'kecamatan_id' => 8,
                'nama' => 'Sidayu',
                'created_at' => '2017-09-26 14:39:00',
                'updated_at' => '2017-09-26 14:39:00',
            ),
            152 => 
            array (
                'id' => 153,
                'kecamatan_id' => 8,
                'nama' => 'Wero',
                'created_at' => '2017-09-27 14:39:00',
                'updated_at' => '2017-09-27 14:39:00',
            ),
            153 => 
            array (
                'id' => 154,
                'kecamatan_id' => 8,
                'nama' => 'Wonokriyo',
                'created_at' => '2017-09-28 14:39:00',
                'updated_at' => '2017-09-28 14:39:00',
            ),
            154 => 
            array (
                'id' => 155,
                'kecamatan_id' => 8,
                'nama' => 'Wonosigro',
                'created_at' => '2017-09-29 14:39:00',
                'updated_at' => '2017-09-29 14:39:00',
            ),
            155 => 
            array (
                'id' => 156,
                'kecamatan_id' => 9,
                'nama' => 'Candi',
                'created_at' => '2017-09-30 14:39:00',
                'updated_at' => '2017-09-30 14:39:00',
            ),
            156 => 
            array (
                'id' => 157,
                'kecamatan_id' => 9,
                'nama' => 'Giripurno',
                'created_at' => '2017-10-01 14:39:00',
                'updated_at' => '2017-10-01 14:39:00',
            ),
            157 => 
            array (
                'id' => 158,
                'kecamatan_id' => 9,
                'nama' => 'Grenggeng',
                'created_at' => '2017-10-02 14:39:00',
                'updated_at' => '2017-10-02 14:39:00',
            ),
            158 => 
            array (
                'id' => 159,
                'kecamatan_id' => 9,
                'nama' => 'Jatiluhur',
                'created_at' => '2017-10-03 14:39:00',
                'updated_at' => '2017-10-03 14:39:00',
            ),
            159 => 
            array (
                'id' => 160,
                'kecamatan_id' => 9,
                'nama' => 'Karanganyar',
                'created_at' => '2017-10-04 14:39:00',
                'updated_at' => '2017-10-04 14:39:00',
            ),
            160 => 
            array (
                'id' => 161,
                'kecamatan_id' => 9,
                'nama' => 'Karangkemiri',
                'created_at' => '2017-10-05 14:39:00',
                'updated_at' => '2017-10-05 14:39:00',
            ),
            161 => 
            array (
                'id' => 162,
                'kecamatan_id' => 9,
                'nama' => 'Panjatan',
                'created_at' => '2017-10-06 14:39:00',
                'updated_at' => '2017-10-06 14:39:00',
            ),
            162 => 
            array (
                'id' => 163,
                'kecamatan_id' => 9,
                'nama' => 'Plarangan',
                'created_at' => '2017-10-07 14:39:00',
                'updated_at' => '2017-10-07 14:39:00',
            ),
            163 => 
            array (
                'id' => 164,
                'kecamatan_id' => 9,
                'nama' => 'Pohkumbang',
                'created_at' => '2017-10-08 14:39:00',
                'updated_at' => '2017-10-08 14:39:00',
            ),
            164 => 
            array (
                'id' => 165,
                'kecamatan_id' => 9,
                'nama' => 'Sidomulyo',
                'created_at' => '2017-10-09 14:39:00',
                'updated_at' => '2017-10-09 14:39:00',
            ),
            165 => 
            array (
                'id' => 166,
                'kecamatan_id' => 9,
                'nama' => 'Wonorejo',
                'created_at' => '2017-10-10 14:39:00',
                'updated_at' => '2017-10-10 14:39:00',
            ),
            166 => 
            array (
                'id' => 167,
                'kecamatan_id' => 10,
                'nama' => 'Binangun',
                'created_at' => '2017-10-11 14:39:00',
                'updated_at' => '2017-10-11 14:39:00',
            ),
            167 => 
            array (
                'id' => 168,
                'kecamatan_id' => 10,
                'nama' => 'Clapar',
                'created_at' => '2017-10-12 14:39:00',
                'updated_at' => '2017-10-12 14:39:00',
            ),
            168 => 
            array (
                'id' => 169,
                'kecamatan_id' => 10,
                'nama' => 'Ginandong',
                'created_at' => '2017-10-13 14:39:00',
                'updated_at' => '2017-10-13 14:39:00',
            ),
            169 => 
            array (
                'id' => 170,
                'kecamatan_id' => 10,
                'nama' => 'Giritirto',
                'created_at' => '2017-10-14 14:39:00',
                'updated_at' => '2017-10-14 14:39:00',
            ),
            170 => 
            array (
                'id' => 171,
                'kecamatan_id' => 10,
                'nama' => 'Glontor',
                'created_at' => '2017-10-15 14:39:00',
                'updated_at' => '2017-10-15 14:39:00',
            ),
            171 => 
            array (
                'id' => 172,
                'kecamatan_id' => 10,
                'nama' => 'Gunungsari',
                'created_at' => '2017-10-16 14:39:00',
                'updated_at' => '2017-10-16 14:39:00',
            ),
            172 => 
            array (
                'id' => 173,
                'kecamatan_id' => 10,
                'nama' => 'Kajoran',
                'created_at' => '2017-10-17 14:39:00',
                'updated_at' => '2017-10-17 14:39:00',
            ),
            173 => 
            array (
                'id' => 174,
                'kecamatan_id' => 10,
                'nama' => 'Kalibening',
                'created_at' => '2017-10-18 14:39:00',
                'updated_at' => '2017-10-18 14:39:00',
            ),
            174 => 
            array (
                'id' => 175,
                'kecamatan_id' => 10,
                'nama' => 'Kalirejo',
                'created_at' => '2017-10-19 14:39:00',
                'updated_at' => '2017-10-19 14:39:00',
            ),
            175 => 
            array (
                'id' => 176,
                'kecamatan_id' => 10,
                'nama' => 'Karanggayam',
                'created_at' => '2017-10-20 14:39:00',
                'updated_at' => '2017-10-20 14:39:00',
            ),
            176 => 
            array (
                'id' => 177,
                'kecamatan_id' => 10,
                'nama' => 'Karangmojo',
                'created_at' => '2017-10-21 14:39:00',
                'updated_at' => '2017-10-21 14:39:00',
            ),
            177 => 
            array (
                'id' => 178,
                'kecamatan_id' => 10,
                'nama' => 'Karangrejo',
                'created_at' => '2017-10-22 14:39:00',
                'updated_at' => '2017-10-22 14:39:00',
            ),
            178 => 
            array (
                'id' => 179,
                'kecamatan_id' => 10,
                'nama' => 'Karangtengah',
                'created_at' => '2017-10-23 14:39:00',
                'updated_at' => '2017-10-23 14:39:00',
            ),
            179 => 
            array (
                'id' => 180,
                'kecamatan_id' => 10,
                'nama' => 'Kebakalan',
                'created_at' => '2017-10-24 14:39:00',
                'updated_at' => '2017-10-24 14:39:00',
            ),
            180 => 
            array (
                'id' => 181,
                'kecamatan_id' => 10,
                'nama' => 'Logandu',
                'created_at' => '2017-10-25 14:39:00',
                'updated_at' => '2017-10-25 14:39:00',
            ),
            181 => 
            array (
                'id' => 182,
                'kecamatan_id' => 10,
                'nama' => 'Pagebangan',
                'created_at' => '2017-10-26 14:39:00',
                'updated_at' => '2017-10-26 14:39:00',
            ),
            182 => 
            array (
                'id' => 183,
                'kecamatan_id' => 10,
                'nama' => 'Penimbun',
                'created_at' => '2017-10-27 14:39:00',
                'updated_at' => '2017-10-27 14:39:00',
            ),
            183 => 
            array (
                'id' => 184,
                'kecamatan_id' => 10,
                'nama' => 'Selogiri',
                'created_at' => '2017-10-28 14:39:00',
                'updated_at' => '2017-10-28 14:39:00',
            ),
            184 => 
            array (
                'id' => 185,
                'kecamatan_id' => 10,
                'nama' => 'Wonotirto',
                'created_at' => '2017-10-29 14:39:00',
                'updated_at' => '2017-10-29 14:39:00',
            ),
            185 => 
            array (
                'id' => 186,
                'kecamatan_id' => 11,
                'nama' => 'Banioro',
                'created_at' => '2017-10-30 14:39:00',
                'updated_at' => '2017-10-30 14:39:00',
            ),
            186 => 
            array (
                'id' => 187,
                'kecamatan_id' => 11,
                'nama' => 'Kaligending',
                'created_at' => '2017-10-31 14:39:00',
                'updated_at' => '2017-10-31 14:39:00',
            ),
            187 => 
            array (
                'id' => 188,
                'kecamatan_id' => 11,
                'nama' => 'Kalisana',
                'created_at' => '2017-11-01 14:39:00',
                'updated_at' => '2017-11-01 14:39:00',
            ),
            188 => 
            array (
                'id' => 189,
                'kecamatan_id' => 11,
                'nama' => 'Karangsambung',
                'created_at' => '2017-11-02 14:39:00',
                'updated_at' => '2017-11-02 14:39:00',
            ),
            189 => 
            array (
                'id' => 190,
                'kecamatan_id' => 11,
                'nama' => 'Kedungwaru',
                'created_at' => '2017-11-03 14:39:00',
                'updated_at' => '2017-11-03 14:39:00',
            ),
            190 => 
            array (
                'id' => 191,
                'kecamatan_id' => 11,
                'nama' => 'Langse',
                'created_at' => '2017-11-04 14:39:00',
                'updated_at' => '2017-11-04 14:39:00',
            ),
            191 => 
            array (
                'id' => 192,
                'kecamatan_id' => 11,
                'nama' => 'Pencil',
                'created_at' => '2017-11-05 14:39:00',
                'updated_at' => '2017-11-05 14:39:00',
            ),
            192 => 
            array (
                'id' => 193,
                'kecamatan_id' => 11,
                'nama' => 'Plumbon',
                'created_at' => '2017-11-06 14:39:00',
                'updated_at' => '2017-11-06 14:39:00',
            ),
            193 => 
            array (
                'id' => 194,
                'kecamatan_id' => 11,
                'nama' => 'Pujotirto',
                'created_at' => '2017-11-07 14:39:00',
                'updated_at' => '2017-11-07 14:39:00',
            ),
            194 => 
            array (
                'id' => 195,
                'kecamatan_id' => 11,
                'nama' => 'Seling',
                'created_at' => '2017-11-08 14:39:00',
                'updated_at' => '2017-11-08 14:39:00',
            ),
            195 => 
            array (
                'id' => 196,
                'kecamatan_id' => 11,
                'nama' => 'Tlepok',
                'created_at' => '2017-11-09 14:39:00',
                'updated_at' => '2017-11-09 14:39:00',
            ),
            196 => 
            array (
                'id' => 197,
                'kecamatan_id' => 11,
                'nama' => 'Totogan',
                'created_at' => '2017-11-10 14:39:00',
                'updated_at' => '2017-11-10 14:39:00',
            ),
            197 => 
            array (
                'id' => 198,
                'kecamatan_id' => 11,
                'nama' => 'Wadasmalang',
                'created_at' => '2017-11-11 14:39:00',
                'updated_at' => '2017-11-11 14:39:00',
            ),
            198 => 
            array (
                'id' => 199,
                'kecamatan_id' => 11,
                'nama' => 'Widoro',
                'created_at' => '2017-11-12 14:39:00',
                'updated_at' => '2017-11-12 14:39:00',
            ),
            199 => 
            array (
                'id' => 200,
                'kecamatan_id' => 12,
                'nama' => 'Adikarso',
                'created_at' => '2017-11-13 14:39:00',
                'updated_at' => '2017-11-13 14:39:00',
            ),
            200 => 
            array (
                'id' => 201,
                'kecamatan_id' => 12,
                'nama' => 'Argopeni',
                'created_at' => '2017-11-14 14:39:00',
                'updated_at' => '2017-11-14 14:39:00',
            ),
            201 => 
            array (
                'id' => 202,
                'kecamatan_id' => 12,
                'nama' => 'Bandung',
                'created_at' => '2017-11-15 14:39:00',
                'updated_at' => '2017-11-15 14:39:00',
            ),
            202 => 
            array (
                'id' => 203,
                'kecamatan_id' => 12,
                'nama' => 'Bumirejo',
                'created_at' => '2017-11-16 14:39:00',
                'updated_at' => '2017-11-16 14:39:00',
            ),
            203 => 
            array (
                'id' => 204,
                'kecamatan_id' => 12,
                'nama' => 'Candimulyo',
                'created_at' => '2017-11-17 14:39:00',
                'updated_at' => '2017-11-17 14:39:00',
            ),
            204 => 
            array (
                'id' => 205,
                'kecamatan_id' => 12,
                'nama' => 'Candiwulan',
                'created_at' => '2017-11-18 14:39:00',
                'updated_at' => '2017-11-18 14:39:00',
            ),
            205 => 
            array (
                'id' => 206,
                'kecamatan_id' => 12,
                'nama' => 'Depokrejo',
                'created_at' => '2017-11-19 14:39:00',
                'updated_at' => '2017-11-19 14:39:00',
            ),
            206 => 
            array (
                'id' => 207,
                'kecamatan_id' => 12,
                'nama' => 'Gemeksekti',
                'created_at' => '2017-11-20 14:39:00',
                'updated_at' => '2017-11-20 14:39:00',
            ),
            207 => 
            array (
                'id' => 208,
                'kecamatan_id' => 12,
                'nama' => 'Gesikan',
                'created_at' => '2017-11-21 14:39:00',
                'updated_at' => '2017-11-21 14:39:00',
            ),
            208 => 
            array (
                'id' => 209,
                'kecamatan_id' => 12,
                'nama' => 'Jatisari',
                'created_at' => '2017-11-22 14:39:00',
                'updated_at' => '2017-11-22 14:39:00',
            ),
            209 => 
            array (
                'id' => 210,
                'kecamatan_id' => 12,
                'nama' => 'Jemur',
                'created_at' => '2017-11-23 14:39:00',
                'updated_at' => '2017-11-23 14:39:00',
            ),
            210 => 
            array (
                'id' => 211,
                'kecamatan_id' => 12,
                'nama' => 'Kalibagor',
                'created_at' => '2017-11-24 14:39:00',
                'updated_at' => '2017-11-24 14:39:00',
            ),
            211 => 
            array (
                'id' => 212,
                'kecamatan_id' => 12,
                'nama' => 'Kalijirek',
                'created_at' => '2017-11-25 14:39:00',
                'updated_at' => '2017-11-25 14:39:00',
            ),
            212 => 
            array (
                'id' => 213,
                'kecamatan_id' => 12,
                'nama' => 'Kalirejo',
                'created_at' => '2017-11-26 14:39:00',
                'updated_at' => '2017-11-26 14:39:00',
            ),
            213 => 
            array (
                'id' => 214,
                'kecamatan_id' => 12,
                'nama' => 'Karangsari',
                'created_at' => '2017-11-27 14:39:00',
                'updated_at' => '2017-11-27 14:39:00',
            ),
            214 => 
            array (
                'id' => 215,
                'kecamatan_id' => 12,
                'nama' => 'Kawedusan',
                'created_at' => '2017-11-28 14:39:00',
                'updated_at' => '2017-11-28 14:39:00',
            ),
            215 => 
            array (
                'id' => 216,
                'kecamatan_id' => 12,
                'nama' => 'Kebumen',
                'created_at' => '2017-11-29 14:39:00',
                'updated_at' => '2017-11-29 14:39:00',
            ),
            216 => 
            array (
                'id' => 217,
                'kecamatan_id' => 12,
                'nama' => 'Kembaran',
                'created_at' => '2017-11-30 14:39:00',
                'updated_at' => '2017-11-30 14:39:00',
            ),
            217 => 
            array (
                'id' => 218,
                'kecamatan_id' => 12,
                'nama' => 'Kutosari',
                'created_at' => '2017-12-01 14:39:00',
                'updated_at' => '2017-12-01 14:39:00',
            ),
            218 => 
            array (
                'id' => 219,
                'kecamatan_id' => 12,
                'nama' => 'Mengkowo',
                'created_at' => '2017-12-02 14:39:00',
                'updated_at' => '2017-12-02 14:39:00',
            ),
            219 => 
            array (
                'id' => 220,
                'kecamatan_id' => 12,
                'nama' => 'Muktirejo',
                'created_at' => '2017-12-03 14:39:00',
                'updated_at' => '2017-12-03 14:39:00',
            ),
            220 => 
            array (
                'id' => 221,
                'kecamatan_id' => 12,
                'nama' => 'Muktisari',
                'created_at' => '2017-12-04 14:39:00',
                'updated_at' => '2017-12-04 14:39:00',
            ),
            221 => 
            array (
                'id' => 222,
                'kecamatan_id' => 12,
                'nama' => 'Panjer',
                'created_at' => '2017-12-05 14:39:00',
                'updated_at' => '2017-12-05 14:39:00',
            ),
            222 => 
            array (
                'id' => 223,
                'kecamatan_id' => 12,
                'nama' => 'Roworejo',
                'created_at' => '2017-12-06 14:39:00',
                'updated_at' => '2017-12-06 14:39:00',
            ),
            223 => 
            array (
                'id' => 224,
                'kecamatan_id' => 12,
                'nama' => 'Selang',
                'created_at' => '2017-12-07 14:39:00',
                'updated_at' => '2017-12-07 14:39:00',
            ),
            224 => 
            array (
                'id' => 225,
                'kecamatan_id' => 12,
                'nama' => 'Sumberadi',
                'created_at' => '2017-12-08 14:39:00',
                'updated_at' => '2017-12-08 14:39:00',
            ),
            225 => 
            array (
                'id' => 226,
                'kecamatan_id' => 12,
                'nama' => 'Tamanwinangun',
                'created_at' => '2017-12-09 14:39:00',
                'updated_at' => '2017-12-09 14:39:00',
            ),
            226 => 
            array (
                'id' => 227,
                'kecamatan_id' => 12,
                'nama' => 'Tanahsari',
                'created_at' => '2017-12-10 14:39:00',
                'updated_at' => '2017-12-10 14:39:00',
            ),
            227 => 
            array (
                'id' => 228,
                'kecamatan_id' => 12,
                'nama' => 'Wonosari',
                'created_at' => '2017-12-11 14:39:00',
                'updated_at' => '2017-12-11 14:39:00',
            ),
            228 => 
            array (
                'id' => 229,
                'kecamatan_id' => 13,
                'nama' => 'Bendogarap',
                'created_at' => '2017-12-12 14:39:00',
                'updated_at' => '2017-12-12 14:39:00',
            ),
            229 => 
            array (
                'id' => 230,
                'kecamatan_id' => 13,
                'nama' => 'Bumiharjo',
                'created_at' => '2017-12-13 14:39:00',
                'updated_at' => '2017-12-13 14:39:00',
            ),
            230 => 
            array (
                'id' => 231,
                'kecamatan_id' => 13,
                'nama' => 'Dorowati',
                'created_at' => '2017-12-14 14:39:00',
                'updated_at' => '2017-12-14 14:39:00',
            ),
            231 => 
            array (
                'id' => 232,
                'kecamatan_id' => 13,
                'nama' => 'Gadungrejo',
                'created_at' => '2017-12-15 14:39:00',
                'updated_at' => '2017-12-15 14:39:00',
            ),
            232 => 
            array (
                'id' => 233,
                'kecamatan_id' => 13,
                'nama' => 'Gebangsari',
                'created_at' => '2017-12-16 14:39:00',
                'updated_at' => '2017-12-16 14:39:00',
            ),
            233 => 
            array (
                'id' => 234,
                'kecamatan_id' => 13,
                'nama' => 'Jatimalang',
                'created_at' => '2017-12-17 14:39:00',
                'updated_at' => '2017-12-17 14:39:00',
            ),
            234 => 
            array (
                'id' => 235,
                'kecamatan_id' => 13,
                'nama' => 'Jerukagung',
                'created_at' => '2017-12-18 14:39:00',
                'updated_at' => '2017-12-18 14:39:00',
            ),
            235 => 
            array (
                'id' => 236,
                'kecamatan_id' => 13,
                'nama' => 'Jogosimo',
                'created_at' => '2017-12-19 14:39:00',
                'updated_at' => '2017-12-19 14:39:00',
            ),
            236 => 
            array (
                'id' => 237,
                'kecamatan_id' => 13,
                'nama' => 'Kaliwungu',
                'created_at' => '2017-12-20 14:39:00',
                'updated_at' => '2017-12-20 14:39:00',
            ),
            237 => 
            array (
                'id' => 238,
                'kecamatan_id' => 13,
                'nama' => 'Karangglonggong',
                'created_at' => '2017-12-21 14:39:00',
                'updated_at' => '2017-12-21 14:39:00',
            ),
            238 => 
            array (
                'id' => 239,
                'kecamatan_id' => 13,
                'nama' => 'Kebadongan',
                'created_at' => '2017-12-22 14:39:00',
                'updated_at' => '2017-12-22 14:39:00',
            ),
            239 => 
            array (
                'id' => 240,
                'kecamatan_id' => 13,
                'nama' => 'Kedungsari',
                'created_at' => '2017-12-23 14:39:00',
                'updated_at' => '2017-12-23 14:39:00',
            ),
            240 => 
            array (
                'id' => 241,
                'kecamatan_id' => 13,
                'nama' => 'Kedungwinangun',
                'created_at' => '2017-12-24 14:39:00',
                'updated_at' => '2017-12-24 14:39:00',
            ),
            241 => 
            array (
                'id' => 242,
                'kecamatan_id' => 13,
                'nama' => 'Klegenrejo',
                'created_at' => '2017-12-25 14:39:00',
                'updated_at' => '2017-12-25 14:39:00',
            ),
            242 => 
            array (
                'id' => 243,
                'kecamatan_id' => 13,
                'nama' => 'Klegenwonosari',
                'created_at' => '2017-12-26 14:39:00',
                'updated_at' => '2017-12-26 14:39:00',
            ),
            243 => 
            array (
                'id' => 244,
                'kecamatan_id' => 13,
                'nama' => 'Klirong',
                'created_at' => '2017-12-27 14:39:00',
                'updated_at' => '2017-12-27 14:39:00',
            ),
            244 => 
            array (
                'id' => 245,
                'kecamatan_id' => 13,
                'nama' => 'Pandanlor',
                'created_at' => '2017-12-28 14:39:00',
                'updated_at' => '2017-12-28 14:39:00',
            ),
            245 => 
            array (
                'id' => 246,
                'kecamatan_id' => 13,
                'nama' => 'Podoluhur',
                'created_at' => '2017-12-29 14:39:00',
                'updated_at' => '2017-12-29 14:39:00',
            ),
            246 => 
            array (
                'id' => 247,
                'kecamatan_id' => 13,
                'nama' => 'Ranterejo',
                'created_at' => '2017-12-30 14:39:00',
                'updated_at' => '2017-12-30 14:39:00',
            ),
            247 => 
            array (
                'id' => 248,
                'kecamatan_id' => 13,
                'nama' => 'Sitirejo',
                'created_at' => '2017-12-31 14:39:00',
                'updated_at' => '2017-12-31 14:39:00',
            ),
            248 => 
            array (
                'id' => 249,
                'kecamatan_id' => 13,
                'nama' => 'Tambakagung',
                'created_at' => '2018-01-01 14:39:00',
                'updated_at' => '2018-01-01 14:39:00',
            ),
            249 => 
            array (
                'id' => 250,
                'kecamatan_id' => 13,
                'nama' => 'Tambakprogaten',
                'created_at' => '2018-01-02 14:39:00',
                'updated_at' => '2018-01-02 14:39:00',
            ),
            250 => 
            array (
                'id' => 251,
                'kecamatan_id' => 13,
                'nama' => 'Tanggulangin',
                'created_at' => '2018-01-03 14:39:00',
                'updated_at' => '2018-01-03 14:39:00',
            ),
            251 => 
            array (
                'id' => 252,
                'kecamatan_id' => 13,
                'nama' => 'Wotbuwono',
                'created_at' => '2018-01-04 14:39:00',
                'updated_at' => '2018-01-04 14:39:00',
            ),
            252 => 
            array (
                'id' => 253,
                'kecamatan_id' => 14,
                'nama' => 'Babadsari',
                'created_at' => '2018-01-05 14:39:00',
                'updated_at' => '2018-01-05 14:39:00',
            ),
            253 => 
            array (
                'id' => 254,
                'kecamatan_id' => 14,
                'nama' => 'Jlegiwinangun',
                'created_at' => '2018-01-06 14:39:00',
                'updated_at' => '2018-01-06 14:39:00',
            ),
            254 => 
            array (
                'id' => 255,
                'kecamatan_id' => 14,
                'nama' => 'Kaliputih',
                'created_at' => '2018-01-07 14:39:00',
                'updated_at' => '2018-01-07 14:39:00',
            ),
            255 => 
            array (
                'id' => 256,
                'kecamatan_id' => 14,
                'nama' => 'Karangsari',
                'created_at' => '2018-01-08 14:39:00',
                'updated_at' => '2018-01-08 14:39:00',
            ),
            256 => 
            array (
                'id' => 257,
                'kecamatan_id' => 14,
                'nama' => 'Korowelang',
                'created_at' => '2018-01-09 14:39:00',
                'updated_at' => '2018-01-09 14:39:00',
            ),
            257 => 
            array (
                'id' => 258,
                'kecamatan_id' => 14,
                'nama' => 'Kutowinangun',
                'created_at' => '2018-01-10 14:39:00',
                'updated_at' => '2018-01-10 14:39:00',
            ),
            258 => 
            array (
                'id' => 259,
                'kecamatan_id' => 14,
                'nama' => 'Kuwarisan',
                'created_at' => '2018-01-11 14:39:00',
                'updated_at' => '2018-01-11 14:39:00',
            ),
            259 => 
            array (
                'id' => 260,
                'kecamatan_id' => 14,
                'nama' => 'Lumbu',
                'created_at' => '2018-01-12 14:39:00',
                'updated_at' => '2018-01-12 14:39:00',
            ),
            260 => 
            array (
                'id' => 261,
                'kecamatan_id' => 14,
                'nama' => 'Lundong',
                'created_at' => '2018-01-13 14:39:00',
                'updated_at' => '2018-01-13 14:39:00',
            ),
            261 => 
            array (
                'id' => 262,
                'kecamatan_id' => 14,
                'nama' => 'Mekarsari',
                'created_at' => '2018-01-14 14:39:00',
                'updated_at' => '2018-01-14 14:39:00',
            ),
            262 => 
            array (
                'id' => 263,
                'kecamatan_id' => 14,
                'nama' => 'Mrinen',
                'created_at' => '2018-01-15 14:39:00',
                'updated_at' => '2018-01-15 14:39:00',
            ),
            263 => 
            array (
                'id' => 264,
                'kecamatan_id' => 14,
                'nama' => 'Pejagatan',
                'created_at' => '2018-01-16 14:39:00',
                'updated_at' => '2018-01-16 14:39:00',
            ),
            264 => 
            array (
                'id' => 265,
                'kecamatan_id' => 14,
                'nama' => 'Pekunden',
                'created_at' => '2018-01-17 14:39:00',
                'updated_at' => '2018-01-17 14:39:00',
            ),
            265 => 
            array (
                'id' => 266,
                'kecamatan_id' => 14,
                'nama' => 'Pesalakan',
                'created_at' => '2018-01-18 14:39:00',
                'updated_at' => '2018-01-18 14:39:00',
            ),
            266 => 
            array (
                'id' => 267,
                'kecamatan_id' => 14,
                'nama' => 'Tanjungmeru',
                'created_at' => '2018-01-19 14:39:00',
                'updated_at' => '2018-01-19 14:39:00',
            ),
            267 => 
            array (
                'id' => 268,
                'kecamatan_id' => 14,
                'nama' => 'Tanjungsari',
                'created_at' => '2018-01-20 14:39:00',
                'updated_at' => '2018-01-20 14:39:00',
            ),
            268 => 
            array (
                'id' => 269,
                'kecamatan_id' => 14,
                'nama' => 'Tanjungseto',
                'created_at' => '2018-01-21 14:39:00',
                'updated_at' => '2018-01-21 14:39:00',
            ),
            269 => 
            array (
                'id' => 270,
                'kecamatan_id' => 14,
                'nama' => 'Triwarno',
                'created_at' => '2018-01-22 14:39:00',
                'updated_at' => '2018-01-22 14:39:00',
            ),
            270 => 
            array (
                'id' => 271,
                'kecamatan_id' => 14,
                'nama' => 'Ungaran',
                'created_at' => '2018-01-23 14:39:00',
                'updated_at' => '2018-01-23 14:39:00',
            ),
            271 => 
            array (
                'id' => 272,
                'kecamatan_id' => 15,
                'nama' => 'Banjareja',
                'created_at' => '2018-01-24 14:39:00',
                'updated_at' => '2018-01-24 14:39:00',
            ),
            272 => 
            array (
                'id' => 273,
                'kecamatan_id' => 15,
                'nama' => 'Bendungan',
                'created_at' => '2018-01-25 14:39:00',
                'updated_at' => '2018-01-25 14:39:00',
            ),
            273 => 
            array (
                'id' => 274,
                'kecamatan_id' => 15,
                'nama' => 'Gandusari',
                'created_at' => '2018-01-26 14:39:00',
                'updated_at' => '2018-01-26 14:39:00',
            ),
            274 => 
            array (
                'id' => 275,
                'kecamatan_id' => 15,
                'nama' => 'Gumawang',
                'created_at' => '2018-01-27 14:39:00',
                'updated_at' => '2018-01-27 14:39:00',
            ),
            275 => 
            array (
                'id' => 276,
                'kecamatan_id' => 15,
                'nama' => 'Gunungmujil',
                'created_at' => '2018-01-28 14:39:00',
                'updated_at' => '2018-01-28 14:39:00',
            ),
            276 => 
            array (
                'id' => 277,
                'kecamatan_id' => 15,
                'nama' => 'Harjodowo',
                'created_at' => '2018-01-29 14:39:00',
                'updated_at' => '2018-01-29 14:39:00',
            ),
            277 => 
            array (
                'id' => 278,
                'kecamatan_id' => 15,
                'nama' => 'Jatimulya',
                'created_at' => '2018-01-30 14:39:00',
                'updated_at' => '2018-01-30 14:39:00',
            ),
            278 => 
            array (
                'id' => 279,
                'kecamatan_id' => 15,
                'nama' => 'Kalipurwo',
                'created_at' => '2018-01-31 14:39:00',
                'updated_at' => '2018-01-31 14:39:00',
            ),
            279 => 
            array (
                'id' => 280,
                'kecamatan_id' => 15,
                'nama' => 'Kamulyan',
                'created_at' => '2018-02-01 14:39:00',
                'updated_at' => '2018-02-01 14:39:00',
            ),
            280 => 
            array (
                'id' => 281,
                'kecamatan_id' => 15,
                'nama' => 'Kuwarasan',
                'created_at' => '2018-02-02 14:39:00',
                'updated_at' => '2018-02-02 14:39:00',
            ),
            281 => 
            array (
                'id' => 282,
                'kecamatan_id' => 15,
                'nama' => 'Kuwaru',
                'created_at' => '2018-02-03 14:39:00',
                'updated_at' => '2018-02-03 14:39:00',
            ),
            282 => 
            array (
                'id' => 283,
                'kecamatan_id' => 15,
                'nama' => 'Lemahduwur',
                'created_at' => '2018-02-04 14:39:00',
                'updated_at' => '2018-02-04 14:39:00',
            ),
            283 => 
            array (
                'id' => 284,
                'kecamatan_id' => 15,
                'nama' => 'Madureso',
                'created_at' => '2018-02-05 14:39:00',
                'updated_at' => '2018-02-05 14:39:00',
            ),
            284 => 
            array (
                'id' => 285,
                'kecamatan_id' => 15,
                'nama' => 'Mangli',
                'created_at' => '2018-02-06 14:39:00',
                'updated_at' => '2018-02-06 14:39:00',
            ),
            285 => 
            array (
                'id' => 286,
                'kecamatan_id' => 15,
                'nama' => 'Ori',
                'created_at' => '2018-02-07 14:39:00',
                'updated_at' => '2018-02-07 14:39:00',
            ),
            286 => 
            array (
                'id' => 287,
                'kecamatan_id' => 15,
                'nama' => 'Pondokgebangsari',
                'created_at' => '2018-02-08 14:39:00',
                'updated_at' => '2018-02-08 14:39:00',
            ),
            287 => 
            array (
                'id' => 288,
                'kecamatan_id' => 15,
                'nama' => 'Purwodadi',
                'created_at' => '2018-02-09 14:39:00',
                'updated_at' => '2018-02-09 14:39:00',
            ),
            288 => 
            array (
                'id' => 289,
                'kecamatan_id' => 15,
                'nama' => 'Sawangan',
                'created_at' => '2018-02-10 14:39:00',
                'updated_at' => '2018-02-10 14:39:00',
            ),
            289 => 
            array (
                'id' => 290,
                'kecamatan_id' => 15,
                'nama' => 'Serut',
                'created_at' => '2018-02-11 14:39:00',
                'updated_at' => '2018-02-11 14:39:00',
            ),
            290 => 
            array (
                'id' => 291,
                'kecamatan_id' => 15,
                'nama' => 'Sidomukti',
                'created_at' => '2018-02-12 14:39:00',
                'updated_at' => '2018-02-12 14:39:00',
            ),
            291 => 
            array (
                'id' => 292,
                'kecamatan_id' => 15,
                'nama' => 'Tambaksari',
                'created_at' => '2018-02-13 14:39:00',
                'updated_at' => '2018-02-13 14:39:00',
            ),
            292 => 
            array (
                'id' => 293,
                'kecamatan_id' => 15,
                'nama' => 'Wonoyoso',
                'created_at' => '2018-02-14 14:39:00',
                'updated_at' => '2018-02-14 14:39:00',
            ),
            293 => 
            array (
                'id' => 294,
                'kecamatan_id' => 16,
                'nama' => 'Karanggede',
                'created_at' => '2018-02-15 14:39:00',
                'updated_at' => '2018-02-15 14:39:00',
            ),
            294 => 
            array (
                'id' => 295,
                'kecamatan_id' => 16,
                'nama' => 'Kertodeso',
                'created_at' => '2018-02-16 14:39:00',
                'updated_at' => '2018-02-16 14:39:00',
            ),
            295 => 
            array (
                'id' => 296,
                'kecamatan_id' => 16,
                'nama' => 'Krubungan',
                'created_at' => '2018-02-17 14:39:00',
                'updated_at' => '2018-02-17 14:39:00',
            ),
            296 => 
            array (
                'id' => 297,
                'kecamatan_id' => 16,
                'nama' => 'Lembupurwo',
                'created_at' => '2018-02-18 14:39:00',
                'updated_at' => '2018-02-18 14:39:00',
            ),
            297 => 
            array (
                'id' => 298,
                'kecamatan_id' => 16,
                'nama' => 'Mangunranan',
                'created_at' => '2018-02-19 14:39:00',
                'updated_at' => '2018-02-19 14:39:00',
            ),
            298 => 
            array (
                'id' => 299,
                'kecamatan_id' => 16,
                'nama' => 'Mirit',
                'created_at' => '2018-02-20 14:39:00',
                'updated_at' => '2018-02-20 14:39:00',
            ),
            299 => 
            array (
                'id' => 300,
                'kecamatan_id' => 16,
                'nama' => 'Miritpetikusan',
                'created_at' => '2018-02-21 14:39:00',
                'updated_at' => '2018-02-21 14:39:00',
            ),
            300 => 
            array (
                'id' => 301,
                'kecamatan_id' => 16,
                'nama' => 'Ngabeyan',
                'created_at' => '2018-02-22 14:39:00',
                'updated_at' => '2018-02-22 14:39:00',
            ),
            301 => 
            array (
                'id' => 302,
                'kecamatan_id' => 16,
                'nama' => 'Patukgawemulyo',
                'created_at' => '2018-02-23 14:39:00',
                'updated_at' => '2018-02-23 14:39:00',
            ),
            302 => 
            array (
                'id' => 303,
                'kecamatan_id' => 16,
                'nama' => 'Patukrejomulyo',
                'created_at' => '2018-02-24 14:39:00',
                'updated_at' => '2018-02-24 14:39:00',
            ),
            303 => 
            array (
                'id' => 304,
                'kecamatan_id' => 16,
                'nama' => 'Pekutan',
                'created_at' => '2018-02-25 14:39:00',
                'updated_at' => '2018-02-25 14:39:00',
            ),
            304 => 
            array (
                'id' => 305,
                'kecamatan_id' => 16,
                'nama' => 'Rowo',
                'created_at' => '2018-02-26 14:39:00',
                'updated_at' => '2018-02-26 14:39:00',
            ),
            305 => 
            array (
                'id' => 306,
                'kecamatan_id' => 16,
                'nama' => 'Sarwogadung',
                'created_at' => '2018-02-27 14:39:00',
                'updated_at' => '2018-02-27 14:39:00',
            ),
            306 => 
            array (
                'id' => 307,
                'kecamatan_id' => 16,
                'nama' => 'Selotumpeng',
                'created_at' => '2018-02-28 14:39:00',
                'updated_at' => '2018-02-28 14:39:00',
            ),
            307 => 
            array (
                'id' => 308,
                'kecamatan_id' => 16,
                'nama' => 'Singoyudan',
                'created_at' => '2018-03-01 14:39:00',
                'updated_at' => '2018-03-01 14:39:00',
            ),
            308 => 
            array (
                'id' => 309,
                'kecamatan_id' => 16,
                'nama' => 'Sitibentar',
                'created_at' => '2018-03-02 14:39:00',
                'updated_at' => '2018-03-02 14:39:00',
            ),
            309 => 
            array (
                'id' => 310,
                'kecamatan_id' => 16,
                'nama' => 'Tlogodepok',
                'created_at' => '2018-03-03 14:39:00',
                'updated_at' => '2018-03-03 14:39:00',
            ),
            310 => 
            array (
                'id' => 311,
                'kecamatan_id' => 16,
                'nama' => 'Tlogopragoto',
                'created_at' => '2018-03-04 14:39:00',
                'updated_at' => '2018-03-04 14:39:00',
            ),
            311 => 
            array (
                'id' => 312,
                'kecamatan_id' => 16,
                'nama' => 'Wergonayan',
                'created_at' => '2018-03-05 14:39:00',
                'updated_at' => '2018-03-05 14:39:00',
            ),
            312 => 
            array (
                'id' => 313,
                'kecamatan_id' => 16,
                'nama' => 'Winong',
                'created_at' => '2018-03-06 14:39:00',
                'updated_at' => '2018-03-06 14:39:00',
            ),
            313 => 
            array (
                'id' => 314,
                'kecamatan_id' => 16,
                'nama' => 'Wirogaten',
                'created_at' => '2018-03-07 14:39:00',
                'updated_at' => '2018-03-07 14:39:00',
            ),
            314 => 
            array (
                'id' => 315,
                'kecamatan_id' => 16,
                'nama' => 'Wiromartan',
                'created_at' => '2018-03-08 14:39:00',
                'updated_at' => '2018-03-08 14:39:00',
            ),
            315 => 
            array (
                'id' => 316,
                'kecamatan_id' => 17,
                'nama' => 'Balingasal',
                'created_at' => '2018-03-09 14:39:00',
                'updated_at' => '2018-03-09 14:39:00',
            ),
            316 => 
            array (
                'id' => 317,
                'kecamatan_id' => 17,
                'nama' => 'Kaligubuk',
                'created_at' => '2018-03-10 14:39:00',
                'updated_at' => '2018-03-10 14:39:00',
            ),
            317 => 
            array (
                'id' => 318,
                'kecamatan_id' => 17,
                'nama' => 'Kalijering',
                'created_at' => '2018-03-11 14:39:00',
                'updated_at' => '2018-03-11 14:39:00',
            ),
            318 => 
            array (
                'id' => 319,
                'kecamatan_id' => 17,
                'nama' => 'Merden',
                'created_at' => '2018-03-12 14:39:00',
                'updated_at' => '2018-03-12 14:39:00',
            ),
            319 => 
            array (
                'id' => 320,
                'kecamatan_id' => 17,
                'nama' => 'Padureso',
                'created_at' => '2018-03-13 14:39:00',
                'updated_at' => '2018-03-13 14:39:00',
            ),
            320 => 
            array (
                'id' => 321,
                'kecamatan_id' => 17,
                'nama' => 'Pejengkolan',
                'created_at' => '2018-03-14 14:39:00',
                'updated_at' => '2018-03-14 14:39:00',
            ),
            321 => 
            array (
                'id' => 322,
                'kecamatan_id' => 17,
                'nama' => 'Rahayu',
                'created_at' => '2018-03-15 14:39:00',
                'updated_at' => '2018-03-15 14:39:00',
            ),
            322 => 
            array (
                'id' => 323,
                'kecamatan_id' => 17,
                'nama' => 'Sendangdalem',
                'created_at' => '2018-03-16 14:39:00',
                'updated_at' => '2018-03-16 14:39:00',
            ),
            323 => 
            array (
                'id' => 324,
                'kecamatan_id' => 17,
                'nama' => 'Sidototo',
                'created_at' => '2018-03-17 14:39:00',
                'updated_at' => '2018-03-17 14:39:00',
            ),
            324 => 
            array (
                'id' => 325,
                'kecamatan_id' => 18,
                'nama' => 'Aditirto',
                'created_at' => '2018-03-18 14:39:00',
                'updated_at' => '2018-03-18 14:39:00',
            ),
            325 => 
            array (
                'id' => 326,
                'kecamatan_id' => 18,
                'nama' => 'Jemur',
                'created_at' => '2018-03-19 14:39:00',
                'updated_at' => '2018-03-19 14:39:00',
            ),
            326 => 
            array (
                'id' => 327,
                'kecamatan_id' => 18,
                'nama' => 'Karangpoh',
                'created_at' => '2018-03-20 14:39:00',
                'updated_at' => '2018-03-20 14:39:00',
            ),
            327 => 
            array (
                'id' => 328,
                'kecamatan_id' => 18,
                'nama' => 'Kebagoran',
                'created_at' => '2018-03-21 14:39:00',
                'updated_at' => '2018-03-21 14:39:00',
            ),
            328 => 
            array (
                'id' => 329,
                'kecamatan_id' => 18,
                'nama' => 'Kebulusan',
                'created_at' => '2018-03-22 14:39:00',
                'updated_at' => '2018-03-22 14:39:00',
            ),
            329 => 
            array (
                'id' => 330,
                'kecamatan_id' => 18,
                'nama' => 'Kedawung',
                'created_at' => '2018-03-23 14:39:00',
                'updated_at' => '2018-03-23 14:39:00',
            ),
            330 => 
            array (
                'id' => 331,
                'kecamatan_id' => 18,
                'nama' => 'Kuwayuhan',
                'created_at' => '2018-03-24 14:39:00',
                'updated_at' => '2018-03-24 14:39:00',
            ),
            331 => 
            array (
                'id' => 332,
                'kecamatan_id' => 18,
                'nama' => 'Logede',
                'created_at' => '2018-03-25 14:39:00',
                'updated_at' => '2018-03-25 14:39:00',
            ),
            332 => 
            array (
                'id' => 333,
                'kecamatan_id' => 18,
                'nama' => 'Pejagoan',
                'created_at' => '2018-03-26 14:39:00',
                'updated_at' => '2018-03-26 14:39:00',
            ),
            333 => 
            array (
                'id' => 334,
                'kecamatan_id' => 18,
                'nama' => 'Pengaringan',
                'created_at' => '2018-03-27 14:39:00',
                'updated_at' => '2018-03-27 14:39:00',
            ),
            334 => 
            array (
                'id' => 335,
                'kecamatan_id' => 18,
                'nama' => 'Peniron',
                'created_at' => '2018-03-28 14:39:00',
                'updated_at' => '2018-03-28 14:39:00',
            ),
            335 => 
            array (
                'id' => 336,
                'kecamatan_id' => 18,
                'nama' => 'Prigi',
                'created_at' => '2018-03-29 14:39:00',
                'updated_at' => '2018-03-29 14:39:00',
            ),
            336 => 
            array (
                'id' => 337,
                'kecamatan_id' => 18,
                'nama' => 'Watulawang',
                'created_at' => '2018-03-30 14:39:00',
                'updated_at' => '2018-03-30 14:39:00',
            ),
            337 => 
            array (
                'id' => 338,
                'kecamatan_id' => 19,
                'nama' => 'Ampelsari',
                'created_at' => '2018-03-31 14:39:00',
                'updated_at' => '2018-03-31 14:39:00',
            ),
            338 => 
            array (
                'id' => 339,
                'kecamatan_id' => 19,
                'nama' => 'Banjarwinangun',
                'created_at' => '2018-04-01 14:39:00',
                'updated_at' => '2018-04-01 14:39:00',
            ),
            339 => 
            array (
                'id' => 340,
                'kecamatan_id' => 19,
                'nama' => 'Grogolbeningsari',
                'created_at' => '2018-04-02 14:39:00',
                'updated_at' => '2018-04-02 14:39:00',
            ),
            340 => 
            array (
                'id' => 341,
                'kecamatan_id' => 19,
                'nama' => 'Grogolpenatus',
                'created_at' => '2018-04-03 14:39:00',
                'updated_at' => '2018-04-03 14:39:00',
            ),
            341 => 
            array (
                'id' => 342,
                'kecamatan_id' => 19,
                'nama' => 'Grujugan',
                'created_at' => '2018-04-04 14:39:00',
                'updated_at' => '2018-04-04 14:39:00',
            ),
            342 => 
            array (
                'id' => 343,
                'kecamatan_id' => 19,
                'nama' => 'Jagamertan',
                'created_at' => '2018-04-05 14:39:00',
                'updated_at' => '2018-04-05 14:39:00',
            ),
            343 => 
            array (
                'id' => 344,
                'kecamatan_id' => 19,
                'nama' => 'Jatimulyo',
                'created_at' => '2018-04-06 14:39:00',
                'updated_at' => '2018-04-06 14:39:00',
            ),
            344 => 
            array (
                'id' => 345,
                'kecamatan_id' => 19,
                'nama' => 'Karangduwur',
                'created_at' => '2018-04-07 14:39:00',
                'updated_at' => '2018-04-07 14:39:00',
            ),
            345 => 
            array (
                'id' => 346,
                'kecamatan_id' => 19,
                'nama' => 'Karanggadung',
                'created_at' => '2018-04-08 14:39:00',
                'updated_at' => '2018-04-08 14:39:00',
            ),
            346 => 
            array (
                'id' => 347,
                'kecamatan_id' => 19,
                'nama' => 'Karangrejo',
                'created_at' => '2018-04-09 14:39:00',
                'updated_at' => '2018-04-09 14:39:00',
            ),
            347 => 
            array (
                'id' => 348,
                'kecamatan_id' => 19,
                'nama' => 'Kebonsari',
                'created_at' => '2018-04-10 14:39:00',
                'updated_at' => '2018-04-10 14:39:00',
            ),
            348 => 
            array (
                'id' => 349,
                'kecamatan_id' => 19,
                'nama' => 'Kritig',
                'created_at' => '2018-04-11 14:39:00',
                'updated_at' => '2018-04-11 14:39:00',
            ),
            349 => 
            array (
                'id' => 350,
                'kecamatan_id' => 19,
                'nama' => 'Kuwangunan',
                'created_at' => '2018-04-12 14:39:00',
                'updated_at' => '2018-04-12 14:39:00',
            ),
            350 => 
            array (
                'id' => 351,
                'kecamatan_id' => 19,
                'nama' => 'Munggu',
                'created_at' => '2018-04-13 14:39:00',
                'updated_at' => '2018-04-13 14:39:00',
            ),
            351 => 
            array (
                'id' => 352,
                'kecamatan_id' => 19,
                'nama' => 'Nampudadi',
                'created_at' => '2018-04-14 14:39:00',
                'updated_at' => '2018-04-14 14:39:00',
            ),
            352 => 
            array (
                'id' => 353,
                'kecamatan_id' => 19,
                'nama' => 'Petanahan',
                'created_at' => '2018-04-15 14:39:00',
                'updated_at' => '2018-04-15 14:39:00',
            ),
            353 => 
            array (
                'id' => 354,
                'kecamatan_id' => 19,
                'nama' => 'Podourip',
                'created_at' => '2018-04-16 14:39:00',
                'updated_at' => '2018-04-16 14:39:00',
            ),
            354 => 
            array (
                'id' => 355,
                'kecamatan_id' => 19,
                'nama' => 'Sidomulyo',
                'created_at' => '2018-04-17 14:39:00',
                'updated_at' => '2018-04-17 14:39:00',
            ),
            355 => 
            array (
                'id' => 356,
                'kecamatan_id' => 19,
                'nama' => 'Tanjungsari',
                'created_at' => '2018-04-18 14:39:00',
                'updated_at' => '2018-04-18 14:39:00',
            ),
            356 => 
            array (
                'id' => 357,
                'kecamatan_id' => 19,
                'nama' => 'Tegalretno',
                'created_at' => '2018-04-19 14:39:00',
                'updated_at' => '2018-04-19 14:39:00',
            ),
            357 => 
            array (
                'id' => 358,
                'kecamatan_id' => 19,
                'nama' => 'Tresnorejo',
                'created_at' => '2018-04-20 14:39:00',
                'updated_at' => '2018-04-20 14:39:00',
            ),
            358 => 
            array (
                'id' => 359,
                'kecamatan_id' => 20,
                'nama' => 'Blater',
                'created_at' => '2018-04-21 14:39:00',
                'updated_at' => '2018-04-21 14:39:00',
            ),
            359 => 
            array (
                'id' => 360,
                'kecamatan_id' => 20,
                'nama' => 'Jatipurus',
                'created_at' => '2018-04-22 14:39:00',
                'updated_at' => '2018-04-22 14:39:00',
            ),
            360 => 
            array (
                'id' => 361,
                'kecamatan_id' => 20,
                'nama' => 'Jembangan',
                'created_at' => '2018-04-23 14:39:00',
                'updated_at' => '2018-04-23 14:39:00',
            ),
            361 => 
            array (
                'id' => 362,
                'kecamatan_id' => 20,
                'nama' => 'Karangtengah',
                'created_at' => '2018-04-24 14:39:00',
                'updated_at' => '2018-04-24 14:39:00',
            ),
            362 => 
            array (
                'id' => 363,
                'kecamatan_id' => 20,
                'nama' => 'Kebapangan',
                'created_at' => '2018-04-25 14:39:00',
                'updated_at' => '2018-04-25 14:39:00',
            ),
            363 => 
            array (
                'id' => 364,
                'kecamatan_id' => 20,
                'nama' => 'Kedungdowo',
                'created_at' => '2018-04-26 14:39:00',
                'updated_at' => '2018-04-26 14:39:00',
            ),
            364 => 
            array (
                'id' => 365,
                'kecamatan_id' => 20,
                'nama' => 'Lerepkebumen',
                'created_at' => '2018-04-27 14:39:00',
                'updated_at' => '2018-04-27 14:39:00',
            ),
            365 => 
            array (
                'id' => 366,
                'kecamatan_id' => 20,
                'nama' => 'Poncowarno',
                'created_at' => '2018-04-28 14:39:00',
                'updated_at' => '2018-04-28 14:39:00',
            ),
            366 => 
            array (
                'id' => 367,
                'kecamatan_id' => 20,
                'nama' => 'Soka',
                'created_at' => '2018-04-29 14:39:00',
                'updated_at' => '2018-04-29 14:39:00',
            ),
            367 => 
            array (
                'id' => 368,
                'kecamatan_id' => 20,
                'nama' => 'Tegalrejo',
                'created_at' => '2018-04-30 14:39:00',
                'updated_at' => '2018-04-30 14:39:00',
            ),
            368 => 
            array (
                'id' => 369,
                'kecamatan_id' => 20,
                'nama' => 'Tirtomoyo',
                'created_at' => '2018-05-01 14:39:00',
                'updated_at' => '2018-05-01 14:39:00',
            ),
            369 => 
            array (
                'id' => 370,
                'kecamatan_id' => 21,
                'nama' => 'Bagung',
                'created_at' => '2018-05-02 14:39:00',
                'updated_at' => '2018-05-02 14:39:00',
            ),
            370 => 
            array (
                'id' => 371,
                'kecamatan_id' => 21,
                'nama' => 'Kabekelan',
                'created_at' => '2018-05-03 14:39:00',
                'updated_at' => '2018-05-03 14:39:00',
            ),
            371 => 
            array (
                'id' => 372,
                'kecamatan_id' => 21,
                'nama' => 'Kabuaran',
                'created_at' => '2018-05-04 14:39:00',
                'updated_at' => '2018-05-04 14:39:00',
            ),
            372 => 
            array (
                'id' => 373,
                'kecamatan_id' => 21,
                'nama' => 'Kedungbulus',
                'created_at' => '2018-05-05 14:39:00',
                'updated_at' => '2018-05-05 14:39:00',
            ),
            373 => 
            array (
                'id' => 374,
                'kecamatan_id' => 21,
                'nama' => 'Kedungwaru',
                'created_at' => '2018-05-06 14:39:00',
                'updated_at' => '2018-05-06 14:39:00',
            ),
            374 => 
            array (
                'id' => 375,
                'kecamatan_id' => 21,
                'nama' => 'Mulyosri',
                'created_at' => '2018-05-07 14:39:00',
                'updated_at' => '2018-05-07 14:39:00',
            ),
            375 => 
            array (
                'id' => 376,
                'kecamatan_id' => 21,
                'nama' => 'Pecarikan',
                'created_at' => '2018-05-08 14:39:00',
                'updated_at' => '2018-05-08 14:39:00',
            ),
            376 => 
            array (
                'id' => 377,
                'kecamatan_id' => 21,
                'nama' => 'Pesuningan',
                'created_at' => '2018-05-09 14:39:00',
                'updated_at' => '2018-05-09 14:39:00',
            ),
            377 => 
            array (
                'id' => 378,
                'kecamatan_id' => 21,
                'nama' => 'Prembun',
                'created_at' => '2018-05-10 14:39:00',
                'updated_at' => '2018-05-10 14:39:00',
            ),
            378 => 
            array (
                'id' => 379,
                'kecamatan_id' => 21,
                'nama' => 'Sembirkadipaten',
                'created_at' => '2018-05-11 14:39:00',
                'updated_at' => '2018-05-11 14:39:00',
            ),
            379 => 
            array (
                'id' => 380,
                'kecamatan_id' => 21,
                'nama' => 'Sidogede',
                'created_at' => '2018-05-12 14:39:00',
                'updated_at' => '2018-05-12 14:39:00',
            ),
            380 => 
            array (
                'id' => 381,
                'kecamatan_id' => 21,
                'nama' => 'Tersobo',
                'created_at' => '2018-05-13 14:39:00',
                'updated_at' => '2018-05-13 14:39:00',
            ),
            381 => 
            array (
                'id' => 382,
                'kecamatan_id' => 21,
                'nama' => 'Tunggalroso',
                'created_at' => '2018-05-14 14:39:00',
                'updated_at' => '2018-05-14 14:39:00',
            ),
            382 => 
            array (
                'id' => 383,
                'kecamatan_id' => 22,
                'nama' => 'Arjowinangun',
                'created_at' => '2018-05-15 14:39:00',
                'updated_at' => '2018-05-15 14:39:00',
            ),
            383 => 
            array (
                'id' => 384,
                'kecamatan_id' => 22,
                'nama' => 'Banjarejo',
                'created_at' => '2018-05-16 14:39:00',
                'updated_at' => '2018-05-16 14:39:00',
            ),
            384 => 
            array (
                'id' => 385,
                'kecamatan_id' => 22,
                'nama' => 'Bumirejo',
                'created_at' => '2018-05-17 14:39:00',
                'updated_at' => '2018-05-17 14:39:00',
            ),
            385 => 
            array (
                'id' => 386,
                'kecamatan_id' => 22,
                'nama' => 'Kaleng',
                'created_at' => '2018-05-18 14:39:00',
                'updated_at' => '2018-05-18 14:39:00',
            ),
            386 => 
            array (
                'id' => 387,
                'kecamatan_id' => 22,
                'nama' => 'Kedalemankulon',
                'created_at' => '2018-05-19 14:39:00',
                'updated_at' => '2018-05-19 14:39:00',
            ),
            387 => 
            array (
                'id' => 388,
                'kecamatan_id' => 22,
                'nama' => 'Kedalemanwetan',
                'created_at' => '2018-05-20 14:39:00',
                'updated_at' => '2018-05-20 14:39:00',
            ),
            388 => 
            array (
                'id' => 389,
                'kecamatan_id' => 22,
                'nama' => 'Krandegan',
                'created_at' => '2018-05-21 14:39:00',
                'updated_at' => '2018-05-21 14:39:00',
            ),
            389 => 
            array (
                'id' => 390,
                'kecamatan_id' => 22,
                'nama' => 'Madurejo',
                'created_at' => '2018-05-22 14:39:00',
                'updated_at' => '2018-05-22 14:39:00',
            ),
            390 => 
            array (
                'id' => 391,
                'kecamatan_id' => 22,
            'nama' => 'Pasuruhan (Pasuruan)',
                'created_at' => '2018-05-23 14:39:00',
                'updated_at' => '2018-05-23 14:39:00',
            ),
            391 => 
            array (
                'id' => 392,
                'kecamatan_id' => 22,
                'nama' => 'Puliharjo',
                'created_at' => '2018-05-24 14:39:00',
                'updated_at' => '2018-05-24 14:39:00',
            ),
            392 => 
            array (
                'id' => 393,
                'kecamatan_id' => 22,
                'nama' => 'Purwoharjo',
                'created_at' => '2018-05-25 14:39:00',
                'updated_at' => '2018-05-25 14:39:00',
            ),
            393 => 
            array (
                'id' => 394,
                'kecamatan_id' => 22,
                'nama' => 'Purwosari',
                'created_at' => '2018-05-26 14:39:00',
                'updated_at' => '2018-05-26 14:39:00',
            ),
            394 => 
            array (
                'id' => 395,
                'kecamatan_id' => 22,
                'nama' => 'Sidobunder',
                'created_at' => '2018-05-27 14:39:00',
                'updated_at' => '2018-05-27 14:39:00',
            ),
            395 => 
            array (
                'id' => 396,
                'kecamatan_id' => 22,
                'nama' => 'Sidodadi',
                'created_at' => '2018-05-28 14:39:00',
                'updated_at' => '2018-05-28 14:39:00',
            ),
            396 => 
            array (
                'id' => 397,
                'kecamatan_id' => 22,
                'nama' => 'Sidoharjo',
                'created_at' => '2018-05-29 14:39:00',
                'updated_at' => '2018-05-29 14:39:00',
            ),
            397 => 
            array (
                'id' => 398,
                'kecamatan_id' => 22,
                'nama' => 'Sitiadi',
                'created_at' => '2018-05-30 14:39:00',
                'updated_at' => '2018-05-30 14:39:00',
            ),
            398 => 
            array (
                'id' => 399,
                'kecamatan_id' => 22,
                'nama' => 'Srusuhjurutengah',
                'created_at' => '2018-05-31 14:39:00',
                'updated_at' => '2018-05-31 14:39:00',
            ),
            399 => 
            array (
                'id' => 400,
                'kecamatan_id' => 22,
                'nama' => 'Surorejan',
                'created_at' => '2018-06-01 14:39:00',
                'updated_at' => '2018-06-01 14:39:00',
            ),
            400 => 
            array (
                'id' => 401,
                'kecamatan_id' => 22,
                'nama' => 'Tambakmulyo',
                'created_at' => '2018-06-02 14:39:00',
                'updated_at' => '2018-06-02 14:39:00',
            ),
            401 => 
            array (
                'id' => 402,
                'kecamatan_id' => 22,
                'nama' => 'Tukinggedong',
                'created_at' => '2018-06-03 14:39:00',
                'updated_at' => '2018-06-03 14:39:00',
            ),
            402 => 
            array (
                'id' => 403,
                'kecamatan_id' => 22,
                'nama' => 'Waluyorejo',
                'created_at' => '2018-06-04 14:39:00',
                'updated_at' => '2018-06-04 14:39:00',
            ),
            403 => 
            array (
                'id' => 404,
                'kecamatan_id' => 22,
                'nama' => 'Wetonkulon',
                'created_at' => '2018-06-05 14:39:00',
                'updated_at' => '2018-06-05 14:39:00',
            ),
            404 => 
            array (
                'id' => 405,
                'kecamatan_id' => 22,
                'nama' => 'Wetonwetan',
                'created_at' => '2018-06-06 14:39:00',
                'updated_at' => '2018-06-06 14:39:00',
            ),
            405 => 
            array (
                'id' => 406,
                'kecamatan_id' => 23,
                'nama' => 'Bumiagung',
                'created_at' => '2018-06-07 14:39:00',
                'updated_at' => '2018-06-07 14:39:00',
            ),
            406 => 
            array (
                'id' => 407,
                'kecamatan_id' => 23,
                'nama' => 'Giyanti',
                'created_at' => '2018-06-08 14:39:00',
                'updated_at' => '2018-06-08 14:39:00',
            ),
            407 => 
            array (
                'id' => 408,
                'kecamatan_id' => 23,
                'nama' => 'Jatiluhur',
                'created_at' => '2018-06-09 14:39:00',
                'updated_at' => '2018-06-09 14:39:00',
            ),
            408 => 
            array (
                'id' => 409,
                'kecamatan_id' => 23,
                'nama' => 'Kalisari',
                'created_at' => '2018-06-10 14:39:00',
                'updated_at' => '2018-06-10 14:39:00',
            ),
            409 => 
            array (
                'id' => 410,
                'kecamatan_id' => 23,
                'nama' => 'Kretek',
                'created_at' => '2018-06-11 14:39:00',
                'updated_at' => '2018-06-11 14:39:00',
            ),
            410 => 
            array (
                'id' => 411,
                'kecamatan_id' => 23,
                'nama' => 'Pringtutul',
                'created_at' => '2018-06-12 14:39:00',
                'updated_at' => '2018-06-12 14:39:00',
            ),
            411 => 
            array (
                'id' => 412,
                'kecamatan_id' => 23,
                'nama' => 'Redisari',
                'created_at' => '2018-06-13 14:39:00',
                'updated_at' => '2018-06-13 14:39:00',
            ),
            412 => 
            array (
                'id' => 413,
                'kecamatan_id' => 23,
                'nama' => 'Rowokele',
                'created_at' => '2018-06-14 14:39:00',
                'updated_at' => '2018-06-14 14:39:00',
            ),
            413 => 
            array (
                'id' => 414,
                'kecamatan_id' => 23,
                'nama' => 'Sukomulyo',
                'created_at' => '2018-06-15 14:39:00',
                'updated_at' => '2018-06-15 14:39:00',
            ),
            414 => 
            array (
                'id' => 415,
                'kecamatan_id' => 23,
                'nama' => 'Wagirpandan',
                'created_at' => '2018-06-16 14:39:00',
                'updated_at' => '2018-06-16 14:39:00',
            ),
            415 => 
            array (
                'id' => 416,
                'kecamatan_id' => 23,
                'nama' => 'Wonoharjo',
                'created_at' => '2018-06-17 14:39:00',
                'updated_at' => '2018-06-17 14:39:00',
            ),
            416 => 
            array (
                'id' => 417,
                'kecamatan_id' => 24,
                'nama' => 'Cangkring',
                'created_at' => '2018-06-18 14:39:00',
                'updated_at' => '2018-06-18 14:39:00',
            ),
            417 => 
            array (
                'id' => 418,
                'kecamatan_id' => 24,
                'nama' => 'Kedunggong',
                'created_at' => '2018-06-19 14:39:00',
                'updated_at' => '2018-06-19 14:39:00',
            ),
            418 => 
            array (
                'id' => 419,
                'kecamatan_id' => 24,
                'nama' => 'Pucangan',
                'created_at' => '2018-06-20 14:39:00',
                'updated_at' => '2018-06-20 14:39:00',
            ),
            419 => 
            array (
                'id' => 420,
                'kecamatan_id' => 24,
                'nama' => 'Sadangkulon',
                'created_at' => '2018-06-21 14:39:00',
                'updated_at' => '2018-06-21 14:39:00',
            ),
            420 => 
            array (
                'id' => 421,
                'kecamatan_id' => 24,
                'nama' => 'Sadangwetan',
                'created_at' => '2018-06-22 14:39:00',
                'updated_at' => '2018-06-22 14:39:00',
            ),
            421 => 
            array (
                'id' => 422,
                'kecamatan_id' => 24,
                'nama' => 'Seboro',
                'created_at' => '2018-06-23 14:39:00',
                'updated_at' => '2018-06-23 14:39:00',
            ),
            422 => 
            array (
                'id' => 423,
                'kecamatan_id' => 24,
                'nama' => 'Wonosari',
                'created_at' => '2018-06-24 14:39:00',
                'updated_at' => '2018-06-24 14:39:00',
            ),
            423 => 
            array (
                'id' => 424,
                'kecamatan_id' => 25,
                'nama' => 'Bejiruyung',
                'created_at' => '2018-06-25 14:39:00',
                'updated_at' => '2018-06-25 14:39:00',
            ),
            424 => 
            array (
                'id' => 425,
                'kecamatan_id' => 25,
                'nama' => 'Bonosari',
                'created_at' => '2018-06-26 14:39:00',
                'updated_at' => '2018-06-26 14:39:00',
            ),
            425 => 
            array (
                'id' => 426,
                'kecamatan_id' => 25,
                'nama' => 'Donorojo',
                'created_at' => '2018-06-27 14:39:00',
                'updated_at' => '2018-06-27 14:39:00',
            ),
            426 => 
            array (
                'id' => 427,
                'kecamatan_id' => 25,
                'nama' => 'Jatinegoro',
                'created_at' => '2018-06-28 14:39:00',
                'updated_at' => '2018-06-28 14:39:00',
            ),
            427 => 
            array (
                'id' => 428,
                'kecamatan_id' => 25,
                'nama' => 'Kalibeji',
                'created_at' => '2018-06-29 14:39:00',
                'updated_at' => '2018-06-29 14:39:00',
            ),
            428 => 
            array (
                'id' => 429,
                'kecamatan_id' => 25,
                'nama' => 'Kedungjati',
                'created_at' => '2018-06-30 14:39:00',
                'updated_at' => '2018-06-30 14:39:00',
            ),
            429 => 
            array (
                'id' => 430,
                'kecamatan_id' => 25,
                'nama' => 'Kedungwringin',
                'created_at' => '2018-07-01 14:39:00',
                'updated_at' => '2018-07-01 14:39:00',
            ),
            430 => 
            array (
                'id' => 431,
                'kecamatan_id' => 25,
                'nama' => 'Kenteng',
                'created_at' => '2018-07-02 14:39:00',
                'updated_at' => '2018-07-02 14:39:00',
            ),
            431 => 
            array (
                'id' => 432,
                'kecamatan_id' => 25,
                'nama' => 'Pekuncen',
                'created_at' => '2018-07-03 14:39:00',
                'updated_at' => '2018-07-03 14:39:00',
            ),
            432 => 
            array (
                'id' => 433,
                'kecamatan_id' => 25,
                'nama' => 'Sampang',
                'created_at' => '2018-07-04 14:39:00',
                'updated_at' => '2018-07-04 14:39:00',
            ),
            433 => 
            array (
                'id' => 434,
                'kecamatan_id' => 25,
                'nama' => 'Selokerto',
                'created_at' => '2018-07-05 14:39:00',
                'updated_at' => '2018-07-05 14:39:00',
            ),
            434 => 
            array (
                'id' => 435,
                'kecamatan_id' => 25,
                'nama' => 'Semali',
                'created_at' => '2018-07-06 14:39:00',
                'updated_at' => '2018-07-06 14:39:00',
            ),
            435 => 
            array (
                'id' => 436,
                'kecamatan_id' => 25,
                'nama' => 'Sempor',
                'created_at' => '2018-07-07 14:39:00',
                'updated_at' => '2018-07-07 14:39:00',
            ),
            436 => 
            array (
                'id' => 437,
                'kecamatan_id' => 25,
                'nama' => 'Sidoharum',
                'created_at' => '2018-07-08 14:39:00',
                'updated_at' => '2018-07-08 14:39:00',
            ),
            437 => 
            array (
                'id' => 438,
                'kecamatan_id' => 25,
                'nama' => 'Somagede',
                'created_at' => '2018-07-09 14:39:00',
                'updated_at' => '2018-07-09 14:39:00',
            ),
            438 => 
            array (
                'id' => 439,
                'kecamatan_id' => 25,
                'nama' => 'Tunjungseto',
                'created_at' => '2018-07-10 14:39:00',
                'updated_at' => '2018-07-10 14:39:00',
            ),
            439 => 
            array (
                'id' => 440,
                'kecamatan_id' => 26,
                'nama' => 'Condongcampur',
                'created_at' => '2018-07-11 14:39:00',
                'updated_at' => '2018-07-11 14:39:00',
            ),
            440 => 
            array (
                'id' => 441,
                'kecamatan_id' => 26,
                'nama' => 'Donosari',
                'created_at' => '2018-07-12 14:39:00',
                'updated_at' => '2018-07-12 14:39:00',
            ),
            441 => 
            array (
                'id' => 442,
                'kecamatan_id' => 26,
                'nama' => 'Giwangretno',
                'created_at' => '2018-07-13 14:39:00',
                'updated_at' => '2018-07-13 14:39:00',
            ),
            442 => 
            array (
                'id' => 443,
                'kecamatan_id' => 26,
                'nama' => 'Jabres',
                'created_at' => '2018-07-14 14:39:00',
                'updated_at' => '2018-07-14 14:39:00',
            ),
            443 => 
            array (
                'id' => 444,
                'kecamatan_id' => 26,
                'nama' => 'Karanggedang',
                'created_at' => '2018-07-15 14:39:00',
                'updated_at' => '2018-07-15 14:39:00',
            ),
            444 => 
            array (
                'id' => 445,
                'kecamatan_id' => 26,
                'nama' => 'Karangjambu',
                'created_at' => '2018-07-16 14:39:00',
                'updated_at' => '2018-07-16 14:39:00',
            ),
            445 => 
            array (
                'id' => 446,
                'kecamatan_id' => 26,
                'nama' => 'Karangpule',
                'created_at' => '2018-07-17 14:39:00',
                'updated_at' => '2018-07-17 14:39:00',
            ),
            446 => 
            array (
                'id' => 447,
                'kecamatan_id' => 26,
                'nama' => 'Karangsari',
                'created_at' => '2018-07-18 14:39:00',
                'updated_at' => '2018-07-18 14:39:00',
            ),
            447 => 
            array (
                'id' => 448,
                'kecamatan_id' => 26,
                'nama' => 'Kejawang',
                'created_at' => '2018-07-19 14:39:00',
                'updated_at' => '2018-07-19 14:39:00',
            ),
            448 => 
            array (
                'id' => 449,
                'kecamatan_id' => 26,
                'nama' => 'Klepusanggar',
                'created_at' => '2018-07-20 14:39:00',
                'updated_at' => '2018-07-20 14:39:00',
            ),
            449 => 
            array (
                'id' => 450,
                'kecamatan_id' => 26,
                'nama' => 'Menganti',
                'created_at' => '2018-07-21 14:39:00',
                'updated_at' => '2018-07-21 14:39:00',
            ),
            450 => 
            array (
                'id' => 451,
                'kecamatan_id' => 26,
                'nama' => 'Pakuran',
                'created_at' => '2018-07-22 14:39:00',
                'updated_at' => '2018-07-22 14:39:00',
            ),
            451 => 
            array (
                'id' => 452,
                'kecamatan_id' => 26,
                'nama' => 'Pandansari',
                'created_at' => '2018-07-23 14:39:00',
                'updated_at' => '2018-07-23 14:39:00',
            ),
            452 => 
            array (
                'id' => 453,
                'kecamatan_id' => 26,
                'nama' => 'Pengempon',
                'created_at' => '2018-07-24 14:39:00',
                'updated_at' => '2018-07-24 14:39:00',
            ),
            453 => 
            array (
                'id' => 454,
                'kecamatan_id' => 26,
                'nama' => 'Penusupan',
                'created_at' => '2018-07-25 14:39:00',
                'updated_at' => '2018-07-25 14:39:00',
            ),
            454 => 
            array (
                'id' => 455,
                'kecamatan_id' => 26,
                'nama' => 'Purwodeso',
                'created_at' => '2018-07-26 14:39:00',
                'updated_at' => '2018-07-26 14:39:00',
            ),
            455 => 
            array (
                'id' => 456,
                'kecamatan_id' => 26,
                'nama' => 'Sidoagung',
                'created_at' => '2018-07-27 14:39:00',
                'updated_at' => '2018-07-27 14:39:00',
            ),
            456 => 
            array (
                'id' => 457,
                'kecamatan_id' => 26,
                'nama' => 'Sidoharjo',
                'created_at' => '2018-07-28 14:39:00',
                'updated_at' => '2018-07-28 14:39:00',
            ),
            457 => 
            array (
                'id' => 458,
                'kecamatan_id' => 26,
                'nama' => 'Sruweng',
                'created_at' => '2018-07-29 14:39:00',
                'updated_at' => '2018-07-29 14:39:00',
            ),
            458 => 
            array (
                'id' => 459,
                'kecamatan_id' => 26,
                'nama' => 'Tanggeran',
                'created_at' => '2018-07-30 14:39:00',
                'updated_at' => '2018-07-30 14:39:00',
            ),
            459 => 
            array (
                'id' => 460,
                'kecamatan_id' => 26,
                'nama' => 'Trikarso',
                'created_at' => '2018-07-31 14:39:00',
                'updated_at' => '2018-07-31 14:39:00',
            ),
        ));
        
        
    }
}