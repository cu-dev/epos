<?php

use Illuminate\Database\Seeder;

class PeriodeOrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('periode_orders')->delete();
        
        \DB::table('periode_orders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'bulan_down' => 0,
                'jumlah_down' => 0,
                'bulan_up' => 0,
                'jumlah_up' => 0,
                'user_id' => 1,
                'created_at' => '2017-10-16 11:16:28',
                'updated_at' => '2017-10-16 11:16:29',
            ),
        ));
        
        
    }
}