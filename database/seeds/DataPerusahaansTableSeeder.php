<?php

use Illuminate\Database\Seeder;

class DataPerusahaansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_perusahaans')->delete();
        
        \DB::table('data_perusahaans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'KENCANA MULYA',
                'alamat' => 'JL. PURING-PETANAHAN NO. 3 KARANGDUWUR',
                'email' => 'kencanamulyakarangduwur@gmail.com',
            'telepon' => '(0287) 66 55 492',
            'wa_perusahaan' => '0878 6 33 000 22 (XL)',
            'wa_penjualan' => '0878 6 33 000 11 (XL)',
                'npwp' => NULL,
                'tanggal_npwp' => '0000-00-00',
                'pkp' => NULL,
                'tanggal_pkp' => '0000-00-00',
                'logo' => '1519304815.png',
                'created_at' => '2018-02-22 19:33:52',
                'updated_at' => '2018-03-30 16:05:51',
            ),
        ));
        
        
    }
}