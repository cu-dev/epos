<?php

use Illuminate\Database\Seeder;

class KecamatansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kecamatans')->delete();
        
        \DB::table('kecamatans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kabupaten_id' => 1,
                'nama' => 'Adimulyo',
                'created_at' => '2017-04-28 14:20:09',
                'updated_at' => '2017-04-28 14:20:09',
            ),
            1 => 
            array (
                'id' => 2,
                'kabupaten_id' => 1,
                'nama' => 'Alian/Aliyan',
                'created_at' => '2017-04-29 14:20:09',
                'updated_at' => '2017-04-29 14:20:09',
            ),
            2 => 
            array (
                'id' => 3,
                'kabupaten_id' => 1,
                'nama' => 'Ambal',
                'created_at' => '2017-04-30 14:20:09',
                'updated_at' => '2017-04-30 14:20:09',
            ),
            3 => 
            array (
                'id' => 4,
                'kabupaten_id' => 1,
                'nama' => 'Ayah',
                'created_at' => '2017-05-01 14:20:09',
                'updated_at' => '2017-05-01 14:20:09',
            ),
            4 => 
            array (
                'id' => 5,
                'kabupaten_id' => 1,
                'nama' => 'Bonorowo',
                'created_at' => '2017-05-02 14:20:09',
                'updated_at' => '2017-05-02 14:20:09',
            ),
            5 => 
            array (
                'id' => 6,
                'kabupaten_id' => 1,
                'nama' => 'Buayan',
                'created_at' => '2017-05-03 14:20:09',
                'updated_at' => '2017-05-03 14:20:09',
            ),
            6 => 
            array (
                'id' => 7,
                'kabupaten_id' => 1,
                'nama' => 'Buluspesantren',
                'created_at' => '2017-05-04 14:20:09',
                'updated_at' => '2017-05-04 14:20:09',
            ),
            7 => 
            array (
                'id' => 8,
                'kabupaten_id' => 1,
                'nama' => 'Gombong',
                'created_at' => '2017-05-05 14:20:09',
                'updated_at' => '2017-05-05 14:20:09',
            ),
            8 => 
            array (
                'id' => 9,
                'kabupaten_id' => 1,
                'nama' => 'Karanganyar',
                'created_at' => '2017-05-06 14:20:09',
                'updated_at' => '2017-05-06 14:20:09',
            ),
            9 => 
            array (
                'id' => 10,
                'kabupaten_id' => 1,
                'nama' => 'Karanggayam',
                'created_at' => '2017-05-07 14:20:09',
                'updated_at' => '2017-05-07 14:20:09',
            ),
            10 => 
            array (
                'id' => 11,
                'kabupaten_id' => 1,
                'nama' => 'Karangsambung',
                'created_at' => '2017-05-08 14:20:09',
                'updated_at' => '2017-05-08 14:20:09',
            ),
            11 => 
            array (
                'id' => 12,
                'kabupaten_id' => 1,
                'nama' => 'Kebumen',
                'created_at' => '2017-05-09 14:20:09',
                'updated_at' => '2017-05-09 14:20:09',
            ),
            12 => 
            array (
                'id' => 13,
                'kabupaten_id' => 1,
                'nama' => 'Klirong',
                'created_at' => '2017-05-10 14:20:09',
                'updated_at' => '2017-05-10 14:20:09',
            ),
            13 => 
            array (
                'id' => 14,
                'kabupaten_id' => 1,
                'nama' => 'Kutowinangun',
                'created_at' => '2017-05-11 14:20:09',
                'updated_at' => '2017-05-11 14:20:09',
            ),
            14 => 
            array (
                'id' => 15,
                'kabupaten_id' => 1,
                'nama' => 'Kuwarasan',
                'created_at' => '2017-05-12 14:20:09',
                'updated_at' => '2017-05-12 14:20:09',
            ),
            15 => 
            array (
                'id' => 16,
                'kabupaten_id' => 1,
                'nama' => 'Mirit',
                'created_at' => '2017-05-13 14:20:09',
                'updated_at' => '2017-05-13 14:20:09',
            ),
            16 => 
            array (
                'id' => 17,
                'kabupaten_id' => 1,
                'nama' => 'Padureso',
                'created_at' => '2017-05-14 14:20:09',
                'updated_at' => '2017-05-14 14:20:09',
            ),
            17 => 
            array (
                'id' => 18,
                'kabupaten_id' => 1,
                'nama' => 'Pejagoan',
                'created_at' => '2017-05-15 14:20:09',
                'updated_at' => '2017-05-15 14:20:09',
            ),
            18 => 
            array (
                'id' => 19,
                'kabupaten_id' => 1,
                'nama' => 'Petanahan',
                'created_at' => '2017-05-16 14:20:09',
                'updated_at' => '2017-05-16 14:20:09',
            ),
            19 => 
            array (
                'id' => 20,
                'kabupaten_id' => 1,
                'nama' => 'Poncowarno',
                'created_at' => '2017-05-17 14:20:09',
                'updated_at' => '2017-05-17 14:20:09',
            ),
            20 => 
            array (
                'id' => 21,
                'kabupaten_id' => 1,
                'nama' => 'Prembun',
                'created_at' => '2017-05-18 14:20:09',
                'updated_at' => '2017-05-18 14:20:09',
            ),
            21 => 
            array (
                'id' => 22,
                'kabupaten_id' => 1,
                'nama' => 'Puring',
                'created_at' => '2017-05-19 14:20:09',
                'updated_at' => '2017-05-19 14:20:09',
            ),
            22 => 
            array (
                'id' => 23,
                'kabupaten_id' => 1,
                'nama' => 'Rowokerle',
                'created_at' => '2017-05-20 14:20:09',
                'updated_at' => '2017-05-20 14:20:09',
            ),
            23 => 
            array (
                'id' => 24,
                'kabupaten_id' => 1,
                'nama' => 'Sadang',
                'created_at' => '2017-05-21 14:20:09',
                'updated_at' => '2017-05-21 14:20:09',
            ),
            24 => 
            array (
                'id' => 25,
                'kabupaten_id' => 1,
                'nama' => 'Sempor',
                'created_at' => '2017-05-22 14:20:09',
                'updated_at' => '2017-05-22 14:20:09',
            ),
            25 => 
            array (
                'id' => 26,
                'kabupaten_id' => 1,
                'nama' => 'Sruweng',
                'created_at' => '2017-05-23 14:20:09',
                'updated_at' => '2017-05-23 14:20:09',
            ),
        ));
        
        
    }
}