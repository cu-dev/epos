<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return vpopmail_del_domain(domain)
     */
    public function run()
    {
        $this->call(LevelsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(JenisItemsTableSeeder::class);
        $this->call(SatuansTableSeeder::class);
        // $this->call(BonusesTableSeeder::class);
        $this->call(SupliersTableSeeder::class);
        // $this->call(SellersTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        // $this->call(ItemMasuksTableSeeder::class);
        // $this->call(ItemKeluarsTableSeeder::class);
        $this->call(HargasTableSeeder::class);
        $this->call(RelasiSatuansTableSeeder::class);
        // $this->call(BanksTableSeeder::class);

        $this->call(TransaksiPembeliansTableSeeder::class);
        $this->call(RelasiTransaksiPembeliansTableSeeder::class);
        $this->call(StoksTableSeeder::class);

        // $this->call(LogTransferBanksTableSeeder::class);
        // $this->call(StockOfNumsTableSeeder::class);
        // $this->call(RelasiStockOfNumsTableSeeder::class);
        $this->call(DataPerusahaansTableSeeder::class);
        $this->call(ProvinsisTableSeeder::class);
        $this->call(KabupatensTableSeeder::class);
        $this->call(KecamatansTableSeeder::class);
        $this->call(DesasTableSeeder::class);
        // $this->call(KreditsTableSeeder::class);
        // $this->call(PelanggansTableSeeder::class);
        // $this->call(TransaksiPenjualansTableSeeder::class);
        // $this->call(RelasiTransaksiPenjualansTableSeeder::class);
        $this->call(MoneyLimitsTableSeeder::class);
        $this->call(CashDrawersTableSeeder::class);
        // $this->call(SetoranKasirsTableSeeder::class);
        // $this->call(HutangBanksTableSeeder::class);
        // $this->call(HutangBanksTableSeeder::class);
        // $this->call(BayarHutangBanksTableSeeder::class);
        // $this->call(PiutangLainsTableSeeder::class);
        // $this->call(BayarPiutangLainsTableSeeder::class);
        // $this->call(PiutangDagangsTableSeeder::class);
        $this->call(AkunsTableSeeder::class);
        $this->call(JurnalsTableSeeder::class);
        // // $this->call(CeksTableSeeder::class);
        // // $this->call(BGsTableSeeder::class);
        $this->call(PerkapsTableSeeder::class);
        // $this->call(KendaraansTableSeeder::class);
        $this->call(BangunansTableSeeder::class);
        $this->call(SaldosTableSeeder::class);
        $this->call(PeriodeOrdersTableSeeder::class);
        // $this->call(RelasiBonusesTableSeeder::class);
        // $this->call(RelasiBonusPenjualansTableSeeder::class);
        // $this->call(RelasiStokPenjualansTableSeeder::class);
        $this->call(CatatansTableSeeder::class);
        // // $this->call(RelasiBundlesTableSeeder::class);
        $this->call(LacisTableSeeder::class);
        

        $this->call(LogLacisTableSeeder::class);

        // // $this->call(Item1sTableSeeder::class);
        // // $this->call(Item2sTableSeeder::class);
        // // $this->call(Item3sTableSeeder::class);
        // // $this->call(Item4sTableSeeder::class);
        // // $this->call(Item5sTableSeeder::class);
        // // $this->call(BonusRulesTableSeeder::class);
        // // $this->call(RelasiBonusRulesTableSeeder::class);

        $this->call(ReturRulesTableSeeder::class);
        $this->call(ArusesTableSeeder::class);
        $this->call(BukaTahunsTableSeeder::class);
        $this->call(ModalsTableSeeder::class);

        $this->call(NoticesTableSeeder::class);
        $this->call(PersenPajaksTableSeeder::class);
        $this->call(BayarPiutangDagangsTableSeeder::class);
        $this->call(BgsTableSeeder::class);
        $this->call(CetakNotasTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(HistoryJualBundlesTableSeeder::class);
        $this->call(HutangsTableSeeder::class);
        $this->call(JurnalPenutupsTableSeeder::class);
        $this->call(LogBebansTableSeeder::class);
        $this->call(MigrationsTableSeeder::class);
        $this->call(ParalabasTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PembayaranReturPenjualansTableSeeder::class);
        $this->call(PenyesuaianPersediaansTableSeeder::class);
        $this->call(PersediaanAwalsTableSeeder::class);
        $this->call(RelasiBonusRulesPenjualansTableSeeder::class);
        $this->call(RelasiPenyesuaianPersediaansTableSeeder::class);
        $this->call(RelasiReturPembeliansTableSeeder::class);
        $this->call(RelasiReturPenjualansTableSeeder::class);
        $this->call(RelasiStokReturPenjualansTableSeeder::class);
        $this->call(ReturPembeliansTableSeeder::class);
        $this->call(ReturPenjualansTableSeeder::class);
        $this->call(SetoranBukasTableSeeder::class);
        // $this->call(Item6sTableSeeder::class);
        // $this->call(Item7sTableSeeder::class);
        // $this->call(Item8sTableSeeder::class);
    }
}
