<?php

use Illuminate\Database\Seeder;

class SupliersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('supliers')->delete();
        
        \DB::table('supliers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'KENCANA MULYA',
                'telepon' => '02873873010',
                'email' => NULL,
                'alamat' => 'JL. LAUT NO. 2 PETANAHAN',
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:50',
                'updated_at' => '2018-03-30 17:18:56',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'CV. BRYAN SENTOSA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:50',
                'updated_at' => '2018-01-30 15:30:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'CV. DIAN PRIMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nama' => 'CV. DUTA AROMATINDO PERSADA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-03-30 15:24:20',
            ),
            4 => 
            array (
                'id' => 5,
                'nama' => 'CV. HASTA KARYA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nama' => 'CV. SRIKANDI MAKMUR SEJAHTERA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nama' => 'CV. SURYA MAJU LESTARI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            7 => 
            array (
                'id' => 8,
                'nama' => 'CV. SURYA MANDIRI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            8 => 
            array (
                'id' => 9,
                'nama' => 'CV. SURYA MANDIRI KEBUMEN',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            9 => 
            array (
                'id' => 10,
                'nama' => 'ENGGAL WARAS',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            10 => 
            array (
                'id' => 11,
                'nama' => 'GUYUB RUKUN',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            11 => 
            array (
                'id' => 12,
                'nama' => 'IDOLA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            12 => 
            array (
                'id' => 13,
                'nama' => 'INTAN',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            13 => 
            array (
                'id' => 14,
                'nama' => 'KARSA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            14 => 
            array (
                'id' => 15,
                'nama' => 'KUMALA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            15 => 
            array (
                'id' => 16,
                'nama' => 'MANDALA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            16 => 
            array (
                'id' => 17,
                'nama' => 'NGGAL WARAS',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:24:51',
            ),
            17 => 
            array (
                'id' => 18,
                'nama' => 'PT. COCA-COLA DISTRIBUSI INDONESIA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            18 => 
            array (
                'id' => 19,
                'nama' => 'PT. BENTOEL DISTRIBUSI UTAMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            19 => 
            array (
                'id' => 20,
                'nama' => 'PT. BERLICO MULIA FARMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:51',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            20 => 
            array (
                'id' => 21,
                'nama' => 'PT. BINA SAN PRIMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            21 => 
            array (
                'id' => 22,
                'nama' => 'PT. BINTANG SIDORAYA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            22 => 
            array (
                'id' => 23,
                'nama' => 'PT. BROWITA CITRA PRIMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            23 => 
            array (
                'id' => 24,
                'nama' => 'PT. CIPTA NIAGA SEMESTA KEBUMEN',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            24 => 
            array (
                'id' => 25,
                'nama' => 'PT. FASTRATA BUANA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            25 => 
            array (
                'id' => 26,
                'nama' => 'PT. GAWIH JAYA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            26 => 
            array (
                'id' => 27,
                'nama' => 'PT. HM SAMPOERNA tbk',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            27 => 
            array (
                'id' => 28,
                'nama' => 'PT. INDOMARCO ADI PRIMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            28 => 
            array (
                'id' => 29,
                'nama' => 'PT. KARYA BARU PERKASA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            29 => 
            array (
                'id' => 30,
                'nama' => 'PT. LANCAR JAYA ADHITAMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:01',
            ),
            30 => 
            array (
                'id' => 31,
                'nama' => 'PT. MITRA ABADI BAHARI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            31 => 
            array (
                'id' => 32,
                'nama' => 'PT. NIAGA NUSA ABADI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            32 => 
            array (
                'id' => 33,
                'nama' => 'PT. NIAGATAMA RAHARJA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            33 => 
            array (
                'id' => 34,
                'nama' => 'PT. PING LOKA DISTRI NIAGA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            34 => 
            array (
                'id' => 35,
                'nama' => 'PT. SARANA BOGATAMA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            35 => 
            array (
                'id' => 36,
                'nama' => 'PT. SATRIA SAKTI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:52',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            36 => 
            array (
                'id' => 37,
                'nama' => 'PT. SINAR SOSRO',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            37 => 
            array (
                'id' => 38,
                'nama' => 'PT. SUKANDA DJAYA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            38 => 
            array (
                'id' => 39,
                'nama' => 'PT. SUMBER CIPTA MULTINIAGA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            39 => 
            array (
                'id' => 40,
                'nama' => 'PT. SURYA MADISTRINDO MAGELANG',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            40 => 
            array (
                'id' => 41,
                'nama' => 'PT. TIGARAKSA SATRIA tbk',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            41 => 
            array (
                'id' => 42,
                'nama' => 'PT. UNIRAMA DUTA NIAGA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            42 => 
            array (
                'id' => 43,
                'nama' => 'PT. YAKULT INDONESIA PERSADA',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:30:02',
            ),
            43 => 
            array (
                'id' => 44,
                'nama' => 'SEHATI KARANGANYAR KEBUMEN',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:24:53',
            ),
            44 => 
            array (
                'id' => 45,
                'nama' => 'TOKO JEMPOL',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:24:53',
            ),
            45 => 
            array (
                'id' => 46,
                'nama' => 'TOKO LANGGENG',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:24:53',
            ),
            46 => 
            array (
                'id' => 47,
                'nama' => 'TOKO PERMADI',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-01-30 15:24:53',
            ),
            47 => 
            array (
                'id' => 48,
                'nama' => 'ALTIS',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-04-29 14:31:39',
            ),
            48 => 
            array (
                'id' => 999,
                'nama' => 'NOT SUPLIER',
                'telepon' => NULL,
                'email' => NULL,
                'alamat' => NULL,
                'piutang' => NULL,
                'aktif' => 1,
                'created_at' => '2018-01-30 15:24:53',
                'updated_at' => '2018-04-29 14:31:36',
            ),
        ));
        
        
    }
}