<?php

use Illuminate\Database\Seeder;

class CashDrawersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cash_drawers')->delete();
        
        \DB::table('cash_drawers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 3,
                'nominal' => 0,
                'created_at' => '2018-03-01 01:23:04',
                'updated_at' => '2018-03-01 01:23:04',
            ),
        ));
        
        
    }
}