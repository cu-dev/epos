<?php

use Illuminate\Database\Seeder;

class LacisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lacis')->delete();
        
        \DB::table('lacis')->insert(array (
            0 => 
            array (
                'id' => 1,
                'grosir' => '0.000',
                'gudang' => '94435000.000',
                'owner' => '14435000.000',
                'created_at' => '2017-09-05 00:00:00',
                'updated_at' => '2018-03-27 15:31:21',
            ),
        ));
        
        
    }
}