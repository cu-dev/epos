<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stoks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->string('item_kode');
            $table->integer('transaksi_pembelian_id')->unsigned();
            $table->integer('retur_pembelian_id')->unsigned()->nullable();
            $table->decimal('harga');
            $table->integer('jumlah');
            $table->integer('jumlah_beli');
            $table->boolean('aktif')->default(1);
            $table->boolean('rusak')->default(0);
            $table->boolean('retur')->default(0);
            $table->string('keterangan')->nullable();
            $table->date('kadaluarsa')->nullable();
            $table->timestamps();

            $table->index('item_kode');

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('item_kode')->references('kode')->on('items');
            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians');
            $table->foreign('retur_pembelian_id')->references('id')->on('retur_pembelians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stoks');
    }
}
