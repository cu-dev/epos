<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiBonusPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_bonus_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bonus_id')->unsigned();
            $table->integer('relasi_transaksi_penjualan_id')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();

            $table->foreign('bonus_id')->references('id')->on('items');
            $table->foreign('relasi_transaksi_penjualan_id')->references('id')->on('relasi_transaksi_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_bonus_penjualans');
    }
}
