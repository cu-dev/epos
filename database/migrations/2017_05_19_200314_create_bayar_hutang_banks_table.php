<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayarHutangBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayar_hutang_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->integer('hutang_bank_id')->unsigned();
            $table->decimal('nominal');
            $table->enum('metode_pembayaran', ['tunai', 'debit', 'cek', 'bg']);
            $table->string('no_transaksi')->nullable();
            $table->timestamps();

            $table->foreign('hutang_bank_id')->references('id')->on('hutang_banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayar_hutang_banks');
    }
}
