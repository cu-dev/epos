<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiTransaksiPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_transaksi_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaksi_penjualan_id')->unsigned();
            $table->string('item_kode');
            $table->integer('jumlah');
            // $table->integer('satuan_id')->unsigned();
            // $table->integer('jumlah_bonus')->nullable();
            $table->decimal('harga')->default(0);
            $table->decimal('subtotal')->default(0);
            $table->decimal('nego')->default(0);
            $table->timestamps();

            $table->index('item_kode');

            $table->foreign('item_kode')->references('kode')->on('items');
            // $table->foreign('satuan_id')->references('id')->on('satuans');
            $table->foreign('transaksi_penjualan_id')->references('id')->on('transaksi_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_transaksi_penjualans');
    }
}
