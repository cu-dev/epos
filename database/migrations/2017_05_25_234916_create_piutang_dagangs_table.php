<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiutangDagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piutang_dagangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->integer('transaksi_penjualan_id')->unsigned();
            $table->decimal('nominal');
            $table->decimal('sisa');
            // $table->date('jatuh_tempo')->nullable();
            $table->timestamps();

            $table->foreign('transaksi_penjualan_id')->references('id')->on('transaksi_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piutang_dagangs');
    }
}
