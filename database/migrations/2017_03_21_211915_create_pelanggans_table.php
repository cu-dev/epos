<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_identitas');
            $table->string('ktp')->unique()->nullable();
            $table->string('nama')->nullable();
            $table->string('toko')->nullable();
            $table->string('alamat')->nullable();
            $table->integer('desa_id')->unsigned()->nullable();
            $table->string('telepon')->unique()->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('no_rekening')->nullable();
            $table->string('atas_nama')->nullable();
            $table->string('bank')->nullable();
            $table->enum('level', ['eceran', 'grosir'])->nullable();
            $table->timestamp('time_change')->nullable();
            $table->boolean('aktif')->default(1);
            $table->decimal('diskon_persen')->nullable();
            $table->decimal('potongan')->nullable();
            $table->decimal('titipan')->nullable();
            $table->decimal('limit_jumlah_piutang')->nullable();
            $table->integer('jatuh_tempo')->default(7);
            $table->integer('user_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('desa_id')->references('id')->on('desas');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *w
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggans');
    }
}
