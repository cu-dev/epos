<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiStokPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_stok_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stok_id')->unsigned();
            $table->integer('relasi_transaksi_penjualan_id')->unsigned();
            $table->integer('jumlah');
            $table->decimal('hpp')->default(0);
            $table->timestamps();

            $table->foreign('stok_id')->references('id')->on('stoks');
            $table->foreign('relasi_transaksi_penjualan_id')->references('id')->on('relasi_transaksi_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_stok_penjualans');
    }
}
