<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->decimal('nominal')->default(0);
            $table->integer('kelipatan')->default(0);
            $table->decimal('profit')->default(0);
            $table->decimal('jumlah')->default(0);
            $table->integer('status')->default(0);
            $table->integer('aktif')->default(1);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_rules');
    }
}
