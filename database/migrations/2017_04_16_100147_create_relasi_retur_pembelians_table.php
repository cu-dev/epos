<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiReturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_retur_pembelians', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retur_pembelian_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('retur');
            $table->integer('jumlah');
            $table->decimal('harga')->nullable();            
            $table->decimal('subtotal')->nullable();            
            $table->string('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('retur_pembelian_id')->references('id')->on('retur_pembelians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_retur_pembelians');
    }
}
