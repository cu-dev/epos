<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiPenyesuaianPersediaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_penyesuaian_persediaans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penyesuaian_persediaan_id')->unsigned();
            $table->string('item_kode');
            $table->integer('jumlah');
            $table->decimal('harga')->nullable();
            $table->decimal('subtotal')->nullable();
            $table->timestamps();

            $table->index('item_kode');

            $table->foreign('item_kode')->references('kode')->on('items');
            $table->foreign('penyesuaian_persediaan_id')->references('id')->on('penyesuaian_persediaans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_penyesuaian_persediaans');
    }
}
