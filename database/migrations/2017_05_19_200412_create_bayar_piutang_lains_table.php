<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayarPiutangLainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayar_piutang_lains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->integer('piutang_lain_id')->unsigned();
            $table->decimal('nominal');
            $table->enum('metode_pembayaran', ['tunai', 'debit', 'cek', 'bg']);
            $table->string('no_transfer')->nullable();
            $table->timestamps();

            $table->foreign('piutang_lain_id')->references('id')->on('piutang_lains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayar_piutang_lains');
    }
}
