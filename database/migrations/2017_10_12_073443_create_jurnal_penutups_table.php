<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalPenutupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_penutups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_akun')->unsigned()->index();
            $table->string('referensi')->nullable();
            $table->decimal('debet')->nullable();
            $table->decimal('kredit')->nullable();
            $table->timestamps();

            $table->foreign('kode_akun')->references('kode')->on('akuns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnal_penutups');
    }
}
