<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_barang')->nullable();
            $table->string('kode')->nullable();
            $table->string('nama_pendek');
            $table->string('nama');
            $table->integer('suplier_id')->unsigned()->nullable();
            $table->integer('jenis_item_id')->unsigned();
            $table->integer('stoktotal')->nullable()->default(0);
            $table->boolean('konsinyasi')->default(0);
            $table->boolean('aktif')->default(1);
            $table->boolean('retur')->default(1);
            $table->boolean('bonus')->default(0);
            $table->boolean('valid_konversi')->default(0);
            // $table->integer('bonus_id')->unsigned()->nullable();
            // $table->integer('syarat_bonus')->nullable();
            // $table->integer('satuan_bonus')->nullable();
            $table->integer('bonus_jual')->nullable()->default(0);
            $table->integer('limit_stok')->nullable()->default(0);
            $table->integer('limit_grosir')->nullable()->default(0);
            // $table->integer('satuan_limit')->nullable();
            $table->decimal('diskon')->nullable()->default(0);
            $table->decimal('profit')->nullable()->default(0.5);
            $table->decimal('profit_eceran')->nullable()->default(1);
            $table->decimal('rentang')->nullable()->default(500);
            $table->decimal('plus_grosir')->nullable()->default(100);
            // $table->integer('bundle')->nullable()->default(0);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('valid_id')->unsigned()->nullable();
            $table->timestamps();

            $table->index('kode');

            $table->foreign('suplier_id')->references('id')->on('supliers');
            $table->foreign('jenis_item_id')->references('id')->on('jenis_items');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('valid_id')->references('id')->on('users');
            // $table->foreign('bonus_id')->references('id')->on('bonuses');
            // $table->foreign('satuan_bonus')->references('id')->on('satuans');
            // $table->foreign('satuan_limit')->references('id')->on('satuans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
