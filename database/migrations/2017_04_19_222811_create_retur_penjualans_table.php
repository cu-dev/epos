<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_retur');
            $table->integer('transaksi_penjualan_id')->unsigned();
            $table->string('status');
            $table->decimal('harga_total');
            $table->decimal('total_uang_masuk')->nullable();
            $table->decimal('total_uang_keluar')->nullable();
            $table->integer('pelanggan_id')->unsigned()->nullable();
            $table->integer('cetak')->default(0);
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('transaksi_penjualan_id')->references('id')->on('transaksi_penjualans');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pelanggan_id')->references('id')->on('pelanggans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_penjualans');
    }
}
