<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_parent');
            $table->string('item_child');
            $table->integer('jumlah');
            $table->timestamps();

            $table->index('item_parent');
            $table->index('item_child');

            $table->foreign('item_parent')->references('kode')->on('items');
            $table->foreign('item_child')->references('kode')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_bundles');
    }
}
