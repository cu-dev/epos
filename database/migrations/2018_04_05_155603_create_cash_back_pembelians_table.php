<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashBackPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_back_pembelians', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaksi_pembelian_id')->unsigned();
            $table->decimal('nominal_tunai')->nullable();
            $table->string('no_transfer')->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();
            $table->string('no_kartu')->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_back_pembelians');
    }
}
