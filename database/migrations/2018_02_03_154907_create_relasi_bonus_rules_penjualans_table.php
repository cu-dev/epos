<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiBonusRulesPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_bonus_rules_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaksi_penjualan_id')->unsigned();
            $table->integer('bonus_id')->unsigned();
            $table->integer('jumlah')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('transaksi_penjualan_id')->references('id')->on('transaksi_penjualans');
            $table->foreign('bonus_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_bonus_rules_penjualans');
    }
}
