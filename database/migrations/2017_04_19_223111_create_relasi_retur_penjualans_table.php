<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiReturPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_retur_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retur_penjualan_id')->unsigned();
            // $table->integer('item_id')->unsigned();
            $table->string('item_kode');
            $table->integer('jumlah');
            $table->decimal('harga')->nullable();
            $table->decimal('subtotal')->nullable();
            $table->integer('retur')->default(0);
            $table->timestamps();

            $table->index('item_kode');

            // $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('item_kode')->references('kode')->on('items');
            $table->foreign('retur_penjualan_id')->references('id')->on('retur_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_retur_penjualans');
    }
}
