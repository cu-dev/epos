<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiBonusRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_bonus_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bonus_rules_id')->unsigned();
            $table->integer('bonus_id')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();

            $table->foreign('bonus_rules_id')->references('id')->on('bonus_rules');
            $table->foreign('bonus_id')->references('id')->on('items');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_bonus_rules');
    }
}
