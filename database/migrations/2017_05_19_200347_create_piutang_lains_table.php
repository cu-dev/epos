<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiutangLainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piutang_lains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->string('nama');
            $table->decimal('nominal');
            $table->integer('karyawan');
            $table->integer('user_id')->unsigned()->nullable();
            $table->decimal('sisa');
            $table->decimal('PT')->nullable();
            $table->string('keterangan')->nullable();;
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piutang_lains');
    }
}
