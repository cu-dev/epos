<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiTransaksiPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_transaksi_pembelians', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaksi_pembelian_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('jumlah');
            $table->decimal('harga')->nullable();
            $table->decimal('subtotal')->nullable();

            $table->timestamps();

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_transaksi_pembelians');
    }
}
