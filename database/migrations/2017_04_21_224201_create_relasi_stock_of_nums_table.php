<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiStockOfNumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_stock_of_nums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_of_num_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->integer('stok_sistem');
            $table->integer('stok_fisik');
            $table->string('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('stock_of_num_id')->references('id')->on('stock_of_nums');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_stock_of_nums');
    }
}
