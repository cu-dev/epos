<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryJualBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_jual_bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('relasi_transaksi_penjualan_id')->unsigned();
            $table->string('item_kode');
            $table->integer('jumlah');
            $table->decimal('harga');
            $table->timestamps();

            $table->index('item_kode');
            $table->index('relasi_transaksi_penjualan_id');

            $table->foreign('item_kode')->references('kode')->on('items');
            // $table->foreign('satuan_id')->references('id')->on('satuans');
            $table->foreign('relasi_transaksi_penjualan_id')->references('id')->on('relasi_transaksi_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_jual_bundles');
    }
}
