<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranReturPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran_retur_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retur_penjualan_id')->unsigned();
            $table->decimal('nominal_tunai')->nullable();
            $table->string('no_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->string('no_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();
            $table->string('no_cek')->nullable();
            $table->integer('bank_cek')->unsigned()->nullable();
            $table->integer('cek_lunas')->nullable();
            $table->decimal('nominal_cek')->nullable();
            $table->string('no_bg')->nullable();
            $table->integer('bank_bg')->unsigned()->nullable();
            $table->integer('bg_lunas')->nullable();
            $table->decimal('nominal_bg')->nullable();
            $table->decimal('nominal_titipan')->nullable();
            $table->decimal('nominal_piutang')->nullable();

            $table->decimal('nominal_tunai_in')->nullable();
            $table->string('no_transfer_in')->nullable();
            $table->integer('bank_transfer_in')->unsigned()->nullable();
            $table->decimal('nominal_transfer_in')->nullable();
            $table->string('no_kartu_in')->nullable();
            $table->integer('bank_kartu_in')->unsigned()->nullable();
            $table->string('jenis_kartu_in')->nullable();
            $table->decimal('nominal_kartu_in')->nullable();
            $table->string('no_cek_in')->nullable();
            $table->decimal('nominal_cek_in')->nullable();
            $table->string('no_bg_in')->nullable();
            $table->decimal('nominal_bg_in')->nullable();
            $table->decimal('nominal_titipan_in')->nullable();
            $table->timestamps();

            $table->foreign('bank_transfer')->references('id')->on('banks');
            $table->foreign('bank_cek')->references('id')->on('banks');
            $table->foreign('bank_bg')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
            $table->foreign('bank_transfer_in')->references('id')->on('banks');
            $table->foreign('bank_kartu_in')->references('id')->on('banks');
            $table->foreign('retur_penjualan_id')->references('id')->on('retur_penjualans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_retur_penjualans');
    }
}
