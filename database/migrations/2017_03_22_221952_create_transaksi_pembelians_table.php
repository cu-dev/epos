<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pembelians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->string('nota')->nullable();
            $table->integer('suplier_id')->unsigned();
            $table->integer('seller_id')->unsigned()->nullable();
            $table->boolean('po');
            $table->decimal('harga_total')->nullable();
            $table->decimal('potongan_pembelian')->nullable();
            $table->decimal('ongkos_kirim')->nullable();
            $table->decimal('jumlah_bayar')->nullable();
            $table->decimal('utang')->nullable();
            $table->decimal('sisa_utang')->nullable();
            $table->boolean('status_hutang')->nullable();

            $table->decimal('nominal_tunai')->nullable();
            
            $table->string('no_transfer')->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();

            $table->string('no_cek')->nullable();
            $table->decimal('nominal_cek')->nullable();
            $table->integer('bank_cek')->unsigned()->nullable();
            $table->integer('aktif_cek')->nullable();

            $table->string('no_bg')->nullable();
            $table->decimal('nominal_bg')->nullable();
            $table->integer('bank_bg')->unsigned()->nullable();
            $table->integer('aktif_bg')->nullable();

            $table->string('no_kartu')->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();

            $table->decimal('piutang')->nullable();

            $table->decimal('ppn_total')->nullable()->default(0);
            $table->date('jatuh_tempo')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('suplier_id')->references('id')->on('supliers');
            $table->foreign('seller_id')->references('id')->on('sellers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bank_cek')->references('id')->on('banks');
            $table->foreign('bank_bg')->references('id')->on('banks');
            $table->foreign('bank_transfer')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelians');
    }
}
