<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_pembelians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_retur');
            $table->string('nota')->nullable();
            $table->integer('transaksi_pembelian_id')->unsigned();
            $table->integer('suplier_id')->unsigned();
            $table->string('status');
            $table->decimal('harga_total')->nullable();
            $table->decimal('total_uang')->nullable();

            $table->decimal('nominal_tunai')->nullable();

            $table->string('no_transfer')->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();

            $table->string('no_cek')->nullable();
            $table->decimal('nominal_cek')->nullable();
            $table->integer('bank_cek')->unsigned()->nullable();
            $table->integer('cek_lunas')->nullable();

            $table->string('no_bg')->nullable();
            $table->decimal('nominal_bg')->nullable();
            $table->integer('bank_bg')->unsigned()->nullable();
            $table->integer('bg_lunas')->nullable();

            $table->string('no_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();

            $table->decimal('nominal_piutang')->nullable();

            $table->integer('hutang_id')->nullable();
            $table->decimal('nominal_hutang')->nullable();

            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('suplier_id')->references('id')->on('supliers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians');
            $table->foreign('bank_cek')->references('id')->on('banks');
            $table->foreign('bank_bg')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
            $table->foreign('bank_transfer')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_pembelians');
    }
}
