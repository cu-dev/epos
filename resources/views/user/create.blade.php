@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Pengguna</title>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Pengguna</h2>
				<a href="{{ URL::previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
	                <i class="fa fa-long-arrow-left"></i>
	            </a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('user') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group">
						<label class="control-label">Nama Lengkap</label>
						<input class="form-control" type="text" name="nama" required="">
					</div>
					<div class="form-group" id="form_username">
						<label class="control-label">Nama Pengguna</label>
						<input class="form-control" type="text" name="username" required="">
						<span id="username_error" style="color:red">Nama pengguna tidak boleh ada spasi dan tidak boleh lebih dari 10 karakter!</span>
					</div>
					<div class="form-group">
						<label class="control-label">Kata Sandi</label>
						<input class="form-control" type="text" name="password" placeholder="Kata sandi diproses menurut nama pengguna" disabled>
					</div>
					<div class="form-group">
						<label class="control-label">Email</label>
						<input class="form-control" type="email" name="email" required="">
					</div>
					<div class="form-group controls xdisplay_inputx has-feedback">
						<label class="control-label">Tanggal Lahir</label>
						<input type="text"  name="tanggal_" class="form-control has-feedback-left active tanggal-putih" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" required="" readonly="">
						<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
						<input class="form-control" type="hidden" name="tanggal_lahir">
					</div>
					<div class="form-group">
						<label class="control-label">Telepon</label>
						<input class="form-control" type="text" name="telepon" required="">
						<span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
					</div>
					<div class="form-group">
						<label class="control-label">Alamat</label>
						<textarea rows="4" class="form-control" name="alamat" required=""></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Tingkatan Pengguna</label>
						<select class="form-control select2_single" id="level_id" name="level_id" required="">
							<option>Pilih Tingkatan Pengguna</option>
							@foreach($levels as $level)
							<option value="{{$level->id}}">{{ $level->kode }} : {{ $level->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-save"></i> <span>Tambah</span>
						</button>
						<button class="btn btn-default" id="btnReset" type="button">
							<i class="fa fa-refresh"></i> Reset
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">

		$(document).ready(function() {
			var url = "{{ url('user') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
			$('.right_col').css('min-height', $('.left_col').css('height'));

			$(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

			// $('#single_cal3').daterangepicker({
			//   	singleDatePicker: true,
			//   	calender_style: "picker_3",
			//   	format: 'DD-MM-YYYY'
			// }, function(start, end, label) {
			//   	var date = start.toISOString().substr(0,10);
			//   	$('input[name="tanggal_lahir"]').val(date);
			// });

			$('#myDatepicker2').datetimepicker({
				format: 'DD-MM-YYYY',
			});

			$('#single_cal3').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				calender_style: "picker_2",
				format: 'DD-MM-YYYY',
				locale: {
					"applyLabel": "Pilih",
					"cancelLabel": "Batal",
					"fromLabel": "Awal",
					"toLabel": "Akhir",
					"customRangeLabel": "Custom",
					"weekLabel": "M",
					"daysOfWeek": [
						"Min",
						"Sen",
						"Sel",
						"Rab",
						"Kam",
						"Jum",
						"Sab"
					],
					"monthNames": [
						"Januari",
						"Februari",
						"Maret",
						"April",
						"Mei",
						"Juni",
						"Juli",
						"Agustus",
						"September",
						"Oktober",
						"November",
						"Desember"
					],
					"firstDay": 1
				},
			}, 
			function(start, end, label) {
				// console.log(start, end, label);
				var awal = (start.toISOString()).substring(0,10);
				$('input[name="tanggal_lahir"]').val(awal);
				// console.log(awal);
			});

            $('#username_error').hide();
		});

		$(document).on('click', '#btnReset', function() {
			$('input[name="nama"]').val('');
			$('input[name="username"]').val('');
			$('input[name="password"]').val('');
			$('input[name="email"]').val('');
			$('input[name="tanggal_lahir"]').val('');
			$('input[name="telepon"]').val('');
			$('input[name="alamat"]').val('');
			//$("#level_id").removeAttr("selected");
			$("#foto").val('');
			$('input[name="username"]').trigger('keyup');
			$('#formSimpanContainer').find('form').attr('action', '{{ url("user") }}');
			$('#formSimpanContainer').find('input[name="_method"]').val('post');
			$('#btnSimpan span').text('Tambah');

			$('#formSimpanTitle').text('Tambah ser');
		});

		$(document).on('keyup', 'input[name="username"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();

            if (text.indexOf(' ') != -1 || text.length > 10) {
                $('#form_username').addClass('has-error');
                $('#username_error').show();
                // $('#btnSimpan').prop('disabled', true);
            } else {
                $('#form_username').removeClass('has-error');
                $('#username_error').hide();
                // $('#btnSimpan').prop('disabled', false);
            }

            cek();
		});

		$(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
            	ini.parents('.form-group').first().addClass('has-error');
            	ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
            	ini.next('span').addClass('sembunyi');
            }

            cek();
		});

		function cek() {
			if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
			} else {
				$('#btnSimpan').prop('disabled', false);
			}
		}
	</script>
@endsection


