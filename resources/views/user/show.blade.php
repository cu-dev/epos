@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Pengguna</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnReset, #btnKembali {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')

<div class="col-md-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title" style="padding-right: 0;">
            <h2>Detail Pengguna</h2>
            @if($user->status == 1)
                {{-- <a href="{{ url('user/edit/'.$user->id) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" >
                    <i class="fa fa-edit"></i> Ubah
                </a> --}}
                <a href="{{ url('user/edit/'.$user->id) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Pengguna">
                    <i class="fa fa-edit"></i>
                </a>
                <button class="btn btn-sm btn-default pull-right" id="btnReset" data-toggle="tooltip" data-placement="top" title="Reset Kata Sandi">
                    <i class="fa fa-undo"></i>
                </button>
            @endif
            <a href="{{ url('user')  }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                <i class="fa fa-long-arrow-left"></i>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    <div class="profile_img">
                        <div id="crop-avatar">
                        <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{URL::to('/foto_profil/'.$user->foto)}}" alt="Avatar" title="Ubah Foto Profil">
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 profile_left">
                    <table class="table table-striped">
                        <tr>
                            <td>
                                <i class="fa fa-user"></i> Nama Lengkap
                            </td>
                            <td>
                                :
                            </td>
                            <td id="nama">
                                {{ $user->nama }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-user"></i> Nama Pengguna
                            </td>
                            <td>
                                :
                            </td>
                            <td id="nama">
                                {{ $user->username }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-map-marker"></i> Alamat
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{ $user->alamat }}, Indonesia
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-phone"></i> Telepon
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{ $user->telepon }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-envelope"></i> Email
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-calendar"></i> Tanggal Lahir
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{ App\Util::date($user->tanggal_lahir) }}
                            </td>
                        </tr>
                    </table>
                    {{-- @if($user->status == 1)
                        <button class="btn btn-xs btn-primary" id="btnReset">
                            <i class="fa fa-edit"></i> Reset Sandi
                        </button>
                    @endif --}}
                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'User berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'User gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'User berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'User gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'User berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'User gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'User sandi berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'User sandi gagal direset!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $(document).on('click', '#btnReset', function() {
            var id = {{ $user->id }};
            var nama_item = "{{ $user->nama }}";
            swal({
                title: 'Reset Sandi?',
                text: 'Sandi \"' + nama_item + '\" akan di reset!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Reset!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    location.href = "{{ URL::to('/user/reset/'.$user->id) }}";
                } else {
                    // Canceled
                }
            });
        });

    </script>
@endsection
