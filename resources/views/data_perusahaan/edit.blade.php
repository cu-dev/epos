@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Data Perusahaan</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Ubah Data Perusahaan</h2>
                <a href="{{ url('data_perusahaan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('data_perusahaan') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="put">
                    <div class="form-group">
                        <label class="control-label">Nama Perusahaan</label>
                        <input class="form-control" type="text" name="nama" value="{{$data_perusahaan->nama}}" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alamat</label>
                        <textarea rows="4" class="form-control" name="alamat" required="">{{$data_perusahaan->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" type="email" name="email" value="{{$data_perusahaan->email}}" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Telepon/Fax</label>
                        <input class="form-control" type="text" name="telepon" value="{{$data_perusahaan->telepon}}" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">HP Perusahaan</label>
                        <input class="form-control" type="text" name="wa_perusahaan" value="{{$data_perusahaan->wa_perusahaan}}" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">HP Penjualan</label>
                        <input class="form-control" type="text" name="wa_penjualan" value="{{$data_perusahaan->wa_penjualan}}" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">NPWP</label>
                        <input class="form-control" type="text" name="npwp" value="{{$data_perusahaan->npwp}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tanggal SK NPWP</label>
                    </div>
                    <div class="input-group">
                        @if($data_perusahaan->tanggal_npwp == '0000-00-00')
                            <div class="input-group-addon"><input type="checkbox" name="checkNPWP" id="checkNPWP" /></div>
                            <input class="form-control" type="text" id="tanggal_npwp" name="" value="" readonly="">
                         @else
                            <div class="input-group-addon"><input type="checkbox" name="checkNPWP" id="checkNPWP" checked="" /></div>
                            <input class="form-control tanggal-putih" type="text" id="tanggal_npwp" name="" value="{{ \App\Util::date($data_perusahaan->tanggal_npwp) }}" readonly="">
                         @endif
                        <input class="form-control" type="hidden" id="tanggal_npwp_save" name="tanggal_npwp" value="{{ $data_perusahaan->tanggal_npwp }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">PKP</label>
                        <input class="form-control" type="text" name="pkp" value="{{$data_perusahaan->pkp}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tanggal SK PKP</label>
                    </div>
                    <div class="input-group">
                        @if($data_perusahaan->tanggal_pkp == '0000-00-00')
                            <div class="input-group-addon"><input type="checkbox" name="checkPKP" id="checkPKP" /></div>
                            <input class="form-control" type="text" id="tanggal_pkp" name="" value="" readonly=""> 
                        @else
                            <div class="input-group-addon"><input type="checkbox" name="checkPKP" id="checkPKP" checked="" /></div>
                            <input class="form-control tanggal-putih" type="text" id="tanggal_pkp" name="" value="{{ \App\Util::date($data_perusahaan->tanggal_pkp) }}" readonly="">
                        @endif
                        <input class="form-control" type="hidden" id="tanggal_pkp_save" name="tanggal_pkp" value="{{ $data_perusahaan->tanggal_pkp }}">
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        $(document).on('keyup', 'textarea[name="alamat"]', function(event) {
            event.preventDefault();

            var alamat = $(this).val();
            if (alamat.length > 40) {
                alamat = alamat.slice(0, -1);
                $(this).val(alamat);
            }
        });

        $(document).on('focus', '#tanggal_npwp', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            $(this).daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_2",
                showDropdowns: true,
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
            }, function(start) {
                var tanggal = start.toISOString().substring(0,10);
                console.log(tanggal);
                $('#tanggal_npwp_save').val(tanggal);
            });
        });

        $(document).on('focus', '#tanggal_pkp', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            $(this).daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_2",
                showDropdowns: true,
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
            }, function(start) {
                var tanggal = start.toISOString().substring(0,10);
                console.log(tanggal);
                $('#tanggal_pkp_save').val(tanggal);
            });
        });

        $(document).on('change', '#checkNPWP', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('#tanggal_npwp').prop('disabled', false);
                $('#tanggal_npwp').removeClass('bg-disabled');
                $('#tanggal_npwp').addClass('tanggal-putih');
            }else{
                $('#tanggal_npwp').prop('disabled', true);
                $('#tanggal_npwp').addClass('bg-disabled');
                $('#tanggal_npwp').val('');
                $('#tanggal_npwp_save').val('00-00-0000');
                $('#tanggal_npwp').removeClass('tanggal-putih');
            }
        });

        $(document).on('change', '#checkPKP', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('#tanggal_pkp').prop('disabled', false);
                $('#tanggal_pkp').removeClass('bg-disabled');
                $('#tanggal_pkp').addClass('tanggal-putih');
            }else{
                $('#tanggal_pkp').addClass('bg-disabled');
                $('#tanggal_pkp').prop('disabled', true);
                $('#tanggal_pkp').val('');
                $('#tanggal_pkp_save').val('00-00-0000');
                $('#tanggal_pkp').removeClass('tanggal-putih');
            }
        });

    </script>
@endsection



