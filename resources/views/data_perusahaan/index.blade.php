@extends('layouts.admin')

@section('title')
    <title>EPOS | Data Perusahaan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnUbah {
            margin: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Perusahaan</h2>
                <a href="{{ url('data_perusahaan/edit') }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Data Perusahaan">
                    <i class="fa fa-edit"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    <div class="profile_img">
                        <div id="crop-avatar">
                        <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{ URL::to('/images/'.$data_perusahaan->logo) }}" alt="Avatar" title="Change the avatar">
                        </div>
                    </div>
                    <form method="POST" action="{{ url('data_perusahaan/upload')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group" style="padding-top: 15px">
                            <input type="hidden" name="id" value="{{ $data_perusahaan->id }}">
                            <input type="file" name="logo">
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">
                            <i class="fa fa-upload"></i> Unggah
                        </button>
                    </form>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 profile_left">
                    <!-- <a href="{{ url('data_perusahaan/edit') }}" class="btn btn-sm btn-warning"><i class="fa fa-edit m-right-xs"></i> Ubah Data Perusahaan</a> -->
                    <table class="table table-striped">
                        <tr>
                            <td><i class="fa fa-user"></i> Nama Perusahaan</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->nama }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-map-marker"></i> Alamat</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->alamat }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-envelope"></i> Email</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->email }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-phone"></i> Telepon / Fax</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->telepon }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-phone"></i> WA Perusahaan</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->wa_perusahaan }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-phone"></i> WA Penjualan</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->wa_penjualan }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-calendar"></i> NPWP</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->npwp }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-calendar"></i> Tanggal SK NPWP</td>
                            <td>:</td>
                            <td>
                                @if($data_perusahaan->tanggal_npwp == '0000-00-00')
                                    -
                                @else
                                    {{ \App\Util::date($data_perusahaan->tanggal_npwp) }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-calendar"></i> Nomor PKP</td>
                            <td>:</td>
                            <td>{{ $data_perusahaan->pkp }}</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-calendar"></i> Tanggal SK PKP</td>
                            <td>:</td>
                            <td>
                                @if($data_perusahaan->tanggal_pkp == '0000-00-00')
                                    -
                                @else
                                    {{ \App\Util::date($data_perusahaan->tanggal_pkp) }}
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'upload')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Logo berhasil diunggah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'upload')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Logo gagal diunggah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data Perusahaan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data Perusahaan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif
@endsection
