@extends('layouts.admin')

@section('title')
    <title>EPOS | Hutang Bank</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
        #tablePiutangBank .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Hutang Bank</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('hutang_bank') }}" class="form-horizontal">
                        <div class="col-md-6 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly="" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pilih Rekening Bank Pinjaman</label>
                                <select name="bank_id" class="select2_single form-control" required="">
                                    <option value="">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control" type="text" name="keterangan">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Nominal Hutang</label>
                                <input class="form-control angka" type="text" name="nominal_" required="">
                                <input class="form-control" type="hidden" name="nominal" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Masuk ke Kas</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank" id="bank" class="select2_single form-control">
                                    <option value="">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nomor Transaksi</label>
                                <input class="form-control" type="text" name="no_transaksi" required="">
                            </div>
                                <input type="hidden" name="kas" />
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- kolom bawah -->
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Hutang Bank</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePiutangBank">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Transaksi</th>
                            <th>Nama Bank Pinjaman</th>
                            <th>Nominal Hutang</th>
                            <th>Sisa Hutang</th>
                            <th>Nomor Transaksi</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($hutang_banks as $num => $hutang_bank)
                        <tr id="{{$hutang_bank->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $hutang_bank->kode_transaksi }}</td>
                            <td>{{ $hutang_bank->bank->nama_bank }}</td>
                            <td align="right">{{ \App\Util::duit($hutang_bank->nominal) }}</td>
                            <td align="right">{{ \App\Util::duit($hutang_bank->sisa_hutang) }}</td>
                            @if($hutang_bank->keterangan!==NULL)
                                <td>{{ $hutang_bank->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                            <td>{{ $hutang_bank->no_transaksi }}</td>
                            <td class="text-center">
                                <a href="{{ url('hutang_bank/show/'.$hutang_bank->id) }}" class="btn btn-xs btn-primary" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Bayar">
                                    <i class="fa fa-money"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- kolom bawah ketiga -->
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Hutang Bank</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePiutangBank">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Transaksi</th>
                            <th>Nama Bank Pinjaman</th>
                            <th>Nominal Hutang</th>
                            <th>Nomor Transaksi</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($hutang_lunass as $num => $hutang_lunas)
                        <tr id="{{$hutang_lunas->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $hutang_lunas->kode_transaksi }}</td>
                            <td>{{ $hutang_lunas->bank->nama_bank }}</td>
                            <td align="right">{{ \App\Util::duit($hutang_lunas->nominal) }}</td>
                            @if($hutang_lunas->keterangan!==NULL)
                                <td>{{ $hutang_lunas->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                            <td>{{ $hutang_lunas->no_transaksi }}</td>
                            <td class="text-center">
                                <a href="{{ url('hutang_bank/show/'.$hutang_lunas->id) }}" class="btn btn-xs btn-success" id="btnBayar"  data-toggle="tooltip" data-placement="top" title="Riwayat Pembayaran">
                                    <i class="fa fa-money"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Hutang bank berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Hutang bank gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tablePiutangBank').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2();

            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(window).on('load', function(event) {
            var url = "{{ url('hutang_bank/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');

            $.get(url, function(data) {
                if (data.hutang_bank === null) {
                    var kode = int4digit(1);
                    var kode_transaksi = kode + '/HB/' + tanggal;
                    console.log('kode_transaksi');
                } else {
                    var kode_transaksi = data.hutang_bank.kode_transaksi;
                    var mm_transaksi = kode_transaksi.split('/')[2];
                    var yyyy_transaksi = kode_transaksi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode_transaksi = kode + '/HB/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/HB/' + tanggal_transaksi;
                    }
                }
                // console.log(kode_transaksi);
                $('input[name="kode_transaksi"]').val(kode_transaksi);
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
                $(this).find('select').prop('required', false);
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
                $(this).find('select').prop('required', true);
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                // console.log(bank);
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            }else{
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
