@extends('layouts.admin')

@section('title')
    <title>EPOS | Grafik Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnHistory, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .form-control {
            height: 34px;
        }
        #pilihRentangTanggal,
        #pilihTanggal {
            height: 38px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <input type="hidden" name="tahun">
        <input type="hidden" name="bulan">
        <input type="hidden" name="rentang_tanggal">
        <input type="hidden" name="tanggal">
        <input type="hidden" name="pelanggan">
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Pilih Tahun
                </label>
                <div class="form-group">
                    <select name="pilihTahun" id="pilihTahun" class="form-control select2_single">
                        <option value="">Pilih Tahun</option>
                        @for ($i = intval(date('Y')); $i >= 2017; $i--)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <label class="control-label">
                    Pilih Bulan
                </label>
                <div class="form-group">
                    <select name="pilihBulan" id="pilihBulan" class="form-control select2_single">
                        <option value="">Pilih Bulan</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <label class="control-label">
                    Pilih Rentang Tanggal
                </label>
                <div class="form-group">
                    <input name="pilihRentangTanggal" id="pilihRentangTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Rentang Tanggal" readonly="">
                </div>
            </div>
            <div class="col-md-2">
                <label class="control-label">
                    Pilih Tanggal
                </label>
                <div class="form-group">
                    <input name="pilihTanggal" id="pilihTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Tanggal" readonly="">
                </div>
            </div>
            <div class="col-md-4">
                <label class="control-label">
                    Pilih Pelanggan
                </label>
                <div class="form-group">
                    <select name="pilihPelanggan" id="pilihPelanggan" class="form-control select2_single">
                        <option value="">Pilih Pelanggan</option>
                        @foreach ($pelanggans as $i => $pelanggan)
                        <option value="{{ $pelanggan->id }}">{{ $pelanggan->nama }} [{{ $pelanggan->toko }}]</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Grafik Penjualan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <canvas id="lineChart"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        Chart.defaults.global.legend = {
            enabled: false
        };

        var transaksi_penjualans = '{{ $transaksi_penjualans }}';
        transaksi_penjualans = transaksi_penjualans.replace(/&quot;/g, '"');
        transaksi_penjualans = JSON.parse(transaksi_penjualans);

        $(document).ready(function() {
            $(".select2_single").select2({
            });

            var tanggal = new Date();
            var dd = tanggal.getDate();
            var mm = tanggal.getMonth() + 1;
            var yyyy = tanggal.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            } 

            if (mm < 10) {
                mm = '0' + mm
            } 

            tanggal = yyyy + '-' + mm + '-' + dd;
            $('input[name="tanggal"]').val(tanggal);
            var url = '{{ url("grafik/penjualan/tanggal") }}' + '/' + tanggal;
            $.get(url, function(data) {
                // console.log(data);
                var ctx = document.getElementById("lineChart");
                var lineChart = new Chart(ctx, {
                    type: 'line',
                        data: {
                        labels: data.labels,
                        datasets: [{
                            label: "",
                            backgroundColor: "rgba(3, 88, 106, 0.3)",
                            borderColor: "rgba(3, 88, 106, 0.70)",
                            pointBorderColor: "rgba(3, 88, 106, 0.70)",
                            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(151,187,205,1)",
                            pointBorderWidth: 1,
                            data: data.data
                        }]
                    },
                });
            });

            $('#pilihTahun').val('');
            $('#pilihBulan').val('');
            $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');
            $('#pilihPelanggan').val('');

            $('input[name="tahun"]').val('');
            $('input[name="bulan"]').val('');
            $('input[name="rentang_tanggal"]').val('');
            $('input[name="tanggal"]').val('');
            $('input[name="pelanggan"]').val('');

            $('#pilihTahun').select2();
            $('#pilihBulan').select2();
            $('#pilihPelanggan').select2();
        });

        $(document).on('change', '#pilihTahun', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');

            $('input[name="rentang_tanggal"]').val('');
            $('input[name="tanggal"]').val('');

            var pelanggan = $('input[name="pelanggan"]').val();
            var tahun = $(this).val();
            if (tahun != '') {
                $('input[name="tahun"]').val(tahun);
                var url = '{{ url("grafik/penjualan/tahun") }}' + '/' + tahun;
                if (pelanggan != '') url += '/pelanggan/' + pelanggan;
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var ctx = document.getElementById("lineChart");
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.data
                            }]
                        },
                    });

                    $('#spinner').hide();
                });
            } else {
                $('input[name="tahun"]').val('');
            }
        });

        $(document).on('change', '#pilihBulan', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');

            $('input[name="rentang_tanggal"]').val('');
            $('input[name="tanggal"]').val('');

            var pelanggan = $('input[name="pelanggan"]').val();
            var tahun = $('#pilihTahun').val();
            var bulan = $(this).val();
            if (tahun != '' && bulan != '') {
                $('input[name="tahun"]').val(tahun);
                $('input[name="bulan"]').val(bulan);
                var url = '{{ url("grafik/penjualan/tahun") }}' + '/' + tahun + '/bulan/' + bulan;
                if (pelanggan != '') url += '/pelanggan/' + pelanggan;
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var ctx = document.getElementById("lineChart");
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.data
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            } else {
                $('input[name="tahun"]').val('');
            }
        });

        $(document).on('focus', '#pilihRentangTanggal', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').daterangepicker({
                autoApply: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: false
            }, function(start, end) {
                $('#pilihTahun').val('').trigger('change');
                $('#pilihBulan').val('').trigger('change');
                $('#pilihTanggal').val('');

                $('input[name="tahun"]').val('');
                $('input[name="bulan"]').val('');
                $('input[name="tanggal"]').val('');


                // var tanggal = (start.toISOString()).substring(0,10);
                var awal = (start.toISOString()).substring(0,10);
                var akhir = (end.toISOString()).substring(0,10);
                var pelanggan = $('input[name="pelanggan"]').val();

                $('#pilihRentangTanggal').val(ymd2dmy(awal) + ' ' + ymd2dmy(akhir));
                $('input[name="rentang_tanggal"]').val(awal + '/' + akhir);
                var url = '{{ url("grafik/penjualan/rentang") }}' + '/' + awal + '/' + akhir;
                if (pelanggan != '') url += '/pelanggan/' + pelanggan;
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var ctx = document.getElementById("lineChart");
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.data
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            });
        });

        $(document).on('focus', '#pilihTanggal', function(event) {
            event.preventDefault();

            $('#pilihTanggal').daterangepicker({
                autoApply: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: true
            }, function(start) {
                $('#pilihTahun').val('').trigger('change');
                $('#pilihBulan').val('').trigger('change');
                $('#pilihRentangTanggal').val('');

                $('input[name="tahun"]').val('');
                $('input[name="bulan"]').val('');
                $('input[name="rentang_tanggal"]').val('');

                var tanggal = (start.toISOString()).substring(0,10);
                var pelanggan = $('input[name="pelanggan"]').val();
                $('#pilihTanggal').val(ymd2dmy(tanggal));
                $('input[name="tanggal"]').val(tanggal);
                var url = '{{ url("grafik/penjualan/tanggal") }}' + '/' + tanggal;
                if (pelanggan != '') url += '/pelanggan/' + pelanggan;
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var ctx = document.getElementById("lineChart");
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.data
                            }]
                        },
                    });

                    $('#spinner').hide();
                });
            });
        });

        $(document).on('change', '#pilihPelanggan', function(event) {
            event.preventDefault();

            var tahun = $('input[name="tahun"]').val();
            var bulan = $('input[name="bulan"]').val();
            var rentang_tanggal = $('input[name="rentang_tanggal"]').val();
            var tanggal = $('input[name="tanggal"]').val();
            var pelanggan = $(this).val();

            $('input[name="pelanggan"]').val(pelanggan);

            var url = '';
            if (tahun != '') {
                url = '{{ url("grafik/penjualan/tahun") }}' + '/' + tahun;
            }
            if (tahun != '' && bulan != '') {
                url = '{{ url("grafik/penjualan/tahun") }}' + '/' + tahun + '/bulan/' + bulan;
            }
            if (rentang_tanggal != '') {
                url = '{{ url("grafik/penjualan/rentang") }}' + '/' + rentang_tanggal;
            }
            if (tanggal != '') {
                url = '{{ url("grafik/penjualan/tanggal") }}' + '/' + tanggal;
            }
            url += '/pelanggan/' + pelanggan

            // console.log(url);
            if (url != '') {
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var ctx = document.getElementById("lineChart");
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.data
                            }]
                        },
                    });

                    $('#spinner').hide();
                });
            }
        });

    </script>
@endsection
