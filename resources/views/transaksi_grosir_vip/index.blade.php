@extends('layouts.admin')

@section('title')
	<title>EPOS | Transaksi Grosir</title>
@endsection

@section('style')
	<style type="text/css" media="screen">
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Rekap Transaksi Grosir</h2>
				<a href="{{ url('transaksi-grosir/create') }}" class="btn btn-sm btn-success pull-right">
					<i class="fa fa-plus"></i> Tambah
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableTransaksiGrosir">
					<thead>
						<tr>
							<th>No.</th>
							<th>Kode Transaksi</th>
							<th>Tanggal</th>
							<th>Total</th>
							<th>Operator</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transaksis as $i => $transaksi)
						<tr data-id="{{ $i++ }}">
							<td>{{ $i++ }}</td>
							<td>{{ $transaksi->kode_transaksi }}</td>
							<td>{{ $transaksi->created_at->format('d-m-Y H:i:s') }}</td>
							<td>Rp {{ number_format($transaksi->harga_total) }}</td>
							<td>{{ $transaksi->user->nama }}</td>
							<td>
								<a href="{{ url('transaksi-grosir/'.$transaksi->id) }}" class="btn btn-xs btn-info" id="btnDetail">
									<i class="fa fa-eye"></i> Detail
								</a>
								<a href="{{ url('transaksi-grosir/'.$transaksi->id.'/retur') }}" class="btn btn-xs btn-warning" id="btnDetail">
									<i class="fa fa-sign-out"></i> Retur
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Grosir berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Grosir gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Grosir berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Grosir gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Grosir berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Grosir gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script>
		$('#tableTransaksiGrosir').DataTable();

		$(document).ready(function() {
			$(".select2_single").select2({
				allowClear: true
			});
		});
		
	</script>
@endsection
