@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Transaksi Grosir {{ $transaksi_grosir->kode_transaksi }}</title>
@endsection

@section('style')
	<style type="text/css" media="screen">
        #btnCairkan {
            margin-right: 0;
        }
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Transaksi Grosir</h2>
				<a href="{{ url('transaksi/'.$transaksi_grosir->id.'/retur/create') }}" class="btn btn-sm btn-warning pull-right">
					<i class="fa fa-sign-in"></i> Retur
				</a>
				<a href="{{ url('transaksi-grosir/create') }}" class="btn btn-sm btn-primary pull-right">
					<i class="fa fa-plus"></i> Tambah
				</a>
				<a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<div class="x_title">
							<h2>{{ $transaksi_grosir->created_at->format('d-m-Y H:i:s') }}</h2>
							<div class="clearfix"></div>
						</div>
						<table class="table">
							<tbody>
								<tr>
									<th style="border-top: none;">Kode Transaksi</th>
									<td style="border-top: none;">{{ $transaksi_grosir->kode_transaksi }}</td>
								</tr>
								<tr>
									<th>Total Harga</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->harga_total) }}</td>
								</tr>
								<tr>
									<th>Total Bayar</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->jumlah_bayar) }}</td>
								</tr>
								<tr>
									<th>Nominal Tunai</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->nominal_tunai) }}</td>
								</tr>
								<tr>
									<th>No. Debit</th>
									<td>
										@if (!empty($transaksi_grosir->no_debit))
										{{ $transaksi_grosir->no_debit }}
										@else
										-
										@endif
									</td>
								</tr>
								<tr>
									<th>Nominal Debit</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->nominal_debit) }}</td>
								</tr>
								<tr>
									<th>No. Cek</th>
									<td>
										@if (!empty($transaksi_grosir->no_cek))
										{{ $transaksi_grosir->no_cek }}
										@else
										-
										@endif
									</td>
								</tr>
								<tr>
									<th>Nominal Cek</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->nominal_cek) }}</td>
								</tr>
								<tr>
									<th>No. BG</th>
									<td>
										@if (!empty($transaksi_grosir->no_bg))
										{{ $transaksi_grosir->no_bg }}
										@else
										-
										@endif
									</td>
								</tr>
								<tr>
									<th>Nominal BG</th>
									<td>Rp {{ \App\Util::duit($transaksi_grosir->nominal_bg) }}</td>
								</tr>
								<tr>
									<th>Operator</th>
									<td>{{ $transaksi_grosir->user->nama }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-7 col-xs-12">
						<div class="x_title">
							<h2>Item</h2>
							<div class="clearfix"></div>
						</div>
						<table class="table table-bordered table-striped table-hover" id="tabel-item">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Item</th>
									<th>Jumlah (pcs)</th>
									<th>Item Bonus</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($relasi_transaksi_grosir as $i => $relasi)
								<tr>
									<td class="text-center">{{ ++$i }}</td>
									<td>{{ $relasi->item->nama }}</td>
									<td>{{ $relasi->jumlah }}</td>
									<td>{{ $relasi->jumlah_bonus }} pcs {{ $relasi->item->bonus->nama }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('#tabel-item').DataTable();

		$(document).ready(function() {
			var url = "{{ url('transaksi-grosir') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
		
	</script>
@endsection
