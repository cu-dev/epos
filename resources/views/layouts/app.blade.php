<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Icon -->
	<link rel="icon" href="{{ asset('images/km.ico') }}">

	<!-- <title>{{ config('app.name', 'Laravel') }}</title> -->
	@yield('app.title')

	<!-- Styles -->
	<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
	@yield('app.style')
	<style type="text/css">
		#spinner {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: rgba(63, 91, 132, 0.5);
			z-index: 100;
			color: #FFFFFF;
			padding-top: 200px;
			text-align: center;
		}
		.slow-spin {
			-webkit-animation: fa-spin 1s infinite linear;
			animation: fa-spin 1s infinite linear;
		}
	</style>

	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>
</head>
<body class="nav-md">
	<div class="container body">
        <div class="main_container">
            @yield('app.content')
        </div>
    </div>
    <div id="spinner">
		<i class="fa fa-refresh fa-spin slow-spin" style="font-size:100px"></i>
	</div>
	<!-- Scripts -->
	<!-- <script src="{{ asset('js/app.js') }}"></script> -->
	
	@yield('app.script')
</body>
</html>
