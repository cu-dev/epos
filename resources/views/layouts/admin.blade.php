@extends('layouts.app')

@section('app.title')
    @yield('title')
@endsection

@section('app.style')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/btn.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('css/prettify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

    <!-- <link href="{{ asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/scroller.bootstrap.min.css') }}" rel="stylesheet"> -->
    <style media="screen">
        table > thead > tr > th {
            text-align: center !important;
        }
        table > tbody > tr > td.tengah-hv{
            vertical-align: middle;
            text-align: center; 
        }
        table > tbody > tr > td.tengah-vh{
            vertical-align: middle;
            text-align: center; 
        }
        table > tbody > tr > td.tengah-v {
            vertical-align: middle;
        }
        table > tbody > tr > td.tengah-h {
            text-align: center; 
        }
        .form-group .input-group > .input-group-addon {
            border-radius: 0;
        }
        .line {
            height: 10px;
            width: 100%;
            border-top: 2px solid #E6E9ED;
        }
        .collapse-link {
            cursor: pointer;
        }
        .btn-purple {
            color: #ffffff;
            background-color: #611BBD;
            border-color: #130269;
        } 

        .btn-purple:hover,
        .btn-purple:focus,
        .btn-purple:active,
        .btn-purple.active,
        .open .dropdown-toggle.btn-purple {
            color: #ffffff;
            background-color: #49247A;
            border-color: #130269;
        } 

        .btn-purple:active,
        .btn-purple.active,
        .open .dropdown-toggle.btn-purple {
            background-image: none;
        }

        .btn-purple.disabled,
        .btn-purple[disabled],
        fieldset[disabled] .btn-purple,
        .btn-purple.disabled:hover,
        .btn-purple[disabled]:hover,
        fieldset[disabled] .btn-purple:hover,
        .btn-purple.disabled:focus,
        .btn-purple[disabled]:focus,
        fieldset[disabled] .btn-purple:focus,
        .btn-purple.disabled:active,
        .btn-purple[disabled]:active,
        fieldset[disabled] .btn-purple:active,
        .btn-purple.disabled.active,
        .btn-purple[disabled].active,
        fieldset[disabled] .btn-purple.active {
            background-color: #611BBD;
            border-color: #130269;
        }

        .btn-purple .badge {
            color: #611BBD;
            background-color: #ffffff;
        }

        .sembunyi {
            display: none;
        }

        ul.msg_list li:last-child {
            padding: 5px;
        }

        .link-mati {
            pointer-events: none;
            cursor: progress;
            color: #999 !important;
        }

        #infoTransaksi {
            border: solid 1px rgb(204, 204, 204);;
            padding: 6px 12px;
            background-color: rgb(238, 238, 238);
            border-radius: 3px;
        }

        textarea {
            min-width: 100%;
            max-width: 100%;
        }

        .titleDetailItem {
            margin: 1px 0 2px 0;
            font-size: 18px;
            display: inline-grid !important;
        }

        .titleDetailItem span {
            color: rgb(115, 135, 156);
        }

        .titleDetailItem .nama-item {
            font-weight: bold;
        }

        .input-group-addon {
            min-width: 38px;
        }

        .tanggal-putih {
            background: #fff !important;
        }
        .bg-disabled {
            background: #eee !important;
        }

        .btn {
            min-width: 24px;
        }

        .geser_kanan {
            margin-right: -4px;
        }

        .select2_tinggi {
            height: 38px;
        }

        .disabled-select {
           background-color:#d5d5d5;
           opacity:0.5;
           border-radius:3px;
           cursor:not-allowed;
           position:absolute;
           top:0;
           bottom:0;
           right:0;
           left:0;
        }

        /*
         * This is for MDT functionalities
         */

        .mdtContainer table.table thead > tr > th.sorting,
        .mdtContainer table.table thead > tr > th.sorting_asc,
        .mdtContainer table.table thead > tr > th.sorting_desc,
        .mdtContainer table.table thead > tr > td.sorting,
        .mdtContainer table.table thead > tr > td.sorting_asc,
        .mdtContainer table.table thead > tr > td.sorting_desc {
            padding-right: 30px;
        }

        .mdtContainer table.table thead > tr > th.sorting,
        .mdtContainer table.table thead > tr > th.sorting_asc,
        .mdtContainer table.table thead > tr > th.sorting_desc {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .mdtContainer table.table thead .sorting,
        .mdtContainer table.table thead .sorting_asc,
        .mdtContainer table.table thead .sorting_desc,
        .mdtContainer table.table thead .sorting_asc_disabled,
        .mdtContainer table.table thead .sorting_desc_disabled {
            cursor: pointer;
            position: relative;
        }

        .mdtContainer table.table thead .sorting::after,
        .mdtContainer table.table thead .sorting_asc::after,
        .mdtContainer table.table thead .sorting_desc::after,
        .mdtContainer table.table thead .sorting_asc_disabled::after,
        .mdtContainer table.table thead .sorting_desc_disabled::after {
            position: absolute;
            bottom: 8px;
            right: 8px;
            display: block;
            font-family: 'Glyphicons Halflings';
            opacity: 0.5;
        }

        .mdtContainer table.table thead .sorting::after {
            opacity: 0.2;
            content: "\e150";
        }

        .mdtContainer table.table thead .sorting_asc::after {
            content: "\e155";
        }

        .mdtContainer table.table thead .sorting_desc::after {
            content: "\e156";
        }

        /*
         * End for MDT functionalities
         */


    </style>
    @yield('style')
@endsection

@section('app.content')
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0; ">
            <a href="{{ url('/') }}" class="site_title">
                {{-- <i class="fa fa-paw"></i><span> EPOS - KM</span> --}}
                <div>
                    <img src="{{ asset('images/km.ico') }}" style="width:50px;" id="logo_perusahaan" alt="Avatar">
                    <span class="pull-right" style="padding-right: 25px;"> EPOS - KM</span>
                </div>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{URL::to('/foto_profil/'.Auth::user()->foto)}}" class="img-circle profile_img" style="width: 52px; height: 52px; vertical-align: middle" >
            </div>
            <div class="profile_info">
                <span>Sugeng Rawuh,</span>
                <h2>{{ Auth::user()->username }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    @if( Auth::user()->level_id == 1 ||  Auth::user()->level_id == 2 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Data Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Pemasok</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    {{-- <li><a href="{{url('bonus')}}">Bonus</a></li> --}}
                                    <li><a href="{{url('item')}}">Item</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{ url('bank') }}"> Bank </a></li>
                                {{-- <li><a href="{{ url('kredit') }}"> Kredit </a></li> --}}
                                <li><a href="{{url('wilayah')}}">Wilayah</a></li>
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Pembelian<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian/create') }}">Tambah Transaksi Pembelian</a></li>
                                <li><a href="{{ url('po-pembelian') }}">Daftar Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian') }}">Daftar Transaksi Pembelian</a></li>
                                <li><a href="{{ url('retur-pembelian') }}">Daftar Retur Pembelian</a></li>
                                {{-- <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li> --}}
                                {{-- <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li> --}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                <li><a href="{{ url('transaksi-grosir-vip/create') }}">Tambah Pesanan Penjualan (Prioritas)</a></li>
                                <li><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan Penjualan</a></li>
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                                {{--<li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li>--}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Konsinyasi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-database"></i> Stok<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('stock-of-num') }}">Pengecekan Stok</a></li>
                                <li><a href="{{ url('penyesuaian_stok') }}">Penyesuaian Stok</a></li>
                                <li><a href="{{ url('penyesuaian_persediaan') }}">Penyesuaian Stok (Akuntansi)</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-archive"></i> Kas<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('kas')}}"></i> Kas</a></li>
                                <!-- <li><a href="{{url('cek')}}"></i> Cek</a></li>
                                <li><a href="{{url('bg')}}"></i> BG</a></li> -->
                                <li><a href="{{url('modal')}}"></i> Modal</a></li>
                                <li><a href="{{url('prive')}}"></i> Prive</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-credit-card"></i> Hutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('hutang') }}">Hutang Dagang </a></li>
                                <!-- <li><a href="{{ url('hutang-suplier') }}">Hutang Suplier</a></li> -->
                                <li><a href="{{ url('hutang_bank') }}"> Hutang Bank</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-newspaper-o"></i> Piutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('piutang-dagang') }}">Piutang Dagang</a></li>
                                <li><a href="{{ url('piutang_lain') }}">Piutang Lain-Lain</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-calculator"></i> Akuntansi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('harta_tetap') }}">Tanah dan Bangunan</a></li>
                                <li><a href="{{url('perlengkapan')}}">Perlengkapan</a></li>
                                <li><a href="{{url('peralatan')}}">Peralatan</a></li>
                                <li><a href="{{url('kendaraan')}}">Kendaraan</a></li>
                                <li><a href="{{ url('beban') }}">Pembayaran Beban Beban</a></li>
                                <li><a href="{{ url('gaji') }}">Pembayaran Gaji</a></li>
                                <li><a href="{{ url('pendapatan_lain') }}">Pendapatan Lain-lain</a></li>
                                <li><a href="{{ url('akun') }}">Akun</a></li>
                                <li><a href="{{ url('jurnal') }}">Jurnal Umum</a></li>
                                {{-- <li><a href="{{ url('jurnal_penyesuaian') }}">Jurnal Penyesuaian</a></li> --}}
                                <li><a href="{{ url('laba_rugi') }}">Laba Rugi</a></li>
                                <li><a href="{{ url('neraca') }}">Neraca</a></li>
                                <li><a href="{{ url('arus_kas') }}">Arus Kas</a></li>
                                <!-- <li><a href="{{ url('laba_ditahan') }}">Laba Ditahan</a></li>                           -->
                                {{-- <li><a href="{{ url('jurnal_penutup') }}">Jurnal Penutup</a></li>   --}} 
                                <li><a href="{{ url('bagi_hasil') }}">Penarikan Bagi Hasil</a></li> 
                            </ul>
                        </li>
                        <li><a><i class="fa fa-area-chart"></i> Grafik <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('grafik/wilayah') }}">Wilayah</a></li>
                                <li><a href="{{ url('grafik/pembelian') }}">Pembelian</a></li>
                                <li><a href="{{ url('grafik/penjualan') }}">Penjualan</a></li>
                                <li><a href="{{ url('grafik/beban') }}">Beban-beban</a></li>
                                <li><a href="{{ url('grafik/laba') }}">Laba Rugi Bersih</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('data_perusahaan')}}">Data Perusahaan</a></li>
                                <!-- <li><a href="{{ url('level') }}"> Tingkatan Pengguna</a></li> -->
                                <li><a href="{{ url('user') }}">Pengguna </a></li>
                                {{-- <li><a href="{{url('money-limit')}}">Batas Laci Kasir Eceran</a></li>
                                <li><a href="{{url('laci')}}">Setoran Laci</a></li>
                                <li><a href="{{url('log_laci')}}">Riwayat Laci</a></li> --}}
                                <li><a>Laci<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('laci')}}">Setoran Laci</a></li>
                                    <li><a href="{{url('log_laci')}}">Riwayat Laci</a></li>
                                    <li><a href="{{url('money-limit')}}">Batas Laci Eceran</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{url('periode')}}">Batas Periode Pesanan</a></li>
                                {{-- <li><a href="{{url('setoran-kasir')}}">Setoran Kasir Eceran</a></li>
                                <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                                <li><a href="{{url('rule-bonus')}}">Bonus Penjualan</a></li>
                                <li><a href="{{url('rule_retur')}}">Batas Syarat Retur</a></li>
                                <li><a href="{{url('catatan')}}">Catatan Struk / Faktur</a></li>
                                <li><a href="{{url('persen_pajak')}}">Persentase Pajak</a></li>
                                {{-- <li><a href="{{url('event')}}">Layout Invoice</a></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 3 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <!-- <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Suplier</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    <li><a href="{{url('bonus')}}">Bonus</a></li>
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                            </ul>
                        </li> -->
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                <li id="tambahPOEceran"><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan Penjualan</a></li>
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                                {{--<li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li>--}}
                            </ul>
                        </li>
                    
                        <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('setoran-kasir')}}">Setoran Laci</a></li>
                                {{-- <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                                {{-- <li><a href="{{url('laci')}}">Laci Grosia></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 4 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                               {{--  <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Suplier</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    <li><a href="{{url('bonus')}}">Bonus</a></li>
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li> --}}
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                                <li><a href="{{url('wilayah')}}">Wilayah</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan</a></li> --}}
                                {{-- <li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li> --}}
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                            </ul>
                        </li>
                    
                        <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('laci')}}">Setoran Laci</a></li>
                                {{-- <li><a href="{{url('setoran-kasir')}}">Setoran Kasir Eceran</a></li> --}}
                                {{-- <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 5 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Data  Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Pemasok</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    {{-- <li><a href="{{url('bonus')}}">Bonus</a></li> --}}
                                    <li><a href="{{url('item')}}">Item</a></li>
                                  </ul>
                                </li>
                                {{-- <li><a href="{{url('pelanggan')}}">Pelanggan</a></li> --}}
                                {{-- <li><a href="{{url('wilayah')}}">Wilayah</a></li> --}}
                                <!-- <li><a href="{{url('perlengkapan')}}">Perlengkapan</a></li>
                                <li><a href="{{url('peralatan')}}">Peralatan</a></li>
                                <li><a href="{{url('kendaraan')}}">Kendaraan</a></li> -->
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Pembelian<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian/create') }}">Tambah Transaksi Pembelian</a></li>
                                <li><a href="{{ url('po-pembelian') }}">Daftar Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian') }}">Daftar Transaksi Pembelian</a></li>
                                <li><a href="{{ url('retur-pembelian') }}">Daftar Retur Pembelian</a></li>
                                {{-- <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li> --}}
                                {{-- <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li> --}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Konsinyasi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                            </ul>
                        </li>
                        {{-- <li><a><i class="fa fa-database"></i> Stok<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('stock-of-num') }}">Pengecekan Stok</a></li>
                                <li><a href="{{ url('penyesuaian_stok') }}">Penyesuaian Stok</a></li>
                            </ul>
                        </li> --}}
                        <li><a><i class="fa fa-credit-card"></i> Hutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('hutang') }}">Hutang Dagang </a></li>
                                {{-- <li><a href="{{ url('hutang_bank') }}"> Hutang Bank</a></li> --}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-calculator"></i> Akuntansi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('beban') }}">Pembayaran Beban</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('laci')}}">Setoran Laci</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small invisible">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
                <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
    <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle" style="width: 10%;">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right" style="width: 90%;">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{URL::to('/foto_profil/'.Auth::user()->foto)}}" alt="">{{ Auth::user()->username }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ url('profil') }}"> Profil</a></li>
                        {{-- <li><a href="javascript:;"> Bantuan</a></li> --}}
                        <li id="btnLogout">
                            <a href="{{url('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-green" id="sumAlert"></span>
                    </a>
                    <ul id="alertNotification" class="dropdown-menu list-unstyled msg_list" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    </ul>
                </li>
                @if (in_array(Auth::user()->level_id, [1, 2]))
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-exchange"></i>
                        <span class="badge bg-green" id="sumNotice"></span>
                    </a>
                    <ul id="noticeNotification" class="dropdown-menu list-unstyled msg_list" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    </ul>
                </li>
                @endif
                {{-- <li role="presentation" class="dropdown" id="duit">
                </li> --}}
                <div id="cash-box" class="nav toggle pull-right sembunyi" style="width: 600px; padding-right: 15px;">
                    <span id="cash" class="pull-right" style="margin-top: 4px;"></span>
                </div>
                <div id="duit-box" class="nav toggle pull-right sembunyi" style="width: 700px; padding-right: 15px;">
                    <span id="duit" class="pull-right" style="margin-top: 4px;"></span>
                </div>
            </ul>
            
        </nav>
    </div>
</div>

<!-- /top navigation -->
<div class="right_col" role="main">
    <div class="row">
        @yield('content')
    </div>
</div>

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/accounting.min.js') }}"></script>

    <!-- <script type="text/javascript" src="{{ asset('js/fastclick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/icheck.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.scannerdetection.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script> -->

    <!-- <script type="text/javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.fixedHeader.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.keyTable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/responsive.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.scroller.min.js') }}"></script> -->

    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('js/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('js/prettify.js') }}"></script>
    <script type="text/javascript">
        var showSpinner = false;
    </script>

    @yield('script')
    <script type="text/javascript">
        // $('.right_col').css('min-height', $('.left_col').css('height'));
        var sumAlert = 0;

        $(document).ready(function() {
            if (!showSpinner) {
                setTimeout(function() {
                    $('#spinner').hide();
                }, 500);
            }

            $('.tanggal').each(function(index, el) {
                var text = $(el).text();
                if (text === undefined || text === '') {
                    $(el).text('-');
                    $(el).css('text-align', 'center');
                } else {
                    var tanggal = text.split('-');
                    var dd = tanggal[2];
                    var mm = tanggal[1];
                    var yyyy = tanggal[0];

                    tanggal = dd+'-'+mm+'-'+yyyy;
                    $(el).text(tanggal);
                }
            });

            $('.duit').each(function(index, el) {
                var duit = parseInt($(el).text());
                $(el).text(duit.toLocaleString());
            });

            $(".select2_single").select2({
                width: '100%',
                allowClear: true
            });

            $('.CapitalFirst').each(function(index, el) {
                var text = $(el).text();
                $(el).text(CapitalFirst(text));
            });
            // $('.select2_single').each(function(index, el) {
            //     $(el).val('').change();
            // });
        });

        /*$(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('logo_perusahaan/json') }}";

            $.get(url, function(data) {
                var src = '{{ URL::to("/images/") }}'+ '/' + data.logo.logo;
                $('#logo_perusahaan').attr('src', src);
            });
        });*/

        //; 
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/stok/json') }}";

            $.get(url, function(data) {
                if (data.kurang.length > 0) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('item/alert') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Batas Minimal Stok</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa item yang kurang dari batas minimal! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        //notice transaksi pembelian
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/notice/json') }}";

            $.get(url, function(data) {
                // console.log(data);
                if (data.notices.length > 0) {
                    for (var i = 0; i < data.notices.length; i++) {
                        var notice = data.notices[i];
                        var url = '{{ url('transaksi-pembelian') }}' + '/' + notice.transaksi_pembelian_id + '/again';
                        $("#noticeNotification").append(''+
                            '<li>'+
                                '<a href="'+url+'">'+
                                    '<span>'+
                                        '<span><h5><strong>Pemberitahuan Perubahan Harga Beli</h5></strong></span>'+
                                    '</span>'+
                                    '<span class="message" style="line-height: 1; margin-bottom: 5px;">'+
                                        'Kode Transaksi: ['+notice.transaksi_pembelian.kode_transaksi+']<br style="margin-bottom: 5px;">'+
                                        (notice.transaksi_pembelian.nota==null?'':'Kode Faktur: ['+notice.transaksi_pembelian.nota+']<br style="margin-bottom: 5px;">')+
                                        'Ada beberapa item yang harga jualnya harus disesuaikan!'+
                                    '</span>'+
                                '</a>'+
                            '</li>');
                        $('#sumNotice').text(data.notices.length);
                    }
                }
            });
        });

        //notice kadaluarsa
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/kadaluarsa/json') }}";
            // console.log(url);
            $.get(url, function(data) {
                if (data.kadaluarsa.length > 0) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('item/kadaluarsa') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Item Kadaluarsa</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa item yang kadaluarsa, segera cek! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        //notice kas kasir close to limit
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_kasir/json') }}";
            $.get(url, function(data) {
                if (data.cash != null) {
                    $('#cash-box').removeClass('sembunyi');
                    var cash = data.cash;
                    var duit = cash.nominal;
                    duit = duit.toLocaleString(['ban', 'id']);
                    $('#cash').text('Saldo Laci Rp'+duit);

                    var money_limit = data.money_limit;
                    var bukaan = data.bukaan;

                    var user = '{{ json_encode(Auth::user()) }}';
                    user = user.replace(/&quot;/g, '"');
                    user = JSON.parse(user);
                    if (cash['nominal'] - bukaan >= money_limit.nominal * 0.75 && user.id == 3) {
                        swal({
                            title: 'Laci hampir penuh!',
                            text: 'Setorkan uang kepada kasir grosir untuk mengurangi!',
                            type: 'warning',
                            showCloseButton: true,
                            // showCancelButton: true,
                            confirmButtonColor: '#26B99A',
                            // cancelButtonColor: '#d9534f',
                            confirmButtonText: '<i class="fa fa-check"></i> OK',
                            // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                        }).then(function() {
                            // console.log('oi');
                        }, function(dismiss) {
                            // console.log(dismiss);
                        });
                    }
                } else {
                    $('#cash-box').addClass('sembunyi');
                }
            });
        });

        /*$(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_grosir/json') }}";
            $.get(url, function(data) {
                if(data.cash != null && Object.keys(data.cash).length > 0){
                    $("#duit").append('<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">'+
                        '<i class="fa fa-money"></i>'+
                        '<span class="badge bg-green" id="sumAlert"></span>'+
                    '</a>'+
                    '<ul id="duit_row" class="dropdown-menu list-unstyled msg_list" role="menu">');

                    for(var i=0; i < Object.keys(data.cash).length; i++){
                        $("#duit_row").append('<li>'+
                            '<a>'+
                                '<span>'+
                                    '<span><h5><strong> Rp' + data.cash[i].nominal.toLocaleString(['ban', 'id']) +'</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Saldo Kasir : '+ data.cash[i].nama +
                                '</span>'+
                            '</a>'+
                        '</li> </ul>');
                    }
                }
            });
        });*/

        //show saldo kasir untuk level [1,2,4]
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_grosir/json') }}";
            // console.log(url);
            $.get(url, function(data) {
                if(data.length != 0){
                    // console.log(data.length);
                    var cash = '';

                    var a = 0;
                    var panjang = Object.keys(data.cash).length;
                    $('#duit-box').removeClass('sembunyi');
                    for(var i=0; i < Object.keys(data.cash).length; i++){
                        a += 1;
                        if(a == panjang){
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal;
                        }else{
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal+' | ';
                        }
                    }
                    $('#duit').text(cash);
                }
            });
        });

        //show saldo kasir n grosir (admin, dll)
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_gudang/json') }}";
            $.get(url, function(data) {
                if(data.length != 0){
                    // console.log(data.length);
                    var cash = '';

                    var a = 0;
                    var panjang = Object.keys(data.cash).length;
                    $('#duit-box').removeClass('sembunyi');
                    for(var i=0; i < Object.keys(data.cash).length; i++){
                        a += 1;
                        if(a == panjang){
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal;
                        }else{
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal+' | ';
                        }
                    }
                    $('#duit').text(cash);
                }
            });
        });

        /*$(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('sidebar/setoran-buka/json') }}";
            $.get(url, function(data) {
                // console.log(data);
                var setoran_buka = data.setoran_buka;
                var cash_drawer = data.cash_drawer;
                // console.log(setoran_buka);
                if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
                    $('#tambahPOEceran').addClass('link-mati');
                    $('#tambahPOEceran').find('a').addClass('link-mati');
                }
            });
        });*/

        //keyup class angka
        $(document).on('keyup', 'input[type="text"]', function(event) {
            event.preventDefault();

            if ($(this).hasClass('angka')) {
                var angka = $(this).val();

                if (angka === undefined || angka === null || angka === '' || angka === ' ' || angka.length === 0) {
                    $(this).val(angka.slice(0, -1));
                    return;
                }

                if (isNaN(angka)) {
                    var angkaBaru = parseInt(angka.replace(/\D/g, ''), 10);
                    if (isNaN(angkaBaru)) {
                        if (angka.length > 0) {
                            $(this).val(angka.slice(0, -1));
                        } else {
                            $(this).val('');
                        }
                        
                    } else {
                        $(this).val(angkaBaru.toLocaleString(['ban', 'id']));
                        $(this).next().val(angkaBaru);
                    }
                } else {
                    angka = parseInt(angka.replace(/\D/g, ''), 10);
                    $(this).val(angka.toLocaleString(['ban', 'id']));
                    $(this).next().val(angka);
                }
            }
        });

        //kasir eceran gak bisa logout kalo saldo laci belum 0
        $(document).on('click', '#btnLogout', function(event) {
            event.preventDefault();

            var href = $(this).find('a').attr('href');
            var url = '{{ url("logout/json") }}';
            $.get(url, function(data) {
                var user = data.user;
                // console.log(user);
                if (user.level_id == 3 && nominal > 0) {
                    var cash_drawer = data.cash_drawer;
                    var laci = data.laci;
                    var nominal = parseFloat(cash_drawer.nominal);
                    var laci_grosir = parseFloat(laci.grosir);
                    swal({
                        title: 'Terjadi kesalahan!',
                        text: 'Masih terdapat uang di laci eceran!',
                        type: 'warning',
                        showCloseButton: true,
                        // showCancelButton: true,
                        confirmButtonColor: '#26B99A',
                        // cancelButtonColor: '#d9534f',
                        confirmButtonText: '<i class="fa fa-check"></i> OK',
                        // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                    }).then(function() {
                        // console.log('oi');
                    }, function(dismiss) {
                        // console.log(dismiss);
                    });
                // } else if (user.level_id == 4 && laci_grosir > 0 ) {
                //     swal({
                //         title: 'Terjadi kesalahan!',
                //         text: 'Masih terdapat uang di laci grosir!',
                //         type: 'warning',
                //         showCloseButton: true,
                //         // showCancelButton: true,
                //         confirmButtonColor: '#26B99A',
                //         // cancelButtonColor: '#d9534f',
                //         confirmButtonText: '<i class="fa fa-check"></i> OK',
                //         // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                //     }).then(function() {
                //         // console.log('oi');
                //     }, function(dismiss) {
                //         // console.log(dismiss);
                //     });
                } else {
                    var ww = window.open(href, '_self');
                }
            });
        });

        //alert jatuh tempo pembelian
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/jatuh_tempo/pembelian/') }}";

            $.get(url, function(data) {
                if(data > 0 && data != undefined) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('pembelian/jatuh_tempo') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Jatuh Tempo Pembelian</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa transaksi pembelian yang melewati jatuh tempo, segera cek! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        //alert jatuh tempo penjulan
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/jatuh_tempo/penjualan/') }}";

            $.get(url, function(data) {
                if(data > 0 && data != undefined) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('penjualan/jatuh_tempo') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Jatuh Tempo Penjualan</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa transaksi penjualan yang melewati jatuh tempo, segera cek! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        //alert setoran laci ada yang masuk
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/laci_laci/') }}";
            $.get(url, function(data) {
                if(data.log.length > 0) {
                    if(data.user_level == 3){
                        $("#alertNotification").append('<li>'+
                            '<a href="{{ url('setoran-kasir/') }}">'+
                                '<span>'+
                                    '<span><h5><strong>Pemberitahuan Setoran Uang Laci</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Ada setoran laci yang belum diseujui, segera cek! '+
                                '</span>'+
                            '</a>'+
                        '</li>');
                        sumAlert += 1;
                        $('#sumAlert').text(sumAlert);
                    }else{
                        $("#alertNotification").append('<li>'+
                            '<a href="{{ url('laci/') }}">'+
                                '<span>'+
                                    '<span><h5><strong>Pemberitahuan Setoran Uang Laci</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Ada setoran laci yang belum diseujui, segera cek! '+
                                '</span>'+
                            '</a>'+
                        '</li>');
                        sumAlert += 1;
                        $('#sumAlert').text(sumAlert);
                    }
                }
            });
        });

        /*
         * This is for MDT functionalities
         */

        var mdt_a = 'Sebelumnya';
        var mdt_z = 'Selanjutnya';

        function renderMDTHeader(base_url) {
            // console.log('renderMDTHeader');

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            $mdtContainer.find('#mdtHeader').empty();
            var mdtHeader = `
                <div class="col-md-6">
                    Tampilkan
                    <select name="data" class="form-control input-sm" style="width: 75px; display: inline;">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    data
                </div>
                <div class="col-md-6" style="text-align: right;">
                    Cari:
                    <input type="text" name="input_query" id="input_query" class="form-control input-sm" style="max-width: 200px; display: inline; margin-left: 5px;">
                </div>
            `;
            $mdtContainer.find('#mdtHeader').append($(mdtHeader));
        }

        function refreshMDTHeader(base_url) {
            // console.log('refreshMDTHeader');

            renderMDTHeader(base_url);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
            var search_query = $mdtContainer.find('input[name="search_query"]').val();
            // console.log(data_per_halaman, search_query);

            $mdtContainer.find('select').val(data_per_halaman);
            $mdtContainer.find('input[name="input_query"]').val(search_query);
        }

        function renderMDTFooter(base_url) {
            // console.log('renderMDTFooter');

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            // $('.mdtContainer .pagination li').remove();
            $mdtContainer.find('.pagination li').remove();

            // $('.mdtContainer .pagination').append('<li halaman="Sebelumnya"><a href="#">Sebelumnya</a></li>');
            $mdtContainer.find('.pagination').append('<li halaman="Sebelumnya"><a href="#">Sebelumnya</a></li>');

            var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
            var data_total = parseInt($mdtContainer.find('input[name="data_total"]').val());
            var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
            var jumlah_halaman = Math.ceil(data_total / data_per_halaman);

            if (jumlah_halaman <= 7) {
                for (var i = 1; i <= jumlah_halaman; i++) {
                    $mdtContainer.find('.pagination').append('<li halaman="' + i + '"><a href="#">' + i + '</a></li>');
                }
            } else if (jumlah_halaman == 8) {
                if (halaman_sekarang <= 4) {
                    // 1 2 3 [4] 5 ... 8
                    for (var i = 1; i <= 5; i++) {
                        $mdtContainer.find('.pagination').append('<li halaman="' + i + '"><a href="#">' + i + '</a></li>');
                    }
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="8"><a href="#">8</a></li>');
                } else {
                    // 1 ... 4 [5] 6 7 8
                    $mdtContainer.find('.pagination').append('<li halaman="1"><a href="#">1</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    for (var i = 4; i <= 8; i++) {
                        $mdtContainer.find('.pagination').append('<li halaman="' + i + '"><a href="#">' + i + '</a></li>');
                    }
                }
            } else {
                var halaman_tengah = Math.floor(jumlah_halaman / 2) + 1;
                if (halaman_sekarang <= 4) {
                    // console.log('a');
                    halaman_tengah = 4;

                    for (var i = 1; i <= 3; i++) {
                        $mdtContainer.find('.pagination').append('<li halaman="' + i + '"><a href="#">' + i + '</a></li>');
                    }

                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah) + '"><a href="#">' + (halaman_tengah) + '</a></li>');

                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah + 1) + '"><a href="#">' + (halaman_tengah + 1) + '</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="' + (jumlah_halaman) + '"><a href="#">' + (jumlah_halaman) + '</a></li>');

                } else if (jumlah_halaman - halaman_sekarang < 4) {
                    // console.log('c');
                    halaman_tengah = jumlah_halaman - 3;

                    $mdtContainer.find('.pagination').append('<li halaman="1"><a href="#">1</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah - 1) + '"><a href="#">' + (halaman_tengah - 1) + '</a></li>');

                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah) + '"><a href="#">' + (halaman_tengah) + '</a></li>');

                    for (var i = halaman_tengah + 1; i <= jumlah_halaman; i++) {
                        $mdtContainer.find('.pagination').append('<li halaman="' + i + '"><a href="#">' + i + '</a></li>');
                    }

                } else {
                    // console.log('b');
                    halaman_tengah = halaman_sekarang;

                    $mdtContainer.find('.pagination').append('<li halaman="1"><a href="#">1</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah - 1) + '"><a href="#">' + (halaman_tengah - 1) + '</a></li>');

                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah) + '"><a href="#">' + (halaman_tengah) + '</a></li>');

                    $mdtContainer.find('.pagination').append('<li halaman="' + (halaman_tengah + 1) + '"><a href="#">' + (halaman_tengah + 1) + '</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="..."><a href="#">...</a></li>');
                    $mdtContainer.find('.pagination').append('<li halaman="' + (jumlah_halaman) + '"><a href="#">' + (jumlah_halaman) + '</a></li>');
                }
            }

            // $('.mdtContainer .pagination').append('<li halaman="Selanjutnya"><a href="#">Selanjutnya</a></li>');
            $mdtContainer.find('.pagination').append('<li halaman="Selanjutnya"><a href="#">Selanjutnya</a></li>');
        }

        function refreshMDTFooter(base_url) {
            // console.log('refreshMDTFooter');

            renderMDTFooter(base_url);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            // $('.mdtContainer .pagination li').removeClass('active');
            // $('.mdtContainer .pagination li[halaman="'+mdt_a+'"]').removeClass('disabled');
            // $('.mdtContainer .pagination li[halaman="'+mdt_z+'"]').removeClass('disabled');
            $mdtContainer.find('.pagination li').removeClass('active');
            $mdtContainer.find('.pagination li[halaman="'+mdt_a+'"]').removeClass('disabled');
            $mdtContainer.find('.pagination li[halaman="'+mdt_z+'"]').removeClass('disabled');

            var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
            var data_total = parseInt($mdtContainer.find('input[name="data_total"]').val());
            var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
            var jumlah_halaman = Math.ceil(data_total / data_per_halaman);

            var index_awal = data_per_halaman * (halaman_sekarang - 1);
            var index_akhir = data_per_halaman * (halaman_sekarang) - 1;
            if (halaman_sekarang == jumlah_halaman) {
                index_akhir = (data_total % data_per_halaman) + (index_awal - 1);
                if (data_total % data_per_halaman == 0) {
                    index_akhir += data_per_halaman;
                }
            }

            if (data_total <= 0) {
                // $('.mdtContainer #paginationText').text('Menampilkan 0 sampai 0 dari 0 data');
                $mdtContainer.find('#paginationText').text('Menampilkan 0 sampai 0 dari 0 data');
            } else {
                // $('.mdtContainer #paginationText').text('Menampilkan ' + (index_awal+1) + ' sampai ' + (index_akhir+1) + ' dari ' + data_total + ' data');
                $mdtContainer.find('#paginationText').text('Menampilkan ' + (index_awal+1) + ' sampai ' + (index_akhir+1) + ' dari ' + data_total + ' data');
            }

            if (halaman_sekarang == 1) {
                // $('.mdtContainer .pagination li[halaman="'+mdt_a+'"]').addClass('disabled');
                $mdtContainer.find('.pagination li[halaman="'+mdt_a+'"]').addClass('disabled');
            }

            if (halaman_sekarang == Math.ceil(data_total / data_per_halaman) || data_total == 0) {
                // $('.mdtContainer .pagination li[halaman="'+mdt_z+'"]').addClass('disabled');
                $mdtContainer.find('.pagination li[halaman="'+mdt_z+'"]').addClass('disabled');
            }

            // $('.mdtContainer .pagination li[halaman="'+halaman_sekarang+'"]').addClass('active');
            $mdtContainer.find('.pagination li[halaman="'+halaman_sekarang+'"]').addClass('active');
        }

        function getMDTData(url) {
            // console.log(url);
            $.get(url, function(data) {
                // console.log(data);
                var base_url = url.split('?')[0];
                var $mdtContainer = null;
                $('.mdtContainer').each(function(index, el) {
                    if ($(el).find('input[name="base_url"]').val() == base_url) {
                        $mdtContainer = $(el);
                    }
                });

                $mdtContainer.find('input[name="data_per_halaman"]').val(data.data_per_halaman);
                $mdtContainer.find('input[name="search_query"]').val(data.search_query);
                $mdtContainer.find('input[name="data_total"]').val(data.data_total);
                $mdtContainer.find('input[name="halaman_sekarang"]').val(data.halaman_sekarang);

                refreshMDTHeader(base_url);
                refreshMDTData(data.data, base_url, data.inverse, data.data_total);
                refreshMDTFooter(base_url);

                if (data.typing) {
                    $mdtContainer.find('input[name="input_query"]').trigger('focus');
                }
            });
        }

        $(document).ready(function() {
            // console.log('mdt');
            $('.mdtContainer').each(function(index, el) {
                var field = $(el).find('input[name="field"]').val();
                var order = $(el).find('input[name="order"]').val();

                if (field == '') field = 'created_at';
                if (order == '') order = 'asc';

                var url = $(el).find('input[name="base_url"]').val();
                url += '?data_per_halaman=10';
                url += '&search_query=';
                url += '&halaman_sekarang=1';
                url += '&field='+field;
                url += '&order='+order;
                url += '&typing=false';
                url += '&first=true';

                // console.log(url);
                getMDTData(url);

                $(el).find('select').val(10);
                $(el).find('input[name="input_query"]').val('');
            });
        });

        $(document).on('change', '.mdtContainer select', function(event) {
            event.preventDefault();

            var data_per_halaman = $(this).val();
            var $mdtContainer = $(this).parents('.mdtContainer');
            // console.log(data_per_halaman);
            $mdtContainer.find('input[name="data_per_halaman"]').val(data_per_halaman);

            var url = $mdtContainer.find('input[name="base_url"]').val();
            var data_per_halaman = $mdtContainer.find('input[name="data_per_halaman"]').val();
            var search_query = $mdtContainer.find('input[name="search_query"]').val();
            // var halaman_sekarang = $mdtContainer.find('input[name="halaman_sekarang"]').val();
            var field = $mdtContainer.find('input[name="field"]').val();
            var order = $mdtContainer.find('input[name="order"]').val();
            url += '?data_per_halaman='+data_per_halaman;
            url += '&search_query='+search_query;
            url += '&halaman_sekarang='+1;
            url += '&field='+field;
            url += '&order='+order;
            url += '&typing=false';
            url += '&first=false';

            getMDTData(url);
        });

        $(document).on('keyup', '.mdtContainer #input_query', function(event) {
            event.preventDefault();

            var search_query = $(this).val();
            var $mdtContainer = $(this).parents('.mdtContainer');
            // console.log(search_query);
            $mdtContainer.find('input[name="search_query"]').val(search_query);

            var url = $mdtContainer.find('input[name="base_url"]').val();
            var data_per_halaman = $mdtContainer.find('input[name="data_per_halaman"]').val();
            var search_query = $mdtContainer.find('input[name="search_query"]').val();
            // var halaman_sekarang = $mdtContainer.find('input[name="halaman_sekarang"]').val();
            var field = $mdtContainer.find('input[name="field"]').val();
            var order = $mdtContainer.find('input[name="order"]').val();
            url += '?data_per_halaman='+data_per_halaman;
            url += '&search_query='+search_query;
            url += '&halaman_sekarang='+1;
            url += '&field='+field;
            url += '&order='+order;
            url += '&typing=true';
            url += '&first=false';

            // console.log(url);
            getMDTData(url);
        });

        $(document).on('click', '.mdtContainer th', function(event) {
            event.preventDefault();

            var $mdtContainer = $(this).parents('.mdtContainer');

            var field = $(this).attr('field');
            var order = 'asc';

            // initial state to asc state
            if ($(this).hasClass('sorting')) {

                $mdtContainer.find('th').removeClass('sorting');
                $mdtContainer.find('th').removeClass('sorting_asc');
                $mdtContainer.find('th').removeClass('sorting_desc');
                $mdtContainer.find('th').addClass('sorting');

                $(this).removeClass('sorting');
                $(this).addClass('sorting_asc');

            }
            // asc state to desc state
            else if ($(this).hasClass('sorting_asc')) {

                $mdtContainer.find('th').removeClass('sorting');
                $mdtContainer.find('th').removeClass('sorting_asc');
                $mdtContainer.find('th').removeClass('sorting_desc');
                $mdtContainer.find('th').addClass('sorting');

                $(this).removeClass('sorting');
                $(this).addClass('sorting_desc');

                order = 'desc';

            }
            // desc state to asc state
            else if ($(this).hasClass('sorting_desc')) {

                $mdtContainer.find('th').removeClass('sorting');
                $mdtContainer.find('th').removeClass('sorting_asc');
                $mdtContainer.find('th').removeClass('sorting_desc');
                $mdtContainer.find('th').addClass('sorting');

                $(this).removeClass('sorting');
                $(this).addClass('sorting_asc');

                order = 'asc';

            }

            $mdtContainer.find('input[name="field"]').val(field);
            $mdtContainer.find('input[name="order"]').val(order);

            var url = $mdtContainer.find('input[name="base_url"]').val();
            var data_per_halaman = $mdtContainer.find('input[name="data_per_halaman"]').val();
            var search_query = $mdtContainer.find('input[name="search_query"]').val();
            // var halaman_sekarang = $mdtContainer.find('input[name="halaman_sekarang"]').val();
            var field = $mdtContainer.find('input[name="field"]').val();
            var order = $mdtContainer.find('input[name="order"]').val();
            url += '?data_per_halaman='+data_per_halaman;
            url += '&search_query='+search_query;
            url += '&halaman_sekarang='+1;
            url += '&field='+field;
            url += '&order='+order;
            url += '&typing=false';
            url += '&first=false';

            getMDTData(url);
        });

        $(document).on('click', '.mdtContainer .pagination li a', function(event) {
            event.preventDefault();

            if (!$(this).parent().hasClass('disabled')) {
                var halaman_sekarang = $(this).text();
                var $mdtContainer = $(this).parents('.mdtContainer');
                // console.log(halaman_sekarang);

                if (isNaN(halaman_sekarang)) {
                    if (halaman_sekarang == mdt_a) {
                        halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val()) - 1;
                    }
                    if (halaman_sekarang == mdt_z) {
                        halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val()) + 1;
                    }
                    $(this).trigger('blur');
                }

                if ($mdtContainer.find('input[name="halaman_sekarang"]').val() != halaman_sekarang) {
                    $mdtContainer.find('input[name="halaman_sekarang"]').val(halaman_sekarang);

                    var url = $mdtContainer.find('input[name="base_url"]').val();
                    var data_per_halaman = $mdtContainer.find('input[name="data_per_halaman"]').val();
                    var search_query = $mdtContainer.find('input[name="search_query"]').val();
                    var halaman_sekarang = $mdtContainer.find('input[name="halaman_sekarang"]').val();
                    var field = $mdtContainer.find('input[name="field"]').val();
                    var order = $mdtContainer.find('input[name="order"]').val();
                    url += '?data_per_halaman='+data_per_halaman;
                    url += '&search_query='+search_query;
                    url += '&halaman_sekarang='+halaman_sekarang;
                    url += '&field='+field;
                    url += '&order='+order;
                    url += '&typing=false';
                    url += '&first=false';

                    getMDTData(url);
                }
            }
        });

        /*
         * End for MDT functionalities
         */

        //alert item ada yang belum validasi
        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/item/validasi/json/') }}";
            {{-- var url = "{{ url('alert/item/validasi/') }}"; --}}
            $.get(url, function(data) {
                if(data > 0) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('alert/item/validasi/') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Validasi Item</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada item yang belum divalidasi, segera cek! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

    </script>
@endsection
