@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Retur Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        #btnRetur, #btnDetail, #btnKembali {
            margin-bottom: 0;
        }
        #btnRetur {
            /*margin-right: 0;*/
        }
        .full-width {
            width: 100%;
        }
        .feedback {
            background-color : #31B0D5;
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
            border-color: #46b8da;
        }
        #mybutton {
            position: fixed;
            bottom: -4px;
            right: 10px;
        }
        #tabelKeranjang td {
            border: none;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="height: auto;">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="full-width">Tambah Retur Penjualan</h2>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
                            </div>
                        </ul>
                        <a href="{{ url('transaksi-grosir/'.$transaksi_penjualan->id.'/retur') }}" id="btnRetur" class="btn btn-sm btn-warning pull-right">
                            <i class="fa fa-sign-in"></i> Lihat Retur
                        </a>
                        <a href="{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}" id="btnDetail" class="btn btn-sm btn-info pull-right">
                            <i class="fa fa-eye"></i> Detail Penjualan
                        </a>
                        <a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_penjualan->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->kode_transaksi }}</td>
                                </tr>
                                @if ($transaksi_penjualan->pelanggan_id != null)
                                <tr>
                                    <th>Nama Pelanggan</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->pelanggan->nama }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Tanggal PO</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Status Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->status }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->user->nama }}</td>
                                </tr>
                                @if ($transaksi_penjualan->pengirim != null)
                                <tr>
                                    <th>Pengirim</th>
                                    <td style="width: 60%;">{{ $transaksi_penjualan->pengirim }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_penjualan->nominal_tunai != null && $transaksi_penjualan->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_penjualan->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_transfer != null && $transaksi_penjualan->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->bank_id != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_penjualan->bank->nama_bank }} [{{ $transaksi_penjualan->bank->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_penjualan->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_cek != null && $transaksi_penjualan->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_penjualan->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_bg != null && $transaksi_penjualan->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_bg) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_kredit != null)
                                <tr>
                                    <th>No Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_penjualan->no_kredit }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_kredit != null && $transaksi_penjualan->nominal_kredit > 0)
                                <tr>
                                    <th>Nominal Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_kredit) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_titipan != null && $transaksi_penjualan->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_titipan) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_penjualan->harga_total != null && $transaksi_penjualan->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->harga_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nego_total != null && $transaksi_penjualan->nego_total > 0)
                                <tr>
                                    <th>Nego Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nego_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->ongkos_kirim != null && $transaksi_penjualan->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->ongkos_kirim) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->jumlah_bayar != null && $transaksi_penjualan->jumlah_bayar > 0)
                                <tr>
                                    <th>Kembali</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->jumlah_bayar - ($transaksi_penjualan->nego_total>0?$transaksi_penjualan->nego_total:$transaksi_penjualan->harga_total) - $transaksi_penjualan->ongkos_kirim) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Subtotal</th>
                                    <th class="text-left">Nego</th>
                                    <th class="text-left">Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_penjualan as $i => $relasi)
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah">{{ $relasi->jumlah }}</td>
                                    <td id="harga" class="text-right">{{ \App\Util::duit0($relasi->subtotal / $relasi->jumlah) }}</td>
                                    {{-- <td class="text-right {{ $relasi->nego != null ? 'text-success' : '' }}">{{ \App\Util::duit0($relasi->nego != null ? $relasi->nego : $relasi->subtotal) }}</td> --}}
                                    <td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->nego) }}</td>
                                    @if ($relasi->bonuses != null)
                                    <td>
                                        @foreach ($relasi->bonuses as $j => $rb)
                                            {{ $rb['jumlah'] }} {{ $rb['bonus']->nama }}
                                            @if ($j != count($relasi->bonuses) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="full-width">Retur Penjualan</h2>
                <h2 id="kodeReturTitle" class="full-width" style="font-size: 12px; margin:0"></h2>
                {{-- <span ></span> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option id="default" value="">Pilih Item</option>
                            @foreach($relasi_transaksi_penjualan as $relasi)
                            <option value="{{ $relasi->item->kode }}">{{$relasi->item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
            </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th class="text-left">Item</th>
                            <th class="text-left">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Retur</h2>
                <div class="clearfix"></div>
            </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            {{-- <table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;"> --}}
                            <table class="table" id="tabelKeranjang">
                                {{-- <thead>
                                    <tr>
                                        <th style="width: 30px;"></th>
                                        <th class="text-left">Item</th>
                                        <th style="width: 100px;">Jumlah</th>
                                        <th style="width: 100px;">Satuan</th>
                                        <th style="width: 125px;">Harga</th>
                                        <th style="width: 125px;">Total</th>
                                    </tr>
                                </thead> --}}
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group col-md-8 col-sm-8 col-xs-8" id="pilihTindakan">
                        <div class="row">
                            <label class="control-label">Tindakan</label>
                            <div class="input-group">
                                <div id="tindakanButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnUang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Uang</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnSama" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Sama</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnLain" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Lain</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="metodePembayaranButtonGroup" >
                            <label class="control-label">Metode Pembayaran</label>
                            <div class="input-group">
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKredit" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kredit</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTitipan" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Titipan</button>
                                    </div>
                                    <div class="btn-group" role="group" style="width: 9px;">
                                        <button type="button" id="btnPiutang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Bayar Piutang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputTunaiContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Tunai</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" id="inputNominalTunai" class="form-control angka">
                            </div>
                        </div>
                        <div id="inputTransferBankContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_id">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNoTransfer" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalTransfrer" class="form-control angka">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputCekContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Cek</label>
                                    <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalCek" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputBGContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor BG</label>
                                    <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalBG" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputKreditContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transaksi Kredit</label>
                                    <input type="text" id="inputNomorKredit" class="form-control" style="height: 38px;">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Kredit</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalKredit" id="inputNominalKredit" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputTitipanContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Titipan</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" id="inputNominalTitipan" class="form-control angka">
                            </div>
                        </div>
                        <div id="inputPiutangContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Transaksi</label>
                                    <select class="form-control select2_single" name="piutang_id">
                                        <option value="">Pilih Transaksi</option>
                                        @foreach ($piutangs as $piutang)
                                        <option value="{{ $piutang->id }}">{{ $piutang->kode_transaksi }} [{{ \App\Util::duit0($piutang->sisa) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Bayar Piutang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalPiutang" id="inputNominalPiutang" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4" id="inputTotali">
                        <label class="control-label">Harga Nilai Barang (Rp)</label>
                        <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" />
                    </div>
                </div>
            </div>
            <div class="row form-group" id="keranjangLain" style="padding-top: 20px">
                <div class="form-group" style="padding: 10px">
                    <div class="line"></div>
                    <h2>Keranjang Retur Barang Lain</h2>
                    <div class="line"></div>
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12">
                            <label class="control-label">Nama Item Barang Retur</label>
                            <select name="item_retur" class="select2_single form-control">
                                @foreach($items as $item)
                                    <option value="{{ $item->kode }}">{{$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="x_content">
                        <table class="table" id="tabelKeranjangRetur" style="border-bottom: 1px solid #dfdfdf;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Item</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga per satuan (Rp)</th>
                                    <th>Input Total (Rp)</th>
                                    <th>Total (Rp)</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="formSimpanContainer">
                        <form id="form-simpan" action="{{ url('transaksi-pembelian/'.$transaksi_penjualan->id.'/retur') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="kode_retur" value="" />
                            <input type="hidden" name="transaksi_penjualan_id" value="{{ $transaksi_penjualan->id }}" />
                            <input type="hidden" name="harga_total" value="0" />
                            <input type="hidden" name="status" value="sama" />

                            <input type="hidden" name="total_uang" />
                            <input type="hidden" name="nominal_tunai" class="uang_retur" />

                            <input type="hidden" name="bank_id" />
                            <input type="hidden" name="no_tansfer" />
                            <input type="hidden" name="nominal_transfer" class="uang_retur" />
                            
                            <input type="hidden" name="no_cek" />
                            <input type="hidden" name="nominal_cek" class="uang_retur" />
                            
                            <input type="hidden" name="no_bg" />
                            <input type="hidden" name="nominal_bg" class="uang_retur" />
                            
                            <input type="hidden" name="no_kredit" />
                            <input type="hidden" name="nominal_kredit" class="uang_retur" />

                            <input type="hidden" name="nominal_titipan" class="uang_retur" />

                            <input type="hidden" name="hutang[]" />
                            <input type="hidden" name="nominal_hutang" class="uang_retur" />

                            <div id="append-section"></div>
                            <div class="clearfix">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success" id="submit" disabled style="width: 100px;">
                                        <i class="fa fa-check"></i> OK
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var selected_items = [];
        var selected_retur = [];

        var satuans = null;
        var relasi_transaksi_penjualan = null;

        // $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());

        function isBtnSimpanDisabled() {
            if ($('#btnLain').hasClass('btn-success')) return cek_lain(1, 0);
            
            if ($('#btnUang').hasClass('btn-success')){
                var tunai = parseFloat($('#formSimpanContainer').find('input[name="nominal_tunai"]').val());
                var transfer = parseFloat($('#formSimpanContainer').find('input[name="nominal_transfer"]').val());
                var cek = parseFloat($('#formSimpanContainer').find('input[name="nominal_cek"]').val());
                var bg = parseFloat($('#formSimpanContainer').find('input[name="nominal_bg"]').val());
                var kredit = parseFloat($('#formSimpanContainer').find('input[name="nominal_kredit"]').val());
                var titipan = parseFloat($('#formSimpanContainer').find('input[name="nominal_titipan"]').val());
                console.log(tunai, transfer, cek, bg, kredit, titipan); 
                if(tunai > 0 || transfer > 0 || cek > 0 || bg > 0 || kredit > 0 || titipan > 0){
                    return false;   
                } else{
                    return true;
                }
            }

            return false;
        }

        $(document).ready(function() {
            var url = "{{ url('transaksi-grosir') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#submit').hide();
            $('#inputTotali').hide();
            $('#pilihTindakan').hide();

            $('input[name="harga_total"]').val(0);
            $('#keranjangLain').hide();
            $('#metodePembayaranButtonGroup').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKreditContainer').hide();
            $('#inputKreditContainer').find('input').val('');
            $('#inputTitipanContainer').hide();
            $('#inputPiutangContainer').hide();
            
            $('#inputNominalTunai').val('');
            $('#inputNominalTransfrer').val('');
            $('#inputNominalCek').val('');
            $('#inputNominalBG').val('');
            $('#inputNominalKredit').val('');
            $('#inputNominalTitipan').val('');
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(0);

            $('#btnSama').removeClass('btn-default');
            $('#btnSama').addClass('btn-success');
            $('#btnSama').find('i').show('fast');

            $('.panel_toolbox').trigger('click');
            
            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var konversi = null;
                var v_jumlah = [];
                var v_satuan = [];
                var text_satuan = '';

                for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                    if (relasi_transaksi_penjualan[i].id == id) {
                        relasi = relasi_transaksi_penjualan[i];
                        break;
                    }
                }

                var item = relasi.item;
                var jumlah = relasi.jumlah;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var temp_konversi = satuan.konversi;
                    if (jumlah / temp_konversi >= 1) {
                        if (konversi == null) konversi = temp_konversi;
                        text_satuan += Math.floor(jumlah / temp_konversi);
                        text_satuan += ' ' + satuan.satuan.kode;
                        if (i != item.satuan_pembelians.length - 1) text_satuan += '<br>';
                        jumlah = jumlah % temp_konversi;
                    }
                }

                var harga = $(el).find('#harga').text();
                harga = harga.split('p')[1];
                harga = parseFloat(harga.replace(/\D/g, ''), 10);
                harga *= konversi;
                $(el).find('#harga').text('Rp'+harga.toLocaleString());

                $(el).find('#jumlah').html(text_satuan);
            });
        });

        // Buat ambil nomer transaksi terakhir
        $(window).on('load', function(event) {
            // var url = "{{ url('transaksi-pembelian/retur/last.json') }}";
            
            // $.get(url, function(data) {
            //     var kode = 1;
            //     if (data.retur_pembelian !== null) {
            //         var kode_retur = data.retur_pembelian.kode_retur;
            //         kode = parseInt(kode_retur.split('/')[0]);
            //         kode++;
            //     }

            //     kode = int4digit(kode);
            //     var tanggal = printTanggalSekarang('dd/mm/yyyy');
            //     kode_retur = kode + '/TRABR/' + tanggal;
            //     $('input[name="kode_retur"]').val(kode_retur);
            //     $('#kodeReturTitle').text(kode_retur);
            // });
        });

        function cariItem(kode, url, tr) {
            $.get(url, function(data) {
                // console.log(url);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;

                var transaksi_penjualan = data.transaksi_penjualan;
                var relasi_transaksi_penjualan = data.relasi_transaksi_penjualan;
                var tp_nego_total = 0;
                var tp_harga_total = parseFloat(transaksi_penjualan.harga_total);

                if (parseFloat(relasi_transaksi_penjualan.nego) > 0) {
                    tp_nego_total += parseFloat(relasi_transaksi_penjualan.nego);
                } else {
                    tp_nego_total += parseFloat(relasi_transaksi_penjualan.subtotal);
                }

                var v_harga = 0;
                if (tp_nego_total != tp_harga_total) {
                    // nego
                    // console.log('nego');
                    v_harga = tp_nego_total / tp_harga_total * parseFloat(relasi_transaksi_penjualan.subtotal) / parseFloat(relasi_transaksi_penjualan.jumlah);
                } else {
                    // tidak nego
                    // console.log('tidak nego');
                    v_harga = parseFloat(relasi_transaksi_penjualan.subtotal) / parseFloat(relasi_transaksi_penjualan.jumlah);
                }
                // console.log(v_harga);
                v_harga = Math.floor(v_harga / 100) * 100;
                // console.log(v_harga);

                $('#submit').show();
                $('#inputTotali').show();
                $('#pilihTindakan').show();

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].id,
                        satuan_id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                // var select_satuans = "";
                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };
                for (var i = 0; i < satuan_item.length; i++) {
                    // select_satuans[i] = satuan_item[i];
                    // if (i == satuan_item.length - 1) select_satuans += `<option value="`+ satuan_item[i].satuan_id +`" selected>`+ satuan_item[i].kode +`</option>`;
                    // else select_satuans += `<option value="`+ satuan_item[i].satuan_id +`">`+ satuan_item[i].kode +`</option>`;
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                // var pilihan_satuan = `<select name="satuan" class="form-control input-sm">`+select_satuans+`</select>`;

                var divs = '';
                var div_rusak = '';
                for (var i = 0; i < relasi_transaksi_penjualan.relasi_stok_penjualan.length; i++) {
                    var rsp = relasi_transaksi_penjualan.relasi_stok_penjualan[i];
                    // console.log(rsp);
                    var kadaluarsa = rsp.stok.kadaluarsa;
                    if (kadaluarsa == null) kadaluarsa = '-';

                    divs += ''+
                        '<div class="row">'+
                            '<div class="form-group col-md-12">'+
                                '<div id="rsp_id" rsp-id="'+rsp.id+'" class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        kadaluarsa+
                                    '</div>'+
                                    '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="'+(i==0?1:'')+'" />'+
                                    '<div id="pilihSatuan" class="input-group-btn">'+
                                        '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                        ul_satuan+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    div_rusak += ''+
                        '<div class="row">'+
                            '<div class="form-group col-md-12">'+
                                '<div id="rsp_id" rsp-id="'+rsp.id+'" class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        '<input type="checkbox" name="checkRusak" id="checkRusak" />'+
                                    '</div>'+
                                    '<input type="text" name="inputRusak" id="inputRusak" class="form-control input-sm angka" value="rusak" readonly />'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                }

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="'+ satuan_item[satuan_item.length-1].satuan_id +'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="'+parseFloat(v_harga) +'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-'+kode+'" value="'+parseFloat(v_harga)+'" class="subtotal" />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td style="width: 50px;">'+
                                '<h3><i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: #c9302c; padding-top: 8px;"></h3></i>'+
                            '</td>'+
                            '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3></td>'+
                            '<td style="width: 250px;">'+
                                // '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="1" />'+
                                // '<h3><small>Jumlah</small></h3>'+
                                '<label class="control-label">Jumlah</label>'+
                                divs+
                            '</td>'+
                            // '<td>'+ pilihan_satuan +'</td>'+
                            '<td style="width: 200px;">'+
                                // '<h3><small>Keterangan</small></h3>'+
                                '<label class="control-label">Keterangan</label>'+
                                div_rusak+
                            '</td>'+
                            '<td style="width: 200px;">'+
                                // '<h3><small>Harga</small></h3>'+
                                '<div class="row">'+
                                    '<div class="form-group col-md-12">'+
                                        '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                            '<label id="satuanHarga" class="control-label">Harga/'+satuan_terkecil.kode+'</label>'+
                                            '<div class="input-group" style="margin-bottom: 0;">'+
                                                '<div class="input-group-addon">'+
                                                    'Rp'+
                                                '</div>'+
                                                '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(v_harga).toLocaleString() + '" readonly="readonly" />'+
                                                '<input type="hidden" name="HargaPerSatuan" id="HargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(v_harga)+ '"/>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="row">'+
                                    '<div class="form-group col-md-12">'+
                                        '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                            '<label class="control-label">Harga Total</label>'+
                                            '<div class="input-group" style="margin-bottom: 0;">'+
                                                '<div class="input-group-addon">'+
                                                    'Rp'+
                                                '</div>'+
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control text-right input-sm angka" value="' + parseFloat(v_harga).toLocaleString() + '" readonly="readonly" />'+
                                                '<input type="hidden" name="SubTotal" id="SubTotal" class="form-control input-sm" value="' + parseFloat(v_harga)+ '"/>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                        '</tr>');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                // console.log(satuan_item);
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr);

                var harga_total = $('input[name="harga_total"]').val();
                var total = parseFloat(harga_total) + parseFloat(v_harga);

                $('#inputHargaTotal').val((total).toLocaleString());
                $('#formSimpanContainer').find('input[name="harga_total"]').val(parseFloat(total));
            });
        }

        function cariItemRetur(kode, url, tr) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = data.item.nama;

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < item.satuans.length; j++) {
                        if (item.satuans[j].satuan_id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: item.satuans[j].id,
                                satuan_id: item.satuan_pembelians[i].satuan.id,
                                kode: item.satuan_pembelians[i].satuan.kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }
                
                var select_satuans = "";
                satuan_item.sort(function(a, b){
                    return a.konversi-b.konversi
                }); 

                for(var i=0; i<satuan_item.length; i++){
                    // select_satuans[i] = satuan_item[i];
                    select_satuans += `<option value="`+ satuan_item[i].satuan_id +`">`+ satuan_item[i].kode +`</option>`
                }
                // #satuan-'+kode
                var pilihan_satuan = 
                    `<select name="satuanIn" id="SatuanIn" class="form-control input-sm">`
                    +select_satuans+`</select>`;

                if (tr === undefined) {               
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode_in[]" id="itemIn-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_in[]" id="jumlahIn-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" value="'+ satuan_item[0].satuan_id +'" name="satuan_id_in[]" id="satuanIn-' +kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga_in[]" id="hargaIn-' + kode + '" value=""/>');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal_in[]" id="subtotalIn-'+kode+'"/>');

                    $('#tabelKeranjangRetur').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang retur" id="removeIn" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="padding-top: 13px">'+nama+'</td>'+
                            '<td>'+
                                '<input type="text" name="inputJumlahItemIn" id="inputJumlahItemIn" class="form-control input-sm angka" value="1" />'+
                            '</td>'+
                            '<td>'+ pilihan_satuan +'</td>'+
                            '<td>'+
                                '<input type="text" name="inputHargaPerSatuanIn" id="inputHargaPerSatuanIn" class="form-control text-right input-sm" readonly="readonly" />'+
                            '</td>'+
                            '<td>'+
                                '<input type="text" name="inputSubTotalIn" class="form-control text-right input-sm inputSubTotalIn" value="0,000"/>'+
                            '</td>'+
                            '<td>'+
                                '<input type="text" name="SubTotalIn" class="form-control text-right input-sm SubTotalIn" readonly="readonly" value="0,000"/>'+
                            '</td>'+
                        '</tr>');
                }
            });
        }

        function deleteMe( arr, me ){
           var i = arr.length;
           while( i-- ) if(arr[i] == me ) arr.splice(i,1);
        }

        function uangClose(){
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKreditContainer').hide();
            $('#inputKreditContainer').find('input').val('');
            $('#inputTitipanContainer').hide();
            $('#inputPiutangContainer').hide();
            
            $('#btnTunai').removeClass('btn-danger');
            $('#btnTunai').addClass('btn-default');
            $('#btnTunai').find('i').hide('fast');
            
            $('#btnTransfer').removeClass('btn-warning');
            $('#btnTransfer').addClass('btn-default');
            $('#btnTransfer').find('i').hide('fast');

            $('#btnBG').removeClass('btn-primary');
            $('#btnBG').addClass('btn-default');
            $('#btnBG').find('i').hide('fast');

            $('#btnCek').removeClass('btn-success');
            $('#btnCek').addClass('btn-default');
            $('#btnCek').find('i').hide('fast');

            $('#btnKredit').removeClass('btn-info');
            $('#btnKredit').addClass('btn-default');
            $('#btnKredit').find('i').hide('fast');

            $('#btnTitipan').removeClass('btn-danger');
            $('#btnTitipan').addClass('btn-default');
            $('#btnTitipan').find('i').hide('fast');
            
            $('#btnPiutang').removeClass('btn-dark');
            $('#btnPiutang').addClass('btn-default');
            $('#btnPiutang').find('i').hide('fast');

            $('#metodePembayaranButtonGroup').hide();

            $('#inputNominalTunai').val('');
            $('#inputNominalTransfrer').val('');
            $('#inputNominalCek').val('');
            $('#inputNominalBG').val('');
            $('#inputNominalKredit').val('');
            $('#inputNominalTitipan').val('');
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_hutang"]').val(0);
        }

        function lainClose(){
            var item_retur = selected_retur;
            console.log(item_retur.length);
            for(var i=0; i<item_retur.length; i++){
                var kode         = item_retur[i];
                var tr           = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');

            deleteMe(selected_retur, kode);

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=In-'+kode+']').remove();
            }
            // var kode         = $(this).parents('tr').data('id');
            // var tr           = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');

            // deleteMe(selected_retur, kode);

            // tr.remove();
            // $('#form-simpan').find('#append-section').find('input[id*=In-'+kode+']').remove();
        }

        function total_uang(){
            var total = 0;
            $('.total_uang').each(function(index, el) {
                var tmp = parseFloat($(el).val());
                if (isNaN(tmp)) tmp = 0;
                total_retur += parseFloat(tmp);
            });
            $('#formSimpanContainer').find('input[name="total_uang"]').val(total);
        }

        function cek_lain(button, hitung){
            var harga_total = $('input[name="harga_total"').val();
            var total_retur = 0;
            $('input[name="subtotal_in[]"').each(function(index, el) {
                var tmp = parseFloat($(el).val());
                if (isNaN(tmp)) tmp = 0;
                total_retur += parseFloat(tmp);
            });

            if(harga_total != total_retur){
                if(button == 0) {
                    $('.inputSubTotalIn').each(function(index, el) {
                        $(this).parents('td').first().addClass('has-error');
                    });
                }else{
                    return true;
                }
            }else{
                if(button == 0) {
                    $('.inputSubTotalIn').each(function(index, el) {
                        $(this).parents('td').first().removeClass('has-error');
                    });
                }else{
                    return false;
                }
            }
        }

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();
            
            var transaksi = {{ $transaksi_penjualan->id }};
            var kode = $(this).val();
            var url  = "{{ url('transaksi-grosir') }}"+'/'+transaksi+'/retur/'+ kode +'/json';
            var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                if (kode) {
                    // kode_ = 'item-'+kode;
                    selected_items.push(kode);
                    cariItem(kode, url, tr);
                    $('#submit').prop('disabled', false);
                }
            }

            // if (!selected_items.includes(kode)) {
            //     if (kode) {
            //         // kode_ = 'item-'+kode;
            //         selected_items.push(kode);
            //         cariItem(kode, url, tr);
            //     }
            // }
        });

        $(document).on('change', 'select[name="item_retur"]', function(event) {
            event.preventDefault();
            var kode = $(this).val();
            var url  = "{{ url('retur-pembelian') }}"+'/'+kode+'/item/json';
            var tr   = $('#tabelKeranjangRetur').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (!selected_retur.includes(kode)) {
                if (kode) {
                    // kode_ = 'item-'+kode;
                    selected_retur.push(kode);
                    cariItemRetur(kode, url, tr);
                }
            }
        });

        /*var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();
            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();
            $(this).val(temp_jumlah);
        });*/

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            // var jumlah = parseFloat($(this).val());
            // temp_jumlah = jumlah;
            var rsp_id = parseFloat($(this).parents('#rsp_id').attr('rsp-id'));
            var jumlah = 0;
            $(this).parents('td').first().find('input[name="inputJumlahItem"]').each(function(index, el) {
                var temp_jumlah = parseFloat($(el).val());
                if (isNaN(temp_jumlah)) temp_jumlah = 0;

                var temp_konversi = 0;
                var temp_kode_satuan = $(el).parents('#rsp_id').find('.text').text();
                temp_kode_satuan = temp_kode_satuan.split(' ')[0];
                $(el).parents('#rsp_id').find('ul li').each(function(index2, el2) {
                    var kode = $(el2).find('a').attr('kode');
                    if (kode == temp_kode_satuan) {
                        temp_konversi = parseFloat($(el2).find('a').attr('konversi'));
                    }
                });

                temp_jumlah *= temp_konversi;
                jumlah += temp_jumlah;
            });
            // console.log(jumlah);

            var kode = $(this).parents('tr').first().data('id');
            // var satuan = $('#satuan-'+kode).val();

            if (isNaN(jumlah)) jumlah = 0;

            // var konversi = parseFloat(data.satuan.konversi);
            var harga = parseFloat($('#harga-'+kode).val());
            // var v_harga = harga * satuan.konversi;
            // var v_subtotal = v_harga * jumlah;
            var v_subtotal = harga * jumlah;

            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            // tr.find('#inputHargaPerSatuan').val(v_harga.toLocaleString());
            tr.find('#inputSubTotal').val(parseFloat(v_subtotal).toLocaleString());
            // tr.find('#HargaPerSatuan').val(v_harga);
            // tr.find('#SubTotal').val(v_subtotal.toFixed(3));

            // $('#harga-'+kode).val(harga);
            $('#jumlah-'+kode).val(jumlah);
            $('#subtotal-'+kode).val(parseFloat(v_subtotal));

            var harga_total = 0;
            $('.subtotal').each(function(index, el) {
                var tmp = parseFloat($(el).val());
                if (isNaN(tmp)) tmp = 0;
                harga_total += tmp;
            });

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('input[name="harga_total"]').val(harga_total);
        });

        /*$(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();

            harga_total = 0;
            var satuan = $(this).val();
            var kode   = $(this).parents('tr').data('id');
            var jumlah = parseFloat($('#jumlah-'+kode).val());
            var transaksi = {{ $transaksi_penjualan->id }};

            if (isNaN(jumlah)) jumlah = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+transaksi+'/retur/'+kode+'/'+satuan+'/konversi/json/';
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            // console.log(url);

            $('#satuan-'+kode).val(satuan);

            $.get(url, function(data) {
                // console.log(data);
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#HargaPerSatuan').val(0);
                    tr.find('#SubTotal').val(0);
                } else {
                    var konversi = parseFloat(data.satuan.konversi);
                    var harga = parseFloat($('#harga-'+kode).val());
                    var v_harga = harga * konversi;
                    var v_subtotal = v_harga * jumlah;

                    tr.find('#inputHargaPerSatuan').val(v_harga.toLocaleString());
                    tr.find('#inputSubTotal').val(parseFloat(v_subtotal).toLocaleString());
                    // tr.find('#HargaPerSatuan').val(v_harga);
                    // tr.find('#SubTotal').val(v_subtotal.toFixed(3));

                    // $('#harga-'+kode).val(harga);
                    $('#subtotal-'+kode).val(parseFloat(v_subtotal));

                    var harga_total = 0;
                    $('.subtotal').each(function(index, el) {
                        var tmp = parseFloat($(el).val());
                        if (isNaN(tmp)) tmp = 0;
                        harga_total += tmp;
                    });

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                }
            });
        });*/

        $(document).on('click', '#pilihSatuan li', function(event) {
            event.preventDefault();

            harga_total = 0;
            var satuan = {
                id: parseInt($(this).find('a').attr('id')),
                kode: $(this).find('a').attr('kode'),
                konversi: parseInt($(this).find('a').attr('konversi'))
            };
            var button = $(this).parents('#pilihSatuan').find('button').find('.text');
            // var satuan = $(this).val();
            var kode   = $(this).parents('tr').data('id');
            var jumlah = parseFloat($('#jumlah-'+kode).val());
            var transaksi = {{ $transaksi_penjualan->id }};

            if (isNaN(jumlah)) jumlah = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+transaksi+'/retur/'+kode+'/'+satuan.id+'/konversi/json/';
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            console.log(url);

            $('#satuan-'+kode).val(satuan.id);

            $.get(url, function(data) {
                // console.log(data);
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#HargaPerSatuan').val(0);
                    tr.find('#SubTotal').val(0);
                } else {
                    var konversi = parseFloat(data.satuan.konversi);
                    var harga = parseFloat($('#harga-'+kode).val());
                    var v_harga = harga * konversi;
                    var v_subtotal = v_harga * jumlah;

                    tr.find('#inputHargaPerSatuan').val(v_harga.toLocaleString());
                    tr.find('#inputSubTotal').val(parseFloat(v_subtotal).toLocaleString());
                    // tr.find('#HargaPerSatuan').val(v_harga);
                    // tr.find('#SubTotal').val(v_subtotal.toFixed(3));

                    // $('#harga-'+kode).val(harga);
                    $('#subtotal-'+kode).val(parseFloat(v_subtotal));

                    var harga_total = 0;
                    $('.subtotal').each(function(index, el) {
                        var tmp = parseFloat($(el).val());
                        if (isNaN(tmp)) tmp = 0;
                        harga_total += tmp;
                    });

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);

                    button.text(satuan.kode+' ');
                }
            });
        });

        $(document).on('change', '#checkRusak', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            var $inputRusak = $(this).parents('#rsp_id').find('#inputRusak');
            if (checked) {
                $inputRusak.css('background-color', '#fff');
            } else {
                $inputRusak.css('background-color', '#eee');
            }
        });

        var temp_jumlah_in = 0;
        $(document).on('click', '#inputJumlahItemIn', function(event) {
            event.preventDefault();
            temp_jumlah_in = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItemIn', function(event) {
            event.preventDefault();
            $(this).val(temp_jumlah_in);
        });

        $(document).on('keyup', '#inputJumlahItemIn', function(event) {
            event.preventDefault();
            var kode   = $(this).parents('tr').first().data('id');
            var sub_total = parseFloat($('#subtotalIn-'+kode).val());

            var jumlah = $(this).val();
            temp_jumlah_in = jumlah;

            if (jumlah === '') jumlah = 0;

            var kode   = $(this).parents('tr').first().data('id');
            var satuan = $('#satuanIn-'+kode).val();
            var url    = "{{ url('retur-pembelian') }}"+'/'+kode+'/konversi/json/'+satuan;
            var tr     = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');
            $('#jumlahIn-'+kode).val(jumlah);
            
            var rega = parseFloat(sub_total / jumlah).toFixed(3);
            $(this).parents('tr').first().find('input[name="inputHargaPerSatuanIn"]').val('Rp'+parseFloat(rega).toLocaleString(undefined, {minimumFractionDigits: 3}));
                // $.get(url, function(data) {
                //  if (data.satuan === null) {
                //      // tr.find('#inputHargaPerSatuan').val(0);
                //      // tr.find('#inputSubTotal').val(0);
                //      // tr.find('#HargaPerSatuan').val(0);
                //      // tr.find('#SubTotal').val(0);
                //  } else {
                //      var transaksi = {{ $transaksi_penjualan->id }}
                //      var konversi = data.satuan.konversi;
                //      var count = parseFloat(jumlah) * parseFloat(data.satuan.konversi); 
                //      var url_ = "{{ url('retur-pembelian') }}"+'/'+kode+'/count/'+count+'/json/'+transaksi;
                //      console.log(url_);

                //      $.get(url_, function(data) {
                //          var harga_ = parseFloat(data.harga_satuan).toFixed(3) * 1.1;    
                //          var harga = parseFloat(harga_).toFixed(3);  
                //          var harga_satuan = parseFloat(harga) * parseFloat(konversi);
                //          var subtotal_ = parseFloat(harga) * parseFloat(data.jumlah);
                //          var subtotal = parseFloat(subtotal_).toFixed(3);    

                //          tr.find('#inputHargaPerSatuan').val(harga_satuan.toLocaleString(undefined, {minimumFractionDigits: 3}));
                //          tr.find('#inputSubTotal').val(parseFloat(subtotal).toLocaleString(undefined, {minimumFractionDigits: 3}));
                //          tr.find('#HargaPerSatuan').val(harga);
                //          tr.find('#SubTotal').val(parseFloat(subtotal).toFixed(3));

                //          $('#harga-'+kode).val(harga);
                //          $('#subtotal-'+kode).val(parseFloat(subtotal).toFixed(3));

                //          $('.subtotal').each(function(index, el) {
                //              var tmp = parseFloat($(el).val());
                //              if (isNaN(tmp)) tmp = 0;
                //              harga_total += tmp;
                //          });
                            
                //          $('input[name="inputHargaTotal"]').val(parseFloat(harga_total.toFixed(3)).toLocaleString(undefined, {minimumFractionDigits: 3}));
                //          $('input[name="harga_total"]').val(parseFloat(harga_total.toFixed(3)));
                //      });
                //  }
                // });
        });

        $(document).on('keyup', '#inputKeterangan', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            var $jumlah = $(this).parent().prev().find('input');

            var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
            var keterangan = $(this).val();
            
            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('input[id="keterangan-' + id + '"]').remove();
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="keterangan[]" id="keterangan-' + id + '" value="' + keterangan + '" />');
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var subtotal     = $('#subtotal-'+kode).val();
            var harga_total  = $('input[name="harga_total"]').val();
            // parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            // console.log(kode);

            harga_total -= subtotal;
            // kode_ = 'item-'+kode;
            deleteMe(selected_items, kode);

            $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 3}));

            $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(3));

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });

        $(document).on('click', '#btnUang', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            $(this).find('i').show('fast');
            $('#metodePembayaranButtonGroup').show();
            $('input[name="status"]').val('uang');
            if($('#btnSama').hasClass('btn-success')){
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
                $('#submit').prop('disabled', true);
            }else if($('#btnLain').hasClass('btn-success')){
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
                $('#submit').prop('disabled', true);
            }
        });

        $(document).on('click', '#btnSama', function(event) {
            event.preventDefault();
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            $(this).find('i').show('fast');
            uangClose();
            $('input[name="status"]').val('sama');
            $('#submit').prop('disabled', false);

            if($('#btnUang').hasClass('btn-success')){
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
            }else if($('#btnLain').hasClass('btn-success')){
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
            }           
        });

        $(document).on('click', '#btnLain', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            $(this).find('i').show('fast');
            uangClose();
            $('input[name="status"]').val('lain');
            $('#keranjangLain').show();

            if($('#btnSama').hasClass('btn-success')){
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
                $('#submit').prop('disabled', true);
            }else if($('#btnUang').hasClass('btn-success')){
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
                $('#submit').prop('disabled', true);
            }
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_tansfer"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKredit', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKreditContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKreditContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_kredit"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_kredit"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTitipan', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                $(this).find('i').show('fast');
                $('#inputTitipanContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTitipanContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_titipan"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnPiutang', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-dark');
                $(this).find('i').show('fast');
                $('#inputPiutangContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-dark')) {
                $(this).removeClass('btn-dark');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputPiutangContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_hutang"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnPiutangSuplier', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTitipanContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTitipanContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_titipan"]').val('');
                    $(this).find('input').val('');
                    //updateHargaOnKeyup();
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();
            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            //updateHargaOnKeyup();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();
            var bank_id = $(this).val();
            // console.log(bank_id);
            $('#formSimpanContainer').find('input[name="bank_id"]').val(bank_id);
            //updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();
            var no_tansfer = $(this).val();
            $('#formSimpanContainer').find('input[name="no_tansfer"]').val(no_tansfer);
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();
            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            total_uang();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();
            var nomor_cek = $(this).val();
            $('#formSimpanContainer').find('input[name="no_cek"]').val(nomor_cek);
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();
            var nominal_cek = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            total_uang();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();
            var nomor = $(this).val();
            $('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();
            var nominal = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal)) nominal = 0;
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
            total_uang();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });

        $(document).on('keyup', '#inputNomorKredit', function(event) {
            event.preventDefault();
            var nomor_kredit = $(this).val();
            $('#formSimpanContainer').find('input[name="no_kredit"]').val(nomor_kredit);
        });

        $(document).on('keyup', '#inputNominalKredit', function(event) {
            event.preventDefault();
            var nominal_kredit = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kredit)) nominal_kredit = 0;
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(nominal_kredit);
            total_uang();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });

        $(document).on('keyup', '#inputNominalTitipan', function(event) {
            event.preventDefault();
            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal)) nominal = 0;
            $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(nominal);
            total_uang();
            $('#submit').prop('disabled', isBtnSimpanDisabled());           
        });
        
        $(document).on('keyup', '.inputSubTotalIn', function(event) {
            event.preventDefault();

            var nominal_t = $(this).val().split(',');
            var nominal_0 = parseFloat(nominal_t[0].replace(/\D/g, ''), 10);
            var nominal = parseFloat(nominal_0 +'.' + nominal_t[1]);
            $(this).next().val(nominal);
            
            var nilais = $(this).val().split(',');
            var nilai = 0;
            if(nilais.length > 1){
                nilai = $(this).val();
                if(nilais[0].length < 1){
                    nilais[0] = 0;
                    $(this).val(nilais[0] + ',' + nilais[1]);
                }
                else if(nilais[1].length > 3){
                    nilai = $(this).val().slice(0,-1);
                    $(this).val(nilai);
                
                }else if(nilais[1].length < 3){
                    nilai = parseFloat(nilai.replace(',', '.'));
                    nilai = nilai.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
                    nilai = nilai.replace('.', '');
                    $(this).val(nilai);
                }
            }else{
                nilai = $(this).val();
                var bel_koma = nilai.substr(nilai.length -3);
                var depan_koma = nilai.substr(0, nilai.length -3);

                $(this).val(depan_koma + ',' + bel_koma);
            }
            var nilai_h = parseFloat(nilai.replace(',', '.'));
            var nilai_v = nilai_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
            
            var kode = $(this).parents('tr').first().attr('data-id');;
            $(this).parents('tr').first().find('input[name="SubTotalIn"]').val('Rp'+nilai_v);
            var jumlah = $(this).parents('tr').first().find('#inputJumlahItemIn').val();
            $('#subtotalIn-'+kode).val(nilai_h);
            var rega = parseFloat(nilai_h / jumlah).toFixed(3);
            $(this).parents('tr').first().find('input[name="inputHargaPerSatuanIn"]').val('Rp'+parseFloat(rega).toLocaleString(undefined, {minimumFractionDigits: 3}));
            cek_lain(0, 1);
            $('#submit').prop('disabled', isBtnSimpanDisabled());
        });

        $(document).on('click', '#removeIn', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');

            deleteMe(selected_retur, kode);

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=In-'+kode+']').remove();
        });
    </script>
@endsection
