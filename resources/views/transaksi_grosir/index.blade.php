@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Transaksi Penjualan</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnTambahVIP {
            /*margin-right: 0;*/
        }
        #btnRetur,
        #btnCanRetur,
        #btnTanggal,
        #btnTambah,
        #btnTambahVIP,
        #btnDetail {
            margin-bottom: 0;
        }
        #btnRetur,
        #btnCahRetur {
            margin-right: 0;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Daftar Transaksi Penjualan</h2>
                @if (in_array(Auth::user()->level_id, [1, 2]))
                <a href="{{ url('transaksi-grosir-vip/create') }}" id="btnTambahVIP" class="btn btn-sm btn-dark pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan (Prioritas)">
                    <i class="fa fa-star"></i>
                </a>
                @endif
                @if(Auth::user()->level_id !=4 )
                    <button id="btnTambah" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan">
                        <i class="fa fa-plus"></i>
                    </button>
                @endif
                {{-- <a href="{{ url('transaksi-grosir/create') }}" id="btnTambah" class="btn btn-sm btn-success pull-right">
                    <i class="fa fa-plus"></i> Tambah
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('transaksi-grosir/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="created_at">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableTransaksiGrosir" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="created_at">No</th>
                                <th class="sorting" field="created_at">Tanggal & Waktu</th>
                                <th class="sorting" field="kode_transaksi">Kode Transaksi</th>
                                <th class="sorting" field="pelanggan_nama">Pelanggan</th>
                                <th class="sorting" field="status">Status</th>
                                <th class="sorting" field="harga_total">Total</th>
                                <th class="sorting" field="created_at" style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
        </form>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_eceran')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Eceran gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Grosir berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('transaksi-grosir/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var transaksi_penjualan = data[i];
                        var buttons = transaksi_penjualan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Transaksi Penjualan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.retur != null) {
                            td_buttons += `
                                <a href="${buttons.retur.url}" class="btn btn-xs btn-success" id="btnRetur" data-toggle="tooltip" data-placement="top" title="Retur Penjualan">
                                    <i class="fa fa-mail-reply"></i>
                                </a>
                            `;
                        }
                        if (buttons.aktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnCanRetur" data-toggle="tooltip" data-placement="top" title="Aktifkan Retur Penjualan">
                                    <i class="fa fa-check"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${transaksi_penjualan.id}" data-id="${i + 1}">
                                    <td>${nomor}</td>
                                    <td>${ymd2dmy(transaksi_penjualan.created_at.split(' ')[0])} ${transaksi_penjualan.created_at.split(' ')[1]}</td>
                                    <td>${transaksi_penjualan.kode_transaksi}</td>
                                    <td>${transaksi_penjualan.pelanggan_nama}</td>
                                    <td>${transaksi_penjualan.mdt_status}</td>
                                    <td class="text-right">${transaksi_penjualan.mdt_harga_total}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(window).on('load', function(event) {
            // var url = "{{ url('sidebar/setoran-buka/json') }}";
            // $.get(url, function(data) {
            //     // console.log(data);
            //     var setoran_buka = data.setoran_buka;
            //     var cash_drawer = data.cash_drawer;
            //     var user = data.user;
            //     // console.log(setoran_buka);
            //     if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
            //         if ([1, 2].indexOf(user.level_id) < 0) {
            //             $('#btnTambah').prop('disabled', true);
            //             // $('#btnTambah').find('a').addClass('link-mati');
            //         }
            //     }
            // });
        });

        $(document).on('click', '#btnTambah', function(event) {
            event.preventDefault();

            var url = '{{ url('transaksi-grosir/create') }}';
            var ww = window.open(url, '_self');
        });

        $(document).on('click', '#btnCanRetur', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan Retur?',
                text: '\"' + nama_item + '\", retur akan diaktifkan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-grosir") }}' + '/' + id + '/retur/aktif');
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

    </script>
@endsection
