@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Transaksi Penjualan</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnTambahVIP {
            /*margin-right: 0;*/
        }
        #btnCanRetur,
        #btnTanggal,
        #btnTambah,
        #btnTambahVIP,
        #btnDetail {
            margin-bottom: 0;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Daftar Transaksi Penjualan</h2>
                @if (in_array(Auth::user()->level_id, [1, 2]))
                <a href="{{ url('transaksi-grosir-vip/create') }}" id="btnTambahVIP" class="btn btn-sm btn-dark pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan (Prioritas)">
                    <i class="fa fa-star"></i>
                </a>
                @endif
                @if(Auth::user()->level_id !=4 )
                    <button id="btnTambah" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan">
                        <i class="fa fa-plus"></i>
                    </button>
                @endif
                {{-- <a href="{{ url('transaksi-grosir/create') }}" id="btnTambah" class="btn btn-sm btn-success pull-right">
                    <i class="fa fa-plus"></i> Tambah
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableTransaksiGrosir">
                    <thead>
                        <tr>
                            <th style="display: none;">No.</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Transaksi</th>
                            <th>Pelanggan</th>
                            <th>Status</th>
                            <th>Total</th>
                            {{-- <th>Operator</th> --}}
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transaksis as $i => $transaksi)
                        <tr  kode="{{ $j = $i }}" data-id="{{ $i++ }}" id="{{ $transaksi->id }}">
                            <td style="display: none;">{{ $i++ }}</td>
                            <td class="tengah-vh">{{ $transaksi->created_at->format('d-m-Y H:i:s') }}</td>
                            <td class="tengah-vh">{{ $transaksi->kode_transaksi }}</td>
                            @if ($transaksi->pelanggan_id != null)
                            <td class="tengah-v">{{ ucwords($transaksi->pelanggan->nama) }}</td>
                            @else
                            <td class="tengah-v">-</td>
                            @endif
                            <td class="tengah-v">{{ ucfirst($transaksi->status) }}</td>
                            <td class="tengah-v text-right">{{ \App\Util::duit0($transaksi->harga_total) }}</td>
                            {{-- <td class="tengah-v">{{ $transaksi->user->nama }}</td> --}}
                            <td>
                                <a href="{{ url('transaksi-grosir/'.$transaksi->id) }}" btn class="btn btn-xs btn-info" id="btnDetail" title="Detail Transaksi Penjualan" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($transaksi->can_retur == 0)
                                    @if($rule->syarat != 0 && $rule->syarat != null)
                                        @if($today <= $batas_retur[$j] && $transaksi->retur_show)
                                            <a href="{{ url('transaksi-grosir/'.$transaksi->id.'/retur/create') }}" btn class="btn btn-xs btn-success" id="btnDetail" title="Retur Penjualan" data-toggle="tooltip" data-placement="top">
                                                <i class="fa fa-mail-reply"></i>
                                            </a>
                                        @elseif($transaksi->aktivasi_show)
                                            <a btn class="btn btn-xs btn-danger" id="btnCanRetur" title="Aktifkan Retur Penjualan" data-toggle="tooltip" data-placement="top">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        @endif
                                    @endif
                                @else
                                    @if($transaksi->retur_show)
                                        <a href="{{ url('transaksi-grosir/'.$transaksi->id.'/retur/create') }}" btn class="btn btn-xs btn-success" id="btnDetail" title="Retur Penjualan" data-toggle="tooltip" data-placement="top">
                                            <i class="fa fa-mail-reply"></i>
                                        </a>
                                    @endif
                                @endif
                                @if($transaksi->jatuh_tempo != NULL && ($transaksi->jumlah_bayar < $transaksi->nego_total))
                                    <!-- <a href="{{ url('transaksi-grosir/tanggal/'.$transaksi->id) }}" class="btn btn-xs btn-warning" title="Ubah Tanggal Jatuh Tempo" id="btnTanggal">
                                        <i class="fa fa-calendar"></i>
                                    </a> -->
                                @endif
                                {{-- <a href="{{ url('transaksi-grosir/'.$transaksi->id.'/retur') }}" class="btn btn-xs btn-warning" id="btnRetur">
                                    <i class="fa fa-sign-out"></i> Retur
                                </a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
        </form>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_eceran')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Eceran gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Grosir berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Grosir berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Grosir gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script>
        $('#tableTransaksiGrosir').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(window).on('load', function(event) {
            // var url = "{{ url('sidebar/setoran-buka/json') }}";
            // $.get(url, function(data) {
            //     // console.log(data);
            //     var setoran_buka = data.setoran_buka;
            //     var cash_drawer = data.cash_drawer;
            //     var user = data.user;
            //     // console.log(setoran_buka);
            //     if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
            //         if ([1, 2].indexOf(user.level_id) < 0) {
            //             $('#btnTambah').prop('disabled', true);
            //             // $('#btnTambah').find('a').addClass('link-mati');
            //         }
            //     }
            // });
        });

        $(document).on('click', '#btnTambah', function(event) {
            event.preventDefault();

            var url = '{{ url('transaksi-grosir/create') }}';
            var ww = window.open(url, '_self');
        });

        $(document).on('click', '#btnCanRetur', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan Retur?',
                text: '\"' + nama_item + '\", retur akan diaktifkan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-grosir") }}' + '/' + id + '/retur/aktif');
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

    </script>
@endsection
