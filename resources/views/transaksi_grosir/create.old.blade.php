@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Transaksi Grosir</title>
@endsection

@section('style')
	<style type="text/css" media="screen">
		#btnKembali {
			margin-right: 0;
		}
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
		#metodePembayaranButtonGroup {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-9">
						<h2 id="formSimpanTitle">Tambah Transaksi Penjualan Grosir</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					<div class="col-md-3 pull-right">
						<a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Pelanggan</label>
						<select name="pelanggan_id" class="select2_single form-control">
							<option value="">Pilih Pelanggan</option>
							@foreach ($pelanggans as $pelanggan)
							<option value="{{ $pelanggan->id }}">{{ $pelanggan->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Item</label>
						<select name="item_id" class="select2_single form-control">
							<option value="">Pilih Item</option>
							@foreach($items as $item)
							<option value="{{$item->kode}}">{{$item->nama}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Informasi Item</h2>
				<a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Jumlah (pcs)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Belanja</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<table class="table" id="tabelKeranjang">
							<thead>
								<tr>
									<th></th>
									<th style="text-align: left;">Item</th>
									<th style="text-align: left;">Jumlah</th>
									<th style="text-align: left;" style="width: 10%;">Satuan</th>
									<th style="text-align: left;">Harga per satuan (Rp)</th>
									<th style="text-align: left;">Sub Total (Rp)</th>
									<th style="text-align: left;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12" id="pilihanPotonganContainer">
								<label class="control-label">Pilihan Potongan</label>
								<div id="pilihanPotonganButtonGroup" class="btn-group btn-group-justified" role="group">
									<div class="btn-group" role="group">
										<button type="button" id="btnPotonganPersen" class="btn btn-default">Potongan Persen</button>
										<input type="hidden" id="checkPotonganPersen" value="false" />
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnPotonganTunai" class="btn btn-default">Potongan Tunai</button>
										<input type="hidden" id="checkPotonganTunai" value="false" />
									</div>
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Metode Pembayaran</label>
								<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
									<div class="btn-group" role="group">
										<button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnDebit" class="btn btn-default"><i class="fa"></i> Debit</button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnCek" class="btn btn-default"><i class="fa"></i> Cek</button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnBG" class="btn btn-default"><i class="fa"></i> BG</button>
									</div>
								</div>
							</div>
							<div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<label class="control-label">Nominal Tunai</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
								</div>
							</div>
							<div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
										<label class="control-label">Pilih Bank</label>
										<select class="form-control select2_single" name="bank_id">
											<option value="">Pilih Bank</option>
											@foreach ($banks as $bank)
											<option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nomor Transfer</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
										</div>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Transfer</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" name="inputNominalTransfrer" id="inputNominalTransfrer" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
							<div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-xs-6 col-md-6">
										<label class="control-label">Nomor Cek</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
										</div>
									</div>
									<div class="col-xs-6 col-md-6">
										<label class="control-label">Nominal Cek</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />
										</div>
									</div>
								</div>
							</div>
							<div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-xs-6 col-md-6">
										<label class="control-label">Nomor BG</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
										</div>
									</div>
									<div class="col-xs-6 col-md-6">
										<label class="control-label">Nominal BG</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />
									<input type="hidden" name="hiddenHargaTotal" id="hiddenHargaTotal" />
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Nego Harga Total (Rp)</label>
								<div class="input-group">
									<div class="input-group-addon"><input type="checkbox" name="checkNegoTotal" id="checkNegoTotal" /></div>
									<input type="text" name="inputNegoTotal" id="inputNegoTotal" class="form-control" readonly="readonly" />
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control" />
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total + Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control" readonly="readonly" />
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Jumlah Bayar</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control" readonly="readonly" />
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Kembali</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control" readonly="readonly" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div id="formSimpanContainer">
									<form id="form-simpan" action="{{ url('transaksi-grosir') }}" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="kode_transaksi" value="" />
										<input type="hidden" name="pelanggan" value="" />
										<input type="hidden" name="potongan" value="" />

										<input type="hidden" name="harga_total" />
										<input type="hidden" name="jumlah_bayar" />
										<input type="hidden" name="kembali" />
										
										<input type="hidden" name="nominal_tunai" />
										
										<input type="hidden" name="no_transfer" />
										<input type="hidden" name="nominal_transfer" />
										
										<input type="hidden" name="no_cek" />
										<input type="hidden" name="nominal_cek" />
										
										<input type="hidden" name="no_bg" />
										<input type="hidden" name="nominal_bg" />

										<div id="append-section"></div>
										<div class="clearfix">
											<div class="form-group pull-left">
												<button type="submit" class="btn btn-success" style="width: 100px;"><i class="fa fa-check"></i> OK</button>	
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		var select_satuan = '<select name="satuan" class="form-control input-sm">@foreach($satuans as $satuan)<option value="{{ $satuan->id }}">{{ $satuan->kode }}</option>@endforeach</select>';

		// function updateHargaTotal() {
		// 	var harga_total = 0;

		// 	$('.subtotal').each(function(index, el) {
		// 		var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
		// 		if (isNaN(tmp)) tmp = 0;
		// 		harga_total += tmp;
		// 	});

		// 	$('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
		// 	$('input[name="harga_total"]').val(harga_total);
		// 	$('#hiddenHargaTotal').val(harga_total);

		// 	var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
		// 	var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
		// 	var kembali      = jumlah_bayar - harga_total;

		// 	if (kembali < 0) kembali = 0;

		// 	$('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
		// 	$('input[name="kembali"]').val(kembali);
		// }

		function updateHargaOnKeyup() {
			var $harga_total = $('#inputHargaTotal');
			var $ongkos_kirim = $('#inputOngkosKirim');
			var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
			var $jumlah_bayar = $('#inputJumlahBayar');
			var $kembali = $('#inputTotalKembali');

			var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
			var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();
			var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
			var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
			var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();

			nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
			nominal_transfer = parseInt(nominal_transfer.replace(/\D/g, ''), 10);
			nominal_cek = parseInt(nominal_cek.replace(/\D/g, ''), 10);
			nominal_bg = parseInt(nominal_bg.replace(/\D/g, ''), 10);
			ongkos_kirim = parseInt(ongkos_kirim.replace(/\D/g, ''), 10);

			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			if (isNaN(nominal_transfer)) nominal_transfer = 0;
			if (isNaN(nominal_cek)) nominal_cek = 0;
			if (isNaN(nominal_bg)) nominal_bg = 0;
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

			// var harga_total = 0;
			// $('input[name="inputSubTotal"]').each(function(index, el) {
			// 	var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
			// 	if (isNaN(subtotal)) subtotal = 0;
			// 	harga_total += subtotal;
			// });
			
			// var harga_total_plus_ongkos_kirim = harga_total;
			// if (!isNaN(parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10)))
			// 	harga_total_plus_ongkos_kirim += parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10);
			// var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg;
			// var kembali = harga_total_plus_ongkos_kirim - jumlah_bayar;

			// if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
			// if (isNaN(harga_total)) harga_total = 0;
			// if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			// if (isNaN(kembali)) kembali = 0;
			// if (kembali < 0) kembali = 0;

			// if (ongkos_kirim == 0) $ongkos_kirim.val('');
			// // else $ongkos_kirim.val(ongkos_kirim.toLocaleString(undefined, {minimumFractionDigits: 2}));
			// else $ongkos_kirim.val(ongkos_kirim);
			// $harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString(undefined, {minimumFractionDigits: 2}));
			// $harga_total.val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
			// $jumlah_bayar.val(jumlah_bayar.toLocaleString(undefined, {minimumFractionDigits: 2}));
			// $kembali.val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));

			// $('input[name="ongkos_kirim"]').val(ongkos_kirim);
			// $('input[name="harga_total"]').val(harga_total);
			// $('input[name="jumlah_bayar"]').val(jumlah_bayar);
			// $('input[name="kembali"]').val(kembali);

			var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
			var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg;
			var kembali = jumlah_bayar - harga_total;

			if (isNaN(harga_total)) harga_total = 0;
			if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

			if (isNaN(harga_total)) harga_total = 0;
			if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			if (isNaN(kembali)) kembali = 0;
			if (kembali < 0) kembali = 0;

			$harga_total.val(harga_total.toLocaleString());
			$jumlah_bayar.val(jumlah_bayar.toLocaleString());
			$kembali.val(kembali.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
			$('input[name="kembali"]').val(kembali);
		}

		function cariItem(kode, url, tr) {
			$.get(url, function(data) {
				var nama      = data.item.nama;
				var bonus_id  = data.item.bonus_id;
				var harga = data.harga.harga;
				var stoktotal = data.stok.stok;
				harga = parseInt(harga.replace(/\D/g, ''), 10) / 100;

				if (tr === undefined) {
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="0" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="1" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="" />');
					$("#form-simpan").find('#append-section').append('<input type="hidden" id="hpp-'+kode+'" class="hpp" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal[]" id="subtotal-'+kode+'" value="" />');

					$('#tabelKeranjang').find('thead').children().children().last().text('Nego Harga (Rp)');
					$('#tabelKeranjang').find('tbody').append(
						'<tr data-id="'+kode+'">'+
							'<td>'+
								'<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
							'</td>'+
							'<td style="padding-top: 13px">'+nama+'</td>'+
							'<td>'+
								'<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm" value="1" />'+
							'</td>'+
							'<td>'+select_satuan+'</td>'+
							'<td>'+
								'<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
							'</td>'+
							'<td>'+
								'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka inputSubTotal" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
							'</td>'+
							'<td>'+
								'<div class="form-group">'+
									'<div class="input-group">'+
										'<div class="input-group-addon">'+
											'<input type="checkbox" id="checkNego" />'+
										'</div>'+
										'<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" readonly="readonly" />'+
									'</div>'+
								'</div>'+
							'</td>'+
						'</tr>');
				} 

				$('#tabelInfo').find('tbody').children('tr').children().first().text(nama);
				$('#tabelInfo').find('tbody').children('tr').children().last().text(stoktotal);

				var $harga_total = $('#inputHargaTotal');
				var $jumlah_bayar = $('#inputJumlahBayar');
				var $kembali = $('#inputTotalKembali');

				var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
				var jumlah_bayar = parseInt($jumlah_bayar.val().replace(/\D/g, ''), 10);
				var kembali = jumlah_bayar - harga_total;
				kembali = kembali < 0 ? 0 : kembali;

				$harga_total.val((harga_total + harga).toLocaleString());
				$kembali.val(kembali.toLocaleString());

				$('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + harga);
				$('#formSimpanContainer').find('input[name="jumlah_bayar"]').val(jumlah_bayar);
				$('#formSimpanContainer').find('input[name="kembali"]').val(kembali);
			});
		}

		$(document).ready(function() {
			var url = "{{ url('transaksi-grosir/create') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$('#pilihanPotonganContainer').hide();
			$('#inputTunaiContainer').hide();
			$('#inputTransferBankContainer').hide();
			$('#inputTransferBankContainer').find('input').val('');
			$('#inputCekContainer').hide();
			$('#inputCekContainer').find('input').val('');
			$('#inputBGContainer').hide();
			$('#inputBGContainer').find('input').val('');

			$(".select2_single").select2({
				width: '100%'
				// allowClear: true
			});

			$('#inputHargaTotal').val(0);
			$('#inputNegoTotal').val(0);
			$('#inputJumlahBayar').val(0);
			$('#inputTotalKembali').val(0);

			// $('select[name="bank_id"]').next('span').css({
			// 	'width': '100%',
			// 	'margin-bottom': '10px'
			// });
		});

		$(window).on('load', function(event) {

			var url = "{{ url('transaksi-grosir/last/json') }}";
			var tanggal = printTanggalSekarang('dd/mm/yyyy');			

			$.get(url, function(data) {
				if (data.transaksi_penjualan === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/TRAJ/' + tanggal;
				} else {
					var kode_transaksi = data.transaksi_penjualan.kode_transaksi;
					var dd_transaksi = kode_transaksi.split('/')[2];
					var mm_transaksi = kode_transaksi.split('/')[3];
					var yyyy_transaksi = kode_transaksi.split('/')[4];
					var tanggal_transaksi = dd_transaksi + '/' + mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/TRAJ/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/TRAJ/' + tanggal_transaksi;
					}
				}

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		// var keyupFromScanner = false;
		$(document).scannerDetection({
			avgTimeByChar: 40,
			onComplete: function(code, qty) {
				console.log('Kode: ' + code, qty);
			},
			onError: function(error) {
				// console.log('Barcode: ' + error);
				var kode = error;
				var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
				var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

				var terpilih = false;
				$('input[name="item_kode[]"]').each(function(index, el) {
					if ($(el).val() == kode) {
						terpilih = true;
					}
				});

				if (terpilih) {
					// keyupFromScanner = true;
					var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val());
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val(jumlah_awal + 1);
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('keyup');
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('blur');
				} else {
					cariItem(kode, url, tr);
				}
			}
		});

		$(document).on('change', 'select[name="pelanggan_id"]', function(event) {
			event.preventDefault();
			
			var id = $(this).val();

			(id !== '') ? $('#pilihanPotonganContainer').show('fast') : $('#pilihanPotonganContainer').hide('fast')

			$('input[name="pelanggan"]').val(id);
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();
			
			var kode = $(this).val();
			var url  = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
			var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

			cariItem(kode, url, tr);
		});

		$(document).on('change', '#checkNego', function(event) {
			event.preventDefault();

			var checked     = $(this).prop('checked');
			var id          = $(this).parents('tr').first().data('id');
			var harga_total = 0;

			if (checked) {
				$(this).parent().next().prop('readonly', false).focus();

				var nego = $(this).parents('tr[data-id="'+id+'"]').find('#inputNego').val();
				nego = parseInt(nego.replace(/\D/g, ''), 10);
				$('#subtotal-'+id).val(nego);

				// updateHargaTotal();
				updateHargaOnKeyup();
			} else {
				$(this).parent().next().prop('readonly', true);

				var subtotal    = $(this).parents('tr[data-id="'+id+'"]').find('#inputSubTotal').val();
				subtotal = parseInt(subtotal.replace(/\D/g, ''), 10);
				$('#subtotal-'+id).val(subtotal);

				// updateHargaTotal();
				updateHargaOnKeyup();
			}
		});

		$(document).on('change', '#checkNegoTotal', function(event) {
			event.preventDefault();
			
			var checked = $(this).prop('checked');

			(checked) ? $(this).parent().next().prop('readonly', false) : $(this).parent().next().prop('readonly', 'true');
		});

		var temp_jumlah = 0;
		$(document).on('click', '#inputJumlahItem', function(event) {
			event.preventDefault();
			temp_jumlah = $(this).val();
			$(this).val('');
		});

		$(document).on('blur', '#inputJumlahItem', function(event) {
			event.preventDefault();
			$(this).val(temp_jumlah);
		});

		$(document).on('keyup', '#inputJumlahItem', function(event) {
			event.preventDefault();
			
			var jumlah = $(this).val();

			if (jumlah === '') jumlah = 0;

			var kode   = $(this).parents('tr').first().data('id');
			var satuan = $('#satuan-'+kode).val();
			var url    = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
			var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

			$('#jumlah-'+kode).val(jumlah);

			$.get(url, function(data) {
				if (data.harga === null) {
					tr.find('#inputHargaPerSatuan').val(0);
					tr.find('#inputSubTotal').val(0);
				} else {
					var harga       = data.harga.harga;
					var subtotal    = parseInt(harga) * parseInt(jumlah);
					var harga_total = 0;

					var konversi    = parseInt(data.satuans.konversi) * parseInt(jumlah);

					tr.find('#inputHargaPerSatuan').val(harga.toLocaleString());
					tr.find('#inputSubTotal').val(subtotal.toLocaleString());

					$('#harga-'+kode).val(parseInt(harga));
					$('#subtotal-'+kode).val(subtotal);

					// updateHargaTotal();
					updateHargaOnKeyup();

					var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';

					$.get(hpp_url, function(data) {
						var arr   = [];
						var index = 0;
						var hpp   = 0;

						for (var i = 0; i < data.stoks.length; i++) {
							arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
						}

						for (var j = 0; j < parseInt(konversi); j++) {
							arr[index][0] -= 1;
							hpp += parseInt(arr[index][1]);

							if (arr[index][0] === 0) index += 1;		
						}

						$('#hpp-'+kode).val(hpp);
					});
				}
			});
		});

		$(document).on('change', 'select[name="satuan"]', function(event) {
			event.preventDefault();

			var satuan = $(this).val();
			var kode   = $(this).parents('tr').data('id');
			var jumlah = $(this).parent().prev().children('input').val();

			if (jumlah === '') jumlah = 0;

			var url    = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
			var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

			$('#satuan-'+kode).val(satuan);

			$.get(url, function(data) {
				if (data.harga === null) {
					tr.find('#inputHargaPerSatuan').val(0);
					tr.find('#inputSubTotal').val(0);
				} else {
					var harga       = data.harga.harga;
					var subtotal    = parseInt(harga) * parseInt(jumlah);

					var konversi    = parseInt(data.satuans.konversi) * parseInt(jumlah);

					tr.find('#inputHargaPerSatuan').val(harga.toLocaleString());
					tr.find('#inputSubTotal').val(subtotal.toLocaleString());

					$('#harga-'+kode).val(parseInt(harga));
					$('#subtotal-'+kode).val(subtotal);

					// updateHargaTotal();
					updateHargaOnKeyup();

					var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';

					$.get(hpp_url, function(data) {
						var arr   = [];
						var index = 0;
						var hpp   = 0;

						for (var i = 0; i < data.stoks.length; i++) {
							arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
						}

						for (var j = 0; j < parseInt(konversi); j++) {
							arr[index][0] -= 1;
							hpp += parseInt(arr[index][1]);

							if (arr[index][0] === 0) index += 1;		
						}

						$('#hpp-'+kode).val(hpp);
					});
				}
			});
		});

		$(document).on('keyup', '#inputNego', function(event) {
			event.preventDefault();
			
			var input  = $(this);
			var nego   = input.val();
			var kode   = input.parents('tr').data('id'); 
			var hpp    = $('#hpp-'+kode).val();
			var jumlah = input.parents('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val();

			nego = parseInt(nego.replace(/\D/g, ''), 10);

			if (hpp > nego) {
				input.parents('.form-group').addClass('has-error');
			} else {
				input.parents('.form-group').removeClass('has-error');
				$('#subtotal-'+kode).val(nego);

				// updateHargaTotal();
				updateHargaOnKeyup();
			}
		});

		$(document).on('click', '#btnPotonganPersen', function(event) {
			event.preventDefault();

			var pelanggan   = $('input[name="pelanggan"]').val();
			var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
			var harga_total = 0;

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-danger');
				$(this).next().val(true);

				$(this).parent().next().find('button').removeClass('btn-primary');
				$(this).parent().next().find('button').addClass('btn-default');
				$(this).parent().next().find('button').next().val(false);

				$.get(url, function(data) {
					var persen = data.pelanggan.diskon_persen;

					harga_total = $('#hiddenHargaTotal').val();
					harga_total = harga_total - (harga_total * (persen/100));

					$('#inputHargaTotal').val(harga_total.toLocaleString());
					$('input[name="harga_total"]').val(harga_total);
				});
			} else if ($(this).hasClass('btn-danger')) {
				$(this).removeClass('btn-danger');
				$(this).addClass('btn-default');
				$(this).next().val(false);

				harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

				$('#inputHargaTotal').val(harga_total.toLocaleString());
				$('input[name="harga_total"]').val(harga_total);
			}
		});

		$(document).on('click', '#btnPotonganTunai', function(event) {
			event.preventDefault();

			var pelanggan   = $('input[name="pelanggan"]').val();
			var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
			var harga_total = 0;
			
			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-primary');
				$(this).next().val(true);

				$(this).parent().prev().find('button').removeClass('btn-danger');
				$(this).parent().prev().find('button').addClass('btn-default');
				$(this).parent().prev().find('button').next().val(false);

				$.get(url, function(data) {
					var potongan = data.pelanggan.potongan;

					harga_total = $('#hiddenHargaTotal').val();
					harga_total -= potongan;

					$('#inputHargaTotal').val(harga_total.toLocaleString());
					$('input[name="harga_total"]').val(harga_total);
				});
			} else if ($(this).hasClass('btn-primary')) {
				$(this).removeClass('btn-primary');
				$(this).addClass('btn-default');
				$(this).next().val(false);

				harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

				$('#inputHargaTotal').val(harga_total.toLocaleString());
				$('input[name="harga_total"]').val(harga_total);
			}
		});

		$(document).on('keyup', '#inputNegoTotal', function(event) {
			event.preventDefault();
			
			var input      = $(this);
			var nego_total = parseInt(input.val().replace(/\D/g, ''), 10);
			var hpp_total  = 0;

			input.val(nego_total.toLocaleString());

			$('.hpp').each(function(index, el) {
				var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(tmp)) tmp = 0;
				hpp_total += tmp;
			});

			if (hpp_total > nego_total) {
				input.parents('.form-group').addClass('has-error');
			} else {
				input.parents('.form-group').removeClass('has-error');

				$('#inputHargaTotal').val(nego_total.toLocaleString());
				$('input[name="harga_total"]').val(nego_total);

				var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
				var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
				var kembali      = jumlah_bayar - harga_total;
				if (kembali < 0) kembali = 0;

				$('#inputTotalKembali').val(kembali.toLocaleString());
				$('input[name="kembali"]').val(kembali);
			}
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-danger');
				$(this).find('i').addClass('fa-check');
				$('#inputTunaiContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-danger')) {
				$(this).removeClass('btn-danger');
				$(this).find('i').removeClass('fa-check');
				$(this).addClass('btn-default');
				$('#inputTunaiContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnDebit', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-warning');
				$(this).find('i').addClass('fa-check');
				$('#inputTransferBankContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-warning')) {
				$(this).removeClass('btn-warning');
				$(this).find('i').removeClass('fa-check');
				$(this).addClass('btn-default');
				$('#inputTransferBankContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_transfer"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnCek', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
				$(this).find('i').addClass('fa-check');
				$('#inputCekContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-success')) {
				$(this).removeClass('btn-success');
				$(this).find('i').removeClass('fa-check');
				$(this).addClass('btn-default');
				$('#inputCekContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnBG', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-primary');
				$(this).find('i').addClass('fa-check');
				$('#inputBGContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-primary')) {
				$(this).removeClass('btn-primary');
				$(this).addClass('btn-default');
				$(this).find('i').removeClass('fa-check');
				$('#inputBGContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_bg"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('keyup', '#inputNominalTunai', function(event) {
			event.preventDefault();
			var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_tunai)) nominal_tunai = 0;

			$(this).val(nominal_tunai.toLocaleString());
			$('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoTransfer', function(event) {
			event.preventDefault();
			var no_transfer = $(this).val();

			$('input[name="no_transfer"]').val(no_transfer);
		});

		$(document).on('keyup', '#inputNominalTransfrer', function(event) {
			event.preventDefault();
			var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_transfer)) nominal_transfer = 0;

			$(this).val(nominal_transfer.toLocaleString());
			$('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoCek', function(event) {
			event.preventDefault();
			var no_cek = $(this).val();

			$('input[name="no_cek"]').val(no_cek);
		});

		$(document).on('keyup', '#inputNominalCek', function(event) {
			event.preventDefault();
			var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_cek)) nominal_cek = 0;

			$(this).val(nominal_cek.toLocaleString());
			$('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoBg', function(event) {
			event.preventDefault();
			var no_bg = $(this).val();

			$('input[name="no_bg"]').val(no_bg);
		});

		$(document).on('keyup', '#inputNominalBg', function(event) {
			event.preventDefault();
			var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_bg)) nominal_bg = 0;

			$(this).val(nominal_bg.toLocaleString());
			$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
			updateHargaOnKeyup();
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			
			var kode         = $(this).parents('tr').data('id');
			var tr           = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
			var subtotal     = parseInt(tr.children().last().find('input').val().replace(/\D/g, ''), 10);
			var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
			var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
			var kembali      = parseInt($('#inputTotalKembali').val().replace(/\D/g, ''), 10);

			harga_total -= subtotal;
			kembali = jumlah_bayar - harga_total;

			$('#inputHargaTotal').val(harga_total.toLocaleString());
			$('#inputTotalKembali').val(kembali.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			$('input[name="kembali"]').val(kembali);

			tr.remove();
			$('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
		});
	</script>
@endsection
