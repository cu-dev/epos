@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Transaksi Grosir {{ $transaksi_grosir->kode_transaksi }}</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnCairkan {
            margin-right: 0;
        }
        #btnCairkan, #btnRetur, #btnKembali, #btnCetakNota, #btnBayarPiutang {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
            <input type="hidden" name="show" value="1">
            <input type="hidden" name="ids" value="{{ $transaksi_grosir->id }}">
        </form>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                {{-- <h2>Detail Transaksi Grosir</h2> --}}
                <h2>Detail Transaksi Penjualan</h2>
                {{-- <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/cairkan-kredit') }}" id="btnCairkan" class="btn btn-sm btn-success pull-right" {{ $transaksi_grosir->nominal_kredit > 0 && $piutang_dagang->sisa > 0 ? '' : 'disabled' }}>
                    <i class="fa fa-money"></i> Cairkan Kredit
                </a> --}}
                {{-- <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur/create') }}" id="btnRetur" class="btn btn-sm btn-warning pull-right">
                    <i class="fa fa-sign-in"></i> Retur
                </a> --}}
                
                @if ($retur_show)
                    <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur/create') }}" btn class="btn btn-sm btn-success pull-right" id="btnDetail" title="Retur Penjualan" data-toggle="tooltip" data-placement="top">
                        <i class="fa fa-mail-reply"></i>
                    </a>
                @else
                    @if ($aktivasi_show) 
                        <a btn class="btn btn-sm btn-danger pull-right" id="btnCanRetur" title="Aktifkan Retur Penjualan" data-toggle="tooltip" data-placement="top">
                            <i class="fa fa-check"></i>
                        </a>
                    @endif
                @endif


                {{-- @if(in_array(Auth::user()->level_id, [1,2,3]))
                    @if($transaksi_grosir->can_retur == 0)
                        @if($rule->syarat !=  0 && $rule->syarat !=  null)
                            @if($today <= $batas_retur)
                                <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur/create') }}" id="btnRetur" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Retur">
                                    <i class="fa fa-mail-reply"></i>
                                </a>
                            @endif
                        @endif
                    @else
                        <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur/create') }}" id="btnRetur" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Retur">
                            <i class="fa fa-mail-reply"></i>
                        </a>
                    @endif
                @endif --}}

                @if ($piutang_dagang != null && in_array(Auth::user()->level_id, [1,2,4]))
                <a href="{{ url('piutang-dagang/'.$piutang_dagang->id) }}" id="btnBayarPiutang" class="btn btn-sm btn-primary pull-right" data-toggle="tooltip" data-placement="top" title="Bayar Piutang Penjualan">
                    <i class="fa fa-money"></i>
                </a>
                @endif
                @if ($boleh_cetak)
                    @if ($struk)
                        <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/cetak-nota') }}" id="btnCetakNota" class="btn btn-sm btn-cetak pull-right" data-toggle="tooltip" data-placement="top" title="Cetak Struk">
                            <i class="fa fa-print"></i>
                        </a>
                    @else
                        <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/cetak-nota') }}" id="btnCetakNota" class="btn btn-sm btn-cetak pull-right" data-toggle="tooltip" data-placement="top" title="Cetak Faktur">
                            <i class="fa fa-print"></i>
                        </a>
                    @endif
                @endif
                <a href="{{ url('transaksi-grosir') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                {{-- <a btn class="btn btn-xs btn-danger" id="btnCanRetur" title="Aktifkan Retur">
                    <i class="fa fa-bolt"></i>
                </a>
                <a href="{{ url('transaksi-grosir/tanggal/'.$transaksi_grosir->id) }}" class="btn btn-xs btn-warning" title="Ubah Tanggal Jatuh Tempo" id="btnTanggal">
                    <i class="fa fa-calendar"></i>
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_grosir->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->kode_transaksi }}</td>
                                </tr>
                                @if ($transaksi_grosir->pelanggan_id != null)
                                <tr>
                                    <th>Pelanggan</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Toko</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->toko }}</td>
                                </tr>
                                <tr>
                                    <th>Telepon</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->telepon }}</td>
                                </tr>
                                @if ($transaksi_grosir->alamat != null)
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->alamat }}</td>
                                </tr>
                                @else
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->alamat }}</td>
                                </tr>
                                @endif
                                @endif
                                <tr>
                                    <th>Tanggal Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->created_at->format('d-m-Y') }}</td>
                                </tr>
                                @if($transaksi_grosir->jatuh_tempo != NULL && $transaksi_grosir->jumlah_bayar < $transaksi_grosir->harga_total - $transaksi_grosir->potongan_penjualan)
                                    <tr>
                                        <th>Jatuh Tempo</th>
                                        <td style="width: 60%;">{{ \App\Util::date($transaksi_grosir->jatuh_tempo) }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Status Penjualan</th>
                                    <td style="width: 60%; text-transform: capitalize;">{{ $transaksi_grosir->status }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->user != null ? $transaksi_grosir->user->nama : $transaksi_grosir->grosir->nama }}</td>
                                </tr>
                                @if ($transaksi_grosir->pengirim != null)
                                <tr>
                                    <th>Pengirim</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pengirim }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        @if(in_array(Auth::user()->level_id, [1, 2]) && $transaksi_grosir->jatuh_tempo != NULL && $transaksi_grosir->jumlah_bayar < $transaksi_grosir->harga_total - $transaksi_grosir->potongan_penjualan)
                            <form action="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/jatuh-tempo') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="_method" value="post" />
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="form-group col-sm-8 col-xs-8">
                                        <label class="control-label">Ubah Jatuh Tempo</label>
                                        <input type="hidden" name="jatuh_tempo_baru_" value="{{$transaksi_grosir->jatuh_tempo}}" />
                                        <input type="text" id="jatuh_tempo_baru" name="jatuh_tempo_baru" class="form-control jatuh-tempo tanggal-putih" placeholder="Edit Jatuh Tempo" value="{{ App\Util::date($transaksi_grosir->jatuh_tempo) }}" readonly="" />
                                    </div>
                                    <div class="form-group col-sm-4 col-xs-4">
                                        <label class="control-label">Ubah</label>
                                        <button type="submit" class="btn btn-success btn-block">
                                            <i class="fa fa-save"></i> Ubah
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @endif
                        
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Rincian</th>
                            </thead>
                            <tbody>
                                @if ($transaksi_grosir->harga_total != null && $transaksi_grosir->harga_total > 0)
                                <tr>
                                    <th>Sub Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total) }}</td>
                                </tr>
                                @endif

                                <tr>
                                    <th>Nego & Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->potongan_penjualan) }}</td>
                                </tr>

                                <tr>
                                    <th>Deposito Pelanggan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_titipan) }}</td>
                                </tr>

                                @if ($kembali >= 0)
                                    <tr>
                                        <th>Jumlah Bayar</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total + $transaksi_grosir->ongkos_kirim - $transaksi_grosir->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kembalian</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali) }}</td>
                                    </tr>
                                @elseif($kembali < 0)
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total + $transaksi_grosir->ongkos_kirim - $transaksi_grosir->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pengurang Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali*-1) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($transaksi_grosir->nominal_tunai != null && $transaksi_grosir->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->banktransfer->nama_bank }} [{{ $transaksi_grosir->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_transfer != null && $transaksi_grosir->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    @if ($transaksi_grosir->jenis_kartu == 'debet')
                                        <td class="text-right" style="width: 60%;">Debit</td>
                                    @else
                                        <td class="text-right" style="width: 60%;">Kredit</td>
                                    @endif
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->bankkartu->nama_bank }} [{{ $transaksi_grosir->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_kartu != null && $transaksi_grosir->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->bankcek->nama_bank }} [{{ $transaksi_grosir->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_cek != null && $transaksi_grosir->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->bankbg->nama_bank }} [{{ $transaksi_grosir->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_bg != null && $transaksi_grosir->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_bg) }}</td>
                                </tr>
                                @endif

                                {{-- @if ($transaksi_grosir->nominal_titipan != null && $transaksi_grosir->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_titipan) }}</td>
                                </tr>
                                @endif --}}

                                {{-- @if ($transaksi_grosir->jumlah_bayar != null && $transaksi_grosir->jumlah_bayar > 0) --}}
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar - $transaksi_grosir->nominal_titipan) }}</td>
                                </tr>
                                {{-- @endif --}}
                            </tbody>
                        </table>

                        <!-- <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_grosir->harga_total != null && $transaksi_grosir->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total) }}</td>
                                </tr>
                                @endif
                        
                                @if ($transaksi_grosir->potongan_penjualan != null && $transaksi_grosir->potongan_penjualan > 0)
                                <tr>
                                    <th>Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->potongan_penjualan) }}</td>
                                </tr>
                                @endif
                        
                                @if ($transaksi_grosir->nego_total != null && $transaksi_grosir->nego_total > 0 && $is_nego)
                                <tr>
                                    <th>Nego Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nego_total) }}</td>
                                </tr>
                                @endif
                        
                                @if ($transaksi_grosir->ongkos_kirim != null && $transaksi_grosir->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->ongkos_kirim) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table> -->
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Total</th>
                                    <th class="text-left">Nego</th>
                                    <th class="text-left">Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_grosir as $i => $relasi)
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item_kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah" class="jumlah">{{ $relasi->jumlah }}</td>
                                    <td id="harga" class="text-right">{{ \App\Util::duit0($relasi->subtotal / $relasi->jumlah) }}</td>
                                    {{-- <td class="text-right {{ $relasi->nego != null ? 'text-success' : '' }}">{{ \App\Util::duit0($relasi->nego != null ? $relasi->nego : $relasi->subtotal) }}</td> --}}
                                    <td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->nego) }}</td>
                                    @if ($relasi->bonuses != null)
                                    <td>
                                        @foreach ($relasi->bonuses as $j => $rb)
                                            {{ $rb['jumlah'] }} {{ $rb['bonus']->nama }}
                                            @if ($j != count($relasi->bonuses) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if (count($relasi_bonus_rules_penjualan) > 0)
                        <div class="x_title">
                            <h2>Bonus Penjualan</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item-bundle">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Paket</th>
                                    <th class="text-left">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_bonus_rules_penjualan as $i => $rbrp)
                                <tr id="{{ $rbrp->id }}" item_kode="{{ $rbrp->bonus->kode }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $rbrp->bonus->nama }}</td>
                                    <td class="jumlah">{{ $rbrp->jumlah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif

                        @if (count($show_relasi_bundles) > 0)
                        <div class="x_title">
                            <h2>Item Paket</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item-bundles">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Paket</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($show_relasi_bundles as $i => $show_relasi_bundle)
                                    @foreach ($show_relasi_bundle as $j => $relasi_bundle)
                                    <tr id="{{ $relasi_bundle->id }}" item_kode="{{ $relasi_bundle->item_child }}">
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $relasi_bundle->bundle->nama }}</td>
                                        <td>{{ $relasi_bundle->itemxx->nama }}</td>
                                        <td class="jumlah">{{ $relasi_bundle->jumlah }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'PO Penjualan berhasil dibayar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'kredit')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kredit berhasil dicairkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Grosir berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah_jatuh_tempo')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Jatuh tempo berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah_stok')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Stok barang ada  yang kurang, cek kembali!',
                timer: 3000,
                type: 'warning'
            });
        </script>
    @endif

    <script type="text/javascript">
        var items = [];
        var satuans = null;
        var relasi_transaksi_grosir = null;

        $(document).ready(function() {
            var url = "{{ url('transaksi-grosir') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            items = '{{ json_encode($item_list) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            relasi_transaksi_grosir = '{{ $relasi_transaksi_grosir }}';
            relasi_transaksi_grosir = relasi_transaksi_grosir.replace(/&quot;/g, '"');
            relasi_transaksi_grosir = JSON.parse(relasi_transaksi_grosir);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var konversi = null;
                var v_jumlah = [];
                var v_satuan = [];
                var text_satuan = '';

                for (var i = 0; i < relasi_transaksi_grosir.length; i++) {
                    if (relasi_transaksi_grosir[i].id == id) {
                        relasi = relasi_transaksi_grosir[i];
                        break;
                    }
                }

                var item = relasi.item;
                var jumlah = relasi.jumlah;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var temp_konversi = satuan.konversi;
                    if (jumlah / temp_konversi >= 1) {
                        if (konversi == null) konversi = temp_konversi;
                        text_satuan += Math.floor(jumlah / temp_konversi);
                        text_satuan += ' ' + satuan.satuan.kode;
                        if (i != item.satuan_pembelians.length - 1) text_satuan += '<br>';
                        jumlah = jumlah % temp_konversi;
                    }
                }

                

                // console.log(text_satuan);
                // $(el).find('#jumlah').html(text_satuan);

                var harga = $(el).find('#harga').text();
                harga = harga.split('p')[1];
                harga = parseFloat(harga.replace(/\D/g, ''), 10);
                harga *= konversi;
                harga = Math.round(harga / 100) * 100;
                $(el).find('#harga').text('Rp'+harga.toLocaleString());
            });

            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);
                // console.log(item_kode);

                var satuan_item = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }
                // console.log(satuan_item);

                // var jumlah1 = '';
                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        // if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                        //     jumlah2 += jumlah_jual;
                        //     jumlah2 += ' ';
                        //     jumlah2 += satuan.kode;
                        //     temp_jumlah = temp_jumlah % satuan.konversi;
                        // }
                        if (jumlah_jual > 0) {
                            jumlah2 += jumlah_jual;
                            jumlah2 += ' ';
                            jumlah2 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;

                            if (i != satuan_item.length - 1 && temp_jumlah > 0) jumlah2 += ' ';
                        }
                    }
                }
                // console.log(jumlah2);

                $(el).text(jumlah2);
                if (jumlah2 == '') $(el).text(jumlah);
            });

            $('#tabel-item').DataTable();
            $('#tabel-item-bundle').DataTable();
            $('#tabel-item-bundles').DataTable();
        });
    
        $(document).on('focus', '#jatuh_tempo_baru', function(event) {
            event.preventDefault();

            $('#jatuh_tempo_baru').daterangepicker({
                    autoApply: true,
                    showDropdowns: true,
                    calender_style: "picker_2",
                    format: 'DD-MM-YYYY',
                    locale: {
                        "applyLabel": "Pilih",
                        "cancelLabel": "Batal",
                        "fromLabel": "Awal",
                        "toLabel": "Akhir",
                        "customRangeLabel": "Custom",
                        "weekLabel": "M",
                        "daysOfWeek": [
                            "Min",
                            "Sen",
                            "Sel",
                            "Rab",
                            "Kam",
                            "Jum",
                            "Sab"
                        ],
                        "monthNames": [
                            "Januari",
                            "Februari",
                            "Maret",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "Agustus",
                            "September",
                            "Oktober",
                            "November",
                            "Desember"
                        ],
                        "firstDay": 1
                    },
                    singleDatePicker: true
            }, function(start) {
                var tanggal = (start.toISOString()).substring(0,10);
                $('input[name="jatuh_tempo_baru_"]').val(tanggal);
            });
        });

        $(document).on('click', '#btnCanRetur', function() {
            // var $tr = $(this).parents('tr').first();
            var id = '{{ $transaksi_grosir->id }}';
            // var nama_item = $tr.find('td').first().next().next().text();
            // $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan Retur?',
                text: 'Retur transaksi ini akan diaktifkan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-grosir") }}' + '/' + id + '/retur/aktif');
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });
    </script>
@endsection
