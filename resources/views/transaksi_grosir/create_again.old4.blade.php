@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Transaksi Grosir</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnKembali {
            margin-bottom: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-9">
                        <h2 id="formSimpanTitle">Tambah Transaksi Penjualan Grosir</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    <div class="col-md-3 pull-right">
                        <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-10 col-xs-10">
                        <label class="control-label">Pilih Pelanggan</label>
                        <select name="pelanggan_id" id="pilihPelanggan" class="select2_single form-control">
                            <option value="">Pilih Pelanggan</option>
                            @foreach ($pelanggans as $pelanggan)
                            <option value="{{ $pelanggan->id }}">{{ $pelanggan->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2 col-xs-2" style="text-align: right;">
                        <label class="control-label" style="opacity: 0;">Aksi</label>
                        <button id="resetPelanggan" class="btn btn-default" style="margin-right: 0;"><i class="fa fa-trash"></i></button>
                    </div>
                    {{-- <div class="form-group col-sm-3 col-xs-3" style="display: none;">
                        <label class="control-label">Kode</label>
                        <input type="text" name="inputKodePelanggan" id="inputKodePelanggan" class="form-control">
                    </div>
                    <div class="form-group col-sm-7 col-xs-7" style="display: none;">
                        <label class="control-label">Nama</label>
                        <input type="text" name="inputNamaPelanggan" id="inputNamaPelanggan" class="form-control">
                    </div> --}}
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelPelanggan" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Nama</th>
                                    <th style="text-align: left;">Alamat</th>
                                    <th style="text-align: left;">Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr>
                                    <td id="namaPelanggan"></td>
                                    <td id="alamatPelanggan"></td>
                                    <td id="teleponPelanggan"></td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 hidden-xs">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-8 col-xs-8">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" id="pilihItem" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                            @foreach($items as $item)
                            <option value="{{$item->kode}}">[{{ $item->kode }}] {{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Item</th>
                                    <th style="text-align: left;">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr>
                                    <td></td>
                                    <td></td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Belanja</h2>
                <div id="tipe_penjualan" class="label pull-right" style="font-size: 14px; font-weight: 400;" data-toggle="tooltip" data-placement="top" title="Status Penjualan">Eceran</div>
                <div id="level_pelanggan" class="label pull-right" style="font-size: 14px; font-weight: 400; margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Status Pelanggan">Eceran</div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <table class="table" id="tabelKeranjang">
                            <thead>
                                <tr>
                                    <th style="width: 20px;"></th>
                                    <th style="text-align: left;">Item</th>
                                    {{-- <th style="text-align: left; width: 200px;">Jumlah</th> --}}
                                    <th style="text-align: center; width: 300px;">Jumlah</th>
                                    {{-- <th style="text-align: left; width: 100px;">Satuan</th> --}}
                                    <th style="text-align: center; width: 125px;">Harga</th>
                                    <th style="text-align: center; width: 150px;">Sub Total</th>
                                    <th style="text-align: center; width: 150px;">Nego Harga</th>
                                    <th style="text-align: left; width: 100px;">Bonus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTitipan" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Titipan</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_id">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Kartu</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTitipanContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Titipan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTitipan" id="inputNominalTitipan" class="form-control angka">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                {{-- <label class="control-label">Metode Pembayaran</label> --}}
                                <div id="diambilAtauDikirmButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDiambil" class="btn btn-default"><i class="fa"></i> Diambil</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDikirim" class="btn btn-default"><i class="fa"></i> Dikirim</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputDikirmContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Karyawan</label>
                                        <select class="form-control select2_single" name="user_id">
                                            <option value="">Pilih Karyawan</option>
                                            @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                            @endforeach
                                            <option value="-">Pengirim Lain</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pengirim Lain</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputPengirimLain" class="form-control" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Harga Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />
                                    <input type="hidden" name="hiddenHargaTotal" id="hiddenHargaTotal" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Nego Harga Total (Rp)</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><input type="checkbox" name="checkNegoTotal" id="checkNegoTotal" disabled="" /></div>
                                    <input type="hidden" name="inputNegoTotalMin" id="inputNegoTotalMin" />
                                    <input type="text" name="inputNegoTotal" id="inputNegoTotal" class="form-control angka" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Potongan Penjualan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputPotonganPenjualan" id="inputPotonganPenjualan" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Harga Total + Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Jumlah Bayar</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Kembali</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('transaksi-grosir') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="_method" value="put" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="pelanggan" value="" />
                                        <input type="hidden" name="kode_pelanggan" value="" />
                                        <input type="hidden" name="nama_pelanggan" value="" />
                                        <input type="hidden" name="level_pelanggan" value="" />
                                        <input type="hidden" name="grosir" value="false" />
                                        <input type="hidden" name="titipan" value="" />
                                        {{-- <input type="hidden" name="potongan" value="" /> --}}

                                        <input type="hidden" name="harga_total" />
                                        <input type="hidden" name="harga_akhir" />
                                        <input type="hidden" name="nego_total" />
                                        <input type="hidden" name="nego_total_min" />
                                        <input type="hidden" name="nego_total_view" />
                                        <input type="hidden" name="potongan_penjualan" />
                                        <input type="hidden" name="ongkos_kirim" />
                                        <input type="hidden" name="jumlah_bayar" />
                                        <input type="hidden" name="kembali" />

                                        <input type="hidden" name="nominal_tunai" />

                                        <input type="hidden" name="no_transfer" />
                                        <input type="hidden" name="bank_transfer" />
                                        <input type="hidden" name="nominal_transfer" />

                                        <input type="hidden" name="no_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />

                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="nominal_cek" />

                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="nominal_bg" />

                                        <input type="hidden" name="nominal_titipan" />

                                        <input type="hidden" name="pengirim" />
                                        <input type="hidden" name="pengirim_lain" />

                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                        </div>
                                    </form>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="btnSimpanPO" id="btnSimpanPO" class="btn btn-success" disabled=""><i class="fa fa-save"></i> Simpan PO</button>
                                    {{--<button type="submit" name="btnCetakPengambilan" id="btnCetakPengambilan" class="btn btn-success" disabled=""><i class="fa fa-print"></i> Cetak Pengambilan</button>--}}
                                    {{--<button type="submit" name="btnBayar" id="btnBayar" class="btn btn-success" disabled=""><i class="fa fa-money"></i> Bayar</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        showSpinner = true;
        var banks = [];
        var users = [];
        var items = [];
        var satuans = [];
        var pelanggans = [];
        var pelanggan = null;
        var selected_items = [];
        var transaksi_penjualan = null;
        var custom_data_last_item = false;
        var relasi_transaksi_penjualan = null;

        /*function isBtnSimpanPODisabled() {
            // console.log('isBtnSimpanPODisabled');
            var pelanggan = $('input[name="pelanggan"]').val();
            var kode_pelanggan = $('input[name="kode_pelanggan"]').val();
            var nama_pelanggan = $('input[name="nama_pelanggan"]').val();
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            // console.log(harga_total);

            if (pelanggan == '' && (kode_pelanggan == '' || nama_pelanggan == '')) return true;

            if (isNaN(harga_total) || harga_total <= 0) return true;

            return false;
        }*/

        function isSubmitButtonDisabled() {
            // console.log('isSubmitButtonDisabled');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var kode_pelanggan = $('input[name="kode_pelanggan"]').val();
            var nama_pelanggan = $('input[name="nama_pelanggan"]').val();
            var grosir = $('input[name="grosir"]').val();
            var is_grosir = grosir == 'true' ? true : false;
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var ongkos_kirim = parseFloat($('input[name="ongkos_kirim"]').val());
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
            var no_transfer = $('input[name="no_transfer"]').val();
            var bank_transfer = $('input[name="bank_transfer"]').val();
            var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
            var no_kartu = $('input[name="no_kartu"]').val();
            var bank_kartu = $('input[name="bank_kartu"]').val();
            var jenis_kartu = $('input[name="jenis_kartu"]').val();
            var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
            var no_cek = $('input[name="no_cek"]').val();
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
            var no_bg = $('input[name="no_bg"]').val();
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
            var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
            // console.log(nominal_titipan);

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;

            /*console.log('');
            console.log('pelanggan_id');
            if (grosir == 'true' && pelanggan_id == '' && (kode_pelanggan == '' || nama_pelanggan == '')) {
                if (transaksi_penjualan.status == 'po_eceran') {
                    // boleh disimpan
                    // return false;
                    if (jumlah_bayar > 0 && jumlah_bayar < harga_total + ongkos_kirim) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    // tidak boleh disimpan
                    return true;
                }
            }*/

            // console.log('harga_total');
            // console.log(harga_total);
            if (isNaN(harga_total) || harga_total <= 0) return true;

            /*if (jumlah_bayar > 0 && jumlah_bayar < harga_total + ongkos_kirim) {
                console.log('oi');
                if (!is_grosir) return true;
                console.log('oi');
                if (transaksi_penjualan.status == 'po_eceran') return true;
                console.log('oi');
            }*/

            // console.log('nominal_tunai');
            if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

            // console.log('bank_transfer');
            if ($('#btnTransfer').hasClass('btn-warning') && (no_transfer == '' || bank_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

            // console.log('no_kartu');
            if ($('#btnKartu').hasClass('btn-info') && (no_kartu == '' || bank_kartu == '' || jenis_kartu == '' || isNaN(nominal_kartu) || nominal_kartu <= 0)) return true;

            // console.log('no_cek');
            if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

            // console.log('no_bg');
            if ($('#btnBG').hasClass('btn-primary') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

            // console.log('nominal_titipan');
            if ($('#btnTitipan').hasClass('btn-purple') && (nominal_titipan <= 0)) return true;

            // console.log('nominal_titipan');
            if ($('#btnTitipan').hasClass('btn-purple') && nominal_titipan > 0 && $('#inputTitipanContainer').find('.input-group').hasClass('has-error')) return true;

            // console.log('checkNego');
            $('#checkNego').each(function(index, el) {
                var checked = $(el).prop('checked');
                var error = $(el).parents('.form-group').first().hasClass('has-error');
                if (checked && error) return true;
            });

            // console.log('checkNegoTotal');
            if ($('#checkNegoTotal').prop('checked') && $('#checkNegoTotal').parents('.form-group').first().hasClass('has-error')) return true;

            // console.log('jumlah_bayar');
            if (jumlah_bayar > 0 && jumlah_bayar - nominal_tunai > harga_total_plus_ongkos_kirim) return true;

            // console.log('jumlah_bayar');
            if (jumlah_bayar > 0 && jumlah_bayar < harga_total_plus_ongkos_kirim) {
                if (pelanggan_id == '') {
                    // console.log('tidak');
                    return true;
                } else {
                    // console.log('ada');
                    if (!is_grosir || pelanggan.level == 'eceran') {
                        // console.log('eceran');
                        return true;
                    }
                }
            }
            // console.log('end');

            return false;
        }

        function cekEceranAtauGrosir() {
            // console.log('cekEceranAtauGrosir');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var level_pelanggan = $('input[name="level_pelanggan"]').val();

            if (pelanggan_id == '') {
                var is_grosir = true;
                var jumlah_item = 0;

                $('input[name="is_grosir[]"]').each(function(index, el) {
                    // jika ada yang salah maka is_grosir false
                    val = $(el).val();
                    if (val == 'false') is_grosir = false;
                    jumlah_item++;
                });

                $('input[name="grosir"]').val(is_grosir && jumlah_item > 0);

                $('#level_pelanggan').text('Eceran');
                $('#level_pelanggan').removeClass('label-warning');
                $('#level_pelanggan').addClass('label-success');

                if (is_grosir) {
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            } else {
                // cek selisih jumlah * konversi grosir dan eceran
                var jumlah_grosir = 0;
                var jumlah_eceran = 0;

                $('input[name="item_kode[]"]').each(function(index, el) {
                    var item_kode = $(el).val();
                    var is_grosir = $('#is_grosir-'+item_kode).val() == 'true' ? true : false;
                    var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                    var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                    var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                    var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                    if (is_grosir) jumlah_grosir += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                    else jumlah_eceran += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                });

                if (level_pelanggan == 'eceran') {
                    $('#level_pelanggan').text('Eceran');
                    $('#level_pelanggan').removeClass('label-warning');
                    $('#level_pelanggan').addClass('label-success');
                } else {
                    $('#level_pelanggan').text('Grosir');
                    $('#level_pelanggan').removeClass('label-success');
                    $('#level_pelanggan').addClass('label-warning');
                }

                if (jumlah_grosir > jumlah_eceran) {
                    $('input[name="grosir"]').val(true);
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('input[name="grosir"]').val(false);
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            }
        }

        function updateHargaTotal() {
            var harga_total = 0;
            var harga_akhir = 0;
            var nego_total = 0;
            var nego_total_min = 0;

            $('.subtotal').each(function(index, el) {
                var item_kode = $(el).attr('id').split('-')[1];

                var tmp_harga = parseFloat($('#subtotal-'+item_kode).val());
                if (isNaN(tmp_harga)) tmp_harga = 0;
                harga_total += tmp_harga;

                var tmp_nego = parseFloat($('#nego-'+item_kode).val());
                if (isNaN(tmp_nego)) tmp_nego = 0;
                nego_total += tmp_nego;

                if (tmp_nego > 0) {
                    harga_akhir += tmp_nego;
                } else {
                    harga_akhir += tmp_harga;
                }

                var tmp_nego_min = parseFloat($('#nego_min-'+item_kode).val());
                if (isNaN(tmp_nego_min)) tmp_nego_min = 0;
                nego_total_min += tmp_nego_min;
            });

            var nego_total_view = parseFloat($('input[name="nego_total_view"]').val());
            if (nego_total_view > 0 && nego_total_view < harga_akhir) harga_akhir = nego_total_view;
            // console.log(harga_total);

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="harga_akhir"]').val(harga_akhir);
            $('input[name="nego_total"]').val(nego_total);
            $('input[name="nego_total_min"]').val(nego_total_min);

            $('#inputNegoTotalMin').val(nego_total_min);
        }

        function updateHargaOnKeyup() {
            updateHargaTotal();

            var $harga_total = $('#inputHargaTotal');
            var $potongan_penjualan = $('#inputPotonganPenjualan');
            var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $kembali = $('#inputTotalKembali');

            var level_pelanggan = $('input[name="level_pelanggan"]').val();
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var nominal_titipan = $('input[name="nominal_titipan"]').val();
            var harga_total = $('input[name="harga_total"]').val();
            if (harga_total === undefined) harga_total = '0';
            var harga_akhir = $('input[name="harga_akhir"]').val();
            if (harga_akhir === undefined) harga_akhir = '0';
            var nego_total = $('input[name="nego_total"]').val();
            if (nego_total === undefined) nego_total = '0';
            var nego_total_min = $('input[name="nego_total_min"]').val();
            if (nego_total_min === undefined) nego_total_min = '0';
            var nego_total_view = $('input[name="nego_total_view"]').val();
            if (nego_total_view === undefined) nego_total_view = '0';
            var ongkos_kirim = $('input[name="ongkos_kirim"]').val();
            if (ongkos_kirim === undefined) ongkos_kirim = '0';

            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_kartu = parseFloat(nominal_kartu);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);
            nominal_titipan = parseFloat(nominal_titipan);
            harga_total = parseFloat(harga_total);
            harga_akhir = parseFloat(harga_akhir);
            nego_total = parseFloat(nego_total);
            nego_total_min = parseFloat(nego_total_min);
            nego_total_view = parseFloat(nego_total_view);
            ongkos_kirim = parseFloat(ongkos_kirim);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(harga_akhir)) harga_akhir = 0;
            if (isNaN(nego_total)) nego_total = 0;
            if (isNaN(nego_total_min)) nego_total_min = 0;
            if (isNaN(nego_total_view)) nego_total_view = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

            /*var nego_total_akhir = 0;
            if (nego_total_view <= 0) {
                if (nego_total_data > 0) {
                    nego_total_akhir = nego_total_data;
                }
            } else if (nego_total_data <= 0) {
                if (nego_total_view > 0 && nego_total_view >= nego_total_min) {
                    nego_total_akhir = nego_total_view;
                }
            } else {
                if (nego_total_data < nego_total_view) {
                    nego_total_akhir = nego_total_data;
                } else {
                    if (nego_total_view >= nego_total_min) {
                        nego_total_akhir = nego_total_view;
                    } else {
                        nego_total_akhir = nego_total_data;
                    }
                }
            }*/

            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_kartu + nominal_cek + nominal_bg + nominal_titipan;
            var kembali = 0;
            var harga_total_plus_ongkos_kirim = 0;

            // Tambah potongan penjualan dengan diskon tiap item
            var potongan = 0;
            var potongan_penjualan = 0;
            $('input[name="diskon[]"]').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');

                var harga = 0;
                var diskon = parseFloat($(el).val());
                var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#harga_grosir-'+item_kode).val());

                if (isNaN(diskon)) diskon = 0;
                if (isNaN(harga_eceran)) harga_eceran = 0;
                if (isNaN(harga_grosir)) harga_grosir = 0;
                if (isNaN(jumlah1)) jumlah1 = 0;
                if (isNaN(jumlah2)) jumlah2 = 0;
                if (isNaN(konversi1)) konversi1 = 0;
                if (isNaN(konversi2)) konversi2 = 0;

                if (diskon > 0) {
                    var jumlah = (jumlah1 * konversi1) + (jumlah2 * konversi2);

                    harga = harga_eceran;
                    if (level_pelanggan == 'grosir') harga = harga_grosir;

                    var temp_potongan = diskon / 100 * harga * jumlah;
                    temp_potongan = Math.ceil(temp_potongan / 100) * 100;

                    potongan += temp_potongan;
                    // potongan_penjualan += temp_potongan;
                }
            });

            harga_akhir -= potongan;
            potongan_penjualan += (harga_total - harga_akhir);
            var laba_rugi_total = harga_akhir - nego_total_min;

            if (harga_akhir <= 0) laba_rugi_total = 0;
            if (harga_akhir <= 0) potongan_penjualan = 0;

            /*if (nego_total_akhir > 0) {
                harga_total_plus_ongkos_kirim = nego_total_akhir + ongkos_kirim;
                kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;
            } else {
                harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
                kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;
            }*/

            harga_total_plus_ongkos_kirim = harga_akhir + ongkos_kirim;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            // if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            // var potongan_penjualan = harga_total - nego_total_akhir;
            // if (nego_total_akhir <= 0) potongan_penjualan = 0;

            $harga_total.val(harga_total.toLocaleString());
            // $nego_total_view.val(nego_total_akhir.toLocaleString());
            $potongan_penjualan.val(potongan_penjualan.toLocaleString());
            $harga_total_plus_ongkos_kirim.val((harga_total_plus_ongkos_kirim).toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());
            $kembali.val(kembali.toLocaleString());

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="potongan_penjualan"]').val(potongan_penjualan);
            // $('input[name="nego_total"]').val(nego_total_akhir);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="kembali"]').val(kembali);

            // cek eceran atau grosir
            if (harga_total > 0) {
                cekEceranAtauGrosir(harga_total);
            }

            // cek boleh nego atau tidak
            if ($('input[name="pelanggan"]').val() == '') {
                $('input[name="checkNego"]').prop('disabled', true);
                $('input[name="checkNegoTotal"]').prop('disabled', true);
            } else {
                if (pelanggan.level == 'grosir') {
                    $('input[name="checkNego"]').prop('disabled', false);
                    $('input[name="checkNegoTotal"]').prop('disabled', false);

                    $('#tabelKeranjang tr').each(function(index, el) {
                        var item_kode = $(el).attr('data-id');
                        var subtotal = parseFloat($('#subtotal-'+item_kode).val());
                        var nego_min = parseFloat($('#nego_min-'+item_kode).val());
                        if (subtotal < nego_min) {
                            $('input[name="checkNego"]').prop('disabled', true);
                        }
                    });
                } else {
                    $('input[name="checkNego"]').prop('disabled', true);
                    $('input[name="checkNegoTotal"]').prop('disabled', true);
                }
            }

            // $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
            $('#btnSimpanPO').prop('disabled', isSubmitButtonDisabled());
        }

        function updateHargaKeEceran() {
            // console.log('updateHargaKeEceran');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                var harga_per_pcs = Math.ceil(harga_eceran / 100) * 100;
                var harga_view = harga_per_pcs * konversi;
                var subtotal = harga_per_pcs * jumlah;
                // console.log(harga_view, subtotal);

                $(el).find('#inputHargaPerSatuan').val(harga_view.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                $('#harga_eceran-'+item_kode).val(harga_per_pcs);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }

        function updateHargaKeGrosir() {
            // console.log('updateHargaKeGrosir');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                var harga_per_pcs = Math.ceil(harga_grosir / 100) * 100;
                var harga_view = harga_per_pcs * konversi;
                var subtotal = harga_per_pcs * jumlah;
                // console.log(harga_view, subtotal);

                $(el).find('#inputHargaPerSatuan').val(harga_view.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                $('#harga_grosir-'+item_kode).val(harga_per_pcs);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }

        function handlePelangganChange(pelanggan_id) {
            var id = pelanggan_id;
            if (id != '') {
                for (var i = 0; i < pelanggans.length; i++) {
                    if (pelanggans[i].id == id) {
                        pelanggan = pelanggans[i];
                        $('input[name="pelanggan"]').val(pelanggan.id);
                        $('input[name="kode_pelanggan"]').val(pelanggan.kode);
                        $('input[name="nama_pelanggan"]').val(pelanggan.nama);
                        $('input[name="level_pelanggan"]').val(pelanggan.level);

                        var nama = pelanggan.nama ? pelanggan.nama : '-';
                        var alamat = pelanggan.alamat ? pelanggan.alamat : '-';
                        var telepon = pelanggan.telepon ? pelanggan.telepon : '-';
                        var tr = ''+
                            '<tr>'+
                                '<td>'+nama+'</td>'+
                                '<td>'+alamat+'</td>'+
                                '<td>'+telepon+'</td>'+
                            '</tr>';

                        $('#tabelPelanggan tbody').empty();
                        $('#tabelPelanggan tbody').append(tr);

                        break;
                    }
                }

                $('#btnTitipan').prop('disabled', false);

                if (pelanggan.level == 'eceran') updateHargaKeEceran();
                else updateHargaKeGrosir();
            } else {
                $('input[name="pelanggan"]').val('');
                $('input[name="kode_pelanggan"]').val('');
                $('input[name="nama_pelanggan"]').val('');
                $('input[name="level_pelanggan"]').val('');

                $('#tabelPelanggan tbody').empty();

                if ($('#btnTitipan').hasClass('btn-purple')) $('#btnTitipan').trigger('click');
                $('#btnTitipan').prop('disabled', true);

                updateHargaKeEceran();
            }

            cekEceranAtauGrosir();

            // (id !== '') ? $('#pilihanPotonganContainer').show('fast') : $('#pilihanPotonganContainer').hide('fast')

            var titipan = pelanggan == null || pelanggan.titipan == null ? '' : pelanggan.titipan;
            titipan = parseFloat(titipan.replace(/\D/g, ''), 10) / 100;
            if (isNaN(titipan)) titipan = 0;

            $('input[name="titipan"]').val(titipan);
            updateHargaOnKeyup();
        }

        function handleItemChange(kode, custom_data) {
            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                selected_items.push(kode);
                $('#spinner').show();
                cariItem(kode, url, tr, custom_data);
            }

            // if (kode == '') $('#tabelInfo tbody').empty();
            // cariItem(kode, url, tr, custom_data);
        }

        function cariItem(kode, url, tr, custom_data) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;
                var diskon = parseFloat(item.diskon);
                var bonus_id = item.bonus_id;
                var hpp = parseFloat(data.hpp.harga);
                var harga_eceran = parseFloat(data.harga.eceran);
                var harga_grosir = parseFloat(data.harga.grosir);
                var nego_min = Math.round(hpp * 110 * (1 + (item.profit / 100))) / 100;
                var bonuses = data.relasi_bonus;
                var is_grosir = false;

                var v_harga = 0;
                var pelanggan_id = $('select[name="pelanggan_id"]').val();
                var temp_pelanggan = null;
                for (var i = 0; i < pelanggans.length; i++) {
                    if (pelanggans[i].id == pelanggan_id) {
                        temp_pelanggan = pelanggans[i];
                    }
                }

                if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                    v_harga = harga_eceran;
                } else {
                    v_harga = harga_grosir;
                }

                // var tp_item = null;
                var rtp_item = null;
                var v_jumlah = 1;
                // var v_harga = harga_eceran;
                var v_subtotal = v_harga;
                var v_nego_min = nego_min;
                var v_nego = null;
                var v_bonuses = [];
                var text_bonus = 'Tidak ada';

                if (custom_data) {
                    var status = transaksi_penjualan.status;
                    if (status == 'po_grosir') {
                        // v_harga = harga_grosir;
                        // v_subtotal = v_harga;
                        is_grosir = true;
                    }

                    // for (var i = 0; i < items.length; i++) {
                    //     if (items[i].kode == kode) {
                    //         tp_item = items[i];
                    //         break;
                    //     }
                    // }

                    for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                        if (relasi_transaksi_penjualan[i].item_kode == kode) {
                            rtp_item = relasi_transaksi_penjualan[i];
                            v_bonuses = rtp_item.relasi_bonus_penjualan;
                            break;
                        }
                    }

                    text_bonus = '';
                    if (bonuses.length > 0) {
                        for (var i = 0; i < v_bonuses.length; i++) {
                            text_bonus += v_bonuses[i].jumlah+' '+v_bonuses[i].bonus.nama;
                            if (i != v_bonuses.length - 1) text_bonus += ', '
                        }
                    } else {
                        text_bonus += 'Tidak ada';
                    }

                    v_jumlah = rtp_item.jumlah;
                    v_harga = rtp_item.harga;
                    v_subtotal = rtp_item.subtotal;

                    if (rtp_item.nego != null) {
                        v_nego = rtp_item.nego;
                    }
                }

                var index_jumlah = 0;
                var v_jumlah1 = 1;
                var v_jumlah2 = 0;
                var v_satuan_id1 = 0;
                var v_satuan_id2 = 0;
                var v_satuan_kode1 = '';
                var v_satuan_kode2 = '';
                var v_konversi1 = 0;
                var v_konversi2 = 0;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var konversi = satuan.konversi;
                    if (v_jumlah / konversi >= 1) {
                        if (index_jumlah == 0) {
                            v_jumlah1 = Math.floor(v_jumlah / konversi);
                            v_satuan_id1 = satuan.satuan.id;
                            v_satuan_kode1 = satuan.satuan.kode;
                            v_konversi1 = konversi;
                        } else {
                            v_jumlah2 = Math.floor(v_jumlah / konversi);
                            v_satuan_id2 = satuan.satuan.id;
                            v_satuan_kode2 = satuan.satuan.kode;
                            v_konversi2 = konversi;
                        }
                        index_jumlah++;
                        v_jumlah = v_jumlah % konversi;
                    }
                }

                if (custom_data) v_nego_min *= rtp_item.jumlah;

                v_jumlah1 = parseFloat(v_jumlah1);
                v_jumlah2 = parseFloat(v_jumlah2);
                v_konversi1 = parseFloat(v_konversi1);
                v_konversi2 = parseFloat(v_konversi2);
                v_harga = parseFloat(v_harga);
                v_subtotal = parseFloat(v_subtotal);
                v_nego_min = parseFloat(v_nego_min);
                v_nego = v_nego != null ? parseFloat(v_nego) : 0;

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < satuans.length; j++) {
                        if (satuans[j].id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: satuans[j].id,
                                kode: satuans[j].kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }

                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };
                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                // Ngurus view
                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah1[]" id="jumlah1-'+kode+'" value="'+(custom_data&&v_satuan_id1>0?v_jumlah1:1)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah2[]" id="jumlah2-'+kode+'" value="'+(custom_data&&v_satuan_id2>0?v_jumlah2:0)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan1[]" id="satuan1-'+kode+'" value="'+(custom_data&&v_satuan_id1>0?v_satuan_id1:satuan_terkecil.id)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan2[]" id="satuan2-'+kode+'" value="'+(custom_data&&v_satuan_id2>0?v_satuan_id2:satuan_terkecil.id)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="konversi[]" id="konversi-' +kode+'" value="'+satuan_terkecil.konversi+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="konversi1[]" id="konversi1-' +kode+'" value="'+(custom_data&&v_satuan_id1>0?v_konversi1:satuan_terkecil.konversi)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="konversi2[]" id="konversi2-' +kode+'" value="'+(custom_data&&v_satuan_id2>0?v_konversi2:satuan_terkecil.konversi)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="diskon[]" id="diskon-' + kode + '" value="'+diskon+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="'+harga_eceran+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga_eceran[]" id="harga_eceran-' + kode + '" value="'+harga_eceran+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga_grosir[]" id="harga_grosir-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-'+kode+'" class="subtotal" value="'+(custom_data?v_subtotal:'')+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="nego[]" id="nego-'+kode+'" class="nego" value="'+(custom_data?v_nego:'')+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="nego_min[]" id="nego_min-'+kode+'" class="nego_min" value="'+nego_min+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="stoktotal[]" id="stoktotal-'+kode+'" class="stoktotal" value="'+stoktotal+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="is_grosir[]" id="is_grosir-'+kode+'" class="is_grosir" value="'+is_grosir+'" />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="vertical-align: middle;">'+nama+'</td>'+
                            '<td id="inputJumlahItemContainer">'+
                                '<div class="row">'+
                                    '<div class="col-md-6" style="padding-right: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem1" id="inputJumlahItem1" class="form-control input-sm" value="1" />'+
                                            '<div id="pilihSatuan1" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-6" style="padding-left: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem2" id="inputJumlahItem2" class="form-control input-sm" />'+
                                            '<div id="pilihSatuan2" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + v_harga.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputSubTotalContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka inputSubTotal" value="' + v_subtotal.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputNegoContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            '<input type="checkbox" name="checkNego" id="checkNego" '+(custom_data&&v_nego>0?'checked="checked"':'')+' />'+
                                        '</div>'+
                                        '<input type="hidden" name="inputNegoMin" id="inputNegoMin" class="form-control input-sm" value="' + v_nego_min + '" />'+(custom_data&&v_nego>0?
                                        '<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" value="'+v_nego.toLocaleString()+'" />':'<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" readonly="readonly" />')+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="vertical-align: middle;">'+
                                '<div id="bonusContainer">'+
                                    text_bonus+
                                '</div>'+
                            '</td>'+
                        '</tr>');
                }

                var tr = $('tr[data-id="'+kode+'"]');
                tr.find('#inputJumlahItem1').val(v_jumlah1);
                tr.find('#pilihSatuan1').find('.text').text(v_satuan_kode1+' ');
                if (v_satuan_id2 != 0) {
                    tr.find('#inputJumlahItem2').val(v_jumlah2);
                    tr.find('#pilihSatuan2').find('.text').text(v_satuan_kode2+' ');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr_stoktotal = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr_stoktotal);

                // Ngurus harga
                if (custom_data) {
                    $('#tabelKeranjang tbody tr:last-child').find('#inputJumlahItem1').trigger('keyup');

                    var harga_total = parseFloat($('input[name="harga_total"]').val());
                    harga_total = (isNaN(harga_total) || harga_total < 0) ? 0 : harga_total;
                    harga_total += v_subtotal;
                    $('input[name="harga_total"]').val(harga_total);

                    var $nego_total_min = $('#inputNegoTotalMin');
                    var nego_total_min = parseFloat($nego_total_min.val());
                    nego_total_min = (isNaN(nego_total_min) || nego_total_min < 0) ? 0 : nego_total_min;
                    nego_total_min = nego_total_min + nego_min * rtp_item.jumlah
                    $nego_total_min.val(nego_total_min);

                    if (custom_data_last_item) {
                        var nego_total = parseFloat(transaksi_penjualan.nego_total);
                        nego_total = (isNaN(nego_total) || nego_total < 0) ? 0 : nego_total;
                        // $('input[name="nego_total"]').val(nego_total);

                        // var nego_total_data = parseFloat($('input[name="nego_total"]'));
                        // nego_total_data = (isNaN(nego_total_data) || nego_total_data < 0) ? 0 : nego_total_data;
                        var nego_total_data = 0;
                        for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                            var relasi = relasi_transaksi_penjualan[i];
                            nego_total_data += parseFloat(relasi.nego);
                        }

                        if (nego_total < nego_total_data) {
                            $('#inputNegoTotal').val(nego_total.toLocaleString());
                            $('#checkNegoTotal').prop('checked', true);
                            $('#checkNegoTotal').trigger('change');
                        }
                    }
                } else {
                    var $harga_total = $('#inputHargaTotal');
                    var $nego_total_min = $('#inputNegoTotalMin');

                    var harga_total = parseFloat($harga_total.val().replace(/\D/g, ''), 10);
                    var nego_total_min = parseFloat($nego_total_min.val());

                    harga_total = (isNaN(harga_total) || harga_total < 0) ? 0 : harga_total;
                    nego_total_min = (isNaN(nego_total_min) || nego_total_min < 0) ? 0 : nego_total_min;

                    $('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + harga_eceran);
                    $nego_total_min.val(nego_total_min + nego_min);
                }

                tr.find('#inputJumlahItem1').trigger('keyup');

                updateHargaOnKeyup();

                $('#spinner').hide();
            });
        }

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            banks = '{{ $banks }}';
            banks = banks.replace(/&quot;/g, '"');
            banks = JSON.parse(banks);

            users = '{{ $users }}';
            users = users.replace(/&quot;/g, '"');
            users = JSON.parse(users);

            items = '{{ $items }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            pelanggans = '{{ $pelanggans }}';
            pelanggans = pelanggans.replace(/&quot;/g, '"');
            pelanggans = JSON.parse(pelanggans);

            transaksi_penjualan = '{{ $transaksi_penjualan }}';
            transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
            transaksi_penjualan = JSON.parse(transaksi_penjualan);

            relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            $(".select2_single").select2({
                width: '100%'
                // allowClear: true
            });

            if (transaksi_penjualan.pelanggan_id != null) {
                // $('#pilihPelanggan').val(transaksi_penjualan.pelanggan_id).trigger('change');
                $("#pilihPelanggan").select2('val', transaksi_penjualan.pelanggan_id+'');
                handlePelangganChange(transaksi_penjualan.pelanggan_id);
                // console.log('ready');
            }

            if (transaksi_penjualan.pengirim == null) {
                $('#btnDiambil').trigger('click');
            } else {
                $('#btnDikirim').trigger('click');
                var user_id = '-';
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    if (transaksi_penjualan.pengirim == user.nama) {
                        user_id = user.id;
                        break;
                    }
                }
                if (user_id == '-') {
                    // pengirim lain
                    $('select[name="user_id"]').val('-').trigger('change');
                    $('select[name="user_id"]').select2('val', '-');
                    $('#inputPengirimLain').prop('disabled', false);

                    $('#inputPengirimLain').val(transaksi_penjualan.pengirim);
                    $('input[name="pengirim_lain"]').val(transaksi_penjualan.pengirim);
                } else {
                    // pengirim dari karyawan
                    $('#inputPengirimLain').prop('disabled', true);
                    $('input[name="pengirim"]').val(user_id);
                    $('input[name="pengirim_lain"]').val('');
                }
                $('select[name="user_id"]').select2('val', user_id+'');
                updateHargaOnKeyup();
            }

            // var v_harga_total = 0;
            // var tp_harga_total = parseFloat(transaksi_penjualan.harga_total);
            // var tp_nego_total = parseFloat(transaksi_penjualan.nego_total);
            // var rtp_nego_total = 0;
            for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                var relasi = relasi_transaksi_penjualan[i];
                // rtp_nego_total += parseFloat(relasi.nego);

                if (i == relasi_transaksi_penjualan.length - 1) custom_data_last_item = true;

                handleItemChange(relasi.item_kode, true);
            }

            // if (tp_nego_total < rtp_nego_total) {
            //     console.log('oi');
            //     $('#checkNegoTotal').prop('checked', true).trigger('change');
            //     $('#inputNegoTotal').val(tp_nego_total.toLocaleString());
            //     $('input[name="nego_total_view"]').val(tp_nego_total);
            //     updateHargaOnKeyup();
            // } else {
            //     console.log('io');
            //     $('#checkNegoTotal').prop('checked', true).trigger('change');
            // }

            // var v_harga_total = 0;
            // // var tp_harga_total = parseFloat(transaksi_penjualan.harga_total);
            // var tp_nego_total = parseFloat(transaksi_penjualan.nego_total);
            // for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
            //     var relasi = relasi_transaksi_penjualan[i];
            //     var subtotal = parseFloat(relasi.subtotal);
            //     v_harga_total += subtotal;

            //     $('#pilihItem').val(relasi.item_kode);
            //     handleItemChange(relasi.item_kode, true);
            // }

            // if (v_harga_total != tp_nego_total) {
            //     $('#checkNegoTotal').prop('checked', true);
            //     $('#inputNegoTotal').prop('readonly', false);
            //     $('#inputNegoTotal').val(tp_nego_total.toLocaleString());
            //     $('input[name="nego_total"]').val(tp_nego_total);
            //     updateHargaOnKeyup();
            // } else {
            //     $('#checkNegoTotal').prop('checked', true).trigger('change');
            // }

            $('input[name="kode_transaksi"]').val(transaksi_penjualan.kode_transaksi);
            // $('#kodeTransaksiTitle').text(transaksi_penjualan.kode_transaksi);

            // $('#pilihanPotonganContainer').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTunaiContainer').find('input').val('').trigger('change');
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('').trigger('change');
            $('#inputTransferBankContainer').find('select').val('').trigger('change');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('').trigger('change');
            $('#inputKartuContainer').find('select').val('').trigger('change');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('').trigger('change');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('').trigger('change');
            $('#inputTitipanContainer').hide();
            $('#inputTitipanContainer').find('input').val('').trigger('change');

            var harga_total = transaksi_penjualan.harga_total;
            var ongkos_kirim = transaksi_penjualan.ongkos_kirim;
            var jumlah_bayar = transaksi_penjualan.jumlah_bayar;
            var nominal_tunai = transaksi_penjualan.nominal_tunai;
            var no_transfer = transaksi_penjualan.no_transfer;
            var bank_transfer = transaksi_penjualan.bank_transfer;
            var nominal_transfer = transaksi_penjualan.nominal_transfer;
            var no_kartu = transaksi_penjualan.no_kartu;
            var nominal_kartu = transaksi_penjualan.nominal_kartu;
            var no_cek = transaksi_penjualan.no_cek;
            var nominal_cek = transaksi_penjualan.nominal_cek;
            var no_bg = transaksi_penjualan.no_bg;
            var nominal_bg = transaksi_penjualan.nominal_bg;
            var nominal_titipan = transaksi_penjualan.nominal_titipan;

            harga_total = harga_total ? parseFloat(transaksi_penjualan.harga_total) : 0;
            ongkos_kirim = ongkos_kirim ? parseFloat(transaksi_penjualan.ongkos_kirim) : 0;
            jumlah_bayar = jumlah_bayar ? parseFloat(transaksi_penjualan.jumlah_bayar) : 0;
            nominal_tunai = nominal_tunai ? parseFloat(transaksi_penjualan.nominal_tunai) : 0;
            nominal_transfer = nominal_transfer ? parseFloat(transaksi_penjualan.nominal_transfer) : 0;
            nominal_kartu = nominal_kartu ? parseFloat(transaksi_penjualan.nominal_kartu) : 0;
            nominal_cek = nominal_cek ? parseFloat(transaksi_penjualan.nominal_cek) : 0;
            nominal_bg = nominal_bg ? parseFloat(transaksi_penjualan.nominal_bg) : 0;
            nominal_titipan = nominal_titipan ? parseFloat(transaksi_penjualan.nominal_titipan) : 0;

            // if (jumlah_bayar > harga_total) {
                nominal_tunai = jumlah_bayar - nominal_transfer - nominal_kartu - nominal_cek - nominal_bg - nominal_titipan;
            // }

            if (nominal_tunai > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnTunai').trigger('click');
                $('#inputNominalTunai').val(nominal_tunai.toLocaleString());
                // Set nominal_tunai
                $('input[name="nominal_tunai"]').val(nominal_tunai);
            }

            if (nominal_transfer > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnTransfer').trigger('click');
                $('#inputNoTransfer').val(no_transfer);
                $('#inputNominalTransfer').val(nominal_transfer.toLocaleString());
                $('select[name="bank_id"]').val(bank_transfer).trigger('change');
                // Set nominal_transfer
                $('input[name="no_transfer"]').val(no_transfer);
                $('input[name="nominal_transfer"]').val(nominal_transfer);
            }

            if (nominal_kartu > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnKartu').trigger('click');
                $('#inputNoKartu').val(no_kartu);
                $('#inputNominalKartu').val(nominal_kartu.toLocaleString());
                // Set nominal_kartu
                $('input[name="no_kartu"]').val(no_kartu);
                $('input[name="nominal_kartu"]').val(nominal_kartu);
            }

            if (nominal_cek > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnCek').trigger('click');
                $('#inputNoCek').val(no_cek);
                $('#inputNominalCek').val(nominal_cek.toLocaleString());
                // Set nominal_cek
                $('input[name="no_cek"]').val(no_cek);
                $('input[name="nominal_cek"]').val(nominal_cek);
            }

            if (nominal_bg > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnBG').trigger('click');
                $('#inputNoBG').val(no_bg);
                $('#inputNominalBG').val(nominal_bg.toLocaleString());
                // Set nominal_bg
                $('input[name="no_bg"]').val(no_bg);
                $('input[name="nominal_bg"]').val(nominal_bg);
            }

            if (nominal_titipan > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnTitipan').trigger('click');
                $('#inputNominalTitipan').val(nominal_titipan.toLocaleString());
                // Set nominal_titipan
                $('input[name="nominal_titipan"]').val(nominal_titipan);
            }

            // console.log('oi');
            if (jumlah_bayar > 0) {
                $('#inputJumlahBayar').val(jumlah_bayar.toLocaleString());
                $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            }

            if (ongkos_kirim > 0) {
                $('#inputOngkosKirim').val(ongkos_kirim.toLocaleString());
                $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            }

            updateHargaOnKeyup();

            $('#spinner').hide();
        });

        // var keyupFromScanner = false;
        $(document).scannerDetection({
            avgTimeByChar: 40,
            onComplete: function(code, qty) {
                console.log('Kode: ' + code, qty);
            },
            onError: function(error) {
                // console.log('Barcode: ' + error);
                var kode = error;
                var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
                var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

                var terpilih = false;
                $('input[name="item_kode[]"]').each(function(index, el) {
                    if ($(el).val() == kode) {
                        terpilih = true;
                    }
                });

                if (terpilih) {
                    // keyupFromScanner = true;
                    var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val());
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val(jumlah_awal + 1);
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('keyup');
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('blur');
                } else {
                    cariItem(kode, url, tr);
                }
            }
        });

        $(document).on('change', 'select[name="pelanggan_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            // $('#pilihPelanggan').val(id);
            handlePelangganChange(id);
            // console.log('change');
        });

        $(document).on('click', '#resetPelanggan', function(event) {
            event.preventDefault();

            $('select[name="pelanggan_id"]').val('');
            $('select[name="pelanggan_id"]').trigger('change');
            $('input[name="kode_pelanggan"]').val('');
            $('input[name="kode_pelanggan"]').removeClass('has-error');
            $('input[name="nama_pelanggan"]').val('');
            $('#inputNamaPelanggan').prop('readonly', false);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputKodePelanggan', function(event) {
            event.preventDefault();

            var ada_yang_sama = false;
            var kode_pelanggan = $(this).val();

            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }

            for (var i = 0; i < pelanggans.length; i++) {
                var pelanggan = pelanggans[i];
                if (kode_pelanggan == pelanggan.kode) {
                    ada_yang_sama = true;
                    break;
                }
            }

            if (ada_yang_sama) {
                $(this).parents('.form-group').addClass('has-error');
            } else {
                $(this).parents('.form-group').removeClass('has-error');
                $(this).val(kode_pelanggan);
                $('input[name="kode_pelanggan"]').val(kode_pelanggan);
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNamaPelanggan', function(event) {
            event.preventDefault();

            var nama_pelanggan = $(this).val();
            $('input[name="nama_pelanggan"]').val(nama_pelanggan);
            
            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var kode = $(this).val();
            handleItemChange(kode, false);

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');
        });

        /*var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah);
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var jumlah = $(this).val();
            temp_jumlah = jumlah;

            var kode = $(this).parents('tr').first().data('id');
            var satuan = $('#satuan-'+kode).val();
            var konversi = $('#konversi-'+kode).val();
            var stoktotal = $('#stoktotal-'+kode).val();
            
            var td = $(this).parents('td').first();

            if (jumlah == '') {
                jumlah = 0;
                td.addClass('has-error');
            } else {
                td.removeClass('has-error');
            }

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            satuan = parseFloat(satuan);
            if (isNaN(satuan) || satuan <= 0) satuan = 0;
            konversi = parseFloat(konversi);
            if (isNaN(konversi) || konversi <= 0) konversi = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.next().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#jumlah-'+kode).val(jumlah);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        var temp_jumlah_1 = 0;
        $(document).on('click', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_1 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem1', function(event) {
            event.preventDefault();


            $(this).val(temp_jumlah_1);
        });

        $(document).on('keyup', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_1 = $(this).val();

            var jumlah1 = parseFloat($(this).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah1-'+item_kode).val(jumlah1);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            td.removeClass('has-error');

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var konversi = parseFloat(data.konversi);
                            var harga_eceran = parseFloat(data.harga_eceran);
                            var harga_grosir = parseFloat(data.harga_grosir);
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            if (pelanggan_is_grosir) updateHargaKeGrosir();
                            else updateHargaKeEceran();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stok) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        var temp_jumlah_2 = 0;
        $(document).on('click', '#inputJumlahItem2', function(event) {
            event.preventDefault();


            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');
            
            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_2 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah_2);
        });

        $(document).on('keyup', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_2 = $(this).val();

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($(this).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah2 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah2-'+item_kode).val(jumlah2);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            td.removeClass('has-error');

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var konversi = parseFloat(data.konversi);
                            var harga_eceran = parseFloat(data.harga_eceran);
                            var harga_grosir = parseFloat(data.harga_grosir);
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            if (pelanggan_is_grosir) updateHargaKeGrosir();
                            else updateHargaKeEceran();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stok) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        $(document).on('click', '#pilihSatuan1 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan1').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($(this).find('a').attr('konversi'));
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan1-'+item_kode).val(satuan1.id);
                $('#konversi1-'+item_kode).val(satuan1.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            td.removeClass('has-error');

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var konversi = parseFloat(data.konversi);
                            var harga_eceran = parseFloat(data.harga_eceran);
                            var harga_grosir = parseFloat(data.harga_grosir);
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            if (pelanggan_is_grosir) updateHargaKeGrosir();
                            else updateHargaKeEceran();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stok) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan1.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        $(document).on('click', '#pilihSatuan2 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan2').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($(this).find('a').attr('konversi'));
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah2 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan2-'+item_kode).val(satuan2.id);
                $('#konversi2-'+item_kode).val(satuan2.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            td.removeClass('has-error');

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var konversi = parseFloat(data.konversi);
                            var harga_eceran = parseFloat(data.harga_eceran);
                            var harga_grosir = parseFloat(data.harga_grosir);
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            if (pelanggan_is_grosir) updateHargaKeGrosir();
                            else updateHargaKeEceran();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stok) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan2.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        /*$(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();

            var satuan = $(this).val();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            var kode = $(this).parents('tr').data('id');
            var stoktotal = $('#stoktotal-'+kode).val();
            var jumlah = $('#jumlah-'+kode).val();
            // var jumlah = $(this).parent().prev().children('input').val();

            if (jumlah === '') jumlah = 0;

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var td = $(this).parents('td').first();

            td.prev().find('#inputJumlahItem').val(jumlah.toLocaleString());

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var konversi = data.konversi.konversi;
                    konversi = parseFloat(konversi);
                    if (isNaN(konversi) || konversi <= 0) konversi = 0;

                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.prev().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);
                        // console.log(subtotal, nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#satuan-'+kode).val(satuan);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        $(document).on('change', '#checkNego', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            var kode = $(this).parents('tr').first().data('id');
            var harga_total = 0;

            if (checked) {
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').prop('readonly', false).focus();

                var nego = $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val();
                var nego_min = $(this).parents('tr[data-id="'+kode+'"]').find('#inputNegoMin').val();
                var jumlah = $(this).parents('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val();

                nego = parseFloat(nego.replace(/\D/g, ''), 10);

                // Success
                if (nego >= nego_min) {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('#subtotal-'+kode).val(nego);

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                } else {
                    $(this).parents('.form-group').addClass('has-error');
                    $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val('');
                    $('#nego-'+kode).val('');

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                }

                updateHargaOnKeyup();
            } else {
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val('');
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').prop('readonly', true);
                $(this).parents('.form-group').removeClass('has-error');

                // var subtotal = $(this).parents('tr[data-id="'+kode+'"]').find('#inputSubTotal').val();
                // subtotal = parseFloat(subtotal.replace(/\D/g, ''), 10) / 100;
                // $('#subtotal-'+kode).val(subtotal);
                $('#nego-'+kode).val('');
                // updateHargaTotal();
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNego', function(event) {
            event.preventDefault();

            var input = $(this);
            var nego = input.val();
            var kode = input.parents('tr').data('id');
            var nego_min = input.parents('tr[data-id="'+kode+'"]').find('#inputNegoMin').val();
            var jumlah = input.parents('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val();

            nego = parseFloat(nego.replace(/\D/g, ''), 10);

            // Success
            if (nego >= nego_min) {
                input.parents('.form-group').removeClass('has-error');
                // $('#subtotal-'+kode).val(nego);
                $('#nego-'+kode).val(nego);

                // updateHargaTotal();
                // updateHargaOnKeyup();
            } else {
                input.parents('.form-group').addClass('has-error');
                // var subtotal = input.parents('tr[data-id="'+kode+'"]').find('#inputSubTotal').val();
                // subtotal = parseFloat(subtotal.replace(/\D/g, ''), 10) / 100;
                // $('#subtotal-'+kode).val(subtotal);
                $('#nego-'+kode).val('');

                // updateHargaTotal();
                // updateHargaOnKeyup();
            }

            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnPotonganPersen', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).next().val(true);

                $(this).parent().next().find('button').removeClass('btn-primary');
                $(this).parent().next().find('button').addClass('btn-default');
                $(this).parent().next().find('button').next().val(false);

                $.get(url, function(data) {
                    var persen = data.pelanggan.diskon_persen;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total = harga_total - (harga_total * (persen/100));

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('click', '#btnPotonganTunai', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;
            
            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).next().val(true);

                $(this).parent().prev().find('button').removeClass('btn-danger');
                $(this).parent().prev().find('button').addClass('btn-default');
                $(this).parent().prev().find('button').next().val(false);

                $.get(url, function(data) {
                    var potongan = data.pelanggan.potongan;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total -= potongan;

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('change', '#checkNegoTotal', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('#inputNegoTotal').prop('readonly', false).focus();

                var nego_total = $('#inputNegoTotal').val();
                var nego_total_min = $('#inputNegoTotalMin').val();

                nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
                if (isNaN(nego_total)) nego_total = 0;

                nego_total_min = parseFloat(nego_total_min);
                if (isNaN(nego_total_min)) nego_total_min = 0;
                
                // Success
                if (nego_total >= nego_total_min) {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('input[name="nego_total_view"]').val(nego_total);
                    // $('input[name="nego_total"]').val(nego_total);
                    // updateHargaOnKeyup();
                } else {
                    $(this).parents('.form-group').addClass('has-error');
                    $('input[name="nego_total_view"]').val('');
                    // $('input[name="nego_total"]').val('');
                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                }

                updateHargaOnKeyup();
            } else {
                $('#inputNegoTotal').val('');
                $('#inputNegoTotal').prop('readonly', true);
                $(this).parents('.form-group').removeClass('has-error');

                // var harga_total = $('#inputHargaTotal').val();
                // harga_total = parseFloat(harga_total.replace(/\D/g, ''), 10);
                // $('input[name="nego_total"]').val('');
                $('input[name="nego_total_view"]').val('');
                // updateHargaTotal();
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNegoTotal', function(event) {
            event.preventDefault();

            var nego_total = $('#inputNegoTotal').val();
            var nego_total_min = $('#inputNegoTotalMin').val();

            nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
            if (isNaN(nego_total)) nego_total = 0;

            nego_total_min = parseFloat(nego_total_min);
            if (isNaN(nego_total_min)) nego_total_min = 0;
            
            // Success
            if (nego_total >= nego_total_min) {
                // $('#labelLabaTotal').show();
                // $('#labelRugiTotal').hide();
                $(this).parents('.form-group').removeClass('has-error');
                // $(this).parents('.form-group').addClass('has-success');
                $('input[name="nego_total_view"]').val(nego_total);
            } else {
                // $('#labelLabaTotal').hide();
                // $('#labelRugiTotal').show();
                // $(this).parents('.form-group').removeClass('has-success');
                $(this).parents('.form-group').addClass('has-error');
                $('input[name="nego_total_view"]').val('');
            }

            // $('input[name="nego_total_view"]').val(nego_total);
            updateHargaOnKeyup();
        });

        /*$(document).on('keyup', '#inputNegoTotal', function(event) {
            event.preventDefault();

            var input = $(this);
            var nego_total_view = input.val();
            var nego_total_min = $('#inputNegoTotalMin').val();
            var nego_total_data = $('input[name="nego_total"]').val();

            nego_total_view = parseFloat(nego_total_view.replace(/\D/g, ''), 10);
            nego_total_min = parseFloat(nego_total_min);
            nego_total_data = parseFloat(nego_total_data);
            
            // Success
            if (nego_total_view >= nego_total_min) {
                if (nego_total_data > 0) {
                    if (nego_total_view < nego_total_data) {
                        $(this).parents('.form-group').removeClass('has-error');
                        $('input[name="nego_total"]').val(nego_total_view);
                        updateHargaOnKeyup();
                    } else {
                        $(this).parents('.form-group').removeClass('has-error');
                        $('input[name="nego_total"]').val(nego_total_data);
                        updateHargaOnKeyup();
                    }
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('input[name="nego_total"]').val(nego_total_view);
                    updateHargaOnKeyup();
                }
            } else {
                $(this).parents('.form-group').addClass('has-error');
                // $('input[name="nego_total"]').val('');
                updateHargaTotal();
                updateHargaOnKeyup();
            }
        });*/

        $(document).on('keyup', '#inputOngkosKirim', function(event) {
            event.preventDefault();

            var ongkos_kirim = parseFloat($(this).val().replace(/\D/g, ''), 10);
            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('input[name="no_transfer"]').val('');
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');

                    $(this).find('select[name="bank_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="no_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');

                    $(this).find('select[name="bank_kartu"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu"]').val('').trigger('change');
                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTitipan', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                $(this).find('i').show('fast');
                $('#inputTitipanContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTitipanContainer').hide('fast', function() {
                    $('input[name="nominal_titipan"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDiambil', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirmContainer').hide('fast', function() {
                    $('select[name="user_id"]').select2('val', '');
                    $('select[name="user_id"]').val('').trigger('change');
                    $('#inputPengirimLain').val('');
                    $('#formSimpanContainer').find('input[name="pengirim"]').val('');
                    $('#formSimpanContainer').find('input[name="pengirim_lain"]').val('');
                    
                    updateHargaOnKeyup();
                });
            }
            $('#btnDikirim').removeClass('btn-primary');
            $('#btnDikirim').addClass('btn-default');
            $('#btnDikirim').find('i').removeClass('fa-check');
        });

        $(document).on('click', '#btnDikirim', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirmContainer').show('fast', function() {
                    updateHargaOnKeyup();
                });
            }
            $('#btnDiambil').removeClass('btn-success');
            $('#btnDiambil').addClass('btn-default');
            $('#btnDiambil').find('i').removeClass('fa-check');
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // $(this).val(nominal_tunai.toLocaleString());
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_transfer"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_transfer += '';
            //     nominal_transfer = nominal_transfer.slice(0, -1);
            //     nominal_transfer = parseFloat(nominal_transfer);
            //     if (isNaN(nominal_transfer)) nominal_transfer = 0;

            //     // $(this).val(nominal_transfer.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            // $(this).val(nominal_kartu.toLocaleString());
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_kartu += '';
            //     nominal_kartu = nominal_kartu.slice(0, -1);
            //     nominal_kartu = parseFloat(nominal_kartu);
            //     if (isNaN(nominal_kartu)) nominal_kartu = 0;

            //     $(this).val(nominal_kartu.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal_kartu);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            // $(this).val(nominal_cek.toLocaleString());
            $('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_cek += '';
            //     nominal_cek = nominal_cek.slice(0, -1);
            //     nominal_cek = parseFloat(nominal_cek);
            //     if (isNaN(nominal_cek)) nominal_cek = 0;

            //     $(this).val(nominal_cek.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNoBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            // $(this).val(nominal_bg.toLocaleString());
            $('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_bg += '';
            //     nominal_bg = nominal_bg.slice(0, -1);
            //     nominal_bg = parseFloat(nominal_bg);
            //     if (isNaN(nominal_bg)) nominal_bg = 0;

            //     $(this).val(nominal_bg.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNominalTitipan', function(event) {
            event.preventDefault();

            var nominal_titipan = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_titipan_max = parseFloat($('input[name="titipan"]').val());

            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(nominal_titipan_max)) nominal_titipan_max = 0;

            if (nominal_titipan < nominal_titipan_max) {
                $(this).parents('.input-group').first().removeClass('has-error');
                // $(this).val(nominal_titipan.toLocaleString());
                $('input[name="nominal_titipan"]').val(nominal_titipan);
                updateHargaOnKeyup();

                // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
                // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

                // if (isNaN(nominal_tunai)) nominal_tunai = 0;
                // if (isNaN(kembali)) kembali = 0;

                // if (nominal_tunai <= 0 && kembali > 0) {
                //     nominal_titipan += '';
                //     nominal_titipan = nominal_titipan.slice(0, -1);
                //     nominal_titipan = parseFloat(nominal_titipan);
                //     if (isNaN(nominal_titipan)) nominal_titipan = 0;

                //     $(this).val(nominal_titipan.toLocaleString());
                //     $('input[name="nominal_titipan"]').val(nominal_titipan);
                //     updateHargaOnKeyup();
                // }
            } else {
                $(this).parents('.input-group').first().addClass('has-error');
                updateHargaOnKeyup();
            }
        });

        $(document).on('change', 'select[name="user_id"]', function(event) {
            event.preventDefault();

            var user_id = $(this).val();
            if (user_id == '-') {
                $('#inputPengirimLain').prop('disabled', false);
                $('input[name="pengirim"]').val('');
            } else {
                $('#inputPengirimLain').prop('disabled', true);
                $('#inputPengirimLain').val('');
                $('input[name="pengirim"]').val(user_id);
                $('input[name="pengirim_lain"]').val('');
            }
            
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputPengirimLain', function(event) {
            event.preventDefault();

            var pengirim_lain = $(this).val();
            $('input[name="pengirim_lain"]').val(pengirim_lain);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnSimpanPO', function(event) {
            event.preventDefault();

            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
            // if (is_grosir && transaksi_penjualan.status == 'po_eceran') $('input[name="grosir"]').val(false);
            // console.log(transaksi_penjualan.status, is_grosir);
            var action = "{{ url('transaksi-grosir/simpan-po') }}" + '/' + transaksi_penjualan.id;
            $('#form-simpan').attr('action', action);
            $('#form-simpan').submit();
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            var index = selected_items.indexOf(kode);
            if (index >= -1) {
                selected_items.splice(index, 1);
            }

            var inputNegoContainer = tr.find('#inputNegoContainer');
            var checked = tr.find('#checkNego').prop('checked');
            var nego = parseFloat(tr.find('#inputNego').val().replace(/\D/g, ''), 10);;
            var nego_min = parseFloat(tr.find('#inputNegoMin').val());
            var subtotal = 0;
            if (checked && !inputNegoContainer.hasClass('has-error')) {
                subtotal = nego;
            } else {
                subtotal = parseFloat(tr.find('#inputSubTotal').val().replace(/\D/g, ''), 10);
            }

            var harga_total = parseFloat($('input[name="harga_total"]').val().replace(/\D/g, ''), 10);
            var nego_total_min = parseFloat($('#inputNegoTotalMin').val().replace(/\D/g, ''), 10);
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('#inputTotalKembali').val().replace(/\D/g, ''), 10);
            // console.log(harga_total, subtotal);

            harga_total -= subtotal;
            nego_total_min -= nego_min;
            harga_total_plus_ongkos_kirim -= subtotal;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            if (isNaN(harga_total) || harga_total == 0) harga_total = 0;
            if (isNaN(nego_total_min) || nego_total_min == 0) nego_total_min = '';
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('#inputNegoTotalMin').val(nego_total_min);
            $('#inputHargaTotalPlusOngkosKirim').val(harga_total_plus_ongkos_kirim.toLocaleString());
            $('#inputTotalKembali').val(kembali.toLocaleString());

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');

            $('input[name="harga_total"]').val(harga_total);
            // $('input[name="kembali"]').val(kembali);

            if (selected_items.length <= 0) {
                $('#level_pelanggan').removeClass('label-success');
                $('#level_pelanggan').removeClass('label-warning');
                $('#tipe_penjualan').removeClass('label-success');
                $('#tipe_penjualan').removeClass('label-warning');
            }

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();

            updateHargaOnKeyup();
        });

    </script>
@endsection
