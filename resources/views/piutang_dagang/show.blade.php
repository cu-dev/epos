@extends('layouts.admin')

@section('title')
    @if (intval($piutang->sisa) > 0)
        <title>EPOS | Detail Piutang Dagang</title>
    @else
        <title>EPOS | Data Riwayat Pembayaran Piutang</title>
    @endif
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnKembali {
            margin: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    @if (intval($piutang->sisa) > 0)
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Bayar Piutang Dagang</h2>
                <a href="{{ url('piutang-dagang') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">Kode Transaksi</label>
                            <input type="text" id="inputKodeTransaksi" class="form-control" readonly="readonly" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Transaksi Penjualan</label>
                            <input type="text" class="form-control" readonly="readonly" value="{{ $piutang->transaksi_penjualan->kode_transaksi }}" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sisa Piutang</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" class="form-control" readonly="readonly" value="{{ \App\Util::angka($piutang->sisa) }}" />
                                <input type="hidden" id="inputSisaPiutang" value="{{ $piutang->sisa }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Jumlah Dibayar</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" class="form-control" id="inputJumlahBayar" readonly="readonly" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">Metode Pembayaran</label>
                            <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnDebit" class="btn btn-default"><i class="fa"></i> Transfer</button>
                                </div><div class="btn-group" role="group">
                                    <button type="button" id="btnKartu" class="btn btn-default"><i class="fa"></i> Kartu</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnCek" class="btn btn-default"><i class="fa"></i> Cek</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnBG" class="btn btn-default"><i class="fa"></i> BG</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="inputTunaiContainer">
                            <label class="control-label">Nominal Tunai (Rp)</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group" id="inputDebitContainer">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <label class="control-label">Nama Bank</label>
                                    <select name="bank" class="select2_single form-control">
                                        <option id="default-bank" value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor Transfer</label>
                                    {{-- <input type="text" name="inputNoDebit" id="inputNoDebit" class="form-control" /> --}}
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoDebit" id="inputNoDebit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalDebit" id="inputNominalDebit" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="inputKartuContainer">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <label class="control-label">Nama Bank Kartu</label>
                                    <select name="bank_kartu" class="select2_single form-control">
                                        <option id="default-bank" value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <label class="control-label">Jenis Kartu</label>
                                    <select name="jenis_kartu" class="select2_single form-control">
                                        <option id="default-bank" value="">Pilih Jenis Kartu</option>
                                        <option value="debet">Kartu Debit</option>
                                        <option value="kredit">Kartu Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor Transfer Kartu</label>
                                    {{-- <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control" /> --}}
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal Kartu</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="inputCekContainer">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor Cek</label>
                                    {{-- <input type="text" name="inputNoCek" id="inputNoCek" class="form-control" /> --}}
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoCek" id="inputNoCek" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="inputBgContainer">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor BG</label>
                                    {{-- <input type="text" name="inputNoBg" id="inputNoBg" class="form-control" /> --}}
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoBg" id="inputNoBg" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalBg" id="inputNominalBg" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="formSimpanContainer" style="margin-top: 20px;">
                            <form id="form-simpan" action="{{ url('piutang-dagang/'.$piutang->id) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="kode_transaksi" />
                                <input type="hidden" name="bank_id" />
                                <input type="hidden" name="jumlah_bayar" />
                                
                                <input type="hidden" name="nominal_tunai" />
                                
                                <input type="hidden" name="no_debit" />
                                <input type="hidden" name="nominal_debit" />
                                
                                <input type="hidden" name="no_cek" />
                                <input type="hidden" name="nominal_cek" />
                                
                                <input type="hidden" name="no_bg" />
                                <input type="hidden" name="nominal_bg" />

                                <input type="hidden" name="no_kartu" />
                                <input type="hidden" name="bank_kartu" />
                                <input type="hidden" name="nominal_kartu" />
                                <input type="hidden" name="jenis_kartu" />


                                <div class="clearfix">
                                    <div class="form-group pull-left">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i> Bayar
                                        </button>   
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Riwayat Pembayaran Piutang - Kode Transaksi Penjualan: {{ $piutang->transaksi_penjualan->kode_transaksi }}</h2>
                <a href="{{ url('piutang-dagang') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-hover table-striped" id="tabelLogPiutang">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Kode Transaksi</th>
                            <th>Tanggal Pembayaran</th>
                            <th>Pembayaran Via</th>
                            <th>Jumlah Dibayar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bayar_piutangs as $i => $bayar_piutang)
                        <tr>
                            <td class="tengah-hv">{{ ++$i }}</td>
                            <td class="tengah-v">{{ $bayar_piutang->kode_transaksi }}</td>
                            <td class="tengah-v">{{ $bayar_piutang->created_at->format('d-m-Y H:i:s') }}</td>
                            <td class="tengah-hv">
                                @if (!empty($bayar_piutang->nominal_tunai))
                                <span class="label label-danger">Tunai</span>
                                @endif
                                @if (!empty($bayar_piutang->nominal_debit))
                                <span class="label label-warning">Transfer</span>
                                @endif
                                @if (!empty($bayar_piutang->nominal_kartu))
                                <span class="label label-info">Kartu</span>
                                @endif
                                @if (!empty($bayar_piutang->nominal_cek))
                                <span class="label label-success">Cek</span>
                                @endif
                                @if (!empty($bayar_piutang->nominal_bg))
                                <span class="label label-BG">BG</span>
                                @endif
                            </td>
                            <td class="tengah-v">{{ \App\Util::duit($bayar_piutang->jumlah_bayar) }}</td>
                            <td>
                                <a href="{{ url('piutang-dagang/'.$piutang->id.'/view-log/'.$bayar_piutang->id) }}" class="btn btn-xs btn-info" title="Detail Pembayaran" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pembayaran Piutang Dagang Berhasil!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pembayaran Piutang Dagang Gagal!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        function updateHargaOnKeyup() {
            var $sisa_piutang  = $('#inputSisaPiutang');
            var $jumlah_bayar = $('#inputJumlahBayar');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_debit = $('#formSimpanContainer').find('input[name="nominal_debit"]').val();
            var nominal_kartu = $('#formSimpanContainer').find('input[name="nominal_kartu"]').val();
            var nominal_cek   = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
            var nominal_bg    = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();

            nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_debit = parseInt(nominal_debit.replace(/\D/g, ''), 10);
            nominal_kartu = parseInt(nominal_kartu.replace(/\D/g, ''), 10);
            nominal_cek   = parseInt(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg    = parseInt(nominal_bg.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_debit)) nominal_debit = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var sisa_piutang = $sisa_piutang.val();
            var jumlah_bayar = nominal_tunai + nominal_debit + nominal_kartu + nominal_cek + nominal_bg;

            if (isNaN(sisa_piutang)) sisa_piutang = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

            $sisa_piutang.val(sisa_piutang.toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());

            $('input[name="sisa_piutang"]').val(sisa_piutang);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
        }

        $('#tabelLogPiutang').DataTable();

        $(document).ready(function() {
            var url = "{{ url('piutang-dagang') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $('#inputTunaiContainer').hide();
            $('#inputDebitContainer').hide();
            $('#inputDebitContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBgContainer').hide();
            $('#inputBgContainer').find('input').val('');

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#inputHargaTotal').val(0);
            $('#inputNegoTotal').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);

            $('select[name="bank"]').next('span').css({
                'width': '100%',
                'margin-bottom': '10px'
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url   = "{{ url('piutang-dagang/last/json') }}";
            var bulan = printBulanSekarang('mm/yyyy');

            $.get(url, function(data) {
                if (data.piutang === null) {
                    var kode_transaksi = '0001/BPD/' + bulan;
                } else {
                    var kode_transaksi  = data.piutang.kode_transaksi;
                    var mm_transaksi    = kode_transaksi.split('/')[2];
                    var yyyy_transaksi  = kode_transaksi.split('/')[3];
                    var bulan_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (bulan !== bulan_transaksi) {
                        kode_transaksi = '0001/BPD/' + bulan;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/BPD/' + bulan_transaksi;
                    }
                }

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#inputKodeTransaksi').val(kode_transaksi);
            });
        });

        $(document).on('change', 'select[name="bank"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_id"]').val(id);
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_kartu"]').val(id);
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="jenis_kartu"]').val(id);
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnDebit', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').addClass('fa-check');
                $('#inputDebitContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputDebitContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_debit"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_debit"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').addClass('fa-check');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputKartuContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_kartu"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_kartu"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputCekContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                $(this).find('i').addClass('fa-check');
                $('#inputBgContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputBgContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;

            $(this).val(nominal_tunai.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoDebit', function(event) {
            event.preventDefault();

            var no_debit = $(this).val();
            $('input[name="no_debit"]').val(no_debit);
        });

        $(document).on('keyup', '#inputNominalDebit', function(event) {
            event.preventDefault();

            var nominal_debit = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_debit)) nominal_debit = 0;

            $(this).val(nominal_debit.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_debit"]').val(nominal_debit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;

            $(this).val(nominal_kartu.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;

            $(this).val(nominal_cek.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoBg', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
        });

        $(document).on('keyup', '#inputNominalBg', function(event) {
            event.preventDefault();

            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;

            $(this).val(nominal_bg.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();
        });

    </script>
@endsection
