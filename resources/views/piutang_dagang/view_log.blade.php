@extends('layouts.admin')

@section('title')
	<title>EPOS | Log Pembayaran Piutang</title>
@endsection

@section('style')
	<style type="text/css" media="screen">
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Log Pembayaran Piutang</h2>
				<a href="{{ url('piutang-dagang/'.$bayar_piutang->piutang_dagang_id) }}" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<table class="table">
							<tbody>
								<tr>
									<th style="border-top: none;">Kode Transaksi</th>
									<td style="border-top: none;">{{ $bayar_piutang->kode_transaksi }}</td>
								</tr>
								<tr>
									<th>Jumlah Bayar</th>
									<td>{{ \App\Util::duit($bayar_piutang->jumlah_bayar) }}</td>
								</tr>
								<tr>
									<th>Tanggal Pembayaran</th>
									<td>{{ $bayar_piutang->created_at->format('d-m-Y H:i:s') }}</td>
								</tr>
								@if($bayar_piutang->nominal_tunai > 0)
									<tr>
										<th>Nominal Tunai</th>
										<td>{{ \App\Util::duit($bayar_piutang->nominal_tunai) }}</td>
									</tr>
								@endif
								@if($bayar_piutang->nominal_debit > 0)
									<tr>
										<th>No. Transfer</th>
										<td>
											@if (!empty($bayar_piutang->no_debit))
											{{ $bayar_piutang->no_debit }}
											@else
											-
											@endif
										</td>
									</tr>
									<tr>
										<th>Nominal Transfer</th>
										<td>{{ \App\Util::duit($bayar_piutang->nominal_debit) }}</td>
									</tr>
									<tr>
										<th>Bank Kartu</th>
										<td>{{ $bayar_piutang->bp_transfer->nama_bank }}</td>
									</tr>
								@endif
								@if($bayar_piutang->nominal_kartu > 0)
									<tr>
										<th>No. Transfer Kartu</th>
										<td>
											@if (!empty($bayar_piutang->no_kartu))
											{{ $bayar_piutang->no_kartu }}
											@else
											-
											@endif
										</td>
									</tr>
									<tr>
										<th>Nominal Kartu</th>
										<td>{{ \App\Util::duit($bayar_piutang->nominal_kartu) }}</td>
									</tr>
									<tr>
										<th>Bank Kartu</th>
										<td>{{ $bayar_piutang->bp_kartu->nama_bank }}</td>
									</tr>
									<tr>
										<th>Jenis Kartu</th>
										<td style="text-transform: capitalize;">{{ $bayar_piutang->jenis_kartu }}</td>
									</tr>
								@endif
								@if($bayar_piutang->nominal_cek > 0)
									<tr>
										<th>No. Cek</th>
										<td>
											@if (!empty($bayar_piutang->no_cek))
											{{ $bayar_piutang->no_cek }}
											@else
											-
											@endif
										</td>
									</tr>
									<tr>
										<th>Nominal Cek</th>
										<td>{{ \App\Util::duit($bayar_piutang->nominal_cek) }}</td>
									</tr>
								@endif
								@if($bayar_piutang->nominal_bg > 0)
									<tr>
										<th>No. BG</th>
										<td>
											@if (!empty($bayar_piutang->no_bg))
											{{ $bayar_piutang->no_bg }}
											@else
											-
											@endif
										</td>
									</tr>
									<tr>
										<th>Nominal BG</th>
										<td>{{ \App\Util::duit($bayar_piutang->nominal_bg) }}</td>
									</tr>
								@endif
								<tr>
									<th>Operator</th>
									<td>{{ $bayar_piutang->user->nama }}</td>
								</tr>
							</tbody>
						</table>	
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('piutang-dagang') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
