@extends('layouts.admin')

@section('title')
    <title>EPOS | Piutang Dagang</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnTransfer, #btnTanggal, #btnDetail {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Piutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">    
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" id="tabelPiutangDagang">
                    <thead>
                        <tr>
                            <th width="5%">No.</th>
                            <th>Kode Transaksi</th>
                            {{-- <th>Transaksi Penjualan</th> --}}
                            <th>Pelanggan</th>
                            <th>Nominal Piutang</th>
                            <th>Sisa Piutang</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($piutangs as $i => $piutang)
                        <tr>
                            <td class="tengah-hv">{{ ++$i }}</td>
                            {{-- <td class="tengah-v">{{ $piutang->kode_transaksi }}</td> --}}
                            <td class="tengah-v">{{ $piutang->transaksi_penjualan->kode_transaksi }}</td>
                            <td class="text-left">{{ $piutang->transaksi_penjualan->pelanggan->nama }}</td>
                            <td class="text-right">{{ \App\Util::ewon($piutang->nominal) }}</td>
                            <td class="text-right">{{ \App\Util::ewon($piutang->sisa) }}</td>
                            <td class="tengah-hv">
                                @if ($piutang->status == 'lunas')
                                <a href="{{ url('piutang-dagang/'.$piutang->id) }}" class="btn btn-xs btn-info" id="btnDetail" title="Detail Piutang Dagang">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @else
                                <a href="{{ url('piutang-dagang/'.$piutang->id) }}" class="btn btn-xs btn-primary" id="btnDetail" title="Bayar Piutang Dagang" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-money"></i>
                                </a>
                                {{-- <a href="{{ url('piutang-dagang/tanggal/'.$piutang->id) }}" class="btn btn-xs btn-warning" title="Ubah Tanggal Jatuh Tempo" id="btnTanggal">
                                    <i class="fa fa-calendar"></i>
                                </a> --}}
                                <a href="{{ url('piutang-dagang/transfer/'.$piutang->id) }}" class="btn btn-xs btn-success" title="Transfer ke Piutang tak Tertagih" id="btnTanggal" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-sign-in"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Piutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-hover" id="tabelPiutangDagangLunas">
                    <thead>
                        <tr>
                            <th width="5%">No.</th>
                            <th>Kode Transaksi</th>
                            <th>Penggalan</th>
                            <th>Nominal Piutang</th>
                            {{-- <th>Sisa Piutang</th> --}}
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lunas_piutangs as $i => $piutang)
                        <tr>
                            <td class="tengah-hv">{{ ++$i }}</td>
                            {{-- <td class="tengah-v">{{ $piutang->kode_transaksi }}</td> --}}
                            <td class="tengah-v">{{ $piutang->transaksi_penjualan->kode_transaksi }}</td>
                            <td class="text-left">{{ $piutang->transaksi_penjualan->pelanggan->nama }}</td>
                            <td class="text-right">{{ \App\Util::ewon($piutang->nominal) }}</td>
                            {{-- <td class="text-right">{{ \App\Util::ewon($piutang->sisa) }}</td> --}}
                            <td class="tengah-hv">
                                <a href="{{ url('piutang-dagang/'.$piutang->id) }}" class="btn btn-xs btn-info" title="Riwayat Pembayaran Piutang Dagang" data-toggle="tooltip" data-placement="top">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'transfer')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transfer kerugian Piutang Berhasil!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif
    
    <script type="text/javascript">
        $('#tabelPiutangDagang').DataTable();
        $('#tabelPiutangDagangLunas').DataTable();

        $(document).ready(function() {
            var url = "{{ url('piutang-dagang') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });
    </script>
@endsection
