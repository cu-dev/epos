@extends('layouts.admin')

@section('title')
    <title>EPOS | Tingkatan Pengguna</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	{{-- <div class="col-md-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Ubah Tingkatan Pengguna</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('level') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group">
						<label class="control-label">Kode Tingkatan</label>
						<input class="form-control" type="text" name="kode">
					</div>
					<div class="form-group">
						<label class="control-label">Nama Tingkatan</label>
						<input class="form-control" type="text" name="nama">
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-sm btn-success" id="btnSimpan" type="submit" disabled="">
							<i class="fa fa-save"></i> <span>Ubah</span>
						</button>
						<button class="btn btn-sm btn-default" id="btnReset" type="button">
							<i class="fa fa-refresh"></i> Reset
						</button>
					</div>
				</form>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div> --}}

	<div class="col-md-8 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Tingkatan Pengguna</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableLevel">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Tingkatan</th>
							<th>Nama</th>
							{{-- <th>Aksi</th> --}}
						</tr>
					</thead>
					<tbody>
						@foreach($levels as $num => $level)
						<tr id="{{$level->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $level->kode }}</td>
							<td>{{ $level->nama }}</td>
							{{-- <td>
								<button class="btn btn-xs btn-primary" id="btnUbah">
									<i class="fa fa-edit"></i> Ubah
								</button>
								<button class="btn btn-xs btn-danger" id="btnHapus">
									<i class="fa fa-trash"></i> Hapus
								</button>
							</td> --}}
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection


@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Tingkatan berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Tingkatan gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Tingkatan berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Tingkatan gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Tingkatan berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Tingkatan gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableLevel').DataTable();
		$(document).on('click', '#btnReset', function() {
			$('input[name="kode"]').val('');
			$('input[name="nama"]').val('');

			$('#formSimpanContainer').find('form').attr('action', '{{ url("level") }}');
			$('#formSimpanContainer').find('input[name="_method"]').val('post');
			$('#btnSimpan span').text('Tambah');

			$('#formSimpanTitle').text('Tambah Satuan Item');
		});

		$(document).on('click', '#btnUbah', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var kode = $tr.find('td').first().next().text();
			var nama = $tr.find('td').first().next().next().text();
			
			$('#btnSimpan').prop('disabled', false);
			$('input[name="kode"]').val(kode);
			$('input[name="nama"]').val(nama);


			$('#formSimpanContainer').find('form').attr('action', '{{ url("level") }}' + '/' + id);
			$('#formSimpanContainer').find('input[name="_method"]').val('put');
			$('#btnSimpan span').text('Ubah');

			$('#formSimpanTitle').text('Ubah Level');
		});
		
		$(document).on('click', '#btnHapus', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama = $tr.find('td').first().next().next().text();
			$('input[name="id"]').val(id);

			swal({
				title: 'Hapus?',
				text: '\"' + nama + '\" akan dihapus!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#009688',
				cancelButtonColor: '#ff5252',
				confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
				cancelButtonText: '<i class="fa fa-close"></i> Batal'
			}).then(function(){
				// Confirmed
				$('#formHapusContainer').find('form').attr('action', '{{ url("level") }}' + '/' + id);
				$('#formHapusContainer').find('form').submit();
			}, function(isConfirm) {
				//canceled
			});
		});
	</script>
@endsection


