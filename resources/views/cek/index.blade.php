@extends('layouts.admin')

@section('title')
	<title>EPOS | Cek</title>
@endsection

@section('style')
	<style media="screen">
		#btnSimpan {
			margin-top: 10px;
		}
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		textarea {
			min-width: 100%;
			max-width: 48px;
			min-height: 100%;
			max-height: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Cek</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableCek">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor</th>
							<th>Nominal</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ceks as $i => $cek)
						<tr id="{{ $cek->id }}">
							<td>{{ $i+1 }}</td>
							<td>{{ $cek->nomor }}</td>
							<td class="text-right">{{ \App\Util::duit($cek->nominal) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2_single").select2();
		});
		$('#tableCek').DataTable();
	</script>
@endsection
