@extends('layouts.admin')

@section('title')
    <title>EPOS | Jenis Item</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Jenis Item</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('jenisitem') }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="control-label">Kode Jenis</label>
                        <input class="form-control" type="text" name="kode" id="kode" required="" />
                        <span style="color: red" id="warning"></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nama Jenis</label>
                        <input class="form-control" type="text" name="nama" required="" />
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <button class="btn btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                        <button class="btn btn-default" id="btnReset" type="button">
                            <i class="fa fa-refresh"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Jenis Item</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Jenis</th>
                            <th>Nama Jenis</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jenis_items as $num => $jenis_item)
                        <tr id="{{$jenis_item->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $jenis_item->kode }}</td>
                            <td>{{ $jenis_item->nama }}</td>
                            <td class="text-center">
                                <button class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Jenis Item">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus Jenis Item">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Jenis item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Jenis item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Jenis item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Jenis item gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kode/Nama sudah digunakan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Jenis item berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Jenis item gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();
        $(document).on('click', '#btnReset', function() {
            $('input[name="kode"]').val('');
            $('input[name="nama"]').val('');

            $('#formSimpanContainer').find('form').attr('action', '{{ url("jenisitem") }}');
            $('#formSimpanContainer').find('input[name="_method"]').val('post');
            $('#btnSimpan span').text('Tambah');

            $('#formSimpanTitle').text('Tambah Jenis Item');
        });

        $(document).on('click', '#btnUbah', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var kode = $tr.find('td').first().next().text();
            var nama = $tr.find('td').first().next().next().text();

            $('input[name="kode"]').val(kode);
            $('input[name="nama"]').val(nama);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("jenisitem") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Jenis Item');
        });
        
        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                // Confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("jenisitem") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('keyup', '#kode', function() {
            var strings = $(this).val();
            var grup = $(this).parents('.form-group').first();
            if(strings.length > 4){
                string = $(this).val().slice(0,-1);
                $(this).val(string);	
                $('#warning').text('');
                grup.removeClass('has-error');
            }else if(strings.length < 2){
                $('#warning').text('Minimal karakter 2 huruf');
                grup.addClass('has-error');
            }else{
                $('#warning').text('');
                grup.removeClass('has-error');
            }
        });

    </script>
@endsection
