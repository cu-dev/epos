@extends('layouts.admin')

@section('title')
    <title>EPOS | Input Laba Ditahan</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Input Laba Ditahan</h2>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row">
    				<form method="post" action="{{ url('laba_ditahan') }}" class="form-horizontal">
						<div class="col-md-12 col-xs-12">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
							<div class="form-group">
								<label class="control-label">Kode Transaksi</label>
								<input class="form-control" type="text" name="kode_transaksi" readonly>
							</div>
							<div class="form-group">
								<label class="control-label">Nominal</label>
								<input class="form-control angka" type="text" name="nominal_">
								<input class="form-control" type="hidden" name="nominal">
							</div>
							<div class="form-group">
								<label class="control-label">Keterangan</label>
								<input class="form-control" type="text" name="keterangan">
							</div>
							<div class="form-group">
								<label class="control-label">Sumber Kas</label>
								<div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
									<div class="btn-group" role="group">
										<button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnBank" class="btn btn-default">Bank</button>
									</div>
								</div>
							</div>
							<div class="form-group" id="BankContainer">
								<label class="control-label">Pilih Rekening Bank</label>
								<select name="bank" class="form-control">
									<option id="default">Pilih Bank</option>
									@foreach($banks as $bank)
									<option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
									@endforeach
								</select>
							</div>
								<input type="hidden" name="kas" />
							<div class="form-group" style="margin-bottom: 0;">
								<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> <span>Simpan</span>
								</button>
							</div>
						</div>
					</form>
    			</div>
      		</div>
    	</div>
  	</div>

  	<div class="col-md-6 col-xs-6" id="formSimpanContainer">
		<div class="row">
			<div class="x_panel">
				<div class="x_title">
					<h2 id="formSimpanTitle">Data Laba Ditahan</h2>
	                <ul class="nav navbar-right panel_toolbox">
	                	<div class="pull-right">
	                		<li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
	                	</div>
	                </ul>
	                <div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	                <div class="row">
						<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
						<thead>
							<tr>
								<th>Kode Akun</th>
								<th>Nama Akun</th>
								<th>Nominal</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					</div>
				</div>
			</div> 	
		</div>
	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$(".select2_single").select2({
				allowClear: true
			});
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
		});

		$(window).on('load', function(event) {

			var url = "{{ url('beban/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			

			$.get(url, function(data) {
				if (data.beban === null) {
					var kode = int4digit(1);
					var referensi = kode + '/LBD/' + tanggal;
				} else {
					var referensi = data.beban.referensi;
					var mm_transaksi = referensi.split('/')[2];
					var yyyy_transaksi = referensi.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						referensi = kode + '/LBD/' + tanggal;
					} else {
						var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
						referensi = kode + '/LBD/' + tanggal_transaksi;				}
				}
				$('input[name="kode_transaksi"]').val(referensi);
			});
		});
	</script>
@endsection
