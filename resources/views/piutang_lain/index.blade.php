@extends('layouts.admin')

@section('title')
    <title>EPOS | Piutang Lain-Lain</title>
@endsection

@section('style')
    <style media="screen">
        #btnBayar, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnLogPembayaran {
            margin: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Piutang Lain-lain</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right">	
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('piutang_lain') }}" class="form-horizontal">
                        <div class="col-md-6 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pilih Status Karyawan</label>
                                <select name="karyawan" class="select2_single form-control">
                                    <option id="default">Pilih Status</option>
                                    <option value="1">Karyawan</option>
                                    <option value="0">Bukan Karyawan</option>
                                </select>
                            </div>
                            <div class="form-group sembunyi" id="DataKaryawan">
                                <label class="control-label">Pilih Karyawan</label>
                                <select name="user" class="select2_single form-control">
                                    <option value="" id="default">Pilih Karyawan</option>
                                    @foreach($karyawans as $karyawan)
                                        <option value="{{$karyawan->id}}">{{$karyawan->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama</label>
                                <input id="inputNama" class="form-control" type="text" name="nama_input">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control" type="text" name="keterangan">
                            </div>
                            {{-- <div class="form-group">
                                <label class="control-label">Status Karyawan</label>
                                <select name="karyawan" class="select2_single form-control">
                                    <option id="default">Pilih Status</option>
                                    <option value="1">Karyawan</option>
                                    <option value="0">Bukan Karyawan</option>
                                </select>
                            </div> --}}
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Nominal Piutang</label>
                                <input class="form-control angka" type="text" name="nominal_">
                                <input class="form-control" type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Dari Kas</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank" class="select2_single form-control">
                                    <option id="default">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                    @endforeach
                                </select>
                            </div>
                                <input type="hidden" name="kas" />
                                <input class="form-control" type="hidden" name="nama">
                            <div class="form-group" style="margin-bottom: 0; margin-top: 20px;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- kolom bawah -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Piutang Lain-Lain</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('piutang_lain/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Nama</th>
                                <th>Status Karyawan</th>
                                <th>Nominal Piutang</th>
                                <th>Sisa Piutang</th>
                                <th>Keterangan</th>
                                <th style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Piutang Lain-Lain</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('piutang_lain/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Nama</th>
                                <th>Status Karyawan</th>
                                <th>Nominal Piutang</th>
                                <th>Keterangan</th>
                                <th style="width: 40px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('piutang_lain/mdt1') }}";
        var mdt2 = "{{ url('piutang_lain/mdt2') }}";

        function refreshMDTData(data, base_url) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="8" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var piutang = data[i];
                        var buttons = piutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.bayar != null) {
                            td_buttons += `
                                <a href="${buttons.bayar.url}" class="btn btn-xs btn-primary" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Bayar">
                                    <i class="fa fa-money"></i>
                                </a>
                            `;
                        }
                        if (buttons.transfer != null) {
                            td_buttons += `
                                <a href="${buttons.transfer.url}" class="btn btn-xs btn-success" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Transfer Piutang">
                                    <i class="fa fa-share-square-o"></i>
                                </a>
                            `;
                        }

                        var tr = `<tr id="${piutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${piutang.kode_transaksi}</td>
                                    <td>${piutang.nama}</td>
                                    <td>${piutang.mdt_karyawan}</td>
                                    <td class="text-right">${piutang.nominal}</td>
                                    <td class="text-right">${piutang.sisa}</td>
                                    <td>${piutang.keterangan}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var piutang = data[i];
                        var buttons = piutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.riwayat != null) {
                            td_buttons += `
                                <a href="${buttons.riwayat.url}" class="btn btn-xs btn-info" id="btnLogPembayaran" data-toggle="tooltip" data-placement="top" title="Riwayat Pembayaran Piutang">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }

                        var tr = `<tr id="${piutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${piutang.kode_transaksi}</td>
                                    <td>${piutang.nama}</td>
                                    <td>${piutang.mdt_karyawan}</td>
                                    <td class="text-right">${piutang.nominal}</td>
                                    <td>${piutang.keterangan}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
        });

        $(window).on('load', function(event) {

            var url = "{{ url('piutang_lain/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			

            $.get(url, function(data) {
                if (data.piutang_lain === null) {
                    var kode = int4digit(1);
                    var kode_transaksi = kode + '/PLL/' + tanggal;
                    console.log('kode_transaksi');
                } else {
                    var kode_transaksi = data.piutang_lain.kode_transaksi;
                    var mm_transaksi = kode_transaksi.split('/')[2];
                    var yyyy_transaksi = kode_transaksi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode_transaksi = kode + '/PLL/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/PLL/' + tanggal_transaksi;				}
                }
                // console.log(kode_transaksi);
                $('input[name="kode_transaksi"]').val(kode_transaksi);
            });
        });

        $(document).on('change', 'select[name="karyawan"]', function(event) {
            event.preventDefault();
            var id = $(this).val();
            console.log(id);
            if(id==1){
                $('#DataKaryawan').removeClass('sembunyi');
                $('#inputNama').prop('disabled', true);
                // $('#inputNama').val('');
            }else{
                $('#DataKaryawan').addClass('sembunyi');
                $('select[name="user"]').val('');
                $('#inputNama').prop('disabled', false);
                $('#inputNama').val('');
            }
        });

        $(document).on('change', 'select[name="user"]', function(event) {
            event.preventDefault();
            var id = $(this).val();
            var nama = $('select[name="user"] option:selected').text();
            // $(this).text();
            console.log(nama);
            $('#inputNama').val(nama);
            $('input[name="nama"]').val(nama);
        });

        $(document).on('keyup', '#inputNama', function(event) {
            event.preventDefault();
            var nama = $(this).val();
            $('input[name="nama"]').val(nama);
        });

    </script>
@endsection
