@extends('layouts.admin')

@section('title')
    <title>EPOS | Transfer Piutang Lain-Lain</title>
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="col-md-8">
			<div class="x_panel">
				<div class="x_title">
					<h2 id="formSimpanTitle">Transfer Piutang ke Beban a/n {{ $piutang_lain->nama }}</h2>
					<a href="{{url('piutang_lain')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content" id="formSimpanContainer">
			        <form method="post" action="{{ url('transfer_piutang_lain') }}" class="form-horizontal">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label"><b>Sisa Piutang </b>: {{ \App\Util::duit0($piutang_lain->sisa) }}</label>
						</div>
						<div class="form-group">
							<label class="control-label">Jumlah Transfer</label>
							<input class="form-control angka" type="text" name="nominal_">
							<input class="form-control" type="hidden" name="nominal">
						</div>
						<div class="form-group">
							<label class="control-label">Dari Kas</label>
							@if($piutang_lain->karyawan==0)
							<input class="form-control angka" type="text" name="asal_" value="Piutang Karyawan" readonly>
							<input class="form-control angka" type="hidden" name="asal" value="1" readonly>
							@else
							<input class="form-control angka" type="text" name="asal_" value="Piutang Lain-lain" readonly>
							<input class="form-control angka" type="hidden" name="asal" value="0" readonly>
							@endif
						</div>
						<div class="form-group">
							<label class="control-label">Ke Beban</label>
							<input class="form-control angka" type="text" name="tujuan_" value="Kerugian Piutang" readonly>
							<input class="form-control angka" type="hidden" name="tujuan" value="{{ \App\Akun::PiutangTakTertagih }}">
						</div>
							<input type="hidden" name="piutang_lain_id" value="{{ $piutang_lain->id }}" />
							<input type="hidden" name="nama" value="{{ $piutang_lain->nama }}" />
							<input type="hidden" name="keterangan" value="{{ $piutang_lain->keterangan }}" />
						<div class="form-group" style="margin-bottom: 0;">
							<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Transfer</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil dibayar!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Hutang gagal dibayar!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();

	</script>
@endsection