@extends('layouts.admin')

@section('title')
    <title>EPOS | Kas</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #BankContainer,
        #CekContainer,
        #BGContainer,
        #BankContainer1,
        #CekContainer1,
        #BGContainer1 {
            padding-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .collapse-link {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')

<!-- <div class="row"> -->
    <div class="col-md-4 col-xs-12" id="">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="">Transfer Kas</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('transfer_kas') }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="control-label">Kode Transaksi</label>
                        <input class="form-control" type="text" name="kode_transaksi" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Sumber Kas</label>
                        <select class="select2_single form-control" name="sumber_kas" required="Tidak Boleh Kosong!!">
                            <option value="">Pilih Kas</option>
                            @foreach($kases as $kas)
                                <option value="{{ $kas->kode }}">{{ $kas->kode }} - {{ $kas->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-11 pull-right" id="BankContainer">
                        <label class="control-label">Pilih Rekening Bank</label>
                        <select name="bank_asal" class="select2_single form-control">
                            <option value="">Pilih Bank</option>
                            @foreach($banks as $bank)
                            <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-11 pull-right" id="CekContainer">
                        <label class="control-label">Pilih Cek</label>
                        <select name="cek_asal" class="select2_single form-control">
                            <option value="">Pilih Cek</option>
                            @foreach($ceks as $cek)
                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [ Rp{{ \App\Util::duit($cek->nominal) }} ]</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-11 pull-right" id="BGContainer">
                        <label class="control-label">Pilih BG</label>
                        <select name="bg_asal" class="select2_single form-control">
                            <option value="">Pilih BG</option>
                            @foreach($BGs as $BG)
                            <option value="{{ $BG->id }}">{{$BG->nomor}} [ Rp{{ App\Util::duit($cek->nominal) }} ]</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row"></div>
                    <div class="form-group">
                        <label class="control-label">Kas Tujuan</label>
                        <select class="select2_single form-control" name="kas_tujuan" required="Tidak Boleh Kosong!!">
                            <option value="">Pilih Kas</option>
                            @foreach ($kases as $kas)
                                <option value="{{ $kas->kode }}">{{ $kas->kode }} - {{ $kas->nama }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group  col-md-11 pull-right" id="BankContainer1">
                        <label class="control-label">Pilih Rekening Bank</label>
                        <select name="bank_tujuan" class="select2_single form-control">
                            <option value="">Pilih Bank</option>
                            @foreach($banks as $bank)
                            <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-11 pull-right" id="CekContainer1">
                        <label class="control-label">Masukkan Nomor Cek</label>
                        <input class="form-control" type="text" name="no_cek">
                    </div>

                    <div class="form-group col-md-11 pull-right" id="BGContainer1">
                        <label class="control-label">Masukkan Nomor BG</label>
                        <input class="form-control" type="text" name="no_bg">
                    </div>

                    <div class="form-group" id=nominal>
                        <label class="control-label">Nominal</label>
                        <input class="form-control text-right" type="text" name="nominal_i" value="0,00">
                        <input type="hidden" name="nominal">
                        <label class="control-label pull-right" id="nominal_v">0,00</label>         
                    </div>
                    <div class="form-group">
                        <label class="control-label">Keterangan</label>
                        <input class="form-control" type="text" name="keterangan" required="">
                    </div>
                    <div class="form-group" style="margin-bottom: 0; margin-top: 20px;">
                        <button class="btn btn-success" id="btnSimpan" type="submit" disabled="">
                            <i class="fa fa-save"></i> <span>Transfer</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-xs-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Data Kas</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Akun</th>
                            <th>Jenis Kas</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($kases as $nums => $kas)                       
                            <tr>
                                <td>{{ $nums+1 }}</td>
                                <td>{{ $kas->kode }}</td>
                                <td>{{ $kas->nama }}</td>
                                <td class="text-right">{{ \App\Util::duit($kas->debet) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-6 col-xs-6" id="">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="">Daftar Cek Aktif</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableCEK">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor CEK</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ceks as $nums => $cek)
                            <tr id="{{ $cek->id }}">
                                <td>{{ $nums+1 }}</td>
                                <td>{{ $cek->nomor }}</td>
                                <td class="text-right">{{ \App\Util::duit($cek->nominal) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Daftar BG Aktif</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBG">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor BG</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($BGs as $nums => $BG)
                            <tr id="{{ $BG->id }}">
                                <td>{{ $nums+1 }}</td>
                                <td>{{ $BG->nomor }}</td>
                                <td class="text-right">{{ \App\Util::duit($BG->nominal) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-6 col-xs-6" id="">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="">Daftar Riwayat Cek</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableCEK_">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor CEK</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($non_ceks as $nums => $non_cek)
                            <tr id="{{ $non_cek->id }}">
                                <td>{{ $nums+1 }}</td>
                                <td>{{ $non_cek->nomor }}</td>
                                <td class="text-right">{{ \App\Util::duit($non_cek->nominal) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Daftar Riwayat BG</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBG_">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor BG</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($non_BGs as $nums => $non_BG)
                            <tr id="{{ $non_BG->id }}">
                                <td>{{ $nums+1 }}</td>
                                <td>{{ $non_BG->nomor }}</td>
                                <td class="text-right">{{ \App\Util::duit($non_BG->nominal) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transfer Kas berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transfer Kas gagal didlakukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();
        $('#tableCEK').DataTable();
        $('#tableBG').DataTable();
        $('#tableCEK_').DataTable();
        $('#tableBG_').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2();

            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#BankContainer1').hide();
            $('#BankContainer1').find('input').val('');
            $('#CekContainer').hide();
            $('#CekContainer').find('input').val('');
            $('#CekContainer1').hide();
            $('#CekContainer1').find('input').val('');
            $('#BGContainer').hide();
            $('#BGContainer').find('input').val('');
            $('#BGContainer1').hide();
            $('#BGContainer1').find('input').val('');
        });

        $(document).on('change', 'select[name="sumber_kas"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="nominal_i').prop('disabled', false); 
                $('input[name="nominal_i').val('0,00'); 
                $('input[name="nominal').val(0);
                $('#nominal_v').text('Rp0,00');
            if (id == '111310') {
                $('#BankContainer').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#CekContainer').hide();
                $('#BGContainer').hide();
            } else if (id == '111320') {
                $('#CekContainer').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer').hide();
                $('#BGContainer').hide();
            } else if (id == '111330') {
                $('#BGContainer').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer').hide();
                $('#CekContainer').hide();
            } else {
                $('#BankContainer').hide();
                $('#CekContainer').hide();
                $('#BGContainer').hide();
            }
        });

        $(document).on('change', 'select[name="cek_asal"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('kas/cek/') }}" + "/" + id;

            $.get(url, function(data) {
                var duit = data.cek.nominal;
                duit = parseFloat(duit);
                var duit_v = duit.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
                var duit_i = duit.toFixed(2).replace('.', ',');

                $('input[name="nominal_i').val(duit_i); 
                $('input[name="nominal_i').prop('disabled', true);
                $('input[name="nominal').val(duit); 
                $('#nominal_v').text('Rp'+duit_v);
            });
        });

        $(document).on('change', 'select[name="bg_asal"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('kas/bg/') }}" + "/" + id;

            $.get(url, function(data) {
                var duit = data.cek.nominal;
                duit = parseFloat(duit);
                var duit_v = duit.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
                var duit_i = duit.toFixed(2).replace('.', ',');

                $('input[name="nominal_i').val(duit_i); 
                $('input[name="nominal_i').prop('disabled', true);
                $('input[name="nominal').val(duit); 
                $('#nominal_v').text('Rp'+duit_v);
            });
        });

        $(document).on('change', 'select[name="kas_tujuan"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if (id == '111310') {
                $('#BankContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#CekContainer1').hide();
                $('#BGContainer1').hide();
            } else if (id == '111320') {
                $('#CekContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer1').hide();
                $('#BGContainer1').hide();
            } else if (id == '111330') {
                $('#BGContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer1').hide();
                $('#CekContainer1').hide();
            } else {
                $('#BankContainer1').hide();
                $('#CekContainer1').hide();
                $('#BGContainer1').hide();
            }
        });

        $(document).on('keyup', 'input[name="nominal_i"]', function(event) {
            event.preventDefault();

            var nominals = $(this).val().split(',');
            var nominal = 0;
            if (nominals.length > 1) {
                nominal = $(this).val();
                if (nominals[0].length < 1) {
                    nominals[0] = 0;
                    $(this).val(nominals[0] + ',' + nominals[1]);
                } else if (nominals[1].length > 2) {
                    nominal = $(this).val().slice(0,-1);
                    $(this).val(nominal);
                    // console.log(nominal.toLocaleString(['ban', 'id']));
                } else if (nominals[1].length < 2) {
                    nominal = parseFloat(nominal.replace(',', '.'));
                    nominal = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
                    nominal = nominal.replace('.', '');
                    $(this).val(nominal);
                }
            } else {
                nominal = $(this).val();
                var bel_koma = nominal.substr(nominal.length -2);
                var depan_koma = nominal.substr(0, nominal.length -2);

                $(this).val(depan_koma + ',' + bel_koma);
            }

            var nominal_h = parseFloat(nominal.replace(',', '.'));
            var nominal_v = nominal_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

            $('#nominal_v').text('Rp'+nominal_v);
            $('input[name="nominal"]').val(nominal_h);

            if(nominal_h > 0){
                $('#btnSimpan').prop('disabled', false);
            }else{
                $('#btnSimpan').prop('disabled', true);
            }
        });

        $(window).on('load', function(event) {
            var url = "{{ url('transfer_kas/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');

            $.get(url, function(data) {
                if (data.kas === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/TFK/' + tanggal;
                } else {
                    var referensi = data.kas.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/TFK/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/TFK/' + tanggal_transaksi;             }
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

    </script>
@endsection
