@extends('layouts.admin')

@section('title')
    <title>EPOS | Neraca</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Neraca</h2>
				<form method="post" action="{{ url('neraca/cetak') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<input type="hidden" name="date">

					<ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
			        <button class="btn btn-sm btn-cetak pull-right" type="submit" data-toggle="tooltip" data-placement="top" title="Cetak Neraca">
						<i class="fa fa-print"></i>
					</button>
					<a href="{{ url('neraca') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
				</form>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('neraca/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-2"><h4><strong>Pilih Data Saldo</strong></h4></div>
	    						<div class="col-md-3">
	    							<select name="data[]" multiple id="data" class="form-control" required="">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-success" id="btnUbah" type="submit" style="margin-top: 2px">
										<span style="color:white">Tampil</span>
									</button>
	    						</div>
	    					</div>
						</form>
    				</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>AKTIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							@foreach($tanggal as  $i => $x)
									<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px"><strong>{{ $x }}</strong></div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6"><strong>ASET LANCAR</strong></div>
						</div>

						<div class="row">
							<div class="col-md-6">Kas Tunai</div>
							@foreach($akuns['KasTunai'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Kecil</div>
							@foreach($akuns['KasKecil'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Bank</div>
							@foreach($akuns['KasBank'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Cek</div>
							@foreach($akuns['KasCek'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas BG</div>
							@foreach($akuns['KasBG'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Persediaan</div>
							@foreach($akuns['Persediaan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Perlengkapan</div>
							@foreach($akuns['Perlengkapan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Dagang</div>
							@foreach($akuns['PiutangDagang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Karyawan</div>
							@foreach($akuns['PiutangKaryawan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Lain</div>
							@foreach($akuns['PiutangLain'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Tak Tertagih</div>
							@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Asuransi Bayar Dimuka</div>
							@foreach($akuns['AsuransiDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Sewa Bayar Dimuka</div>
							@foreach($akuns['SewaDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">PPN Masukan</div>
							@foreach($akuns['PPNMasukan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Total Aset Lancar</strong></div>
							@foreach($akuns['AsetLancar'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
						
						<div class="row">
							<div class="col-md-6" style="padding-top: 20px"><strong>ASET TETAP</strong></div>
						</div>
						<div class="row" ">
							<div class="col-md-6">Tanah</div>
							@foreach($akuns['Tanah'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Bangunan</div>
							@foreach($akuns['Bangunan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Peralatan</div>
							@foreach($akuns['Peralatan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kendaraan</div>
							@foreach($akuns['Kendaraan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Bangunan</div>
							@foreach($akuns['ADBangunan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Peralatan</div>
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Kendaraan</div>
							@foreach($akuns['ADKendaraan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Total Aset Tetap</strong></div>
							@foreach($akuns['AsetTetap'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
					</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>PASIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							@foreach($tanggal as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px;"><strong>{{ $x }}</span></strong></div>
							@endforeach
							{{-- @foreach($tanggal as  $i => $x)
									<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px"><strong>{{ $x }}</strong></div>
							@endforeach --}}
						</div>
						<div class="row">
							<div class="col-md-6"><strong>KEWAJIBAN</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6">Kartu Kredit</div>
							@foreach($akuns['KartuKredit'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang Dagang</div>
							@foreach($akuns['HutangDagang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang Pajak</div>
							@foreach($akuns['HutangPajak'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						{{-- <div class="row">
							<div class="col-md-6">Taksiran Hutang Garansi</div>
							@foreach($akuns['TaksiranUtangGaransi'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div> --}}
						<div class="row">
							<div class="col-md-6">Hutang Konsinyasi</div>
							@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang PPN</div>
							@foreach($akuns['HutangPPN'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div><div class="row">
							<div class="col-md-6">PPN Keluaran</div>
							@foreach($akuns['PPNKeluaran'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Deposito Pelanggan</div>
							@foreach($akuns['PendapatanDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6 menjorok"><strong>Hutang Jangka Pendek</strong></div>
							@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6">Hutang Bank</div>
							@foreach($akuns['HutangBank'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Hutang Jangka Panjang</strong></div>
							@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6"><strong>Total Kewajiban</strong></div>
							@foreach($akuns['Kewajiban'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>

						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-6"><strong>MODAL</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6">Modal</strong></div>
							@foreach($akuns['Modal'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Laba di Tahan</div>
							@foreach($akuns['LabaTahan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data"> {{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Prive</div>
							@foreach($akuns['Prive'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">BagiHasil</div>
							@foreach($akuns['BagiHasil'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6"><strong>Total Modal</strong></div>
							@foreach($akuns['Equitas'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
					</div>
    			</div>

    			<div class="row">
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 10px; padding-bottom: 10px">
							<div class="col-md-6"><strong>Total Aset</strong></div>
							@foreach($akuns['Aset'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
    				</div>
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 10px; padding-bottom: 10px">
							<div class="col-md-6"><strong>Total Kewajiban + Modal</strong></div>
							@foreach($akuns['TotalKE'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
    				</div>
    			</div>

      		</div>
    	</div>
  	</div>
</div>

@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			var url = "{{ url('neraca') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

            $('.right_col').css('min-height', $('.left_col').css('height'));

			$("#data").select2({
				maximumSelectionLength: 2
			});

			// var date = $(".tanggal").text();
			// var date = $(".tanggal").text().match(/.{1,10}/g);
			// $('input[name="date"]').val(date);
            // console.log(tanggal);
            var date = '{{ $tanggal_data }}';
			$('input[name="date"]').val(date);

			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                	$(el).text(duit);
                }else{
                	var a = '<span class="invisible">)</span>';
                	$(el).text(duit);
                	$(el).append(a);
                }
            });
		});
	</script>
@endsection
