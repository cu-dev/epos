<!DOCTYPE html>
<html>
<head>
	<title></title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

	<style media="print">
        .kanan{
        	text-align: right;
        }

        .gemuk {
        	font-weight: bold;
        }

        @page {
        size: 330mm 210mm;
        margin: 10mm 0mm 0mm 0mm;
	    }

	    table td {font-size: 12px;}
	    /*@media print {
	    		table td {font-size: 5px;}
	    }*/
    </style>
</head>
<body>
<div class="row" >
	<div class="col-md-12">
		<div class="title-hide">
			<center>
				<STRONG>KENCANA MULYA </STRONG><br/>
				<STRONG>LAPORAN POSISI KEUANGAN </STRONG><br/>
				
			</center>
			<br>
			<br>
		</div>
		<!-- content -->
		<div class="x_content">
			<div class="row" style="overflow: hidden;">
				<table width="90%" cellspacing="0" style="margin-left: 5%; border-spacing: 2px; border-collapse: separate; border: 1px solid black">
					<tbody>
						<tr>
							<td class="text-center" 
							@if(sizeof($tanggal)==2)
								colspan="3">AKTIVA</td>
							@else
								colspan="2">AKTIVA</td>
							@endif
							<td class="text-center"
							@if(sizeof($tanggal)==2)
								colspan="3">PASIVA</td>
							@else
								colspan="2">PASIVA</td>
							@endif
						</tr>
						<tr>
							@if(sizeof($tanggal)==2 )
								@foreach($tanggal as  $i => $x)
									@if($i == 0)
										<td class="text-right" colspan="2">{{ $x }}</td>
									@else
										<td width="12%" class="text-right">{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($tanggal as  $i => $x)
									<td class="text-right" colspan="2">{{ $x }}</td>
								@endforeach
							@endif

							@if(sizeof($tanggal)==2 )
								@foreach($tanggal as  $i => $x)
									@if($i == 0)
										<td class="text-right" colspan="2">{{ $x }}</td>
									@else
										<td width="12%" class="text-right">{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($tanggal as  $i => $x)
									<td class="text-right" colspan="2">{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Kas Tunai</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KasTunai'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KasTunai'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td colspan="3" style="padding-left: 15px">KEWAJIBAN</td>
						</tr>
						<tr>
							<td width="26%">Kas Kecil</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KasKecil'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KasKecil'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Kartu Kredit</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KartuKredit'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KartuKredit'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Kas Bank</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KasBank'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KasBank'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Dagang</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangDagang'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangDagang'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Kas Cek</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KasCek'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KasCek'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Pajak</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangPajak'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangPajak'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Kas BG</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['KasBG'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['KasBG'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Taksiran Hutang Garansi</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['TaksiranUtangGaransi'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['TaksiranUtangGaransi'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Persediaan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Persediaan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Persediaan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Konsinyasi</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Perlengkapan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Perlengkapan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Perlengkapan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang PPN</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangPPN'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangPPN'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Piutang Dagang</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PiutangDagang'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PiutangDagang'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">PPN Keluaran</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PPNKeluaran'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PPNKeluaran'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Piutang Karyawan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PiutangKaryawan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PiutangKaryawan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Pendapatan Dibayar Dimuka</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PendapatanDimuka'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PendapatanDimuka'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Piutang Lain-Lain</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PiutangLain'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PiutangLain'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Jangka Pendek</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Piutang Tak Tertagih</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Bank</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangBank'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangBank'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Asuransi Bayar Dimuka</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['AsuransiDimuka'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['AsuransiDimuka'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Hutang Jangka Panjang</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Sewa Bayar Dimuka</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['SewaDimuka'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['SewaDimuka'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Total Kewajiban</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Kewajiban'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Kewajiban'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">PPN Masukan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['PPNMasukan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['PPNMasukan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
						</tr>
						<tr>
							<td width="26%">Aset Lancar</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['AsetLancar'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['AsetLancar'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td colspan="3" style="padding-left: 15px">EQUITAS</td>
						</tr>
						<tr>
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
							<td width="26%" style="padding-left: 15px">Modal</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Modal'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Modal'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Tanah</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Tanah'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Tanah'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Laba Ditahan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['LabaTahan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['LabaTahan'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Bangunan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Bangunan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Bangunan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Prive</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Prive'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Prive'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Peralatan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Peralatan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Peralatan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Bagi Hasil</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['BagiHasil'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['BagiHasil'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Kendaraan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Kendaraan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Kendaraan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-left: 15px">Total Equitas</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Equitas'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Equitas'] as  $i => $x)
									<td class="text-right" width="48%">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
						<tr>
							<td width="26%">Akumulasi Depresiasi Bangunan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['ADBangunan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['ADBangunan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
						</tr>
						<tr>
							<td width="26%">Akumulasi Depresiasi Peralatan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['ADPeralatan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['ADPeralatan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
						</tr>
						<tr>
							<td width="26%">Akumulasi Depresiasi Peralatan</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['ADPeralatan'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['ADPeralatan'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
						</tr>
						<tr>
							<td width="26%">Aset Tetap</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['AsetTetap'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['AsetTetap'] as  $i => $x)
									<td class="text-right" width="24%">Rp{{ $x }}</td>
								@endforeach
							@endif
							@if(sizeof($tanggal) == 2 )
								<td colspan="3" class="text-right" width="12%"></td>
							@else
								<td colspan="2" class="text-right" width="12%"></td>
							@endif
						</tr>
						<tr>
							<td width="26%" style="padding-top: 15px">Total Aset</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['Aset'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%" style="padding-top: 15px">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%" style="padding-top: 15px">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['Aset'] as  $i => $x)
									<td class="text-right" width="24%" style="padding-top: 15px">Rp{{ $x }}</td>
								@endforeach
							@endif
							<td width="26%" style="padding-top: 15px; padding-left: 15px">Total Kewajiban + Equitas</td>
							@if(sizeof($tanggal) == 2 )
								@foreach($akuns['TotalKE'] as  $i => $x)
									@if($i == 0)
										<td class="text-right" width="12%" style="padding-top: 15px">Rp{{ $x }}</td>
									@else
										<td class="text-right" width="12%" style="padding-top: 15px">Rp{{ $x }}</td>
									@endif
								@endforeach
							@else
								@foreach($akuns['TotalKE'] as  $i => $x)
									<td class="text-right" width="48%" style="padding-top: 15px">Rp{{ $x }}</td>
								@endforeach
							@endif
						</tr>
					</tbody>
				</table>
			</div>

  		</div>

	</div>
</div>
</body>
</html>


<script type="text/javascript" charset="utf-8" async defer>
		window.print();
</script>