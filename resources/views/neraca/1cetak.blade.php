<html>
<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=Generator content="Microsoft Word 15 (filtered)">
	<style>
		.kanan{
        	text-align: right;
        }

        .x_content table tr td{
        	font-size: 12px
        }

        .text-center{
        	text-align: center
        }

        .text-right{
        	text-align: right
        }

        .gemuk {
        	font-weight: bold;
        }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:841.9pt 595.3pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

	</style>
</head>

<body lang=IN link="#0563C1" vlink="#954F72">
<div class=WordSection1>
	<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><img width=185 height=100 src="{{ URL::to('images/image001.png') }}" align=left hspace=12><b><span style='font-size:14.0pt;line-height:107%; font-family:"Clarendon Blk BT","serif"'>KENCANA MULYA</span></b></p>
	<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='font-size:14.0pt;line-height:107%;font-family: "Clarendon Blk BT","serif"'>KARANGDUWUR-PETANAHAN</span></p>
	<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Jl. Puring-Petanahan No. 3 Karangduwur, Kec. Petanahan, Kab. Kebumen</span></p>
	<p class=MsoNormal align=center style='margin-bottom:3px; text-align:center'><a href="mailto:kencanamulyakarangduwur@gmail.com"><span style='line-height:107%;font-family:"Arial","sans-serif"'>kencanamulyakarangduwur@gmail.com</span></a></p>
	<p class=MsoNormal align=center style='margin-bottom:0cm; margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Telp./Fax (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</span></p>
	
	<p class=MsoFooter>

	<table cellpadding=0 cellspacing=0 align=left>
		 <tr>
			  <td width=0 height=5></td>
		 </tr>
		 <tr>
			  <td></td>
			  <td><img width=1049 height=5 src="{{ URL::to('images/image0021.png') }}"></td>
		 </tr>
	</table>

	<center style="padding-top: 40px; padding-bottom: 20px; font-size: 12px">
		<STRONG>KENCANA MULYA </STRONG><br/>
		<STRONG>LAPORAN POSISI KEUANGAN </STRONG><br/>
	</center>

	<div class="x_content">
		<div class="row" style="overflow: hidden;">
			<table width="100%" cellspacing="0" style="border-spacing: 2px; border-collapse: separate; border: 1px solid black">
				<tbody>
					<tr>
						<td class="text-center" 
						@if(sizeof($tanggal)==2)
							colspan="3"><strong>AKTIVA</strong></td>
						@else
							colspan="2"><strong>AKTIVA</strong></td>
						@endif
						<td class="text-center"
						@if(sizeof($tanggal)==2)
							colspan="3"><strong>PASIVA</strong></td>
						@else
							colspan="2"><strong>PASIVA</strong></td>
						@endif
					</tr>

					<tr>
						@if(sizeof($tanggal)==2 )
							@foreach($tanggal as  $i => $x)
								@if($i == 0)
									<td class="text-right" colspan="2">{{ $x }}</td>
								@else
									<td width="12%" class="text-right">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($tanggal as  $i => $x)
								<td class="text-right" colspan="2">{{ $x }}</td>
							@endforeach
						@endif

						@if(sizeof($tanggal)==2 )
							@foreach($tanggal as  $i => $x)
								@if($i == 0)
									<td class="text-right" colspan="2">{{ $x }}</td>
								@else
									<td width="12%" class="text-right">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($tanggal as  $i => $x)
								<td class="text-right" colspan="2">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						@if(sizeof($tanggal)==2 )
							<td colspan="3" style=""><strong>ASET LACAR</strong></td>
							<td colspan="3" style="padding-left: 15px; "><strong>KEWAJIBAN</strong></td>
						@else
							<td colspan="2" style=""><strong>ASET LACAR</strong></td>
							<td colspan="2" style="padding-left: 15px; "><strong>KEWAJIBAN</strong></td>
						@endif
					</tr>

					<tr>
						<td width="26%">Kas Tunai</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KasTunai'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KasTunai'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Kartu Kredit</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KartuKredit'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KartuKredit'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Kas Kecil</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KasKecil'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KasKecil'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Hutang Dagang</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangDagang'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangDagang'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Kas Bank</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KasBank'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KasBank'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Hutang Pajak</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangPajak'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangPajak'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>
					</tr>

					<tr>
						<td width="26%">Kas Cek</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KasCek'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KasCek'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Hutang Konsinyasi</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Kas BG</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['KasBG'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['KasBG'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Hutang PPN</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangPPN'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangPPN'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Persediaan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Persediaan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Persediaan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">PPN Keluaran</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PPNKeluaran'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PPNKeluaran'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Perlengkapan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Perlengkapan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Perlengkapan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Deposito Pelanggan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PendapatanDimuka'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PendapatanDimuka'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Piutang Dagang</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PiutangDagang'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PiutangDagang'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 35px"><strong>Total Hutang Jangka Pendek</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Piutang Karyawan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PiutangKaryawan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PiutangKaryawan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Hutang Bank</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangBank'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangBank'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Piutang Lain-Lain</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PiutangLain'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PiutangLain'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 35px"><strong>Total Hutang Jangka Panjang</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Piutang Tak Tertagih</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px"><strong>Total Kewajiban</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Kewajiban'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Kewajiban'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Asuransi Bayar Dimuka</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['AsuransiDimuka'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['AsuransiDimuka'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						
					</tr>

					<tr>
						<td width="26%">Sewa Bayar Dimuka</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['SewaDimuka'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['SewaDimuka'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td colspan="3" style="padding-left: 15px"><strong>MODAL</strong></td>
					</tr>

					<tr>
						<td width="26%">PPN Masukan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['PPNMasukan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['PPNMasukan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Modal</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Modal'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Modal'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%"><strong>Total Aset Lancar</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['AsetLancar'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['AsetLancar'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px">Laba Ditahan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['LabaTahan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">
										@if($x >= 0)
											{{ $x }}
										@else
											({{ $x }})
										@endif
									</td>
								@else
									<td class="text-right" width="12%">
										@if($x >= 0)
											{{ $x }}
										@else
											({{ $x }})
										@endif
									</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['LabaTahan'] as  $i => $x)
								<td class="text-right" width="48%">
									@if($x >= 0)
										{{ $x }}
									@else
										({{ $x }})
									@endif
								</td>
							@endforeach
						@endif
					</tr>

					<tr>
						@if(sizeof($tanggal)==2 )
							<td colspan="3" style=""><strong></strong></td>
						@else
							<td colspan="2" style=""><strong></strong></td>
						@endif
						<td width="26%" style="padding-left: 15px">Prive</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Prive'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">({{ $x }})</td>
								@else
									<td class="text-right" width="12%">({{ $x }})</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Prive'] as  $i => $x)
								<td class="text-right" width="48%">({{ $x }})</td>
							@endforeach
						@endif
					</tr>

					<tr>
						@if(sizeof($tanggal)==2 )
							<td colspan="3" style=""><strong>ASET TETAP</strong></td>
						@else
							<td colspan="2" style=""><strong>ASET TETAP</strong></td>
						@endif
						<td width="26%" style="padding-left: 15px">Bagi Hasil</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['BagiHasil'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['BagiHasil'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Tanah</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Tanah'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Tanah'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-left: 15px"><strong>Total Modal</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Equitas'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Equitas'] as  $i => $x)
								<td class="text-right" width="48%">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

					<tr>
						<td width="26%">Bangunan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Bangunan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Bangunan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						
					</tr>

					<tr>
						<td width="26%">Peralatan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Peralatan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Peralatan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						
					</tr>

					<tr>
						<td width="26%">Kendaraan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Kendaraan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">{{ $x }}</td>
								@else
									<td class="text-right" width="12%">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Kendaraan'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						
					</tr>

					<tr>
						<td width="26%">Akumulasi Depresiasi Bangunan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['ADBangunan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">({{ $x }})</td>
								@else
									<td class="text-right" width="12%">({{ $x }})</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['ADBangunan'] as  $i => $x)
								<td class="text-right" width="24%">({{ $x }})</td>
							@endforeach
						@endif
						@if(sizeof($tanggal) == 2 )
							<td colspan="3" class="text-right" width="12%"></td>
						@else
							<td colspan="2" class="text-right" width="12%"></td>
						@endif
					</tr>

					<tr>
						<td width="26%">Akumulasi Depresiasi Peralatan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">({{ $x }})</td>
								@else
									<td class="text-right" width="12%">({{ $x }})</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								<td class="text-right" width="24%">({{ $x }})</td>
							@endforeach
						@endif
						@if(sizeof($tanggal) == 2 )
							<td colspan="3" class="text-right" width="12%"></td>
						@else
							<td colspan="2" class="text-right" width="12%"></td>
						@endif
					</tr>

					<tr>
						<td width="26%">Akumulasi Depresiasi Peralatan</td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">({{ $x }})</td>
								@else
									<td class="text-right" width="12%">({{ $x }})</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								<td class="text-right" width="24%">({{ $x }})</td>
							@endforeach
						@endif
						@if(sizeof($tanggal) == 2 )
							<td colspan="3" class="text-right" width="12%"></td>
						@else
							<td colspan="2" class="text-right" width="12%"></td>
						@endif
					</tr>

					<tr>
						<td width="26%"><STRONG>Total Aset Tetap</STRONG></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['AsetTetap'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%">({{ $x }})</td>
								@else
									<td class="text-right" width="12%">({{ $x }})</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['AsetTetap'] as  $i => $x)
								<td class="text-right" width="24%">{{ $x }}</td>
							@endforeach
						@endif
						@if(sizeof($tanggal) == 2 )
							<td colspan="3" class="text-right" width="12%"></td>
						@else
							<td colspan="2" class="text-right" width="12%"></td>
						@endif
					</tr>

					<tr>
						<td width="26%" style="padding-top: 15px"><strong>Total Aset</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['Aset'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%" style="padding-top: 15px">{{ $x }}</td>
								@else
									<td class="text-right" width="12%" style="padding-top: 15px">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['Aset'] as  $i => $x)
								<td class="text-right" width="24%" style="padding-top: 15px">{{ $x }}</td>
							@endforeach
						@endif
						<td width="26%" style="padding-top: 15px; padding-left: 15px"><strong>Total Kewajiban + Equitas</strong></td>
						@if(sizeof($tanggal) == 2 )
							@foreach($akuns['TotalKE'] as  $i => $x)
								@if($i == 0)
									<td class="text-right" width="12%" style="padding-top: 15px">{{ $x }}</td>
								@else
									<td class="text-right" width="12%" style="padding-top: 15px">{{ $x }}</td>
								@endif
							@endforeach
						@else
							@foreach($akuns['TotalKE'] as  $i => $x)
								<td class="text-right" width="48%" style="padding-top: 15px">{{ $x }}</td>
							@endforeach
						@endif
					</tr>

				</tbody>
			</table>
		</div>
	</div>
</div>

</body>

</html>
