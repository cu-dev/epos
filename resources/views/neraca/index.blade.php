@extends('layouts.admin')

@section('title')
    <title>EPOS | Neraca</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Neraca Saat Ini</h2>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('neraca/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-2"><h4><strong>Pilih Data Saldo</strong></h4></div>
	    						<div class="col-md-4">
	    							<select name="data[]" multiple id="data" class="form-control" required="">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-success" id="btnUbah" type="submit" style="margin-top: 2px">
										<span style="color:white">Tampil</span>
									</button>
	    						</div>
	    					</div>
						</form>
    				</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>AKTIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8"><strong>ASET LANCAR</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8">Kas Tunai</div>
							<div class="col-md-4 text-right data">{{ $data['KasTunai'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Kas Kecil</div>
							<div class="col-md-4 text-right data">{{ $data['KasKecil'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Kas Bank</div>
							<div class="col-md-4 text-right data">{{ $data['KasBank'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Kas Cek</div>
							<div class="col-md-4 text-right data">{{ $data['KasCek'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Kas BG</div>
							<div class="col-md-4 text-right data">{{ $data['KasBG'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Persediaan</div>
							<div class="col-md-4 text-right data">{{ $data['Persediaan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Perlengkapan</div>
							<div class="col-md-4 text-right data">{{ $data['Perlengkapan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Piutang Dagang</div>
							<div class="col-md-4 text-right data">{{ $data['PiutangDagang'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Piutang Karyawan</div>
							<div class="col-md-4 text-right data">{{ $data['PiutangKaryawan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Piutang Lain</div>
							<div class="col-md-4 text-right data">{{ $data['PiutangLain'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Piutang Tak Tertagih</div>
							<div class="col-md-4 text-right data">{{ $data['PiutangTakTertagih'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Asuransi Bayar Dimuka</div>
							<div class="col-md-4 text-right data">{{ $data['AsuransiDimuka'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Sewa Bayar Dimuka</div>
							<div class="col-md-4 text-right data">{{ $data['SewaDimuka'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">PPN Masukan</div>
							<div class="col-md-4 text-right data">{{ $data['PPNMasukan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8 menjorok"><strong>Total Aset Lancar</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['AsetLancar'] }}</strong></div>
						</div>
						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-8"><STRONG>ASET TETAP</STRONG></div>
							<div class="col-md-4 text-right data"></div>
						</div>

						<div class="row">
							<div class="col-md-8">Tanah</div>
							<div class="col-md-4 text-right data">{{ $data['Tanah'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Bangunan</div>
							<div class="col-md-4 text-right data">{{ $data['Bangunan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Peralatan</div>
							<div class="col-md-4 text-right data">{{ $data['Peralatan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Kendaraan</div>
							<div class="col-md-4 text-right data">{{ $data['Kendaraan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Akumulasi Depresiasi Bangunan</div>
							<div class="col-md-4 text-right data">({{ $data['ADBangunan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-8">Akumulasi Depresiasi Peralatan</div>
							<div class="col-md-4 text-right data">({{ $data['ADPeralatan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-8">Akumulasi Depresiasi Kendaraan</div>
							<div class="col-md-4 text-right data">({{ $data['ADKendaraan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-8 menjorok"><strong>Total Aset Tetap</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['AsetTetap'] }}</strong></div>
						</div>
					</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>PASIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8"><strong>KEWAJIBAN</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8">Kartu Kredit</div>
							<div class="col-md-4 text-right data">{{ $data['KartuKredit'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Hutang Dagang</div>
							<div class="col-md-4 text-right data">{{ $data['HutangDagang'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Hutang Pajak</div>
							<div class="col-md-4 text-right data">{{ $data['HutangPajak'] }}</div>
						</div>
						{{-- <div class="row">
							<div class="col-md-8">Taksiran Hutang Garansi</div>
							<div class="col-md-4 text-right data">{{ $data['TaksiranUtangGaransi'] }}</div>
						</div> --}}
						<div class="row">
							<div class="col-md-8">Hutang Konsinyasi</div>
							<div class="col-md-4 text-right data">{{ $data['HutangKonsinyasi'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Hutang PPN</div>
							<div class="col-md-4 text-right data">{{ $data['HutangPPN'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">PPN Keluaran</div>
							<div class="col-md-4 text-right data">{{ $data['PPNKeluaran'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Pembayaran dalam Proses</div>
							<div class="col-md-4 text-right data">{{ $data['PembayaranProses'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Deposito Pelanggan</div>
							<div class="col-md-4 text-right data">{{ $data['PendapatanDimuka'] }}</div>
						</div>

						<div class="row">
							<div class="col-md-8 menjorok"><strong>Hutang Jangka Pendek</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['HutangJangkaPendek'] }}</strong></div>
						</div>

						<div class="row">
							<div class="col-md-8">Hutang Bank</div>
							<div class="col-md-4 text-right data">{{ $data['HutangBank'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8 menjorok"><strong>Hutang Jangka Panjang</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['HutangJangkaPanjang'] }}</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8"><strong>Total Kewajiban</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['Kewajiban'] }}</strong></div>
						</div>

						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-8"><strong>MODAL</strong></div>
						</div>
						<div class="row">
							<div class="col-md-8">Modal</strong></div>
							<div class="col-md-4 text-right data">{{ $data['Modal'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Laba ditahan</div>
							<div class="col-md-4 text-right data">{{ $data['LabaTahan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8">Prive</div>
							<div class="col-md-4 text-right data">({{ $data['Prive'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-8">Bagi Hasil</div>
							<div class="col-md-4 text-right data">{{ $data['BagiHasil'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-8"><strong>Total Modal</strong></div>
							<div class="col-md-4 text-right data" style="text-decoration: overline; font-weight: bold"><strong>{{ $data['Equitas'] }}</strong></div>
						</div>
					</div>
    			</div>

    			<div class="row">
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 5px">
							<div class="col-md-8"><strong>Total Aset</strong></div>
							<div class="col-md-4 text-right data" style="font-weight: bold"><strong>{{ $data['Aset'] }}</strong></div>
						</div>
    				</div>
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 5px">
							<div class="col-md-8"><strong>Total Kewajiban + Modal</strong></div>
							<div class="col-md-4 text-right data" style="font-weight: bold"><strong>{{ $data['TotalKE'] }}</strong></div>
						</div>
    				</div>
    			</div>
      		</div>
    	</div>
  	</div>
<!-- </div> -->


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

            $('.right_col').css('min-height', $('.left_col').css('height'));

			$("#data").select2({
				maximumSelectionLength: 2
			});

			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                	$(el).text(duit);
                }else{
                	var a = '<span class="invisible">)</span>';
                	$(el).text(duit);
                	$(el).append(a);
                }
            });
		});
	</script>
@endsection
