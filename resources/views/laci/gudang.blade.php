@extends('layouts.admin')

@section('title')
    <title>EPOS | Setoran Laci</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Setoran Gudang ke Owner</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSimpanContainer">
                        <form method="post" action="{{ url('setoran_gudang') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($owner_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Setoran Gudang ke Grosir</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSimpanContainer">
                        <form method="post" action="{{ url('setoran_gudang_grosir') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal1">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($grosir_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanG" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Setoran Gudang ke Grosir</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSimpanContainer">
                        <form method="post" action="{{ url('setoran_gudang_grosir') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal1" required="">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control select2_single" required="">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($grosir_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}} [{{$user->level->nama}}]</option>
                                    @endforeach
                                    @foreach($owner_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}} [{{$user->level->nama}}]</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanG" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Bukaan Grosir</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('setoranke_grosir') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_setor">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanSetor" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Setor Tunai ke Bank</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('transfer_bank') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_setor" required="">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank_asal" class="select2_single form-control" required="">
                                    <option value="">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanTransfer" type="submit">
                                    <i class="fa fa-save"></i> <span>Transfer</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    <!-- kolom kanan -->
    <div class="col-md-8 col-xs-12">
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Gudang Hari Ini</h2>
                        <p class="pull-right">Uang di Laci : {{ \App\Util::ewon($laci->gudang) }}</p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranKasir">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setoran_today as $i => $setoran)
                                <tr id="{{ $setoran->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setoran->updated_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setoran->nominal) }}</td>
                                    <td style="text-align: center">
                                        @if($setoran->status == 0)
                                            <span class="label label-danger">Belum disetujui</span>
                                        @else
                                            <span class="label label-info">Sudah disetujui</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @foreach ($setoran_belum as $i => $setoran)
                                <tr id="{{ $setoran->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setoran->updated_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setoran->nominal) }}</td>
                                    <td style="text-align: center">
                                        @if($setoran->status == 0)
                                            <span class="label label-danger">Belum disetujui</span>
                                        @else
                                            <span class="label label-info">Sudah disetujui</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Grosir Hari Ini</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranGrosir">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setoran_grosir as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setor->user->nama }}</td>
                                    <td>{{ $setor->created_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td style="text-align: center">
                                        @if($setor->status == 0)
                                            <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                                Setujui
                                            </button>
                                        @else
                                            <span class="label label-info">Sudah disetujui</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Masuk ke Gudang</h2>
                        <p class="pull-right">Uang di Laci : {{ \App\Util::ewon($laci->gudang) }}</p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranTransfer">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pengirim</th>
                                    <th>Nominal</th>
                                    <th>Waktu</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($to_gudang as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setor->sender->nama }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td>{{ $setor->updated_at->format('d-m-Y H:i') }}</td>
                                    <td style="text-align: center">
                                        @if($setor->approve == 0)
                                            <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                                Setujui
                                            </button>
                                        @else
                                            <span class="label label-info">Sudah disetujui</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Keluar dari Gudang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranDariGudang">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Penerima</th>
                                    <th>Nominal</th>
                                    <th>Waktu</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($from_gudang as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                        @if($setor->penerima == NULL)
                                            <td>Bank</td>
                                        @else
                                            <td>{{ $setor->receiver->level->nama }} ({{ $setor->receiver->nama }})</td>
                                        @endif
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td>{{ $setor->updated_at->format('d-m-Y H:i') }}</td>
                                    <td style="text-align: center">
                                        @if($setor->approve == 0)
                                            <span class="label label-danger">
                                                Belum diterima
                                            </span>
                                        @else
                                            <span class="label label-info">Sudah diterima</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="formApprove" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="post">
        </form>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Buka berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'transfer_owner')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Tunai ke Bank berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'setuju')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Berhasil disetujui!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'setor_grosir')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran ke Grosir berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableSetoranKasir').DataTable();
        $('#tableSetoranDariGudang').DataTable();
        $('#tableSetoranGrosir').DataTable();
        $('#tableSetoranTransfer').DataTable();

        $(document).on('click', '#approve', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // console.log(id);
            $('#formApprove').find('form').attr('action', '{{ url("gudang_approve") }}' + '/' + id);

            var nama = $tr.find('td').first().next().text();
            var jumlah = $tr.find('td').first().next().next().text();
            // $('input[name="id"]').val(id);

            swal({
                title: 'Setujui?',
                text: '\"Setoran dari ' + nama + ' sebesar ' + jumlah + '?\"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Setujui!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formApprove').find('form').attr('action', '{{ url("gudang_approve") }}' + '/' + id);
                $('#formApprove').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('keyup', '#nominal', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->gudang }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpan').prop('disabled', false);
            } else {
                $('#btnSimpan').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('keyup', '#nominal1', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->gudang }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpanG').prop('disabled', false);
            } else {
                $('#btnSimpanG').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('keyup', '#nominal_setor', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->gudang }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpanSetor').prop('disabled', false);
            } else {
                $('#btnSimpanSetor').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('keyup', '#nominal_setor', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->gudang }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpanTransfer').prop('disabled', false);
            } else {
                $('#btnSimpanTransfer').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

    </script>
@endsection
