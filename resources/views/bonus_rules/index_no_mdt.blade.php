@extends('layouts.admin')

@section('title')
    <title>EPOS | Bonus Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnSimpan,
        #btnReset {
            margin-bottom: 0;
        }
        #tableBonus .btn {
            margin-bottom: 0;
        }

        .ikut_select2 {
            height: 38px;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Aturan Bonus Penjualan</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('rule-bonus') }}">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nama Aturan</label>
                                <input class="form-control ikut_select2" type="text" name="nama" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Minimal Nominal Transaksi</label>
                                {{-- <input class="form-control angka" type="text" name="nominal_" required="">
                                <input class="form-control" type="hidden" name="nominal"> --}}
                                <div class="input-group ikut_select2" style="margin-bottom: 0;">
                                    <div class="input-group-addon"> Rp</div>
                                    <input class="form-control angka ikut_select2" type="text" name="nominal_" required="">
                                    <input class="form-control" type="hidden" name="nominal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keuntungan yang Diharapkan</label>
                                {{-- <input class="form-control angka" type="text" name="profit_" required="">
                                <input class="form-control" type="hidden" name="profit"> --}}
                                <div class="input-group ikut_select2" style="margin-bottom: 0;">
                                    <div class="input-group-addon"> Rp</div>
                                    <input class="form-control angka ikut_select2" type="text" name="profit_" required="">
                                    <input class="form-control" type="hidden" name="profit">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Minimal Jumlah Item</label>
                                <input class="form-control angka ikut_select2" type="text" name="jumlah_" required="">
                                <input class="form-control" type="hidden" name="jumlah">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Status Pelanggan</label>
                                <select name="status" class="form-control select2_single" required="" style="height: 34px">
                                    <option value="">Pilih Status Pelanggan</option>
                                    <option value="0">Semua Status Pelanggan</option>
                                    <option value="1">Pelanggan Eceran</option>
                                    <option value="2">Pelanggan Grosir</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Kelipatan Bonus</label>
                                <select name="kelipatan" class="form-control select2_single" required="" style="height: 34px">
                                    <option value="">Pilih Kelipatan Bonus</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                            
                            <div class="form-group" style="margin-top: 20px;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->
    <!-- baris bawah -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Aturan Bonus Penjualan</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover table-responsive" style="margin-bottom: 0;" id="tableBonus">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Aturan</th>
                            <th>Minimal Nominal Transaksi</th>
                            <th>Keuntungan yang Diharapkan</th>
                            <th>Minimal Jumlah Item Bonus</th>
                            <th>Status Pelanggan</th>
                            <th>Kelipatan Bonus</th>
                            <th>Operator</th>
                            <th style="width: 75px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rules as $i => $rule)
                            <tr id="{{ $rule->id }}">
                                <td>{{ $i+1 }}</td>
                                <td>{{ $rule->nama }}</td>
                                <td class="text-right" nominal="{{ $rule->nominal }}">{{ \App\Util::ewon($rule->nominal) }}</td>
                                <td class="text-right" profit="{{ $rule->profit }}">{{ \App\Util::ewon($rule->profit) }}</td>
                                <td jumlah="{{ $rule->jumlah }}">{{ \App\Util::angka($rule->jumlah) }} Item</td>
                                @if($rule->status == 0)
                                    <td status="{{ $rule->status }}">Semua Status Pelanggan</td>
                                @elseif($rule->status == 1)
                                    <td status="{{ $rule->status }}">Eceran</td>
                                @else
                                    <td status="{{ $rule->status }}">Grosir</td>
                                @endif
                                @if($rule->kelipatan == 1)
                                    <td kelipatan="{{ $rule->kelipatan }}">Aktif</td>
                                @else
                                    <td kelipatan="{{ $rule->kelipatan }}">Tidak Aktif</td>
                                @endif
                                <td>{{ $rule->user->nama }}</td>
                                <td class="text-center">
                                    <a href="{{ url('rule-bonus/'.$rule->id.'/show') }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Bonus Penjualan"> 
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <button class="btn btn-xs btn-warning" id="btnUbah" title="Ubah Bonus Penjualan">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    @if($rule->aktif == 1)
                                        <button class="btn btn-xs btn-danger btnHapus" id="btnHapus" title="Nonakifkan Bonus Penjualan">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    @else
                                        <button class="btn btn-xs btn-success" id="btnAktif" title="Akifkan Bonus Penjualan">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- </div> -->

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil nonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableBonus').DataTable();

        $(document).on('click', '#btnUbah', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            var nominal = $tr.find('td').first().next().next().attr('nominal');
            // var nominal = $tr.find('td').first().next().next().next().attr('nominal');
            var profit = $tr.find('td').first().next().next().next().attr('profit');
            var jumlah = $tr.find('td').first().next().next().next().next().attr('jumlah');
            var status = $tr.find('td').first().next().next().next().next().next().attr('status');
            var kelipatan = $tr.find('td').first().next().next().next().next().next().next().attr('kelipatan');
            nominal = parseInt(nominal);
            profit = parseInt(profit);
            jumlah = parseInt(jumlah);

            console.log('id '+id+'nama '+nama+' nominal '+nominal+' profit '+ profit+' jumlah '+jumlah+' status '+ status+' kelipatan '+ kelipatan);
            
            $('input[name="nama"]').val(nama);
            $('input[name="nominal_"]').val(nominal.toLocaleString());
            $('input[name="nominal"]').val(nominal);
            $('input[name="profit_"]').val(profit.toLocaleString());
            $('input[name="profit"]').val(profit);
            $('input[name="jumlah_"]').val(jumlah);
            $('input[name="jumlah"]').val(jumlah);
            $('select[name="status"]').val(status).change();
            $('select[name="kelipatan"]').val(kelipatan).change();

            $('#formSimpanContainer').find('form').attr('action', '{{ url("rule-bonus") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Aturan Bonus');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama + '\" akan dinonaktifkan!',
                type: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("rule-bonus") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("rule-bonus/aktif") }}' + '/' + id);
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

    </script>
@endsection
