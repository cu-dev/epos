@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Bonus Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnSimpan,
        #btnReset {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Item Bonus</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('rule-bonus/relasi') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="rule_id" id="rule_id" class="form-control" value="{{ $rule->id }}">
                    <input type="hidden" name="sum" id="sum" class="form-control">

                    <div class="form-group">
                        <label class="control-label">Pilih Item</label>
                        <select name="item_id" class="select2_single form-control" id="item_id" reuired="">
                            <option value="">Pilih Item</option>
                            @foreach ($items as $item)
                            <option value="{{ $item->id }}">[{{ $item->kode }}] {{ $item->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Jumlah</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="divJumlah">
                                <input type="text" name="jumlah" class="form-control select2_tinggi" id="jumlah" required="">
                            </div>
                            <div class="col-md-6" id="divSatuan">
                                <select id="satuan" name="satuan" class="form-control select2_single" required="">
                                    <option value="">Pilih Satuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <button class="btn btn-success" id="btnSimpan" type="submit" disabled="">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                    </div>
                </form>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <!-- kolom kanan -->
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Item Bonus Aturan <b>{{ $rule->nama }}</b></h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <a href="{{ url('rule-bonus') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover table-responsive" style="margin-bottom: 0;" id="tableBonus">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Item</th>
                            <th>Jumlah</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($relasis as $i => $relasi)
                            <tr id="{{ $relasi->id }}" kode="{{ $relasi->bonus->kode }}" item_id="{{ $relasi->bonus_id }}">
                                <td>{{ $i+1 }}</td>
                                <td>{{ $relasi->bonus->nama }}</td>
                                <td id="jumlah_relasi" jumlah="{{ $relasi_satuan[$i]['jumlah'] }}" satuan_id="{{ $relasi_satuan[$i]['satuan_id'] }}">
                                    {{ $relasi_satuan[$i]['jumlah'] }} {{ $relasi_satuan[$i]['satuan'] }}
                                </td>
                                <td class="tengah-h">
                                    <button class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Item Bonus">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button class="btn btn-xs btn-danger btnHapus" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus Item Bonus">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item Bonus Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh Item Sudah Ada!',
                text: 'Item Bonus Penjualan gagal ditambah!',
                timer: 3000,
                type: 'warning'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item Bonus Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item Bonus Penjualan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableBonus').DataTable();
        var satuans = [];
        // var nominal_bank = [];

        $(document).ready(function() {
            var url = "{{ url('rule-bonus') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $(".select2_single").each(function(index, el) {
                $(el).val('').trigger('change');
            });

            $('#jumlah').val('');
            $('#sum').val('');
            $('#satuan').val('');

            $('#jumlah').prop('disabled', true);
            $('#satuan').prop('disabled', true);

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);
        });

        $(document).on('click', '#btnUbah', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var item_id = $tr.attr('item_id');
            // var kode = $tr.find('td').first().next().text();
            var jumlah = $tr.find('td').first().next().next().attr('jumlah');
            var satuan_id = $tr.find('td').first().next().next().attr('satuan_id');
            // console.log(item_id, jumlah, satuan_id);

            $('#item_id').val(item_id).trigger('change');
            $('#item_id').prop('disabled', true);
            $('#jumlah').val(jumlah);

            // wait(7000);
            $('#jumlah').prop('disabled', false);
            $('#satuan').prop('disabled', false);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("rule-bonus/relasi") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Bonus Item');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            console.log(id);
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus Item?',
                text: '\"' + nama + '\" akan dihapus dari Relasi!',
                type: 'warning',
                            showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("rule-bonus/relasi") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('change', '#item_id', function(event) {
            event.preventDefault();

            var item_id = $(this).val();
            if (item_id == '') {
                $('#jumlah').val('');
                $('#satuan').val('');

                $('#jumlah').prop('disabled', true);
                $('#satuan').prop('disabled', true);

                $('#btnSimpan').prop('disabled', true);
            } else {
                var rule = {{ $rule->id }};
                var url  = "{{ url('rule-bonus') }}" + '/' + rule + '/item'+ '/' + item_id + '/json';

                // console.log(url);
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var satuan_item = [];
                    var item_data = data.item;
                    var item_list = data.item_list;
                    var satuan_list = data.satuan_list;
                    for (var i = 0; i < item_data.satuan_pembelians.length; i++) {
                        for (var j = 0; j < satuans.length; j++) {
                            if (satuans[j].id == item_data.satuan_pembelians[i].satuan_id) {
                                var satuan = {
                                    id: satuans[j].id,
                                    kode: satuans[j].kode,
                                    konversi: item_data.satuan_pembelians[i].konversi
                                }
                                satuan_item.push(satuan);
                                break;
                            }
                        }
                    }

                    ul_satuan = ``;
                    var count = satuan_item.length -1;

                    for (var i = count; i >= 0; i--) {
                        var satuan = satuan_item[i];
                        if(i == count){
                        ul_satuan += `<option value="`+ satuan.id +`" konversi="`+ satuan.konversi +`">`+ satuan.kode +`</option>`;
                        }else{
                            ul_satuan += `<option value="`+ satuan.id +`" konversi="`+ satuan.konversi +`" >`+ satuan.kode +`</option>`;
                        }
                    }

                    $('#satuan').empty();
                    $('#satuan').append(ul_satuan);
                    var index_item = -1;

                    for(var i=0; i <= item_list.length; i++){
                        if(item_list[i] == item_id) index_item = i;
                    }

                    if(index_item == -1){
                        //jika item yang terpilih bukan yang sudah terpilih
                        $('#jumlah').val(1);
                        $('#sum').val(1);
                    }else{
                        // console.log(satuan_list[index_item]);
                        $('#satuan').val(satuan_list[index_item]);
                    }

                    $('#spinner').hide();

                    $('#jumlah').prop('disabled', false);
                    $('#satuan').prop('disabled', false);
                    $('#btnSimpan').prop('disabled', false);
                });
            }
        });

        $(document).on('keyup', '#jumlah', function(event) {
            event.preventDefault();

            var jumlah = $(this).val();
            var satuan = $('#satuan').val();
            var konversi = $('#satuan').find('option[value="'+satuan+'"]').attr('konversi');
            var sum = jumlah * konversi;
            var item_id = $('#item_id').val();

            if(item_id != ''){
                if(isNaN(sum)) {
                    sum = 0;
                    $('#divJumlah').addClass('has-error');
                    $('#divSatuan').addClass('has-error');
                }else{
                    $('#divJumlah').removeClass('has-error');
                    $('#divSatuan').removeClass('has-error');
                    $('#sum').val(sum);
                }
            }
        });

        $(document).on('change', '#satuan', function(event) {
            event.preventDefault();

            var satuan = $(this).val();
            var jumlah = $('#jumlah').val();
            var satuan = $(this).val(); var konversi = $('#satuan').find('option[value="'+satuan+'"]').attr('konversi');

            var sum = jumlah * konversi;

            var item_id = $('#item_id').val();

            if(item_id != ''){
                if(isNaN(sum)) {
                    sum = 0;
                    $('#divJumlah').addClass('has-error');
                    $('#divSatuan').addClass('has-error');
                }else{
                    $('#divJumlah').removeClass('has-error');
                    $('#divSatuan').removeClass('has-error');
                    $('#sum').val(sum);
                }
            }
        });

    </script>
@endsection
