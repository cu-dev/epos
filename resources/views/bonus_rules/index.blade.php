@extends('layouts.admin')

@section('title')
    <title>EPOS | Bonus Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        #btnHapus,
        #btnAktif {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnSimpan,
        #btnReset {
            margin-bottom: 0;
        }
        #tableBonus .btn {
            margin-bottom: 0;
        }

        .ikut_select2 {
            height: 38px;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Aturan Bonus Penjualan</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('rule-bonus') }}">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nama Aturan</label>
                                <input class="form-control ikut_select2" type="text" name="nama" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Minimal Nominal Transaksi</label>
                                {{-- <input class="form-control angka" type="text" name="nominal_" required="">
                                <input class="form-control" type="hidden" name="nominal"> --}}
                                <div class="input-group ikut_select2" style="margin-bottom: 0;">
                                    <div class="input-group-addon"> Rp</div>
                                    <input class="form-control angka ikut_select2" type="text" name="nominal_" required="">
                                    <input class="form-control" type="hidden" name="nominal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keuntungan yang Diharapkan</label>
                                {{-- <input class="form-control angka" type="text" name="profit_" required="">
                                <input class="form-control" type="hidden" name="profit"> --}}
                                <div class="input-group ikut_select2" style="margin-bottom: 0;">
                                    <div class="input-group-addon"> Rp</div>
                                    <input class="form-control angka ikut_select2" type="text" name="profit_" required="">
                                    <input class="form-control" type="hidden" name="profit">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Minimal Jumlah Item</label>
                                <input class="form-control angka ikut_select2" type="text" name="jumlah_" required="">
                                <input class="form-control" type="hidden" name="jumlah">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Status Pelanggan</label>
                                <select name="status" class="form-control select2_single" required="" style="height: 34px">
                                    <option value="">Pilih Status Pelanggan</option>
                                    <option value="0">Semua Status Pelanggan</option>
                                    <option value="1">Pelanggan Eceran</option>
                                    <option value="2">Pelanggan Grosir</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Kelipatan Bonus</label>
                                <select name="kelipatan" class="form-control select2_single" required="" style="height: 34px">
                                    <option value="">Pilih Kelipatan Bonus</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                            
                            <div class="form-group" style="margin-top: 20px;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->
<!-- baris bawah -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Aturan Bonus Penjualan</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('rule-bonus/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="nama">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableBonus" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="nama">Nama Aturan</th>
                                <th class="sorting" field="nominal">Minimal Nominal Transaksi</th>
                                <th class="sorting" field="profit">Keuntungan yang Diharapkan</th>
                                <th class="sorting" field="jumlah">Minimal Jumlah Item Bonus</th>
                                <th class="sorting" field="stastu">Status Pelanggan</th>
                                <th class="sorting" field="kelipatan">Kelipatan Bonus</th>
                                <th class="sorting" field="user_nama">Operator</th>
                                <th class="sorting" field="kode" style="width: 120px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Aturan Bonus Penjualan</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('rule-bonus/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="nama">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableBonusNon" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="nama">Nama Aturan</th>
                                <th class="sorting" field="nominal">Minimal Nominal Transaksi</th>
                                <th class="sorting" field="profit">Keuntungan yang Diharapkan</th>
                                <th class="sorting" field="jumlah">Minimal Jumlah Item Bonus</th>
                                <th class="sorting" field="stastu">Status Pelanggan</th>
                                <th class="sorting" field="kelipatan">Kelipatan Bonus</th>
                                <th class="sorting" field="user_nama">Operator</th>
                                <th class="sorting" field="kode" style="width: 120px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil nonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Aturan Bonus Penjualan berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('rule-bonus/mdt1') }}";
        var mdt2 = "{{ url('rule-bonus/mdt2') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="9" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="9" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var bonus_rule = data[i];
                        var buttons = bonus_rule.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Bonus Penjualan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Bonus Penjualan">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonakifkan Bonus Penjualan">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${bonus_rule.id}">
                                    <td>${nomor}</td>
                                    <td>${bonus_rule.nama}</td>
                                    <td class="text-right" nominal="${bonus_rule.nominal}">${bonus_rule.mdt_nominal}</td>
                                    <td class="text-right" profit="${bonus_rule.profit}">${bonus_rule.mdt_profit}</td>
                                    <td jumlah="${bonus_rule.jumlah}">${bonus_rule.mdt_jumlah} Item</td>
                                    <td status="${bonus_rule.status}">${bonus_rule.mdt_status}</td>
                                    <td kelipatan="${bonus_rule.kelipatan}">${bonus_rule.mdt_kelipatan}</td>
                                    <td>${bonus_rule.operator}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var bonus_rule = data[i];
                        var buttons = bonus_rule.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Bonus Penjualan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Bonus Penjualan">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.aktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnAktif" data-toggle="tooltip" data-placement="top" title="Akifkan Bonus Penjualan">
                                    <i class="fa fa-check"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${bonus_rule.id}">
                                    <td>${nomor}</td>
                                    <td>${bonus_rule.nama}</td>
                                    <td class="text-right" nominal="${bonus_rule.nominal}">${bonus_rule.mdt_nominal}</td>
                                    <td class="text-right" profit="${bonus_rule.profit}">${bonus_rule.mdt_profit}</td>
                                    <td jumlah="${bonus_rule.jumlah}">${bonus_rule.mdt_jumlah} Item</td>
                                    <td status="${bonus_rule.status}">${bonus_rule.mdt_status}</td>
                                    <td kelipatan="${bonus_rule.kelipatan}">${bonus_rule.mdt_kelipatan}</td>
                                    <td>${bonus_rule.operator}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).on('click', '#btnUbah', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            var nominal = $tr.find('td').first().next().next().attr('nominal');
            // var nominal = $tr.find('td').first().next().next().next().attr('nominal');
            var profit = $tr.find('td').first().next().next().next().attr('profit');
            var jumlah = $tr.find('td').first().next().next().next().next().attr('jumlah');
            var status = $tr.find('td').first().next().next().next().next().next().attr('status');
            var kelipatan = $tr.find('td').first().next().next().next().next().next().next().attr('kelipatan');
            nominal = parseInt(nominal);
            profit = parseInt(profit);
            jumlah = parseInt(jumlah);

            console.log('id '+id+'nama '+nama+' nominal '+nominal+' profit '+ profit+' jumlah '+jumlah+' status '+ status+' kelipatan '+ kelipatan);
            
            $('input[name="nama"]').val(nama);
            $('input[name="nominal_"]').val(nominal.toLocaleString());
            $('input[name="nominal"]').val(nominal);
            $('input[name="profit_"]').val(profit.toLocaleString());
            $('input[name="profit"]').val(profit);
            $('input[name="jumlah_"]').val(jumlah);
            $('input[name="jumlah"]').val(jumlah);
            $('select[name="status"]').val(status).change();
            $('select[name="kelipatan"]').val(kelipatan).change();

            $('#formSimpanContainer').find('form').attr('action', '{{ url("rule-bonus") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Aturan Bonus');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama + '\" akan dinonaktifkan!',
                type: 'warning',
                showCancelButton: true,
                showCloseButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("rule-bonus") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function(event) {
            event.preventDefault();
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("rule-bonus/aktif") }}' + '/' + id);
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

    </script>
@endsection
