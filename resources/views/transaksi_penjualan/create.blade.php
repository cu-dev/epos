@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Transaksi Eceran</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .form-danger, 
        .form-danger:focus {
            border-color: red;
        }

        #metodePembayaranButtonGroup,
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }

        #metodePembayaranButtonGroup > label {
            border-radius: 0;
        }

        #metodePembayaranButtonGroup > label.active {
            background: #26b99a;
            border: 1px solid #169f85;
            color: white;
            font-weight: bold;
            border-radius: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-8">
                        <h2 id="formSimpanTitle">Tambah Transaksi Eceran</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Item</label>
                        <select name="item_id" id="pilihItem" class="select2_single form-control">
                            <option id="default">Pilih Item</option>
                            @foreach ($items as $item)
                            <option value="{{ $item->kode }}">[{{ $item->kode }}] {{ $item->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo">
                    <thead>
                        <tr>
                            <th style="text-align: left;">Item</th>
                            <th style="text-align: left;">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Belanja</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;">
                            <thead>
                                <tr>
                                    <th style="width: 20px;"></th>
                                    <th style="text-align: left;">Item</th>
                                    <th style="text-align: left; width: 100px;">Jumlah</th>
                                    <th style="text-align: left; width: 100px;">Satuan</th>
                                    <th style="text-align: left; width: 150px;">Harga/Satuan</th>
                                    <th style="text-align: left; width: 160px;">Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa"></i> Transfer</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select name="bank_id" class="select2_single form-control">
                                            <option id="default-bank" value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalTransfer" id="inputNominalTransfer" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Harga Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" readonly="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Jumlah Bayar</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" readonly="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Kembali</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control angka" readonly="">
                                </div>
                            </div>
                        </div>
                        <div id="formSimpanContainer" style="margin-top: 20px;">
                            <form id="form-simpan" action="{{ url('transaksi') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="kode_transaksi" value="" />
                                <input type="hidden" name="harga_total" />
                                <input type="hidden" name="jumlah_bayar" />
                                <input type="hidden" name="kembali" />
                                <input type="hidden" name="bank_id" />
                                
                                <input type="hidden" name="nominal_tunai" />
                                
                                <input type="hidden" name="no_transfer" />
                                <input type="hidden" name="nominal_transfer" />

                                <div id="append-section"></div>
                                <div class="clearfix">
                                    <div class="form-group pull-left">
                                        <button type="submit" class="btn btn-success" id="submit" style="width: 100px;">
                                            <i class="fa fa-check"></i> OK
                                        </button>   
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                html: '<p>Transaksi Eceran berhasil ditambah!</p>' + 
                        '<table class="table table-bordered table-striped">' +
                            '<tr>' +
                                '<td class="text-left">Total</td>' +
                                '<td class="text-right">' + parseFloat('{{ session('data')['total'] }}').toLocaleString(undefined, {minimumFractionDigits: 2}) + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td class="text-left">Tunai</td>' +
                                '<td class="text-right">' + parseFloat('{{ session('data')['tunai'] }}').toLocaleString(undefined, {minimumFractionDigits: 2}) + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td class="text-left">Kembalian</td>' +
                                '<td class="text-right">' + parseFloat('{{ session('data')['kembalian'] }}').toLocaleString(undefined, {minimumFractionDigits: 2}) + '</td>' +
                            '</tr>' +
                        '</table>',
                // timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Eceran gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tabelTransaksiPenjualan').DataTable();

        /*function updateHargaTotal() {
            var harga_total = 0;

            $('.subtotal').each(function(index, el) {
                var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(tmp)) tmp = 0;
                harga_total += tmp;
            });

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('input[name="harga_total"]').val(harga_total);
            $('#hiddenHargaTotal').val(harga_total);

            var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            var kembali      = jumlah_bayar - harga_total;

            if (kembali < 0 || isNaN(kembali)) {
                kembali = 0;
                $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
            } else {
                $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
            }

            $('#inputTotalKembali').val(kembali.toLocaleString());
            $('input[name="kembali"]').val(kembali);
        }*/

        function updateHargaOnKeyup() {
            var $harga_total = $('#inputHargaTotal');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $kembali = $('#inputTotalKembali');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();

            nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_transfer = parseInt(nominal_transfer.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;

            var harga_total = parseFloat($harga_total.val().replace(/\D/g, ''), 10) / 100;
            var jumlah_bayar = nominal_tunai + nominal_transfer;
            var kembali = jumlah_bayar - harga_total;

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            
            if (kembali < 0 || isNaN(kembali)) {
                kembali = 0;
                $('#form-simpan').find('button[type="submit"]').prop('disabled', true);
            } else if ($('#btnTransfer').hasClass('btn-warning')) {
                if ($('input[name="bank_id"]').val() == '' || $('input[name="no_transfer"]').val() == '') {
                    $('#form-simpan').find('button[type="submit"]').prop('disabled', true);
                } else {
                    $('#form-simpan').find('button[type="submit"]').prop('disabled', false);
                }
            } else {
                $('#form-simpan').find('button[type="submit"]').prop('disabled', false);
            }

            if (harga_total <= 0) {
                $('#form-simpan').find('button[type="submit"]').prop('disabled', true);
            }

            $harga_total.val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
            $jumlah_bayar.val(jumlah_bayar.toLocaleString(undefined, {minimumFractionDigits: 2}));
            $kembali.val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="kembali"]').val(kembali);
        }

        function cariItem(kode, url, tr) {
            $.get(url, function(data) {
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;
                var harga = data.harga.harga;
                harga = parseInt(harga.replace(/\D/g, ''), 10) / 100;
                
                var satuan_item = [];
                for (var i = 0; i < item.satuans.length; i++) {
                    for (var j = 0; j < satuans.length; j++) {
                        if (satuans[j].id == item.satuans[i].satuan_id) {
                            satuan_item.push(satuans[j]);
                            break;
                        }
                    }
                }
                
                // var satuan_id = [];
                // for (var i = 0; i < item.satuans.length; i++) {
                //  satuan_id.push(item.satuans[i].id);
                // }

                var select_satuan = '' +
                    '<select name="satuan" class="form-control input-sm">';
                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    select_satuan += '' +
                    '<option value="'+satuan.id+'">'+satuan.kode+'</option>';
                }
                // for (var i = 0; i < satuans.length; i++) {
                //  var satuan = satuans[i];
                //  if (satuan_id.includes(satuan.id)) {
                //      select_satuan += '' +
                //      '<option value="'+satuan.id+'">'+satuan.kode+'</option>';
                //  }
                // }
                select_satuan += '' +
                    '</select>';
                
                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="konversi[]" id="konversi-' +kode+'" value="1" disabled />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="'+harga+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-'+kode+'" class="subtotal" value="'+harga+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="stoktotal[]" id="stoktotal-'+kode+'" class="stoktotal" value="'+stoktotal+'" disabled />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="padding-top: 13px">'+nama+'</td>'+
                            '<td>'+
                                '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="1" />'+
                            '</td>'+
                            '<td>'+select_satuan+'</td>'+
                            '<td>'+
                                '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + harga.toLocaleString(undefined, {minimumFractionDigits: 2}) + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                                // '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + harga.toLocaleString(undefined, {minimumFractionDigits: 2}) + '" readonly="readonly" />'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputSubTotalContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                    '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka inputSubTotal" value="' + harga.toLocaleString(undefined, {minimumFractionDigits: 2}) + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                                // '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" value="' + harga.toLocaleString(undefined, {minimumFractionDigits: 2}) + '" readonly="readonly" />'+
                            '</td>'+
                        '</tr>');
                }

                $('#tabelInfo').find('tbody').children('tr').children().first().text(nama);
                $('#tabelInfo').find('tbody').children('tr').children().last().text(stoktotal);

                var $harga_total = $('#inputHargaTotal');
                var $jumlah_bayar = $('#inputJumlahBayar');
                var $kembali = $('#inputTotalKembali');

                var harga_total = parseFloat($harga_total.val().replace(/\D/g, ''), 10) / 100;
                var jumlah_bayar = parseFloat($jumlah_bayar.val().replace(/\D/g, ''), 10) / 100;
                var kembali = jumlah_bayar - harga_total;
                kembali = kembali < 0 ? 0 : kembali;

                $harga_total.val((harga_total + harga).toLocaleString(undefined, {minimumFractionDigits: 2}));
                $jumlah_bayar.val(jumlah_bayar.toLocaleString(undefined, {minimumFractionDigits: 2}));
                $kembali.val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));

                $('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + harga);
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val(jumlah_bayar);
                $('#formSimpanContainer').find('input[name="kembali"]').val(kembali);
            });
        }

        var satuans = [];
        $(document).ready(function() {
            var url = "{{ url('transaksi/create') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                allowClear: true
            });

            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');

            $('#inputTunaiContainer').hide();
            $('#inputTransferContainer').hide();
            $('#inputTransferContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $(".select2_single").select2({
                // allowClear: true
            });

            $('#inputHargaTotal').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);

            $('select[name="bank_id"]').next('span').css({
                'width': '100%',
                'margin-bottom': '10px'
            });

            url = "{{ url('satuan/json') }}";
            $.get(url, function(response) {
                satuans = response.satuans;
            })
        });

        $(window).on('load', function(event) {
            var url = "{{ url('transaksi/last/json') }}";
            var tanggal = printTanggalSekarang('dd/mm/yyyy');

            $.get(url, function(data) {
                if (data.transaksi_penjualan === null) {
                    var kode = int4digit(1);
                    var kode_transaksi = kode + '/TRAJ/' + tanggal;
                } else {
                    var kode_transaksi = data.transaksi_penjualan.kode_transaksi;
                    var dd_transaksi = kode_transaksi.split('/')[2];
                    var mm_transaksi = kode_transaksi.split('/')[3];
                    var yyyy_transaksi = kode_transaksi.split('/')[4];
                    var tanggal_transaksi = dd_transaksi + '/' + mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode_transaksi = kode + '/TRAJ/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/TRAJ/' + tanggal_transaksi;
                    }
                }

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });

        // var keyupFromScanner = false;
        $(document).scannerDetection({
            avgTimeByChar: 40,
            onComplete: function(code, qty) {
                console.log('Kode: ' + code, qty);
            },
            onError: function(error) {
                // console.log('Barcode: ' + error);
                var kode = error;
                var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
                var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

                var terpilih = false;
                $('input[name="item_kode[]"]').each(function(index, el) {
                    if ($(el).val() == kode) {
                        terpilih = true;
                    }
                });

                if (terpilih) {
                    // keyupFromScanner = true;
                    var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val());
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val(jumlah_awal + 1);
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('keyup');
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('blur');
                } else {
                    cariItem(kode, url, tr);
                }
            }
        });

        $(document).on('change', '#pilihItem', function(event) {
            event.preventDefault();
            
            var kode = $(this).val();
            var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
            var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            cariItem(kode, url, tr);
        });

        var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();
            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();
            $(this).val(temp_jumlah);
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();
            
            var jumlah = $(this).val();
            temp_jumlah = jumlah;

            var kode = $(this).parents('tr').first().data('id');
            var satuan = $('#satuan-'+kode).val();
            var konversi = $('#konversi-'+kode).val();
            var stoktotal = $('#stoktotal-'+kode).val();
            
            var td = $(this).parents('td').first();

            if (jumlah == '') {
                jumlah = 0;
                td.addClass('has-error');
            } else {
                td.removeClass('has-error');
            }

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            satuan = parseFloat(satuan);
            if (isNaN(satuan) || satuan <= 0) satuan = 0;
            konversi = parseFloat(konversi);
            if (isNaN(konversi) || konversi <= 0) konversi = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                } else {
                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.next().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;
                        var harga_total = 0;

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

                        $('#harga-'+kode).val(harga);
                        $('#subtotal-'+kode).val(subtotal);

                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });

        $(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();

            var satuan = $(this).val();

            var kode = $(this).parents('tr').data('id');
            var stoktotal = $('#stoktotal-'+kode).val();
            var jumlah = $('#jumlah-'+kode).val();

            if (jumlah === '') jumlah = 0;

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var td = $(this).parents('td').first();

            td.prev().find('#inputJumlahItem').val(jumlah.toLocaleString());

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                } else {
                    var konversi = data.konversi;
                    konversi = parseFloat(konversi);
                    if (isNaN(konversi) || konversi <= 0) konversi = 0;

                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.prev().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;
                        var harga_total = 0;

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

                        $('#harga-'+kode).val(harga);
                        $('#subtotal-'+kode).val(subtotal);

                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
                $('#btnTransfer').removeClass('btn-warning');
                $('#btnTransfer').addClass('btn-default');
                $('#btnTransfer').find('i').removeClass('fa-check');
                $('#inputTransferContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_transfer"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').addClass('fa-check');
                $('#inputTransferContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
                $('#btnTunai').removeClass('btn-danger');
                $('#btnTunai').addClass('btn-default');
                $('#btnTunai').find('i').removeClass('fa-check');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTransferContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_transfer"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();
            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;

            $(this).val(nominal_tunai.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();
            var id = $(this).val();

            $('input[name="bank_id"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();
            var no_transfer = $(this).val();

            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();
            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;

            $(this).val(nominal_transfer.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();
            
            var kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var subtotal = parseFloat(tr.children().last().find('input').val().replace(/\D/g, ''), 10) / 100;
            var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
            var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
            var kembali = parseFloat($('#inputTotalKembali').val().replace(/\D/g, ''), 10) / 100;

            harga_total -= subtotal;
            kembali = jumlah_bayar - harga_total;
            kembali = (kembali > 0) ? kembali : 0;

            $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
            $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="kembali"]').val(kembali);

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });
        
    </script>
@endsection
