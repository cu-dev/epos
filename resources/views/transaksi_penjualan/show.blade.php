@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Transaksi Eceran {{ $transaksi_penjualan->kode_transaksi }}</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Transaksi Eceran</h2>
                <a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur/create') }}" class="btn btn-sm btn-success pull-right" style="margin-right: 0;">
                    <i class="fa fa-sign-in"></i> Tambah Retur
                </a>
                <a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur') }}" class="btn btn-sm btn-warning pull-right">
                    <i class="fa fa-sign-in"></i> Lihat Retur
                </a>
                <a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_penjualan->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="border-top: none;">Kode Transaksi</th>
                                    <td style="border-top: none;">{{ $transaksi_penjualan->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Total Harga</th>
                                    <td>Rp {{ \App\Util::duit($transaksi_penjualan->harga_total) }}</td>
                                </tr>
                                <tr>
                                    <th>Total Bayar</th>
                                    <td>Rp {{ \App\Util::duit($transaksi_penjualan->jumlah_bayar) }}</td>
                                </tr>
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td>Rp {{ \App\Util::duit($transaksi_penjualan->nominal_tunai) }}</td>
                                </tr>
                                <tr>
                                    <th>No. Debit</th>
                                    <td>
                                        @if (!empty($transaksi_penjualan->no_debit))
                                        {{ $transaksi_penjualan->no_debit }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nominal Debit</th>
                                    <td>Rp {{ \App\Util::duit($transaksi_penjualan->nominal_debit) }}</td>
                                </tr>
                                <tr>
                                    <th>Kasir</th>
                                    <td>{{ $transaksi_penjualan->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Item</th>
                                    <th>Jumlah (pcs)</th>
                                    <th>Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_penjualan as $i => $relasi)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td>{{ $relasi->jumlah }}</td>
                                    <td>{{ $relasi->jumlah_bonus }} pcs {{ $relasi->item->bonus->nama }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#tabel-item').DataTable();

        $(document).ready(function() {
            var url = "{{ url('transaksi') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });
    </script>
@endsection
