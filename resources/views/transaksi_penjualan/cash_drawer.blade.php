@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Transaksi Eceran</title>
@endsection

@section('style')
	<style type="text/css">
		.alert {
			color: white;
			font-size: 1.25em;
		}

		.form-group > .control-label {
			margin-bottom: 5px;
		}

		.form-group > .input-group > .input-group-addon {
			border-radius: 0;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Transaksi Eceran</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div class="alert alert-danger text-center">
							<strong>
								Jumlah Nominal Transaksi Melebihi Nominal Money Limit
							</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('transaksi/create') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});

		$(document).on('keyup', '#nominal_setor', function(event) {
			event.preventDefault();
			
			var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal)) nominal = 0;

			$(this).val(nominal.toLocaleString());
			$(this).next().val(nominal);
		});
	</script>
@endsection
