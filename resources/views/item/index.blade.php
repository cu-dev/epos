@extends('layouts.admin')

@section('title')
    <title>EPOS | Item</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnHistory, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnHapus,
        #btnAktifItem {
            margin-right: 0;
        }
        table .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Item</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="collapse-peralatan">
                    <form method="POST" action="{{ url('item') }}">
                    {!! csrf_field() !!}
                        <div class="row">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Kode Item</label>
                                            <input class="form-control" type="text" id="kode" name="kode" placeholder="Kode Item" style="height: 38px;" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Nama Item</label>
                                            <input class="form-control" type="text" id="nama" name="nama" placeholder="Nama Item" style="height: 38px" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Nama Pendek</label>
                                            <input class="form-control" type="text" id="nama_pendek" name="nama_pendek" placeholder="Nama Pendek" style="height: 38px" required="">
                                            <span style="color:red" class="sembunyi">Panjang kalimat tidak boleh lebih dari 15 karakter!</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div class="form-group col-sm-12 col-xs-12">
                                        <label class="control-label">Pilih Pemasok</label>
                                        <select id="suplier_id" name="suplier_id" class="select2_single form-control" required="">
                                            <option value="">Pilih Pemasok</option>
                                            @foreach($supliers as $suplier)
                                            <option value="{{ $suplier->id }}">{{$suplier->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Jenis Item</label>
                                            <select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id" required="">
                                                <option value="">Pilih Jenis Item</option>
                                                @foreach($jenis_items as $jenis_item)
                                                <option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Kategori Item</label>
                                            <select class="form-control select2_single" id="konsinyasi" name="konsinyasi" required="">
                                                <option value="">Pilih Kategori Item</option>
                                                <option value="0">Item Biasa</option>
                                                <option value="3">Item Bonus</option>
                                                <option value="1">Item Konsinyasi</option>
                                                <option value="2">Item Paket</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12" style="padding-right: 0; margin-top: 10px;">
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                        <i class="fa fa-save"></i> Tambah
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <!-- Item Biasa -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Item Biasa</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('item/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableItem" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="kode">Kode Item</th>
                                <th class="sorting" field="nama">Nama Item</th>
                                <th class="sorting" field="jenis_item_nama">Jenis Item</th>
                                <th class="sorting" field="stoktotal">Stok</th>
                                <th class="sorting" field="valid_konversi">Validasi Satuan</th>
                                <th class="sorting" field="kode" style="width: 160px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <!-- Item Bonus -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Item Bonus</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('item/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableItemBonus" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="kode">Kode Item</th>
                                <th class="sorting" field="nama">Nama Item</th>
                                <th class="sorting" field="jenis_item_nama">Jenis Item</th>
                                <th class="sorting" field="stoktotal">Stok</th>
                                <th class="sorting" field="valid_konversi">Validasi Satuan</th>
                                <th class="sorting" field="kode" style="width: 160px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <!-- Item Konsinyasi -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Item Konsinyasi</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('item/mdt3') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode_barang">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableKon" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode_barang">No</th>
                                <th class="sorting" field="kode_barang">Kode Barang</th>
                                <th class="sorting" field="nama">Nama Item</th>
                                <th class="sorting" field="jenis_item_nama">Jenis Item</th>
                                <th class="sorting" field="stoktotal">Stok</th>
                                <th class="sorting" field="valid_konversi">Validasi Satuan</th>
                                <th class="sorting" field="kode_barang" style="width: 160px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <!-- Item Bundle -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Item Paket</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('item/mdt4') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode_barang">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableBund" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode_barang">No</th>
                                <th class="sorting" field="kode_barang">Kode Barang</th>
                                <th class="sorting" field="nama">Nama Item</th>
                                <th class="sorting" field="jenis_item_nama">Jenis Item</th>
                                <th class="sorting" field="kode_barang" style="width: 120px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <!-- Item History -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Item</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('item/mdt5') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode_barang">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="offTable" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode_barang">No</th>
                                <th class="sorting" field="kode_barang">Kode Barang</th>
                                <th class="sorting" field="nama">Nama Item</th>
                                <th class="sorting" field="jenis_item_nama">Jenis Item</th>
                                <th class="sorting" field="kode_barang" style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_suplier')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kode berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_suplier')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kode gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                // text: 'Item berhasil dihapus!',
                text: 'Item berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                // text: 'Item gagal dihapus!',
                text: 'Item gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 're_aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diaktifkan kembali!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 're_aktif')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diaktifkan kembali!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('item/mdt1') }}";
        var mdt2 = "{{ url('item/mdt2') }}";
        var mdt3 = "{{ url('item/mdt3') }}";
        var mdt4 = "{{ url('item/mdt4') }}";
        var mdt5 = "{{ url('item/mdt5') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt3) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt4) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="5" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt5) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="5" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                // Item Biasa
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        var buttons = item.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.history_harga_beli != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_beli.url}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.history_harga_jual != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_jual.url}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${item.id}" kode_barang="${item.kode_barang}">
                                    <td>${nomor}</td>
                                    <td>${item.kode}</td>
                                    <td>${item.nama}</td>
                                    <td>${item.jenis_item_nama}</td>
                                    <td>${item.mdt_stok}</td>
                                    <td>${item.mdt_valid_konversi}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
                // Item Bonus
                else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var bonus = data[i];
                        var buttons = bonus.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.history_harga_beli != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_beli.url}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.history_harga_jual != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_jual.url}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${bonus.id}" kode_barang="${bonus.kode_barang}">
                                    <td>${nomor}</td>
                                    <td>${bonus.kode}</td>
                                    <td>${bonus.nama}</td>
                                    <td>${bonus.jenis_item_nama}</td>
                                    <td>${bonus.mdt_stok}</td>
                                    <td>${bonus.mdt_valid_konversi}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
                // Item Konsinyasi
                else if (base_url == mdt3) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var konsinyasi = data[i];
                        var buttons = konsinyasi.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.history_harga_beli != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_beli.url}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.history_harga_jual != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_jual.url}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${konsinyasi.id}" kode_barang="${konsinyasi.kode_barang}">
                                    <td>${nomor}</td>
                                    <td>${konsinyasi.kode_barang}</td>
                                    <td>${konsinyasi.nama}</td>
                                    <td>${konsinyasi.jenis_item_nama}</td>
                                    <td>${konsinyasi.mdt_stok}</td>
                                    <td>${konsinyasi.mdt_valid_konversi}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
                // Item Bundle
                else if (base_url == mdt4) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var bundle = data[i];
                        var buttons = bundle.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        // if (buttons.history_harga_beli != null) {
                        //     td_buttons += `
                        //         <a href="${buttons.history_harga_beli.url}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                        //             <i class="fa fa-history"></i>
                        //         </a>
                        //     `;
                        // }
                        if (buttons.history_harga_jual != null) {
                            td_buttons += `
                                <a href="${buttons.history_harga_jual.url}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${bundle.id}" kode_barang="${bundle.kode_barang}">
                                    <td>${nomor}</td>
                                    <td>${bundle.kode_barang}</td>
                                    <td>${bundle.nama}</td>
                                    <td>${bundle.jenis_item_nama}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
                // Item History
                else if (base_url == mdt5) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        var buttons = item.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        // if (buttons.history_harga_beli != null) {
                        //     td_buttons += `
                        //         <a href="${buttons.history_harga_beli.url}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                        //             <i class="fa fa-history"></i>
                        //         </a>
                        //     `;
                        // }
                        // if (buttons.history_harga_jual != null) {
                        //     td_buttons += `
                        //         <a href="${buttons.history_harga_jual.url}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                        //             <i class="fa fa-history"></i>
                        //         </a>
                        //     `;
                        // }
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.aktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnAktifItem" data-toggle="tooltip" data-placement="top" title="Aktifkan Item">
                                    <i class="fa fa-check"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${item.id}" kode_barang="${item.kode_barang}">
                                    <td>${nomor}</td>
                                    <td>${item.kode_barang}</td>
                                    <td>${item.nama}</td>
                                    <td>${item.jenis_item_nama}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                width: '100%'
            });

            $('#formSimpanContainer').find('#kode').val('').trigger('change');
            $('#formSimpanContainer').find('#nama').val('').trigger('change');
            $('#formSimpanContainer').find('#nama_pendek').val('').trigger('change');
            $('#formSimpanContainer').find('#suplier_id').val('').trigger('change');
            $('#formSimpanContainer').find('#jenis_item_id').val('').trigger('change');
            $('#formSimpanContainer').find('#konsinyasi').val('').trigger('change');
        });

        $(document).on('click', '#btnSimpan', function(event) {
            event.preventDefault();

            var $form = $(this).parents('form').first();
            var item_kode = $('#kode').val();
            var item_nama = $('#nama').val();
            var item_nama_pendek = $('#nama_pendek').val();
            var suplier_id = $('#suplier_id').val();
            var jenis_item_id = $('#jenis_item_id').val();
            var konsinyasi = $('#konsinyasi').val();
            if (item_nama == '' ||
                item_nama_pendek == '' ||
                suplier_id == '' ||
                jenis_item_id == '' ||
                konsinyasi == '') {
                swal({
                    title: 'Waduh!',
                    text: 'Tidak boleh ada data yang kosong!',
                    timer: 3000,
                    type: 'error'
                });
            } else {
                if (item_kode != '') {
                    $form.submit();
                } else {
                    swal({
                        title: 'Anda yakin?',
                        text: 'Item baru ini tidak memiliki kode item!',
                        type: 'question',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: '#26B99A',
                        cancelButtonColor: '#d9534f',
                        confirmButtonText: '<i class="fa fa-check"></i> Yakin',
                        cancelButtonText: '<i class="fa fa-close"></i> Batal'
                    }).then(function() {
                        // console.log(po_penjualan);
                        $form.submit();
                    }).catch(function(data) {
                        console.log(data);
                    });
                }
            }

        });

        /* $(document).on('keyup', '#nama_pendek', function(event) {
            event.preventDefault();

            var nama_pendek = $(this).val();
            if (nama_pendek.length > 15) {
                nama_pendek = nama_pendek.slice(0, -1);
                $(this).val(nama_pendek);
            }
        }); */

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('kode_barang');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama_item + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("item") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktifItem', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('kode_barang');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama_item + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("item") }}' + '/aktif/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('change', '#jenis_item_id', function(event) {
            event.preventDefault();

            var jenis_item_id = $(this).val();
            if (jenis_item_id == 1) {
                $('#konsinyasi').val(3).trigger('change');
                $('#konsinyasi').prop('disabled', true);
            } else {
                $('#konsinyasi').val('').trigger('change');
                $('#konsinyasi').prop('disabled', false);
            }
        });

        $(document).on('keyup', '#nama_pendek', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);
            
            if (text.length > 15) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
