@extends('layouts.admin')

@section('title')
    <title>EPOS | Item</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnHistory, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12" id="formSimpanContainer">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="formSimpanTitle">Tambah Item</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="collapse-peralatan">
                        <form method="POST" action="{{ url('item') }}">
                        {!! csrf_field() !!}
                            <div class="row">
                                <input type="hidden" name="id">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Kode Item</label>
                                                <input class="form-control" type="text" id="kode" name="kode" placeholder="Kode Item" style="height: 38px;" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Nama Item</label>
                                                <input class="form-control" type="text" id="nama" name="nama" placeholder="Nama Item" style="height: 38px" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Nama Pendek</label>
                                                <input class="form-control" type="text" id="nama_pendek" name="nama_pendek" placeholder="Nama Pendek" style="height: 38px" required="">
                                                <span style="color:red" class="sembunyi">Panjang kalimat tidak boleh lebih dari 15 karakter!</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <div class="form-group col-sm-12 col-xs-12">
                                            <label class="control-label">Pilih Pemasok</label>
                                            <select id="suplier_id" name="suplier_id" class="select2_single form-control" required="">
                                                <option value="">Pilih Pemasok</option>
                                                @foreach($supliers as $suplier)
                                                <option value="{{ $suplier->id }}">{{$suplier->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Jenis Item</label>
                                                <select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id" required="">
                                                    <option value="">Pilih Jenis Item</option>
                                                    @foreach($jenis_items as $jenis_item)
                                                    <option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Kategori Item</label>
                                                <select class="form-control select2_single" id="konsinyasi" name="konsinyasi" required="">
                                                    <option value="">Pilih Kategori Item</option>
                                                    <option value="0">Item Biasa</option>
                                                    <option value="3">Item Bonus</option>
                                                    <option value="1">Item Konsinyasi</option>
                                                    <option value="2">Item Paket</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12" style="padding-right: 0; margin-top: 10px;">
                                    <div class="col-md-12 form-group">
                                        <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                            <i class="fa fa-save"></i> Tambah
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Item Biasa</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Item</th>
                                <!-- <th>Kode Item</th> -->
                                <th>Nama Item</th>
                                <th>Jenis Item</th>
                                <th>Stok</th>
                                <th>Validasi Satuan</th>
                                <th style="width: 110px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $num => $item)
                            <tr id="{{ $item->kode }}" kode_barang="{{ $item->kode_barang }}">
                                <td>{{ $num+1 }}</td>
                                <td>{{ $item->kode }}</td>
                                {{-- <td>{{ $item->kode }}</td> --}}
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_item->nama }}</td>
                                <td class="stok">{{ $item->stok }}</td>
                                <td >
                                    @if($item->valid_konversi == 1)
                                        Valid
                                    @else
                                        Belum Valid
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    <a href="{{ url('item/history-harga-beli/'.$item->kode_barang) }}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode_barang) }}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/show/'.$item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <!-- <a href="{{ url('item/edit/'.$item->kode_barang) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_suplier/'.$item->kode_barang) }}" class="btn btn-xs btn-success" id="btnTambahSuplier" data-toggle="tooltip" data-placement="top" title="Tambah Pemasok">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_barcode/'.$item->kode_barang) }}" class="btn btn-xs btn-primary" id="btnBarcode" data-toggle="tooltip" data-placement="top" title="Tambah Barcode">
                                        <i class="fa fa-edit"></i>
                                    </a> -->
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Item Bonus</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemBonus">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Item</th>
                                <!-- <th>Kode Item</th> -->
                                <th>Nama Item</th>
                                <th>Jenis Item</th>
                                <th>Stok</th>
                                <th>Validasi Satuan</th>
                                <th style="width: 110px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item_bonus as $num => $item)
                            <tr id="{{ $item->kode }}" kode_barang="{{ $item->kode_barang }}">
                                <td>{{ $num+1 }}</td>
                                <td>{{ $item->kode }}</td>
                                {{-- <td>{{ $item->kode }}</td> --}}
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_item->nama }}</td>
                                <td class="stok_bonus">{{ $item->stok }}</td>
                                <td >
                                    @if($item->valid_konversi == 1)
                                        Valid
                                    @else
                                        Belum Valid
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    <a href="{{ url('item/history-harga-beli/'.$item->kode_barang) }}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode_barang) }}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/show/'.$item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <!-- <a href="{{ url('item/edit/'.$item->kode_barang) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_suplier/'.$item->kode_barang) }}" class="btn btn-xs btn-success" id="btnTambahSuplier" data-toggle="tooltip" data-placement="top" title="Tambah Pemasok">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_barcode/'.$item->kode_barang) }}" class="btn btn-xs btn-primary" id="btnBarcode" data-toggle="tooltip" data-placement="top" title="Tambah Barcode">
                                        <i class="fa fa-edit"></i>
                                    </a> -->
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Item Konsinyasi</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableKon">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                {{-- <th>Kode Item</th> --}}
                                <th>Nama Item</th>
                                <th>Jenis Item</th>
                                <th>Stok</th>
                                <th>Validasi Satuan</th>
                                <th style="width: 110px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($konsinyasi as $num => $item)
                            <tr id="{{$item->kode}}" kode_barang="{{ $item->kode_barang }}">
                                <td>{{ $num+1 }}</td>
                                <td>{{ $item->kode_barang }}</td>
                                {{-- <td>{{ $item->kode }}</td> --}}
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_item->nama }}</td>
                                <td class="stok-konsinyasi">{{ $item->stok }}</td>
                                <td >
                                    @if($item->isValidKonversi())
                                        Valid
                                    @else
                                        Belum Valid
                                    @endif
                                </td>
                                <td style="text-align: center; width: 100px;">
                                    {{-- <a href="{{ url('item/history-harga-beli/'.$item->kode) }}" class="btn btn-xs btn-default" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Lihat History Harga Beli">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode) }}" class="btn btn-xs btn-primary" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Lihat History Harga Jual">
                                        <i class="fa fa-money"></i>
                                    </a>
                                    <a href="{{ url('item/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ url('item/edit/'.$item->kode) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_suplier/'.$item->kode) }}" class="btn btn-xs btn-success" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Tambah Suplier">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus">
                                        <i class="fa fa-trash"></i>
                                    </button> --}}
                                    {{-- <a href="{{ url('item/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ url('item/edit/'.$item->kode) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus">
                                        <i class="fa fa-trash"></i>
                                    </button> --}}
                                    <a href="{{ url('item/history-harga-beli/'.$item->kode_barang) }}" class="btn btn-xs btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode_barang) }}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/show/'.$item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <!-- <a href="{{ url('item/edit/'.$item->kode_barang) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_suplier/'.$item->kode_barang) }}" class="btn btn-xs btn-success" id="btnTambahSuplier" data-toggle="tooltip" data-placement="top" title="Tambah Pemasok">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_barcode/'.$item->kode_barang) }}" class="btn btn-xs btn-primary" id="btnBarcode" data-toggle="tooltip" data-placement="top" title="Tambah Barcode">
                                        <i class="fa fa-edit"></i>
                                    </a> -->
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {{-- <a href="{{ url('item/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail">
                                        <i class="fa fa-eye"></i> Detail
                                    </a> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Item Paket</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBund">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                {{-- <th>Kode Item</th> --}}
                                <th>Nama Item</th>
                                <th>Jenis Item</th>
                                <!-- <th>Stok</th> -->
                                <th style="width: 75px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bundle as $num => $item)
                            <tr id="{{$item->kode}}" kode_barang="{{$item->kode_barang}}">
                                <td>{{ $num+1 }}</td>
                                <!-- <td>{{ $item->kode }}</td> -->
                                <td>{{ $item->kode_barang }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_item->nama }}</td>
                                <td style="text-align: center;">
                                    <!-- <a href="{{ url('item/history-harga-beli/'.$item->kode) }}" class="btn btn-xs btn-default" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Lihat History Harga Beli">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode) }}" class="btn btn-xs btn-primary" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Lihat History Harga Jual">
                                        <i class="fa fa-money"></i>
                                    </a>
                                    <a href="{{ url('item/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ url('item/edit/'.$item->kode) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ url('item/tambah_suplier/'.$item->kode) }}" class="btn btn-xs btn-success" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Tambah Suplier">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus">
                                        <i class="fa fa-trash"></i>
                                    </button> -->
                                    <a href="{{ url('item/history-harga-jual/'.$item->kode_barang) }}" class="btn btn-xs btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                                        <i class="fa fa-history"></i>
                                    </a>
                                    <a href="{{ url('item-bundle/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    {{-- <a href="{{ url('item-bundle/edit/'.$item->kode) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                        <i class="fa fa-edit"></i>
                                    </a> --}}
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Item">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {{-- <a href="{{ url('item/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail">
                                        <i class="fa fa-eye"></i> Detail
                                    </a> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Riwayat Item</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="offTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                {{-- <th>Kode Item</th> --}}
                                <th>Kode Barang</th>
                                <th>Nama Item</th>
                                <th>Jenis Item</th>
                                <!-- <th>Stok</th> -->
                                <th style="width: 100px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item_off as $num => $item)
                            <tr id="{{$item->kode}}" kode_barang="{{ $item->kode_barang }}">
                                <td>{{ $num+1 }}</td>
                                {{-- <td>{{ $item->kode }}</td> --}}
                                <td>{{ $item->kode }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jenis_item->nama }}</td>
                                <td style="text-align: center;">
                                    @if ($item->konsinyasi == 2) 
                                        <a href="{{ url('item-bundle/show/'.$item->kode) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @else
                                        <a href="{{ url('item/show/'.$item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @endif
                                    <button class="btn btn-xs btn-success" id="btnAktifItem" data-toggle="tooltip" data-placement="top" title="Aktifkan Item">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_suplier')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kode berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_suplier')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kode gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                // text: 'Item berhasil dihapus!',
                text: 'Item berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                // text: 'Item gagal dihapus!',
                text: 'Item gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 're_aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diaktifkan kembali!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 're_aktif')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diaktifkan kembali!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        // $('#tableItem').DataTable();
        // $('#offTable').DataTable();
        // $('#tableKon').DataTable();

        var items = [];
        var konsinyasi = [];
        var item_bonus = [];

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            items = '{{ json_encode($items) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            item_bonus = '{{ json_encode($item_bonus) }}';
            item_bonus = item_bonus.replace(/&quot;/g, '"');
            item_bonus = JSON.parse(item_bonus);

            konsinyasi = '{{ $konsinyasi }}';
            konsinyasi = konsinyasi.replace(/&quot;/g, '"');
            konsinyasi = JSON.parse(konsinyasi);

            $(".select2_single").select2({
                width: '100%'
            });

            $('#formSimpanContainer').find('#kode').val('').trigger('change');
            $('#formSimpanContainer').find('#nama').val('').trigger('change');
            $('#formSimpanContainer').find('#nama_pendek').val('').trigger('change');
            $('#formSimpanContainer').find('#suplier_id').val('').trigger('change');
            $('#formSimpanContainer').find('#jenis_item_id').val('').trigger('change');
            $('#formSimpanContainer').find('#konsinyasi').val('').trigger('change');

            $('.stok').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('id');
                var jumlah = parseFloat($(el).text());
                var item = null;

                for (var i = 0; i < items.length; i++) {
                    if (items[i].kode == item_kode) {
                        item = items[i];
                        // console.log(item, items[i]);
                        break;
                    }
                }

                // if(item == null) console.log(item_kode);

                var satuan_item = [];
                // if(item != null){
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }
                // }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
                // console.log(jumlah);
            });

            $('.stok-konsinyasi').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('id');
                var jumlah = parseFloat($(el).text());
                var item = null;

                for (var i = 0; i < konsinyasi.length; i++) {
                    if (konsinyasi[i].kode == item_kode) {
                        item = konsinyasi[i];
                        break;
                    }
                }

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
            });

            $('.stok_bonus').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('id');
                var jumlah = parseFloat($(el).text());
                var item = null;

                for (var i = 0; i < item_bonus.length; i++) {
                    if (item_bonus[i].kode == item_kode) {
                        item = item_bonus[i];
                        break;
                    }
                }

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
            });

            // $('#tableItem').DataTable();
            $('#tableItemBonus').DataTable();
            $('#offTable').DataTable();
            $('#tableKon').DataTable();
            $('#tableBund').DataTable();
        });

        $(document).on('click', '#btnSimpan', function(event) {
            event.preventDefault();

            var $form = $(this).parents('form').first();
            var item_kode = $('#kode').val();
            var item_nama = $('#nama').val();
            var item_nama_pendek = $('#nama_pendek').val();
            var suplier_id = $('#suplier_id').val();
            var jenis_item_id = $('#jenis_item_id').val();
            var konsinyasi = $('#konsinyasi').val();
            if (item_nama == '' ||
                item_nama_pendek == '' ||
                suplier_id == '' ||
                jenis_item_id == '' ||
                konsinyasi == '') {
                swal({
                    title: 'Waduh!',
                    text: 'Tidak boleh ada data yang kosong!',
                    timer: 3000,
                    type: 'error'
                });
            } else {
                if (item_kode != '') {
                    $form.submit();
                } else {
                    swal({
                        title: 'Anda yakin?',
                        text: 'Item baru ini tidak memiliki kode item!',
                        type: 'question',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: '#26B99A',
                        cancelButtonColor: '#d9534f',
                        confirmButtonText: '<i class="fa fa-check"></i> Yakin',
                        cancelButtonText: '<i class="fa fa-close"></i> Batal'
                    }).then(function() {
                        // console.log(po_penjualan);
                        $form.submit();
                    }).catch(function(data) {
                        console.log(data);
                    });
                }
            }

        });

        /* $(document).on('keyup', '#nama_pendek', function(event) {
            event.preventDefault();

            var nama_pendek = $(this).val();
            if (nama_pendek.length > 15) {
                nama_pendek = nama_pendek.slice(0, -1);
                $(this).val(nama_pendek);
            }
        }); */

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('kode_barang');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama_item + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("item") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktifItem', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('kode_barang');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama_item + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("item") }}' + '/aktif/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('change', '#jenis_item_id', function(event) {
            event.preventDefault();

            var jenis_item_id = $(this).val();
            if (jenis_item_id == 1) {
                $('#konsinyasi').val(3).trigger('change');
                $('#konsinyasi').prop('disabled', true);
            } else {
                $('#konsinyasi').val('').trigger('change');
                $('#konsinyasi').prop('disabled', false);
            }
        });

        $(document).on('keyup', '#nama_pendek', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);
            
            if (text.length > 15) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
