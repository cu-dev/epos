@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Item {{ $item->nama }}</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <div class="row">
                    <div class="col-md-9">
                        <p id="formSimpanTitle" class="titleDetailItem">
                            <span><span>Ubah Item </span><span class="nama-item">{{ $item->nama }}</span></span>
                        </p>
                        <!-- <p id="formSimpanTitle" class="titleDetailItem">Ubah Item <span>{{ $item->nama }}</span></p> -->
                    </div>
                    <div class="col-md-3">
                        <a href="{{ URL::previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('item/'.$item->kode_barang.'/ubah') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="put">
                    {{-- <input type="hidden" name="konsinyasi" value="0"> --}}
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">
                                    Kode Item
                                </label>
                                <input class="form-control" type="text" id="kode" name="kode" value="{{$item->kode}}" disabled="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">
                                    Nama Item
                                </label>
                                <input class="form-control" type="text" id="nama" name="nama" value="{{$item->nama}}" required="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">
                                    Nama Pendek
                                </label>
                                <input class="form-control" type="text" id="nama_pendek" name="nama_pendek" value="{{$item->nama_pendek}}" required="">
                                <span style="color:red" class="sembunyi">Panjang kalimat tidak boleh lebih dari 15 karakter!</span>
                            </div>
                        </div>
                        <!-- <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Nama Suplier</label>
                                <select class="form-control select2_single" id="suplier_id" name="suplier_id">
                                    @foreach($supliers as $suplier)
                                    @if($item->suplier_id == $suplier->id)
                                        <option value="{{$suplier->id}}" selected>{{ $suplier->nama }}</option>
                                    @else
                                        <option value="{{$suplier->id}}">{{ $suplier->nama }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div> -->
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Jenis Item</label>
                                <select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id" required="">
                                    <option value="">Pilih Jenis Item</option>
                                    @foreach($jenis_items as $jenis_item)
                                        @if($item->jenis_item_id == $jenis_item->id)
                                        <option value="{{$jenis_item->id}}" selected>{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
                                        @else
                                        <option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12"  id="kategoriItemForm">
                            <div class="form-group">
                                <label class="control-label">Kategori Item</label>
                                <select class="form-control select2_single" id="konsinyasis" name="konsinyasis" required="">
                                    <option value="">Pilih Kategori Item</option>
                                    <option value="0">Item Biasa</option>
                                    <option value="3">Item Bonus</option>
                                </select>
                            </div>
                        </div>
                        <div class="hidden">
                            <select class="form-control select2_single sembunyi" id="konsinyasi" name="konsinyasi" required="">
                                    <option value="0">Item Biasa</option>
                                    <option value="3">Item Bonus</option>
                                    <option value="1" style="display: none">Item Konsinyasi</option>
                                    <option value="2" style="display: none">Item Paket</option>
                                </select>
                        </div>
                        <!-- <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Kategori Item</label>
                                <select class="form-control select2_single" id="konsinyasi" name="konsinyasi">
                                    <option value="">Pilih kategori item</option>
                                    <option value="0" selected="">Item Biasa</option>
                                    <option value="1">Item Konsinyasi</option>
                                </select>
                            </div>
                        </div> -->
                        @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                            {{-- <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Rentang Penyesuaian Harga Jual dari Pembelian (Rp)
                                    </label>
                                    <input class="form-control angka" type="text" id="rentanID" name="rentang_" value="{{ \App\Util::angka($item->rentang) }}" required="">
                                    <input class="form-control" type="hidden" id="profit" name="rentang" value="{{ $item->rentang }}" required="">
                                </div>
                            </div> --}}
                            <div class="col-md-12 col-xs-12" id="bonusJualForm">
                                <div class="form-group">
                                    <label class="control-label">Status Item Terikat</label>
                                    <select class="form-control select2_single" id="bonus_jual" name="bonus_jual">
                                        <option value="">Status Item Terikat</option>
                                        <option value="1">Dijual Sebelum Stok Item Terikat Habis</option>
                                        <option value="0">Tidak Dijual Sebelum Stok Item Terikat Habis</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Status Retur</label>
                                    <select class="form-control select2_single" id="retur" name="retur" required="">
                                        <option value="">Pilih Status Retur</option>
                                        <option value="1">Boleh Retur</option>
                                        <option value="0">Tidak Boleh Retur</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 konsinyasiForm">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label">
                                                Keuntungan yang Diharapkan (E) (%) | (Rp)
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input class="form-control col-md-6" type="text" id="profit_eceran" name="profit_eceran" value="" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control angka" type="text" id="plus_grosir_" name="plus_grosir_" value="{{ \App\Util::angka($item->plus_grosir) }}" required="">
                                            <input class="form-control" type="hidden" id="plus_grosir" name="plus_grosir" value="{{ $item->plus_grosir}}" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 konsinyasiForm">
                                <div class="form-group">
                                    <label class="control-label">
                                        Keuntungan yang Diharapkan (G) (%)
                                    </label>
                                    <input class="form-control" type="text" id="profit" name="profit" value="" required="">
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Rentang Batas Perubahan Harga (Rp)
                                    </label>
                                    <input class="form-control angka" type="text" id="rentanID" name="rentang_" value="{{ \App\Util::angka($item->rentang) }}" required="">
                                    <input class="form-control" type="hidden" id="profit" name="rentang" value="{{ $item->rentang }}" required="">
                                </div>
                            </div>
                        @endif
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-success" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> Ubah
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function() {
            var url = "{{url('item')}}";
            var a = $('a[href="' + url + '"]');
            // console.log(a.text());
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2();

            var retur = '{{ $item->retur }}';
            $('#retur').val(retur).trigger('change');

            var bonus_jual = '{{ $item->bonus_jual }}';
            $('#bonus_jual').val(bonus_jual).trigger('change');

            var profit = '{{ $item->profit }}';
            profit = parseFloat(profit);
            $('#profit').val(profit.toLocaleString());

            var profit_eceran = '{{ $item->profit_eceran }}';
            profit_eceran = parseFloat(profit_eceran);
            $('#profit_eceran').val(profit_eceran.toLocaleString());

            var kategori = '{{ $item->konsinyasi }}';
            $('#konsinyasi').val(kategori).change();
            $('#konsinyasis').val(kategori).change();
            if (kategori != 0 && kategori != 3) {
                $('#kategoriItemForm').hide();
            }

            if(kategori != 3) $('#bonusJualForm').hide();

            if (kategori == 1) $('.konsinyasiForm').hide();
        });

        // $(document).on('keyup', '#nama_pendek', function(event) {
        //     event.preventDefault();

        //     var nama_pendek = $(this).val();
        //     if (nama_pendek.length > 15) {
        //         nama_pendek = nama_pendek.slice(0, -1);
        //         $(this).val(nama_pendek);
        //     }
        // });

        $(document).on('keyup', '#nama_pendek', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);
            
            if (text.length > 15) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }

        $(document).on('change', '#konsinyasis', function(event) {
            event.preventDefault();
            var val = $(this).val();

            if(val != '') {
                $('#konsinyasi').val(val).change();
            }
        });

        $(document).on('keyup', '#profit_eceran', function(event) {
            event.preventDefault();

            var text = $(this).val();
            if (text.indexOf(',') != -1) {
                var x = text.split(',');
                if (x.length > 0 && x[1].length > 2) {
                    var after = x[0]+','+x[1].substring(0, 2);
                    $(this).val(after);
                }
            } else if (text.indexOf('.') != -1) {
                var x = text.split('.');
                if (x.length > 0 && x[1].length > 2) {
                    var after = x[0]+'.'+x[1].substring(0, 2);
                    $(this).val(after);
                }
            }
        });

        $(document).on('keyup', '#profit', function(event) {
            event.preventDefault();

            var text = $(this).val();
            if (text.indexOf(',') != -1) {
                var x = text.split(',');
                if (x.length > 0 && x[1].length > 2) {
                    var after = x[0]+','+x[1].substring(0, 2);
                    $(this).val(after);
                }
            } else if (text.indexOf('.') != -1) {
                var x = text.split('.');
                if (x.length > 0 && x[1].length > 2) {
                    var after = x[0]+'.'+x[1].substring(0, 2);
                    $(this).val(after);
                }
            }
        });
    </script>
@endsection
