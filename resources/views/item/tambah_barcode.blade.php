@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Kode Item</title>
@endsection

@section('style')
    <style media="screen">
        .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
        #btnKembali,
        .btnSimpan {
            margin: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-9">
                        <p id="formSimpanTitle" class="titleDetailItem">
                            <span><span>Daftar Kode Item </span><span class="nama-item">{{ $item->nama }}</span></span>
                        </p>
                        <!-- <p id="formSimpanTitle" class="titleDetailItem">Datfar Kode Item <span>{{ $item->nama }}</span></p> -->
                    </div>
                    <div class="col-md-3">
                        <a href="{{url('item/show/'.$item->kode_barang)}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <form class="form-horizontal" action="{{ url('item/tambah_barcode/'.$item->kode_barang) }}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                        <div class="col-md-9 col-xs-9">
                            <div class="form-group">
                                <input class="form-control" type="text" id="kode" name="kode" style="height: 38px" placeholder="Kode Item" required="" autofocus="">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="form-group">
                                <button class="btn btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <th>No</th>
                                <th>Kode Item</th>
                            </thead>
                            <tbody>
                                @foreach($items as $i => $item)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $item->kode }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kode item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kode item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableHarga').DataTable();
        $('#StokTable').DataTable();

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2();
        });

    </script>
@endsection
