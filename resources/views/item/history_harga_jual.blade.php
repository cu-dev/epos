@extends('layouts.admin')

@section('title')
	<title>EPOS | Riwayat Harga Jual</title>
@endsection

@section('style')
	<style media="screen">
    	#btnDetail, #btnUbah, #btnHapus, #btnKembali {
    		margin-bottom: 0;
    	}
        #btnKembali {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        .link {
        	text-decoration: underline;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Riwayat Harga Jual <b>{{ $item->nama }}</b></h2>
				<a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover" id="tabel-history">
						<thead>
							<tr>
								<th width="5%">No.</th>
								<th>Tanggal & Waktu</th>
								<th>Kode Transaksi</th>
								<th>Jumlah</th>
								<th>Harga</th>
								<th>Pelanggan</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($prices as $i => $relasi)
							<tr>
								<td>{{ ++$i }}</td>
								<td>{{ $relasi['waktu'] }}</td>
								<td class="link">
									<a href="{{ url('transaksi-grosir/'.$relasi['id_transaksi']) }}" >
										{{ $relasi['kode_transaksi'] }}
									</a>
								</td>
								<td class="stok">{{ $relasi['jumlah'] }}</td>
                            	<td style="text-align: right;">
									@foreach($satuans as $i => $satuan)
									<!-- <div class="row">
										<div class="form-group col-sm-12">
											<div class="input-group">
												<input type="text" class="form-control input-sm" readonly="readonly" value="{{ \App\Util::duit($satuan->konversi*$relasi['harga']) }}" />
												<div class="input-group-addon">per {{ $satuan->satuan->kode }}</div>
											</div>
										</div>
									</div> -->
                                	{{ \App\Util::duit0($satuan->konversi*$relasi['harga']) }}/{{$satuan->satuan->kode }}
                                	<br>
									@endforeach
								</td>
								<td>{{ $relasi['pelanggan'] }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
        var item = [];

		$(document).ready(function() {
			var url = "{{ url('item') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			item = '{{ json_encode($item) }}';
            item = item.replace(/&quot;/g, '"');
            item = JSON.parse(item);

            $('.stok').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('id');
                var jumlah = parseFloat($(el).text());

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
            });

			$('#tabel-history').DataTable();
		});
	
	</script>
@endsection
