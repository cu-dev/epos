@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Item {{ $item->nama }}</title>
@endsection

@section('style')
    <style media="screen">
        .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .dataTables_filter input {
            max-width: 150px;
        }
        #btnUbah, #btnKembali, #btnResetHarga {
            margin-bottom: 0;
        }
        td span {
            line-height: 1.42857143;
        }

        #satuan_harga {
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            background-color: #fff;
            background-image: none;
            background: #fff;
        }
    </style>
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <!-- Detail Item -->
        <div class="col-md-6" id="formSimpanContainer">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11">
                            <p id="formSimpanTitle" class="titleDetailItem">
                                <span><span>Detail Item </span><span class="nama-item">{{ $item->nama }}</span></span>
                            </p>
                        </div>
                        <div class="col-md-1">
                            <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                <div class="pull-right">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Kode Barang</th>
                                        <td id="item_kode">{{ $item->kode_barang }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Kode Item</th>
                                        <td id="item_kode">
                                            @foreach ($item_kodes as $i => $item_kode )
                                            <span class="label label-info">{{ $item_kode->kode }}</span> 
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Nama Item</th>
                                        <td>{{ $item->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Nama Pendek</th>
                                        <td>{{ $item->nama_pendek }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Pemasok</th>
                                        <td>
                                            {{-- @foreach ($items as $i => $item )
                                            <span class="label label-info">{{ $item->suplier->nama }}</span> 
                                            @endforeach --}}
                                            @foreach ($supliers as $i => $suplier )
                                            <span class="label label-info">{{ $suplier->nama }}</span> 
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="50%">Jenis Item</th>
                                        <td>{{ $item->jenis_item->nama}}</td>
                                    </tr>
                                    <tr>
                                        <th width="50%">Kategori Item</th>
                                        @if ($item->konsinyasi == 0) <td> Item Biasa </td>
                                        @elseif ($item->konsinyasi == 3) <td> Item Bonus </td>
                                        @elseif ($item->konsinyasi == 1) <td> Item Konsinyasi </td>
                                        @elseif ($item->konsinyasi == 2) <td> Item Paket </td>
                                        @endif
                                    </tr>
                                    @if ($item->konsinyasi == 3)
                                        <tr>
                                            <th width="50%">Status Item Terikat</th>
                                            @if ($item->bonus_jual == 1)
                                                <td>Dijual Sebelum Stok Item Terikat Habis</td>
                                            @else
                                                <td>Tidak Dijual Sebelum Stok Item Terikat Habis</td>
                                            @endif
                                        </tr>
                                    @endif
                                    <tr>
                                        <th width="50%">Status Retur</th>
                                        @if ($item->retur == 1)
                                            <td>Boleh Retur</td>
                                        @else
                                            <td>Tidak Boleh Retur</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Stok</th>
                                        <!-- <td>{{ $stok->stok }} PCS</td> -->
                                        <td>
                                            @for ($i = 0; $i < count($stok_total); $i++)
                                                <span class="label label-info stok_detail">{{ $stok_total[$i]['jumlah'] }} {{ $stok_total[$i]['satuan'] }}</span>
                                            @endfor
                                        </td>
                                    </tr>
                                    @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                        @if ($item->konsinyasi != 1)
                                            <tr>
                                                <th width="50%">Keuntungan yang Diharapkan (E)</th>
                                                <td id="profit" class="profit_eceran">{{ \App\Util::angka_koma($item->profit_eceran) }}% | {{ \App\Util::duit0($item->plus_grosir) }} (Grosir)</td>
                                            </tr>
                                            <tr>
                                                <th width="50%">Keuntungan yang Diharapkan (G)</th>
                                                <td id="profit" class="profit_grosir">{{ $item->profit }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th width="50%">Rentang Batas Perubahan Harga</th>
                                            <td>{{ App\Util::duit0($item->rentang) }}</td>
                                        </tr>
                                        @if ($item->user_id != null)
                                            <tr>
                                                <th width="50%">Operator Perubahan Data Item</th>
                                                <td>{{ $item->users->nama }}</td>
                                            </tr>
                                        @endif
                                        @if ($item->valid_id != null)
                                            <tr>
                                                <th width="50%">Operator Validasi Satuan</th>
                                                <td>{{ $item->valids->nama }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="formHapusItemKode" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="hapus_item_kode" value="">
                                </form>
                            </div>
                            <hr>
                            <table class="table table-bordered table-striped table-hover" id="BarcodeTable">
                                <thead>
                                    <tr>
                                        <th class="sembunyi"></th>
                                        <th class="text-center">Kode</th>
                                        <th class="text-center" style="width: 25px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($item_kodes as $i => $item_kode)
                                    <tr id="{{ $item_kode->kode }}" var="{{ $a = $i + 1 }}">
                                        <td class="sembunyi">{{ $a }}</td>
                                        <td>{{ $item_kode->kode }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-danger btnHapus" id="hapus_item_kode" data-toggle="tooltip" data-placement="top" title="Hapus Barcode">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <table class="table table-bordered table-striped table-hover" id="PemasokTable">
                                <thead>
                                    <tr>
                                        <th class="sembunyi"></th>
                                        <th class="text-center">Pemasok</th>
                                        <th class="text-center" style="width: 25px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($supliers as $i => $suplier )
                                    <tr id="{{ $suplier->id }}" var="{{ $a = $i + 1 }}">
                                        <td class="sembunyi">{{ $a }}</td>
                                        <td>{{ $suplier->nama }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-danger btnHapus" id="hapus_suplier" data-toggle="tooltip" data-placement="top" title="Hapus Pemasok">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div id="formHapusPemasok" style="display: none;">
            <form method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="post">
                <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                <input type="hidden" name="suplier_id" value="">
            </form>
        </div>

        <!-- Stok Item -->
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Stok</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <a href="{{ url('item/tambah_suplier/'.$item->kode_barang) }}" class="btn btn-sm btn-pemasok pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Tambah Pemasok">
                        <i class="fa fa-user"></i>
                    </a>
                    <a href="{{ url('item/tambah_barcode/'.$item->kode_barang) }}" class="btn btn-sm btn-barcode pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Tambah Barcode">
                        <i class="fa fa-barcode"></i>
                    </a>
                    <a href="{{ url('item/edit/'.$item->kode_barang) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Item">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- <div class="table-responsive"> -->
                    <table class="table table-bordered table-striped table-hover" id="StokTable">
                        <thead>
                            <tr>
                                <th class="sembunyi"></th>
                                <th class="text-center">Tanggal Masuk</th>
                                <th class="text-center">Pemasok</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Harga Beli</th>
                                <th class="text-center">Tanggal Kadaluarsa</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stoks as $i => $stok)
                            <tr id="{{ $stok->id }}" var="{{ $a = $i + 1 }}">
                                <td class="sembunyi">{{ $a }}</td>
                                <td>{{ $stok->created_at->format("d-m-Y") }}</td>
                                <td>{{ $stok->transaksi_pembelian->suplier->nama }}</td>
                                <td style="text-align:center;"><!-- {{$stok->jumlah}} -->
                                    @for ($j = 0; $j < count($stok_pcs[$i]); $j++)
                                    <span class="stok">{{ $stok_pcs[$i][$j]['jumlah'] }} {{ $stok_pcs[$i][$j]['satuan'] }}</span><br>
                                    @endfor
                                </td>
                                <td style="text-align: right;"> 
                                    @for ($j = 0; $j < count($harga_satuan[$i]); $j++)
                                        {{ \App\Util::duit0($harga_satuan[$i][$j]['harga'] * 1.1) }}/{{$harga_satuan[$i][$j]['satuan'] }}
                                        <br>
                                    @endfor
                                </td>
                                <td class="tanggal">{{ $stok->kadaluarsa }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <!-- Satuan Item -->
    {{-- <div class="col-md-6" id="formSimpanContainer"> --}}
        <div class="col-md-6" id="satuanContainer">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="formSimpanTitle">Satuan Item</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="formSimpanSatuan">
                                <form class="form-horizontal" action="{{url('/relasi_satuan')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                                    <div class="row">
                                        <div class="form-group col-xs-5">
                                            <select name="satuanID" class="form-control select2_single" id="satuan_relasi" required="">
                                                <option value="">Pilih Satuan</option>
                                                @foreach ($satuans as $satuan)
                                                <option value="{{ $satuan->id }}">{{ $satuan->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <input type="text" name="konversi" class="form-control" placeholder="Konversi" required="" style="height: 38px">
                                        </div>
                                        <div class="form-group col-xs-3">
                                            <button class="btn btn-sm btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit" style="margin-right: 0; width: 100%; height: 34px;">
                                                <i class="fa fa-save"></i> <span>Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div id="formHapusSatuan" style="display: none;">
                                    <form method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="_method" value="delete">
                                    </form>
                                </div>
                                <table class="table table-bordered table-striped table-hover" id="tableSatuan">
                                    <thead>
                                        <tr>
                                            <th class="text-left hidden">#</th>
                                            <th class="text-left">Jenis Satuan</th>
                                            <th class="text-left">Konversi</th>
                                            <th class="text-left" style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($relasi_satuans as $i => $relasi_satuan)
                                        <tr id="{{ $relasi_satuan->id }}">
                                            <th class="text-left hidden">{{$i}}</th>
                                            <td data-satu="{{ $relasi_satuan->satuan_id }}">{{ $relasi_satuan->satuan->nama }}</td>
                                            <td class="duit">{{ $relasi_satuan->konversi }}</td>
                                            <td class="text-center">
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_relasi_satuan" data-toggle="tooltip" data-placement="top" title="Ubah Satuan Item">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_relasi_satuan" data-toggle="tooltip" data-placement="top" title="Hapus Satuan Item">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <form id="formFixKonversiSatuan" method="post" action="{{ url('item/'.$item->kode.'/fix-konversi-satuan') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                        </form>
                        @if (!$item->isValidKonversi())
                            @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                <div class="col-xs-6">
                                    <button id="aktivasiValidasiSatuan" class="btn btn-success btn-block" disabled="" item_kode="{{ $item->kode_barang }}">
                                        Aktifkan Validasi Satuan Item
                                    </button>
                                </div>
                            @endif
                            <div class="col-xs-6">
                                <button id="fixKonversiSatuan" class="btn btn-success btn-block">
                                    Validasi Satuan Item
                                </button>
                            </div>
                        @else
                            @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                <div class="col-xs-6">
                                    <button id="aktivasiValidasiSatuan" class="btn btn-success btn-block" disabled="" item_kode="{{ $item->kode_barang }}">
                                        Aktifkan Validasi Satuan Item
                                    </button>
                                </div>
                            @endif
                            <div class="col-xs-6 pull-right">
                                <button id="fixKonversiSatuan" class="btn btn-success btn-block" disabled="">
                                    Validasi Satuan Item
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Harga Item -->
        @if((Auth::user()->level_id == 1 || Auth::user()->level_id == 2) || (Auth::user()->level_id == 5 && $item->konsinyasi == 1))
            <div class="col-md-6" id="formSimpanHargaContainer">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Harga Jual</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <a class="btn btn-sm btn-default pull-right" id="btnResetHarga" data-toggle="tooltip" data-placement="top" title="Reset Harga Jual">
                            <i class="fa fa-undo"></i>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{url('/harga')}}" method="post" id="formHargaJual">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                            <div class="row">
                                <div class="form-group col-xs-6" id="formEceran">
                                    <label class="label-control">Harga Eceran</label>
                                    <input type="text" name="eceran_" class="form-control angka" placeholder="Harga Eceran">
                                    <input type="hidden" name="eceran" required="">
                                    <span id="eceran_error" style="color: red"></span>
                                </div>
                                <div class="form-group col-xs-6" id="formGrosiran">
                                    <label class="label-control">Harga Grosir</label>
                                    <input type="text" name="grosir_" class="form-control angka" placeholder="Harga Grosir">
                                    <input type="hidden" name="grosir" required="">
                                    <span id="grosir_error" style="color: red"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <select name="satuan_ids" class="form-control select2_single" id="satuan_harga" multiple required="">
                                        <option value="">Satuan</option>
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <option value="{{ $relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama }}</option>
                                        @endforeach
                                    </select>
                                    <select name="satuan_id" class="form-control hidden" id="satuan_hargas" required="" readonly="">
                                        {{-- <option value="">Satuan</option> --}}
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <option value="{{ $relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-6">
                                    <button class="btn btn-success" id="btnSimpanHarga" type="submit" style="width: 100%; height: 34px;" disabled="">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div id="formHapusContainer" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                            </form>
                        </div>
                        <div class="line"></div>

                        <table class="table table-bordered table-striped table-hover" id="tableHarga">
                            <thead>
                                <tr>
                                    <th style="display: none;">#</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Harga Eceran</th>
                                    <th class="text-center">Harga Grosir</th>
                                    {{-- <th class="text-center">Nego</th> --}}
                                    <th class="text-center" style="width: 25px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($hargas as $i => $harga)
                                    <tr id="{{ $harga->id }}">
                                        <td style="display: none;">{{ $i }}</td>
                                        <td name="satuan_id" id="{{ $harga->satuan_id }}"> 1 {{ $harga->satuan->nama }}</td>
                                        
                                        @if ($item->konsinyasi != 3)
                                            @if ($nego[$i]['eceran'])
                                                <td class="text-right" style="color: orange">{{ \App\Util::ewon($harga->eceran) }}</td>
                                            @else
                                                <td class="text-right">{{ \App\Util::ewon($harga->eceran) }}</td>
                                            @endif

                                            @if ($nego[$i]['grosir'])
                                                <td class="text-right" style="color: green">{{ \App\Util::angka2($nego[$i]['persen']) }}% | {{ \App\Util::ewon($harga->grosir) }}</td>
                                            @else
                                                <td class="text-right">{{ \App\Util::ewon($harga->grosir) }}</td>
                                            @endif
                                        @else
                                            <td class="text-right">{{ \App\Util::ewon($harga->eceran) }}</td>
                                            <td class="text-right">{{ \App\Util::ewon($harga->grosir) }}</td>
                                        @endif
                                        {{-- @if ($nego[$i]['persen']) 
                                            <td>{{ \App\Util::angka2($nego[$i]['persen']) }}%</td>
                                        @else 
                                            <td>-</td>
                                        @endif --}}
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-warning btnUbah" id="UbahHarga" data-toggle="tooltip" data-placement="top" title="Ubah Harga Jual">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <table>
                            <tr>
                                <th colspan="2">Keterangan</th>
                            </tr>
                            <tr>
                                <td style="padding-right: 10px">Rp0</td>
                                {{-- <td>:</td> --}}
                                <td>: Tidak menjual satuan item tersebut</td>
                            </tr>
                            <tr>
                                <td style="text-align: center">#</td>
                                {{-- <td>:</td> --}}
                                <td>: Ubah harga eceran dulu, baru ubah harga grosir</td>
                            </tr>
                            <tr>
                                <td style="color: orange; text-align: center"><i class="fa fa-circle"></i></td>
                                {{-- <td>:</td> --}}
                                <td>: Harga eceran lebih dari keuntungan yang diharapkan</td>
                            </tr>
                            <tr>
                                <td style="color: green; text-align: center"><i class="fa fa-circle"></i></td>
                                {{-- <td>:</td> --}}
                                <td>: Harga grosir lebih dari keuntungan yang diharapkan (bisa dinego)</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
    
    @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
        <div class="row">
            <!-- Batas Minimal Item -->
            <div class="col-md-6" id="limitStokContainer">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Batas Minimal Stok</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form action="{{ url('/item/limit-stok') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <input type="text" name="jumlah" class="form-control select2_tinggi" placeholder="Jumlah" required="">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <select name="satuan_id_limit" class="form-control select2_single" id="satuan_bonus" required="">
                                            <option value="">Pilih Satuan</option>
                                            @foreach ($relasi_satuans as $relasi_satuan)
                                            <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Batas Minimal Stok</h3>
                                        @if($limit_stok == 0 || $limit_stok == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div class="count text-center">{{ $limit_stok['jumlah'] }} {{ $limit_stok['satuan'] }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Batas Grosir Item -->
            <div class="col-md-6" id="limitGrosirContainer">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Batas Minimal Grosir</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form action="{{ url('/item/limit-grosir') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <input type="text" name="jumlah" class="form-control select2_tinggi" placeholder="Jumlah" required="">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <select name="satuan_id_limit" class="form-control select2_single" id="satuan_bonus" required="">
                                            <option value="">Pilih Satuan</option>
                                            @foreach ($relasi_satuans as $relasi_satuan)
                                            <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Batas Minimal Grosir</h3>
                                        @if($limit_grosir == 0 || $limit_grosir == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div class="count text-center">{{ $limit_grosir['jumlah'] }} {{ $limit_grosir['satuan'] }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <!-- Diskon Item -->
            <div class="col-md-6" id="diskonContainer">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Diskon</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form class="form-horizontal" action="{{url('/item/diskon')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                                <div class="row">
                                    <div class="form-group col-xs-6" id="diskonPersenForm">
                                        <div class="input-group">
                                            <input type="text" id="diskon_persen" name="persen" class="form-control" placeholder="Diskon Total" required="">
                                            <div class="input-group-addon">%</div>
                                        </div>
                                        <span style="color: red" id="diskonInfo">Diskon terlalu besar</span>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <button class="btn btn-success" id="btnSimpanDiskon" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white;  padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Diskon</h3>
                                        @if($item->diskon == 0 || $item->diskon == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div id="diskon" class="diskon count text-center">{{ $item->diskon }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div id="formHapusContainer" style="display: none;">
                                    <form method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="_method" value="post">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Bonus Item -->
            @if($item->konsinyasi != 3)
                <div class="col-md-6" id="bonusContainer">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bonus</h2>
                            <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                <div class="pull-right">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </div>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="formBonusContainer">
                                <form action="{{ url('/item/bonus/create') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <input type="text" name="syarat" class="form-control select2_tinggi" placeholder="Jumlah Minimal Pembelian" required="">
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <select name="satuan_id" class="form-control select2_single" id="satuan_bonus" required="">
                                                <option value="">Pilih Satuan</option>
                                                @foreach ($relasi_satuans as $relasi_satuan)
                                                <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <select name="bonus" class="form-control select2_single" id="bonus" required="">
                                                <option value="">Pilih Bonus</option>
                                                @foreach ($bonuses as $bonus)
                                                <option value="{{ $bonus->id }}">{{ $bonus->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <button class="btn btn-success" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                                <i class="fa fa-save"></i> <span>Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-bordered table-striped table-hover" id="tableBonus">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Item Bonus</th>
                                            <th class="text-left">Syarat</th>
                                            <th class="text-left" style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($relasi_bonuses as $x => $relasi_bonus)
                                            <tr id="{{ $relasi_bonus->id }}">
                                                <td id="{{ $relasi_bonus->bonus_id }}">{{ $relasi_bonus->bonus->nama }}</td>
                                                <td id="{{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }}" class="{{ $satuan_bonuses[$x]['satuan']->id }}">
                                                    {{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }} {{ $satuan_bonuses[$x]['satuan']->kode }}
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-warning btnUbah" id="UbahRB" data-toggle="tooltip" data-placement="top" title="Ubah Bonus">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button class="btn btn-xs btn-danger btnHapus" id="hapusRB" data-toggle="tooltip" data-placement="top" title="Hapus Bonus">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div id="formHapusBonus" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    @endif
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_satuan')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Satuan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_satuan')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Satuan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kode item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_kode')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kode item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah_harga')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga Jual berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah_item')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah_item')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Satuan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Satuan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_diskon')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Diskon berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_stok')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Stok Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_grosir')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Grosir Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'delete_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'reset_harga')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga Jual berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus_suplier')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pemasok berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'fix_konversi_satuan')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Konversi satuan untuk item ini sudah benar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableSatuan').DataTable();
        $('#tableHarga').DataTable();
        $('#tableBonus').DataTable();
        $('#StokTable').DataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true
        });
        $('#BarcodeTable').DataTable();
        $('#PemasokTable').DataTable();

        var item = null;
        var disable_form_satuan = false;
        var supliers = null;
        var satuan_terbesar = null;
        var satuan_terkecil = null;
        var relasi_satuans = null;
        var hargas = null;
        var satuan_harga = 0;
        var is_terbesar = false;
        var is_terkecil = false;
        var harga = 0;
        var harga_eceran = 0;
        var harga_grosir = 0;
        var in_grosir = null;
        // var stok_acuan = 0;

        function handleEditKonversiSatuan() {
            // console.log('handleEditKonversiSatuan');
            var satuan_id = $('#formSimpanSatuan #satuan_relasi').val();
            var konversi = $('#formSimpanSatuan input[name="konversi"]').val();
            // console.log(satuan_id, konversi);

            if (disable_form_satuan || item.valid_konversi == 1 || satuan_id != '' || konversi != '') {
                $('#satuanContainer').find('#fixKonversiSatuan').prop('disabled', true);
            } else {
                $('#satuanContainer').find('#fixKonversiSatuan').prop('disabled', false);
            }
        }

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
            $('#diskonInfo').hide();

            item = '{{ json_encode($item) }}';
            item = item.replace(/&quot;/g, '"');
            item = JSON.parse(item);

            hargas = '{{ json_encode($hargas) }}';
            hargas = hargas.replace(/&quot;/g, '"');
            hargas = JSON.parse(hargas);

            var length_stok = '{{ sizeof($stoks) }}';
            if (length_stok > 0) {
                harga = '{{ json_encode($stoks[0]->harga) }}';
                harga = harga.replace(/&quot;/g, '"');
                harga = JSON.parse(harga);
            }

            in_grosir = '{{ json_encode($in_grosir) }}';
            in_grosir = in_grosir.replace(/&quot;/g, '"');
            in_grosir = JSON.parse(in_grosir);

            // harga_eceran = Math.round(parseFloat(harga) * 1.1 * ( 1 + item.profit_eceran / 100) / 100) * 100;
            // harga_grosir = Math.round(parseFloat(harga) * 1.1 * ( 1 + item.profit / 100) / 100) * 100;
            harga_eceran = parseFloat(harga) * 1.1 * ( 1 + item.profit_eceran / 100);
            harga_grosir = parseFloat(harga) * 1.1 * ( 1 + item.profit / 100);

            relasi_satuans = '{{ json_encode($relasi_satuans) }}';
            relasi_satuans = relasi_satuans.replace(/&quot;/g, '"');
            relasi_satuans = JSON.parse(relasi_satuans);

            satuan_terbesar = {satuan_id: relasi_satuans[0].satuan_id,
                            konversi: relasi_satuans[0].konversi
                            };

            satuan_terkecil = {satuan_id: relasi_satuans[relasi_satuans.length-1].satuan_id,
                            konversi: relasi_satuans[relasi_satuans.length-1].konversi
                            };

            supliers = '{{ json_encode($supliers) }}';
            supliers = supliers.replace(/&quot;/g, '"');
            supliers = JSON.parse(supliers);

            if (supliers.length == 1) {
                $('#PemasokTable').find('button').prop('disabled', true);
            }

            $('#formHargaJual').find('button').prop('disabled', true);

            disable_form_satuan = '{{ json_encode($disable_form_satuan) }}';
            disable_form_satuan = disable_form_satuan.replace(/&quot;/g, '"');
            disable_form_satuan = JSON.parse(disable_form_satuan);

            $(".select2_single").select2({
            });

            $("#satuan_harga").select2({
                maximumSelectionLength: 1
            });

            $("#satuan_harga").val('').change();

            $('input[name="grosir_"]').prop('disabled', true);
            $('input[name="eceran_"]').prop('disabled', true);
            $('#satuan_harga').prop('disabled', true);

            $('.stok').each(function(index, el) {
                var text = $(el).text();
                text = text.split(' ');
                var stok = parseFloat(text[0]);
                if (stok <= 0) {
                    $(el).hide();
                    $(el).next().hide();
                }
            });

            $('.stok_detail').each(function(index, el) {
                var text = $(el).text();
                text = text.split(' ');
                var stok = parseFloat(text[0]);
                if (stok <= 0) {
                    $(el).hide();
                    // $(el).next().hide();
                }
            });

            var profit_eceran = parseFloat($('.profit_eceran').text());
            var profit_grosir = parseFloat($('.profit_grosir').text());
            var diskon = parseFloat($('.diskon').text());

            // $('.profit_eceran').text(profit_eceran.toLocaleString() + '%');
            $('.profit_grosir').text(profit_grosir.toLocaleString() + '%');
            $('.diskon').text(diskon.toLocaleString() + '%');

            // if (item.bonus == 1) {
            //     $('#bonusContainer').find('input').prop('disabled', true);
            //     $('#bonusContainer').find('select').prop('disabled', true);
            //     $('#bonusContainer').find('button').prop('disabled', true);

            //     $('#limitStokContainer').find('input').prop('disabled', true);
            //     $('#limitStokContainer').find('select').prop('disabled', true);
            //     $('#limitStokContainer').find('button').prop('disabled', true);

            //     $('#limitGrosirContainer').find('input').prop('disabled', true);
            //     $('#limitGrosirContainer').find('select').prop('disabled', true);
            //     $('#limitGrosirContainer').find('button').prop('disabled', true);
            // }

            if (disable_form_satuan || item.valid_konversi == 1) {
                // console.log(disable_form_satuan, item.valid_konversi);
                $('#satuanContainer').find('input').prop('disabled', true);
                $('#satuanContainer').find('select').prop('disabled', true);
                $('#satuanContainer').find('button').prop('disabled', true);
                $('#satuanContainer').find('#aktivasiValidasiSatuan').prop('disabled', false);
                if (item.valid_konversi == 0) {
                    $('#satuanContainer').find('#aktivasiValidasiSatuan').prop('disabled', true);
                    $('#satuanContainer').find('#fixKonversiSatuan').prop('disabled', false);
                }
            }

            if (item.valid_konversi == 0) {
                $('#satuanContainer').find('input').prop('disabled', false);
                $('#satuanContainer').find('select').prop('disabled', false);
                $('#satuanContainer').find('button').prop('disabled', false);
                $('#satuanContainer').find('#aktivasiValidasiSatuan').prop('disabled', true);
                $('#satuanContainer').find('#fixKonversiSatuan').prop('disabled', false);
            }
        });

        $(document).on('click', '#UbahHarga', function(event) {
            event.preventDefault();

            var $form = $('#formSimpanHargaContainer').find('form');
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var ecer = $tr.find('td').first().next().next().text().split('Rp');
            var grosir = $tr.find('td').first().next().next().next().text().split('Rp');

            satuan_harga = satuan_id;
            is_terkecil = (satuan_id == satuan_terkecil.satuan_id) ? true : false;
            is_terbesar = (satuan_id == satuan_terbesar.satuan_id) ? true : false;

            $('#satuan_harga').val(satuan_id).change();
            $('#satuan_hargas').val(satuan_id).change();
            $('input[name="eceran_"]').prop('disabled', false);
            $('input[name="eceran_"]').val(ecer[1]);
            $('input[name="eceran"]').val(parseInt(ecer[1].replace(/\D/g, ''), 10));

            $('#formGrosiran').removeClass('has-error');
            $('#grosir_error').text('');
            $('#formEceran').removeClass('has-error');
            $('#eceran_error').text('');

            $('input[name="grosir_"]').prop('disabled', false);
            $('input[name="grosir_"]').val(grosir[1]);
            $('input[name="grosir"]').val(parseInt(grosir[1].replace(/\D/g, ''), 10));

            $form.attr('action', '{{ url("harga") }}' + '/' + id + '/update');
            $('#btnSimpanHarga span').text('Ubah');

            $(this).parents('.x_content').find('#btnSimpanHarga').prop('disabled', false);
        });

        $(document).on('click', '#ubah_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $(this).parents('tr').attr('id');
            var satu = $tr.find('td').first().data('satu');
            var konversi = $tr.find('td').first().next().text();
            
            $('#satuan_relasi').val(satu).trigger("change")
            // $('select[name="satuanID"]').val(satu);
            $('input[name="konversi"]').val(konversi);

            $('#formSimpanSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
            $('#formSimpanSatuan').find('input[name="_method"]').val('put');
            $('#btnSimpanSatuan span').text('Ubah');
        });

        $(document).on('click', '#UbahRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().attr('id');
            var syarat = $tr.find('td').first().next().attr('id');
            var satuan = $tr.find('td').first().next().attr('class');

            $('#formBonusContainer').find('#bonus').val(bonus).trigger('change');
            $('#formBonusContainer').find('#satuan_bonus').val(satuan).trigger('change');

            $('#formBonusContainer').find('input[name="syarat"]').val(syarat);
            
            $('#formBonusContainer').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id  + '/update');
            $('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapusRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().text();
            var kode = $('#item_kode').text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: 'Bonus \ "' + bonus + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusBonus').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id + '/' + kode +'/delete');
                $('#formHapusBonus').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_harga', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id + '/delete');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $tr.find('td').first().text();
            var konversi = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + satuan_id + ' ' + konversi +'\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
                $('#formHapusSatuan').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_item_kode', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var item_kode = $tr.attr('id');
            $('#formHapusItemKode input[name="item_kode"]').val(item_kode);

            swal({
                title: 'Hapus?',
                text: '\"' + item_kode + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusItemKode').find('form').attr('action', '{{ url("item") }}' + '/' + item_kode + '/kode');
                $('#formHapusItemKode').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_suplier', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').next().text();
            $('#formHapusPemasok input[name="suplier_id"]').val(id);

            swal({
                title: 'Hapus?',
                text: 'Pemasok "' + nama + '" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusPemasok').find('form').attr('action', '{{ url("item") }}' + '/hapus/pemasok');
                $('#formHapusPemasok').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#fixKonversiSatuan', function(event) {
            event.preventDefault();

            swal({
                title: 'Anda yakin?',
                text: 'Anda yakin konversi satuan item ini sudah benar?',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Yakin',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                console.log('yakin');
                $('#formFixKonversiSatuan').submit();
            }).catch(function(data) {
                console.log(data);
            });
        });

        $(document).on('click', '#aktivasiValidasiSatuan', function(event) {
            event.preventDefault();

            var item_kode = $(this).attr('item_kode');
            var url = "{{ url('item') }}"+'/'+item_kode+'/valid-konversi';
            console.log(url);
            $.post(url, {
                _token: '{{ csrf_token() }}',
                _method: 'post',
                valid_konversi: 0
            }, function(data) {
                // console.log(data);
                if (!data.error) {
                    $('#satuanContainer').find('input').prop('disabled', false);
                    $('#satuanContainer').find('select').prop('disabled', false);
                    $('#satuanContainer').find('button').prop('disabled', false);
                    $('#satuanContainer').find('#aktivasiValidasiSatuan').prop('disabled', true);
                    if (disable_form_satuan) {
                        $('#satuanContainer').find('.btnUbah').prop('disabled', true);
                        $('#satuanContainer').find('.btnHapus').prop('disabled', true);
                    }
                }
            });
        });

        $(document).on('change', '#formSimpanSatuan #satuan_relasi', function(event) {
            event.preventDefault();

            handleEditKonversiSatuan();
        });

        $(document).on('keyup', '#formSimpanSatuan input[name="konversi"]', function(event) {
            event.preventDefault();

            handleEditKonversiSatuan();
        });

        $(document).on('keyup', 'input[name="eceran_"]', function(event) {
            event.preventDefault();
            // cekHarga();
            // var ecer = $('input[name="eceran"]').val();
            var eceran = parseInt($(this).val().replace(/\D/g, ''), 10);
            var is_ratusan = eceran % 100 == 0;
            var konversi = 0;

            for (var i=0; i < relasi_satuans.length; i++) {
                if (relasi_satuans[i].satuan_id == satuan_harga) konversi = relasi_satuans[i].konversi;
            }
            var _harga = Math.round((harga_eceran * konversi) / 100) * 100;

            if (eceran == 0) {
                $('#formEceran').removeClass('has-error');
            } else if (is_ratusan) {
                // $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', false);
                // $('#formEceran').removeClass('has-error');
                if (satuan_harga == satuan_terbesar.satuan_id) {
                    console.log('aw2');
                    var harga_atas = []; 
                    var harga_bawah = []; 
                    var is_done = false;
                    var konversi = 0;
                    for (var i=0; i < relasi_satuans.length; i++) {
                        if (relasi_satuans[i].satuan_id == satuan_harga) {
                            is_done = true;
                            konversi = relasi_satuans[i].konversi;
                        } else {
                            harga_bawah.push({harga: hargas[i].eceran,
                                konversi: relasi_satuans[i].konversi});
                        }
                    }

                    var is_wrong = false;

                    for (var i=0; i < harga_bawah.length; i++) {
                        if (eceran / konversi/harga_bawah[i].konversi > harga_bawah[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu mahal';
                            break;
                        }
                    }

                    if (is_wrong) {
                        $('#formEceran').addClass('has-error');
                        $('#eceran_error').text(text_error);
                    } else {
                        $('#formEceran').removeClass('has-error');
                        $('#eceran_error').text('');
                    }
                } else if (satuan_harga == satuan_terkecil.satuan_id) {
                    var harga_atas = []; 
                    var harga_bawah = []; 
                    var is_done = false;
                    var konversi = 0;
                    for (var i=0; i < relasi_satuans.length; i++) {
                        if (relasi_satuans[i].satuan_id == satuan_harga) {
                            is_done = true;
                            konversi = relasi_satuans[i].konversi;
                        } else {
                            harga_atas.push({harga: hargas[i].eceran,
                                konversi: relasi_satuans[i].konversi});
                        }
                    }
                    
                    var is_wrong = false;

                    var text_error = '';
                    for (var i=0; i < harga_atas.length; i++) {
                        if (eceran * harga_atas[i].konversi/konversi < harga_atas[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu murah';
                            break;
                        }
                    }

                    if (is_wrong) {
                        $('#formEceran').addClass('has-error');
                        $('#eceran_error').text(text_error);
                    } else {
                        $('#formEceran').removeClass('has-error');
                        $('#eceran_error').text('');
                    }
                } else {
                    var harga_atas = []; 
                    var harga_bawah = []; 
                    var is_done = false;
                    var konversi = 0;
                    for (var i=0; i < relasi_satuans.length; i++) {
                        if (relasi_satuans[i].satuan_id == satuan_harga) {
                            is_done = true;
                            konversi = relasi_satuans[i].konversi;
                        } else {
                            if (is_done) {
                                harga_bawah.push({harga: hargas[i].eceran,
                                    konversi: relasi_satuans[i].konversi});
                            } else {
                                harga_atas.push({harga: hargas[i].eceran,
                                    konversi: relasi_satuans[i].konversi});
                            }
                        }
                    }
                    
                    var is_wrong = false;

                    var text_error = '';
                    for (var i=0; i < harga_atas.length; i++) {
                        if (eceran * harga_atas[i].konversi/konversi < harga_atas[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu murah';
                            break;
                        }
                    }

                    for (var i=0; i < harga_bawah.length; i++) {
                        if (eceran / konversi/harga_bawah[i].konversi > harga_bawah[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu mahal';
                            break;
                        }
                    }

                    if (is_wrong) {
                        $('#formEceran').addClass('has-error');
                        $('#eceran_error').text(text_error);
                    } else {
                        $('#formEceran').removeClass('has-error');
                        $('#eceran_error').text('');
                    }
                }
            } else {
                // $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
                $('#formEceran').addClass('has-error');
            }
            cekHarga();
        });

        $(document).on('keyup', 'input[name="grosir_"]', function(event) {
            event.preventDefault();
            // var grosir = $('input[name="grosir"]').val();
            var grosir = parseInt($(this).val().replace(/\D/g, ''), 10);
            var is_ratusan = grosir % 100 == 0;
            var konversi = 0;

            for (var i=0; i < relasi_satuans.length; i++) {
                if (relasi_satuans[i].satuan_id == satuan_harga) konversi = relasi_satuans[i].konversi;
            }
            var _harga = Math.round((harga_grosir * konversi) / 100) * 100;

            if (grosir == 0) {
                $('#formGrosiran').removeClass('has-error');
            } else if (is_ratusan) {
                if (satuan_harga == satuan_terbesar.satuan_id) {
                    if (grosir < _harga) {
                        $('#formGrosiran').addClass('has-error');
                        text_error = 'Harga terlalu murah';
                        $('#grosir_error').text(text_error);
                    } else {
                        // var harga_atas = []; 
                        // var harga_bawah = []; 
                        // var is_done = false;
                        // var konversi = 0;
                        // for (var i=0; i < relasi_satuans.length; i++) {
                        //     if (relasi_satuans[i].satuan_id == satuan_harga) {
                        //         is_done = true;
                        //         konversi = relasi_satuans[i].konversi;
                        //     } else {
                        //         harga_bawah.push({harga: hargas[i].grosir,
                        //             konversi: relasi_satuans[i].konversi});
                        //     }
                        // }

                        // var is_wrong = false;

                        // for (var i=0; i < harga_bawah.length; i++) {
                        //     if (grosir / konversi/harga_bawah[i].konversi > harga_bawah[i].harga) {
                        //         is_wrong = true;
                        //         text_error = 'Harga terlalu mahal';
                        //         break;
                        //     }
                        // }

                        // if (is_wrong) {
                        //     $('#formGrosiran').addClass('has-error');
                        //     $('#grosir_error').text(text_error);
                        // } else {
                            $('#formGrosiran').removeClass('has-error');
                            $('#grosir_error').text('');
                        // }
                    }
                } else if (satuan_harga == satuan_terkecil.satuan_id) {
                    var harga_atas = []; 
                    var harga_bawah = []; 
                    var is_done = false;
                    var konversi = 0;
                    for (var i=0; i < relasi_satuans.length; i++) {
                        if (relasi_satuans[i].satuan_id == satuan_harga) {
                            is_done = true;
                            konversi = relasi_satuans[i].konversi;
                        } else {
                            harga_atas.push({harga: hargas[i].grosir,
                                konversi: relasi_satuans[i].konversi});
                        }
                    }
                    
                    var is_wrong = false;

                    var text_error = '';
                    for (var i=0; i < harga_atas.length; i++) {
                        if (grosir * harga_atas[i].konversi/konversi < harga_atas[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu murah';
                            break;
                        }
                    }

                    if (is_wrong) {
                        $('#formGrosiran').addClass('has-error');
                        $('#grosir_error').text(text_error);
                    } else {
                        $('#formGrosiran').removeClass('has-error');
                        $('#grosir_error').text('');
                    }
                } else {
                    var harga_atas = []; 
                    var harga_bawah = []; 
                    var is_done = false;
                    var konversi = 0;
                    for (var i=0; i < relasi_satuans.length; i++) {
                        if (relasi_satuans[i].satuan_id == satuan_harga) {
                            is_done = true;
                            konversi = relasi_satuans[i].konversi;
                        } else {
                            if (is_done) {
                                harga_bawah.push({harga: hargas[i].grosir,
                                    konversi: relasi_satuans[i].konversi});
                            } else {
                                harga_atas.push({harga: hargas[i].grosir,
                                    konversi: relasi_satuans[i].konversi});
                            }
                        }
                    }
                    
                    var is_wrong = false;

                    var text_error = '';
                    for (var i=0; i < harga_atas.length; i++) {
                        if (grosir * harga_atas[i].konversi/konversi < harga_atas[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu murah';
                            break;
                        }
                    }

                    for (var i=0; i < harga_bawah.length; i++) {
                        if (grosir / konversi/harga_bawah[i].konversi > harga_bawah[i].harga) {
                            is_wrong = true;
                            text_error = 'Harga terlalu mahal';
                            break;
                        }
                    }

                    if (is_wrong) {
                        $('#formGrosiran').addClass('has-error');
                        $('#grosir_error').text(text_error);
                    } else {
                        $('#formGrosiran').removeClass('has-error');
                        $('#grosir_error').text('');
                    }
                }
                // $('#formGrosiran').removeClass('has-error');
            } else {
                // $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
                $('#formGrosiran').addClass('has-error');
            }
            cekHarga();
        });

        function cekHarga() {
            if ($('#formGrosiran').hasClass('has-error') || $('#formEceran').hasClass('has-error')) {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
            } else {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', false);
            }
        }

        $(document).on('click', '#btnResetHarga', function(event) {
            event.preventDefault();

            var id = item.kode_barang;
            id = id.replace(/amp;/g, '');
            var url = '{{ url('item/reset/harga/') }}' + '/' + id;

            swal({
                title: 'Reset Harga Jual?',
                text: 'Harga Jual akan direset!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Reset!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                //confirmed
                // $('#formHapusContainer').find('form').attr('action', '{{ url("user") }}' + '/' + id);
                window.open(url, '_self');
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('keyup', '#diskon_persen', function(event) {
            event.preventDefault();

            var text = $(this).val();
            var diskon_val = 0;

            if (text.indexOf(',') != -1) {
                var x = text.split(',');
                if (x.length > 0 && x[1].length > 2) {
                    var after = parseFloat(x[0]+'.'+x[1].substring(0, 2));
                    diskon_val = after;
                }
            } else if (text.indexOf('.') != -1) {
                var x = text.split('.');
                if (x.length > 0 && x[1].length > 2) {
                    var after = parseFloat(x[0]+'.'+x[1].substring(0, 2));
                    diskon_val = after;
                }
            } else {
                diskon_val = parseFloat(text);
            }

            var diskon_allowed = true;
            for (var i=0; i < relasi_satuans.length; i++) {
                var harga_super = hargas[i].grosir * parseFloat(1 - (diskon_val/100));
                var harga_hpp = harga * relasi_satuans[i].konversi;
                if (harga_super < harga_hpp) {
                    diskon_allowed = false;
                    break;
                }
            }

            if (diskon_allowed) {
                $('#diskonPersenForm').removeClass('has-error');
                $('#btnSimpanDiskon').prop('disabled', false);
                $('#diskonInfo').hide();
            } else {
                $('#diskonPersenForm').addClass('has-error');
                $('#btnSimpanDiskon').prop('disabled', true);
                $('#diskonInfo').show();
            }
        });
    </script>
@endsection
