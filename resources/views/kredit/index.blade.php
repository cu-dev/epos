@extends('layouts.admin')

@section('title')
    <title>EPOS | Kredit</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Tambah Akun Kredit</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('kredit') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group">
						<label class="control-label">Atas Nama</label>
						<input class="form-control" type="text" name="atas_nama">
					</div>
					<div class="form-group">
						<label class="control-label">Nomor kartu Kredit</label>
						<input class="form-control" type="text" name="nomor_kredit">
					</div>
					<div class="form-group">
						<label class="control-label">Validasi</label>
						<input class="form-control" type="text" name="validasi">
					</div>
					<div class="form-group">
						<label class="control-label">VCC</label>
						<input class="form-control" type="text" name="vcc">
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-save"></i> <span>Tambah</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- kolom kanan -->
	<div class="col-md-8 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Akun Kredit</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
					<thead>
						<tr>
							<th>No</th>
							<th>Atas Nama</th>
							<th>Nomor Kartu Kredit</th>
							<th>Validasi</th>
							<th>VCC</th>
							<th>Operator</th>
							<!-- <th>Aksi</th> -->
						</tr>
					</thead>
					<tbody>
						@foreach($kredits as $num => $kredit)
						<tr id="{{$kredit->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $kredit->nama }}</td>
							<td>{{ $kredit->nomor }}</td>
							<td>{{ $kredit->validasi }}</td>
							<td>{{ $kredit->vcc }}</td>
							<td>{{ $kredit->user->nama }}</td>
<!-- 							<td>
								<button class="btn btn-xs btn-primary" id="btnUbah">
									<i class="fa fa-edit"></i> Ubah
								</button>
							</td> -->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableBank').DataTable();

		$(document).on('click', '#btnUbah', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama_bank = $tr.find('td').first().next().text();
			var no_rekening = $tr.find('td').first().next().next().text();
			var atas_nama = $tr.find('td').first().next().next().next().text();
			var nominal_get = $tr.find('td').first().next().next().next().next().text();
			var nominal_set = nominal_get.substr(2, nominal_get.length);
			var a = '';
			var nominal = nominal_set.split('.').join(a);

			var nominal_h = parseFloat(nominal.replace(',', '.'));
			console.log(nominal, nominal_get, nominal_set, nominal_h);
			
			$('input[name="nominal"]').val(nominal_h);
			$('#nominal_v').text(nominal_get);
			$('input[name="nama_bank"]').val(nama_bank);
			$('input[name="no_rekening"]').val(no_rekening);
			$('input[name="atas_nama"]').val(atas_nama);
			$('input[name="nominal_i"]').val(nominal);

			$('#formSimpanContainer').find('form').attr('action', '{{ url("bank") }}' + '/' + id);
			$('#formSimpanContainer').find('input[name="_method"]').val('put');
			$('#btnSimpan span').text('Ubah');

			$('#formSimpanTitle').text('Ubah Akun bank');
		});

		$(document).on('keyup', 'input[name="nominal_i"]', function(event) {
			event.preventDefault();
			var nominals = $(this).val().split(',');
			var nominal = 0;
			if(nominals.length > 1){
				nominal = $(this).val();
				if(nominals[0].length < 1){
					nominals[0] = 0;
					$(this).val(nominals[0] + ',' + nominals[1]);
				}
				else if(nominals[1].length > 3){
					nominal = $(this).val().slice(0,-1);
					$(this).val(nominal);
					// console.log(nominal.toLocaleString(['ban', 'id']));
				}else if(nominals[1].length < 3){
					nominal = parseFloat(nominal.replace(',', '.'));
					nominal = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
					nominal = nominal.replace('.', '');
					$(this).val(nominal);
				}
			}else{
				nominal = $(this).val();
				var bel_koma = nominal.substr(nominal.length -2);
				var depan_koma = nominal.substr(0, nominal.length -2);

				$(this).val(depan_koma + ',' + bel_koma);
			}

			var nominal_h = parseFloat(nominal.replace(',', '.'));
			var nominal_v = nominal_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});

			$('#nominal_v').text('Rp'+nominal_v);
			$('input[name="nominal"]').val(nominal_h);
		}); 
	</script>
@endsection
