@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Item Belum Validasi </title>
@endsection

@section('style')
    <style media="screen">
    	#btnHistory, #btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Daftar Item Belum Validasi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
					<thead>
						<tr>
							<th width="5px">No</th>
							<th >Kode Item</th>
							<th >Nama Item</th>
							<th width="25px">Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($list as $i => $item)
						<tr>
							<td>{{ $i + 1 }}</td>
							<td>{{ $item->kode }}</td>
							<td>{{ $item->nama }}</td>
							<td>
								<a href="{{ url('item/show/'.$item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                    <i class="fa fa-eye"></i>
                                </a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Item Konsinyasi berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Item Konsinyasi gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Item Konsinyasi berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Item Konsinyasi gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Item Konsinyasi berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Item Konsinyasi gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();

		$(document).ready(function() {
			$(".select2_single").select2({
				width: '100%'
			});
		});

		
	</script>
@endsection
