@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Penjual</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Penjual</h2>
                <a href="{{ url('seller') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('seller') }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="control-label">Nama</label>
                        <input class="form-control" type="text" name="nama" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alamat</label>
                        <input class="form-control" type="text" name="alamat">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Telepon</label>
                        <input class="form-control" type="text" name="telepon">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" type="text" name="email">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Pilih Pemasok</label>
                        <select name="suplier_id" class="form-control select2_single" required="">
                            <option value="">Pilih Pemasok</option>
                            @foreach ($supliers as $suplier)
                            <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group" style="margin-top: 20px; margin-bottom: 0;">
                        <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function() {
            var url = "{{ url('seller') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2();
        });

        $(document).on('click', '#btnReset', function() {
            $('input[name="nama"]').val('');
            $('input[name="alamat"]').val('');
            $('input[name="telepon"]').val('');
            $('input[name="email"]').val('');
            $('input[name="suplier"]').val('');
        });
    </script>
@endsection
