@extends('layouts.admin')

@section('title')
    <title>EPOS | Penjual</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-11">
                        <h2 id="formSimpanTitle">Tambah Penjual</h2>
                    </div>
                    <div class="col-md-1">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('seller') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Nama Penjual</label>
                                <input class="form-control" type="text" name="nama" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pilih Pemasok</label>
                                <select name="suplier_id" class="form-control select2_single" required="">
                                    <option value="">Pilih Pemasok</option>
                                    @foreach ($supliers as $suplier)
                                    <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Telepon</label>
                                <input class="form-control" type="text" name="telepon">
                                <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="email" name="email">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Alamat</label>
                                <textarea rows="4" class="form-control" name="alamat" ></textarea>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                                <button class="btn btn-default" id="btnReset" type="button">
                                    <i class="fa fa-refresh"></i> Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    {{-- Penjual Aktif --}}
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Penjual</h2>
                {{-- <a href="{{ url('seller/create') }}" class="btn btn-sm btn-success pull-right">
                    <i class="fa fa-plus"></i> Tambah
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePenjual">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Penjual</th>
                            <th>Pemasok</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sellers as $num => $seller)
                        <tr id="{{ $seller->id }}">
                            <td>{{ $num + 1 }}</td>
                            <td>{{ $seller->nama }}</td>
                            <td suplier_id="{{ $seller->suplier->id }}">{{ $seller->suplier->nama }}</td>
                            <td>{{ $seller->telepon }}</td>
                            <td>{{ $seller->email }}</td>
                            <td>{{ $seller->alamat }}</td>
                            <td class="tengah-h">
                                <button class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Penjual">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Penjual">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- Penjual non-aktif --}}
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Penjual</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePenjualNon">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Penjual</th>
                            <th>Pemasok</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th style="width: 25px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($seller_off as $num => $seller)
                        <tr id="{{ $seller->id }}">
                            <td>{{ $num + 1 }}</td>
                            <td>{{ $seller->nama }}</td>
                            <td suplier_id="{{ $seller->suplier->id }}">{{ $seller->suplier->nama }}</td>
                            <td>{{ $seller->telepon }}</td>
                            <td>{{ $seller->email }}</td>
                            <td>{{ $seller->alamat }}</td>
                            <td class="tengah-h">
                                <button class="btn btn-xs btn-success" id="btnAktif"  data-toggle="tooltip" data-placement="top" title="Aktifkan Penjual">
                                    <i class="fa fa-check"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penjual berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Penjual gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penjual berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Penjual gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penjual berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penjual berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Penjual gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $(document).ready(function() {
            $('#tablePenjual').DataTable();
            $('#tablePenjualNon').DataTable();
            $(".select2_single").select2();

            $('#btnReset').trigger('click');
        });

        $(document).on('click', '#btnReset', function() {
            $('input[name="nama"]').val('');
            $('input[name="telepon"]').val('');
            $('input[name="email"]').val('');
            $('textarea[name="alamat"]').val('');
            $('select[name="suplier_id"]').val('').trigger('change');

            $('#formSimpanContainer').find('form').attr('action', '{{ url("seller") }}');
            $('#formSimpanContainer').find('input[name="_method"]').val('post');
            $('#btnSimpan span').text('Tambah');
            $('#formSimpanTitle').text('Tambah Penjual');
        });

        $(document).on('click', '#btnUbah', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            var suplier_id = $tr.find('td').first().next().next().attr('suplier_id');
            var telepon = $tr.find('td').first().next().next().next().text();
            var email = $tr.find('td').first().next().next().next().next().text();
            var alamat = $tr.find('td').first().next().next().next().next().next().text();

            $('input[name="nama"]').val(nama);
            $('select[name="suplier_id"]').val(suplier_id).trigger('change');
            $('input[name="telepon"]').val(telepon);
            $('input[name="email"]').val(email);
            $('textarea[name="alamat"]').val(alamat);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("seller") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Penjual');
        });
        
        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                $('#formHapusContainer').find('form').attr('action', '{{ url("seller") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var nama = $tr.find('td').first().next().text();
            var id = $tr.attr('id');

            var url = '{{ url('seller/aktif/') }}' + '/' + id;

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                window.open(url, '_self');
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
