@extends('layouts.admin')

@section('title')
    <title>EPOS | Pembayaran Gaji Karyawan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-5" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Pembayaran Gaji Karyawan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right">	
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('gaji') }}" class="form-horizontal">
                        <div class="col-md-12 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pilih Karyawan</label>
                                <select name="karyawan" id="karyawan" class="select2_single form-control" required="">
                                    <option value ="" id="default">Pilih Karyawan</option>
                                    @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- <div class="form-group" id=nominal>
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_i">
                                <input type="hidden" name="nominal">		
                            </div> --}}
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control input_cek" type="text" name="keterangan" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Sumber Kas</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnGaji" class="btn btn-default">Piutang Karyawan</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <label class="control-label">Nominal Tunai</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                        </div>
                                    </div>
                                <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                    <div class="line"></div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                            <label class="control-label">Pilih Bank</label>
                                            <select class="form-control select2_single" name="bank_id" id="bank_id">
                                                <option value="">Pilih Bank</option>
                                                @foreach ($banks as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <label class="control-label">Nomor Transfer</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">#</div>
                                                <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control input_cek">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <label class="control-label">Nominal Transfer</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">Rp</div>
                                                <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka input_cek">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="inputPiutangContainer" class="form-group col-sm-12 col-xs-12">
                                    <div class="line"></div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            <label class="control-label">Pilih Piutang Karyawan</label>
                                            <select class="form-control select2_single" name="piutang" id="selectPiutang">
                                            </select>
                                        </div>
                                        <div class="col-sm-6 col-xs-6" id="piutangGroup">
                                            <label class="control-label">Nominal Bayar Piutang</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">Rp</div>
                                                <input type="text" name="inputNominalPiutang" id="inputNominalPiutang" class="form-control angka input_cek" style="height: 38px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <input type="hidden" name="nominal_tunai" />
                                <input type="hidden" name="nama_karyawan" />

                                <input type="hidden" name="no_transfer" />
                                <input type="hidden" name="bank_transfer" />
                                <input type="hidden" name="nominal_transfer" />

                                <input type="hidden" name="piutang_id" />
                                <input type="hidden" name="piutang_nominal" />
                                <input type="hidden" name="sisa_piutang" />

                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Bayar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7 col-xs-7" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Daftar Pembayaran Gaji Karyawan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('gaji/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="referensi">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableJenisItem" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="referensi">No</th>
                                <th class="sorting" field="referensi">Refensi</th>
                                <th class="sorting" field="debet">Nominal</th>
                                <th class="sorting" field="keterangan">Karyawan</th>
                                <th class="sorting" field="keterangan">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </div>	 -->


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('gaji/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="5" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var gaji = data[i];

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${gaji.id}">
                                    <td>${nomor}</td>
                                    <td>${gaji.referensi}</td>
                                    <td class="text-right">${gaji.debet}</td>
                                    <td>${gaji.karyawan}</td>
                                    <td>${gaji.keterangan}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        var piutang = 0;
        $(document).ready(function() {
            $('#inputTunaiContainer').hide();
            $('#inputPiutangContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputNominalTunai').val('');
            $('#btnGaji').prop('disabled', true);
            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            $('#formSimpanContainer').find('input[name="no_transfer"]').remove();
            btnSimpan();
        });

        $(document).on('click', '#btnGaji', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                // $('#inputPiutangContainer')
                $('#inputPiutangContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $('#inputPiutangContainer').hide('hide', function() {
                    // updateHargaOnKeyup();
                    $('input[name="sisa_piutang"]').val('');
                    $('input[name="piutang_id"]').val('');
                    $('input[name="piutang_nominal"]').val('');
                    $('#selectPiutang').val('').change();
                    $('#inputNominalPiutang').val('');
                });
            }
            // updateHargaOnKeyup();
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $('#inputTransferBankContainer').hide('hide', function() {
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="no_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');
                    $('#inputNoTransfer').val('');
                    $('#inputNominalTransfer').val('');

                    $(this).find('select[name="bank_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
            btnSimpan();
        });

        $(window).on('load', function(event) {
            var url = "{{ url('gaji/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');
            $.get(url, function(data) {
                if (data.beban === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/BGK/' + tanggal;
                } else {
                    var referensi = data.beban.referensi.substr(0,16);
                    // var referensi = data.beban.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/BGK/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/BGK/' + tanggal_transaksi;				}
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // $(this).val(nominal_tunai.toLocaleString());
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            // updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_transfer"]').val(id);
            // updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            // updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            // updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="karyawan"]', function(event) {
            event.preventDefault();
            var id = $(this).val();
            var nama = $('select[name="karyawan"] option:selected').text();
            $('input[name="nama_karyawan"]').val(nama);

            var url = "{{ url('gaji') }}"+'/'+id+'/piutang/json';

            $.get(url, function(data) {
                var option = '<option value="">Pilih Piutang</option>';
                if(Object.keys(data.piutangs).length > 0){
                    $('#btnGaji').prop('disabled', false);
                    for(var i=0; i<Object.keys(data.piutangs).length; i++){
                        var sisa = parseInt(data.piutangs[i].sisa).toLocaleString(['ban', 'id']);
                        // var sisa = data.piutangs[i].sisa.toLocaleString(['ban', 'id']);
                        option += '<option value="'+data.piutangs[i].id+'" sisa="'+data.piutangs[i].sisa+'">'+data.piutangs[i].kode_transaksi+' [Rp'+sisa+']</option>';
                    }
                    $('select[name="piutang"]').empty();
                    $('select[name="piutang"]').append(option);
                }else{
                    $('#btnGaji').prop('disabled', true);
                    $('select[name="piutang"]').empty();
                    $('input[name="piutang_id"]').val('');
                    $('#btnGaji').removeClass('btn-info');
                    $('#btnGaji').addClass('btn-default');
                     $('#inputPiutangContainer').hide('hide', function() {
                        // updateHargaOnKeyup();
                        $('input[name="sisa_piutang"]').val('');
                        $('input[name="piutang_id"]').val('');
                        $('input[name="piutang_nominal"]').val('');
                        $('#selectPiutang').val('').change();
                        $('#inputNominalPiutang').val('');
                    });
                }
            });
        });

        $(document).on('change', 'select[name="piutang"]', function(event) {
            event.preventDefault();
            var id = $(this).val();
            var url = "{{ url('gaji') }}"+'/'+id+'/sisa/json';
            $('input[name="piutang_id"]').val(id);

            $.get(url, function(data) {
                // console.log(data.sisa);
                piutang = parseInt(data.sisa);
                $('input[name="sisa_piutang"]').val(piutang);
            });
        });

        $(document).on('keyup', '#inputNominalPiutang', function(event) {
            event.preventDefault();
            var tr = $(this).parents('selector')
            var nominal_piutang = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if(nominal_piutang>piutang){
                $('#piutangGroup').addClass('has-error');
            }else{
                $('#piutangGroup').removeClass('has-error');
                $('input[name="piutang_nominal"]').val(nominal_piutang);
            }
            
            // updateHargaOnKeyup();
        });

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#karyawan', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank_id', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-warning')){
                var bank = $('#bank_id').val();
                console.log(bank);
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            }
        }

    </script>
@endsection
