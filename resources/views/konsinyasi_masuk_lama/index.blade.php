@extends('layouts.admin')

@section('title')
    <title>EPOS | Konsinyasi Masuk</title>
@endsection

@section('style')
    <style media="screen">
    	#btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Konsinyasi Masuk</h2>
				<a href="{{ url('konsinyasi-masuk/create') }}" class="btn btn-sm btn-success pull-right" id="btnUbah">
	                <i class="fa fa-plus"></i> Tambah
	            </a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Item</th>
							<th>Nama Suplier</th>
							<th>Jumlah</th>
							<th>Harga Dasar</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($konsinyasi_masuks as $num => $konsinyasi)
						<tr id="{{$konsinyasi->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $konsinyasi->item->nama }}</td>
							<td>{{ $konsinyasi->suplier->nama }}</td>
							<td>{{ $konsinyasi->jumlah }}</td>
							<td>{{ $konsinyasi->harga_dasar }}</td>
							<td>
								<a href="{{ url('konsinyasi-masuk/'.$konsinyasi->id) }}" class="btn btn-xs btn-info" id="btnDetail">
									<i class="fa fa-eye"></i> Detail
								</a>
								<a href="{{ url('konsinyasi-masuk/edit/'.$konsinyasi->id) }}" class="btn btn-xs btn-primary" id="btnUbah">
									<i class="fa fa-edit"></i> Ubah
								</a>
								<a href="{{ url('konsinyasi-keluar/create/'.$konsinyasi->id) }}" class="btn btn-xs btn-danger" id="btnUbah">
									<i class="fa fa-sign-out"></i> Keluar
								</a>
								<!-- <button class="btn btn-xs btn-danger" id="btnHapus">
									<i class="fa fa-trash"></i> Hapus
								</button> -->
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Masuk berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Masuk gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Masuk berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Masuk gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Masuk berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Masuk gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItemMasuk').DataTable();

		$(document).on('click', '#btnHapus', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama_item = $tr.find('td').first().next().text();
			var nama_suplier = $tr.find('td').first().next().next().text();
			$('input[name="id"]').val(id);

			swal({
				title: 'Hapus?',
				text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#009688',
				cancelButtonColor: '#ff5252',
				confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
				cancelButtonText: '<i class="fa fa-close"></i> Batal'
			}, function(isConfirm) {
				if (isConfirm) {
					// Confirmed
					$('#formHapusContainer').find('form').attr('action', '{{ url("konsinyasi-masuk") }}' + '/' + id);
					$('#formHapusContainer').find('form').submit();
				} else {
					// Canceled
				}
			});
		});
	</script>
@endsection
