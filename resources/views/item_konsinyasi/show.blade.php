@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Item Konsinyasi</title>
@endsection

@section('style')
    <style media="screen">
        .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Item Konsinyasi</h2>
                <a href="{{ url('item-konsinyasi/tambah_suplier/'.$item->kode) }}" class="btn btn-sm btn-success pull-right" id="btnUbah">
                    <i class="fa fa-plus"></i> Tambah Suplier
                </a>
                <a href="{{ url('item-konsinyasi/edit/'.$item->kode) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah">
                    <i class="fa fa-edit"></i> Ubah
                </a>
                <a href="{{ url('item-konsinyasi/history-harga-beli/'.$item->kode) }}" class="btn btn-sm btn-primary pull-right" id="btnUbah">
                    <i class="fa fa-history"></i> History
                </a>
                <a href="{{url('item-konsinyasi')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="content invoice">
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="lead">{{ $item->nama }} </p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th style="width:50%">Suplier</th>
                                            <td>
                                                @foreach ($items as $i => $item )
                                                <span class="label label-info">{{ $item->suplier->nama }}</span> 
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="50%">Jenis Item</th>
                                            <td>{{ $item->jenis_item->nama}}</td>
                                        </tr>
                                        <tr>
                                            <th>Stok</th>
                                            <td>{{ $stok->stok }} PCS</td>
                                        </tr>
                                        <tr>
                                            <th>Item Bonus</th>
                                            @if($item->bonus_id==NULL)
                                            <td> Tidak ada bonus </td>
                                            @else
                                            <td> {{ $item->bonus->nama }} </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th>Syarat Bonus</th>
                                            @if($item->bonus_id==NULL)
                                            <td> 0 PCS</td>
                                            @else
                                            <td> {{ $item->syarat_bonus }} </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th>Stok Limit</th>
                                            @if($item->stok_limit==NULL)
                                            <td> 0 PCS</td>
                                            @else
                                            <td> {{ $item->stok_limit }} </td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end of data-item -->
                        <div class="col-xs-6">
                            <p class="lead">Satuan Item </p>
                            <div class="table-responsive" id="formSimpanSatuan">
                                <form class="form-horizontal" action="{{url('/relasi_satuan')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="item_kode" value="{{$item->kode}}">
                                    <div class="form-group col-xs-5">
                                        <select name="satuanID" class="form-control">
                                            <option>Pilih satuan</option>
                                            @foreach($satuans as $satuan)
                                            <option value="{{$satuan->id}}">{{$satuan->kode}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <input type="text" name="konversi" class="form-control" placeholder="Konversi">
                                    </div>
                                    <div class="form-group col-xs-3" style="margin-bottom: 20px">
                                        <button class="btn btn-sm btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit">
                                            <i class="fa fa-save"></i> <span>Tambah</span>
                                        </button>
                                    </div>
                                </form>
                                <div id="formHapusSatuan" style="display: none;">
                                    <form method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="_method" value="delete">
                                    </form>
                                </div>
                                <table class="table table-striped" id="tableSatuan">
                                    <thead>
                                        <tr>
                                            <th>Jenis Satuan</th>
                                            <th>Konversi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <tr id="{{$relasi_satuan->id}}">
                                            <td data-satu="{{$relasi_satuan->satuan_id}}">{{$relasi_satuan->satuan->kode}}</td>
                                            <td class="duit">{{$relasi_satuan->konversi}}</td>
                                            <td>
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_relasi_satuan" title="Ubah Satuan">
                                                    <i class="fa fa-edit"></i> Ubah
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_relasi_satuan" title="Hapus Satuan">
                                                    <i class="fa fa-trash"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="col-md-6">
                <div class="x_title">
                    <h2>Stok {{ $item->nama }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                         <table class="table table-striped" id="StokTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Tanggal Masuk</th>
                                    <th class="text-center">Pemasok</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Tanggal Kadaluarsa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stoks as $stok)
                                <tr id="{{ $stok->id }}">
                                    <td>{{ $stok->created_at->format("d-m-Y") }}</td>
                                    <td>{{ $stok->item->suplier->nama }}</td>
                                    <td style="text-align:center;">{{ $stok->jumlah }}</td>
                                    <td style="text-align: right;"> {{ number_format($stok->harga) }}</td>
                                    <td class="tanggal">{{ $stok->kadaluarsa }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end sisi kiri -->
            <div class="col-md-6">
                <div class="x_title">
                    <h2>Harga</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive" id="formSimpanContainer">
                        <form class="form-horizontal" action="{{url('/harga')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="item_kode" value="{{$item->kode}}">
                            <!-- <input type="hidden" name="status" value="1"> -->
                            <div class="form-group col-xs-4">
                                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah">
                            </div>
                            <div class="form-group col-xs-4">
                                <select name="satuan_id" class="form-control">
                                    <option>Pilih satuan</option>
                                    @foreach($relasi_satuans as $relasi_satuan)
                                    <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->kode}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-4">
                                <input type="text" name="harga-show" class="form-control angka" placeholder="Harga">
                                <input type="text" name="harga" class="form-control hide" placeholder="Harga">
                            </div>
                            <div class="form-group col-xs-12">
                                <button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                        <div id="formHapusContainer" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                            </form>
                        </div>
                        <table class="table table-striped" id="tableHarga">
                            <thead>
                                <tr>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($item->harga as $harga)
                                    @if($harga->status==1)
                                    <tr id="{{$harga->id}}">
                                        <td>{{$harga->jumlah}}</td>
                                        <td name="satuan_id" id="{{$harga->satuan_id}}">{{$harga->satuan->kode}}</td>
                                        <td class="duit">{{$harga->harga}}</td>
                                        <td>
                                            <button class="btn btn-xs btn-warning btnUbah" id="UbahHarga" title="Ubah Harga Jual">
                                                <i class="fa fa-edit"> Ubah</i> 
                                            </button>
                                            <button class="btn btn-xs btn-danger btnHapus" id="hapus_harga" title="Hapus Harga Jual">
                                                <i class="fa fa-trash"> Hapus</i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableHarga').DataTable();
        $('#StokTable').DataTable();

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
        });

        $(document).on('click', '#UbahHarga', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var harga = $tr.find('td').first().next().next().text();
            // console.log(satuan_id);

            $('input[name="jumlah"]').val(jumlah);
            $('select[name="satuan_id"]').val(satuan_id);
            $('input[name="harga-show"]').val(harga);
            $('input[name="harga"]').val(parseInt(harga.replace(/\D/g, ''), 10));

            $('#formSimpanContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id  + '/update');
            $('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#ubah_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $(this).parents('tr').attr('id');
            var satu = $tr.find('td').first().data('satu');
            var konversi = $tr.find('td').first().next().text();
            // console.log(id);

            $('select[name="satuanID"]').val(satu);
            $('input[name="konversi"]').val(konversi);

            $('#formSimpanSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
            $('#formSimpanSatuan').find('input[name="_method"]').val('put');
            $('#btnSimpanSatuan span').text('Ubah');
        });

        $(document).on('click', '#hapus_harga', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id + '/delete');
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });

        $(document).on('click', '#hapus_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $tr.find('td').first().text();
            var konversi = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + satuan_id + ' ' + konversi +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
                    $('#formHapusSatuan').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });

    </script>
@endsection
