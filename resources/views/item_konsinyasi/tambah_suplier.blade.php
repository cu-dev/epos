@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Suplier</title>
@endsection

@section('style')
    <style media="screen">
    	.btnUbah, .btnHapus {
    		margin-bottom: 0;
    	}
        .btnSimpan {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    	#btnUbah, #btnKembali {
    		margin-bottom: 0;
    	}
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6 col-xs-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pemasok Item {{ $item->nama }} </h2>
                <a href="{{url('item-konsinyasi')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                    <div class="row">
                        <form class="form-horizontal" action="{{url('item-konsinyasi/tambah_suplier/'.$item->kode)}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="item_kode" value="{{$item->kode}}">
                                    <div class="form-group col-xs-5">
                                        <select name="suplier" class="form-control select2_single">
                                            <option>Pilih suplier</option>
                                            @foreach($supliers as $suplier)
                                                <option value="{{$suplier->id}}">{{$suplier->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-3" style="margin-bottom: 20px">
                                        <button class="btn btn-sm btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit">
                                            <i class="fa fa-save"></i> <span>Tambah</span>
                                        </button>
                                    </div>
                                </form>
                        <div class="col-xs-12">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Pemasok</th>
                                </thead>
                                <tbody>
                                    @foreach($items as $nums => $item)
                                        <tr>
                                            <td>{{ $nums+1 }}</td>
                                            <td>{{ $item->suplier->nama }}</td>
                                        </tr>   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                            <!-- end of data-item -->
                    </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableHarga').DataTable();
        $('#StokTable').DataTable();
        
        $(document).ready(function() {
            $(".select2_single").select2();
        });

        $(document).on('click', '#UbahHarga', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var harga = $tr.find('td').first().next().next().text();
            console.log(satuan_id);

            $('input[name="jumlah"]').val(jumlah);
            $('select[name="satuan_id"]').val(satuan_id);
            $('input[name="harga-show"]').val(harga);
            $('input[name="harga"]').val(parseInt(harga.replace(/\D/g, ''), 10));            

            $('#formSimpanContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id  + '/update');
            $('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#ubah_relasi_satuan', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $(this).parents('tr').attr('id');
            var satu = $tr.find('td').first().data('satu');
            var konversi = $tr.find('td').first().next().text();
            console.log(id);

            $('select[name="satuanID"]').val(satu);
            $('input[name="konversi"]').val(konversi);

            $('#formSimpanSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
            $('#formSimpanSatuan').find('input[name="_method"]').val('put');
            $('#btnSimpanSatuan span').text('Ubah');
        });

        $(document).on('click', '#hapus_harga', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id + '/delete');
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });

        $(document).on('click', '#hapus_relasi_satuan', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $tr.find('td').first().text();
            var konversi = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);


            swal({
                title: 'Hapus?',
                text: '\"' + satuan_id + ' ' + konversi +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
                    $('#formHapusSatuan').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });
    </script>
@endsection
