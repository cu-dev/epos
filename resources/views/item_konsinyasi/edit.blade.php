@extends('layouts.admin')

@section('title')
    <title>EPOS | Edit Item Konsinyasi</title>
@endsection

@section('style')
    <style media="screen">
    </style>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Item</h2>
				<a href="{{ url('item-konsinyasi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
            	</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('item-konsinyasi/'.$item->kode.'/ubah') }}">
		        	<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="put">
					<input type="hidden" name="konsinyasi" value="1">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Kode Item
								</label>
								<input class="form-control" type="text" id="kode" name="kode" value="{{$item->kode}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Nama Item
								</label>
								<input class="form-control" type="text" id="nama" name="nama" value="{{$item->nama}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jenis Item</label>
								<select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id">
									@foreach($jenis_items as $jenis_item)
									@if($item->jenis_item_id == $jenis_item->id)
										<option value="{{$jenis_item->id}}" selected>{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@else
										<option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="col-md-12form-group">
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> Simpan
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
    	$(document).ready(function() {
			$(".select2_single").select2();
		});

    	// console.log('oi');
	    var url = "{{url('item-konsinyasi')}}";
	    var a = $('a[href="' + url + '"]');
	    // console.log(a.text());
	    a.parent().addClass('current-page');
	    a.parent().parent().show();
	    a.parent().parent().parent().addClass('active');
    });
</script>
@endsection
