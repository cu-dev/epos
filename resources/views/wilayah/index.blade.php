@extends('layouts.admin')

@section('title')
    <title>EPOS | Wilayah</title>
@endsection

@section('style')
    <style media="screen">
        .btnSimpan, .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan,
        .btnHapus {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Desa</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanDesaContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/desa')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-2">
                                     <select name="provinsi" class="form-control select2_single" id="prov_des" style="height: 39px;" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kabupaten" class="form-control select2_single" id="kab_des" style="height: 39px;" required="">
                                        <option value="">Pilih Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kecamatan" class="form-control select2_single" id="kec_des" style="height: 39px;" required="">
                                        <option value="">Pilih Kecamatan</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <input type="text" name="desa" class="form-control" placeholder="Desa" style="height: 39px;" required="">
                                </div>
                                <div class="form-group col-xs-3 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>

                            <div id="desaHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <div class="mdtContainer">
                                    <input type="hidden" name="base_url" value="{{ url('wilayah/mdt1') }}">
                                    <input type="hidden" name="data_per_halaman" value="">
                                    <input type="hidden" name="search_query" value="">
                                    <input type="hidden" name="data_total" value="">
                                    <input type="hidden" name="halaman_sekarang" value="">
                                    <input type="hidden" name="field" value="provinsi_nama">
                                    <input type="hidden" name="order" value="asc">

                                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                                    <table id="tableDesa" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th class="sorting" field="provinsi_nama">No</th>
                                                <th class="sorting" field="provinsi_nama">Provinsi</th>
                                                <th class="sorting" field="kabupaten_nama">Kabupaten</th>
                                                <th class="sorting" field="kecamatan_nama">Kecamatan</th>
                                                <th class="sorting" field="nama">Desa</th>
                                                <th class="sorting" field="user_nama">Operator</th>
                                                <th class="sorting" field="provinsi_nama" style="width: 80px;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                        <div class="col-md-6" id="paginationText"></div>
                                        <div class="col-md-6" style="text-align: right;">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" style="margin: 0;">
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kecamatan</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanKecamatanContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/kecamatan')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-2">
                                     <select name="provinsi" class="form-control select2_single" id="prov_kab" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kabupaten" class="form-control select2_single" id="kabs" required="">
                                        <option value="">Pilih Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" style="height: 39px;" id="camat_" required="">
                                </div>
                                <div class="form-group col-xs-2 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="kecamatanHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <div class="mdtContainer">
                                    <input type="hidden" name="base_url" value="{{ url('wilayah/mdt2') }}">
                                    <input type="hidden" name="data_per_halaman" value="">
                                    <input type="hidden" name="search_query" value="">
                                    <input type="hidden" name="data_total" value="">
                                    <input type="hidden" name="halaman_sekarang" value="">
                                    <input type="hidden" name="field" value="provinsi_nama">
                                    <input type="hidden" name="order" value="asc">

                                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                                    <table id="tableKecamatan" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th class="sorting" field="provinsi_nama">No</th>
                                                <th class="sorting" field="provinsi_nama">Provinsi</th>
                                                <th class="sorting" field="kabupaten_nama">Kabupaten</th>
                                                <th class="sorting" field="nama">Kecamatan</th>
                                                <th class="sorting" field="user_nama">Operator</th>
                                                <th class="sorting" field="provinsi_nama" style="width: 80px;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                        <div class="col-md-6" id="paginationText"></div>
                                        <div class="col-md-6" style="text-align: right;">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" style="margin: 0;">
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kabupaten</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanKabupatenContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/kabupaten')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-4">
                                     <select name="provinsi" class="form-control select2_single" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-4">
                                    <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" style="height: 39px;" required="">
                                </div>
                                <div class="form-group col-xs-4">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="kabupatenHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>
                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <div class="mdtContainer">
                                    <input type="hidden" name="base_url" value="{{ url('wilayah/mdt3') }}">
                                    <input type="hidden" name="data_per_halaman" value="">
                                    <input type="hidden" name="search_query" value="">
                                    <input type="hidden" name="data_total" value="">
                                    <input type="hidden" name="halaman_sekarang" value="">
                                    <input type="hidden" name="field" value="provinsi_nama">
                                    <input type="hidden" name="order" value="asc">

                                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                                    <table id="tableKabupaten" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th class="sorting" field="provinsi_nama">No</th>
                                                <th class="sorting" field="provinsi_nama">Provinsi</th>
                                                <th class="sorting" field="nama">Kabupaten</th>
                                                <th class="sorting" field="user_nama">Operator</th>
                                                <th class="sorting" field="provinsi_nama" style="width: 80px;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                        <div class="col-md-6" id="paginationText"></div>
                                        <div class="col-md-6" style="text-align: right;">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" style="margin: 0;">
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Provinsi</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanProvinsiContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/provinsi')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="col-xs-4">
                                    <input type="text" name="provinsi" class="form-control" placeholder="Provinsi" style="height: 39px;" required="">
                                </div>
                                <div class="col-xs-4 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="provinsiHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <div class="mdtContainer">
                                    <input type="hidden" name="base_url" value="{{ url('wilayah/mdt4') }}">
                                    <input type="hidden" name="data_per_halaman" value="">
                                    <input type="hidden" name="search_query" value="">
                                    <input type="hidden" name="data_total" value="">
                                    <input type="hidden" name="halaman_sekarang" value="">
                                    <input type="hidden" name="field" value="nama">
                                    <input type="hidden" name="order" value="asc">

                                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                                    <table id="tableProvinsi" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th class="sorting" field="nama">No</th>
                                                <th class="sorting" field="nama">Provinsi</th>
                                                <th class="sorting" field="user_nama">Operator</th>
                                                <th class="sorting" field="nama" style="width: 80px;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                        <div class="col-md-6" id="paginationText"></div>
                                        <div class="col-md-6" style="text-align: right;">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination" style="margin: 0;">
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('wilayah/mdt1') }}";
        var mdt2 = "{{ url('wilayah/mdt2') }}";
        var mdt3 = "{{ url('wilayah/mdt3') }}";
        var mdt4 = "{{ url('wilayah/mdt4') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="6" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt3) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="5" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt4) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="4" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var desa = data[i];
                        var buttons = desa.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning btnUbah" id="ubah_desa" data-toggle="tooltip" data-placement="top" title="Ubah Desa">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger btnHapus" id="hapus_desa" data-toggle="tooltip" data-placement="top" title="Hapus Desa">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${desa.desa_id}">
                                    <td>${nomor}</td>
                                    <td data-pro="${desa.provinsi_id}">${desa.provinsi_nama}</td>
                                    <td data-kab="${desa.kabupaten_id}">${desa.kabupaten_nama}</td>
                                    <td data-kec="${desa.kecamatan_id}">${desa.kecamatan_nama}</td>
                                    <td>${desa.desa_nama}</td>
                                    <td>${desa.operator}</td>
                                    <td class="tengah-h">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var kecamatan = data[i];
                        var buttons = kecamatan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning btnUbah" id="ubah_kecamatan" data-toggle="tooltip" data-placement="top" title="Ubah Desa">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger btnHapus" id="hapus_kecamatan" data-toggle="tooltip" data-placement="top" title="Hapus Desa">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${kecamatan.kecamatan_id}">
                                    <td>${nomor}</td>
                                    <td data-pro="${kecamatan.provinsi_id}">${kecamatan.provinsi_nama}</td>
                                    <td data-kab="${kecamatan.kabupaten_id}">${kecamatan.kabupaten_nama}</td>
                                    <td>${kecamatan.kecamatan_nama}</td>
                                    <td>${kecamatan.operator}</td>
                                    <td class="tengah-h">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt3) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var kabupaten = data[i];
                        var buttons = kabupaten.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning btnUbah" id="ubah_kabupaten" data-toggle="tooltip" data-placement="top" title="Ubah Desa">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger btnHapus" id="hapus_kabupaten" data-toggle="tooltip" data-placement="top" title="Hapus Desa">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${kabupaten.kabupaten_id}">
                                    <td>${nomor}</td>
                                    <td data-pro="${kabupaten.provinsi_id}">${kabupaten.provinsi_nama}</td>
                                    <td>${kabupaten.kabupaten_nama}</td>
                                    <td>${kabupaten.operator}</td>
                                    <td class="tengah-h">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt4) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var provinsi = data[i];
                        var buttons = provinsi.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning btnUbah" id="ubah_provinsi" data-toggle="tooltip" data-placement="top" title="Ubah Desa">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger btnHapus" id="hapus_provinsi" data-toggle="tooltip" data-placement="top" title="Hapus Desa">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${provinsi.provinsi_id}">
                                    <td>${nomor}</td>
                                    <td>${provinsi.provinsi_nama}</td>
                                    <td>${provinsi.operator}</td>
                                    <td class="tengah-h">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        // $('#tableDesa').DataTable();
        // $('#tableKecamatan').DataTable();
        // $('#tableProvinsi').DataTable();
        // $('#tableKabupaten').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2();
        });

        var provinsi_id = '';
        var kabupaten_id = '';
        var kabupaten_x = '';
        var kecamatan_id = '';
        var kecamatan_x = '';
        var sata =  '';

        $(document).on('click', '#ubah_provinsi', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi = $tr.find('td').first().next().text();

            $('#SimpanProvinsiContainer').find('input[name="provinsi"]').val(provinsi);
            $('#SimpanProvinsiContainer').find('form').attr('action', '{{ url("wilayah/provinsi") }}' + '/' + id);
            $('#SimpanProvinsiContainer').find('input[name="_method"]').val('put');
            $('#SimpanProvinsiContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_provinsi', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#provinsiHapusContainer').find('form').attr('action', '{{ url("wilayah/provinsi") }}' + '/' + id);
                    $('#provinsiHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('click', '#ubah_kabupaten', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi_id = $tr.find('td').first().next().data('pro');
            var kabupaten = $tr.find('td').first().next().next().text();

            $('#SimpanKabupatenContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            $('#SimpanKabupatenContainer').find('input[name="kabupaten"]').val(kabupaten);
            $('#SimpanKabupatenContainer').find('form').attr('action', '{{ url("wilayah/kabupaten") }}' + '/' + id);
            $('#SimpanKabupatenContainer').find('input[name="_method"]').val('put');
            $('#SimpanKabupatenContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_kabupaten', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#kabupatenHapusContainer').find('form').attr('action', '{{ url("wilayah/kabupaten") }}' + '/' + id);
                    $('#kabupatenHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('click', '#ubah_kecamatan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi_id = $tr.find('td').first().next().data('pro');
            kabupaten_id = $tr.find('td').first().next().next().data('kab');
            var kecamatan = $tr.find('td').first().next().next().next().text();
            // $('#SimpanKabupatenContainer').find('select[name="provinsi"]')
            $('#SimpanKecamatanContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            var url = "{{ url('wilayah') }}" + '/' + provinsi_id + '/kabupaten.json';

            var $select = $('#kabs');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kabupaten_x = data.kabupaten;
                }
            });

            for (var i = 0; i < kabupaten_x.length; i++) {
                var kabupaten = kabupaten_x[i];
                if(kabupaten.id == kabupaten_id){
                    $select.append($('<option value="' + kabupaten.id + '" selected>' + kabupaten.nama + '</option>'));
                }else{
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            }

            $('#SimpanKecamatanContainer').find('input[name="kecamatan"]').val(kecamatan);
            $('#SimpanKecamatanContainer').find('form').attr('action', '{{ url("wilayah/kecamatan") }}' + '/' + id);
            $('#SimpanKecamatanContainer').find('input[name="_method"]').val('put');
            $('#SimpanKecamatanContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('change', '#prov_kab', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';

            var $select = $('#kabs');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            });

            $('#SimpanKecamatanContainer').find('input[name=kecamatan]').val('');
        });

        $(document).on('click', '#hapus_kecamatan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                // console.log(isConfirm);
                if (isConfirm) {
                    // Confirmed
                    $('#kecamatanHapusContainer').find('form').attr('action', '{{ url("wilayah/kecamatan") }}' + '/' + id);
                    $('#kecamatanHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('change', '#prov_des', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var $select = $('#kab_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));
            var $select_kec = $('#kec_des');
                $select_kec.empty();
                $select_kec.append($('<option value="">Pilih kecamatan</option>'));

            $('#SimpanDesaContainer').find('input[name="desa"]').val('');
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';
            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                // console.log(kabupatens);
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    $select.append($('<option value="' + kabupaten.id + '" >' + kabupaten.nama + '</option>'));
                }
            });
        });

        $(document).on('change', '#kab_des', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var $select = $('#kec_des');
                $select.empty();
                $select.append($('<option value="">Pilih kecamatan</option>'));
            $('#SimpanDesaContainer').find('input[name="desa"]').val('');
            var url = "{{ url('wilayah') }}" + '/' + id + '/kecamatan.json';
            $.get(url, function(data) {
                var kecamatans = data.kecamatan;
                // console.log(kecamatans);
                for (var i = 0; i < kecamatans.length; i++) {
                    var kecamatan = kecamatans[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
                }
            });
        });

        $(document).on('click', '#ubah_desa', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            provinsi_id = $tr.find('td').first().next().data('pro');
            kabupaten_id = $tr.find('td').first().next().next().data('kab');
            kecamatan_id = $tr.find('td').first().next().next().next().data('kec');
            var nama = $tr.find('td').first().next().next().next().next().text();

            // console.log(id, provinsi_id, kabupaten_id, kecamatan_id, nama);

            $('#SimpanDesaContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            var url = "{{ url('wilayah') }}" + '/' + provinsi_id + '/kabupaten.json';

            var $select = $('#kab_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kabupaten_x = data.kabupaten;
                }
            });

            for (var i = 0; i < kabupaten_x.length; i++) {
                var kabupaten = kabupaten_x[i];
                if(kabupaten.id == kabupaten_id){
                    $select.append($('<option value="' + kabupaten.id + '" selected>' + kabupaten.nama + '</option>'));
                }else{
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            }

            var url = "{{ url('wilayah') }}" + '/' + kabupaten_id + '/kecamatan.json';

            var $select = $('#kec_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kecamatan</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kecamatan_x = data.kecamatan;
                }
            });

            for (var i = 0; i < kecamatan_x.length; i++) {
                var kecamatan = kecamatan_x[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
            }

            $("select[name='kecamatan']").val(kecamatan_id).trigger("change");
            // console.log(nama);
            $('#SimpanDesaContainer').find('input[name="desa"]').val(nama);
            $('#SimpanDesaContainer').find('form').attr('action', '{{ url("wilayah/desa") }}' + '/' + id);
            $('#SimpanDesaContainer').find('input[name="_method"]').val('put');
            $('#SimpanDesaContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_desa', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                // console.log(isConfirm);
                if (isConfirm) {
                    // Confirmed
                    $('#desaHapusContainer').find('form').attr('action', '{{ url("wilayah/desa") }}' + '/' + id);
                    $('#desaHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

    </script>
@endsection
