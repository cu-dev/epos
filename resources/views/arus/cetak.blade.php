@extends('layouts.print')

@section('app.style')
	<style>
		.speleng {
            padding-right: 5px;
        }
        s
		.garis{
        	border-style: solid;
        	border-width: 2px;
        }

		.kanan{
        	text-align: right;
        }

        .x_content{
        	font-size: 12px;
        	line-height: 120%;
        }

        .text-center{
        	text-align: center
        }

        .text-right{
        	text-align: right
        }

        .gemuk {
        	font-weight: bold;
        }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:841.9pt 595.3pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

	</style>
@endsection

@section('app.body')
	<div class=WordSection1>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><img width=185 height=100 src="{{ URL::to('images/image001.png') }}" align=left hspace=12><b><span style='font-size:14.0pt;line-height:107%; font-family:"Clarendon Blk BT","serif"'>KENCANA MULYA</span></b></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='font-size:14.0pt;line-height:107%;font-family: "Clarendon Blk BT","serif"'>KARANGDUWUR-PETANAHAN</span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Jl. Puring-Petanahan No. 3 Karangduwur, Kec. Petanahan, Kab. Kebumen</span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'><a href="mailto:kencanamulyakarangduwur@gmail.com">kencanamulyakarangduwur@gmail.com</a></span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Telp./Fax (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</span></p>
		
		<p class=MsoFooter>

		<table cellpadding=0 cellspacing=0 align=left>
			 <tr>
		  		<td width=0 height=5></td>
		 	</tr>
		 	<tr>
		  		<td></td>
		  		<td><img width=721 height=5 src="{{ URL::to('images/image002.png') }}"></td>
		 	</tr>
		</table>
		<center style="padding-top: 50px">
			<STRONG>LAPORAN ARUS KAS </STRONG><br/>
			Periode {{ \App\Util::tanggalan($tanggal_cetak) }}<br/><br/>
		</center>
		<table width="80%" role="grid" aria-describedby="demo-dt-basic_info" style="margin-left: 10%; padding-top: 20px">
			<tbody>
				<tr>
					<td width="50%"><strong>Kas Masuk</strong></td>
				</tr>
				<tr>
					<td width="50%"> Modal </td>
					<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($data['Modal']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Pendapatan Lain-lain </td>
					<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($data['PendapatanLain']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Deposito Pelanggan </td>
					<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($data['Deposito']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Penjualan Barang Dagang </td>
					<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($data['JBD']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Penjualan Aset </td>
					<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit2($data['JualAset']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Retur Pembelian </td>
					<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit2($data['ReturPembelian']) }} </td>
				</tr>
				<tr>
					<td width="50%"> Pembayaran Piutang </td>
					<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit2($data['PembayaranPiutang']) }} </td>
				</tr>

				<tr>
					<td width="50%" style="padding-top: 25px"> <strong>Kas Keluar</strong> </td>
				</tr>
				<tr>
					<td width="50%"> Pembelian Aset </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['BeliAset']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Biaya Beban </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['Beban']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Pembelian Barang Dagang </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['PBD']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Piutang Lain-lain </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['PiutangLain']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Penarikan Bagi Hasil </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['BagiHasil']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Prive </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['Prive']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Retur Penjualan </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['ReturPenjualan']) }}) </td>
				</tr>
				<tr>
					<td width="50%"> Kewajiban </td>
					<td style="text-align: right" width="50%"> ({{ \App\Util::duit2($data['Kewajiban']) }}) </td>
				</tr>

				@if($data['kenaikan'] >= 0)
					<tr>
						<td style="padding-top: 25px" width="50%"> Kenaikan Kas </td>
						<td style="text-align: right; padding-top: 25px" width="50%" class="data">{{ \App\Util::duit2($data['kenaikan']) }} </td>
					</tr>
					<tr>
						<td width="50%"> Posisi Kas {{ $sebelum }} </td>
						<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($kas_awal) }} </td>
					</tr>
				@elseif($data['kenaikan'] < 0)
					<tr>
						<td style="padding-top: 25px" width="50%"> Posisi Kas {{ $sebelum }} </td>
						<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($kas_awal) }} </td>
					</tr>
					<tr>
						<td width="50%"> Penurunan Kas </td>
						<td style="text-align: right; padding-top: 25px" width="50%" class="data">({{ \App\Util::duit2($data['kenaikan'] * -1)  }}) </td>
					</tr>
				@endif
				<tr>
					<td width="50%"> Posisi Kas {{ $sebelum }} </td>
					<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($kas_awal) }} </td>
				</tr>
				<tr>
					<td width="50%"> Posisi Kas {{ $tanggal_akhir }} </td>
						@if($kas_akhir > 0)
                            <td style="text-align: right" width="50%" class="data">{{ \App\Util::duit2($kas_akhir) }} </td>
                        @else
                        	<td style="text-align: right" width="50%" class="data">({{ \App\Util::duit2($kas_akhir * -1) }}) </td>
                        @endif
					</td>
				</tr>
			</tbody>
		</table>

		
	</div>

@endsection

@section('app.script')
    <script type="text/javascript">
    	$(document).ready(function() {
			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                	$(el).text(duit);
                }else{
                	// var a = '<span class="invisible">)</span>';
                	$(el).text(duit);
                	// $(el).append(a);
                	// $(el).addClass('speleng');
                	$(el).addClass('speleng');
                }
            });

            // window.print();
		});
    </script>
@endsection