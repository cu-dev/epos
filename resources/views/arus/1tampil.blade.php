@extends('layouts.admin')

@section('title')
    <title>EPOS | Arus Kas</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }

        .gemuk {
        	font-weight: bold;
        }

        .title-hide {
        	display: none;
        }

        @page {
        size: 210cm 297cm;
        margin: 0mm 0mm 0mm 0mm;
	    }

	    @media print {
	    	.make-grid(md);
	    }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Arus Kas per {{ $tanggal_akhir }}</h2>
				<button onclick="PrintElem('#myDiv')" class="btn btn-default action"><i class="fa fa-print"></i></button>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('arus_kas/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-4"><h4><strong>Pilih Data Arus Kas</strong></h4></div>
	    						<div class="col-md-6">
	    							<select name="data" id="data" class="form-control">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-sm btn-success" id="btnUbah" type="submit"  style="margin-top: 5px">
										<span style="color:white">Tampil</span>
									</button>
	    						</div>
	    					</div>
						</form>
    				</div>
    			</div>
    			<div class="row" id="myDiv">
    				<div class="col-md-12">
    					<div class="title-hide">
							<center>
								<STRONG>KENCANA MULYA </STRONG><br/>
								<STRONG>LAPORAN ARUS KAS </STRONG><br/>
								Periode {{ \App\Util::tanggalan($tanggal_cetak) }}<br/><br/>
							</center>
						</div>
						<table class="table table-striped table-bordered dataTable no-footer dtr-inline eksportan" cellspacing="0" width="100%" role="grid" aria-describedby="demo-dt-basic_info" style="width:100%;">
							<tbody>
								<tr>
									<td width="50%">Penjualan</td>
									<td class="text-right" width="50%"></td>
								</tr>
							</tbody>
						</table>

    					<div class="row">
    						<div class="col-md-8">
    							Penjualan
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['penjualan']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Retur Pembelian
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['retur_pembelian']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembayaran Piutang
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['bayar_piutang']) }}
    						</div>
    					</div>

						<div class="row">
    						<div class="col-md-8">
    							Penjualan Aset
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['jual_aset']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Titipan Masuk
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['titipan']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Penambahan Modal
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['modal']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Peminjaman Bank
    						</div>
    						<div class="col-md-4 text-right">
    							{{ \App\Util::duit($data['pinjam_bank']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembelian Barang Dagang
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['pembelian']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembelian Aset
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['pembelian_aset']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembayaran Kewajiban
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['bayar_kewajiban']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Prive
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['prive']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembayaran Asuransi Dimuka
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['asuransi_dimuka']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembayaran Sewa Dimuka
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['sewa_dimuka']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Piutang
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['piutang']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Total Biaya Beban
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['beban']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Pembelian Perlengkapan
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['perlengkapan']) }} )
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8">
    							Penarikan Bagi Hasil
    						</div>
    						<div class="col-md-4 text-right">
    							( {{ \App\Util::duit($data['bagi_hasil']) }} )
    						</div>
    					</div>

    					<div class="row" style="padding-top: 50px">
    						<div class="col-md-8 gemuk">
    							Kenaikan Kas
    						</div>
    						<div class="col-md-4 text-right gemuk">
    							{{ \App\Util::duit($data['kenaikan']) }}
    						</div>
    					</div>

    					<div class="row">
    						<div class="col-md-8 gemuk">
    							Posisi Kas {{ $sebelum }}
    						</div>
    						<div class="col-md-4 text-right gemuk">
    							{{ \App\Util::duit($kas_awal) }}
    						</div>
    					</div>

    					<div class="row">
     						<div class="col-md-8 gemuk">
    							Posisi Kas {{ $tanggal_akhir }}
    						</div>
    						<div class="col-md-4 text-right gemuk">
    							{{ \App\Util::duit($kas_akhir) }}
    						</div>
    					</div>

    				</div>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$("#data").select2({
				maximumSelectionLength: 2
			});
		});

	function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open();
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
	</script>
@endsection
