@extends('layouts.admin')

@section('title')
    <title>EPOS | Arus Kas</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }

        .gemuk {
        	font-weight: bold;
        }

        .speleng {
            padding-right: 14px;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Arus Kas {{ $tanggal_akhir }} </h2>
				<form method="post" action="{{ url('arus_kas/cetak') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<input type="hidden" name="data" value="{{ $date }}">

					<ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>

			        {{-- <button class="btn btn-sm btn-info pull-right" type="submit">
						<i class="fa fa-print"></i><span style="color:white"> Cetak</span>
					</button> --}}
                    <button class="btn btn-sm btn-cetak pull-right" type="submit" data-toggle="tooltip" data-placement="top" title="Cetak Arus Kas">
                        <i class="fa fa-print"></i>
                    </button>
                    <a href="{{ url('arus_kas') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
				</form>

				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('arus_kas/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-4"><h4><strong>Pilih Data Arus Kas</strong></h4></div>
	    						<div class="col-md-6">
	    							<select name="data" id="data" class="form-control" required="">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-success" id="btnUbah" type="submit" style="margin-top: 2px">
                                        <span style="color:white">Tampil</span>
                                    </button>
	    						</div>
	    					</div>
						</form>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-md-12">
    					<div class="row">
    						<div class="col-md-12">
    							<strong>Kas Masuk</strong>
    						</div>
    					</div>

                        <div class="row">
                            <div class="col-md-8">
                                Modal
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['Modal']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Pendapatan Lain-lain
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['PendapatanLain']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Deposito Pelanggan
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['Deposito']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Penjualan Barang Dagang
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['JBD']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Penjualan Aset
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['JualAset']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Retur Pembelian
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['ReturPembelian']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Pembayaran Piutang
                            </div>
                            <div class="col-md-4 text-right data">
                                {{ \App\Util::duit2($data['PembayaranPiutang']) }}
                            </div>
                        </div>

                        <div class="row" style="padding-top: 25px">
                            <div class="col-md-12">
                                <strong>Kas Keluar</strong>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Pembelian Aset
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['BeliAset']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Biaya Beban
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['Beban']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Pembelian Barang Dagang
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['PBD']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Piutang Lain-lain
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['PiutangLain']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Penarikan Bagi Hasil
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['BagiHasil']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Prive   
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['Prive']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Retur Penjualan   
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['ReturPenjualan']) }})
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                Kewajiban
                            </div>
                            <div class="col-md-4 text-right data">
                                ({{ \App\Util::duit2($data['Kewajiban']) }})
                            </div>
                        </div>

    					@if($data['kenaikan'] > 0)
                           <div class="row" style="padding-top: 25px">
                                <div class="col-md-8 gemuk">
                                    Kenaikan Kas
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    {{ \App\Util::duit2($data['kenaikan']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 gemuk">
                                    Posisi Kas {{ $sebelum }}
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    {{ \App\Util::duit2($kas_awal) }}
                                </div>
                            </div>
                        @elseif($data['kenaikan'] < 0)
                           <div class="row" style="padding-top: 25px">
                                <div class="col-md-8 gemuk">
                                    Posisi Kas {{ $sebelum }}
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    {{ \App\Util::duit2($kas_awal) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 gemuk">
                                    Penurunan Kas
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    ({{ \App\Util::duit2($data['kenaikan'] * -1) }})
                                </div>
                            </div>
                        @else
                            <div class="row" style="padding-top: 25px">
                                <div class="col-md-8 gemuk">
                                    Kenaikan Kas
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    {{ \App\Util::duit2($data['kenaikan']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 gemuk">
                                    Posisi Kas {{ $sebelum }}
                                </div>
                                <div class="col-md-4 text-right gemuk data">
                                    {{ \App\Util::duit2($kas_awal) }}
                                </div>
                            </div>
                        @endif

    					{{-- <div class="row">
    						<div class="col-md-8 gemuk">
    							Posisi Kas {{ $sebelum }}
    						</div>
    						<div class="col-md-4 text-right gemuk data">
    							{{ \App\Util::duit2($kas_awal) }}
    						</div>
    					</div> --}}

    					<div class="row">
     						<div class="col-md-8 gemuk">
    							Posisi Kas {{ $tanggal_akhir }}
    						</div>
    						<div class="col-md-4 text-right gemuk data">
    							@if($kas_akhir > 0)
                                    {{ \App\Util::duit2($kas_akhir) }}
                                @else
                                    ({{ \App\Util::duit2($kas_akhir * -1) }})
                                @endif
    						</div>
    					</div>

    				</div>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
            var url = "{{ url('arus_kas') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$("#data").select2({
				maximumSelectionLength: 2
			});

            $('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                    $(el).text(duit);
                }else{
                    // var a = '<span class="invisible">)</span>';
                    $(el).text(duit);
                    $(el).addClass('speleng');
                }
            });
		});
	</script>
@endsection
