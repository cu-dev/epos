@extends('layouts.admin')

@section('title')
    <title>EPOS | Arus Kas</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }

        .kanan{
        	text-align: right;
        }

        .gemuk {
        	font-weight: bold;
        }

        .title-hide {
        	display: none;
        }

        @page {
        size: 210cm 297cm;
        margin: 30mm 30mm 30mm 30mm;
	    }

	    @media print {
	    	.make-grid(md);
	    	table: width="80%", margin-left:50px;
	    }

	    .speleng {
            padding-right: 14px;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Arus Kas per {{ $tanggal_akhir }}</h2>
				<form method="post" action="{{ url('arus_kas/cetak') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<input type="hidden" name="data" value="{{ $date }}">

					<ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>

			        <button class="btn btn-sm btn-info pull-right" type="submit">
						<i class="fa fa-print"></i><span style="color:white">Cetak</span>
					</button>
				</form>

				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('arus_kas/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-4"><h4><strong>Pilih Data Arus Kas</strong></h4></div>
	    						<div class="col-md-6">
	    							<select name="data" id="data" class="form-control" required="">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-sm btn-success" id="btnUbah" type="submit"  style="margin-top: 5px">
										<span style="color:white">Tampil</span>
									</button>
	    						</div>
	    					</div>
						</form>
    				</div>
    			</div>
    			<div class="row" id="myDiv">
    				<div class="col-md-12">
    					<div class="title-hide">
							<center>
								<STRONG>KENCANA MULYA </STRONG><br/>
								<STRONG>LAPORAN ARUS KAS </STRONG><br/>
								Periode {{ \App\Util::tanggalan($tanggal_cetak) }}<br/><br/>
							</center>
						</div>
						<table cellspacing="0" width="100%" role="grid" aria-describedby="demo-dt-basic_info">
							<tbody>
								<tr>
									<td width="50%"><strong>Kas Masuk</strong></td>
								</tr>
								<tr>
									<td width="50%">Modal</td>
									<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit($data['Modal']) }}</td>
								</tr>
								<tr>
									<td width="50%">Pendapatan Lain-lain</td>
									<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit($data['PendapatanLain']) }}</td>
								</tr>
								<tr>
									<td width="50%">Deposito Pelanggan</td>
									<td style="text-align: right" width="50%" class="data">{{ \App\Util::duit($data['Deposito']) }}</td>
								</tr>
								<tr>
									<td width="50%"> Penjualan Barang Dagang </td>
									<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit($data['JBD']) }} </td>
								</tr>
								<tr>
									<td width="50%"> Penjualan Aset </td>
									<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit($data['JualAset']) }} </td>
								</tr>
								<tr>
									<td width="50%"> Retur Pembelian </td>
									<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit($data['ReturPembelian']) }} </td>
								</tr>
								<tr>
									<td width="50%"> Pembayaran Piutang </td>
									<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit($data['PembayaranPiutang']) }} </td>
								</tr>




								<tr>
									<td width="50%" style="padding-top: 25px"> <strong>Kas Keluar</strong> </td>
								</tr>
								<tr>
									<td width="50%"> Pembelian Aset </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['BeliAset']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Biaya Beban </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['Beban']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Pembelian Barang Dagang </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['PBD']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Piutang Lain-lain </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['PiutangLain']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Penarikan Bagi Hasil </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['BagiHasil']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Prive </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['Prive']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Retur Penjualan </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['ReturPenjualan   ']) }} ) </td>
								</tr>
								<tr>
									<td width="50%"> Kewajiban </td>
									<td style="text-align: right" width="50%"> ( {{ \App\Util::duit($data['Kewajiban']) }} ) </td>
								</tr>



								<tr>
									@if($data['kenaikan'] > 0)
										<td style="padding-top: 25px" width="50%"> Kenaikan Kas </td>
										<td style="text-align: right; padding-top: 25px" width="50%" class="data"> 
											{{ \App\Util::duit($data['kenaikan']) }} 
										</td>
									@if($data['kenaikan'] < 0)
									 	<td style="padding-top: 25px" width="50%"> Penurunan Kas </td>
										<td style="text-align: right; padding-top: 25px" width="50%" class="data"> 
											({{ \App\Util::duit($data['kenaikan'] * -1) }} )
										</td>
									@else
										<td style="padding-top: 25px" width="50%"> Penurunan Kas </td>
										<td style="text-align: right; padding-top: 25px" width="50%" class="data"> 
											{{ \App\Util::duit($data['kenaikan']) }}
										</td>
									@endif
								</tr>
								<tr>
									<td width="50%"> Posisi Kas {{ $sebelum }} </td>
									<td style="text-align: right" width="50%" class="data"> {{ \App\Util::duit($kas_awal) }} </td>
								</tr>
								<tr>
									<td width="50%"> Posisi Kas {{ $tanggal_akhir }} </td>
									<td style="text-align: right" width="50%" class="data"> 
										@if($kas_akhir > 0)
		                                    {{ \App\Util::duit($kas_akhir) }}
		                                @else
		                                    ({{ \App\Util::duit($kas_akhir * -1) }})
		                                @endif
									</td>
								</tr>
							</tbody>
						</table>    							

    				</div>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$("#data").select2({
				maximumSelectionLength: 2
			});

			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                    $(el).text(duit);
                }else{
                    // var a = '<span class="invisible">)</span>';
                    $(el).text(duit);
                    $(el).addClass('speleng');
                }
            });
		});

	</script>
@endsection
