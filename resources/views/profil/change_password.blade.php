@extends('layouts.admin')

@section('title')
    <title>EPOS | Profil Saya</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Ubah Kata Sandi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('profil/password') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="put">
					<div class="form-group">
						<label class="control-label">Kata Sandi Lama</label>
						<input class="form-control" type="password" name="password">
					</div>
					<div class="form-group">
						<label class="control-label">Kata Sandi Baru</label>
						<input class="form-control" type="password" name="new_password" id="password">
						<span id='pesan'></span>
					</div>
					<div class="form-group">
						<label class="control-label">Konfirmasi Kata Sandi</label>
						<input class="form-control" type="password" name="new_password_confirmation" id="confirm_password">
						<span id='message'></span>
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-edit"></i> <span>Ubah</span>
						</button>
						<button class="btn btn-sm btn-default" id="btnReset" type="button">
							<i class="fa fa-refresh"></i> Reset
						</button>
					</div>
				</form>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')

	@if (session('unmatch') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konfirmasi kata sandi tidak cocok!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('salah') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Kata Sandi lama salah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#password').on('keyup', function () {
			var pass = $('#password').val();
		    if (pass.length >=6 ) {
		    	$('#pesan').html('Panjang Kata Sandi Cukup').css('color', 'green');
		    }else{
		    	$('#pesan').html('Panjang Kata Sandi Kurang, minimal 6 karakter').css('color', 'red');
		    }
		});		

		$('#confirm_password').on('keyup', function () {
		  	if ($('#password').val() == $('#confirm_password').val()) {
		    	$('#message').html('Kata Sandi Baru Sama').css('color', 'green');
		  	} else 
		    	$('#message').html('Kata Sandi Baru Tidak Sama').css('color', 'red');
		});		
	</script>
@endsection
