@extends('layouts.admin')

@section('title')
    <title>EPOS | Profil Saya</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Profil Pengguna  <small><strong>{{ $user->nama }}</strong></small></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
					<div class="profile_img">
						<div id="crop-avatar">
						<!-- Current avatar -->
							<img class="img-responsive avatar-view" src="{{URL::to('/foto_profil/'.$user->foto)}}" alt="Avatar" title="Change the avatar">
						</div>
					</div>
					<form method="POST" action="{{ url('profil/upload')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
						<div class="form-group">
							<input type="file" name="foto_profil">
						</div>
						<button type="submit" class="btn btn-sm btn-success">
							<i class="fa fa-upload"></i> Unggah
						</button>
					</form>
					<h4>{{ $user->nama }}</h4>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12 profile_left">
					<a href="{{ url('profil/edit') }}" class="btn btn-sm btn-success"><i class="fa fa-edit m-right-xs"></i> Ubah Profil</a>
					<a href="{{ url('profil/password') }}" class="btn btn-sm btn-success"><i class="fa fa-key m-right-xs"></i> Ubah Sandi</a>
					<br/>
					<br/>
					<table class="table table-striped">
						<tr>
							<td>
								<i class="fa fa-user"></i> Nama
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user->nama }}
							</td>
						</tr>
						<tr>
							<td>
								<i class="fa fa-map-marker"></i> Alamat
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user->alamat }}, Indonesia
							</td>
						</tr>
						<tr>
							<td>
								<i class="fa fa-phone"></i> Telepon
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user->telepon }}
							</td>
						</tr>
						<tr>
							<td>
								<i class="fa fa-envelope"></i> Email
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user->email }}
							</td>
						</tr>
						<tr>
							<td>
								<i class="fa fa-calendar"></i> Tanggal Lahir
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user->tanggal_lahir }}
							</td>
						</tr>
					</table>
                </div>
			</div>
		</div>
	</div>
@endsection

@section('script')

	@if (session('sukses') == 'upload')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Foto berhasil diunggah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'upload')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Foto gagal diunggah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Profil berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Profil gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Kata Sandi berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Kata Sandi gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('salah') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Kata Sandi lama salah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('unmatch') == 'password')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Kata Sandi konfirmasi tidak cocok!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif
@endsection
