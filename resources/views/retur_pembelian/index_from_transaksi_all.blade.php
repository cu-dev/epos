@extends('layouts.admin')

@section('title')
    <title>EPOS | Retur Pembelian</title>
@endsection

@section('style')
    <style media="screen">
    	#btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Retur Pembelian</h2>
				<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/create') }}" class="btn btn-sm btn-success pull-right" id="btnUbah">
	                <i class="fa fa-plus"></i> Tambah
	            </a>
	            <a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Kode Retur</th>
							<th>Kode Transaksi</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transaksi_pembelian->returs as $i => $retur)
						<tr id="{{$retur->id}}">
							<td>{{ $i + 1 }}</td>
							<td>{{ $retur->created_at->format('Y-m-d') }}</td>
							<td>{{ $retur->kode_retur }}</td>
							<td>{{ $transaksi_pembelian->kode_transaksi }}</td>
							<td>
								<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/'.$retur->id.'/transaksi') }}" class="btn btn-xs btn-info" id="btnDetail">
									<i class="fa fa-eye"></i> Detail
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$('#tableItemMasuk').DataTable({
				'order': [[1, 'desc']]
			});
		});
	</script>
@endsection
