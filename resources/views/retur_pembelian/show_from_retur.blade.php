@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Retur Transaksi Pembelian</title>
    {{-- yang dipake buat show --}}
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
        .no-border {
            border: none;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-6">
                        <h2 id="formSimpanTitle">Detail Retur Transaksi Pembelian</h2>
                        <span id="kodeReturTitle"></span>
                    </div>
                    <div class="col-md-6 current-page">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                       {{--  <a href="{{ url('retur-pembelian') }}" class="btn btn-sm btn-success pull-right">
                            <i class="fa fa-eye"></i> Cairkan Nominal Kredit
                        </a> --}}
                        <a href="{{ url('retur-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        @if($retur_pembelian->bg_lunas == 0 && $retur_pembelian->bank_bg != null)
                            <a id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right">
                                <!-- <i class="fa fa-money"></i> -->BG
                            </a>
                        @endif
                        @if($retur_pembelian->cek_lunas == 0 && $retur_pembelian->bank_cek != null)
                            <a id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right">
                                <!-- <i class="fa fa-money"></i> -->CEK
                            </a>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <!-- <div class="x_title"> -->
                            <h2>{{ $retur_pembelian->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        <!-- </div> -->
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <tr>
                                    <th>Kode Retur</th>
                                    <td style="width: 60%;">{{ $retur_pembelian->kode_retur }}</td>
                                </tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->kode_transaksi }}</td>
                                </tr>
                                </tr>
                                    <th>Kode Faktur Retur</th>
                                    <td style="width: 60%;">{{ $retur_pembelian->nota }}</td>
                                </tr>
                                <tr>
                                    <th>Pemasok</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Status Retur</th>
                                    <td style="width: 60%;">
                                        @if($retur_pembelian->status == 'sama')
                                            Retur Barang Sama
                                        @elseif($retur_pembelian->status == 'lain')
                                            Retur Barang Lain
                                        @elseif($retur_pembelian->status == 'uang')
                                            Diganti Uang
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $retur_pembelian->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center">Rincian</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @if ($retur_pembelian->status == 'lain')
                                        <th>Sub Total (Keluar)</th>
                                    @else
                                        <th>Sub Total</th>
                                    @endif
                                    @if ($retur_pembelian->harga_total != null && $retur_pembelian->harga_total > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->harga_total) }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>

                                @if ($retur_pembelian->total_uang != null && $retur_pembelian->total_uang > 0 && $retur_pembelian->status == 'uang')
                                    @if ($selisih < 0)
                                        <tr>
                                            <th>Keuntungan Retur</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih * -1) }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Jumlah Uang Diterima</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->total_uang) }}</td>
                                    </tr>
                                    @if ($selisih > 0)
                                        <tr>
                                            <th>Kerugian Retur</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih * -1) }}</td>
                                        </tr>
                                    @endif
                                @endif    
                                
                                @if ($retur_pembelian->status == 'lain')
                                    @if($selisih_keluar_masuk < 0)
                                        <tr>
                                            <th>Kerugian</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih_keluar_masuk * -1) }}</td>
                                        </tr>
                                    @elseif ($selisih_keluar_masuk > 0)
                                        <tr>
                                            <th>Keuntungan</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih_keluar_masuk) }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Sub Total (Masuk)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->total_uang) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        @if($retur_pembelian->status == 'uang' || $retur_pembelian->status == 'lain')

                        <table class="table table-bordered table-striped table-hover">
                            @if($retur_pembelian->status == 'lain' && $retur_pembelian->total_uang - $retur_pembelian->harga_total >0 )
                                {{-- <table class="table table-bordered table-striped table-hover"> --}}
                                    <thead>
                                        <tr>
                                            {{-- <th colspan="2" class="text-center">Nominal Tambahan Pembelian Barang</th> --}}
                                            <th colspan="2" class="text-center">Pembayaran</th>
                                        </tr>
                                    </thead>
                                {{-- </table> --}}
                            @else
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">Pembayaran</th>
                                    </tr>
                                </thead>
                            @endif

                            <tbody>
                                @if ($retur_pembelian->nominal_tunai != null && $retur_pembelian->nominal_tunai > 0)
                                    <tr>
                                        <th>Nominal Tunai</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_tunai) }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_transfer != null && $retur_pembelian->nominal_transfer > 0)
                                    <tr>
                                        <th>Nomor Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->no_transfer }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nominal Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_transfer) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bank Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->bank_returs->nama_bank }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_kartu != null && $retur_pembelian->nominal_kartu > 0)
                                    <tr>
                                        <th>Nomor Transfer Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->no_kartu }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nominal Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_kartu) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bank Transfer Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->kartu_returs->nama_bank }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kartu</th>
                                        <td class="text-right" style="width: 60%; text-transform: capitalize;">{{ $retur_pembelian->jenis_kartu }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_cek != null && $retur_pembelian->nominal_cek > 0)
                                    <tr>
                                        <th>Nomor Cek</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->no_cek }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nominal Cek</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_cek) }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_bg != null && $retur_pembelian->nominal_bg > 0)
                                    <tr>
                                        <th>Nomor BG</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->no_bg }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nominal BG</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_bg) }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_piutang != null && $retur_pembelian->nominal_piutang > 0)
                                    <tr>
                                        <th>Nominal Piutang Suplier</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_piutang) }}</td>
                                    </tr>
                                @endif

                                @if ($retur_pembelian->nominal_hutang != null && $retur_pembelian->nominal_hutang > 0)
                                    {{-- <tr>
                                        <th>Kode Hutang Transaksi</th>
                                        <td class="text-right" style="width: 60%;">{{ $retur_pembelian->hutang_id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th>Nominal Bayar Hutang Transaksi</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_pembelian->nominal_hutang) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @endif
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item Retur</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Total</th>
                                    <th class="text-left">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item_out as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="text-center">
                                        @foreach($hasil[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-right">{{ App\Util::duit($relasi->subtotal) }}</td>
                                    <td>{{ $relasi->keterangan }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if($retur_pembelian->status == 'lain')
                            <div class="line" style="padding-top: 15px; padding-bottom: 25px"> </div>
                            <div class="x_title">
                                <h2>Item Pengganti Retur</h2>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-bordered table-striped table-hover" id="tabel-retur">
                                <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">Nama Item</th>
                                        <th class="text-left">Jumlah</th>
                                        <th class="text-left">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($item_in as $i => $relasi)
                                    <span style="display: none"> {{ $a = $i }} </span>
                                    <tr id="{{ $relasi->id }}">
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $relasi->item->nama }}</td>
                                        <td class="text-center">
                                            @foreach($hasil_in[$a] as $x => $jumlah)
                                                <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                            @endforeach
                                        </td>
                                        <td class="text-right">{{ App\Util::duit($relasi->subtotal) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                        {{-- @if($retur_pembelian->status == 'lain')
                            <div class="line" style="padding-top: 15px; padding-bottom: 25px"> </div>
                            <div class="x_title">
                                <h2>Stok Masuk</h2>
                                <div class="clearfix"></div>
                            </div>
                            <table class="table table-bordered table-striped table-hover" id="tabel-retur-masuk">
                                <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">Nama Item</th>
                                        <th class="text-left">Jumlah</th>
                                        <th class="text-left">Kadaluarsa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stok_masuk as $i => $stok)
                                    <span style="display: none"> {{ $a = $i }} </span>
                                    <tr id="{{ $stok->id }}">
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $stok->item->nama }}</td>
                                        <td class="text-center">
                                            @foreach($stok_in[$a] as $x => $jumlah)
                                                <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                            @endforeach
                                        </td>
                                        <td class="text-right">{{ date('d-m-Y', strtotime($stok->kadaluarsa)) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    @if (session('sukses') == 'cek')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran Cek Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'bg')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran BG Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif
    
    <script type="text/javascript">
        $('#tabel-item').DataTable();
        $('#tabel-retur').DataTable();
        $('#tabel-retur-masuk').DataTable();
        
        var retur_pembelian = '{{ json_encode($retur_pembelian) }}';
        retur_pembelian = retur_pembelian.replace(/&quot;/g, '"');
        retur_pembelian = JSON.parse(retur_pembelian);

        $(document).ready(function() {
            var url = "{{ url('retur-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
        });

        $(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    // console.log('transaksi-pembelian/' + retur_pembelian.id + '/cairkan/cek');
                    location.href = retur_pembelian.id + '/cairkan/cek';
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    // console.log('transaksi-pembelian/' + retur_pembelian.id + '/cairkan/bg');
                    location.href = retur_pembelian.id + '/cairkan/bg';
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
    </script>
@endsection
