@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Retur Pembelian</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnKembali {
			margin-bottom: 0;
		}
		#btnKembali {
			margin-right: 0;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
	</style>
@endsection

@section('content')

<div class="col-md-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<div class="row">
				<div class="col-md-10">
					<h2 id="formSimpanTitle">Detail Retur Pembelian</h2>
					<span id="kodeReturTitle"></span>
				</div>
				<div class="col-md-2">
					<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<section class="content invoice">
				<div class="row">
					<div class="col-xs-6">
						<div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
										<th>Kode Retur</th>
										<td>{{ $retur_pembelian->kode_retur }}</td>
									</tr>
									<tr>
										<th>Kode Transaksi</th>
										<td>{{ $transaksi_pembelian->kode_transaksi }}</td>
									</tr>
								</tbody>
							</table>
						</div>

						<p class="lead">Item</p>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>Nama Item</td>
										<td>Jumlah</td>
										<td>Keterangan</td>
									</tr>
								</thead>
								<tbody>
									@foreach($retur_pembelian->items as $i => $item)
										<tr>
											<td>{{ $item->nama }}</td>
											<td>{{ $item->pivot->jumlah }}</td>
											<td>{{ $item->pivot->keterangan }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-xs-6">
						<p class="lead pull-right">{{ $retur_pembelian->created_at }} </p>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@endsection


@section('script')
	<script type="text/javascript">
		$(window).load(function() {
			var kode_retur = '{{ $retur_pembelian->kode_retur }}';
			$('#kodeReturTitle').text(kode_retur);
		});

		$(document).ready(function() {
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
