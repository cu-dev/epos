@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Retur Pembelian</title>
    {{-- yang dipake buat index --}}
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Retur Pembeliannnn</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Retur</th>
                            <th>Kode Transaksi</th>
                            <th style="width: 100px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($retur_pembelians as $i => $retur)
                        <tr id="{{ $retur->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $retur->created_at->format('d-m-Y H:i:s') }}</td>
                            <td>{{ $retur->kode_retur }}</td>
                            <td>{{ $retur->transaksi_pembelian->kode_transaksi }}</td>
                            <td>
                                <a href="{{ url('retur-pembelian/'.$retur->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($retur->cek_lunas == 0 && $retur->bank_cek != null)
                                    <a class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                        CEK
                                    </a>
                                @endif
                                @if($retur->bg_lunas == 0 && $retur->bank_bg != null)
                                    <a class="btn btn-xs btn-BG" id="btnDetail" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                        BG
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Pembelian berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Transaksi Pembelian gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Pembelian berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Transaksi Pembelian gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Transaksi Pembelian berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Transaksi Pembelian gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableItemMasuk').DataTable({
                'order': [[1, 'desc'], [2, 'desc']]
            });
        });
    </script>
@endsection
