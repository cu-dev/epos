@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Retur Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnRetur, #btnDetail, #btnKembali {
            margin-bottom: 0;
        }
        .full-width {
            width: 100%;
        }
        .feedback {
            background-color : #31B0D5;
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
            border-color: #46b8da;
        }
        #mybutton {
            position: fixed;
            bottom: -4px;
            right: 10px;
        }
        #tabelKeranjang td {
            border: none;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="full-width">Detail Transaksi Pembelian</h2>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                {{-- <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li> --}}
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" id="btnRetur" class="btn btn-sm btn-warning pull-right">
                            <i class="fa fa-sign-in"></i> Lihat Retur
                        </a>
                        <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id) }}" id="btnDetail" class="btn btn-sm btn-info pull-right">
                            <i class="fa fa-eye"></i> Detail Penjualan
                        </a>
                        <a href="{{ url('transaksi-pembelian') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {{-- <div class="row">
                    <div class="col-xs-6">
                        <h4>Detail Transaksi</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Kode Transaksi</th>
                                        <td>{{ $transaksi_pembelian->kode_transaksi }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nama Suplier</th>
                                        <td>{{ $transaksi_pembelian->suplier->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th>Harga Sub Total</th>
                                        <td>{{ \App\Util::ewon($transaksi_pembelian->harga_total) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <h4>Daftar Item</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Nama Item</td>
                                        <td>Jumlah</td>
                                        <td>Total</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($relasi_transaksi_pembelian as $num => $relasi)
                                        <tr>
                                            <td>{{ $relasi->item->nama }}</td>
                                            <td>
                                                @foreach($hasil[$num] as $x => $jumlah)
                                                    <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                                @endforeach
                                            </td>
                                            <td>{{ \App\Util::ewon($relasi->subtotal)}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_pembelian->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Suplier</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Sales</td>
                                    @if ($transaksi_pembelian->seller_id != null)
                                    <td>{{ $transaksi_pembelian->seller->nama }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Tanggal Pembelian</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_pembelian->utang != null && $transaksi_pembelian->utang > 0)
                                <tr>
                                    <th>Hutang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->utang) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_tunai != null && $transaksi_pembelian->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_transfer != null && $transaksi_pembelian->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_transfer) }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_transfers->nama_bank }} [{{ $transaksi_pembelian->bank_transfers->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_kartu != null && $transaksi_pembelian->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_kartu) }}</td>
                                </tr>
                                <tr>
                                    <th>Nominal Transaksi Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->no_kartu) }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->jenis_kartu }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_kartus->nama_bank }} [{{ $transaksi_pembelian->bank_kartus->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_cek != null && $transaksi_pembelian->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">
                                    @if($transaksi_pembelian->aktif_cek == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_cek != null)
                                    <tr>
                                        <th>Bank Cek</th>
                                        <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_ceks->nama_bank }} [{{ $transaksi_pembelian->bank_ceks->no_rekening }}]</td>
                                    </tr>
                                @endif

                                @if ($transaksi_pembelian->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_bg != null && $transaksi_pembelian->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">@if($transaksi_pembelian->aktif_bg == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_bg != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_bgs->nama_bank }} [{{ $transaksi_pembelian->bank_bgs->no_rekening }}]</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Harga Total</th>
                                    @if ($transaksi_pembelian->harga_total != null && $transaksi_pembelian->harga_total > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total) }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Ongkos Kirim</th>
                                    @if ($transaksi_pembelian->ongkos_kirim != null && $transaksi_pembelian->ongkos_kirim > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ongkos_kirim) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Jumlah Bayar Awal</th>
                                    @if ($transaksi_pembelian->jumlah_bayar != null && $transaksi_pembelian->jumlah_bayar > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->jumlah_bayar) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Jumlah Bayar Saat Ini</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->sisa_utang) }}</td>
                                </tr>

                                @if($transaksi_pembelian->sisa_utang>0)
                                    <tr>
                                        <th>Kekurangan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->sisa_utang) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-right">HPP</th>
                                    <th class="text-right">PPN</th>
                                    <th class="text-right">Harga</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_pembelian as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="text-left">
                                        {{-- @foreach($hasil[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        @foreach($hasil[$a] as $x => $jumlah)
                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                            @if ($x != count($hasil[$a]) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 11 * 10) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 11) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="x_title">
                            <h2>Stok</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Kadaluarsa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stoks as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td>
                                        {{-- @foreach($hasil_stoks[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        @foreach($hasil_stoks[$a] as $x => $jumlah)
                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                            @if ($x != count($hasil_stoks[$a]) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ \App\Util::date($relasi->kadaluarsa) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="full-width">Tambah Retur Pembelian</h2>
                {{-- <h2 id="kodeReturTitle" class="full-width" style="font-size: 10px; margin:0"></h2> --}}
                {{-- <span ></span> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option id="default" value="">Pilih Item</option>
                            @foreach($relasi_transaksi_pembelian as $relasi)
                            <option value="{{ $relasi->item->id }}">{{$relasi->item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
            </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo">
                    <thead>
                        <tr>
                            <th class="text-left">Item</th>
                            <th class="text-left">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Retur</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    {{-- <table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;"> --}}
                    <table class="table" id="tabelKeranjang">
                        {{-- <thead>
                            <tr>
                                <th></th>
                                <th>Item</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga per satuan (Rp)</th>
                                <th>Total (Rp)</th>
                            </tr>
                        </thead> --}}
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group col-md-7 col-sm-7 col-xs-7" id="pilihTindakan">
                        <div class="row">
                            <label class="control-label">Tindakan</label>
                            <div class="input-group">
                                <div id="tindakanButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnUang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Uang</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnSama" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Sama</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnLain" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Lain</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="metodePembayaranButtonGroup" >
                            <label class="control-label">Metode Pembayaran</label>
                            <div class="input-group">
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnPiutang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Piutang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputTunaiContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Tunai</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                            </div>
                        </div>
                        <div id="inputTransferBankContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_transfer">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalTransfrer" id="inputNominalTransfrer" class="form-control angka">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputKartuContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_kartu">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Jenis Kartu</label>
                                    <select class="form-control select2_single" name="jenis_kartu">
                                        <option value="">Pilih Kartu</option>
                                        <option value="debet">Kartu Debit</option>
                                        <option value="kredit">Kartu Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transaksi</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Kartu</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputCekContainer" class="form-group row">
                            <div class="line"></div>
                            {{-- <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Cek</label>
                                    <select class="form-control select2_single" name="cek_id">
                                        <option value="">Pilih Cek</option>
                                        @foreach ($ceks as $cek)
                                        <option value="{{ $cek->id }}">{{ $cek->nomor }} [Rp {{ \App\Util::ewon($cek->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor Cek</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control angka" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputBGContainer" class="form-group row">
                            <div class="line"></div>
                            {{-- <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih BG</label>
                                    <select class="form-control select2_single" name="bg_id">
                                        <option value="">Pilih BG</option>
                                        @foreach ($bgs as $bg)
                                        <option value="{{ $bg->id }}">{{ $bg->nomor }} [Rp {{ \App\Util::ewon($bg->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nomor BG</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control angka" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputPiutangContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Piutang</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" name="inputNominalPiutang" id="inputNominalPiutang" class="form-control angka">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2"></div>
                    <div class="form-group col-md-3" id="inputTotali">
                        <label class="control-label">Harga Nilai Barang</label>
                        <div class="input-group" style="margin-bottom: 0;">
                            <div class="input-group-addon">Rp</div>
                            <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group" id="keranjangLain" style="padding-top: 20px">
                <div class="form-group" style="padding: 10px">
                    <div class="line"></div>
                    <h2>Keranjang Retur Barang Lain</h2>
                    <div class="line"></div>
                    <div class="row">
                        <div class="form-group col-md-4 col-xs-4">
                            <label class="control-label">Nama Item Barang Retur</label>
                            <select name="item_retur" class="select2_single form-control">
                                <option value="">Pilih Item</option>w
                                @foreach($item_returs as $retur)
                                    <option value="{{ $retur->id }}">{{$retur->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="x_content">
                        <table class="table" id="tabelKeranjangRetur" style="border-bottom: 1px solid #dfdfdf;">
                            {{-- <thead>
                                <tr>
                                    <th></th>
                                    <th>Item</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga per satuan (Rp)</th>
                                    <th>Input Total (Rp)</th>
                                    <th>Total (Rp)</th>
                                </tr>
                            </thead> --}}
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="formSimpanContainer">
                        <form id="form-simpan" action="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="kode_retur" value="" />
                            <input type="hidden" name="transaksi_pembelian_id" value="{{ $transaksi_pembelian->id }}" />
                            <input type="hidden" name="harga_total" value="0" />
                            <input type="hidden" name="jumlah_bayar" value="0" />
                            <input type="hidden" name="kurang" value="0" />
                            <input type="hidden" name="status" value="sama" />

                            <input type="hidden" name="nominal_tunai" />

                            <input type="hidden" name="no_transfer" />
                            <input type="hidden" name="bank_transfer" />
                            <input type="hidden" name="nominal_transfer" />

                            <input type="hidden" name="no_kartu" />
                            <input type="hidden" name="bank_kartu" />
                            <input type="hidden" name="jenis_kartu" />
                            <input type="hidden" name="nominal_kartu" />

                            <input type="hidden" name="no_cek" />
                            <input type="hidden" name="nominal_cek" />

                            <input type="hidden" name="no_bg" />
                            <input type="hidden" name="nominal_bg" />

                            <input type="hidden" name="nominal_piutang" />

                            <div id="append-section"></div>
                            <div id="append-section-in-metode">
                                <input type="hidden" name="jumlah_bayar_in" />
                                <input type="hidden" name="nominal_tunai_in" />
                                <input type="hidden" name="no_transfer_in" />
                                <input type="hidden" name="bank_transfer_in" />
                                <input type="hidden" name="nominal_transfer_in" />
                                <input type="hidden" name="no_kartu_in" />
                                <input type="hidden" name="bank_kartu_in" />
                                <input type="hidden" name="jenis_kartu_in" />
                                <input type="hidden" name="nominal_kartu_in" />
                                <input type="hidden" name="no_cek_in" />
                                <input type="hidden" name="bank_cek_in" />
                                <input type="hidden" name="nominal_cek_in" />
                                <input type="hidden" name="no_bg_in" />
                                <input type="hidden" name="bank_bg_in" />
                                <input type="hidden" name="nominal_bg_in" />
                                <input type="hidden" name="nominal_titipan_in" />
                            </div>
                            <div id="append-section-in"></div>

                            <div class="clearfix">
                                <div class="col-md-3 pull-right">
                                    <button type="submit" class="btn btn-success" id="submit" disabled style="width: 100%;">
                                        <i class="fa fa-check"></i> OK
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var selected_items = [];
        var selected_retur = [];

        var transaksi_pembelian = null;
        var relasi_transaksi_pembelian = null;

        function isBtnSimpanDisabled() {
            // console.log('isBtnSimpanDisabled');
            if ($('#btnUang').hasClass('btn-success')) {
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
                var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
                var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
                var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
                var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
                var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
                var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
                var nominal_piutang = parseFloat($('input[name="nominal_piutang"]').val());
                // console.log(nominal_tunai, nominal_transfer, nominal_cek, nominal_bg, nominal_kartu, nominal_titipan, nominal_piutang);
                var no_transfer = $('input[name="no_transfer"]').val();
                var bank_transfer = $('input[name="bank_transfer"]').val();
                var no_kartu = $('input[name="no_kartu"]').val();
                var bank_kartu = $('input[name="bank_kartu"]').val();
                var jenis_kartu = $('input[name="jenis_kartu"]').val();
                var no_cek = $('input[name="no_cek"]').val();
                var bank_cek = $('input[name="bank_cek"]').val();
                var no_bg = $('input[name="no_bg"]').val();
                var bank_bg = $('input[name="bank_bg"]').val();
                var piutang_id = $('input[name="piutang_id"]').val();

                if (isNaN(harga_total)) harga_total = 0;
                if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
                if (isNaN(nominal_tunai)) nominal_tunai = 0;
                if (isNaN(nominal_transfer)) nominal_transfer = 0;
                if (isNaN(nominal_kartu)) nominal_kartu = 0;
                if (isNaN(nominal_cek)) nominal_cek = 0;
                if (isNaN(nominal_bg)) nominal_bg = 0;
                if (isNaN(nominal_titipan)) nominal_titipan = 0;
                if (isNaN(nominal_piutang)) nominal_piutang = 0;
                // console.log(nominal_tunai, nominal_titipan);

                if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

                if ($('#btnTransfer').hasClass('btn-warning') && (nominal_transfer <= 0 || no_transfer == '' || bank_transfer == '')) return true;

                if ($('#btnKartu').hasClass('btn-info') && (nominal_kartu <= 0 || no_kartu == '' || bank_kartu == '' || jenis_kartu == '')) return true;

                // if ($('#btnCek').hasClass('btn-success') && (nominal_cek <= 0 || no_cek == '' || bank_cek == '')) return true;

                if ($('#btnCek').hasClass('btn-success')) {
                    var cek_id = $('select[name="cek_id"]').val();
                    if (cek_id == '') {
                        // disabled
                        return true;
                    } else if (cek_id == 'cek_baru' && (nominal_cek <= 0 || no_cek == '' || bank_cek == '')) {
                        // enabled
                        return true;
                    } else if (!isNaN(parseInt(cek_id)) && (nominal_cek <= 0 || no_cek == '')) {
                        return true;
                    } else {
                        // return false;
                    }
                }

                // if ($('#btnBG').hasClass('btn-primary') && (nominal_bg <= 0 || no_bg == '' || bank_bg == '')) return true;

                if ($('#btnBG').hasClass('btn-primary')) {
                    var bg_id = $('select[name="bg_id"]').val();
                    if (bg_id == '') {
                        // disabled
                        return true;
                    } else if (bg_id == 'bg_baru' && (nominal_bg <= 0 || no_bg == '' || bank_bg == '')) {
                        // enabled
                        return true;
                    } else if (!isNaN(parseInt(bg_id)) && (nominal_bg <= 0 || no_bg == '')) {
                        return true;
                    } else {
                        // return false;
                    }
                }

                if ($('#btnTitipan').hasClass('btn-purple') && (nominal_titipan <= 0)) return true;

                if ($('#btnPiutang').hasClass('btn-dark') && (nominal_piutang <= 0 || piutang_id == '')) return true;

                if (nominal_tunai > 0 || nominal_transfer > 0 || nominal_cek > 0 || nominal_bg > 0 || nominal_kartu > 0 || nominal_titipan > 0 || nominal_piutang > 0) {
                    // console.log('oi', jumlah_bayar, harga_total);
                    if (jumlah_bayar != harga_total) return true;
                    return false;
                } else {
                    return true;
                }
            }

            if ($('#btnSama').hasClass('btn-success')) {
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                if (harga_total <= 0) return true;
            }

            if ($('#btnLain').hasClass('btn-success')) {
                // masuk
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                // keluar
                var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
                // masuk
                var jumlah_bayar_in = parseFloat($('input[name="jumlah_bayar_in"]').val());
                // console.log(harga_total, jumlah_bayar, jumlah_bayar_in);
                // boleh
                if (harga_total + jumlah_bayar_in == jumlah_bayar) return false;
                // tidak boleh
                else return true;
                // if (jumlah_bayar != harga_total) return true;
                // return false;
            }

            return false;
        }

        function updateHargaOnKeyup() {
            // console.log('updateHargaOnKeyup');
            // var harga_total = parseFloat($('input[name="harga_total"]').val());
            var status_retur = $('input[name="status"]').val();
            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
            var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
            var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
            var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
            var nominal_piutang = parseFloat($('input[name="nominal_piutang"]').val());
            var nominal_tunai_in = parseFloat($('input[name="nominal_tunai_in"]').val());
            var nominal_transfer_in = parseFloat($('input[name="nominal_transfer_in"]').val());
            var nominal_cek_in = parseFloat($('input[name="nominal_cek_in"]').val());
            var nominal_bg_in = parseFloat($('input[name="nominal_bg_in"]').val());
            var nominal_kartu_in = parseFloat($('input[name="nominal_kartu_in"]').val());
            var nominal_titipan_in = parseFloat($('input[name="nominal_titipan_in"]').val());

            // if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if (isNaN(nominal_tunai_in)) nominal_tunai_in = 0;
            if (isNaN(nominal_transfer_in)) nominal_transfer_in = 0;
            if (isNaN(nominal_cek_in)) nominal_cek_in = 0;
            if (isNaN(nominal_bg_in)) nominal_bg_in = 0;
            if (isNaN(nominal_kartu_in)) nominal_kartu_in = 0;
            if (isNaN(nominal_titipan_in)) nominal_titipan_in = 0;

            var jumlah_bayar = 0;
            var jumlah_bayar_in = 0;
            if (status_retur == 'uang') {
                // console.log(nominal_tunai, nominal_titipan);
                jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kartu + nominal_titipan + nominal_piutang;
            } else if (status_retur == 'lain') {
                // ngitung semua subtotal di append-section-in
                // var subtotal_in = 0;
                $('input[name="subtotal_in[]"]').each(function(index, el) {
                    var temp = $(el).val();
                    if (isNaN(temp)) temp = 0;
                    jumlah_bayar += parseFloat(temp);
                });
                jumlah_bayar_in += nominal_tunai_in + nominal_transfer_in + nominal_cek_in + nominal_bg_in + nominal_kartu_in + nominal_titipan_in;
            }

            // $('input[name="nominal_tunai"]').val(nominal_tunai);
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            $('input[name="nominal_cek"]').val(nominal_cek);
            $('input[name="nominal_bg"]').val(nominal_bg);
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            $('input[name="nominal_titipan"]').val(nominal_titipan);
            $('input[name="nominal_titipan"]').val(nominal_titipan);
            $('input[name="nominal_piutang"]').val(nominal_piutang);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="jumlah_bayar_in"]').val(jumlah_bayar_in);

            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isBtnSimpanDisabled());
        }

        $(document).ready(function() {
            var url = "{{ url('transaksi-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
            // console.log('oi');

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#submit').hide();
            $('#inputTotali').hide();
            $('#pilihTindakan').hide();

            $('input[name="harga_total"]').val(0);
            $('#keranjangLain').hide();
            $('#metodePembayaranButtonGroup').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('');
            $('#inputPiutangContainer').hide();

            $('#btnSama').removeClass('btn-default');
            $('#btnSama').addClass('btn-success');
            // $('#btnSama').find('i').show('fast');

            transaksi_pembelian = '{{ $transaksi_pembelian }}';
            transaksi_pembelian = transaksi_pembelian.replace(/&quot;/g, '"');
            transaksi_pembelian = JSON.parse(transaksi_pembelian);

            relasi_transaksi_pembelian = '{{ $relasi_transaksi_pembelian }}';
            relasi_transaksi_pembelian = relasi_transaksi_pembelian.replace(/&quot;/g, '"');
            relasi_transaksi_pembelian = JSON.parse(relasi_transaksi_pembelian);
        });

        // Buat ambil nomer transaksi terakhir
        /*$(window).on('load', function(event) {
            var url = "{{ url('transaksi-pembelian/retur/last.json') }}";

            $.get(url, function(data) {
                var kode = 1;
                if (data.retur_pembelian !== null) {
                    var kode_retur = data.retur_pembelian.kode_retur;
                    kode = parseInt(kode_retur.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_retur = kode + '/TRABR/' + tanggal;
                $('input[name="kode_retur"]').val(kode_retur);
                $('#kodeReturTitle').text(kode_retur);
            });
        });*/

        function cariItem(kode, url, tr) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;

                var rtp = data.relasi_transaksi_pembelian;
                var harga = parseFloat(rtp.subtotal / rtp.jumlah);

                var stoks = data.stoks;

                $('#submit').show();
                $('#inputTotali').show();
                $('#pilihTindakan').show();

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].id,
                        satuan_id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };
                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.satuan_id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.satuan_id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                var div_jumlah = '';
                var div_harga = '';
                var div_subtotal = '';
                var div_keterangan_rusak = '';
                var input_jumlah_rusak = '';
                var input_satuan_rusak = '';
                var input_konversi_rusak = '';
                var input_harga_rusak = '';
                var input_subtotal_rusak = '';
                var input_keterangan_rusak = '';
                var input_relasi_stok_penjualan = '';
                for (var i = 0; i < stoks.length; i++) {
                    var stok = stoks[i];
                    // console.log(stok);
                    var keterangan = stok.keterangan;
                    if (keterangan == null) keterangan = '';

                    var kadaluarsa = stok.kadaluarsa;
                    if (kadaluarsa == null) kadaluarsa = '00-00-0000';

                    // var harga = parseFloat(stok.harga);
                    // if (isNaN(harga)) harga = 0;
                    // harga *= 1.1;

                    var jumlah = stok.jumlah;
                    // console.log(jumlah);

                    div_jumlah += ''+
                        '<div class="row">'+
                            '<div class="form-group col-md-12 input-grup" jumlah="'+jumlah+'">'+
                                '<div id="stok_id" stok-id="'+stok.id+'" class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        kadaluarsa+
                                    '</div>'+
                                    '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="'+(i==0?1:'')+'" />'+
                                    '<div id="pilihSatuan" class="input-group-btn">'+
                                        '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                        ul_satuan+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    div_harga += ''+
                        '<div class="row">'+
                            '<div class="form-group col-md-12">'+
                                '<div class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        'Rp'+
                                    '</div>'+
                                    '<input type="text" name="inputHagra" id="inputHagra" class="form-control input-sm" value="'+harga.toLocaleString(undefined, {minimumFractionDigits: 2})+'" readonly />'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    div_subtotal += ''+
                        '<div class="row">'+
                            '<div class="form-group col-md-12">'+
                                '<div class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        'Rp'+
                                    '</div>'+
                                    '<input type="text" name="inputSubtotal" id="inputSubtotal" class="form-control input-sm" value="'+(i==0?harga.toLocaleString(undefined, {minimumFractionDigits: 2}):(0).toLocaleString(undefined, {minimumFractionDigits: 2}))+'" stok-id="'+stok.id+'" readonly />'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    div_keterangan_rusak += ''+
                        '<div class="row">'+
                            '<div id="stok_id" stok-id="'+stok.id+'" class="form-group col-md-12">'+
                                '<input type="text" name="inputKeterangan" id="inputKeterangan" class="form-control input-sm" value="'+keterangan+'" />'+
                            '</div>'+
                        '</div>';

                    input_jumlah_rusak += '<input type="hidden" item-id="'+kode+'" name="jumlah_rusak['+kode+'][]" id="jumlah_rusak-'+kode+'-'+stok.id+'" value="'+(i==0?1:0)+'" />';
                    input_satuan_rusak += '<input type="hidden" item-id="'+kode+'" name="satuan_rusak['+kode+'][]" id="satuan_rusak-'+kode+'-'+stok.id+'" value="'+satuan_terkecil.id+'" />';
                    input_konversi_rusak += '<input type="hidden" item-id="'+kode+'" name="konversi_rusak['+kode+'][]" id="konversi_rusak-'+kode+'-'+stok.id+'" value="'+satuan_terkecil.konversi+'" />';
                    input_harga_rusak += '<input type="hidden" item-id="'+kode+'" name="harga_rusak['+kode+'][]" id="harga_rusak-'+kode+'-'+stok.id+'" value="'+harga.toFixed(2)+'" />';
                    input_subtotal_rusak += '<input type="hidden" item-id="'+kode+'" name="subtotal_rusak['+kode+'][]" id="subtotal_rusak-'+kode+'-'+stok.id+'" value="'+(i==0?harga.toFixed(2):(0).toFixed(2))+'" />';
                    input_keterangan_rusak += '<input type="hidden" item-id="'+kode+'" name="keterangan_rusak['+kode+'][]" id="keterangan_rusak-'+kode+'-'+stok.id+'" value="'+keterangan+'" />';
                    input_relasi_stok_penjualan += '<input type="hidden" item-id="'+kode+'" name="relasi_stok_penjualan['+kode+'][]" id="relasi_stok_penjualan-'+kode+'-'+stok.id+'" value="'+stok.id+'" />';
                }

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item-id="'+kode+'" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append(input_jumlah_rusak);
                    $('#form-simpan').find('#append-section').append(input_satuan_rusak);
                    $('#form-simpan').find('#append-section').append(input_konversi_rusak);
                    $('#form-simpan').find('#append-section').append(input_harga_rusak);
                    $('#form-simpan').find('#append-section').append(input_subtotal_rusak);
                    $('#form-simpan').find('#append-section').append(input_keterangan_rusak);
                    $('#form-simpan').find('#append-section').append(input_relasi_stok_penjualan);

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td style="width: 50px;">'+
                                '<h3><i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: #c9302c; padding-top: 8px;"></h3></i>'+
                            '</td>'+
                            '<td><h3><small>'+item.kode+'</small></h3><h3>'+nama+'</h3></td>'+
                            '<td style="width: 250px;">'+
                                '<label class="control-label">Jumlah</label>'+
                                div_jumlah+
                            '</td>'+
                            '<td style="width: 150px;">'+
                                '<label class="control-label">Harga/Satuan</label>'+
                                div_harga+
                            '</td>'+
                            '<td style="width: 150px;">'+
                                '<label class="control-label">Subtotal</label>'+
                                div_subtotal+
                            '</td>'+
                            '<td style="width: 200px;">'+
                                '<label class="control-label">Keterangan</label>'+
                                div_keterangan_rusak+
                            '</td>'+
                        '</tr>');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                // console.log(satuan_item);
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr);

                var harga_total = $('input[name="harga_total"]').val();
                var total = parseFloat(harga_total) + parseFloat(harga);

                $('#inputHargaTotal').val((total).toLocaleString(undefined, {minimumFractionDigits: 2}));
                $('#formSimpanContainer').find('input[name="harga_total"]').val(parseFloat(total).toFixed(2));
            });
        }

        function cariItemRetur(kode, url, tr) {
            $.get(url, function(data) {
                console.log(url, data);
                var pcs = data.pcs;
                var item = data.item;
                var nama = item.nama;
                var item_kode = item.kode;

                var satuan_item = [];
                var satuan_terkecil = null;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].id,
                        satuan_id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);

                    if (i == item.satuan_pembelians.length - 1) satuan_terkecil = satuan;
                }

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="item_kode_in[]" id="itemIn-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="jumlah_in[]" id="jumlahIn-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="satuan_id_in[]" id="satuanIn-' +kode+'" value="'+ satuan_item[0].satuan_id +'" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="harga_in[]" id="hargaIn-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="subtotal_in[]" id="subtotalIn-'+kode+'" class="subtotal" />');

                    var divs = '<div class="form-group">'+
                                    '<label class="control-label">Jumlah</label>';

                    if (satuan_item.length > 0) {
                        for (var i = 0; i < satuan_item.length; i++) {
                            var satuan = satuan_item[i];
                            divs += '<div class="input-group text-center">' +
                                        '<input type="text" id="inputJumlahItemIn" name="inputJumlahItemIn" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'"">' +
                                        '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+satuan.kode+'</div>' +
                                    '</div>';
                        }
                        divs += '</div>';
                    } else {
                        divs += '<div class="input-group">' +
                                    '<input type="text" id="inputJumlahItemIn" name="inputJumlahItemIn" class="form-control input-sm angka" konversi="'+1+'">' +
                                    '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+pcs.kode+'</div>' +
                                '</div>' +
                            '</div>';
                    }

                    var tr = '<tr data-id="' + kode + '">'+
                                '<input type="hidden" name="jumlah">'+
                                '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                '<td><h3><small>'+item_kode+'</small></h3><h3>'+nama+'</h3>'+
                                    `<div class="form-group" id="anak-kadaluarsa-` + kode + `" jkal="0">
                                            <label class="control-label">Kadaluarsa</label>
                                            <i class="fa fa-plus" id="tambah_kadaluarsa" style="color: green; cursor: pointer; margin-left: 10px;"></i>
                                        </div>`+
                                '</td>'+
                                '<td style="width: 200px;">'+
                                    divs+
                                '</td>'+
                                '<td style="width: 200px;">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Total</label>'+
                                        '<div class="input-group">'+
                                            '<div class="input-group-addon">Rp</div>' +
                                            '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Harga</label>'+
                                        '<div class="input-group">'+
                                            '<div class="input-group-addon">Rp</div>' +
                                            '<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                                '<td style="width: 200px;">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">HPP</label>'+
                                        '<div class="input-group">'+
                                            '<div class="input-group-addon">Rp</div>' +
                                            '<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">PPN</label>'+
                                        '<div class="input-group">'+
                                            '<div class="input-group-addon">Rp</div>' +
                                            '<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="">'+
                                        '</div>'+
                                    '</div>'+
                                '</td>'+
                            '</tr>';

                    $('#tabelKeranjangRetur').find('tbody').append($(tr));
                    $('#tabelKeranjangRetur tr[data-id="'+kode+'"]').find('#tambah_kadaluarsa').trigger('click');

                    /*$('#tabelKeranjangRetur').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang retur" id="removeIn" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="padding-top: 13px">'+nama+'</td>'+
                            '<td>'+
                                '<input type="text" name="inputJumlahItemIn" id="inputJumlahItemIn" class="form-control input-sm angka" value="1" />'+
                            '</td>'+
                            '<td>'+ pilihan_satuan +'</td>'+
                            '<td>'+
                                '<input type="text" name="inputHargaPerSatuanIn" id="inputHargaPerSatuanIn" class="form-control text-right input-sm" readonly="readonly" />'+
                            '</td>'+
                            '<td>'+
                                '<input type="text" name="inputSubTotalIn" class="form-control text-right input-sm inputSubTotalIn" value="0,000"/>'+
                            '</td>'+
                            '<td>'+
                                '<input type="text" name="SubTotalIn" class="form-control text-right input-sm SubTotalIn" readonly="readonly" value="0,000"/>'+
                            '</td>'+
                        '</tr>');*/
                }
            });
        }

        function deleteMe( arr, me ) {
           var i = arr.length;
           while( i-- ) if(arr[i] == me ) arr.splice(i,1);
        }

        function removeSelectedItem(item) {
            var index = selected_items.indexOf(item);
            if (index > -1) {
                selected_items.splice(index, 1);
            }
        }

        function removeSelectedRetur(item) {
            var index = selected_retur.indexOf(item);
            if (index > -1) {
                selected_retur.splice(index, 1);
            }
        }

        function uangClose() {
            // $('#inputTunaiContainer').hide();
            // $('#inputTransferBankContainer').hide();
            // $('#inputTransferBankContainer').find('input').val('');
            // $('#inputCekContainer').hide();
            // $('#inputCekContainer').find('input').val('');
            // $('#inputBGContainer').hide();
            // $('#inputBGContainer').find('input').val('');
            // $('#inputKartuContainer').hide();
            // $('#inputKartuContainer').find('input').val('');
            // $('#inputPiutangContainer').hide();

            // $('#btnTunai').removeClass('btn-danger');
            // $('#btnTunai').addClass('btn-default');
            // $('#btnTunai').find('i').hide('fast');
            if ($('#btnTunai').hasClass('btn-danger')) $('#btnTunai').trigger('click');

            // $('#btnTransfer').removeClass('btn-warning');
            // $('#btnTransfer').addClass('btn-default');
            // $('#btnTransfer').find('i').hide('fast');
            if ($('#btnTransfer').hasClass('btn-warning')) $('#btnTransfer').trigger('click');

            // $('#btnBG').removeClass('btn-primary');
            // $('#btnBG').addClass('btn-default');
            // $('#btnBG').find('i').hide('fast');
            if ($('#btnBG').hasClass('btn-primary')) $('#btnBG').trigger('click');

            // $('#btnCek').removeClass('btn-success');
            // $('#btnCek').addClass('btn-default');
            // $('#btnCek').find('i').hide('fast');
            if ($('#btnCek').hasClass('btn-success')) $('#btnCek').trigger('click');

            // $('#btnKartu').removeClass('btn-info');
            // $('#btnKartu').addClass('btn-default');
            // $('#btnKartu').find('i').hide('fast');
            if ($('#btnKartu').hasClass('btn-info')) $('#btnKartu').trigger('click');

            // $('#btnPiutang').removeClass('btn-purple');
            // $('#btnPiutang').addClass('btn-default');
            // $('#btnPiutang').find('i').hide('fast');
            if ($('#btnPiutang').hasClass('btn-purple')) $('#btnPiutang').trigger('click');

            // $('#inputNominalTunai').val('');
            // $('#inputNominalTransfrer').val('');
            // $('#inputNominalCek').val('');
            // $('#inputNominalBG').val('');
            // $('#inputNominalKartu').val('');
            // $('#inputNominalPiutang').val('');
            // $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_transfer"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_transfer"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="jenis_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_cek"]').val('');
            // // $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_cek"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_bg"]').val('');
            // // $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_bg"]').val(0);
            // $('#formSimpanContainer').find('input[name="nominal_piutang"]').val(0);
        }

        function lainClose() {
            selected_retur = [];
            $('select[name="item_retur"]').val('').trigger('change');
            $('#tabelKeranjangRetur').find('tbody tr').remove();
            $('#form-simpan').find('#append-section-in').find('input').remove();

            $('#inputTunaiContainerIn').hide();
            // $('#inputTransferBankContainerIn').find('select').val('').trigger('change');
            // $('#inputTransferBankContainerIn').find('input').val('');
            $('#inputTransferBankContainerIn').hide();
            // $('#inputKartuContainerIn').find('select').val('').trigger('change');
            // $('#inputKartuContainerIn').find('input').val('');
            $('#inputKartuContainerIn').hide();
            // $('#inputCekContainerIn').find('select').val('').trigger('change');
            // $('#inputCekContainerIn').find('input').val('');
            $('#inputCekContainerIn').hide();
            // $('#inputBGContainerIn').find('select').val('').trigger('change');
            // $('#inputBGContainerIn').find('input').val('');
            $('#inputBGContainerIn').hide();
            // $('#inputPiutangContainer').find('input').val('');
            $('#inputPiutangContainer').hide();

            // $('#btnTunaiIn').removeClass('btn-danger');
            // $('#btnTunaiIn').addClass('btn-default');
            // $('#btnTunaiIn').find('i').hide('fast');
            if ($('#btnTunaiIn').hasClass('btn-danger')) $('#btnTunaiIn').trigger('click');

            // $('#btnTransferIn').removeClass('btn-warning');
            // $('#btnTransferIn').addClass('btn-default');
            // $('#btnTransferIn').find('i').hide('fast');
            if ($('#btnTransferIn').hasClass('btn-warning')) $('#btnTransferIn').trigger('click');

            // $('#btnKartuIn').removeClass('btn-info');
            // $('#btnKartuIn').addClass('btn-default');
            // $('#btnKartuIn').find('i').hide('fast');
            if ($('#btnKartuIn').hasClass('btn-info')) $('#btnKartuIn').trigger('click');

            // $('#btnCekIn').removeClass('btn-success');
            // $('#btnCekIn').addClass('btn-default');
            // $('#btnCekIn').find('i').hide('fast');
            if ($('#btnCekIn').hasClass('btn-success')) $('#btnCekIn').trigger('click');

            // $('#btnBGIn').removeClass('btn-primary');
            // $('#btnBGIn').addClass('btn-default');
            // $('#btnBGIn').find('i').hide('fast');
            if ($('#btnBGIn').hasClass('btn-primary')) $('#btnBGIn').trigger('click');

            // $('#btnPiutangIn').removeClass('btn-purple');
            // $('#btnPiutangIn').addClass('btn-default');
            // $('#btnPiutangIn').find('i').hide('fast');
            if ($('#btnPiutangIn').hasClass('btn-purple')) $('#btnPiutangIn').trigger('click');

            // $('#inputNominalTunaiIn').val('');
            // $('#inputNominalTransferIn').val('');
            // $('#inputNominalCekIn').val('');
            // $('#inputNominalBGIn').val('');
            // $('#inputNominalKartuIn').val('');
            // $('#inputNominalTitipanIn').val('');

            // $('input[name="nominal_tunai_in"]').val(0);
            // $('input[name="no_transfer_in"]').val('');
            // $('input[name="bank_transfer_in"]').val('');
            // $('input[name="nominal_transfer_in"]').val(0);
            // $('input[name="no_kartu_in"]').val('');
            // $('input[name="bank_kartu_in"]').val('');
            // $('input[name="jenis_kartu_in"]').val('');
            // $('input[name="nominal_kartu_in"]').val(0);
            // $('input[name="no_cek_in"]').val('');
            // $('input[name="nominal_cek_in"]').val(0);
            // $('input[name="no_bg_in"]').val('');
            // $('input[name="nominal_bg_in"]').val(0);
            // $('input[name="nominal_titipan_in"]').val(0);
        }

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var transaksi = {{ $transaksi_pembelian->id }};
            var kode = $(this).val();
            var url = "{{ url('retur-pembelian') }}"+'/'+kode+'/item/'+ transaksi +'/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                if (kode) {
                    // kode_ = 'item-'+kode;
                    selected_items.push(kode);
                    cariItem(kode, url, tr);
                }
            }
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var item_id = $(this).parents('tr').first().data('id');
            var stok_id = parseFloat($(this).parents('#stok_id').attr('stok-id'));
            // console.log(item_id, stok_id);
            var jumlah = parseFloat($(this).val());
            var konversi = parseFloat($('#konversi_rusak-'+item_id+'-'+stok_id).val());
            // jumlah *= konversi;

            var harga_rusak = parseFloat($('#harga_rusak-'+item_id+'-'+stok_id).val());
            var subtotal = harga_rusak * jumlah * konversi;
            $(this).parents('tr').first().find('#inputSubtotal[stok-id="'+stok_id+'"]').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

            $('#jumlah_rusak-'+item_id+'-'+stok_id).val(jumlah);
            $('#subtotal_rusak-'+item_id+'-'+stok_id).val(subtotal);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#pilihSatuan li', function(event) {
            event.preventDefault();

            var item_id = $(this).parents('tr').first().data('id');
            var stok_id = parseFloat($(this).parents('#stok_id').attr('stok-id'));
            // console.log(item_id, stok_id);
            var jumlah = parseFloat($('#jumlah_rusak-'+item_id+'-'+stok_id).val());
            var kode = $(this).find('a').attr('kode');
            var satuan = parseFloat($(this).find('a').attr('id'));
            var konversi = parseFloat($(this).find('a').attr('konversi'));
            // jumlah *= konversi;

            var harga_rusak = parseFloat($('#harga_rusak-'+item_id+'-'+stok_id).val());
            var subtotal = harga_rusak * jumlah * konversi;
            $(this).parents('tr').first().find('#inputSubtotal[stok-id="'+stok_id+'"]').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

            $(this).parents('#pilihSatuan').find('button').find('.text').text(kode+' ');

            $('#satuan_rusak-'+item_id+'-'+stok_id).val(satuan);
            $('#konversi_rusak-'+item_id+'-'+stok_id).val(konversi);
            $('#subtotal_rusak-'+item_id+'-'+stok_id).val(subtotal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputKeterangan', function(event) {
            event.preventDefault();

            var item_id = $(this).parents('tr').first().data('id');
            var stok_id = parseFloat($(this).parents('#stok_id').attr('stok-id'));
            // console.log(item_id, stok_id);
            var keterangan = $(this).val();

            $('#keterangan_rusak-'+item_id+'-'+stok_id).val(keterangan);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var item_id = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+item_id+'"]').remove();

            // var subtotal = parseFloat($('input[name="subtotal_rusak['+item_id+'][]"]').val());
            var subtotal = 0;
            $('input[name="subtotal_rusak['+item_id+'][]"]').each(function(index, el) {
                var temp_subtotal = parseFloat($(el).val());
                if (isNaN(temp_subtotal)) temp_subtotal = 0;
                subtotal += temp_subtotal;
            });
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            // console.log(subtotal);

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(harga_total)) harga_total = 0;

            harga_total -= subtotal;
            $('input[name="harga_total"]').val(harga_total);
            $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));

            // $('#form-simpan').find('#append-section').find('input[id*=In-'+item_id+']').remove();
            $('input[item-id="'+item_id+'"]').remove();
            $('select[name="item_retur"]').val('').trigger('change');

            removeSelectedItem(item_id+'');
            updateHargaOnKeyup();

            // var kode = $(this).parents('tr').data('id');
            // var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            // var subtotal = $('#subtotal-'+kode).val();
            // var harga_total = $('input[name="harga_total"]').val();
            // // parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            // // console.log(kode);

            // harga_total -= subtotal;
            // // kode_ = 'item-'+kode;
            // deleteMe(selected_items, kode);

            // $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 3}));

            // $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(3));

            // tr.remove();
            // $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });

        $(document).on('click', '#btnUang', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('uang');
            // $('#submit').prop('disabled', true);

            $('#metodePembayaranButtonGroup').show();

            if ($('#btnSama').hasClass('btn-success')) {
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
            } else if ($('#btnLain').hasClass('btn-success')) {
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
            }

            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnSama', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('sama');
            // $('#submit').prop('disabled', false);

            if ($('#btnUang').hasClass('btn-success')) {
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
                $('#metodePembayaranButtonGroup').hide();
                uangClose();
            } else if($('#btnLain').hasClass('btn-success')) {
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
            }

            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnLain', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('lain');
            // $('#submit').prop('disabled', true);

            lainClose();
            $('#keranjangLain').show();

            if ($('#btnSama').hasClass('btn-success')) {
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
            } else if ($('#btnUang').hasClass('btn-success')) {
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
                $('#metodePembayaranButtonGroup').hide();
                uangClose();
            }

            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                // $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                // $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('input[name="no_transfer"]').val('');
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');

                    $(this).find('select[name="bank_transfer"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                // $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="no_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');

                    $(this).find('select[name="bank_kartu"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                // $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="bank_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');

                    $(this).find('select[name="cek_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    $(this).find('p').hide();
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                // $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="bank_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');

                    $(this).find('select[name="bg_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    $(this).find('p').hide();
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnPiutang', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                // $(this).find('i').show('fast');
                $('#inputPiutangContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputPiutangContainer').hide('fast', function() {
                    $('input[name="nominal_piutang"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        // INPUT TUNAI
        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        // INPUT TRANSFER
        $(document).on('change', 'select[name="bank_transfer"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            $('input[name="bank_transfer"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            console.log(no_transfer);
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT KARTU
        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT CEK
        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            $('input[name="nominal_cek"]').val(nominal_cek);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT BG
        $(document).on('keyup', '#inputNoBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            $('input[name="nominal_bg"]').val(nominal_bg);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT PIUTANG
        $(document).on('keyup', '#inputNominalPiutang', function(event) {
            event.preventDefault();

            var nominal_piutang = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            $('input[name="nominal_piutang"]').val(nominal_piutang);
            // total_uang();
            updateHargaOnKeyup();

            // var nominal_piutang = parseFloat($(this).val().replace(/\D/g, ''), 10);
            // var piutang_max = parseFloat($('input[name="piutang_max"]').val());

            // if (isNaN(nominal_piutang)) nominal_piutang = 0;
            // if (isNaN(piutang_max)) piutang_max = 0;

            // if (nominal_piutang <= piutang_max) {
            //     // boleh
            //     $(this).parents('.input-group').removeClass('has-error');
            //     $('input[name="nominal_piutang"]').val(nominal_piutang);
            //     // total_uang();
            //     // $('#submit').prop('disabled', isBtnSimpanDisabled());
            //     updateHargaOnKeyup();
            // } else {
            //     // tidak boleh
            //     $(this).parents('.input-group').addClass('has-error');
            // }

            // // var harga_total = parseFloat($('input[name="harga_total"]').val());
            // // var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            // // var kurang = harga_total - jumlah_bayar;
        });

        $(document).on('change', 'select[name="item_retur"]', function(event) {
            event.preventDefault();

            var kode = $(this).val();
            var url  = "{{ url('retur-pembelian') }}"+'/'+kode+'/item/json';
            var tr   = $('#tabelKeranjangRetur').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (!selected_retur.includes(kode)) {
                if (kode) {
                    // kode_ = 'item-'+kode;
                    selected_retur.push(kode);
                    cariItemRetur(kode, url, tr);
                }
            }
        });

        $(document).on('click', '#tambah_kadaluarsa', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // var satuan = [[],[]];
            var satuan = [];
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                // console.log(index);
                var konversi = parseInt($(el).attr('konversi'));
                var kode_satuan = $(el).next().text();
                satuan.push({'konversi' : konversi, 'kode' : kode_satuan});
            });
            var satuan_pilihan = '';
            for (var i = 0; i < satuan.length; i++) {
                satuan_pilihan = satuan_pilihan+`<div class="row">
                            <div class="col-md-12">
                                <div class="input-group text-center">
                                    <input id="inputJumlahExp" name="inputJumlahExp" class="pull-right form-control input-sm angka" konversi="`+satuan[i]['konversi']+`"  type="text">
                                    <div class="input-group-addon" style="width: 60px; text-align: right;">`+satuan[i]['kode']+`
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }

            var jumlah_kadaluarsa = 0;
            $('#anak-kadaluarsa-'+id+' .m-kadal').each(function(index, el) {
                jumlah_kadaluarsa++;
            });
            jumlah_kadaluarsa++;

            $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" />');
            $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-'+jumlah_kadaluarsa+'" value="' + id + '" />');
            $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="hpp-stok-' + id + '" />');
            $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="jumlah-stok-' + id + '"/>');

            $('#anak-kadaluarsa-'+id).append(`
                <div class="line"></div>
                    <div class="row kol-kadal m-kadal" kol-kadal="`+jumlah_kadaluarsa+`">
                        <div class="col-md-12">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Jumlah
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Kadaluarsa
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0;">
                                `+satuan_pilihan+`
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input name="checkKadaluarsa" id="checkKadaluarsa" type="checkbox">
                                    </span>
                                    <input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok" disabled="" value="" type="text">
                                </div>
                            </div>
                        </div>
                    </div>`);
        });

        $(document).on('keyup', '#inputJumlahItemIn', function(event) {
            event.preventDefault();

            // item_id
            var kode = $(this).parents('tr').first().data('id');
            // var sub_total = $('#satuanIn-'+kode).val();

            var jumlah = parseFloat($(this).val());
            var konversi = parseFloat($(this).attr('konversi'));
            // temp_jumlah_in = jumlah;

            if (isNaN(jumlah)) jumlah = 0;
            if (isNaN(konversi)) konversi = 0;

            var kode   = $(this).parents('tr').first().data('id');
            var satuan = $('#satuanIn-'+kode).val();
            var url    = "{{ url('retur-pembelian') }}"+'/'+kode+'/konversi/json/'+satuan;
            console.log(url);
            var tr     = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');
            $('#jumlahIn-'+kode).val(jumlah);

            $.get(url, function(data) {
                if (data.satuan === null) {
                    // tr.find('#inputHargaPerSatuan').val(0);
                    // tr.find('#inputSubTotal').val(0);
                    // tr.find('#HargaPerSatuan').val(0);
                    // tr.find('#SubTotal').val(0);
                } else {
                    var transaksi = {{ $transaksi_pembelian->id }}
                    var konversi = data.satuan.konversi;
                    var count = parseFloat(jumlah) * parseFloat(data.satuan.konversi); 
                    var url_ = "{{ url('retur-pembelian') }}"+'/'+kode+'/count/'+count+'/json/'+transaksi;
                    console.log(url_);

                    $.get(url_, function(data) {
                        var harga_ = parseFloat(data.harga_satuan).toFixed(3) * 1.1;
                        var harga = parseFloat(harga_).toFixed(3);
                        var harga_satuan = parseFloat(harga) * parseFloat(konversi);
                        var subtotal_ = parseFloat(harga) * parseFloat(data.jumlah);
                        var subtotal = parseFloat(subtotal_).toFixed(3);

                        tr.find('#inputHargaPerSatuan').val(harga_satuan.toLocaleString(undefined, {minimumFractionDigits: 3}));
                        tr.find('#inputSubTotal').val(parseFloat(subtotal).toLocaleString(undefined, {minimumFractionDigits: 3}));
                        tr.find('#HargaPerSatuan').val(harga);
                        tr.find('#SubTotal').val(parseFloat(subtotal).toFixed(3));

                        $('#harga-'+kode).val(harga);
                        $('#subtotal-'+kode).val(parseFloat(subtotal).toFixed(3));

                        $('.subtotal').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        $('input[name="inputHargaTotal"]').val(parseFloat(harga_total.toFixed(3)).toLocaleString(undefined, {minimumFractionDigits: 3}));
                        $('input[name="harga_total"]').val(parseFloat(harga_total.toFixed(3)));
                    });
                }
            });
        });

        $(document).on('keyup', '.inputSubTotalIn', function(event) {
            event.preventDefault();

            var nominal_t = $(this).val().split(',');
            var nominal_0 = parseFloat(nominal_t[0].replace(/\D/g, ''), 10);
            var nominal = parseFloat(nominal_0 +'.' + nominal_t[1]);
            $(this).next().val(nominal);
            
            var nilais = $(this).val().split(',');
            var nilai = 0;
            if(nilais.length > 1){
                nilai = $(this).val();
                if(nilais[0].length < 1){
                    nilais[0] = 0;
                    $(this).val(nilais[0] + ',' + nilais[1]);
                }
                else if(nilais[1].length > 3){
                    nilai = $(this).val().slice(0,-1);
                    $(this).val(nilai);
                
                }else if(nilais[1].length < 3){
                    nilai = parseFloat(nilai.replace(',', '.'));
                    nilai = nilai.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
                    nilai = nilai.replace('.', '');
                    $(this).val(nilai);
                }
            }else{
                nilai = $(this).val();
                var bel_koma = nilai.substr(nilai.length -3);
                var depan_koma = nilai.substr(0, nilai.length -3);

                $(this).val(depan_koma + ',' + bel_koma);
            }
            var nilai_h = parseFloat(nilai.replace(',', '.'));
            var nilai_v = nilai_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
            
            $(this).parents('tr').first().find('input[name="SubTotalIn"]').val('Rp'+nilai_v);
            var jumlah = $(this).parents('tr').first().find('#inputJumlahItemIn').val();
            var satuan = $(this).parents('tr').first().find('#SatuanIn').val();
            var kode = $(this).parents('tr').first().attr('data-id');
            console.log(jumlah, satuan, kode, nilai_h);
            // cekDK();
            var url  = "{{ url('retur-pembelian') }}"+'/'+kode+'/item/'+jumlah+'/jumlah/'+satuan+'/satuan/'+nilai_h;
            console.log(url)
            $.get(url, function(data) {
                
            });
            // hitung_harga(url);
        });

        $(document).on('click', '#removeIn', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabelKeranjangRetur').find('tr[data-id="'+kode+'"]');
            // var subtotal     = $('#subtotal-'+kode).val();
            // var harga_total  = $('input[name="harga_total"]').val();

            // harga_total -= subtotal;
            // // kode_ = 'item-'+kode;
            deleteMe(selected_retur, kode);

            // $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 3}));

            // $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(3));

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=In-'+kode+']').remove();
        });

    </script>
@endsection
