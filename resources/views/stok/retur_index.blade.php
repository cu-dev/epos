@extends('layouts.admin')

@section('title')
	<title>EPOS | Rekap Retur Penjualan</title>
@endsection

@section('style')
	<style type="text/css">
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Retur Penjualan</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-bordered table-striped table-hover" id="tableReturPenjualan" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Kode Retur</th>
							<th>Kode Transaksi</th>
							<th>Kasir</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($retur_penjualan as $i => $retur)
						<tr data-id="{{ $retur->id }}">
							<td>{{ ++$i }}</td>
							{{-- <td>{{ $retur->created_at->format('d-m-Y H:i:s') }}</td> --}}
							<td>{{ $retur->created_at->format('d-m-Y') }}</td>
							<td>{{ $retur->kode_retur }}</td>
							<td>{{ $retur->transaksi_penjualan->kode_transaksi }}</td>
							<td>{{ $retur->user->nama }}</td>
							<td>
								<a href="{{ url('transaksi/'.$retur->transaksi_penjualan->id.'/retur/'.$retur->id) }}" class="btn btn-xs btn-info">
									<i class="fa fa-eye"></i> Detail
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tableReturPenjualan').DataTable({
				'order': [[1, 'desc']]
			});
		});
	</script>
@endsection
