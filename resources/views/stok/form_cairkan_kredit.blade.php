@extends('layouts.admin')

@section('title')
    <title>EPOS | Form Cairkan Kredit</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Form Cairkan Kredit</h2>
                <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id) }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <form method="post" action="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/cairkan-kredit') }}">
                            <div class="row">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Kode Transaksi</label>
                                    <div class="input-group">
                                        <div class="input-group-addon" style="padding: 0 17px;">#</div>
                                        <input class="form-control" type="text" name="kode_transaksi" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Nominal</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input class="form-control" type="text" name="nominal_kredit" readonly="">
                                    </div>
                                </div>
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Keterangan</label>
                                    <div class="input-group">
                                        <div class="input-group-addon" style="padding: 0 17px;">#</div>
                                        <input type="hidden" name="tujuan" value="tunai">
                                        <input class="form-control" type="text" name="keterangan" readonly="">
                                    </div>
                                </div>
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Kas Tujuan</label>
                                    <div id="kasTujuanButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa"></i> Transfer</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                            <label class="control-label">Pilih Bank</label>
                                            <select class="form-control select2_single" name="bank_id">
                                                <option value="">Pilih Bank</option>
                                                @foreach ($banks as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0; margin-top: 10px;">
                                    <div class="line"></div>
                                    <div class="form-group" style="margin-bottom: 0; margin-top: 10px;">
                                        <button type="submit" name="btnSimpan" id="btnSimpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var transaksi_grosir = null;
        var keterangan = '';

        function isButtonSimpanDisabled() {
            var bank_id = $('select[name="bank_id"]').val();
            // console.log(bank_id);
            if ($('#btnTransfer').hasClass('btn-success') && (bank_id == '')) return true;

            return false;
        }

        $(document).ready(function() {
            $(".select2_single").select2();

            transaksi_grosir = '{{ $transaksi_grosir }}';
            transaksi_grosir = transaksi_grosir.replace(/&quot;/g, '"');
            transaksi_grosir = JSON.parse(transaksi_grosir);

            var kode_transaksi = transaksi_grosir.kode_transaksi;
            var nominal_kredit = transaksi_grosir.nominal_kredit;
            nominal_kredit = parseFloat(nominal_kredit.replace(/\D/g, ''), 10) / 100;

            $('input[name="kode_transaksi"]').val(kode_transaksi);
            $('input[name="nominal_kredit"]').val(nominal_kredit.toLocaleString(undefined, {minimumFractionDigits: 2}));
            $('input[name="keterangan"]').val(keterangan);

            $('#btnTunai').trigger('click');
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnTransfer').removeClass('btn-success');
            $('#btnTransfer').addClass('btn-default');

            keterangan = 'Pencairan Penjualan Kredit';

            $('#inputTransferBankContainer').hide('fast');
            $('input[name="tujuan"]').val('tunai');
            $('input[name="keterangan"]').val(keterangan);
            $('select[name="bank_id"]').val('');

            $('#btnSimpan').prop('disabled', isButtonSimpanDisabled());
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#inputTransferBankContainer').show('fast');
            $('input[name="tujuan"]').val('bank');
            $('input[name="keterangan"]').val('');

            $('#btnSimpan').prop('disabled', isButtonSimpanDisabled());
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();
            var id = $(this).val();

            var bank_text = $(this).select2('data')[0].text;
            keterangan = 'Pencairan Penjualan Kredit - ' + bank_text;
            
            $('input[name="bank_id"]').val(id);
            $('input[name="keterangan"]').val(keterangan);

            $('#btnSimpan').prop('disabled', isButtonSimpanDisabled());
        });

    </script>
@endsection
