@extends('layouts.admin')

@section('title')
    <title>EPOS | Edit Konsinyasi</title>
@endsection

@section('style')
    <style media="screen">
    </style>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Item</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('konsinyasi/'.$konsinyasi->id) }}">
		        	<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="put">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Kode Item
								</label>
								<input class="form-control" type="text" id="kode" name="kode" value="{{$konsinyasi->kode}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Nama Item
								</label>
								<input class="form-control" type="text" id="nama" name="nama" value="{{$konsinyasi->nama}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jenis Item</label>
								<select class="form-control" id="jenis_item_id" name="jenis_item_id">
									@foreach($jenis_items as $jenis_item)
									@if($konsinyasi->jenis_item_id == $jenis_item->id)
									<option value="{{$jenis_item->id}}" selected>{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@else
									<option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Satuan Item</label>
								<select class="form-control" id="satuan_id" name="satuan_id">
									<option>Pilih satuan item</option>
									@foreach($satuans as $satuan)
									@if($konsinyasi->satuan_id == $satuan->id)
									<option value="{{$satuan->id}}" selected>{{ $satuan->kode }} : {{ $satuan->nama }}</option>
									@else
									<option value="{{$satuan->id}}">{{ $satuan->kode }} : {{ $satuan->nama }}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Konversi</label>
								<input class="form-control" type="text" id="konversi" name="konversi" value="{{$konsinyasi->konversi}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Harga Dasar</label>
								<input class="form-control" type="text" id="harga_dasar" name="harga_dasar" value="{{$konsinyasi->harga_dasar}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jumlah Barang</label>
								<input class="form-control" type="text" id="stok" name="stok" value="{{$konsinyasi->stok}}">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="col-md-12form-group">
								<a href="{{url('konsinyasi')}}" class="btn btn-sm btn-default" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                					<i class="fa fa-long-arrow-left"></i>
								</a>
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> Simpan
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
    	// console.log('oi');
	    var url = "{{url('konsinyasi')}}";
	    var a = $('a[href="' + url + '"]');
	    // console.log(a.text());
	    a.parent().addClass('current-page');
	    a.parent().parent().show();
	    a.parent().parent().parent().addClass('active');
    });
</script>
@endsection
