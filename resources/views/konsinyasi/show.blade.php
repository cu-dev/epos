@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Item</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    	#btnUbah, #btnKembali {
    		margin-bottom: 0;
    	}
    </style>
@endsection

@section('content')

<div class="col-md-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Detail Item</h2>
            <a href="{{ url('konsinyasi/edit/'.$konsinyasi->id) }}" class="btn btn-sm btn-primary pull-right" id="btnUbah">
                <i class="fa fa-edit"></i> Ubah
            </a>
            <a href="{{url('konsinyasi')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                <i class="fa fa-long-arrow-left"></i>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <section class="content invoice">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="lead">{{ $konsinyasi->nama }} </p>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Jenis Item</th>
                                        <td>{{ $konsinyasi->jenis_item->nama}}</td>
                                    </tr>
                                    <tr>
                                        <th>Satuan</th>
                                        <td>{{ $konsinyasi->satuan->kode }}</td>
                                    </tr>
                                    <tr>
                                        <th>Konversi</th>
                                        <td>{{ $konsinyasi->konversi }}</td>
                                    </tr>
                                    <tr>
                                        <th>Harga Dasar</th>
                                        <td>{{ $konsinyasi->harga_dasar }}</td>
                                    </tr>
                                    <tr>
                                        <th>Stok</th>
                                        <td>{{ $konsinyasi->stok }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <p class="lead">Harga Item </p>
                        <div class="table-responsive" id="formSimpanContainer">
                            <form class="form-horizontal" action="{{url('/harga')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="item_id" value="{{$konsinyasi->id}}">
                                <div class="form-group col-xs-4">
                                    <input type="text" name="jumlah" class="form-control" placeholder="Jumlah">
                                </div>
                                <div class="form-group col-xs-4">
                                    <select name="satuan_id" class="form-control">
                                        <option>Pilih satuan</option>
                                        @foreach($satuans as $satuan)
                                        <option value="{{$satuan->id}}">{{$satuan->kode}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-4">
                                    <input type="text" name="harga" class="form-control" placeholder="Harga">
                                </div>
                                <div class="form-group col-xs-12">
                                    <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="formHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>
                            <table class="table table-striped" id="tableHarga">
                                <thead>
                                    <tr>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($konsinyasi->harga as $harga)
                                    <tr id="{{$harga->id}}">
                                        <td>{{$harga->jumlah}}</td>
                                        <td name="satuan_id" id="{{$harga->satuan_id}}">{{$harga->satuan->kode}}</td>
                                        <td>{{$harga->harga}}</td>
                                        <td>
                                            <button class="btn btn-xs btn-warning" id="btnUbah">
                                                <i class="fa fa-edit"></i> Ubah
                                            </button>
                                            <button class="btn btn-xs btn-danger" id="btnHapus">
                                                <i class="fa fa-trash"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Harga gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Harga gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        // $('#tableHarga').DataTable();

        $(document).on('click', '#btnUbah', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var harga = $tr.find('td').first().next().next().text();

            $('input[name="jumlah"]').val(jumlah);
            $('select[name="satuan_id"]').val(satuan_id);
            $('input[name="harga"]').val(harga);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');
        });
        
        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id);
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });
    </script>
@endsection