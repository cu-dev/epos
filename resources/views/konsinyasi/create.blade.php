@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Item Konsinyasi</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Item Konsinyasi</h2>
				<a href="{{ url('konsinyasi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="POST" action="{{ url('konsinyasi') }}">
		        	{!! csrf_field() !!}
					<input type="hidden" name="id">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Kode Item
								</label>
								<input class="form-control" type="text" id="kode" name="kode">
							</div>
						</div>

						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Nama Item
								</label>
								<input class="form-control" type="text" id="nama" name="nama">
							</div>
						</div>

						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jenis Item</label>
								<select class="select2_single form-control" id="jenis_item_id" name="jenis_item_id">
									<option>Pilih jenis item</option>
									@foreach($jenis_items as $jenis_item)
									<option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Satuan Item</label>
								<select class="select2_single form-control" id="satuan_id" name="satuan_id">
									<option>Pilih satuan item</option>
									@foreach($satuans as $satuan)
									<option value="{{$satuan->id}}">{{ $satuan->kode }} : {{ $satuan->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Konversi</label>
								<input class="form-control" type="text" id="konversi" name="konversi">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Harga Dasar</label>
								<input class="form-control" type="text" id="harga_dasar" name="harga_dasar">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jumlah Barang</label>
								<input class="form-control" type="text" id="stok" name="stok">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="col-md-12form-group">
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> Simpan
								</button>
								{{-- <button class="btn btn-sm btn-default" id="btnReset" type="button">
									<i class="fa fa-refresh"></i> Reset
								</button> --}}
							</div>
						</div>
					</div>
				</form>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		var url = "{{ url('konsinyasi') }}";
		var a = $('a[href="' + url + '"]');
		a.parent().addClass('current-page');
		a.parent().parent().show();
		a.parent().parent().parent().addClass('active');

		$(".select2_single").select2();
	});
	// $(document).on('click', '#btnReset', function() {
	// 	$('input[name="nama"]').val('');
	// 	$('input[name="jenis_item_id"]').val('');
	// 	$('input[name="konversi"]').val('');
	// 	$('input[name="harga_dasar"]').val('');
	// 	$('input[name="stok"]').val('');

	// 	$('#formSimpanContainer').find('form').attr('action', '{{ url("item") }}');
	// 	$('#formSimpanContainer').find('input[name="_method"]').val('post');
	// 	$('#btnSimpan span').text('Tambah');

	// 	$('#formSimpanTitle').text('Tambah Jenis Item');
	// });
</script>
@endsection
