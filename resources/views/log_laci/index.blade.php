@extends('layouts.admin')

@section('title')
	<title>EPOS | Riwayat Laci</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
		/*.debet {
			background: #f9f9f9;
		}
		.kredit {
			padding-left: 30px;
		}*/
		table > thead > tr > th {
			text-align: center;
		}
		table > tbody > tr > td.tengah-hv{
			vertical-align: middle;
			text-align: center;	
			background: white;
		}
	</style>
@endsection

@section('content')
<div class="col-md-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Riwayat Perpindahan Uang di Laci Hari Ini ({{ $today->format('d-m-Y') }})</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_title col-md-12" id="formPilihan">
			<form method="post" action="{{ url('log_laci/tampil') }}" class="form-horizontal">
				<div class="row">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label" style="padding-bottom: 15px">Pilih Rentang Data</label>
						<fieldset>
						<div class="row">
							<div class="control-group col-md-3">
								<div class="controls">
									<div class="input-prepend input-group">
										<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
										<input type="text" style="width: 100%" name="data" id="rentang_tanggal" class="form-control active tanggal-putih" readonly="" required="">
										<input type="hidden" name="awal">
										<input type="hidden" name="akhir">
									</div>
								</div>
							</div>
							<div class="control-group col-md-8">
								@foreach($option_array as $i => $list)
									<label class="checkbox-inline"><input name="user[]" type="checkbox" value="{{ $list['value'] }}" id="{{ $list['id'] }}" class="{{ $list['class'] }}">{{ $list['text'] }}</label>
								@endforeach
								<input name="user[]" type="hidden" id="user_add">
							</div>
							<div class="form-group col-lg-1">
								<button class="btn btn-success" id="btnUbah" type="submit">
									<span style="color:white">Tampil</span>
								</button>
							</div>
						</div>
						</fieldset>
					</div>
				</div>
			</form>
		</div>

		<div class="x_content">
			<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableLog">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th width="25%">Pengirim</th>
						<th width="25%">Penerima</th>
						<th width="10%">Nominal</th>
						<th width="20%">Keterangan</th>
						<th width="15%">Tanggal</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($logs as $i => $log)
						@if($log->pengirim == NULL && in_array($log->penerima, $owner_array))
							<tr>
								<td>{{ $i+1 }}</td>
								@if($log->status == 5)
									<td>Deposito Pelanggan</td>
								@elseif($log->status == 8)
									<td>Pembeli (Eceran)</td>
								@elseif($log->status == 9)
									<td>Pembeli (Grosir)</td>
								@elseif($log->status == 9)
									<td>Pembayaran Piutang Dagang Pelanggan</td>
								@elseif($log->status == 11)
									<td>Retur Pembelian (Uang Masuk)</td>
								@elseif($log->status == 15)
									<td>Retur Penjualan (Uang Masuk)</td>
								@else
									<td>-</td>
								@endif
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->pengirim, $owner_array) && $log->penerima == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $log->sender->level->nama }} ({{ $log->sender->nama }})</td>
								@if($log->status == 3)
									<td>Setoran Tunai Bank</td>
								@elseif($log->status == 6)
									<td>Pembayaran Beban</td>
								@elseif($log->status == 7)
									<td>Transaksi Pembelian</td>
								@elseif($log->status == 16)
									<td>Pembayaran Transaksi Konsinyasi</td>
								@elseif($log->status == 11)
									<td>Retur Pembelian (Uang Keluar)</td>
								@elseif($log->status == 15)
									<td>Retur Penjualan (Uang Keluar)</td>
								@else
									<td>-</td>
								@endif
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->pengirim, $grosir_array) && $log->penerima == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $log->sender->level->nama }} ({{ $log->sender->nama }})</td>
								@if($log->status == 3)
									<td>Setoran Tunai Bank</td>
								@elseif($log->status == 11)
									<td>Retur Pembelian (Uang Keluar)</td>
								@else
									<td>-</td>
								@endif
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->penerima, $grosir_array) && $log->Pengirim == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								@if($log->status == 5)
									<td>Deposito Pelanggan</td>
								@elseif($log->status == 9)
									<td>Transaksi Penjualan Grosir</td>
								@elseif($log->status == 17)
									<td>Piutang Dagang</td>
								@elseif($log->status == 15)
									<td>Retur Penjualan (Uang Masuk)</td>
								@else
									<td>-</td>
								@endif
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->penerima, $eceran_array) && $log->Pengirim == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								@if($log->status == 8)
									<td>Transaksi Penjualan Eceran</td>
								@elseif($log->status == 15)
									<td>Retur Penjualan (Uang Keluar)</td>
								@else
									<td>-</td>
								@endif
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->penerima, $eceran_array) && $log->Pengirim == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								@if($log->status == 15)
									<td>Retur Penjualan (Uang Masuk)</td>
								@else
									<td>-</td>
								@endif
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->pengirim, $gudang_array) && $log->penerima == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $log->sender->level->nama }} ({{ $log->sender->nama }})</td>
								@if($log->status == 6)
									<td>Pembayaran Beban</td>
								@elseif($log->status == 7)
									<td>Transaksi Pembelian</td>
								@elseif($log->status == 16)
									<td>Pembayaran Transaksi Konsinyasi</td>
								@elseif($log->status == 11)
									<td>Retur Pembelian (Uang Keluar)</td>
								@else
									<td>-</td>
								@endif
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@elseif(in_array($log->penerima, $gudang_array) && $log->Pengirim == NULL)
								<tr>
								<td>{{ $i+1 }}</td>
								@if($log->status == 11)
									<td>Retur Pembelian (Uang Masuk)</td>
								@else
									<td>-</td>
								@endif
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@else
							<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $log->sender->level->nama }} ({{ $log->sender->nama }})</td>
								<td>{{ $log->receiver->level->nama }} ({{ $log->receiver->nama }})</td>
								<td class="text-right">{{ \App\Util::ewon($log->nominal) }}</td>
								<td>{{ $log->keterangan }}</td>
								<td>{{ $log->created_at->format('d-m-Y h:i') }}</td>
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection


@section('script')
  <script type="text/javascript">
    $('#tableLog').DataTable();
    $(document).ready(function() {
  //       $('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
		// 	var mulai = (start.toISOString()).substring(0,10);
		// 	var akhir = (end.toISOString()).substring(0,10);
		// 	$('#formPilihan').find('input[name="awal"]').val(mulai);
		// 	$('#formPilihan').find('input[name="akhir"]').val(akhir);
		// });

		$('#rentang_tanggal').daterangepicker({
            autoApply: true,
            calender_style: "picker_2",
            showDropdowns: true,
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
            singleDatePicker: false
        }, 
        function(start, end, label) {
            var mulai = (start.toISOString()).substring(0,10);
            var akhir = (end.toISOString()).substring(0,10);
            $('#formPilihan').find('input[name="awal"]').val(mulai);
            $('#formPilihan').find('input[name="akhir"]').val(akhir);
        });
    });

    $(document).on('change', '#CheckBoxList', function(event) {
        event.preventDefault();

        var checked = $('#CheckBoxList').prop('checked');
        if (checked) {
        	$('#user_add').val('2');
        }else{
        	$('#user_add').val('');
        }
    });

    $(document).on('change', 'input[name="user[]"]', function(event) {
        event.preventDefault();

        var user = $(this).val();
        // console.log(user);
        if(user == 6){
        	var checked = $(this).prop('checked');
	        if (checked) {
	        	console.log('aw');
	        	$(".penjualan").each(function(index, el) {
	                $(el).prop('checked', true);;
	            });
	        }
        }
    });
  </script>
@endsection
