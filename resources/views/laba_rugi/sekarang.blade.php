@extends('layouts.admin')

@section('title')
	<title>EPOS | Laba Rugi</title>
@endsection

@section('style')
	<style media="screen">
		#btnCetak, #btnKembali {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
		
		table > thead > tr > th {
			text-align: center;
		}

		table > tbody > tr > td.tengah-hv{
			vertical-align: middle;
			text-align: center;	
			background: white;
		}

		table > tbody > tr > td.atasan{
			font-weight: bold;
			/*text-decoration: underline;*/
		}

		table > tbody > tr > td.bawahan{
			border-bottom: 1px solid;
		}		

		table > tbody > tr > td.jumlah{
			font-weight: bold;
			border-top: 1px solid;
		}

		table > tbody > tr > td.money{
			text-align: right;
		}		
	</style>
@endsection

@section('content')
<div class="col-md-9 col-xs-12">
	<div class="x_panel">
		<div class="x_title" style="padding-right: 0;">
            <div class="row">
                <div class="col-md-8">
                    <h2 id="header_tanggal">Laporan Laba Rugi Hari Ini ({{ \App\Util::tanggal($akhir_->format('d m Y')) }})</h2>
                </div>
                <div class="col-md-4 pull-right">
                    <form method="post" action="{{ url('laba_rugi/cetak') }}" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="awal" value="{{ $awal }}">
                        <input type="hidden" name="akhir" value="{{ $akhir_end }}">
                        <input type="hidden" name="newAkhir" value="{{ $newAkhir }}">
                        {{-- <button class="btn btn-sm btn-info pull-right" type="submit">
                            <i class="fa fa-print"></i><span style="color:white"> Cetak</span>
                        </button> --}}
                        <button class="btn btn-sm btn-cetak pull-right" type="submit" id="btnCetak" data-toggle="tooltip" data-placement="top" title="Cetak Laba Rugi">
                            <i class="fa fa-print"></i>
                        </button>
                        <a href="{{ url('laba_rugi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </form>
                </div>
            </div>
			<div class="clearfix"></div>
		</div>
		<div class="x_title col-md-12" id="formPilihan">
            <div class="row">
                <div class="form-group col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" style="padding-bottom: 15px">Pilih Rentang Data </label>
                        </div>
                    </div>
                    <div class="row">
                        <fieldset>
                            <form method="post" action="{{ url('laba_rugi/tampil') }}" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="control-group col-md-7">
                                    <div class="controls">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                            <input type="text" style="width: 100%" name="data" id="rentang_tanggal" class="form-control active tanggal-putih" readonly="" required="">
                                            <input type="hidden" name="awal">
                                            <input type="hidden" name="akhir">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 pull-right" style="padding-left: 50px">
                                    <a href="{{ url('laba_rugi/tampil/sekarang') }}" class="btn btn-xs btn-primary pull-right" id="btnBayar" style="height: 30px; width: 30px" data-toggle="tooltip" data-placement="top" title="Tampilkan data hari ini">
                                        <i class="fa fa-calendar-o" style="padding-top: 7px"></i> 
                                    </a>
                                </div>
                                <div class="form-group col-lg-2 pull-left">
                                    <button class="btn btn-success" id="btnUbah" type="submit">
                                        <span style="color:white">Tampil</span>
                                    </button>
                                </div>
                                
                            </form>
                        </fieldset>
                    </div>
                </div>
            </div>
		</div>

		<div class="x_content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <table class="" style="margin-bottom: 0;" width="100%">
                        <tbody>
                            <tr>
                                <td class="atasan" width="60%">Pendapatan Penjualan</td>
                                <td class="money" width="20%"></td>
                                <td class="money" width="20%"></td>
                            </tr>
                            <tr>
                                <td>Penjualan</td>
                                <td class="money"></td>
                                <td class="money data">Rp{{ $data['penjualan'] }}</td>
                            </tr>
                            <tr>
                                <td>Potongan Pembelian</td>
                                <td class="money"></td>
                                <td class="money data">Rp{{ $data['pot_pembelian'] }}</td>
                            </tr>
                            {{-- <tr>
                                <td>Retur Pembelian</td>
                                <td class="money"></td>
                                <td class="money">Rp{{ $data['retur_pembelian'] }}</td>
                            </tr>
                            <tr>
                                <td>Potongan Pembelian</td>
                                <td class="money"></td>
                                <td class="money">Rp{{ $data['pot_pembelian'] }}</td>
                            </tr> --}}
                            <tr>
                                <td>Dikurangi: Retur Penjualan</td>
                                <td class="money"></td>
                                <td class="money data">(Rp{{ $data['retur_penjualan'] }})</td>
                            </tr>
                            <tr>
                                <td><i style="color:white">Dikurangi:</i> Potongan Penjualan</td>
                                <td class="money"></td>
                                <td class="money data">(Rp{{ $data['pot_penjualan'] }})</td>
                            </tr>
                            <tr>
                                <td><i style="color:white">Dikurangi:</i> Bonus Penjualan</td>
                                <td class="money"></td>
                                <td class="money data">(Rp{{ $data['bonus_penjualan'] }})</td>
                            </tr>
                            <tr>
                                <td><b>Pendapatan Penjualan Bersih<b></td>
                                <td class="money"></td>
                                @if($data['penjualan_bersih'] >= 0)
                                    <td class="money jumlah data">Rp{{ $data['penjualan_bersih'] }}</td>
                                @else
                                    <td class="money jumlah data">(Rp{{ $data['penjualan_bersih'] }})</td>
                                @endif
                            </tr>
                            <tr>
                                <td><i style="color:white"><br></i></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>Harga Pokok Penjualan</td>
                                <td class="money"></td>
                                <td class="money data">(Rp{{ $data['HPP'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Ongkir</td>
                                <td class="money"></td>
                                <td class="money data">(Rp{{ $data['beban_ongkir'] }})</td>
                            </tr>
                            <tr>
                                <td><b>Laba Rugi Kotor</b></td>
                                <td class="money"></td>
                                @if($data['laba_kotor'] >= 0)
                                    <td class="money jumlah data" id="laba_kotor">Rp{{ $data['laba_kotor'] }}</td>
                                @else
                                    <td class="money jumlah data" id="laba_kotor">(Rp{{ $data['laba_kotor'] }})</td>
                                @endif
                            </tr>
                            <tr>
                                <td><i style="color:white"><br></i></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td class="atasan">Beban Beban</td>
                            </tr>
                            <tr>
                                <td>Beban Iklan</td>
                                <td class="money">(Rp{{ $data['beban_iklan'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Sewa</td>
                                <td class="money">(Rp{{ $data['beban_sewa'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Perlengkapan</td>
                                <td class="money">(Rp{{ $data['beban_perlengkapan'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Perawatan dan Perbaikan</td>
                                <td class="money">(Rp{{ $data['beban_rawat'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Kerugian Aset</td>
                                <td class="money">(Rp{{ $data['beban_rugi_aset'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Kerugian Piutang</td>
                                <td class="money">(Rp{{ $data['rugi_piutang'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Kerugian Pembelian</td>
                                <td class="money">(Rp{{ $data['beban_beli'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Depresiasi Peralatan</td>
                                <td class="money">(Rp{{ $data['beban_dep_alat'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Depresiasi Kendaraan</td>
                                <td class="money">(Rp{{ $data['beban_dep_kendaraan'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Depresiasi Bangunan</td>
                                <td class="money">(Rp{{ $data['dep_bangunan'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Asuransi</td>
                                <td class="money">(Rp{{ $data['beban_asuransi'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Gaji</td>
                                <td class="money">(Rp{{ $data['beban_gaji'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Administrasi Bank</td>
                                <td class="money">(Rp{{ $data['beban_adm_bank'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Bunga</td>
                                <td class="money">(Rp{{ $data['bunga'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Denda</td>
                                <td class="money">(Rp{{ $data['beban_denda'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Internet dan Telepon</td>
                                <td class="money">(Rp{{ $data['beban_utilitas'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Listrik</td>
                                <td class="money">(Rp{{ $data['beban_listrik'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Kerugian Persediaan</td>
                                <td class="money">(Rp{{ $data['rugi_persediaan'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban BBM</td>
                                <td class="money">(Rp{{ $data['transportasi'] }})</td>
                            </tr>
                            {{-- <tr>
                                <td>Beban Garansi</td>
                                <td class="money">(Rp{{ $data['garansi'] }})</td>
                            </tr> --}}
                            <tr>
                                <td>Beban Konsumsi</td>
                                <td class="money">(Rp{{ $data['konsumsi'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Sosial</td>
                                <td class="money">(Rp{{ $data['sosial'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Pajak</td>
                                <td class="money" id="beban_pajak">(Rp{{ $data['pajak'] }})</td>
                            </tr>
                            <tr>
                                <td>Beban Lain-Lain</td>
                                <td class="money bawahan">(Rp{{ $data['beban_lain'] }})</td>
                            </tr>
                            <tr>
                                <td>&emsp;&emsp; <b>Total Beban</b></td>
                                <td class="money">(Rp{{ $data['total_beban'] }})</td>
                            </tr>
                            <tr>
                                <td><b>Laba dikurangi Beban</b></td>
                                <td class="money"></td>
                                @if($data['laba_beban'] >= 0)
                                    <td class="money jumlah data">Rp{{ $data['laba_beban'] }}</td>
                                @else
                                    <td class="money jumlah data">(Rp{{ $data['laba_beban'] }})</td>
                                @endif
                            </tr>

                            <tr>
                                <td><i style="color:white"><br></i></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td class="atasan">Pendapatan Lain-Lain</td>
                            </tr>
                            <tr>
                                <td>Pendapatan Lain-Lain</td>
                                <td class="money"></td>
                                <td class="money data">Rp{{ $data['pendapatan_lain'] }}</td>
                            </tr>
                            <tr>
                                <td>Keuntungan Persediaan</td>
                                <td class="money"></td>
                                <td class="money data">Rp{{ $data['untung_persediaan'] }}</td>
                            </tr>

                            <tr>
                                <td>
                                    Beban Pajak {{ App\Util::angka_koma($persen_pajak) }} % (Sementara)
                                    <input name="persenCheckbox" id="persenCheckbox" type="checkbox">
                                </td>
                                <td class="money"></td>
                                <td class="money persen_pajak data" id="persen_pajak"></td>
                            </tr>

                            <tr>
                                <td style="padding-top: 5px;"><b>Laba Rugi Bersih</b></td>
                                <td class="money"></td>
                                @if($data['laba_sebelum_pajak'] >= 0)
                                    <td class="money jumlah data" id="laba_bersih">Rp{{ $data['laba_sebelum_pajak'] }}</td>
                                @else
                                    <td class="money jumlah data" id="laba_bersih">(Rp{{ $data['laba_sebelum_pajak'] }})</td>
                                @endif
                            </tr>

                            <tr>
                                <td><i style="color:white"><br></i></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script type="text/javascript">
        var persen_pajak = 0;
        var temp_beban_pajak = '';
        var temp_laba_kotor = '';
        var laba_kotor = '';
        var laba_kotor_negatif = false;
        var temp_laba_bersih = '';
        var laba_bersih = '';
        var laba_bersih_negatif = false;
        var beban_pajak = '';

        $(document).ready(function() {
            // $('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
            //     var mulai = (start.toISOString()).substring(0,10);
            //     // console.log(mulai);
            //     var akhir = (end.toISOString()).substring(0,10);
            //     // console.log(akhir);
            //     $('#formPilihan').find('input[name="awal"]').val(mulai);
            //     $('#formPilihan').find('input[name="akhir"]').val(akhir);
            // });
            var url = "{{ url('laba_rugi') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $('#rentang_tanggal').daterangepicker({
                autoApply: true,
                calender_style: "picker_2",
                showDropdowns: true,
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: false
            }, 
            function(start, end, label) {
                var mulai = (start.toISOString()).substring(0,10);
                var akhir = (end.toISOString()).substring(0,10);
                $('#formPilihan').find('input[name="awal"]').val(mulai);
                $('#formPilihan').find('input[name="akhir"]').val(akhir);
            });

            // temp_laba_kotor = '{{ $data['laba_kotor'] }}';
            temp_laba_kotor = $('#laba_kotor').text();
            if (temp_laba_kotor.split('(').length > 1) {
                temp_laba_kotor = temp_laba_kotor.split('(')[1];
                temp_laba_kotor = temp_laba_kotor.split(')')[0];
                temp_laba_kotor = temp_laba_kotor.split('Rp')[1];
                laba_kotor_negatif = true;
            }
            laba_kotor = temp_laba_kotor.split(',');
            laba_kotor = (parseInt(laba_kotor[0].replace(/\D/g, ''), 10))+'.'+laba_kotor[1];
            laba_kotor = parseFloat(laba_kotor);
            if (laba_kotor_negatif) laba_kotor *= -1;

            persen_pajak = '{{ json_encode($persen_pajak) }}';
            persen_pajak = persen_pajak.replace(/&quot;/g, '"');
            persen_pajak = JSON.parse(persen_pajak);
            persen_pajak = parseFloat(persen_pajak);

            // temp_laba_bersih = '{{ $data['laba_sebelum_pajak'] }}';
            temp_laba_bersih = $('#laba_bersih').text();
            if (temp_laba_bersih.split('(').length > 1) {
                temp_laba_bersih = temp_laba_bersih.split('(')[1];
                temp_laba_bersih = temp_laba_bersih.split(')')[0];
                temp_laba_bersih = temp_laba_bersih.split('Rp')[1];
                laba_bersih_negatif = true;
            }
            laba_bersih = temp_laba_bersih.split(',');
            laba_bersih = (parseInt(laba_bersih[0].replace(/\D/g, ''), 10))+'.'+laba_bersih[1];
            laba_bersih = parseFloat(laba_bersih);
            if (laba_bersih_negatif) laba_bersih *= -1;

            temp_beban_pajak = '{{ $data['pajak'] }}';
            beban_pajak = temp_beban_pajak.split(',');
            beban_pajak = (parseInt(beban_pajak[0].replace(/\D/g, ''), 10))+'.'+beban_pajak[1];
            beban_pajak = parseFloat(beban_pajak);

            if (beban_pajak > 0) {
                $('#persenCheckbox').prop('checked', false);
                $('#persen_pajak').empty();
                $('#persen_pajak').append('<span>Rp0,00</span>');
                // $('#persen_pajak').append('<span class="invisible">)</span>');
            } else {
                // centang beban pajak sementara
                $('#persenCheckbox').prop('checked', true);

                var nom_persen_pajak = persen_pajak / 100 * laba_kotor;
                nom_persen_pajak = Math.round(nom_persen_pajak * 100) / 100;

                var text_persen_pajak = nom_persen_pajak.toLocaleString(undefined, {minimumFractionDigits: 2});
                // console.log(text_persen_pajak);
                $('#persen_pajak').empty();
                $('#persen_pajak').append('(Rp'+text_persen_pajak+')');

                var nom_laba_bersih = laba_bersih - nom_persen_pajak;
                nom_laba_bersih = Math.round(nom_laba_bersih * 100) / 100;
                var text_laba_bersih = nom_laba_bersih.toLocaleString(undefined, {minimumFractionDigits: 2});
                $('#laba_bersih').text('Rp'+text_laba_bersih);
                $('#laba_bersih').append('<span class="invisible"></span>');

                if (nom_laba_bersih < 0) {
                    $('#laba_bersih').text('(Rp'+text_laba_bersih+')');
                }
            }
            // console.log(beban_pajak);

            $('.money').each(function(index, el) {
                var text = $(el).text();
                text = text.replace('-', '');
                $(el).text(text);
            });

            $('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                    $(el).text(duit);
                }else{
                    var a = '<span class="invisible">)</span>';
                    $(el).text(duit);
                    $(el).append(a);
                }
            });
        });

        $(document).on('change', '#persenCheckbox', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                // centang beban pajak sementara
                $('#persenCheckbox').prop('checked', true);

                var nom_persen_pajak = persen_pajak / 100 * laba_kotor;
                nom_persen_pajak = Math.round(nom_persen_pajak * 100) / 100;

                var text_persen_pajak = nom_persen_pajak.toLocaleString(undefined, {minimumFractionDigits: 2});
                // console.log(text_persen_pajak);
                $('#persen_pajak').empty();
                $('#persen_pajak').append('(Rp'+text_persen_pajak+')');
                $('#persen_pajak').append('<span class="invisible"></span>');

                var nom_laba_bersih = laba_bersih - nom_persen_pajak;
                nom_laba_bersih = Math.round(nom_laba_bersih * 100) / 100;
                var text_laba_bersih = nom_laba_bersih.toLocaleString(undefined, {minimumFractionDigits: 2});
                $('#laba_bersih').text('Rp'+text_laba_bersih);
                $('#laba_bersih').append('<span class="invisible">)</span>');
                if (nom_laba_bersih < 0) {
                    text_laba_bersih = (nom_laba_bersih * -1).toLocaleString(undefined, {minimumFractionDigits: 2});
                    $('#laba_bersih').text('(Rp'+text_laba_bersih+')');
                }
            } else {
                var text_persen_pajak = (0).toLocaleString(undefined, {minimumFractionDigits: 2});
                // console.log(text_persen_pajak);
                $('#persen_pajak').empty();
                $('#persen_pajak').append('Rp'+text_persen_pajak);
                $('#persen_pajak').append('<span class="invisible">)</span>');

                // var text_laba_bersih = laba_bersih.toLocaleString(undefined, {minimumFractionDigits: 2});
                // $('#laba_bersih').text('Rp'+text_laba_bersih);

                var nom_laba_bersih = laba_bersih;
                nom_laba_bersih = Math.round(nom_laba_bersih * 100) / 100;
                var text_laba_bersih = nom_laba_bersih.toLocaleString(undefined, {minimumFractionDigits: 2});
                $('#laba_bersih').text('Rp'+text_laba_bersih);
                $('#laba_bersih').append('<span class="invisible">)</span>');


                if (nom_laba_bersih < 0) {
                    text_laba_bersih = (nom_laba_bersih * -1).toLocaleString(undefined, {minimumFractionDigits: 2});
                    $('#laba_bersih').text('(Rp'+text_laba_bersih+')');
                }
            }
        });

    </script>
@endsection
