@extends('layouts.print')

@section('app.style')
	<style>
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<!--
		 /* Font Definitions */
		 .kanan{
        	text-align: right;
        }

        .x_content{
        	/*font-size: 12px;*/
        	line-height: 130%;
        }

        .gemuk {
        	font-weight: bold;
        }
/*
        @page {
        size: 210cm 297cm;
        margin: 30mm 30mm 30mm 30mm;
	    }*/

	    @media print {
	    	.make-grid(md);
	    	table: width="80%", margin-left:50px;
	    }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:595.3pt 841.9pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}
	</style>
@endsection

@section('app.body')
	<div class=WordSection1>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><img width=185 height=100 src="{{ URL::to('images/image001.png') }}" align=left hspace=12><b><span style='font-size:14.0pt;line-height:107%; font-family:"Clarendon Blk BT","serif"'>KENCANA MULYA</span></b></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='font-size:14.0pt;line-height:107%;font-family: "Clarendon Blk BT","serif"'>KARANGDUWUR-PETANAHAN</span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Jl. Puring-Petanahan No. 3 Karangduwur, Kec. Petanahan, Kab. Kebumen</span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'><a>kencanamulyakarangduwur@gmail.com</a></span></p>

		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Telp./Fax (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</span></p>
		
		<p class=MsoFooter>
	</div>

		<table cellpadding=0 cellspacing=0 align=left>
			 <tr>
		  		<td width=0 height=5></td>
		 	</tr>
		 	<tr>
		  		<td></td>
		  		<td><img width=721 height=5 src="{{ URL::to('images/image002.png') }}"></td>
		 	</tr>
		</table>

		<center style="padding-top: 25px">
			<STRONG>LAPORAN LABA RUGI </STRONG><br/>
			@if($status==1)
				<STRONG> PERIODE {{ \App\Util::tanggal($newAwal->format('d m Y')) }} </STRONG><br/>
			@else
				<STRONG> PERIODE {{ \App\Util::tanggal($newAwal->format('d m Y')) }} s/d {{ \App\Util::tanggal($newAkhir->format('d m Y')) }}  </STRONG><br/>
			@endif
		</center>
		<br>
		<br>
		<!-- content -->
		<div class="x_content">
			<div class="row" style="overflow: hidden;">
				<table class="" style="margin-left:10%; font-size: 14px" width="80%" >
					<tbody>
						<tr>
							<td class="atasan" width="60%"><strong>Pendapatan Penjualan</strong></td>
							<td style="text-align: right" class="data" width="20%"></td>
							<td style="text-align: right" class="data" width="20%"></td>
						</tr>
						<tr>
							<td>Penjualan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['penjualan'] }}</td>
						</tr>
						<tr>
							<td>Potongan Pembelian</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['pot_pembelian'] }}</td>
						</tr>
						{{-- <tr>
							<td>Retur Pembelian</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['retur_pembelian'] }}</td>
						</tr>
						<tr>
							<td>Potongan Pembelian</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['pot_pembelian'] }}</td>
						</tr> --}}
						<tr>
							<td>Dikurangi: Retur Penjualan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">(Rp{{ $data['retur_penjualan'] }})</td>
						</tr>
						<tr>
							<td><i style="color:white">Dikurangi:</i> Potongan Penjualan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">(Rp{{ $data['pot_penjualan'] }})</td>
						</tr>
						<tr>
							<td><i style="color:white">Dikurangi:</i> Bonus Penjualan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">(Rp{{ $data['bonus_penjualan'] }})</td>
						</tr>
						<tr>
							<td><b>Pendapatan Penjualan Bersih<b></td>
							<td style="text-align: right" class="data"></td>
							@if($data['penjualan_bersih'] >= 0)
								<td style="text-align: right" class="data"><strong>Rp{{ $data['penjualan_bersih'] }}</strong></td>
							@else
								<td style="text-align: right" class="data"><strong>(Rp{{ $data['penjualan_bersih'] }})</strong></td>
							@endif
						</tr>
						<tr>
							<td><i style="color:white"><br></i></td>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td>Harga Pokok Penjualan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">(Rp{{ $data['HPP'] }})</td>
						</tr>
						<tr>
							<td>Beban Ongkir</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_ongkir'] }})</td>
						</tr>					
						<tr>
							<td><b>Laba Rugi Kotor</b></td>
							<td style="text-align: right" class="data"></td>
							@if($data['laba_kotor'] >= 0)
								<td style="text-align: right" class="data"><strong>Rp{{ $data['laba_kotor'] }}</strong></td>
							@else
								<td style="text-align: right" class="data"><strong>(Rp{{ $data['laba_kotor'] }})</strong></td>
							@endif
						</tr>
						<tr>
							<td><i style="color:white"><br></i></td>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td class="atasan"><strong>Beban Beban</strong></td>
						</tr>
						<tr>
							<td>Beban Iklan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_iklan'] }})</td>
						</tr>
						<tr>
							<td>Beban Sewa</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_sewa'] }})</td>
						</tr>
						<tr>
							<td>Beban Perlengkapan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_perlengkapan'] }})</td>
						</tr>
						<tr>
							<td>Beban Perawatan dan Perbaikan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_rawat'] }})</td>
						</tr>
						<tr>
							<td>Beban Kerugian Aset</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_rugi_aset'] }})</td>
						</tr>
						<tr>
							<td>Beban Kerugian Piutang</td>
							<td style="text-align: right" class="data">(Rp{{ $data['rugi_piutang'] }})</td>
						</tr>
						<tr>
							<td>Beban Kerugian Pembelian</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_beli'] }})</td>
						</tr>
						<tr>
							<td>Beban Depresiasi Peralatan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_dep_alat'] }})</td>
						</tr>
						<tr>
							<td>Beban Depresiasi Kendaraan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_dep_kendaraan'] }})</td>
						</tr>
						<tr>
							<td>Beban Depresiasi Bangunan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['dep_bangunan'] }})</td>
						</tr>
						<tr>
							<td>Beban Asuransi</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_asuransi'] }})</td>
						</tr>
						<tr>
							<td>Beban Gaji</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_gaji'] }})</td>
						</tr>
						<tr>
							<td>Beban Administrasi Bank</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_adm_bank'] }})</td>
						</tr>
						<tr>
							<td>Beban Bunga</td>
							<td style="text-align: right" class="data">(Rp{{ $data['bunga'] }})</td>
						</tr>
						<tr>
							<td>Beban Denda</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_denda'] }})</td>
						</tr>
						<tr>
							<td>Beban Internet dan Telepon</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_utilitas'] }})</td>
						</tr>
						<tr>
							<td>Beban Listrik</td>
							<td style="text-align: right" class="data">(Rp{{ $data['beban_listrik'] }})</td>
						</tr>
						<tr>
							<td>Beban Kerugian Persediaan</td>
							<td style="text-align: right" class="data">(Rp{{ $data['rugi_persediaan'] }})</td>
						</tr>
						<tr>
							<td>Beban BBM</td>
							<td style="text-align: right" class="data">(Rp{{ $data['transportasi'] }})</td>
						</tr>
						{{-- <tr>
							<td>Beban Garansi</td>
							<td style="text-align: right" class="data">(Rp{{ $data['garansi'] }})</td>
						</tr> --}}
						<tr>
							<td>Beban Konsumsi</td>
							<td style="text-align: right" class="data">(Rp{{ $data['konsumsi'] }})</td>
						</tr>
						<tr>
							<td>Beban Sosial</td>
							<td style="text-align: right" class="data">(Rp{{ $data['sosial'] }})</td>
						</tr>
						<tr>
							<td>Beban Pajak</td>
							<td style="text-align: right" class="data">(Rp{{ $data['pajak'] }})</td>
						</tr>
						<tr>
							<td>Beban Lain-Lain</td>
							<td style="text-align: right" class="data"bawahan">(Rp{{ $data['beban_lain'] }})</td>
						</tr>
						<tr>
							<td>&emsp;&emsp; <b>Total Beban</b></td>
							<td style="text-align: right" class="data"><strong>(Rp{{ $data['total_beban'] }})</strong></td>
						</tr>
						<tr>
							<td><b>Laba dikurangi Beban</b></td>
							<td style="text-align: right" class="data"></td>
							@if($data['laba_beban'] >= 0)
								<td style="text-align: right" class="data"><strong>Rp{{ $data['laba_beban'] }}</strong></td>
							@else
								<td style="text-align: right" class="data"><strong>(Rp{{ $data['laba_beban'] }})</strong></td>
							@endif
						</tr>

						<tr>
							<td><i style="color:white"><br></i></td>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td class="atasan"><strong>Pendapatan Lain-Lain</strong></td>
						</tr>
						<tr>
							<td>Pendapatan Lain-Lain</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['pendapatan_lain'] }}</td>
						</tr>
						<tr>
							<td>Keuntungan Persediaan</td>
							<td style="text-align: right" class="data"></td>
							<td style="text-align: right" class="data">Rp{{ $data['untung_persediaan'] }}</td>
						</tr>

						<tr>
							<td><b>Laba Rugi Bersih</b></td>
							<td style="text-align: right" class="data"></td>
							@if($data['laba_sebelum_pajak'])
								<td style="text-align: right" class="data"><strong>Rp{{ $data['laba_sebelum_pajak'] }}</strong></td>
							@else
								<td style="text-align: right" class="data"><strong>(Rp{{ $data['laba_sebelum_pajak'] }})</strong></td>
							@endif
						</tr>

						<tr>
							<td><i style="color:white"><br></i></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>		
			</div>
  		</div>
@endsection

@section('app.script')
    <script type="text/javascript">
    	$(document).ready(function() {
			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                	duit = duit.replace('-', '');
                	$(el).text(duit);
                }else{
                	var a = '<span class="invisible">)</span>';
                	duit = duit.replace('-', '');
                	$(el).text(duit);
                	$(el).append(a);
                }
            });

		window.print();
		});
    </script>
@endsection


{{-- <script type="text/javascript" charset="utf-8" async defer>
	// $(document).ready(function() {
	// 	$('.money').each(function(index, el) {
	// 		var text = $(el).text();
	// 		text = text.replace('-', '');
	// 		$(el).text(text);
 //        });
 //    });    
	window.print();
</script> --}}