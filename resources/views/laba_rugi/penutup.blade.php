@extends('layouts.admin')

@section('title')
    <title>EPOS | Input Jurnal Penutup</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-10" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<div class="col-md-8">
					@if($status==1)
						<h2>Jurnal Penutup</h2>
						<h2 id="header_tanggal"> Sumber Laporan Laba Rugi Periode {{ \App\Util::tanggal($newAwal->format('d m Y')) }}</h2>
					@else
						<h2>Jurnal Penutup</h2>
						<h2 id="header_tanggal"> Sumber Laporan Laba Rugi Periode {{ \App\Util::tanggal($newAwal->format('d m Y')) }} s/d {{ \App\Util::tanggal($newAkhir->format('d m Y')) }} </h2>
					@endif
				</div>
					
				<div class="col-md-4 pull-right">
					<a href="{{ url()->previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
				</div>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row">
    				<form method="post" action="{{ url('penutup') }}" class="form-horizontal" id="formJurnal">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="row">
							<div class="form-group col-md-4 col-xs-12">
								<label class="control-label">Laba Bersih</label>
							</div>
							<div class="col-md-4 col-md-offset-4">
								<input class="form-control text-right" type="text" name="laba_bersih" readonly value="{{ $request->laba_bersih }}">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4 col-xs-12">
								<label class="control-label">Laba Ditahan</label>
							</div>
							<div class="col-md-4">
								<input class="form-control text-right" type="text" name="laba_ditahan_i" value="0,00">
							</div>
							<div class="col-md-4">
								<input class="form-control text-right" type="text" name="laba_ditahan_v" readonly value="0,00">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4 col-xs-12">
								<label class="control-label">Bagi Hasil</label>
							</div>
							<div class="col-md-4">
								<input class="form-control text-right" type="text" name="bagi_hasil_i" value="0,00">
							</div>
							<div class="col-md-4">
								<input class="form-control text-right" type="text" name="bagi_hasil_v" readonly value="0,00">
							</div>
						</div>
						<div class="row" style="padding-top: 20px">
							<div class="col-md-12 col-xs-12">
								<div class="form-group" style="margin-bottom: 0;">
									<input type="hidden" name="bagi_hasil">
									<input type="hidden" name="laba_tahan">
									<input type="hidden" name="laba_bersih_x">
									<input type="hidden" name="awal" value="{{ $newAwal }}">
									<input type="hidden" name="akhir" value="{{ $akhiri }}">
									<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
										<i class="fa fa-save"></i> <span>Simpan</span>
									</button>
									<span class="pull-right" id="notif" style="padding-right: 20px; padding-top: 5px"></span>
								</div>
							</div>
						</div>
					</form>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();
		

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$(".select2_single").select2({
				allowClear: true
			});

			$('#btnSimpan').prop('disabled', true);
		});

		$(document).on('keyup', 'input[name="laba_ditahan_i"]', function(event) {
			event.preventDefault();
			var laba_tahans = $(this).val().split(',');
			var laba_tahan = 0;
			if(laba_tahans.length > 1){
				laba_tahan = $(this).val();
				if(laba_tahans[0].length < 1){
					laba_tahans[0] = 0;
					$(this).val(laba_tahans[0] + ',' + laba_tahans[1]);
				}
				else if(laba_tahans[1].length > 2){
					laba_tahan = $(this).val().slice(0,-1);
					$(this).val(laba_tahan);
					// console.log(laba_tahan.toLocaleString(['ban', 'id']));
				}else if(laba_tahans[1].length < 2){
					laba_tahan = parseFloat(laba_tahan.replace(',', '.'));
					laba_tahan = laba_tahan.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
					laba_tahan = laba_tahan.replace('.', '');
					$(this).val(laba_tahan);
					// console.log('tengah');
				}
			}else{
				laba_tahan = $(this).val();
				var bel_koma = laba_tahan.substr(laba_tahan.length -2);
				var depan_koma = laba_tahan.substr(0, laba_tahan.length -2);

				$(this).val(depan_koma + ',' + bel_koma);
			}

			var laba_tahan_h = parseFloat(laba_tahan.replace(',', '.'));
			var laba_tahan_v = laba_tahan_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

			$('input[name="laba_ditahan_v"]').val(laba_tahan_v);
			$('input[name="laba_tahan"]').val(laba_tahan_h);
			cek();
		});  

		$(document).on('keyup', 'input[name="bagi_hasil_i"]', function(event) {
			event.preventDefault();
			var bagi_hasils = $(this).val().split(',');
			var bagi_hasil = 0;
			if(bagi_hasils.length > 1){
				bagi_hasil = $(this).val();
				if(bagi_hasils[0].length < 1){
					bagi_hasils[0] = 0;
					$(this).val(bagi_hasils[0] + ',' + bagi_hasils[1]);
				}
				else if(bagi_hasils[1].length > 2){
					bagi_hasil = $(this).val().slice(0,-1);
					$(this).val(bagi_hasil);
					// console.log(bagi_hasil.toLocaleString(['ban', 'id']));
				}else if(bagi_hasils[1].length < 2){
					bagi_hasil = parseFloat(bagi_hasil.replace(',', '.'));
					bagi_hasil = bagi_hasil.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
					bagi_hasil = bagi_hasil.replace('.', '');
					$(this).val(bagi_hasil);
					// console.log('tengah');
				}
			}else{
				bagi_hasil = $(this).val();
				var bel_koma = bagi_hasil.substr(bagi_hasil.length -2);
				var depan_koma = bagi_hasil.substr(0, bagi_hasil.length -2);

				$(this).val(depan_koma + ',' + bel_koma);
			}
			var bagi_hasil_h = parseFloat(bagi_hasil.replace(',', '.'));
			var bagi_hasil_v = bagi_hasil_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
			$('input[name="bagi_hasil_v"]').val(bagi_hasil_v);
			$('input[name="bagi_hasil"]').val(bagi_hasil_h);
			cek();
		});	

		function cek(){
			var laba_tahan = parseFloat(($('input[name="laba_ditahan_i"]').val()).replace(',', '.'));
			var bagi_hasil = parseFloat(($('input[name="bagi_hasil_i"]').val()).replace(',', '.'));
			var laba_bersih = parseFloat(($('input[name="laba_bersih"]').val()).replace(',', '.'));

			$('input[name="laba_bersih_x"]').val(laba_bersih);

			if((laba_tahan + bagi_hasil) == laba_bersih){
				$('#btnSimpan').prop('disabled', false);
			}else{
				$('#btnSimpan').prop('disabled', true);
			}			
		}
	</script>
@endsection


