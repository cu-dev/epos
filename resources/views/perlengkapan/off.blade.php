@extends('layouts.admin')

@section('title')
    <title>EPOS | Perlengkapan</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }
        button[data-target="#collapse-perlengkapan"] {
        	background-color: transparent;
        	border: none;
        }
    </style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Perlengkapan Lama</h2>
				<a href="{{url('perlengkapan')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
					</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePerlengkapan">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Barang</th>
							<th>Item</th>
							<th>Harga</th>
							<th>Keterangan</th>
							<th>Nomor Transaksi</th>
							<th>Tanggal pindah</th>
						</tr>
					</thead>
					<tbody>
						@foreach($perlengkapans as $num => $perlengkapan)
						<tr id="{{$perlengkapan->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $perlengkapan->kode }}</td>
							<td>{{ $perlengkapan->nama }}</td>
							<td align="right">{{ \App\Util::ewon($perlengkapan->harga) }}</td>
							@if($perlengkapan->keterangan!==NULL)
								<td>{{ $perlengkapan->keterangan }}</td>
							@else
								<td align="center"> - </td>
							@endif
							<td>{{ $perlengkapan->no_transaksi }}</td>
							<td class="text-center">{{ $perlengkapan->updated_at->format('d-m-Y') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tablePerlengkapan').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			var url = "{{ url('perlengkapan') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});

		$(document).on('click', 'button[data-target="#collapse-perlengkapan"]', function(event) {
			event.preventDefault();
			
			if ($(this).hasClass('tampil')) {
				$(this).removeClass('tampil');
				$(this).addClass('sembunyi');
				$(this).find('i').removeClass('fa-chevron-up');
				$(this).find('i').addClass('fa-chevron-down');
			} else if ($(this).hasClass('sembunyi')) {
				$(this).removeClass('sembunyi');
				$(this).addClass('tampil');
				$(this).find('i').removeClass('fa-chevron-down');
				$(this).find('i').addClass('fa-chevron-up');
			}
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
		});

		$(window).on('load', function(event) {

			var url = "{{ url('perkap/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			

			$.get(url, function(data) {
				if (data.perkap === null) {
					var kode = int4digit(1);
					var kode = kode + '/KM/' + tanggal;
					console.log('kode');
				} else {
					var kode = data.perkap.kode;
					var mm_transaksi = kode.split('/')[2];
					var yyyy_transaksi = kode.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(parseInt(kode.split('/')[0]) + 1);
						kode = kode + '/KM/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode.split('/')[0]) + 1);
						kode = kode + '/KM/' + tanggal_transaksi;				}
				}
				// console.log(kode);
				$('input[name="kode"]').val(kode);
			});
		});
	</script>
@endsection
