@extends('layouts.admin')

@section('title')
    <title>EPOS | Pembayaran Beban</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-5" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Pembayaran Beban</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right">	
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('beban') }}" class="form-horizontal">
                        <div class="col-md-12 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jenis Beban</label>
                                <select name="beban" id="jenis_beban" class="select2_single form-control" required="">
                                    <option value ="" id="default">Pilih Beban</option>
                                    <option value="{{ \App\Akun::KartuKredit }}">{{ \App\Akun::KartuKredit }} - Kartu Kredit </option>
                                    @foreach($bebans as $beban)
                                    <option value="{{ $beban->kode }}">{{ $beban->kode }} - {{ $beban->nama }}</option>
                                    @endforeach
                                </select>
                                <p id="KK" style="color:red" class="sembunyi">Nominal Hutang Kartu Kredit {{ \App\Util::duit($kartu_kredit->kredit) }}</p>
                            </div>
                            <div class="form-group" id=nominal>
                                <label class="control-label">Nominal</label>
                                <input class="form-control input_cek text-right" type="text" name="nominal_i" value="0,00" required="">
                                <input type="hidden" name="nominal">
                                <label class="control-label pull-right" id="nominal_v">0,00</label><br>
                                @if(Auth::user()->level_id == 5)
                                    <label class="control-label pull-left">Saldo Laci Gudang: {{ \App\Util::ewon($laci->gudang) }}</label>
                                @endif
                                @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                    <label class="control-label pull-left">Saldo Laci Owner: {{ \App\Util::ewon($laci->owner) }}</label>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control input_cek" type="text" name="keterangan">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Sumber Kas</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKecil" class="btn btn-default">Kas Kecil</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank" id="bank" class="form-control select2_single">
                                    <option value="" id="default">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="KecilContainer">
                                <label class="control-label pull-right">Saldo Kas Kecil : {{ number_format($kas_kecil, 0,",", ".") }}</label>
                            </div>
                                <input type="hidden" name="kas" />
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Bayar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Daftar Pembayaran Beban</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                    <thead>
                        <tr>
                            <th>Kode Akun</th>
                            <th>Nama Akun</th>
                            <th>Nominal</th>
                            <th>Operator</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dones as $nums => $done)
                            @if($done->debet !=0 || $done->debet != NULL)
                            <tr>
                                <td>{{ $done->kode_akun }}</td>
                                <td>{{ $done->akun->nama }}</td>
                                <td class="text-right">{{ \App\Util::duit($done->debet) }}</td>
                                @if ($done->user_id != null)
                                    <td>{{ $done->user->nama }}</td>
                                @else
                                    <td></td>
                                @endif

                                <td>{{ $done->keterangan }}</td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- </div> -->


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();
        var kas_kecil = {{ $kas_kecil }};
        $(document).ready(function() {
            $('#KecilContainer').hide();
            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');

            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');
            $('#btnKecil').removeClass('btn-success');
            $('#btnKecil').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });
            $('#KecilContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
            nominal_keyup();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');
            $('#btnKecil').removeClass('btn-success');
            $('#btnKecil').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });
            $('#KecilContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            btnSimpan();
            nominal_keyup();
        });

        $(document).on('click', '#btnKecil', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');
            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');
            
            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });
            $('#KecilContainer').show();

            $('#formSimpanContainer').find('input[name="kas"]').val('kecil');
            btnSimpan();
            nominal_keyup();
        });

        $(window).on('load', function(event) {
            var url = "{{ url('beban/last/json') }}";
            console.log(url);
            var tanggal = printBulanSekarang('mm/yyyy');

            $.get(url, function(data) {
                if (data.beban === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/PBB/' + tanggal;
                } else {
                    var referensi = data.beban.referensi.substr(0,16);
                    // var referensi = data.beban.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/PBB/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/PBB/' + tanggal_transaksi;
                    }
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

        $(document).on('change', 'select[name="beban"]', function(event) {
            event.preventDefault();
            var id = $(this).val();
            var kredit = {{ $kartu_kredit->kode }};
            if(id == kredit) {
                $('#btnTunai').prop('disabled', true);
                $('#btnKecil').prop('disabled', true);
                $('#btnTunai').removeClass('btn-success');
                $('#btnKecil').removeClass('btn-success');
                $('#btnTunai').addClass('btn-default');
                $('#btnKecil').addClass('btn-default');
                $('#btnBank').addClass('btn-success');
                $('#BankContainer').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            }else{
                $('#btnTunai').prop('disabled', false);
                $('#btnKecil').prop('disabled', false);
                $('#btnTunai').addClass('btn-default');
                $('#btnKecil').addClass('btn-default');
            }
        });
        $(document).on('keyup', 'input[name="nominal_i"]', function(event) {
            event.preventDefault();
            nominal_keyup();
            // var $this = $(this);
            // var nominals = $(this).val().split(',');
            // var nominal = 0;
            // if(nominals.length > 1){
            // 	nominal = $(this).val();
            // 	if(nominals[0].length < 1){
            // 		nominals[0] = 0;
            // 		$(this).val(nominals[0] + ',' + nominals[1]);
            // 	}
            // 	else if(nominals[1].length > 2){
            // 		nominal = $(this).val().slice(0,-1);
            // 		$(this).val(nominal);
            // 		// console.log(nominal.toLocaleString(['ban', 'id']));
            // 	}else if(nominals[1].length < 2){
            // 		nominal = parseFloat(nominal.replace(',', '.'));
            // 		nominal = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
            // 		nominal = nominal.replace('.', '');
            // 		$(this).val(nominal);
            // 	}
            // }else{
            // 	nominal = $(this).val();
            // 	var bel_koma = nominal.substr(nominal.length -2);
            // 	var depan_koma = nominal.substr(0, nominal.length -2);

            // 	$(this).val(depan_koma + ',' + bel_koma);
            // }

            // var nominal_h = parseFloat(nominal.replace(',', '.'));
            // var nominal_v = nominal_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

            // var level_user = {{ Auth::user()->level_id }};
            // var laci_gudang = {{ $laci->gudang }};
            // var kas = $('input[name="kas"]').val();

            // if(level_user == 5 && nominal_h > parseFloat(laci_gudang)){
            // 	$('#nominal').addClass('has-error');
            // 	$('#btnSimpan').prop('disabled', true);
            // }else{
            // 	$('#nominal').removeClass('has-error');
            // 	$('#nominal_v').text('Rp'+nominal_v);
            // 	$('input[name="nominal"]').val(nominal_h);
            // 	$('#btnSimpan').prop('disabled', false);
            // 	if(kas == 'kecil'){
            // 		if(nominal_h > parseFloat(kas_kecil)){
            // 			$('#nominal').addClass('has-error');
            // 			$('#btnSimpan').prop('disabled', true);
            // 		}else{
            // 			$('#nominal').removeClass('has-error');
            // 			$('#nominal_v').text('Rp'+nominal_v);
            // 			$('input[name="nominal"]').val(nominal_h);
            // 			$('#btnSimpan').prop('disabled', false);
            // 		}
            // 	}else{
            // 		$('#nominal_v').text('Rp'+nominal_v);
            // 		$('input[name="nominal"]').val(nominal_h);
            // 	}
            // }
        });

        function nominal_keyup(){
            var $this = $('input[name="nominal_i"]');
            var nominals = $('input[name="nominal_i"]').val().split(',');
            var nominal = 0;
            if(nominals.length > 1){
                nominal = $('input[name="nominal_i"]').val();
                if(nominals[0].length < 1){
                    nominals[0] = 0;
                    $('input[name="nominal_i"]').val(nominals[0] + ',' + nominals[1]);
                }
                else if(nominals[1].length > 2){
                    nominal = $('input[name="nominal_i"]').val().slice(0,-1);
                    $('input[name="nominal_i"]').val(nominal);
                    // console.log(nominal.toLocaleString(['ban', 'id']));
                }else if(nominals[1].length < 2){
                    nominal = parseFloat(nominal.replace(',', '.'));
                    nominal = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
                    nominal = nominal.replace('.', '');
                    $('input[name="nominal_i"]').val(nominal);
                }
            }else{
                nominal = $('input[name="nominal_i"]').val();
                var bel_koma = nominal.substr(nominal.length -2);
                var depan_koma = nominal.substr(0, nominal.length -2);

                $('input[name="nominal_i"]').val(depan_koma + ',' + bel_koma);
            }

            var nominal_h = parseFloat(nominal.replace(',', '.'));
            var nominal_v = nominal_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

            var level_user = {{ Auth::user()->level_id }};
            var laci_gudang = {{ $laci->gudang }};
            var laci_owner = {{ $laci->owner }};
            var kas = $('input[name="kas"]').val();
            var bank = 1;
            if(kas != 'bank') bank = 0;

            $('#nominal_v').text('Rp'+nominal_v);

            if(level_user == 5 && nominal_h > parseFloat(laci_gudang) && bank == 0){
                if(kas == 'kecil'){
                    if(nominal_h > parseFloat(kas_kecil)){
                        $('#nominal').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('#nominal').removeClass('has-error');
                        $('#btnSimpan').prop('disabled', false);
                        $('input[name="nominal"]').val(nominal_h);
                    }
                }else{
                    $('#nominal').addClass('has-error');
                    $('#btnSimpan').prop('disabled', true);
                }
            }else if(level_user == 1 || level_user == 2){
                if(kas == 'kecil'){
                    if(nominal_h > parseFloat(kas_kecil)){
                        $('#nominal').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('#nominal').removeClass('has-error');
                        $('#btnSimpan').prop('disabled', false);
                        $('input[name="nominal"]').val(nominal_h);
                    }
                }else{
                    if(nominal_h > parseFloat(laci_owner) && bank == 0){
                        $('#nominal').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('#nominal').removeClass('has-error');
                        $('#btnSimpan').prop('disabled', false);
                        $('input[name="nominal"]').val(nominal_h);
                    }
                }
            }else{
                $('#nominal').removeClass('has-error');
                // $('#nominal_v').text('Rp'+nominal_v);
                $('input[name="nominal"]').val(nominal_h);
                $('#btnSimpan').prop('disabled', false);
                if(kas == 'kecil'){
                    if(nominal_h > parseFloat(kas_kecil)){
                        $('#nominal').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('#nominal').removeClass('has-error');
                        // $('#nominal_v').text('Rp'+nominal_v);
                        $('input[name="nominal"]').val(nominal_h);
                        $('#btnSimpan').prop('disabled', false);
                    }
                }else{
                    // $('#nominal_v').text('Rp'+nominal_v);
                    $('input[name="nominal"]').val(nominal_h);
                }
            }
        }

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#jenis_beban', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
