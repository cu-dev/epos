@extends('layouts.admin')

@section('title')
    <title>EPOS | Input Jurnal Penutup</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .kanan {
        	text-align: right;
        }

        table > tbody > tr > td.menjorok {
        	padding-left: 30px
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				@if($status==1)
					<h2 id="header_tanggal">Input Jurnal Penutup Periode {{ \App\Util::tanggal($end_str->format('d m Y')) }}</h2>
				@else
					<h2 id="header_tanggal">Input Jurnal Penutup Periode {{ \App\Util::tanggal($end_str->format('d m Y')) }} s/d {{ \App\Util::tanggal($akhir->format('d m Y')) }} </h2>
				@endif
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row">
    				<form method="post" action="{{ url('jurnal_penutup') }}" class="form-horizontal">
						<div class="col-md-12 col-xs-12">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">

							<table class="table table-bordered table-hover" style="margin-bottom: 0;" id="tablePDPT">
								<thead>
									<tr> 
										<th width="60%">Nama Akun</th>
										<th width="20%">Debit</th>
										<th width="20%">Kredit</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{ $akun['Pendapatan']->nama }}</td>
										<td class="kanan">{{ \App\Util::Duit($akun['Pendapatan']->kredit) }}</td>
										<td></td>
									</tr>

									<tr>
										<td class="menjorok">{{ $akun['LabaRugi']->nama }}</td>
										<td></td>
										<td class="kanan">{{ \App\Util::Duit($akun['Pendapatan']->kredit) }}</td>
									</tr>
								</tbody>
							</table>

							<table class="table table-bordered table-hover" style="margin-bottom: 0; margin-top: 30px" id="tableBeban">
								<thead>
									<tr> 
										<th width="60%">Nama Akun</th>
										<th width="20%">Debit</th>
										<th width="20%">Kredit</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{ $akun['LabaRugi']->nama }}</td>
										<td class="kanan">{{ \App\Util::Duit($akun['Beban']->debet) }}</td>
										<td></td>
									</tr>

									@if($akun['BebanIklan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanIklan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanIklan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanSewa']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanSewa']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanSewa']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanPerlengkapan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanPerlengkapan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanPerlengkapan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanPerawatan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanPerawatan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanPerawatan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanKerugianPiutang']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanKerugianPiutang']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanKerugianPiutang']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanKerugianPersediaan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanKerugianPersediaan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanKerugianPersediaan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanRugiAset']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanRugiAset']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanRugiAset']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanDepBangunan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanDepBangunan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanDepBangunan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanDepPeralatan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanDepPeralatan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanDepPeralatan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanDepKendaraan']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanDepKendaraan']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanDepKendaraan']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanAsuransi']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanAsuransi']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanAsuransi']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanGaji']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanGaji']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanGaji']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanAdministrasiBank']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanAdministrasiBank']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanAdministrasiBank']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanDenda']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanDenda']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanDenda']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanUtilitas']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanUtilitas']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanUtilitas']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanOngkir']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanOngkir']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanOngkir']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanTransportasi']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanTransportasi']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanTransportasi']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanGaransi']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanGaransi']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanGaransi']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanPajak']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanPajak']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanPajak']->debet) }}</td>
										</tr>
									@endif

									@if($akun['BebanLain']->debet > 0)
										<tr>
											<td class="menjorok">{{ $akun['BebanLain']->nama }}</td>
											<td></td>
											<td class="kanan">{{ \App\Util::Duit($akun['BebanLain']->debet) }}</td>
										</tr>
									@endif
								</tbody>
							</table>

							<table class="table table-bordered table-hover" style="margin-top: 30px;" id="tableLaba">
								<thead>
									<tr> 
										<th width="60%">Nama Akun</th>
										<th width="20%">Debit</th>
										<th width="20%">Kredit</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{ $akun['LabaRugi']->nama }}</td>
										<td class="kanan">{{ \App\Util::Duit($selisih) }}</td>
										<td></td>
									</tr>

									<tr>
										<td class="menjorok">{{ $akun['LabaTahan']->nama }}</td>
										<td><input class="form-control text-right" type="text" name="laba_ditahan_i" value="0,000"></td>
										<td class="kanan"><span id="laba_tahan_v">0,000</span></td>
									</tr>

									<tr>
										<td class="menjorok">{{ $akun['BagiHasil']->nama }}</td>
										<td><input class="form-control text-right" type="text" name="bagi_hasil_i" value="0,000"></td>
										<td class="kanan"><span id="bagi_hasil_v">0,000</span></td>
									</tr>
								</tbody>
							</table>

									<input type="hidden" name="bagi_hasil">
									<input type="hidden" name="laba_tahan">
									<input type="hidden" name="selisih" value="{{ $selisih }}">
									<input type="hidden" name="awal" value="{{ $end_str->format('Y-m-d') }}">
									<input type="hidden" name="akhir" value="{{ $akhir->format('Y-m-d') }}">

							<div class="form-group" style="margin-bottom: 0;">
								<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> <span>Simpan</span>
								</button>
							</div>
						</div>
					</form>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	

<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="header_tanggal">Jurnal Penutup</h2>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableJurnal">
					<thead>
						<tr>
							{{-- <th>Tahun</th>
							<th>Bulan</th>
							<th>Tanggal</th> --}}
							<th colspan="3">Tanggal</th>
							<th>Kode Akun</th>
							<th>Rekening</th>
							<th>Referensi</th>
							<th>Debit</th>
							<th>Kredit</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($jurnals as $i => $tahunan)
							@foreach ($tahunan['isi'] as $j => $bulanan)
								@foreach ($bulanan['isi'] as $k => $harian)
									@foreach ($harian['isi'] as $l => $jurnal)
										@if ($jurnal->awal_ref)
											<tr>
												<td colspan="6" style="background: white;"></td>
											</tr>
										@endif
										<tr class="{{ $jurnal->kredit > 0 ? 'kredit' : 'debet'}}">
											@if ($jurnal->awal_tahun)
												<td  class="tengah-hv" rowspan="{{ $tahunan['row'] }}">{{ $i }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											@if ($jurnal->awal_bulan)
												<td class="tengah-hv" rowspan="{{ $bulanan['row'] }}">{{ $j }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											@if ($jurnal->awal_hari)
												<td class="tengah-hv" rowspan="{{ $harian['row'] }}">{{ $k }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											<td align="center">{{ $jurnal->kode_akun }}</td>
											@if ($jurnal->kredit > 0)
												<td style="padding-left: 30px">{{ $jurnal->akun->nama }}</td>
											@else
												<td>{{ $jurnal->akun->nama }}</td>
											@endif
											<td>{{ $jurnal->referensi }}</td>
											<td class="text-right">{{ \App\Util::DK($jurnal->debet) }}</td>
											<td class="text-right">{{ \App\Util::DK($jurnal->kredit) }}</td>
										</tr>
									@endforeach
								@endforeach
							@endforeach
						@endforeach
					</tbody>
				</table>
      		</div>
    	</div>
  	</div>
</div>	

@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$('#btnSimpan').prop('disabled', true);


			$(".select2_single").select2({
				allowClear: true
			});
		});

		$(document).on('keyup', 'input[name="laba_ditahan_i"]', function(event) {
			event.preventDefault();
			var laba_tahans = $(this).val().split(',');
			var laba_tahan = 0;
			if(laba_tahans.length > 1){
				laba_tahan = $(this).val();
				if(laba_tahans[0].length < 1){
					laba_tahans[0] = 0;
					$(this).val(laba_tahans[0] + ',' + laba_tahans[1]);
				}
				else if(laba_tahans[1].length > 3){
					laba_tahan = $(this).val().slice(0,-1);
					$(this).val(laba_tahan);
					// console.log(laba_tahan.toLocaleString(['ban', 'id']));
				}else if(laba_tahans[1].length < 3){
					laba_tahan = parseFloat(laba_tahan.replace(',', '.'));
					laba_tahan = laba_tahan.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
					laba_tahan = laba_tahan.replace('.', '');
					$(this).val(laba_tahan);
				}
			}else{
				laba_tahan = $(this).val();
				var bel_koma = laba_tahan.substr(laba_tahan.length -2);
				var depan_koma = laba_tahan.substr(0, laba_tahan.length -2);

				$(this).val(depan_koma + ',' + bel_koma);
			}

			var laba_tahan_h = parseFloat(laba_tahan.replace(',', '.'));
			var laba_tahan_v = laba_tahan_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});

			$('#laba_tahan_v').text('Rp'+laba_tahan_v);
			$('input[name="laba_tahan"]').val(laba_tahan_h);
			cek();
		});  

		$(document).on('keyup', 'input[name="bagi_hasil_i"]', function(event) {
			event.preventDefault();
			var bagi_hasils = $(this).val().split(',');
			var bagi_hasil = 0;
			if(bagi_hasils.length > 1){
				bagi_hasil = $(this).val();
				if(bagi_hasils[0].length < 1){
					bagi_hasils[0] = 0;
					$(this).val(bagi_hasils[0] + ',' + bagi_hasils[1]);
				}
				else if(bagi_hasils[1].length > 3){
					bagi_hasil = $(this).val().slice(0,-1);
					$(this).val(bagi_hasil);
					// console.log(bagi_hasil.toLocaleString(['ban', 'id']));
				}else if(bagi_hasils[1].length < 3){
					bagi_hasil = parseFloat(bagi_hasil.replace(',', '.'));
					bagi_hasil = bagi_hasil.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
					bagi_hasil = bagi_hasil.replace('.', '');
					$(this).val(bagi_hasil);
				}
			}else{
				bagi_hasil = $(this).val();
				var bel_koma = bagi_hasil.substr(bagi_hasil.length -2);
				var depan_koma = bagi_hasil.substr(0, bagi_hasil.length -2);

				$(this).val(depan_koma + ',' + bel_koma);
			}
			var bagi_hasil_h = parseFloat(bagi_hasil.replace(',', '.'));
			var bagi_hasil_v = bagi_hasil_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
			$('#bagi_hasil_v').text('Rp'+bagi_hasil_v);
			$('input[name="bagi_hasil"]').val(bagi_hasil_h);
			cek();
		});	

		function cek(){
			var laba_tahan = parseFloat(($('input[name="laba_ditahan_i"]').val()).replace(',', '.'));
			var bagi_hasil = parseFloat(($('input[name="bagi_hasil_i"]').val()).replace(',', '.'));
			var selisih = parseFloat(($('input[name="selisih"]').val()).replace(',', '.'));

			$('input[name="selisih"]').val(selisih);

			if((laba_tahan + bagi_hasil) == selisih){
				$('#btnSimpan').prop('disabled', false);
			}else{
				$('#btnSimpan').prop('disabled', true);
			}			
		}
	</script>
@endsection
