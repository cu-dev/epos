@extends('layouts.admin')

@section('title')
    <title>EPOS | Kendaraan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
        button[data-target="#collapse-kendaraan"] {
            background-color: transparent;
            border: none;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Kendaraan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div>
                    <div class="row">
                        <form method="post" action="{{ url('kendaraan') }}" class="form-horizontal">
                            <div class="col-md-6 col-xs-12">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Nomor Polisi</label>
                                    <input class="form-control input_cek" type="text" name="nopol" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Umur Ekonomis</label>
                                    <input class="form-control input_cek" type="text" name="umur" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Atas Nama</label>
                                    <input class="form-control input_cek" type="text" name="atas_nama" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tahun Perakitan</label>
                                    <input class="form-control input_cek" type="text" name="tahun_perakitan" required="">
                                </div>
                                <div class="form-group controls xdisplay_inputx has-feedback" style="padding-left: 0">
                                    <label class="control-label">Tanggal Pembelian</label>
                                    <input type="text"  name="tanggal_" class="form-control has-feedback-left active tanggal-putih" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" readonly="">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <input class="form-control" type="hidden" name="tanggal">
                                </div>
                                <!-- <div class="form-group controls xdisplay_inputx has-feedback col-md-6" style="padding-right: 0">
                                    <label class="control-label">Tanggal Pakai</label>
                                    <input type="text"  name="tanggal_a_" class="form-control has-feedback-left active" id="single_cal3_" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <input class="form-control" type="hidden" name="tanggal_a">
                                </div> -->
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Harga</label>
                                    <input class="form-control input_cek angka" type="text" name="harga_" required="">
                                    <input class="form-control input_cek" type="hidden" name="harga">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nilai Residu</label>
                                    <input class="form-control input_cek angka" type="text" name="residu_">
                                    <input class="form-control input_cek" type="hidden" name="residu">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Sumber Kas</label>
                                    <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="BankContainer">
                                    <label class="control-label">Pilih Rekening Bank</label>
                                    <select name="bank" id="bank" class="select2_single form-control">
                                        <option value="" id="default">Pilih Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="control-label">Nomor Transaksi</label>
                                    <input class="form-control" type="text" name="no_transaksi">
                                </div> --}}
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control" type="text" name="keterangan">
                                </div>
                                    <input type="hidden" name="kas" />
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->


<!-- <div class="row"> -->
    <div class="col-md-12">
        <div class="x_panel">
              <div class="x_title">
                <h2>Daftar Kendaraan</h2>
                <ul class="nav navbar-right panel_toolbox" style="padding-left: 100px">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePiutangBank">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Polisi</th>
                            <th>Nama Pemilik</th>
                            <th>Umur Ekonomis</th>
                            <th>Tahun Perakitan</th>
                            <th>Harga</th>
                            <th>Nilai Saat ini</th>
                            <th>Nilai Residu</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                            <th width="90px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($kendaraans as $num => $kendaraan)
                        <tr id="{{$kendaraan->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $kendaraan->nopol }}</td>
                            <td>{{ $kendaraan->atas_nama }}</td>
                            <td>{{ $kendaraan->umur }} Tahun</td>
                            <td>{{ $kendaraan->tahun_perakitan }}</td>
                            <td align="right">{{ \App\Util::duit($kendaraan->harga) }}</td>
                            <td align="right">{{ \App\Util::duit($kendaraan->nominal) }}</td>
                            <td align="right">{{ \App\Util::duit($kendaraan->residu) }}</td>
                            <td class="text-right">{{ $kendaraan->created_at->format('d-m-Y') }}</td>
                            @if($kendaraan->keterangan!==NULL)
                                <td>{{ $kendaraan->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                            <td class="text-left">
                                <a href="{{ url('kendaraan/show/'.$kendaraan->id) }}" class="btn btn-xs btn-pelihara" id="btnBayar" title="Pemeliharaan" data-placement="bottom" data-toggle="tooltip">
                                    <i class="fa fa-chain"></i>
                                </a><br>
                                <a href="#myModal" data-target="#myModal" data-toggle="modal" class="btn btn-xs btn-danger" id="btnRusak" title="Kendaraan Rusak/Hilang" data-placement="bottom">
                                    <i class="fa fa-trash"></i>
                                </a><br>
                                <a href="#myModal_jual" data-target="#myModal_jual" data-toggle="modal" class="btn btn-xs btn-primary" id="btnJual" title="Jual Kendaraan" data-placement="bottom">
                                    <i class="fa fa-money"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12">
        <div class="x_panel">
              <div class="x_title">
                <h2>Riwayat Kendaraan</h2>
                <ul class="nav navbar-right panel_toolbox" style="padding-left: 100px">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tabelOff">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Polisi</th>
                            <th>Nama Pemilik</th>
                            <th>Umur</th>
                            <th>Tahun Perakitan</th>
                            <th>Harga</th>
                            <th>Nilai Saat ini</th>
                            <th>Nilai Residu</th>
                            <th>Tanggal Dipindahkan</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rusak_kendaraans as $num => $rusak_kendaraan)
                        <tr id="{{$rusak_kendaraan->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $rusak_kendaraan->nopol }}</td>
                            <td>{{ $rusak_kendaraan->atas_nama }}</td>
                            <td>{{ $rusak_kendaraan->umur }} Tahun</td>
                            <td>{{ $rusak_kendaraan->tahun_perakitan }}</td>
                            <td align="right">{{ \App\Util::duit($rusak_kendaraan->harga) }}</td>
                            <td align="right">{{ \App\Util::duit($rusak_kendaraan->nominal) }}</td>
                            <td align="right">{{ \App\Util::duit($rusak_kendaraan->residu) }}</td>
                            <td class="text-right">{{ $rusak_kendaraan->updated_at->format('d-m-Y') }}</td>
                            @if($rusak_kendaraan->keterangan!==NULL)
                                <td>{{ $rusak_kendaraan->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content" id="formModal">
            <form method="post" action="{{ url('kendaraan/rusak') }}" class="form-horizontal">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="modal_info"> <h4 class="modal-title">Keterangan Kendaraan <i id="modal_nama"></i> Mengalami Rusak/Hilang</h4> </span>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" id="modal_id">
                    <div class="form-group" id="label_modal">
                        <label class="control-label">Keterangan</label>
                        <input class="form-control" type="text" name="keterangan" required="">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-default">Simpan</button>
                  </div>
              </form>
        </div>
      </div>
</div>

<div class="modal fade" id="myModal_jual" role="dialog">
      <div class="modal-dialog modal-md" id="formSimpanContainerJual">
        <div class="modal-content" id="formModal">
            <form method="post" action="{{ url('kendaraan/jual') }}" class="form-horizontal">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="modal_info"> <h4 class="modal-title">Penjualan Aset <i id="modal_jual_nama"></i></h4> </span>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" id="modal_jual">
                    <div class="form-group">
                        <label class="control-label">Nominal Penjualan</label>
                        <input class="form-control input_cek_jual angka" type="text" name="nominal_" required="">
                        <input class="form-control input_cek_jual" type="hidden" name="nominal">
                    </div>
                    <div class="form-group" id="label_modal">
                        <label class="control-label">Keterangan</label>
                        <input class="form-control input_cek_jual" type="text" name="keterangan" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kas Tujuan</label>
                        <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="button" id="btnTunaiJual" class="btn btn-default">Tunai</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" id="btnBankJual" class="btn btn-default">Bank</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="BankContainerJual">
                        <label class="control-label">Pilih Rekening Bank</label>
                        <select name="bank" id="bank_jual" class="form-control select2_single">
                            <option value="" id="default">Pilih Bank</option>
                            @foreach($banks as $bank)
                            <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="kas" />
                    <button type="submit" name="submit" id="btnSimpanJual" class="btn btn-default">Simpan</button>
                  </div>
              </form>
        </div>
      </div>
</div>
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kendaraan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kendaraan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kendaraan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kendaraan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kendaraan berhasil dipindahkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Kendaraan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tablePiutangBank').DataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true
        });
        $('#tabelOff').DataTable();

        $(document).ready(function() {
              $('[data-toggle="modal"][title]').tooltip();

            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');

            $('#BankContainerJual').hide();
            $('#BankContainerJual').find('input').val('');
            $('#btnTunaiJual').removeClass('btn-default');
            $('#btnTunaiJual').addClass('btn-success');
            $('#formSimpanContainerJual').find('input[name="kas"]').val('tunai');

            // $('#single_cal3').daterangepicker({
      //         	singleDatePicker: true,
      //         	calender_style: "picker_3"
      //       }, function(start, end, label) {
      //         	var date = start.toISOString().substr(0,10);
      //         	$('input[name="tanggal"]').val(date);
      //       });

            $('#single_cal3').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                  calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start, end, label) {
                  var date = start.toISOString().substr(0,10);
                  $('input[name="tanggal"]').val(date);
            });

            // $('#single_cal3_').daterangepicker({
            //   	singleDatePicker: true,
            //   	calender_style: "picker_3"
            // }, function(start, end, label) {
            // 	console.log(start.toISOString());
            //   	var date = start.toISOString().substr(0,10);
            //   	$('input[name="tanggal_a"]').val(date);
            // });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            btnSimpan();
        });

        $(window).on('load', function(event) {

            var url = "{{ url('perkap/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			

            $.get(url, function(data) {
                if (data.perkap === null) {
                    var kode = int4digit(1);
                    var kode = kode + '/KM/' + tanggal;
                    console.log('kode');
                } else {
                    var kode = data.perkap.kode;
                    var mm_transaksi = kode.split('/')[2];
                    var yyyy_transaksi = kode.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode = kode + '/KM/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode.split('/')[0]) + 1);
                        kode = kode + '/KM/' + tanggal_transaksi;				}
                }
                // console.log(kode);
                $('input[name="kode"]').val(kode);
            });
        });

        $(document).on('click', '#btnTunaiJual', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBankJual').removeClass('btn-success');
            $('#btnBankJual').addClass('btn-default');

            $('#BankContainerJual').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainerJual').find('input[name="kas"]').val('tunai');
            btnSimpanJual();
        });

        $(document).on('click', '#btnBankJual', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunaiJual').removeClass('btn-success');
            $('#btnTunaiJual').addClass('btn-default');

            $('#BankContainerJual').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainerJual').find('input[name="kas"]').val('bank');
            btnSimpanJual();
        });

        $(document).on('click', 'button[data-target="#collapse-kendaraan"]', function(event) {
            event.preventDefault();
            
            if ($(this).hasClass('tampil')) {
                $(this).removeClass('tampil');
                $(this).addClass('sembunyit');
                $(this).find('i').removeClass('fa-chevron-up');
                $(this).find('i').addClass('fa-chevron-down');
            } else if ($(this).hasClass('sembunyit')) {
                $(this).removeClass('sembunyit');
                $(this).addClass('tampil');
                $(this).find('i').removeClass('fa-chevron-down');
                $(this).find('i').addClass('fa-chevron-up');
            }
        });

        $(document).on('click', '#btnRusak', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();	
            
            $('#modal_id').val(id);
            $('#modal_nama').text(nama);
        });

        $(document).on('click', '#btnJual', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();	
            
            $('#modal_jual').val(id);
            $('#modal_jual_nama').text(nama);
        });

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }

        $(document).on('keyup', '.input_cek_jual', function(event) {
            event.preventDefault();

            btnSimpanJual();
        });

        $(document).on('change', '#bank_jual', function(event) {
            event.preventDefault();

            btnSimpanJual();
        });

        function btnSimpanJual() {
            if ($('#btnBankJual').hasClass('btn-success')){
                var bank = $('#bank_jual').val();
                console.log(bank+'cek');
                if (bank != ''){
                    $('#btnSimpanJual').prop('disabled', false);
                } else {
                    $('#btnSimpanJual').prop('disabled', true);
                }
            } else {
                $('#btnSimpanJual').prop('disabled', false);
            }
        }
    </script>
@endsection
