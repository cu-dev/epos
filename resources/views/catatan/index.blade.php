@extends('layouts.admin')

@section('title')
    <title>EPOS | Catatan Struk / Faktur</title>
@endsection

@section('style')
    <style media="screen">
        .btnSimpan {
            margin: 0;
        }
        .lead {
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Struk / Faktur</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="formSimpanContainer">
                            <form class="form-horizontal" action="{{url('catatan')}}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-12">
                                    <label class="control-label">Jenis Catatan</label>
                                    <select name="jenis" class="form-control select2_single" required="">
                                        <option value="">Pilih Jenis Catatan</option>
                                        <option value="po_beli">PESANAN PEMBELIAN</option>
                                        <option value="surat_jalan">SURAT JALAN</option>
                                        <option value="faktur">FAKTUR</option>
                                        <option value="faktur_kredit">FAKTUR KREDIT</option>
                                        <option value="deposito">DEPOSITO PELANGGAN</option>
                                        <option value="struk">PENJUALAN</option>
                                        <option value="pengambilan">PEMERIKSAAN PENGAMBILAN BARANG</option>
                                        <option value="retur">RETUR PENJUALAN</option>
                                        {{-- <option value="invoice">INVOICE</option> --}}
                                        {{-- <option value="surat">SURAT JALAN</option> --}}
                                    </select>
                                </div>
                                <div class="form-group col-xs-12" id="baris1_group">
                                    <label class="control-label">Baris 1</label>
                                    <textarea name="baris[]" id="baris_1" class="form-control"></textarea>
                                    <span id="baris_1_label" class="text-danger"></label>
                                </div>
                                <div class="form-group col-xs-12"  id="baris2_group">
                                    <label class="control-label">Baris 2</label>
                                    <textarea name="baris[]" id="baris_2" class="form-control"></textarea>
                                    <span id="baris_2_label" class="text-danger"></label>
                                </div>
                                <div class="form-group col-xs-12" id="baris3_group">
                                    <label class="control-label">Baris 3</label>
                                    <textarea name="baris[]" id="baris_3" class="form-control"></textarea>
                                    <span id="baris_3_label" class="text-danger"></label>
                                </div>
                                <div class="form-group col-xs-12">
                                    <button class="btn btn-sm btn-success pull-left btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Simpan</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- po beli --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Pesanan Pembelian</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($po_belis as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {{-- <h4>Operator : {{ $periode->user->nama }}</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- surat jalan --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Surat Jalan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-transform: uppercase;">
                                                {{ $surat_jalan->catatan }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            {{-- surat faktur --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Faktur</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fakturs as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {{-- <h4>Operator : {{ $periode->user->nama }}</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- surat faktur kredit --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Faktur Kredit</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($faktur_kredits as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {{-- <h4>Operator : {{ $periode->user->nama }}</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            {{-- surat faktur deposito --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Deposito Pelanggan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($depositos as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {{-- <h4>Operator : {{ $periode->user->nama }}</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- surat struk --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Penjualan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($struk as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            {{-- surat pengambilan --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Pemeriksaan Pengambilan Barang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pengambilan as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- surat retur --}}
            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Catatan Retur Penjualan</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
                                    <thead>
                                        <tr>
                                            <th>Baris</th>
                                            <th>Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($returs as $i => $faktur)
                                        <tr>
                                            <td width="10%">{{ $i+1 }}</td>
                                            <td style="text-transform: uppercase;">
                                                {{ $faktur->catatan }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Money Limit berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Money Limit gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Catatan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Money Limit gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Money Limit berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Money Limit gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        var satu_arr = ['surat_jalan'];
        var deposito_arr = ['deposito'];
        var thermal_arr = ['struk', 'pengambilan', 'retur', 'po_beli'];
        var tiga_arr = ['faktur', 'faktur_kredit'];
        var catatans = null;

        $(document).ready(function() {
            $(".select2_single").select2({
                width: '100%',
                allowClear: true
            });

            catatans = '{{ $catatans }}';
            catatans = catatans.replace(/&quot;/g, '"');
            catatans = JSON.parse(catatans);

            $('#baris_2').prop('readonly', true);
            $('#baris_3').prop('readonly', true);

            $('#baris_1').val('');
            $('#baris_2').val('');
            $('#baris_3').val('');

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 
        });

        /*$(document).on('keyup', '#baris_1', function(event) {
            event.preventDefault();

            var jenis = $('select[name="jenis"]').val();
            var $baris_1 = $(this);
            var $baris_2 = $('#baris_2');
            var $baris_3 = $('#baris_3');

            var baris_1 = $baris_1.val();
            var baris_2 = $baris_2.val();
            var baris_3 = $baris_3.val();
            console.log(baris_1.length);
            if(jenis != 'struk' && jenis != 'pengambilan'){
                if (baris_1.length + baris_2.length + baris_3.length > 3 * 45) {
                    var text = baris_1.slice(0, -1);
                    $baris_1.val(text);
                }

                $baris_2.prop('readonly', false);
            }else{
                if (baris_1.length > 45) {
                    var text = baris_1.slice(0, -1);
                    $baris_1.val(text);
                }
            }
        });

        $(document).on('keyup', '#baris_2', function(event) {
            event.preventDefault();

            var $baris_1 = $('#baris_1');
            var $baris_2 = $(this);
            var $baris_3 = $('#baris_3');

            var baris_1 = $baris_1.val();
            var baris_2 = $baris_2.val();
            var baris_3 = $baris_3.val();
            console.log(baris_2.length);

            if (baris_1.length + baris_2.length + baris_3.length > 3 * 45) {
                var text = baris_2.slice(0, -1);
                $baris_2.val(text);
            }

            $baris_3.prop('readonly', false);
        });

        $(document).on('keyup', '#baris_3', function(event) {
            event.preventDefault();

            var $baris_1 = $('#baris_1');
            var $baris_2 = $('#baris_2');
            var $baris_3 = $(this);

            var baris_1 = $baris_1.val();
            var baris_2 = $baris_2.val();
            var baris_3 = $baris_3.val();
            console.log(baris_3.length);

            if (baris_1.length + baris_2.length + baris_3.length > 3 * 45) {
                var text = baris_3.slice(0, -1);
                $baris_3.val(text);
            }
        });*/

        function keyup_all () {
            $('#baris_1').trigger('keyup');
            $('#baris_2').trigger('keyup');
            $('#baris_3').trigger('keyup');
        }

        $(document).on('change', 'select[name="jenis"]', function(event) {
            var jenis = $(this).val();
            if (tiga_arr.indexOf(jenis) != -1 && satu_arr.indexOf(jenis) != -1) {
                $('#baris_2').prop('readonly', true);
                $('#baris_3').prop('readonly', true);
            } else if (thermal_arr.indexOf(jenis) != -1) {
                $('#baris_2').prop('readonly', false);
                $('#baris_3').prop('readonly', false);
            }

            var catatan_temp = [];
            if (jenis != '') {
                for (var i = 0; i < catatans.length; i++) {
                    if (catatans[i].jenis_catatan == jenis) catatan_temp.push(catatans[i]);
                }

                for (var i = 0; i < 3; i++) {
                    if (catatan_temp.length == 3) {
                        $('#baris_'+(i+1)).val(catatan_temp[i].catatan.toUpperCase()).trigger('keyup');
                    } else {
                        $('#baris_1').val(catatan_temp[0].catatan.toUpperCase());
                        $('#baris_2').val('');
                        $('#baris_3').val('');
                    }
                }
            }

            // $('#baris_1').val('');
            // $('#baris_2').val('');
            // $('#baris_3').val('');
            keyup_all();
            $('#baris_1_label').hide();
            $('#baris_1_label').parent('.form-group').first().removeClass('has-error');
            $('#baris_2_label').hide();
            $('#baris_2_label').parent('.form-group').first().removeClass('has-error');
            $('#baris_3_label').hide();
            $('#baris_3_label').parent('.form-group').first().removeClass('has-error');
        });

        $(document).on('keyup', '#baris_1', function(event) {
            event.preventDefault();

            var kalimat = $(this).val();
            var baris_2 = $('#baris_2').val();
            var n = kalimat.length;
            var m = baris_2.length;
            console.log('1'+n);
            var jenis = $('select[name="jenis"').val();

            if(tiga_arr.indexOf(jenis) != -1){
                if (n <= 135) {
                    $('#baris_2').prop('readonly', false);
                }

                if (n > 45 && n <= 90 && m > 0) {
                    $('#baris_3').prop('readonly', true);
                    $('#baris_3').val('');
                    if (m >= 90) {
                        $(this).parent('.form-group').first().addClass('has-error');
                        $('#baris_1_label').text('Karakter tidak boleh lebih dari 90 karakter');
                        $('#baris_1_label').show();
                    } else{
                        $(this).parent('.form-group').first().removeClass('has-error');
                        $('#baris_1_label').hide();
                    }
                }

                if (n > 90) {
                    $('#baris_2').prop('readonly', true);
                    $('#baris_3').prop('readonly', true);
                    $('#baris_2').val('');
                    $('#baris_3').val('');
                }

                if (n > 121) {
                    if (n >= 121) {
                        $(this).parent('.form-group').first().addClass('has-error');
                        $('#baris_1_label').text('Karakter tidak boleh lebih dari 121 karakter');
                        $('#baris_1_label').show();
                    } else{
                        $(this).parent('.form-group').first().removeClass('has-error');
                        $('#baris_1_label').hide();
                    }
                }
            } else if(deposito_arr.indexOf(jenis) != -1){
                if (n <= 84) {
                    $('#baris_2').prop('readonly', false);
                }

                if (n > 28 && n <= 56 && m > 0) {
                    $('#baris_3').prop('readonly', true);
                    $('#baris_3').val('');
                    if (m >= 90) {
                        $(this).parent('.form-group').first().addClass('has-error');
                        $('#baris_1_label').text('Karakter tidak boleh lebih dari 56 karakter');
                        $('#baris_1_label').show();
                    } else{
                        $(this).parent('.form-group').first().removeClass('has-error');
                        $('#baris_1_label').hide();
                    }
                }

                if (n > 56) {
                    $('#baris_2').prop('readonly', true);
                    $('#baris_3').prop('readonly', true);
                    $('#baris_2').val('');
                    $('#baris_3').val('');
                }

                if (n > 84) {
                    if (n >= 84) {
                        $(this).parent('.form-group').first().addClass('has-error');
                        $('#baris_1_label').text('Karakter tidak boleh lebih dari 84 karakter');
                        $('#baris_1_label').show();
                    } else{
                        $(this).parent('.form-group').first().removeClass('has-error');
                        $('#baris_1_label').hide();
                    }
                }
            } else if (thermal_arr.indexOf(jenis) != -1) {
                if (n > 35) {
                    $('#baris1_group').addClass('has-error');
                    $('#baris_1_label').text('Karakter tidak boleh lebih dari 35 karakter');
                    $('#baris_1_label').show();
                } else{
                    $('#baris1_group').removeClass('has-error');
                    $('#baris_1_label').hide();
                }
            } else if (satu_arr.indexOf(jenis) != -1) {
                if (n > 45) {
                    $('#baris1_group').addClass('has-error');
                    $('#baris_1_label').text('Karakter tidak boleh lebih dari 45 karakter');
                    $('#baris_1_label').show();
                } else{
                    $('#baris1_group').removeClass('has-error');
                    $('#baris_1_label').hide();
                }

                $('#baris_2').prop('readonly', true);
                $('#baris_3').prop('readonly', true);
                $('#baris_2').val('');
                $('#baris_3').val('');
            }

            $('#btnSimpan').prop('disabled', cekEror());
        });

        $(document).on('keyup', '#baris_2', function(event) {
            event.preventDefault();

            var kalimat = $(this).val();
            var baris_1 = $('#baris_1').val();
            var n = kalimat.length;
            var m = baris_1.length;
            console.log('2'+n);
            var jenis = $('select[name="jenis"').val();

            if(tiga_arr.indexOf(jenis) != -1){
                if (m <= 45) {
                    if (n <= 45) {
                        $('#baris_3').prop('readonly', false);
                    }

                    if (n > 45) {
                        $('#baris_3').prop('readonly', true);
                        $('#baris_3').val('');
                    }

                    if (n >= 90) {
                        $('#baris2_group').addClass('has-error');
                        $('#baris_56_label').text('Karakter tidak boleh lebih dari 90 karakter');
                        $('#baris_56_label').show();
                    } else{
                        $('#baris2_group').removeClass('has-error');
                        $('#baris_56_label').hide();
                    }
                } else {
                    if (n > 45) {
                        $('#baris_3').prop('readonly', true);
                        $('#baris_3').val('');
                        $('#baris2_group').addClass('has-error');
                        $('#baris_2_label').text('Karakter tidak boleh lebih dari 45 karakter');
                        $('#baris_2_label').show();
                    } else{
                        $('#baris2_group').removeClass('has-error');
                        $('#baris_2_label').hide();
                    }
                }
            } else if(deposito_arr.indexOf(jenis) != -1){
                if (m <= 28) {
                    if (n <= 28) {
                        $('#baris_3').prop('readonly', false);
                    }

                    if (n > 28) {
                        $('#baris_3').prop('readonly', true);
                        $('#baris_3').val('');
                    }

                    if (n >= 56) {
                        $('#baris2_group').addClass('has-error');
                        $('#baris_2_label').text('Karakter tidak boleh lebih dari 56 karakter');
                        $('#baris_2_label').show();
                    } else{
                        $('#baris2_group').removeClass('has-error');
                        $('#baris_2_label').hide();
                    }
                } else {
                    if (n > 28) {
                        $('#baris_3').prop('readonly', true);
                        $('#baris_3').val('');
                        $('#baris2_group').addClass('has-error');
                        $('#baris_2_label').text('Karakter tidak boleh lebih dari 28 karakter');
                        $('#baris_2_label').show();
                    } else{
                        $('#baris2_group').removeClass('has-error');
                        $('#baris_2_label').hide();
                    }
                }
            } else if (thermal_arr.indexOf(jenis) != -1) {
                if (n > 35) {
                    $('#baris2_group').addClass('has-error');
                    $('#baris_2_label').text('Karakter tidak boleh lebih dari 35 karakter');
                    $('#baris_2_label').show();
                } else {
                    $('#baris2_group').removeClass('has-error');
                    $('#baris_2_label').hide();
                }
            }

            $('#btnSimpan').prop('disabled', cekEror());
        });

        $(document).on('keyup', '#baris_3', function(event) {
            event.preventDefault();

            var kalimat = $(this).val();
            var n = kalimat.length;
            console.log('3'+n);
            var jenis = $('select[name="jenis"').val();

            if(tiga_arr.indexOf(jenis) != -1){
                if (n > 45) {
                    $('#baris3_group').addClass('has-error');
                    $('#baris_3_label').text('Karakter tidak boleh lebih dari 45 karakter');
                    $('#baris_3_label').show();
                } else{
                    $('#baris3_group').removeClass('has-error');
                    $('#baris_3_label').hide();
                } 
            } else if(deposito_arr.indexOf(jenis) != -1){
                if (n > 28) {
                    $('#baris3_group').addClass('has-error');
                    $('#baris_3_label').text('Karakter tidak boleh lebih dari 28 karakter');
                    $('#baris_3_label').show();
                } else{
                    $('#baris3_group').removeClass('has-error');
                    $('#baris_3_label').hide();
                } 
            } else if (thermal_arr.indexOf(jenis) != -1) {
                if (n > 35) {
                    $('#baris3_group').addClass('has-error');
                    $('#baris_3_label').text('Karakter tidak boleh lebih dari 35 karakter');
                    $('#baris_3_label').show();
                } else{
                    $('#baris3_group').removeClass('has-error');
                    $('#baris_3_label').hide();
                }
            }
            $('#btnSimpan').prop('disabled', cekEror());
        });

        function cekEror() {
            var bool = false;
            $('.form-group').each(function(index, el) {
                if($(el).hasClass('has-error')) bool = true;
            });

            return bool;
        }

    </script>
@endsection

