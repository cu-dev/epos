@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Data Pelanggan</title>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Ubah Data Pelanggan</h2>
                <a href="{{ url('pelanggan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('pelanggan/'.$pelanggan->id) }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="put">
                    <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Jenis Identitas KTP / SIM</label>
                            <select id="StatusIdentitas" name="status_identitas" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Identitas</option>
                                <option value="1">KTP</option>
                                <option value="2">SIM</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Identitias KTP / SIM</label>
                            <input class="form-control" type="text" name="ktp"  value="{{ $pelanggan->ktp }}" required="">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nama Pelanggan</label>
                            <input class="form-control" type="text" name="nama"  value="{{ $pelanggan->nama }}" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nama Toko</label>
                            <input class="form-control" type="text" name="toko"  value="{{ $pelanggan->toko }}" required="">
                            <p class="">Jika pelanggan lupa/tidak memiliki nama toko, isilah dengan nama pelanggan.</p>
                            <p id="TokoAlert" class="text-danger">Nama Toko tidak boleh lebih dari 21 karakter!</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alamat </label>
                            <textarea style="margin: 0px 0.5px 0px 0px;height: 104px;width: 505px;" class="form-control" name="alamat" required="">{{ $pelanggan->alamat }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Provinsi </label>
                            <select name="provinsi_id" class="select2_single form-control" required="">
                                <option value="">Pilih Provinsi</option>
                                @foreach($provinsis as $provinsi)
                                    <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                @endforeach
                                <option id="lain_prov">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kabupaten </label>
                            <select name="kabupaten_id" class="select2_single form-control" required="">
                                <option value="">Pilih Kabupaten</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kecamatan </label>
                            <select name="kecamatan_id" class="select2_single form-control" required="">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Desa </label>
                            <select name="desa_id" class="select2_single form-control" required="">
                                <option value="">Pilih Desa</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Telepon</label>
                            <input class="form-control" type="text" name="telepon" value="{{ $pelanggan->telepon }}" required="">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Fax</label>
                            <input class="form-control" type="text" name="fax" value="{{ $pelanggan->fax }}">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="text" name="email" value="{{ $pelanggan->email }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Rekening</label>
                            <input class="form-control" type="text" name="no_rekening" value="{{ $pelanggan->no_rekening }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rekening Atas Nama</label>
                            <input class="form-control" type="text" name="atas_nama" value="{{ $pelanggan->atas_nama }}">
                        </div>
                        <div class="form-group" style="padding-top: 10px">
                            <label class="control-label">Bank</label>
                            <input class="form-control" type="text" name="bank" value="{{ $pelanggan->bank }}">
                        </div>
                        @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                            <div class="form-group">
                                <label class="control-label">Status Pelanggan </label>
                                <select class="form-control select2_single" id="level" name="level">
                                    <option value="">Pilih Status Pelanggan</option>
                                    @foreach ($enumLevel as $val)
                                        {{-- @if($pelanggan->level == $val) --}}
                                            {{-- <option value="{{ $val }}" selected>  --}}
                                            {{-- <option value="{{ $val }}">  --}}
                                            @if($val == 'eceran')
                                                <option value="{{ $val }}">Eceran</option>
                                            @else
                                                <option value="{{ $val }}">Grosir</option>
                                            @endif
                                        {{-- @else
                                            <option value="{{ $val }}"> 
                                            @if($val == 'eceran')
                                                Eceran
                                            @else
                                                Grosir
                                            @endif
                                        </option>
                                        @endif --}}
                                    @endforeach
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label">Diskon dengan Persen</label>
                                <input class="form-control" type="text" name="diskon_persen" value="{{ $pelanggan->diskon_persen }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jumlah Potongan</label>
                                <input class="form-control angka" type="text" name="potongan_" value="{{ number_format($pelanggan->potongan) }}">
                                <input class="form-control hide" type="text" name="potongan" value="{{ $pelanggan->potongan }}">
                            </div> -->
                            <div class="form-group">
                                <label class="control-label">Limit Jumlah Piutang (Rp)</label>
                                <input class="form-control angka" id="input_limit_piutang" type="text" name="limit_jumlah_piutang_">
                                <input class="form-control hide" type="text" name="limit_jumlah_piutang" value="{{ $pelanggan->limit_jumlah_piutang }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Waktu Jatuh Tempo (Hari)</label>
                                <input class="form-control" id="" type="text" name="jatuh_tempo" value="{{ $pelanggan->jatuh_tempo }}">
                            </div>
                        @endif
                    </div>
                    </div>
                    <div class="form-group pull-right" style="margin-bottom: 0;">
                        <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Ubah</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="display: none;">
        @if($pelanggan->desa_id != NULL)
            <span id="status">1</span>
            <span id="desa">{{ $pelanggan->desa->id }}</span>
            <span id="kecamatan">{{ $pelanggan->desa->kecamatan_id }}</span>
        @else
            <span id="status">0</span>
        @endif
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var desa_x = '';
        var kecamatan_x = '';
        var camat = '';
        var kabupaten_x = '';
        var paten = '';
        var kab_stat = '';
        var kec_stat = '';
        var des_stat = '';
        var limit_piutang = '{{ \App\Util::angka($pelanggan->limit_jumlah_piutang) }}';
        var level_pelanggan = '';
        var status_identitas = '';

        $(document).ready(function() {
            var url = "{{ url('pelanggan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                // allowClear: true
            });

            $('#input_limit_piutang').val(limit_piutang);
            // $('#btnSimpan').prop('disabled', true);

            pelanggan = '{{ $pelanggan->level }}';
            $('#level').val(pelanggan).change();

            status_identitas = '{{ $pelanggan->status_identitas }}';
            $('#StatusIdentitas').val(status_identitas).change();

            $('#TokoAlert').hide();
            var status = $('#status').text();
            if(status == 1){
                desa_x = $('#desa').text();
                kecamatan_x = $('#kecamatan').text();
                var url_kab = "{{ url('wilayah') }}" + '/' + kecamatan_x + '/camat.json';
                // console.log(kecamatan_x);
                $.ajax({
                    url: url_kab,
                    type: 'get',
                    async: false,
                    success:function(data) {
                        camat = data.kecamatan;
                        kabupaten_x = camat.kabupaten_id;
                    }
                });

                var url_prov = "{{ url('wilayah') }}" + '/' + kabupaten_x + '/paten.json';
                $.ajax({
                    url: url_prov,
                    type: 'get',
                    async: false,
                    success:function(data) {
                        paten = data.kabupaten;
                        // console.log(paten);
                        provinsi = paten.provinsi_id;
                    }
                });
                $("select[name='provinsi_id']").val(provinsi).trigger("change");

                // console.log("desa " + desa_x + "Kecamatan " + kecamatan_x + "kabupaten " + kabupaten_x);
                kab_stat = 1;
                kec_stat = 1;
                des_stat = 1;
            }
        });

        $(document).on('change', 'select[name="provinsi_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';

            var $select = $('select[name="kabupaten_id"]');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));
            var $select_kec = $('select[name="kecamatan_id"]');
                $select_kec.empty();
                $select_kec.append($('<option value="">Pilih Kecamatan</option>'));
            var $select_des = $('select[name="desa_id"]');
                $select_des.empty();
                $select_des.append($('<option value="">Pilih Desa</option>'));

            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    // console.log(kabupatens[$i]);
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }

                if(kab_stat==1){
                    $("select[name='kabupaten_id']").val(kabupaten_x).trigger("change");
                }
                kab_stat = 0;
            });
        });

        $(document).on('change', 'select[name="kabupaten_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kecamatan.json';

            var $select = $('select[name="kecamatan_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Kecamatan</option>'));

            $.get(url, function(data) {
                var kecamatans = data.kecamatan;
                // console.log(kecamatans);
                for (var i = 0; i < kecamatans.length; i++) {
                    var kecamatan = kecamatans[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
                }
                if(kec_stat==1){
                    // console.log("aya desa " + desa_x + "Kecamatan " + kecamatan_x + "kabupaten " + kabupaten_x);
                    $("select[name='kecamatan_id']").val(kecamatan_x).trigger("change");
                }
                kec_stat = 0;
            });
        });

        $(document).on('change', 'select[name="kecamatan_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/desa.json';

            var $select = $('select[name="desa_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Desa</option>'));


            $.get(url, function(data) {
                var desas = data.desa;
                for (var i = 0; i < desas.length; i++) {
                    var desa = desas[i];
                    $select.append($('<option value="' + desa.id + '">' + desa.nama + '</option>'));
                }
                if(des_stat==1){
                    $("select[name='desa_id']").val(desa_x).trigger("change");
                }
                des_stat = 0;
            });
        });

        /*$(document).on('keyup', 'input[name="kode"]', function(event) {
            var kode = $(this).val();
            var id = {{ $pelanggan->id }};
            var url = "{{ url('pelanggan') }}"+'/'+ kode + '/'+ id +'/kodeJson';
            console.log(url);
             $.get(url, function(data) {
                console.log(data);
                if(data.kode != null && Object.keys(data.kode).length > 0){
                    $('input[name="kode"]').parents('.form-group').addClass('has-error');
                    $('#btnSimpan').prop('disabled', true);
                }else{
                    $('input[name="kode"]').parents('.form-group').removeClass('has-error');
                    $('#btnSimpan').prop('disabled', false);
                }
            });
        });*/

        $(document).on('keyup', 'input[name="ktp"]', function(event) {
            event.preventDefault();

            var ktp = $(this).val();
            var id = {{ $pelanggan->id }};
            var url = "{{ url('pelanggan') }}"+'/'+ ktp + '/'+ id +'/ktpEditJson';
            var ini = $(this);
            // console.log(url);
            $.get(url, function(data) {
                // console.log(data);
                if (isNaN(ktp)) {
                    ini.parents('.form-group').first().addClass('has-error');
                    ini.next('span').removeClass('sembunyi');
                } else {
                    ini.next('span').addClass('sembunyi');
                    if(data.ktp != null && Object.keys(data.ktp).length > 0){
                        $('input[name="ktp"]').parents('.form-group').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('input[name="ktp"]').parents('.form-group').removeClass('has-error');
                        $('#btnSimpan').prop('disabled', false);
                    }
                }

                cek();
            });
        });

        $(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();

            var telepon = $(this).val();
            var id = {{ $pelanggan->id }};
            var url = "{{ url('pelanggan') }}"+'/'+ telepon + '/'+ id +'/teleponEditJson';
            var ini = $(this);
            // console.log(url);

            $.get(url, function(data) {
                // console.log(data);
                if (isNaN(telepon)) {
                    ini.parents('.form-group').first().addClass('has-error');
                    ini.next('span').removeClass('sembunyi');
                } else {
                    ini.next('span').addClass('sembunyi');
                    if(data.telepon != null && Object.keys(data.telepon).length > 0){
                        $('input[name="telepon"]').parents('.form-group').addClass('has-error');
                    }else{
                        $('input[name="telepon"]').parents('.form-group').removeClass('has-error');
                    }
                }
                
                cek();
            });
        });

        $(document).on('keyup', 'input[name="toko"]', function(event) {
            event.preventDefault();

            var toko = $(this).val();
            if (toko == '') {
                $('input[name="toko"]').parents('.form-group').addClass('has-error');
                $('#btnSimpan').prop('disabled', true);
            } else {
                if (toko.length > 21) {
                    $('#TokoAlert').show();
                    $('#btnSimpan').prop('disabled', true);
                } else {
                    $('#TokoAlert').hide();
                    $('input[name="toko"]').parents('.form-group').removeClass('has-error');
                    if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
                }
            }
        });

        $(document).on('keyup', 'input[name="fax"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection


