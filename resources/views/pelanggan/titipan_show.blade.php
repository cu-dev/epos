@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Deposito Pelanggan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
        #tableJenisItem_filter input {
            max-width: 100px;
        }

        /*.kas span {
            padding-top: 5px;
        }*/
    </style>
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row" style="margin-bottom: 0; padding-bottom: 0">
                                <div class="col-md-12">
                                    <h2 id="formSimpanTitle"> Detail Deposito <b>{{ $pelanggan->nama }}</b> [{{ $log->kode_transaksi }}]</h2>
                                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                        <div class="pull-right">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        </div>
                                    </ul>
                                    <a href="{{ url('pelanggan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                                        <i class="fa fa-long-arrow-left"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="pull-left">Saldo Saat ini : {{ \App\Util::ewon($pelanggan->titipan) }}</h5>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-md-1">
                            <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                <div class="pull-right">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </div>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <th class="text-left" colspan="2">Pembayaran</th>
                        </thead>
                        <tbody>
                            @if ($log->nominal_tunai != null && $log->nominal_tunai > 0)
                            <tr>
                                <th>Nominal Tunai</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->nominal_tunai) }}</td>
                            </tr>
                            @endif

                            @if ($log->no_transfer != null)
                            <tr>
                                <th>Nomor Transfer</th>
                                <td class="text-right" style="width: 60%;">{{ $log->no_transfer }}</td>
                            </tr>
                            @endif

                            @if ($log->bank_transfer != null)
                            <tr>
                                <th>Bank Transfer</th>
                                <td class="text-right" style="width: 60%;">{{ $log->banktransfer->nama_bank }} [{{ $log->banktransfer->no_rekening }}]</td>
                            </tr>
                            @endif

                            @if ($log->nominal_transfer != null && $log->nominal_transfer > 0)
                            <tr>
                                <th>Nominal Transfer</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->nominal_transfer) }}</td>
                            </tr>
                            @endif

                            @if ($log->no_kartu != null)
                            <tr>
                                <th>Nomor Kartu</th>
                                <td class="text-right" style="width: 60%;">{{ $log->no_kartu }}</td>
                            </tr>
                            @endif

                            @if ($log->jenis_kartu != null)
                            <tr>
                                <th>Jenis Kartu</th>
                                @if ($log->jenis_kartu == 'debet')
                                    <td class="text-right" style="width: 60%;">Debit</td>
                                @else
                                    <td class="text-right" style="width: 60%;">Kredit</td>
                                @endif
                            </tr>
                            @endif

                            @if ($log->bank_kartu != null)
                            <tr>
                                <th>Bank Kartu</th>
                                <td class="text-right" style="width: 60%;">{{ $log->bankkartu->nama_bank }} [{{ $log->bankkartu->no_rekening }}]</td>
                            </tr>
                            @endif

                            @if ($log->nominal_kartu != null && $log->nominal_kartu > 0)
                            <tr>
                                <th>Nominal Kartu</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->nominal_kartu) }}</td>
                            </tr>
                            @endif

                            @if ($log->no_cek != null)
                            <tr>
                                <th>Nomor Cek</th>
                                <td class="text-right" style="width: 60%;">{{ $log->no_cek }}</td>
                            </tr>
                            @endif

                            @if ($log->bank_cek != null)
                            <tr>
                                <th>Bank Cek</th>
                                <td class="text-right" style="width: 60%;">{{ $log->bankcek->nama_bank }} [{{ $log->bankcek->no_rekening }}]</td>
                            </tr>
                            @endif

                            @if ($log->nominal_cek != null && $log->nominal_cek > 0)
                            <tr>
                                <th>Nominal Cek</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->nominal_cek) }}</td>
                            </tr>
                            @endif

                            @if ($log->no_bg != null)
                            <tr>
                                <th>Nomor BG</th>
                                <td class="text-right" style="width: 60%;">{{ $log->no_bg }}</td>
                            </tr>
                            @endif

                            @if ($log->bank_bg != null)
                            <tr>
                                <th>Bank BG</th>
                                <td class="text-right" style="width: 60%;">{{ $log->bankbg->nama_bank }} [{{ $log->bankbg->no_rekening }}]</td>
                            </tr>
                            @endif

                            @if ($log->nominal_bg != null && $log->nominal_bg > 0)
                            <tr>
                                <th>Nominal BG</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->nominal_bg) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <th>Jumlah Bayar</th>
                                <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($log->jumlah_bayar - $log->nominal_titipan) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Deposito berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();

        $(document).ready(function() {
            var url = "{{ url('pelanggan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

        });
    </script>
@endsection
