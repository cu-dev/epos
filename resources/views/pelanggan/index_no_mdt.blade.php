@extends('layouts.admin')

@section('title')
    <title>EPOS | Pelanggan</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah {
            margin: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        td.capitalize {
            text-transform:capitalize;
        }
        #tableUser .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pelanggan</h2>
                <a href="{{ url('pelanggan/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Pelanggan">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableUser">
                    <thead>
                        <tr>
                            <!-- <th width="3%">No</th>
                            <th width="5%">ID Pelanggan</th>
                            <th>Nama</th>
                            <th>Nama Toko</th>
                            <th>Alamat</th>
                            <th >Telepon</th>
                            <th width="5%">Level</th>
                            <th>Titipan</th>
                            <th width="30%">Aksi</th> -->
                            <th>No</th>
                            <th>ID Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Toko</th>
                            {{-- <th>Alamat</th> --}}
                            {{-- <th>Telepon</th> --}}
                            <th>Status Pelanggan</th>
                            <th>Deposito</th>
                            {{-- <th>Operator</th> --}}
                            <th style="50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pelanggans as $num => $pelanggan)
                        <tr id="{{$pelanggan->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $pelanggan->id }}</td>
                            <td>{{ $pelanggan->nama }}</td>
                            <td>{{ $pelanggan->toko }}</td>
                            {{-- <td>{{ $pelanggan->alamat }}</td> --}}
                            {{-- <td>{{ $pelanggan->telepon }}</td> --}}
                            <td class="capitalize">{{ $pelanggan->level }}</td>
                            <td class="text-right">{{ \App\Util::duit0($pelanggan->titipan) }}</td>
                            {{-- <td>{{ $pelanggan->user->nama }}</td> --}}
                            <td class="tengah-h">
                                <a href="{{ url('pelanggan/show/'.$pelanggan->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pelanggan">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <!-- <a href="{{ url('pelanggan/edit/'.$pelanggan->id) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ url('pelanggan/titipan/'.$pelanggan->id) }}" class="btn btn-xs btn-success" id="btnTitip" data-toggle="tooltip" data-placement="top" title="Deposito">
                                    <i class="fa fa-money"></i>
                                </a> -->
                                @if(in_array(Auth::user()->level_id, [1,2]))
                                    <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Pelanggan">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Pelanggan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableUserOff">
                    <thead>
                        <tr>
                            <!-- <th width="3%">No</th>
                            <th width="5%">ID Pelanggan</th>
                            <th>Nama</th>
                            <th>Nama Toko</th>
                            <th>Alamat</th>
                            <th >Telepon</th>
                            <th width="5%">Level</th>
                            <th>Titipan</th>
                            <th width="30%">Aksi</th> -->
                            <th>No</th>
                            <th>ID Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>Nama Toko</th>
                            {{-- <th>Alamat</th> --}}
                            {{-- <th>Telepon</th> --}}
                            <th>Status Pelanggan</th>
                            <th>Deposito</th>
                            <th style="50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pelanggan_off as $num => $pelanggan)
                        <tr id="{{$pelanggan->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $pelanggan->id }}</td>
                            <td>{{ $pelanggan->nama }}</td>
                            <td>{{ $pelanggan->toko }}</td>
                            {{-- <td>{{ $pelanggan->alamat }}</td> --}}
                            {{-- <td>{{ $pelanggan->telepon }}</td> --}}
                            <td class="capitalize">{{ $pelanggan->level }}</td>
                            <td class="text-right">{{ \App\Util::duit0($pelanggan->titipan) }}</td>
                            <td class="tengah-h">
                                <a href="{{ url('pelanggan/show/'.$pelanggan->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pelanggan">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <!-- <a href="{{ url('pelanggan/edit/'.$pelanggan->id) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ url('pelanggan/titipan/'.$pelanggan->id) }}" class="btn btn-xs btn-success" id="btnTitip" data-toggle="tooltip" data-placement="top" title="Deposito">
                                    <i class="fa fa-money"></i>
                                </a> -->
                                @if(in_array(Auth::user()->level_id, [1,2]))
                                    <button class="btn btn-xs btn-success" id="btnAktif" data-toggle="tooltip" data-placement="top" title="Aktifkan Pelanggan">
                                        <i class="fa fa-check"></i>
                                    </button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pelanggan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggn berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggan berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pelanggan gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data kata sandi berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data kata sandi gagal direset!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $('#tableUser').DataTable();
        $('#tableUserOff').DataTable();

        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama_item + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                // Confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("pelanggan/delete") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            var url = '{{ url('pelanggan/aktif') }}' + '/' + id;

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama_item + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                // Confirmed
                window.open(url, '_self');
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });
    </script>
@endsection
