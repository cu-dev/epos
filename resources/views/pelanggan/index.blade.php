@extends('layouts.admin')

@section('title')
    <title>EPOS | Pelanggan</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah {
            margin: 0;
        }
        #btnHapus,
        #btnAktif {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        td.capitalize {
            text-transform:capitalize;
        }
        #tableUser .btn,
        #tableUserOff .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pelanggan</h2>
                <a href="{{ url('pelanggan/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Pelanggan">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('pelanggan/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="id">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableUser" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="id">No</th>
                                <th class="sorting" field="id">ID Pelanggan</th>
                                <th class="sorting" field="nama">Nama Pelanggan</th>
                                <th class="sorting" field="toko">Nama Toko</th>
                                <th class="sorting" field="level">Status Pelanggan</th>
                                <th class="sorting" field="titipan">Deposito</th>
                                <th class="sorting" field="id" style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Pelanggan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('pelanggan/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="id">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableUserOff" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="id">No</th>
                                <th class="sorting" field="id">ID Pelanggan</th>
                                <th class="sorting" field="nama">Nama Pelanggan</th>
                                <th class="sorting" field="toko">Nama Toko</th>
                                <th class="sorting" field="level">Status Pelanggan</th>
                                <th class="sorting" field="titipan">Deposito</th>
                                <th class="sorting" field="id" style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pelanggan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggn berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pelanggan berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pelanggan gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data kata sandi berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data kata sandi gagal direset!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('pelanggan/mdt1') }}";
        var mdt2 = "{{ url('pelanggan/mdt2') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var pelanggan = data[i];
                        var buttons = pelanggan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pelanggan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Pelanggan">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${pelanggan.id}">
                                    <td>${nomor}</td>
                                    <td>${pelanggan.id}</td>
                                    <td>${pelanggan.nama}</td>
                                    <td>${pelanggan.toko}</td>
                                    <td class="capitalize">${pelanggan.level}</td>
                                    <td class="text-right">${pelanggan.titipan}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var pelanggan = data[i];
                        var buttons = pelanggan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pelanggan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.aktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnAktif" data-toggle="tooltip" data-placement="top" title="Aktifkan Pelanggan">
                                    <i class="fa fa-check"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${pelanggan.id}">
                                    <td>${nomor}</td>
                                    <td>${pelanggan.id}</td>
                                    <td>${pelanggan.nama}</td>
                                    <td>${pelanggan.toko}</td>
                                    <td class="capitalize">${pelanggan.level}</td>
                                    <td class="text-right">${pelanggan.titipan}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama_item + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                // Confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("pelanggan/delete") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            var url = '{{ url('pelanggan/aktif') }}' + '/' + id;

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama_item + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                // Confirmed
                window.open(url, '_self');
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

    </script>
@endsection
