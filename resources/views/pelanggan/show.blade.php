@extends('layouts.admin')

@section('title')
    <title>EPOS | Pelanggan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Detail Pelanggan</h2>
                @if ($pelanggan->aktif == 1)
                    <a href="{{ url('pelanggan/titipan/'.$pelanggan->id) }}" class="btn btn-sm btn-purple pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Deposito Pelanggan">
                        <i class="fa fa-money"></i>
                    </a>
                    @if(in_array(Auth::user()->level_id , [1,2]))
                        <a href="{{ url('pelanggan/edit/'.$pelanggan->id) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Pelanggan">
                            <i class="fa fa-edit"></i>
                        </a>
                    @endif
                @endif
                <a href="{{ URL::previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
                        <table class="table table-striped">
                            <tr>
                                <td  width="30%"> ID Pelanggan </td>
                                <td width="1%"> : </td>
                                <td> {{ $pelanggan->id }} </td>
                            </tr>
                            <tr>
                                <td  width="30%"> Jenis Identitas </td>
                                <td width="1%"> : </td>
                                @if ($pelanggan->status_identitas == 1)
                                    <td> KTP </td>
                                @else
                                    <td> SIM </td>
                                @endif
                            </tr>
                            <tr>
                                <td  width="30%"> Nomor KTP </td>
                                <td width="1%"> : </td>
                                <td> {{ $pelanggan->ktp }} </td>
                            </tr>
                            <tr>
                                <td  width="30%"> Nama Pelanggan</td>
                                <td width="1%"> : </td>
                                <td> {{ $pelanggan->nama }} </td>
                            </tr>
                            <tr>
                                <td  width="30%"> Nama Toko</td>
                                <td width="1%"> : </td>
                                <td> {{ $pelanggan->toko }} </td>
                            </tr>
                            <tr>
                                <td> Alamat </td>
                                <td> : </td>
                                <td> {{ $pelanggan->alamat }} </td>
                            </tr>
                            <tr>
                                <td> Desa </td>
                                <td> : </td>
                                <td> 
                                    @if($pelanggan->desa_id != NULL)
                                        {{ $pelanggan->desa->nama }} 
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td> Kecamatan </td>
                                <td> : </td>
                                <td>
                                    @if($pelanggan->desa_id != NULL)
                                        {{ $kecamatan->kecamatan->nama }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td> Kabupaten </td>
                                <td> : </td>
                                <td> 
                                    @if($pelanggan->desa_id != NULL)
                                        {{ $kabupaten->kabupaten->nama }} 
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td> Telepon </td>
                                <td> : </td>
                                <td> {{ $pelanggan->telepon }} </td>
                            </tr>
                            <tr>
                                <td> Fax </td>
                                <td> : </td>
                                <td> {{ $pelanggan->fax }} </td>
                            </tr>
                            <tr>
                                <td> Email </td>
                                <td> : </td>
                                <td> {{ $pelanggan->email }} </td>
                            </tr>
                            <tr>
                                <td> Nomor Rekening </td>
                                <td> : </td>
                                <td> {{ $pelanggan->no_rekening }} </td>
                            </tr>
                            <tr>
                                <td> Rekening Atas Nama </td>
                                <td> : </td>
                                <td> {{ $pelanggan->atas_nama }} </td>
                            </tr>
                            <tr>
                                <td> Bank</td>
                                <td> : </td>
                                <td> {{ $pelanggan->bank }} </td>
                            </tr>
                            <tr>
                                <td> Status Pelanggan </td>
                                <td> : </td>
                                <td> 
                                    @if($pelanggan->level == 'eceran')
                                        Eceran
                                    @else
                                        Grosir
                                    @endif
                                </td>
                            </tr>
                           <!--  <tr>
                                <td> Diskon dengan Persen </td>
                                <td> : </td>
                                <td>
                                    @if($pelanggan->diskon_persen != NULL || $pelanggan->diskon_persen != 0)
                                        {{ $pelanggan->diskon_persen }} %
                                    @else
                                        - 
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td> Jumlah Potongan </td>
                                <td> : </td>
                                <td> {{ number_format($pelanggan->potongan) }} </td>
                            </tr> -->
                            <tr>
                                <td> Limit Jumlah Piutang </td>
                                <td> : </td>
                                <td> {{ \App\Util::duit0($pelanggan->limit_jumlah_piutang) }} </td>
                            </tr>
                            <tr>
                                <td> Waktu Jatuh Tempo </td>
                                <td> : </td>
                                <td> {{ $pelanggan->jatuh_tempo }} Hari</td>
                            </tr>
                            <tr>
                                <td> Deposito </td>
                                <td> : </td>
                                <td> {{ \App\Util::duit0($pelanggan->titipan) }}</td>
                            </tr>
                            <tr>
                                <td> Operator</td>
                                <td> : </td>
                                <td> {{ $pelanggan->user->nama }} </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data password berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data password gagal direset!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
            var url = "{{ url('pelanggan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

        });
    </script>

@endsection
