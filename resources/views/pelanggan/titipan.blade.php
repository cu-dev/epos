@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Deposito Pelanggan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
        #tableJenisItem_filter input {
            max-width: 100px;
        }

        /*.kas span {
        	padding-top: 5px;
        }*/
    </style>
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12" id="">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="formSimpanTitle">Tambah Deposito Pelanggan <b>{{ $pelanggan->nama}}</b></h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <a href="{{ url('pelanggan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <form method="post" action="{{ url('pelanggan/titipan') }}" class="form-horizontal" id="formSimpanContainer">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Kode Transaksi</label>
                                    <input class="form-control" type="text" name="kode_transaksi" readonly>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="control-label">Nominal</label>
                                    <input class="form-control angka" type="text" name="nominal_">
                                    <input class="form-control" type="hidden" name="nominal">
                                </div> --}}
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control" type="text" name="keterangan" required="" id="keterangan">
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label">Penyerah Lain</label>
                                        <div class="input-group" style="margin-bottom: 0;">
                                            <div class="input-group-addon"><input type="checkbox" name="cek_penyerah" id="cek_penyerah" /></div>
                                            <input class="form-control select2_tinggi" type="text" id="penyerah" disabled="" name="penyerah">
                                        </div>
                                        <p>Jika penyerah bukan pelanggan, cek penyerah lain!</p>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Penerima</label>
                                        <select class="form-control select2_single" name="user_id">
                                            <option value="">Pilih Penerima</option>
                                            @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8" style="padding: 0px">
                                        <div class="form-group col-sm-12 col-xs-12">
                                            <label class="control-label">Metode Pembayaran</label>
                                            <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                                <div class="btn-group" role="group">
                                                    <button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>
                                                </div>
                                                <div class="btn-group" role="group">
                                                    <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa"></i> Transfer</button>
                                                </div>
                                                <div class="btn-group" role="group">
                                                    <button type="button" id="btnKartu" class="btn btn-default"><i class="fa"></i> Kartu</button>
                                                </div>
                                                <div class="btn-group" role="group">
                                                    <button type="button" id="btnCek" class="btn btn-default"><i class="fa"></i> Cek</button>
                                                </div>
                                                <div class="btn-group" role="group">
                                                    <button type="button" id="btnBG" class="btn btn-default"><i class="fa"></i> BG</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                            <div class="line"></div>
                                            <label class="control-label">Nominal Tunai</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">Rp</div>
                                                <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                            </div>
                                        </div>
                                        <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                            <div class="line"></div>
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                                    <label class="control-label">Pilih Bank</label>
                                                    <select class="form-control select2_single" name="bank_id">
                                                        <option value="">Pilih Bank</option>
                                                        @foreach ($banks as $bank)
                                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Nomor Transfer</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">#</div>
                                                        <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Nominal Transfer</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Rp</div>
                                                        <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                            <div class="line"></div>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Pilih Bank</label>
                                                    <select class="form-control select2_single" name="bank_kartu">
                                                        <option value="">Pilih Bank</option>
                                                        @foreach ($banks as $bank)
                                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Pilih Jenis Kartu</label>
                                                    <select class="form-control select2_single" name="jenis_kartu">
                                                        <option value="">Pilih Kartu</option>
                                                        <option value="debet">Kartu Debit</option>
                                                        <option value="kredit">Kartu Kredit</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Nomor Transaksi</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">#</div>
                                                        <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <label class="control-label">Nominal Kartu</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Rp</div>
                                                        <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                            <div class="line"></div>
                                            <div class="row">
                                                <div class="col-xs-6 col-md-6">
                                                    <label class="control-label">Nomor Cek</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">#</div>
                                                        <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <label class="control-label">Nominal Cek</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Rp</div>
                                                        <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control angka" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                            <div class="line"></div>
                                            <div class="row">
                                                <div class="col-xs-6 col-md-6">
                                                    <label class="control-label">Nomor BG</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">#</div>
                                                        <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <label class="control-label">Nominal BG</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Rp</div>
                                                        <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control angka" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <input type="hidden" name="nominal_tunai" />

                                    <input type="hidden" name="no_transfer" />
                                    <input type="hidden" name="bank_transfer" />
                                    <input type="hidden" name="nominal_transfer" />

                                    <input type="hidden" name="no_kartu" />
                                    <input type="hidden" name="bank_kartu" />
                                    <input type="hidden" name="jenis_kartu" />
                                    <input type="hidden" name="nominal_kartu" />

                                    <input type="hidden" name="no_cek" />
                                    <input type="hidden" name="nominal_cek" />

                                    <input type="hidden" name="no_bg" />
                                    <input type="hidden" name="nominal_bg" />
                                    <input type="hidden" name="nominal_total" />
                                    <input type="hidden" name="id" value="{{ $pelanggan->id }}" />
                                    <input type="hidden" name="penerima"/>
                            </form>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;" data-toggle="tooltip" data-placement="top" title="Tambah & Depositkan">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="row" style="margin-bottom: 0; padding-bottom: 0">
                                <div class="col-md-12">
                                    <h2 id="formSimpanTitle" class="titleDetailItem"> <b>{{ $pelanggan->nama }}</b></h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="pull-left">Saldo Saat ini : {{ \App\Util::ewon($pelanggan->titipan) }}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                <div class="pull-right">    
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </div>
                            {{-- <a href="{{ url('pelanggan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                                <i class="fa fa-long-arrow-left"></i>
                            </a> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th width="150px">Kode Transaksi</th>
                                <th width="100px">Nominal</th>
                                <th width="100px">Penyerah</th>
                                <th width="100px">Penerima</th>
                                <th width="100px">Eksekutor</th>
                                <th width="300px">Keterangan</th>
                                <th width="50px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach($jurnals as $nums => $jurnal)
                                <tr id="{{ $a = $nums }}">
                                    <td>{{ $nums+1 }}</td>
                                    <td>{{ $jurnal->referensi }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($jurnal->kredit) }}</td>
                                    <td class="kas text-center" >
                                        @for( $i=0; $i < sizeof($kas[$a]); $i++)
											@if ($kas[$a][$i] == 'Tunai')
                                    			<span class="label label-danger">Tunai</span><br>
											@elseif ($kas[$a][$i] == 'Transfer')
                                    			<span class="label label-warning">Transfer</span><br>
                                            @elseif ($kas[$a][$i] == 'Kartu')
                                                <span class="label label-info">Kartu</span><br>
											@elseif ($kas[$a][$i] == 'Cek')
                                    			<span class="label label-success">Cek</span><br>
											@elseif ($kas[$a][$i] == 'BG')
                                    			<span class="label btn-BG">BG</span><br>
											@endif 
                                        @endfor
                                    </td>
                                    <td class="keterangan CapitalFirst">{{ $jurnal->user->nama }}</td>
                                    <td class="keterangan CapitalFirst">{{ $jurnal->keterangan }}</td>
                                </tr>
                            @endforeach --}}

                            @foreach($log_deposito as $i => $log)
                            <tr id="{{ $log->id }}">
                                <td>{{ $i + 1 }}</td>
                                <td>{{ $log->kode_transaksi }}</td>
                                <td class="text-right">{{ \App\Util::ewon($log->jumlah_bayar) }}</td>
                                {{-- <td class="text-center">
                                    @if($log['metode'] == 'tunai')
                                        <span class="label label-danger">Tunai</span>
                                    @elseif($log['metode'] == 'transfer')
                                        <span class="label label-warning">Transfer</span>
                                    @elseif($log['metode'] == 'kartu')
                                        <span class="label label-info">Kartu</span>
                                    @elseif($log['metode'] == 'cek')
                                        <span class="label label-success">Cek</span>
                                    @elseif($log['metode'] == 'hutang')
                                        <span class="label label-warning">Bayar Hutang</span>
                                    @elseif($log['metode'] == 'bg')
                                        <span class="label btn-BG">BG</span>
                                    @endif
                                </td> --}}
                                <td>{{ $log->penyerah }}</td>
                                <td>{{ $log->grosir->nama }}</td>
                                <td>{{ $log->user->nama }}</td>
                                <td>{{ $log->keterangan }}</td>
                                <td>
                                    <a href="{{ url('pelanggan/titipan/show/'.$log->id) }}" class="btn btn-xs btn-info" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Detail">
                                        <i class="fa fa-money"></i>
                                    </a>
                                    @if ($log->cetak < 2)
                                        <a href="{{ url('cetak/titipan/'.$log->id) }}" id="btnCetakNota" class="btn btn-xs btn-cetak pull-right" data-toggle="tooltip" data-placement="top" title="Cetak Struk Retur Penjualan">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Deposito berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();

        function updateHargaOnKeyup() {
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();

            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);
            nominal_kartu = parseFloat(nominal_kartu);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;

            var nominal_total = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kartu;
            $('input[name="nominal_total"]').val(nominal_total);
            $('#btnSimpan').prop('disabled', isSubmitButtonDisabled());
        }

        $(window).on('load', function(event) {
            var url = "{{ url('pelanggan/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');
            var kode_pelanggan = '{{ $pelanggan->id }}';
            // console.log(kode_pelanggan);
            $.get(url, function(data) {
                if (data.titipan === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/DPS/' + kode_pelanggan + '/' + tanggal;
                } else {
                    var referensi = data.titipan.referensi;
                    var mm_transaksi = referensi.split('/')[3];
                    var yyyy_transaksi = referensi.split('/')[4];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    // console.log(tanggal_transaksi);
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/DPS/' + kode_pelanggan + '/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/DPS/' + kode_pelanggan + '/' + tanggal_transaksi;
                    }
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

        $(document).ready(function() {
            var url = "{{ url('pelanggan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                allowClear: true
            });

            $('#keterangan').trigger('keyup');

            // $('#pilihanPotonganContainer').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            // $('#inputTitipanContainer').hide();

            $('.keterangan').each(function(index, el) {
                // console.log($(el).text());
                var text = $(el).text();

                // var duit = parseInt($(el).text());
                // $(el).text(duit.toLocaleString());
            });
        });

        /*$(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
             $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
             $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
        });*/

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').addClass('fa-check');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTransferBankContainer').hide('hide', function() {
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="no_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');

                    $(this).find('select[name="bank_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').addClass('fa-check');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputKartuContainer').hide('hide', function() {
                    $('input[name="no_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');

                    $(this).find('select[name="bank_kartu"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu"]').val('').trigger('change');
                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputCekContainer').hide('hide', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                $(this).find('i').addClass('fa-check');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    // updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputBGContainer').hide('hide', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');

                    $(this).find('input').val('');
                    // updateHargaOnKeyup();
                });
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // $(this).val(nominal_tunai.toLocaleString());
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_transfer"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="user_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="penerima"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_transfer += '';
            //     nominal_transfer = nominal_transfer.slice(0, -1);
            //     nominal_transfer = parseFloat(nominal_transfer);
            //     if (isNaN(nominal_transfer)) nominal_transfer = 0;

            //     // $(this).val(nominal_transfer.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            // $(this).val(nominal_kartu.toLocaleString());
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_kartu += '';
            //     nominal_kartu = nominal_kartu.slice(0, -1);
            //     nominal_kartu = parseFloat(nominal_kartu);
            //     if (isNaN(nominal_kartu)) nominal_kartu = 0;

            //     $(this).val(nominal_kartu.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal_kartu);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            // $(this).val(nominal_cek.toLocaleString());
            $('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_cek += '';
            //     nominal_cek = nominal_cek.slice(0, -1);
            //     nominal_cek = parseFloat(nominal_cek);
            //     if (isNaN(nominal_cek)) nominal_cek = 0;

            //     $(this).val(nominal_cek.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNoBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#keterangan', function(event) {
            event.preventDefault();
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            // $(this).val(nominal_bg.toLocaleString());
            $('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_bg += '';
            //     nominal_bg = nominal_bg.slice(0, -1);
            //     nominal_bg = parseFloat(nominal_bg);
            //     if (isNaN(nominal_bg)) nominal_bg = 0;

            //     $(this).val(nominal_bg.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
            //     updateHargaOnKeyup();
            // }
        });

        function isSubmitButtonDisabled() {
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var no_transfer = $('input[name="no_transfer"]').val();
            var bank_transfer = $('input[name="bank_transfer"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var no_kartu = $('input[name="no_kartu"]').val();
            var bank_kartu = $('input[name="bank_kartu"]').val();
            var jenis_kartu = $('input[name="jenis_kartu"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var no_cek = $('input[name="no_cek"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var no_bg = $('input[name="no_bg"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var penerima = $('input[name="penerima"]').val();
            var keterangan = $('#keterangan').val();
            var penyerah = $('#penyerah').val();
            var cek_penyerah = $('#cek_penyerah').prop('checked');
            var nominal_total = $('input[name="nominal_total"]').val();

            nominal_total = parseFloat(nominal_total);
            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_kartu = parseFloat(nominal_kartu);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);

            if (isNaN(nominal_total)) nominal_total = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            if (nominal_total <= 0) return true;

            // console.log('btnTunai');
            if ($('#btnTunai').hasClass('btn-danger')) {
                if (nominal_tunai <= 0) return true;
            }

            // console.log('btnTransfer');
            if ($('#btnTransfer').hasClass('btn-warning')) {
                if (no_transfer == '') return true;
                if (bank_transfer == '') return true;
                if (nominal_transfer <= 0) return true;
            }

            // console.log('btnKartu');
            if ($('#btnKartu').hasClass('btn-info')) {
                if (no_kartu == '') return true;
                if (bank_kartu == '') return true;
                if (jenis_kartu == '') return true;
                if (nominal_kartu <= 0) return true;
            }

            // console.log('btnCek');
            if ($('#btnCek').hasClass('btn-success')) {
                if (no_cek == '') return true;
                if (nominal_cek <= 0) return true;
            }

            // console.log('btnBG');
            if ($('#btnBG').hasClass('btn-BG')) {
                if (no_bg == '') return true;
                if (nominal_bg <= 0) return true;
            }

            if (penerima == null || penerima == '') return true;
            if (keterangan == null || penerima == '') return true;
            console.log(cek_penyerah, penyerah);
            if(cek_penyerah && penyerah == '') return true;

            // console.log('end');
            return false;
        }

        $(document).on('click', '#btnSimpan', function(event) {

        });

        $(document).on('change', '#cek_penyerah', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('#penyerah').prop('disabled', false);
            }else{
                $('#penyerah').val('');
                $('#penyerah').prop('disabled', true);
            }
            updateHargaOnKeyup();
        });

        $(document).on('change', '#penyerah', function(event) {
            event.preventDefault();
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnSimpan', function(event) {
            event.preventDefault();

            swal({
                title: 'Tambah Deposito Pelanggan?',
                text: 'Anda yakin akan menambah deposito pelanggan?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Depositkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                //confirmed
                $('#formSimpanContainer').submit();
                console.log('oke');
             }).catch(function(isConfirm) {
                    //canceled
            });
        });
    </script>
@endsection
