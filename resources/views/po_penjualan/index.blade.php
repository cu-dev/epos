@extends('layouts.admin')

@section('title')
    <title>EPOS | Pesanan Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambahVIP {
            /*margin-right: 0;*/
        }
        #btnTambah,
        #btnTambahVIP {
            margin-bottom: 0;
        }
        #btnDetail,
        #btnGantiVIP,
        #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Daftar Pesanan Penjualan</h2>
                @if (in_array(Auth::user()->level_id, [1, 2]))
                <a href="{{ url('transaksi-grosir-vip/create') }}" id="btnTambahVIP" class="btn btn-sm btn-dark pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan (Prioritas)">
                    <i class="fa fa-star"></i>
                </a>
                @endif
                @if(Auth::user()->level_id !=4 )
                    <button id="btnTambah" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan">
                        <i class="fa fa-plus"></i>
                    </button>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('po-penjualan/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="created_at">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tabelPOPenjualan" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="created_at">No</th>
                                <th class="sorting" field="created_at">Tanggal & Waktu</th>
                                <th class="sorting" field="kode_transaksi">Kode Pesanan</th>
                                <th class="sorting" field="pelanggan_nama">Pelanggan</th>
                                <th class="sorting" field="status">Status</th>
                                <th class="sorting" field="created_at" style="width: 120px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('po-penjualan/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="6" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var po_penjualan = data[i];
                        var buttons = po_penjualan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pesanan Penjualan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.ganti_vip != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnGantiVIP" data-toggle="tooltip" data-placement="top" title="Ganti Pesanan Grosir menjadi Pesanan Prioritas">
                                    <i class="fa fa-arrow-up"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus Pesanan Penjualan">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${po_penjualan.id}">
                                    <td>${nomor}</td>
                                    <td>${ymd2dmy(po_penjualan.created_at.split(' ')[0])} ${po_penjualan.created_at.split(' ')[1]}</td>
                                    <td>${po_penjualan.kode_transaksi}</td>
                                    <td>${po_penjualan.pelanggan_nama}</td>
                                    <td>${po_penjualan.mdt_status}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });

        $(window).on('load', function(event) {
            var url = "{{ url('sidebar/setoran-buka/json') }}";
            $.get(url, function(data) {
                // console.log(data);
                var setoran_buka = data.setoran_buka;
                var cash_drawer = data.cash_drawer;
                var user = data.user;
                // console.log(setoran_buka);
                if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
                    if ([1, 2].indexOf(user.level_id) < 0) {
                        $('#btnTambah').prop('disabled', true);
                        // $('#btnTambah').find('a').addClass('link-mati');
                    }
                }
            });
        });

        $(document).on('click', '#btnGantiVIP', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('id');

            swal({
                title: 'Anda yakin?',
                text: 'PO Grosir akan diubah menjadi PO VIP',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ganti',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-penjualan/") }}' + '/' + id + '/ganti-status';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

        $(document).on('click', '#btnTambah', function(event) {
            event.preventDefault();

            var url = '{{ url('transaksi-grosir/create') }}';
            var ww = window.open(url, '_self');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('id');

            swal({
                title: 'Hapus Pesanan?',
                text: 'Pesanan Penjualan akan dihapus!',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-penjualan/") }}' + '/' + id + '/hapus';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

    </script>
@endsection
