@extends('layouts.admin')

@section('title')
    <title>EPOS | Pesanan Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambahVIP {
            /*margin-right: 0;*/
        }
        #btnTambah,
        #btnTambahVIP {
            margin-bottom: 0;
        }
        #btnDetail,
        #btnGantiVIP,
        #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Daftar Pesanan Penjualan</h2>
                @if (in_array(Auth::user()->level_id, [1, 2]))
                <a href="{{ url('transaksi-grosir-vip/create') }}" id="btnTambahVIP" class="btn btn-sm btn-dark pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan (Prioritas)">
                    <i class="fa fa-star"></i>
                </a>
                @endif
                @if(Auth::user()->level_id !=4 )
                    <button id="btnTambah" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Penjualan">
                        <i class="fa fa-plus"></i>
                    </button>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-hover table-striped" style="margin-bottom: 0;" id="tabelPOPenjualan">
                    <thead>
                        <tr>
                            <th style="display: none;">No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Pesanan</th>
                            <th>Pelanggan</th>
                            <th>Status</th>
                            <th style="width: 75px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($po_penjualans as $i => $po_penjualan)
                        <tr id="{{ $po_penjualan->id }}">
                            <td class="tengah-v" style="display: none;">{{ ++$i }}</td>
                            <td class="tengah-vh">
                                <span class="hidden">{{ $po_penjualan->id }}</span>
                                {{ $po_penjualan->created_at->format('d-m-Y H:m:s') }}
                            </td>
                            <td class="tengah-vh">{{ $po_penjualan->kode_transaksi }}</td>
                            @if ($po_penjualan->pelanggan_id != null)
                                <td class="tengah-v">{{ ucwords($po_penjualan->pelanggan->nama) }}</td>
                            @else
                                <td class="tengah-v">-</td>
                            @endif
                            @if($po_penjualan->status == 'po_vip')
                                <td class="tengah-v">Prioritas</td>
                            @else
                                <td class="tengah-v">{{ ucfirst(substr($po_penjualan->status, 3)) }}</td>
                            @endif
                            <td class="text-left">
                                <a href="{{ url('po-penjualan/'.$po_penjualan->id) }}" id="btnDetail" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Detail Pesanan Penjualan">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if (in_array(Auth::user()->level_id, [1, 2]) && $po_penjualan->status == 'po_grosir' && $po_penjualan->pelanggan->level == 'grosir')
                                <button id="btnGantiVIP" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Ganti Pesanan Grosir menjadi Pesanan Prioritas">
                                    <i class="fa fa-arrow-up"></i>
                                </button>
                                @endif
                                {{-- <a href="{{ $po_penjualan->status == 'po' ? url('transaksi-grosir/create/'.$po_penjualan->id) : url('transaksi-grosir-vip/create/'.$po_penjualan->id) }}" class="btn btn-xs btn-primary" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Edit Pesanan Penjualan">
                                    <i class="fa fa-edit"></i>
                                </a> --}}
                                <button id="btnHapus" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Pesanan Penjualan">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Penjualan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tabelPOPenjualan').DataTable({
            // 'order': [[0, 'asc']]
        });

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });

        $(window).on('load', function(event) {
            var url = "{{ url('sidebar/setoran-buka/json') }}";
            $.get(url, function(data) {
                // console.log(data);
                var setoran_buka = data.setoran_buka;
                var cash_drawer = data.cash_drawer;
                var user = data.user;
                // console.log(setoran_buka);
                if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
                    if ([1, 2].indexOf(user.level_id) < 0) {
                        $('#btnTambah').prop('disabled', true);
                        // $('#btnTambah').find('a').addClass('link-mati');
                    }
                }
            });
        });

        $(document).on('click', '#btnGantiVIP', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('id');

            swal({
                title: 'Anda yakin?',
                text: 'PO Grosir akan diubah menjadi PO VIP',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ganti',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-penjualan/") }}' + '/' + id + '/ganti-status';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

        $(document).on('click', '#btnTambah', function(event) {
            event.preventDefault();

            var url = '{{ url('transaksi-grosir/create') }}';
            var ww = window.open(url, '_self');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('id');

            swal({
                title: 'Hapus Pesanan?',
                text: 'Pesanan Penjualan akan dihapus!',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-penjualan/") }}' + '/' + id + '/hapus';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

    </script>
@endsection
