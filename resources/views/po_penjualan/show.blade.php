@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Pesanan Penjualan - {{ $po_penjualan->kode_transaksi }}</title>
@endsection

@section('style')
    <style media="screen">
        #btnBayar, #btnEdit, #btnKembali, #btnCetak {
            margin-bottom: 0;
        }
        .no-border {
            border: none;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <form id="formBayar" action="{{ url('transaksi-grosir/bayar/'.$po_penjualan->id) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                </form>
                <h2>Detail Pesanan Penjualan</h2>
                {{-- @if (in_array(Auth::user()->level_id, [1, 2]) || ($po_penjualan->status == 'po_eceran' && Auth::user()->level_id == 3) || ($po_penjualan->status == 'po_grosir' && Auth::user()->level_id == 4)) --}}
                @if (in_array(Auth::user()->level_id, [1, 2]) || $boleh_bayar)
                {{-- <button id="btnBayar" class="btn btn-sm btn-success pull-right" style="margin-right: 0; {{ $po_penjualan->status == 'po_eceran' ? 'margin-left: 5px;' : '' }}">
                    <i class="fa fa-money"></i> Bayar
                </button> --}}
                    <button id="btnBayar" class="btn btn-sm btn-primary pull-right" data-toggle="tooltip" data-placement="top" title="{{ $text_bayar }}">
                        <i class="fa fa-money"></i>
                    </button>
                @endif

                @if (in_array(Auth::user()->level_id, [1, 2, 3]))
                    {{-- @if (in_array($po_penjualan->status, ['po_grosir', 'po_vip']) || ($po_penjualan->status == 'po_eceran' && count($relasi_po_penjualan) > 3)) --}}
                    @if ($jumlah_item_asli > 3)
                    <button id="btnCetak" class="btn btn-sm btn-cetak pull-right" data-toggle="tooltip" data-placement="top" title="Cetak Pengambilan Barang">
                        <i class="fa fa-print"></i>
                    </button>
                    @endif
                    @if ($po_penjualan->status == 'po_eceran' || $po_penjualan->status == 'po_grosir')
                    <a href="{{ url('transaksi-grosir/create/'.$po_penjualan->id) }}" id="btnEdit" class="btn btn-sm btn-warning pull-right" data-toggle="tooltip" data-placement="top" title="Ubah Pesanan Penjualan">
                        <i class="fa fa-edit"></i>
                    </a>
                    @elseif ($po_penjualan->status == 'po_vip')
                    <a href="{{ url('transaksi-grosir-vip/create/'.$po_penjualan->id) }}" id="btnEdit" class="btn btn-sm btn-warning pull-right" data-toggle="tooltip" data-placement="top" title="Ubah Pesanan Penjualan">
                        <i class="fa fa-edit"></i>
                    </a>
                    @endif
                    @if (in_array(Auth::user()->level_id, [1, 2]) && $po_penjualan->status != 'po_vip' && ($po_penjualan->pelanggan_id != null && $po_penjualan->pelanggan->level == 'grosir'))
                        <button id="btnGantiVIP" class="btn btn-sm btn-danger pull-right" data-toggle="tooltip" data-placement="top" title="Ganti Pesanan Grosir menjadi Pesanan Prioritas">
                            <i class="fa fa-arrow-up"></i>
                        </button>
                    @endif
                @endif

                {{-- <a href="{{ url('po-penjualan') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" style="{{ $po_penjualan->status == 'po_eceran' && Auth::user()->level_id == 4 ? 'margin-right: 0;' : '' }}"> --}}
                <a href="{{ url('po-penjualan') }}" id="btnKembali" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $po_penjualan->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $po_penjualan->kode_transaksi }}</td>
                                </tr>
                                @if ($po_penjualan->pelanggan_id != null)
                                <tr>
                                    <th>Pelanggan</th>
                                    <td style="width: 60%;">{{ $po_penjualan->pelanggan->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Toko</th>
                                    <td style="width: 60%;">{{ $po_penjualan->pelanggan->toko }}</td>
                                </tr>
                                <tr>
                                    <th>Telepon</th>
                                    <td style="width: 60%;">{{ $po_penjualan->pelanggan->telepon }}</td>
                                </tr>
                                @if ($po_penjualan->alamat != null)
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $po_penjualan->alamat }}</td>
                                </tr>
                                @else
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $po_penjualan->pelanggan->alamat }}</td>
                                </tr>
                                @endif
                                @endif
                                <tr>
                                    <th>Tanggal Pesanan</th>
                                    <td style="width: 60%;">{{ $po_penjualan->created_at->format('d-m-Y') }}</td>
                                </tr>
                                @if($po_penjualan->jatuh_tempo != NULL && $po_penjualan->jumlah_bayar < $po_penjualan->harga_total - $po_penjualan->potongan_penjualan)
                                    <tr id="JatuhTempotr">
                                        <th>Jatuh Tempo</th>
                                        <td style="width: 60%;">{{ \App\Util::date($po_penjualan->jatuh_tempo) }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Status Penjualan</th>
                                    <td style="width: 60%; text-transform: capitalize;">
                                        @if($po_penjualan->status == 'po_eceran')
                                            Eceran
                                        @elseif($po_penjualan->status == 'po_grosir')
                                            Grosir
                                        @elseif($po_penjualan->status == 'po_vip')
                                            Prioritas
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $po_penjualan->user != null ? $po_penjualan->user->nama : $po_penjualan->grosir->nama }}</td>
                                </tr>
                                @if ($po_penjualan->pengirim != null)
                                <tr>
                                    <th>Pengirim</th>
                                    <td style="width: 60%;">{{ $po_penjualan->pengirim }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <!-- <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($po_penjualan->nominal_tunai != null && $po_penjualan->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_tunai) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->no_transfer }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->banktransfer->nama_bank }} [{{ $po_penjualan->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nominal_transfer != null && $po_penjualan->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_transfer) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->no_kartu }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->jenis_kartu }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->bankkartu->nama_bank }} [{{ $po_penjualan->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nominal_kartu != null && $po_penjualan->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_kartu) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->no_cek }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->bankcek->nama_bank }} [{{ $po_penjualan->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nominal_cek != null && $po_penjualan->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_cek) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->no_bg }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $po_penjualan->bankbg->nama_bank }} [{{ $po_penjualan->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nominal_bg != null && $po_penjualan->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_bg) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nominal_titipan != null && $po_penjualan->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_titipan) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->jumlah_bayar != null && $po_penjualan->jumlah_bayar > 0)
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->jumlah_bayar) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($po_penjualan->harga_total != null && $po_penjualan->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->harga_total) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->potongan_penjualan != null && $po_penjualan->potongan_penjualan > 0)
                                <tr>
                                    <th>Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->potongan_penjualan) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->nego_total != null && $po_penjualan->nego_total > 0 && $is_nego)
                                <tr>
                                    <th>Nego Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nego_total) }}</td>
                                </tr>
                                @endif
                        
                                @if ($po_penjualan->ongkos_kirim != null && $po_penjualan->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->ongkos_kirim) }}</td>
                                </tr>
                                @endif
                        
                                {{-- @if ($po_penjualan->jumlah_bayar != null && $po_penjualan->jumlah_bayar > 0) --}}
                                <tr>
                                    @if ($kembali > 0)
                                    <th>Kembali</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali) }}</td>
                                    @elseif($kembali < 0)
                                    <th>Kurang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali*-1) }}</td>
                                    @endif
                                </tr>
                                {{-- @endif --}}
                            </tbody>
                        </table> -->
                        
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Rincian</th>
                            </thead>
                            <tbody>
                                @if ($po_penjualan->harga_total != null && $po_penjualan->harga_total > 0)
                                <tr>
                                    <th>Sub Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->harga_total) }}</td>
                                </tr>
                                @endif

                                <tr>
                                    <th>Nego & Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->potongan_penjualan) }}</td>
                                </tr>

                                <tr>
                                    <th>Deposito Pelanggan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_titipan) }}</td>
                                </tr>

                                @if ($kembali >= 0)
                                    <tr>
                                        <th>Jumlah Bayar</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->harga_total + $po_penjualan->ongkos_kirim - $po_penjualan->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kembali</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali) }}</td>
                                    </tr>
                                @elseif($kembali < 0)
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->harga_total + $po_penjualan->ongkos_kirim - $po_penjualan->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pengurang Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali*-1) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($po_penjualan->nominal_tunai != null && $po_penjualan->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->banktransfer->nama_bank }} [{{ $po_penjualan->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_transfer != null && $po_penjualan->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    {{-- <td class="text-right" style="width: 60%;">{{ $po_penjualan->jenis_kartu }}</td> --}}
                                    @if ($po_penjualan->jenis_kartu == 'debet')
                                        <td class="text-right" style="width: 60%;">Debit</td>
                                    @else
                                        <td class="text-right" style="width: 60%;">Kredit</td>
                                    @endif
                                </tr>
                                @endif

                                @if ($po_penjualan->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->bankkartu->nama_bank }} [{{ $po_penjualan->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_kartu != null && $po_penjualan->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->bankcek->nama_bank }} [{{ $po_penjualan->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_cek != null && $po_penjualan->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->bankbg->nama_bank }} [{{ $po_penjualan->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_bg != null && $po_penjualan->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_bg) }}</td>
                                </tr>
                                @endif

                                {{-- @if ($po_penjualan->nominal_titipan != null && $po_penjualan->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->nominal_titipan) }}</td>
                                </tr>
                                @endif --}}

                                {{-- @if ($po_penjualan->jumlah_bayar != null && $po_penjualan->jumlah_bayar > 0) --}}
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($po_penjualan->jumlah_bayar - $po_penjualan->nominal_titipan) }}</td>
                                </tr>
                                {{-- @endif --}}
                            </tbody>
                        </table>

                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Total</th>
                                    <th class="text-left">Nego</th>
                                    <th class="text-left">Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_po_penjualan as $i => $relasi)
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item_kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah" class="jumlah">{{ $relasi->jumlah }}</td>
                                    <td id="harga" class="text-right">{{ \App\Util::duit0($relasi->subtotal / $relasi->jumlah) }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->nego) }}</td>
                                    @if ($relasi->bonuses != null)
                                    <td>
                                        @foreach ($relasi->bonuses as $j => $rb)
                                            {{ $rb['jumlah'] }} {{ $rb['bonus']->nama }}
                                            @if ($j != count($relasi->bonuses) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if (count($relasi_bonus_rules_penjualan) > 0)
                        <div class="x_title">
                            <h2>Bonus Penjualan</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item-bundle">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Paket</th>
                                    <th class="text-left">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_bonus_rules_penjualan as $i => $rbrp)
                                <tr id="{{ $rbrp->id }}" item_kode="{{ $rbrp->bonus->kode }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $rbrp->bonus->nama }}</td>
                                    <td class="jumlah">{{ $rbrp->jumlah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif

                        @if (count($show_relasi_bundles) > 0)
                        <div class="x_title">
                            <h2>Item Paket</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item-bundles">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Paket</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($show_relasi_bundles as $i => $show_relasi_bundle)
                                    @foreach ($show_relasi_bundle as $j => $relasi_bundle)
                                    <tr id="{{ $relasi_bundle->id }}" item_kode="{{ $relasi_bundle->item_child }}">
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $relasi_bundle->bundle->nama }}</td>
                                        <td>{{ $relasi_bundle->itemxx->nama }}</td>
                                        <td class="jumlah">{{ $relasi_bundle->jumlah }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah_stok')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Stok barang ada  yang kurang, cek kembali!',
                timer: 3000,
                type: 'warning'
            });
        </script>
    @endif

    <script type="text/javascript">
        var items = [];
        var satuans = null;
        var po_penjualan = null;
        var relasi_po_penjualan = null;
        var user_level = null;
        var duit_masuk = 0;
        var kembali_temp = 0;
        var setoran_buka = null;
        var cash_drawer = null;
        var limit = null;
        var is_setoran_buka = null;
        var pelanggan = null;
        var text_bayar = '';

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            is_setoran_buka = '{{ json_encode($is_setoran_buka) }}';
            text_bayar = '{{ json_encode($text_bayar) }}';
            text_bayar = text_bayar.replace(/&quot;/g, '');

            items = '{{ json_encode($item_list) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            user_level = '{{ Auth::user()->level_id }}';
            if(user_level == 3 && !is_setoran_buka) $('#btnBayar').prop('disabled', true);
            
            po_penjualan = '{{ $po_penjualan }}';
            po_penjualan = po_penjualan.replace(/&quot;/g, '"');
            po_penjualan = JSON.parse(po_penjualan);

            pelanggan = '{{ json_encode($pelanggan) }}';
            pelanggan = pelanggan.replace(/&quot;/g, '"');
            pelanggan = JSON.parse(pelanggan);
            // console.log(po_penjualan);

            var nego_total_temp = po_penjualan.nego_total;
            var harga_total_temp = po_penjualan.harga_total;
            if(nego_total_temp > 0 ){
                var pengurang = nego_total_temp;
            }else{
                var pengurang = harga_total_temp;
            }
            if(po_penjualan.jumlah_bayar > 0){
                var jumlah_bayar_temp = parseFloat(po_penjualan.nominal_tunai)
                                        + parseFloat(po_penjualan.nominal_transfer)
                                        + parseFloat(po_penjualan.nominal_kartu)
                                        + parseFloat(po_penjualan.nominal_cek)
                                        + parseFloat(po_penjualan.nominal_bg)
                                        + parseFloat(po_penjualan.nominal_titipan);
                kembali_temp = jumlah_bayar_temp
                                    + parseFloat(po_penjualan.potongan_penjualan)
                                    - parseFloat(po_penjualan.ongkos_kirim)
                                    - parseFloat(pengurang);
                duit_masuk = parseFloat(po_penjualan.nominal_tunai);
                if (kembali_temp > 0) duit_masuk -= kembali_temp;
            }

            relasi_po_penjualan = '{{ $relasi_po_penjualan }}';
            relasi_po_penjualan = relasi_po_penjualan.replace(/&quot;/g, '"');
            relasi_po_penjualan = JSON.parse(relasi_po_penjualan);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var konversi = null;
                var v_jumlah = [];
                var v_satuan = [];
                var text_satuan = '';

                for (var i = 0; i < relasi_po_penjualan.length; i++) {
                    if (relasi_po_penjualan[i].id == id) {
                        relasi = relasi_po_penjualan[i];
                        break;
                    }
                }

                var item = relasi.item;
                var jumlah = relasi.jumlah;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var temp_konversi = satuan.konversi;
                    if (jumlah / temp_konversi >= 1) {
                        if (konversi == null) konversi = temp_konversi;
                        text_satuan += Math.floor(jumlah / temp_konversi);
                        text_satuan += ' ' + satuan.satuan.kode;
                        if (i != item.satuan_pembelians.length - 1) text_satuan += '<br>';
                        jumlah = jumlah % temp_konversi;
                    }
                }

                // console.log(text_satuan);
                // $(el).find('#jumlah').html(text_satuan);

                var harga = $(el).find('#harga').text();
                harga = harga.split('p')[1];
                harga = parseFloat(harga.replace(/\D/g, ''), 10);
                harga *= konversi;
                harga = Math.ceil(harga / 100) * 100;
                $(el).find('#harga').text('Rp'+harga.toLocaleString());
                var harga_yang_harus_dibayar = parseFloat(po_penjualan.harga_total) - parseFloat(po_penjualan.potongan_penjualan);
                var jumlah_bayar = parseFloat(po_penjualan.jumlah_bayar);

                if (jumlah_bayar < harga_yang_harus_dibayar) {
                    $('#btnBayar').prop('disabled', true);
                    var kurang = harga_yang_harus_dibayar - jumlah_bayar;
                    if (po_penjualan.pelanggan != null && po_penjualan.pelanggan.level == 'grosir') {
                        if (kurang <= po_penjualan.pelanggan.batas_piutang && pelanggan.lewat_jatuh_tempo <= 0) {
                            $('#btnBayar').prop('disabled', false); //cheked
                        }
                    } else if (po_penjualan.pelanggan != null && pelanggan.level == 'eceran') {
                        if(kurang <= po_penjualan.pelanggan.batas_piutang && po_penjualan.status == 'po_grosir' && pelanggan.lewat_jatuh_tempo <= 0){
                            $('#btnBayar').prop('disabled', false); //checked
                        }
                    }
                }
            });

            var harga_yang_harus_dibayar = parseFloat(po_penjualan.harga_total) - parseFloat(po_penjualan.potongan_penjualan);
            var jumlah_bayar = parseFloat(po_penjualan.jumlah_bayar);
            var kurang = harga_yang_harus_dibayar - jumlah_bayar;

            if (pelanggan != null) {
                if (pelanggan.level == 'eceran' && kurang > 0 && po_penjualan.status == 'grosir') {
                    $('#JatuhTempotr').show('fast');
                } else if (pelanggan.level == 'grosir' && kurang > 0 && pelanggan.lewat_jatuh_tempo > 0) {
                    $('#JatuhTempotr').hide('fast');
                } else if (pelanggan.level == 'grosir' && kurang > 0) {
                    $('#JatuhTempotr').show('fast');
                } else {
                    $('#JatuhTempotr').hide('fast');
                }
            }
            
            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);
                // console.log(item_kode);

                var satuan_item = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }

                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        // console.log(satuan.konversi);
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        // if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                        if (jumlah_jual > 0) {
                            jumlah2 += jumlah_jual;
                            jumlah2 += ' ';
                            jumlah2 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;

                            if (i != satuan_item.length - 1 && temp_jumlah > 0) jumlah2 += ' ';
                        }
                    }
                }

                $(el).text(jumlah2);
                if (jumlah2 == '') $(el).text(jumlah);
            });

            $('#tabel-item').DataTable();
            $('#tabel-item-bundle').DataTable();
            $('#tabel-item-bundles').DataTable();
        });

        $(document).on('click', '#btnBayar', function(event) {
            event.preventDefault();
            var bayar = 'Pesanan akan dibayar';
            var oke = 'Bayar';
            if(text_bayar != 'Bayar Pesanan Penjualan') {
                bayar = 'Pesanan akan dibayar dikreditkan';
                oke = 'Kreditkan'
            }
            if(user_level == 3){
                var url = "{{ url('sidebar/setoran-buka/json') }}";
                $.get(url, function(data) {
                    setoran_buka = data.setoran_buka;
                    cash_drawer = data.cash_drawer;
                    limit = data.limit;

                    var cash_temp = parseFloat(cash_drawer.nominal) + parseFloat(duit_masuk);
                    var bukaan = parseFloat(setoran_buka.nominal);
                    if(parseFloat(cash_temp) - parseFloat(bukaan) > parseFloat(limit.nominal)){
                        swal({
                            title: 'Maaf!',
                            text: 'Pesanan tidak bisa dibayarkan, uang di dalam laci anda mencapai batas limit!',
                            type: 'warning',
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonColor: '#009688',
                            cancelButtonColor: '#ff5252',
                            confirmButtonText: '<i class="fa fa-money"></i> Setor',
                            cancelButtonText: '<i class="fa fa-close"></i> Batal'
                        }).then(function() {
                            // console.log(po_penjualan);
                            $('#formBayar').submit();
                            var url_new = "{{ url('setoran-kasir') }}";
                            window.location.replace(url_new);
                        });
                    }else{
                        swal({
                            title: 'Anda yakin?',
                            text: 'Pesanan akan dibayar!',
                            type: 'question',
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonColor: '#009688',
                            cancelButtonColor: '#ff5252',
                            confirmButtonText: '<i class="fa fa-money"></i> Bayar',
                            cancelButtonText: '<i class="fa fa-close"></i> Batal'
                        }).then(function() {
                            // console.log(po_penjualan);
                            $('#formBayar').submit();
                        });
                    }
                });
            }else{
                swal({
                    title: 'Anda yakin?',
                    text: bayar,
                    type: 'question',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#009688',
                    cancelButtonColor: '#ff5252',
                    confirmButtonText: '<i class="fa fa-money"></i> '+oke,
                    cancelButtonText: '<i class="fa fa-close"></i> Batal'
                }).then(function() {
                    // console.log(po_penjualan);
                    $('#formBayar').submit();

                    // if (po_penjualan.pelanggan == null || po_penjualan.pelanggan.level == 'eceran') {
                    //     if (po_penjualan.status == 'po_eceran') {
                    //         // Cetak eceran
                    //         var url = '{{ url('cetak/eceran/'.$po_penjualan->id) }}';
                    //         var ww = window.open(url, '_blank');
                    //         setTimeout(function () {
                    //             ww.close();
                    //         }, 100);
                    //         $('#formBayar').submit();
                    //     } else if (po_penjualan.status == 'po_grosir' || po_penjualan.status == 'po_vip') {
                    //         // Cetak grosir
                    //         var url = '{{ url('cetak/grosir/'.$po_penjualan->id) }}';
                    //         var ww = window.open(url, '_blank');
                    //         setTimeout(function () {
                    //             ww.close();
                    //             $('#formBayar').submit();
                    //         }, 100);
                    //     }
                    // } else {
                    //     // Cetak grosir
                    //     var url = '{{ url('cetak/grosir/'.$po_penjualan->id) }}';
                    //     var ww = window.open(url, '_blank');
                    //     setTimeout(function () {
                    //         ww.close();
                    //         $('#formBayar').submit();
                    //     }, 100);
                    // }
                });
            }
        });

        $(document).on('click', '#btnCetak', function(event) {
            event.preventDefault();

            var url = '{{ url('cetak/pengambilan/'.$po_penjualan->id) }}';
            var ww = window.open(url, '_self');
            // setTimeout(function () {
            //     ww.close();
            // }, 100);
        });

        $(document).on('click', '#btnGantiVIP', function(event) {
            event.preventDefault();

            swal({
                title: 'Anda yakin?',
                text: 'Pesanan Grosir akan diubah menjadi Pesanan Prioritas',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ganti',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-penjualan/".$po_penjualan->id."/ganti-status") }}';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

        /*$(document).on('click', '#btnCetak', function(event) {
            event.preventDefault();

            var url = '{{ url('cetak/eceran/'.$po_penjualan->id) }}';
            var ww = window.open(url, '_blank');
            setTimeout(function () {
                ww.close();
            }, 100);
        });*/

    </script>

@endsection
