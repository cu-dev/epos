@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail PO Penjualan - {{ $po_penjualan->kode_transaksi }}</title>
@endsection

@section('style')
    <style media="screen">
        #btnEdit, #btnKembali, #btnCetak {
            /*margin-bottom: 0;*/
        }

        .no-border {
            border: none;
        }

        .thumbnail {
            padding: 20px;
        }

        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail PO Penjualan</h2>
                @if (in_array(Auth::user()->level_id, [1, 2, 4]))
                <a href="" id="btnBayar" class="btn btn-sm btn-success pull-right" style="margin-right: 0;" {{ $po_penjualan->jumlah_bayar == null ? 'disabled' : '' }}>
                    <i class="fa fa-money"></i> Bayar
                </a>
                @endif

                @if (Auth::user()->level_id == 3)
                <a href="" id="btnCetak" class="btn btn-sm btn-warning pull-right" style="margin-right: 0;">
                    <i class="fa fa-print"></i> Cetak
                </a>
                <a href="{{ ($po_penjualan->status == 'po') ? url('transaksi-grosir/create/'.$po_penjualan->id) : url('transaksi-grosir-vip/create/'.$po_penjualan->id) }}" id="btnEdit" class="btn btn-sm btn-primary pull-right">
                    <i class="fa fa-edit"></i> Edit
                </a>
                @endif

                <a href="{{ url('po-penjualan') }}" id="btnKembali" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $po_penjualan->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th style="border-top: none;">Kode Transaksi</th>
                                    <td style="border-top: none;">{{ $po_penjualan->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Pelanggan</th>
                                    <td>{{ $po_penjualan->pelanggan->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal PO</th>
                                    <td>{{ $po_penjualan->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td>{{ $po_penjualan->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($po_penjualan->nominal_tunai != null && $po_penjualan->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_tunai + ($po_penjualan->jumlah_bayar - $po_penjualan->harga_total)) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_transfer != null && $po_penjualan->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->bank_id != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->bank->nama_bank }} [{{ $po_penjualan->bank->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_cek != null && $po_penjualan->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_bg != null && $po_penjualan->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_bg) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->no_kredit != null)
                                <tr>
                                    <th>No Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ $po_penjualan->no_kredit }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_kredit != null && $po_penjualan->nominal_kredit > 0)
                                <tr>
                                    <th>Nominal Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_kredit) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->nominal_titipan != null && $po_penjualan->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->nominal_titipan) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($po_penjualan->harga_total != null && $po_penjualan->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->harga_total) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->ongkos_kirim != null && $po_penjualan->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->ongkos_kirim) }}</td>
                                </tr>
                                @endif

                                @if ($po_penjualan->jumlah_bayar != null && $po_penjualan->jumlah_bayar > 0)
                                <tr>
                                    <th>Kembali</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($po_penjualan->jumlah_bayar - $po_penjualan->harga_total) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{--<div class="x_title">--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label">Harga Total (Rp)</label>--}}
                            {{--<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />--}}
                            {{--<input type="hidden" name="hiddenHargaTotal" id="hiddenHargaTotal" />--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label">Jumlah Bayar (Rp)</label>--}}
                            {{--<input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control" readonly="readonly" />--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label">Kembali (Rp)</label>--}}
                            {{--<input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control" readonly="readonly" />--}}
                        {{--</div>--}}
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Subtotal</th>
                                    {{-- <th class="text-left">Item Bonus</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_po_penjualan as $i => $relasi)
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah" class="text-right">{{ $relasi->jumlah }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal / $relasi->jumlah) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
                                    {{-- <td class="text-right">{{ $relasi->jumlah_bonus }} pcs {{ $relasi->item->bonus->nama }}</td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{--<div class="col-md-7 col-xs-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="control-label">Metode Pembayaran</label>--}}
                            {{--<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">--}}
                                {{--<div class="btn-group" role="group">--}}
                                    {{--<button type="button" id="btnTunai" class="btn btn-default"><i class="fa"></i> Tunai</button>--}}
                                {{--</div>--}}
                                {{--<div class="btn-group" role="group">--}}
                                    {{--<button type="button" id="btnDebit" class="btn btn-default"><i class="fa"></i> Debit</button>--}}
                                {{--</div>--}}
                                {{--<div class="btn-group" role="group">--}}
                                    {{--<button type="button" id="btnCek" class="btn btn-default"><i class="fa"></i> Cek</button>--}}
                                {{--</div>--}}
                                {{--<div class="btn-group" role="group">--}}
                                    {{--<button type="button" id="btnBG" class="btn btn-default"><i class="fa"></i> BG</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group" id="inputTunaiContainer">--}}
                            {{--<label class="control-label">Nominal Tunai (Rp)</label>--}}
                            {{--<input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control" />--}}
                        {{--</div>--}}
                        {{--<div class="form-group" id="inputDebitContainer">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12 col-xs-12">--}}
                                    {{--<label class="control-label">Nama Bank</label>--}}
                                    {{--<select name="bank_id" class="select2_single form-control">--}}
                                        {{--<option id="default-bank" value="">Pilih Bank</option>--}}
                                        {{--@foreach ($banks as $bank)--}}
                                        {{--<option value="{{ $bank->id }}">{{ $bank->nama_bank }}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">No. Debit</label>--}}
                                    {{--<input type="text" name="inputNoDebit" id="inputNoDebit" class="form-control" />--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">Nominal Debit (Rp)</label>--}}
                                    {{--<input type="text" name="inputNominalDebit" id="inputNominalDebit" class="form-control" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group" id="inputCekContainer">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">No. Cek</label>--}}
                                    {{--<input type="text" name="inputNoCek" id="inputNoCek" class="form-control" />--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">Nominal Cek (Rp)</label>--}}
                                    {{--<input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group" id="inputBgContainer">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">No. BG</label>--}}
                                    {{--<input type="text" name="inputNoBgt" id="inputNoBg" class="form-control" />--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-6 col-md-6">--}}
                                    {{--<label class="control-label">Nominal BG (Rp)</label>--}}
                                    {{--<input type="text" name="inputNominalBg" id="inputNominalBg" class="form-control" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div id="formHiddenContainer">--}}
                            {{--<form id="form-hidden">--}}
                                {{--@foreach ($relasi_po_penjualan as $relasi)--}}
                                {{--<input type="hidden" id="hidden-kode-{{ $relasi->item_kode }}" value="{{ $relasi->item_kode }}" />--}}
                                {{--<input type="hidden" id="hidden-jumlah-{{ $relasi->item_kode }}" value="{{ $relasi->jumlah }}" />--}}
                                {{--<input type="hidden" id="hidden-satuan-{{ $relasi->item_kode }}" value="{{ $relasi->satuan_id }}" />--}}
                                {{--<input type="hidden" id="hidden-subtotal-{{ $relasi->item_kode }}" value="{{ intval($relasi->subtotal) }}">--}}
                                {{--@endforeach--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        {{--<div id="formSimpanContainer" style="margin-top: 20px;">--}}
                            {{--<form id="form-simpan" method="POST" action="{{ url('po-penjualan/'.$po_penjualan->id) }}">--}}
                                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}" />--}}
                                {{--<input type="hidden" name="kode_transaksi" value="{{ $po_penjualan->kode_transaksi }}" />--}}
                                {{--<input type="hidden" name="pelanggan" value="{{ $po_penjualan->pelanggan_id }}" />--}}
                                {{--<input type="hidden" name="potongan" value="" />--}}

                                {{--<input type="hidden" name="harga_total" />--}}
                                {{--<input type="hidden" name="jumlah_bayar" />--}}
                                {{--<input type="hidden" name="kembali" />--}}
                                {{----}}
                                {{--<input type="hidden" name="nominal_tunai" />--}}
                                {{----}}
                                {{--<input type="hidden" name="no_debit" />--}}
                                {{--<input type="hidden" name="nominal_debit" />--}}
                                {{----}}
                                {{--<input type="hidden" name="no_cek" />--}}
                                {{--<input type="hidden" name="nominal_cek" />--}}
                                {{----}}
                                {{--<input type="hidden" name="no_bg" />--}}
                                {{--<input type="hidden" name="nominal_bg" />--}}

                                {{--<div id="append-section">--}}
                                    {{--@foreach ($relasi_po_penjualan as $relasi)--}}
                                    {{--<input type="hidden" id="item-{{ $relasi->item_kode }}" value="{{ $relasi->item_kode }}" />--}}
                                    {{--<input type="hidden" id="jumlah-{{ $relasi->item_kode }}" value="{{ $relasi->jumlah }}" />--}}
                                    {{--<input type="hidden" id="satuan-{{ $relasi->item_kode }}" value="{{ $relasi->satuan_id }}" />--}}
                                    {{--<input type="hidden" class="subtotal" id="subtotal-{{ $relasi->item_kode }}" value="{{ intval($relasi->subtotal) }}" />--}}
                                    {{--@endforeach--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<div class="form-group pull-left">--}}
                                        {{--<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>   --}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    {{--<div class="col-md-12 col-xs-12">--}}
        {{--<div class="x_panel">--}}
            {{--<div class="x_title">--}}
                {{--<h2>Keranjang PO Penjualan</h2>--}}
                {{--<button type="button" class="btn btn-sm btn-primary pull-right" id="btn-aksi">--}}
                    {{--<i class="fa fa-plus"></i> <span style="color: white;">Tambah Item</span>--}}
                {{--</button>--}}
                {{--<div class="clearfix"></div>--}}
            {{--</div>--}}
            {{--<div class="x_content">--}}
                {{--<div class="row" id="selectItemContainer" style="margin-bottom: 10px">--}}
                    {{--<div class="col-md-5 col-xs-12">--}}
                        {{--<form class="form-horizontal">--}}
                            {{--<div class="form-group">--}}
                                {{--<label class="control-label" style="margin-bottom: 5px;">Nama Item</label>--}}
                                {{--<select name="item_id" class="select2_single form-control">--}}
                                    {{--<option id="default">Pilih Item</option>--}}
                                    {{--@foreach ($items as $item)--}}
                                    {{--<option value="{{ $item->kode }}">{{ $item->nama }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-7 col-xs-12">--}}
                        {{--<div class="x_title">--}}
                            {{--<h2>Informasi Item</h2>--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        {{--<table class="table" id="tabel-info">--}}
                            {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Nama Item</th>--}}
                                    {{--<th>Stok</th>--}}
                                {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<td></td>--}}
                                    {{--<td></td>--}}
                                {{--</tr>--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<table class="table" id="tabel-item">--}}
                    {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th></th>--}}
                            {{--<th>Nama Item</th>--}}
                            {{--<th>Jumlah</th>--}}
                            {{--<th>Satuan</th>--}}
                            {{--<th>Sub Total (Rp)</th>--}}
                            {{--<th>Aksi</th>--}}
                        {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                        {{--@foreach ($relasi_po_penjualan as $relasi)--}}
                        {{--<tr data-id="{{ $relasi->item_kode }}">--}}
                            {{--<td class="tengah-hv">--}}
                                {{--<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato;"></i>--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</td>--}}
                            {{--<td class="tengah-v">{{ $relasi->item->nama }}</td>--}}
                            {{--<td class="tengah-hv">--}}
                                {{--<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm" readonly="readonly" value="{{ $relasi->jumlah }}" />--}}
                            {{--</td>--}}
                            {{--<td class="tengah-hv">--}}
                                {{--<select name="satuan" class="form-control input-sm" id="selectSatuanItem" disabled="disabled">--}}
                                    {{--@foreach ($satuans as $satuan)--}}
                                    {{--<option value="{{ $satuan->id }}" {{ $relasi->satuan_id == $satuan->id ? 'selected="selected"' : '' }} >{{ $satuan->kode }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</td>--}}
                            {{--<td class="tengah-hv">--}}
                                {{--<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm" readonly="readonly" value="{{ \App\Util::duit($relasi->subtotal) }}" />--}}
                                {{--<input type="hidden" value="{{ intval($relasi->subtotal) }}">--}}
                            {{--</td>--}}
                            {{--<td style="padding-top: 13px;" class="text-center">--}}
                                {{--<button type="button" class="btn btn-sm btn-success" id="btn-ubah">--}}
                                    {{--<i class="fa fa-edit"></i> Edit--}}
                                {{--</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--@endforeach--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('script')
    <script type="text/javascript">
        $('#tabel-item').DataTable();

        var banks = null;
        var items = null;
        var satuans = null;
        var po_penjualan = null;
        var relasi_po_penjualan = null;

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            banks = '{{ $banks }}';
            banks = banks.replace(/&quot;/g, '"');
            banks = JSON.parse(banks);
            
            items = '{{ $items }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);
            
            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            po_penjualan = '{{ $po_penjualan }}';
            po_penjualan = po_penjualan.replace(/&quot;/g, '"');
            po_penjualan = JSON.parse(po_penjualan);

            relasi_po_penjualan = '{{ $relasi_po_penjualan }}';
            relasi_po_penjualan = relasi_po_penjualan.replace(/&quot;/g, '"');
            relasi_po_penjualan = JSON.parse(relasi_po_penjualan);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var v_jumlah = 1;
                var v_satuan = satuans[0];

                for (var i = 0; i < relasi_po_penjualan.length; i++) {
                    if (relasi_po_penjualan[i].id == id) {
                        relasi = relasi_po_penjualan[i];
                    }
                }

                var item = relasi.item;

                for (var i = 0; i < item.satuans.length; i++) {
                    var satuan = item.satuans[i];
                    var konversi = satuan.konversi;

                    if (relasi.jumlah >= konversi && relasi.jumlah % konversi == 0) {
                        v_jumlah = relasi.jumlah / konversi;
                        v_satuan = satuan.satuan;
                    }
                }

                $(el).find('#jumlah').text(v_jumlah + ' ' + v_satuan.kode);
            });
        });
    </script>

    {{-- <script type="text/javascript">
        $('#tabel-item').DataTable();

        var select_satuan = 
            '<select name="satuan" class="form-control input-sm" id="selectSatuanItem">'+
                @foreach($satuans as $satuan)
                '<option value="{{ $satuan->id }}">{{ $satuan->kode }}</option>'+
                @endforeach
            '</select>';

        function updateHargaOnKeyup() {
            var $harga_total  = $('#inputHargaTotal');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $kembali      = $('#inputTotalKembali');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_debit = $('#formSimpanContainer').find('input[name="nominal_debit"]').val();
            var nominal_cek   = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
            var nominal_bg    = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();

            nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_debit = parseInt(nominal_debit.replace(/\D/g, ''), 10);
            nominal_cek   = parseInt(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg    = parseInt(nominal_bg.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_debit)) nominal_debit = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
            var jumlah_bayar = nominal_tunai + nominal_debit + nominal_cek + nominal_bg;
            var kembali = jumlah_bayar - harga_total;

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            $harga_total.val(harga_total.toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());
            $kembali.val(kembali.toLocaleString());

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="kembali"]').val(kembali);
        }

        function updateHargaTotal() {
            var harga_total = 0;

            $('.subtotal').each(function(index, el) {
                var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(tmp)) tmp = 0;
                harga_total += tmp;
            });

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('input[name="harga_total"]').val(harga_total);
            $('#hiddenHargaTotal').val(harga_total);

            var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            var kembali      = jumlah_bayar - harga_total;

            if (kembali < 0) kembali = 0;

            $('#inputTotalKembali').val(kembali.toLocaleString());
            $('input[name="kembali"]').val(kembali);
        }

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                allowClear: true
            });

            $('#selectItemContainer').hide();
            $('#inputTunaiContainer').hide();
            $('#inputDebitContainer').hide();
            $('#inputDebitContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBgContainer').hide();
            $('#inputBgContainer').find('input').val('');

            $('#inputHargaTotal').val({{ $po_penjualan->harga_total }}.toLocaleString());
            $('#inputNegoTotal').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);

            $('select[name="bank_id"]').next('span').css({
                'width': '100%',
                'margin-bottom': '10px'
            });
        });

        $(document).on('click', '#btn-aksi', function(event) {
            event.preventDefault();
            
            if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary').addClass('btn-warning');
                $(this).find('i').removeClass('fa-plus').addClass('fa-times').next().text('Batal');
                $('#selectItemContainer').show(400)
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning').addClass('btn-primary');
                $(this).find('i').removeClass('fa-times').addClass('fa-plus').next().text('Tambah Item');
                $('#selectItemContainer').hide(400);
            }
        });

        $(document).on('click', '#btn-ubah', function(event) {
            event.preventDefault();
            
            var id = $(this).parents('tr').data('id');
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]');

            tr.find('#inputJumlahItem').removeAttr('readonly').select();
            tr.find('#selectSatuanItem').removeAttr('disabled');

            tr.find('td').first().find('i').removeAttr('title').attr('title', 'Reset');
            tr.find('td').first().find('i').removeAttr('id').attr('id', 'reset');
            tr.find('td').first().find('i').removeAttr('style').css('cursor', 'pointer');
            tr.find('td').first().find('i').removeClass('fa-times').addClass('fa-refresh');
            tr.find('td').first().find('input[name="_token"]').remove();

            tr.css('background-color', '#f5f5f5');

            $(this).empty();
            $(this).removeAttr('id').attr('id', 'btn-simpan');
            $(this).removeAttr('type').attr('type', 'submit');
            $(this).append('<i class="fa fa-save"></i> Update');

            $(this).parent().prepend('{{ csrf_field() }}');
        });

        $(document).on('click', '#btn-simpan', function(event) {
            event.preventDefault();

            var btn = $(this);
            
            var id = $(this).parents('tr').data('id');
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]');

            var url = "{{ url('po-penjualan') }}"+'/{{ $po_penjualan->id }}/item/update/'+id;
            var token = $(this).prev().val();

            var jumlah   = tr.find('#inputJumlahItem').val();
            var satuan   = tr.find('#selectSatuanItem').val();
            var subtotal = tr.find('#inputSubTotal').val();

            subtotal = parseInt(subtotal.replace(/\D/g, ''), 10);

            $.ajax({
                url: url,
                type: 'PUT',
                data: {
                    _token: token,
                    jumlah: jumlah,
                    satuan: satuan,
                    subtotal: subtotal
                },
                beforeSend:function() {
                    btn.attr('disabled', 'disabled').empty();
                    btn.append('<i class="fa fa-circle-o-notch fa-spin"></i> Updating');
                },
                success:function() {
                    btn.removeAttr('disabled').empty();
                    btn.append('<i class="fa fa-edit"></i> Edit');
                    btn.removeAttr('id').attr('id', 'btn-ubah');
                    btn.removeAttr('type').attr('type', 'button');

                    btn.parent().find('input[name="_token"]').remove();

                    tr.find('#inputJumlahItem').attr('readonly', 'readonly');
                    tr.find('#selectSatuanItem').attr('disabled', 'disabled');

                    tr.find('td').first().find('i').removeAttr('title').attr('title', 'Hapus Barang Belanja');
                    tr.find('td').first().find('i').removeAttr('id').attr('id', 'remove');
                    tr.find('td').first().find('i').removeAttr('style').css({
                        'cursor': 'pointer',
                        'color': 'tomato'
                    });
                    tr.find('td').first().find('i').removeClass('fa-refresh').addClass('fa-times');

                    tr.css('background-color', 'transparent');

                    $('#form-hidden').find('#hidden-jumlah-'+id).val(jumlah);
                    $('#form-hidden').find('#hidden-satuan-'+id).val(satuan);
                    $('#form-hidden').find('#hidden-subtotal-'+id).val(subtotal);
                }
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnDebit', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').addClass('fa-check');
                $('#inputDebitContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputDebitContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_debit"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_debit"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputCekContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
                $('#inputBgContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputBgContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();
            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;

            $(this).val(nominal_tunai.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoDebit', function(event) {
            event.preventDefault();
            var no_debit = $(this).val();

            $('input[name="no_debit"]').val(no_debit);
        });

        $(document).on('keyup', '#inputNominalDebit', function(event) {
            event.preventDefault();
            var nominal_debit = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_debit)) nominal_debit = 0;

            $(this).val(nominal_debit.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_debit"]').val(nominal_debit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();
            var no_cek = $(this).val();

            $('input[name="no_cek"]').val(no_cek);
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();
            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;

            $(this).val(nominal_cek.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoBg', function(event) {
            event.preventDefault();
            var no_bg = $(this).val();

            $('input[name="no_bg"]').val(no_bg);
        });

        $(document).on('keyup', '#inputNominalBg', function(event) {
            event.preventDefault();
            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;

            $(this).val(nominal_bg.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name=item_id]', function(event) {
            event.preventDefault();
            
            var id = $(this).val();
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]').data('id');

            var url  = "{{ url('transaksi-grosir') }}"+'/'+id+'/item/json';

            $.get(url, function(data) {
                var nama      = data.item.nama;
                var stoktotal = data.stok.stok;

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+id+'" value="'+id+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+id+'" value="0" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +id+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal[]" id="subtotal-'+id+'" value="" />');

                    $('#tabel-item').find('tbody').append(
                        '<tr data-id="'+id+'">'+
                            '<td class="tengah-hv">'+
                                '<i class="fa fa-times" title="Hapus Barang Belanja" id="hapus" style="cursor: pointer; color: tomato;"></i>'+
                                '{{ csrf_field() }}'+
                            '</td>'+
                            '<td class="tengah-v">'+nama+'</td>'+
                            '<td class="tengah-hv">'+
                                '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm" />'+
                            '</td>'+
                            '<td class="tengah-hv">'+select_satuan+'</td>'+
                            '<td class="tengah-hv">'+
                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm" value="0" readonly="readonly" />'+
                                '<input type="hidden" value="0" />'+
                            '</td>'+
                            '<td style="padding-top: 13px;" class="text-center">'+
                                '<button type="submit" class="btn btn-sm btn-primary" id="btn-tambah" disabled="disabled">'+
                                    '<i class="fa fa-plus"></i> Tambah ke PO'+
                                '</button>'+
                            '</td>'+
                        '</tr>');
                }

                $('#tabel-info').find('tbody').children('tr').children().first().text(nama);
                $('#tabel-info').find('tbody').children('tr').children().last().text(stoktotal);
            });
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();
            
            var jumlah = $(this).val();
            if (jumlah === '') jumlah = 0;

            var id     = $(this).parents('tr').data('id');
            var tr     = $('#tabel-item').find('tr[data-id="'+id+'"]');
            var satuan = $('#satuan-'+id).val();

            ($(this).val() === '') ? tr.find('button').last().attr('disabled', 'disabled') : tr.find('button').last().removeAttr('disabled');

            var url    = "{{ url('transaksi-grosir') }}"+'/'+id+'/harga/json/'+satuan+'/'+jumlah;

            $('#jumlah-'+id).val(jumlah);

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputSubTotal').val(0);
                } else {
                    var harga       = data.harga;
                    var subtotal    = parseInt(harga) * parseInt(jumlah);
                    var harga_total = 0;

                    var konversi    = parseInt(data.satuans.konversi) * parseInt(jumlah);

                    tr.find('#inputSubTotal').val(subtotal.toLocaleString());
                    tr.find('#inputSubTotal').next().val(subtotal);

                    $('#subtotal-'+id).val(subtotal);

                    updateHargaTotal();
                }
            });
        });

        $(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();
            
            var id = $(this).parents('tr').data('id');
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]');

            var satuan = $(this).val();
            var jumlah = $(this).parent().prev().find('input').val();
            if (jumlah === '') jumlah = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+id+'/harga/json/'+satuan+'/'+jumlah;

            $('#satuan-'+id).val(satuan);

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputSubTotal').val(0);
                } else {
                    var harga       = data.harga;
                    var subtotal    = parseInt(harga) * parseInt(jumlah);
                    var harga_total = 0;

                    var konversi    = parseInt(data.satuans.konversi) * parseInt(jumlah);

                    tr.find('#inputSubTotal').val(subtotal.toLocaleString());
                    tr.find('#inputSubTotal').next().val(subtotal);

                    $('#subtotal-'+id).val(subtotal);

                    updateHargaTotal();
                }
            });
        });

        $(document).on('click', '#btn-tambah', function(event) {
            event.preventDefault();

            var btn = $(this);
            btn.parent().prepend('{{ csrf_field() }}');

            var id = $(this).parents('tr').data('id');
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]');

            var jumlah   = parseInt(tr.find('#inputJumlahItem').val());
            var satuan   = parseInt(tr.find('#selectSatuanItem').val());
            var subtotal = parseInt(tr.find('#inputSubTotal').next().val());

            var url   = "{{ url('po-penjualan') }}"+'/'+{{ $po_penjualan->id }}+'/item/tambah';
            var token = $(this).prev().val();

            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    _token: token,
                    kode: id,
                    jumlah: jumlah,
                    satuan: satuan,
                    subtotal: subtotal
                },
                beforeSend:function() {
                    btn.attr('disabled', 'disabled').empty();
                    btn.append('<i class="fa fa-circle-o-notch fa-spin"></i> Menambah');
                },
                success:function(data) {
                    btn.removeAttr('disabled').empty();
                    btn.removeAttr('id').attr('id', 'btn-ubah');
                    btn.removeClass('btn-primary');
                    btn.addClass('btn-success');
                    btn.append('<i class="fa fa-edit"></i> Edit');

                    tr.find('#inputJumlahItem').attr('readonly', 'readonly');
                    tr.find('#selectSatuanItem').attr('disabled', 'disabled');

                    if ($('#form-hidden').find('#hidden-kode-'+id).val() === undefined) {
                        $('#form-hidden').append('<input type="hidden" id="hidden-kode-'+id+'" value="'+id+'" />');
                        $('#form-hidden').append('<input type="hidden" id="hidden-jumlah-'+id+'" value="'+jumlah+'" />');
                        $('#form-hidden').append('<input type="hidden" id="hidden-satuan-'+id+'" value="'+satuan+'" />');
                        $('#form-hidden').append('<input type="hidden" id="hidden-subtotal-'+id+'" value="'+subtotal+'" />');
                    }
                }
            });
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabel-item').find('tr[data-id="'+kode+'"]');
            var subtotal     = parseInt(tr.children().last().prev().find('input').next().val().replace(/\D/g, ''), 10);
            var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            var kembali      = parseInt($('#inputTotalKembali').val().replace(/\D/g, ''), 10);

            var url = "{{ url('po-penjualan') }}"+'/{{ $po_penjualan->id }}/item/hapus/'+kode;
            var token = $(this).next().val();

            $.ajax({
                url: url,
                type: 'DELETE',
                data: {
                    _token: token
                },
                beforeSend:function() {
                    tr.css('background-color', '#eee');
                    tr.find('i.fa').first().removeClass('fa-times');
                    tr.find('i.fa').first().addClass('fa-spinner fa-spin fa-lg');
                    tr.find('i.fa').first().css('color', '#aaa');
                    tr.find('button').last().attr('disabled', 'disabled');
                },
                success:function() {
                    harga_total -= subtotal;
                    kembali = jumlah_bayar - harga_total;

                    if (kembali < 0) kembali = 0;

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('#inputTotalKembali').val(kembali.toLocaleString());

                    $('input[name="harga_total"]').val(harga_total);
                    $('input[name="kembali"]').val(kembali);

                    tr.fadeOut('fast', function() {
                        tr.remove();
                    });
                    $('#form-hidden').find('input[id*=-'+kode+']').remove();
                }
            });
        });

        $(document).on('click', '#hapus', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabel-item').find('tr[data-id="'+kode+'"]');
            var subtotal     = parseInt(tr.children().last().prev().find('input').next().val().replace(/\D/g, ''), 10);
            var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            var kembali      = parseInt($('#inputTotalKembali').val().replace(/\D/g, ''), 10);

            harga_total -= subtotal;
            kembali = jumlah_bayar - harga_total;

            if (kembali < 0) kembali = 0;

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('#inputTotalKembali').val(kembali.toLocaleString());

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="kembali"]').val(kembali);

            tr.fadeOut(400, function() {
                tr.remove();
            });
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });

        $(document).on('click', '#reset', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').data('id');
            var tr = $('#tabel-item').find('tr[data-id="'+id+'"]');

            var jumlah   = $('#form-hidden').find('#hidden-jumlah-'+id).val();
            var satuan   = $('#form-hidden').find('#hidden-satuan-'+id).val();
            var subtotal = $('#form-hidden').find('#hidden-subtotal-'+id).val();

            subtotal = parseInt(subtotal).toLocaleString();

            tr.find('#inputJumlahItem').val(jumlah).attr('readonly', 'readonly');
            tr.find('#selectSatuanItem').val(satuan).attr('disabled', 'disabled');
            tr.find('#inputSubTotal').val(subtotal);

            tr.removeAttr('style');
            tr.find('td').first().empty();
            tr.find('td').first().append('<i class="fa fa-times"></i>');
            tr.find('i.fa').first().attr('id', 'hapus');
            tr.find('i.fa').first().removeAttr('style').css({
                'cursor': 'pointer',
                'color': 'tomato'
            });
            tr.find('button').last().empty();
            tr.find('button').last().removeAttr('id').attr('id', 'btn-ubah');
            tr.find('button').last().append('<i class="fa fa-edit"></i> Edit');
        });

    </script> --}}

@endsection
