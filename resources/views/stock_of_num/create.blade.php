@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Pengecekan Stok</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #tableItem input {
            min-width: 50px;
            width: 100%;
        }
        textarea {
            min-width: 100%;
            max-width: 48px;
            min-height: 100%;
            max-height: 100%;
        }
        .full-width {
            width: 100%;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="full-width">Tambah Pengecekan Stok</h2>
                        <span id="kodeSonTitle" class="full-width"></span>
                    </div>
                    <div class="col-md-4">
                        <form method="post" action="{{ url('stock-of-num') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="kode_son">
                            <div id="append-section" class="hidden">
                                @foreach ($items as $i => $item)
                                    <input type="hidden" name="item_id[]" value="{{$item->id}}">
                                    <input type="hidden" name="stok_sistem[]" value="" id="stok_sistem-{{$item->id}}">
                                    <input type="hidden" name="stok_fisik[]" value="" id="stok_fisik-{{$item->id}}">
                                    <input type="hidden" name="keterangan[]" value="" id="keterangan-{{$item->id}}">
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-sm btn-success pull-right" id="btnUbah">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                            <a href="{{ url('stock-of-num') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                                <i class="fa fa-long-arrow-left"></i>
                            </a>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Item</th>
                            <th width="12%">Stok Sistem</th>
                            <th width="12%">Stok Fisik</th>
                            <th width="5">Cocok</th>
                            <th width="25%">Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $i => $item)
                        <tr id="{{ $item->id }}" item_kode="{{ $item->kode }}">
                            {{-- <input type="hidden" name="item_id[]" value="{{ $item->id }}"> --}}
                            <td width="5%">{{ $i + 1 }}</td>
                            <td width="20%">
                                <p>{{ $item->nama }}</p>
                                <p>{{ $item->kode }}</p>
                                </td>
                            <td width="25%">
                                <input type="hidden" class="stok_sistem" name="stok_sistem[]" value="{{ $item->stok }}">
                                @if(sizeof($item->jumlah) > 0)
                                    @foreach ($item->jumlah as $j => $jumlah)
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" disabled="" value="{{ \App\Util::angka($jumlah['jumlah']) }}">
                                                <div class="input-group-addon" style="width: 70px;">{{ $jumlah['satuan']->kode }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </td>
                            <td width="25%">
                                {{-- <input type="hidden" name="stok_fisik[]" value="{{ $item->stok }}"> --}}
                                @if(sizeof($item->jumlah) > 0)
                                    @foreach ($item->jumlah as $j => $jumlah)
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm angka inputJumlah" konversi="{{ $jumlah['konversi'] }}">
                                                <div class="input-group-addon" style="width: 70px;">{{ $jumlah['satuan']->kode }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center match">
                                {{-- <span class="match_badge badge bg-red"><i class="fa_badge fa fa-close"></i></span> --}}
                                <span class="match_badge label label-danger">Tidak</label>
                            </td>
                            <td width="25%">
                                <textarea name="keterangan[]" class="form-control input-sm ket_input"></textarea>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{-- <button type="submit" class="btn btn-sm btn-success pull-right" id="btnUbah">
                <i class="fa fa-save"></i> Simpan
            </button> --}}
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        var items = [];

        $('#tableItem').DataTable();

        $(document).ready(function() {
            var url = "{{ url('stock-of-num') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.ket_input').prop('readonly', false);

            items = '{{ json_encode($items) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);
            // console.log(items.length);
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var id = item.id;
                var stok = item.stok;
                $('#stok_sistem-'+id).val(stok);
                // $('#stok_fisik-'+id).val(stok);
                $('#keterangan-'+id).val('');
                // console.log(i);
            }

            $('.inputJumlah').val('');
            $('textarea[name="keterangan[]"]').val('');
        });

        // Buat ambil nomer transaksi terakhir
        $(window).on('load', function(event) {
            var url = "{{ url('stock-of-num/last.json') }}";

            $.get(url, function(data) {
                var kode = 1;
                if (data.stock_of_num !== null) {
                    var kode_son = data.stock_of_num.kode_son;
                    kode = parseInt(kode_son.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_son = kode + '/PGS/' + tanggal;

                $('input[name="kode_son"]').val(kode_son);
                $('#kodeSonTitle').text(kode_son);
            });
        });

        $(document).on('keyup', '.inputJumlah', function(event) {
            event.preventDefault();

            var x = $(this).parents('tr').first();
            var id = x.attr('id');
            var jumlah_sistem = x.find('.stok_sistem').val();
            var match_badge = x.find('.match_badge');

            var $td = $(this).parents('td').first();
            var jumlah = 0;
            $td.find('.inputJumlah').each(function(index, el) {
                // jumlah += $(el).val();
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            });
            // console.log(jumlah_sistem, jumlah);
            var keterangan = x.find('.ket_input');

            // $td.find('input[name="stok_fisik[]"]').val(jumlah);
            $('#stok_fisik-'+id).val(jumlah);
            if(jumlah_sistem == jumlah){
                $(match_badge).removeClass('label-danger');
                $(match_badge).addClass('label-success');
                $(match_badge).text('Cocok');

                $(keterangan).prop('readonly', true);
                // console.log('sama');
            }else{
                $(match_badge).removeClass('label-success');
                $(match_badge).addClass('label-danger');
                $(match_badge).text('Tidak');
                $(keterangan).prop('readonly', false);
                // console.log('tidak');
            }
        });

        /*$(document).on('keyup', '.inputJumlah', function(event) {
            event.preventDefault();

            var $td = $(this).parents('td').first();
            var jumlah = 0;
            $td.find('.inputJumlah').each(function(index, el) {
                var temp_jumlah = parseInt($(el).val());
                var konversi = parseInt($(el).attr('konversi'));

                if (isNaN(temp_jumlah)) temp_jumlah = 0;
                if (isNaN(konversi)) konversi = 0;

                jumlah += (temp_jumlah * konversi);
            });
            // console.log(jumlah);

            var id = $(this).parents('tr').first().attr('id');
            $('#stok_fisik-'+id).val(jumlah);
        });*/

        $(document).on('keyup', 'textarea[name="keterangan[]"]', function(event) {
            event.preventDefault();

            var $td = $(this).parents('td').first();
            var keterangan = $(this).val();

            var id = $(this).parents('tr').first().attr('id');
            $('#keterangan-'+id).val(keterangan);
        });

    </script>
@endsection
