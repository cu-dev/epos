@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Pengecekan Stok</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pengecekan Stok</h2>
                <a href="{{ url('stock-of-num/create') }}" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Tambah Pengecekan Stok">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Kode Pengecekan Stok</th>
                            <th>Operator</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sons as $i => $son)
                        <tr id="{{ $son->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $son->created_at->format('Y-m-d') }}</td>
                            <td>{{ $son->kode_son }}</td>
                            <td>{{ $son->user->nama }}</td>
                            <td>
                                <a href="{{ url('stock-of-num/'.$son->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fa fa-eye"></i>
                                </a>
                                {{-- <button class="btn btn-xs btn-danger" id="btnHapus">
                                    <i class="fa fa-trash"></i> Hapus
                                </button> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $('#tableItem').DataTable({
            // 'order': [[1, 'desc'], [2, 'desc']]
        });

        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama_item + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("item") }}' + '/' + id);
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });

    </script>
@endsection
