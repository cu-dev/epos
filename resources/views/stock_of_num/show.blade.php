@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Pengecekan Stok</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #tableItem input {
            min-width: 50px;
            width: 100%;
        }
        td h4 {
            margin-top: 0;
            margin-bottom: 5px;
        }
        .full-width {
            width: 100%;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="full-width">Detail Pengecekan Stok - Operator : {{ $son->user->nama }} </h2>
                        <span id="kodeSonTitle" class="full-width">{{ $son->kode_son }}</span>
                    </div>
                    <div class="col-md-4">
                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="kode_son">
                        <button type="submit" class="btn btn-sm btn-success pull-right" id="btnUbah">
                            <i class="fa fa-save"></i> Simpan
                        </button> --}}
                        <a href="{{ url('stock-of-num') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Item</th>
                            <th width="12%">Stok Sistem</th>
                            <th width="12%">Stok Fisik</th>
                            <th width="12%">Cocok</th>
                            <th width="25%">Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($son->items as $i => $item)
                        <tr id="{{ $item->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td class="text-center">
                                @foreach ($item->stok_sistem as $i => $stok_sistem )
                                    <h4><span class="label label-info">{{ $stok_sistem['jumlah'] }} {{ $stok_sistem['satuan']->nama }}</span></h4>
                                @endforeach
                            </td>
                            <td class="text-center">
                                @foreach ($item->stok_fisik as $i => $stok_fisik )
                                    <h4><span class="label label-info">{{ $stok_fisik['jumlah'] }} {{ $stok_fisik['satuan']->nama }}</span></h4>
                                @endforeach
                            </td>
                            <td>
                                @if($item->sistem_stok == $item->fisik_stok)
                                    Cocok
                                @else
                                    Tidak
                                @endif
                            </td>
                            {{-- <td>{{ $item->pivot->stok_sistem }}</td>
                            <td>{{ $item->pivot->stok_fisik }}</td> --}}
                            @if($item->pivot->keterangan == NULL)
                                <td class="text-center">-
                            @else
                                <td>{{ $item->pivot->keterangan }}
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#tableItem').DataTable();
        $(document).ready(function() {
            var url = "{{ url('stock-of-num') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });
    </script>
@endsection
