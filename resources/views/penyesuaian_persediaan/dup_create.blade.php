@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Prive Persediaan</title>
@endsection

@section('style')
	<style media="screen">
		#btnKembali {
			margin-right: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.form-danger, 
		.form-danger:focus {
			border-color: red;
		}

		#metodePembayaranButtonGroup,
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}

		#metodePembayaranButtonGroup > label {
			border-radius: 0;
		}

		#metodePembayaranButtonGroup > label.active {
			background: #26b99a;
			border: 1px solid #169f85;
			color: white;
			font-weight: bold;
			border-radius: 0;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-8">
						<h2 id="formSimpanTitle">Tambah Prive Persediaan</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					<div class="col-md-4 pull-right">
						<a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Pilih Item</label>
						<select name="item_id" id="pilihItem" class="select2_single form-control">
							<option id="default">Pilih Item</option>
							@foreach ($items as $item)
							<option value="{{ $item->kode }}">[{{ $item->kode }}] {{ $item->nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Informasi Item</h2>
				<a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Stok</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Belanja</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;">
					<thead>
						<tr>
							<th></th>
							<th>Item</th>
							<th>Jumlah</th>
							<th>Satuan</th>
							<th>Harga per satuan (Rp)</th>
							<th>Sub Total (Rp)</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			
			<div class="col-md-4 col-xs-6 col-md-offset-8">
				<div class="form-group">
					<label class="control-label">Harga Nilai Barang (Rp)</label>
					<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />
				</div>
				<div id="formSimpanContainer" style="margin-top: 20px;">
					<form id="form-simpan" action="{{ url('transaksi') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						<input type="hidden" name="kode_transaksi" value="" />
						<input type="hidden" name="harga_total" />

						<div id="append-section"></div>
						<div class="clearfix">
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>	
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				html: '<p>Transaksi Eceran berhasil ditambah!</p>' + 
						'<table class="table table-bordered table-striped">' +
							'<tr>' +
								'<td>Total</td>' +
								'<td>' + '{{ session('data')['total'] }}'.toLocaleString() + '</td>' +
							'</tr>' +
							'<tr>' +
								'<td>Tunai</td>' +
								'<td>' + '{{ session('data')['tunai'] }}'.toLocaleString() + '</td>' +
							'</tr>' +
							'<tr>' +
								'<td>Kembalian</td>' +
								'<td>' + '{{ session('data')['kembalian'] }}'.toLocaleString() + '</td>' +
							'</tr>' +
						'</table>',
				// timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Eceran gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script>
		$('#tabelTransaksiPenjualan').DataTable();

		$(document).ready(function() {
			$(".select2_single").select2({
				allowClear: true
			});
		});
		
	</script>

	<script type="text/javascript">
		// var satuan = '1';
		// var harga_total	= 0;
		var select_satuan = '<select name="satuan" class="form-control input-sm">@foreach($satuans as $satuan)<option value="{{ $satuan->id }}">{{ $satuan->kode }}</option>@endforeach</select>';

		function updateHargaTotal() {
			var harga_total = 0;

			$('.subtotal').each(function(index, el) {
				var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(tmp)) tmp = 0;
				harga_total += tmp;
			});

			$('#inputHargaTotal').val(harga_total.toLocaleString());
			$('input[name="harga_total"]').val(harga_total);
			$('#hiddenHargaTotal').val(harga_total);

			// var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
			var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
			// var kembali      = jumlah_bayar - harga_total;

			// if (kembali < 0 || isNaN(kembali)) {
			// 	kembali = 0;
			// 	$('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
			// } else {
			// 	$('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
			// }

			// $('#inputTotalKembali').val(kembali.toLocaleString());
			// $('input[name="kembali"]').val(kembali);
		}

		function updateHargaOnKeyup() {
			var $harga_total  = $('#inputHargaTotal');
			// var $jumlah_bayar = $('#inputJumlahBayar');
			// var $kembali      = $('#inputTotalKembali');

			// var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
			// var nominal_debit = $('#formSimpanContainer').find('input[name="nominal_debit"]').val();
			// var nominal_cek   = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
			// var nominal_bg    = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();

			// nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
			// nominal_debit = parseInt(nominal_debit.replace(/\D/g, ''), 10);
			// nominal_cek   = parseInt(nominal_cek.replace(/\D/g, ''), 10);
			// nominal_bg    = parseInt(nominal_bg.replace(/\D/g, ''), 10);

			// if (isNaN(nominal_tunai)) nominal_tunai = 0;
			// if (isNaN(nominal_debit)) nominal_debit = 0;
			// if (isNaN(nominal_cek)) nominal_cek = 0;
			// if (isNaN(nominal_bg)) nominal_bg = 0;

			var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
			// var jumlah_bayar = nominal_tunai + nominal_debit + nominal_cek + nominal_bg;
			// var kembali = jumlah_bayar - harga_total;

			if (isNaN(harga_total)) harga_total = 0;
			// if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			
				// if (kembali < 0 || isNaN(kembali)) {
				// 	kembali = 0;
				// 	$('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
				// } else {
				// 	$('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
				// }

			$harga_total.val(harga_total.toLocaleString());
			// $jumlah_bayar.val(jumlah_bayar.toLocaleString());
			// $kembali.val(kembali.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			// $('input[name="jumlah_bayar"]').val(jumlah_bayar);
			// $('input[name="kembali"]').val(kembali);
		}

		function cariItem(kode, url, tr) {
			$.get(url, function(data) {
				// console.log(data, url);
				var nama = data.item.nama;
				var harga = data.harga.harga;
				var stoktotal = data.item.stoktotal;
				harga = parseInt(harga.replace(/\D/g, ''), 10) / 100;
				
				if (tr === undefined) {
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="1" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="1" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal[]" id="subtotal-'+kode+'" value="" />');

					$('#tabelKeranjang').find('tbody').append(
						'<tr data-id="'+kode+'">'+
							'<td>'+
								'<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
							'</td>'+
							'<td style="padding-top: 13px">'+nama+'</td>'+
							'<td>'+
								'<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="1" />'+
							'</td>'+
							'<td>'+select_satuan+'</td>'+
							'<td>'+
								'<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
							'</td>'+
							'<td>'+
								'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
							'</td>'+
						'</tr>');
				}

				$('#tabelInfo').find('tbody').children('tr').children().first().text(nama);
				$('#tabelInfo').find('tbody').children('tr').children().last().text(stoktotal);

				var $harga_total = $('#inputHargaTotal');
				// var $jumlah_bayar = $('#inputJumlahBayar');
				// var $kembali = $('#inputTotalKembali');

				var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
				// var jumlah_bayar = parseInt($jumlah_bayar.val().replace(/\D/g, ''), 10);
				// var kembali = jumlah_bayar - harga_total;
				// kembali = kembali < 0 ? 0 : kembali;

				$harga_total.val((harga_total + harga).toLocaleString());
				// $kembali.val(kembali.toLocaleString());

				$('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + harga);
				// $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val(jumlah_bayar);
				// $('#formSimpanContainer').find('input[name="kembali"]').val(kembali);
			});
		}

		$(document).ready(function() {
			var url = "{{ url('transaksi/create') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			// $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');

			// $('#inputTunaiContainer').hide();
			// $('#inputDebitContainer').hide();
			// $('#inputDebitContainer').find('input').val('');
			// $('#inputCekContainer').hide();
			$(".select2_single").select2({
				allowClear: true
			});

			$('#inputHargaTotal').val(0);
			// $('#inputJumlahBayar').val(0);
			// $('#inputTotalKembali').val(0);

			// $('select[name="bank_id"]').next('span').css({
			// 	'width': '100%',
			// 	'margin-bottom': '10px'
			// });
		});

		$(window).on('load', function(event) {

			var url = "{{ url('transaksi/last/json') }}";
			var tanggal = printTanggalSekarang('dd/mm/yyyy');

			$.get(url, function(data) {
				if (data.transaksi_penjualan === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/TRAJ/' + tanggal;
				} else {
					var kode_transaksi = data.transaksi_penjualan.kode_transaksi;
					var dd_transaksi = kode_transaksi.split('/')[2];
					var mm_transaksi = kode_transaksi.split('/')[3];
					var yyyy_transaksi = kode_transaksi.split('/')[4];
					var tanggal_transaksi = dd_transaksi + '/' + mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/TRAJ/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/TRAJ/' + tanggal_transaksi;
					}
				}

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		// var keyupFromScanner = false;
		$(document).scannerDetection({
			avgTimeByChar: 40,
			onComplete: function(code, qty) {
				console.log('Kode: ' + code, qty);
			},
			onError: function(error) {
				// console.log('Barcode: ' + error);
				var kode = error;
				var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
				var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

				var terpilih = false;
				$('input[name="item_kode[]"]').each(function(index, el) {
					if ($(el).val() == kode) {
						terpilih = true;
					}
				});

				if (terpilih) {
					// keyupFromScanner = true;
					var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val());
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val(jumlah_awal + 1);
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('keyup');
					$('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('blur');
				} else {
					cariItem(kode, url, tr);
				}
			}
		});

		$(document).on('change', '#pilihItem', function(event) {
			event.preventDefault();
			
			var kode = $(this).val();
			var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
			var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

			cariItem(kode, url, tr);
		});

		var temp_jumlah = 0;
		$(document).on('click', '#inputJumlahItem', function(event) {
			event.preventDefault();
			temp_jumlah = $(this).val();
			$(this).val('');
		});

		$(document).on('blur', '#inputJumlahItem', function(event) {
			event.preventDefault();
			$(this).val(temp_jumlah);
		});

		$(document).on('keyup', '#inputJumlahItem', function(event) {
			event.preventDefault();
			
			var jumlah = $(this).val();
			temp_jumlah = jumlah;

			if (jumlah === '') jumlah = 0;

			var kode   = $(this).parents('tr').first().data('id');
			var satuan = $('#satuan-'+kode).val();
			var url    = "{{ url('transaksi') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
			var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

			$('#jumlah-'+kode).val(jumlah);
			// console.log('keyup jumlah item', url);

			// $('#inputJumlahItem').blur();
			// $('#spinner').show();
			$.get(url, function(data) {
				if (data.harga === null) {
					tr.find('#inputHargaPerSatuan').val(0);
					tr.find('#inputSubTotal').val(0);
				} else {
					var harga       = data.harga.harga;
					harga = parseInt(harga.replace(/\D/g, ''), 10) / 100;
					var subtotal    = harga * parseInt(jumlah);
					var harga_total = 0;

					tr.find('#inputHargaPerSatuan').val(harga.toLocaleString());
					tr.find('#inputSubTotal').val(subtotal.toLocaleString());

					$('#harga-'+kode).val(harga);
					$('#subtotal-'+kode).val(subtotal);

					$('.subtotal').each(function(index, el) {
						var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
						if (isNaN(tmp)) tmp = 0;
						harga_total += tmp;
					});

					$('#inputHargaTotal').val(harga_total.toLocaleString());
					$('input[name="harga_total"]').val(harga_total);

					// var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
					var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
					// var kembali      = jumlah_bayar - harga_total;
					
					// if (kembali < 0 || isNaN(kembali)) {
					// 	kembali = 0;
					// 	$('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
					// } else {
					// 	$('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
					// }

					// $('#inputTotalKembali').val(kembali.toLocaleString());
					// $('input[name="kembali"]').val(kembali);
				}

				// if (!keyupFromScanner) {
				// 	$('#inputJumlahItem').focus();
				// }
				// $('#spinner').hide();
				// keyupFromScanner = false;
			});
			// console.log('keyup jumlah item selesai', url);
		});

		$(document).on('change', 'select[name="satuan"]', function(event) {
			event.preventDefault();

			var satuan = $(this).val();
			var kode   = $(this).parents('tr').data('id');
			var jumlah = $(this).parent().prev().children('input').val();

			if (jumlah === '') jumlah = 0;

			var url    = "{{ url('transaksi') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
			var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

			$('#satuan-'+kode).val(satuan);

			$.get(url, function(data) {
				if (data.harga === null) {
					tr.find('#inputHargaPerSatuan').val(0);
					tr.find('#inputSubTotal').val(0);
				} else {
					var harga       = data.harga.harga;
					var subtotal    = parseInt(harga) * parseInt(jumlah);
					var harga_total = 0;

					tr.find('#inputHargaPerSatuan').val(harga.toLocaleString());
					tr.find('#inputSubTotal').val(subtotal.toLocaleString());

					$('#harga-'+kode).val(harga);
					$('#subtotal-'+kode).val(subtotal);

					$('.subtotal').each(function(index, el) {
						var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
						if (isNaN(tmp)) tmp = 0;
						harga_total += tmp;
					});

					$('#inputHargaTotal').val(harga_total.toLocaleString());
					$('input[name="harga_total"]').val(harga_total);

					// var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
					var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
					// var kembali      = jumlah_bayar - harga_total;
					
					// if (kembali < 0 || isNaN(kembali)) {
					// 	kembali = 0;
					// 	$('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
					// } else {
					// 	$('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
					// }

					// $('#inputTotalKembali').val(kembali.toLocaleString());
					// $('input[name="kembali"]').val(kembali);
				}
			});
		});

		// $(document).on('click', '#btnTunai', function(event) {
		// 	event.preventDefault();

		// 	if ($(this).hasClass('btn-default')) {
		// 		$(this).removeClass('btn-default');
		// 		$(this).addClass('btn-danger');
		// 		$(this).find('i').addClass('fa-check');
		// 		$('#inputTunaiContainer').show('fast', function() {
		// 			$(this).find('input').first().trigger('focus');
		// 		});
		// 	} else if ($(this).hasClass('btn-danger')) {
		// 		$(this).removeClass('btn-danger');
		// 		$(this).find('i').removeClass('fa-check');
		// 		$(this).addClass('btn-default');
		// 		$('#inputTunaiContainer').hide('hide', function() {
		// 			$('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
		// 			$(this).find('input').val('');
		// 			updateHargaOnKeyup();
		// 		});
		// 	}
		// });

		// $(document).on('click', '#btnDebit', function(event) {
		// 	event.preventDefault();

		// 	if ($(this).hasClass('btn-default')) {
		// 		$(this).removeClass('btn-default');
		// 		$(this).addClass('btn-warning');
		// 		$(this).find('i').addClass('fa-check');
		// 		$('#inputDebitContainer').show('fast', function() {
		// 			$(this).find('input').first().trigger('focus');
		// 		});
		// 	} else if ($(this).hasClass('btn-warning')) {
		// 		$(this).removeClass('btn-warning');
		// 		$(this).find('i').removeClass('fa-check');
		// 		$(this).addClass('btn-default');
		// 		$('#inputDebitContainer').hide('hide', function() {
		// 			$('#formSimpanContainer').find('input[name="no_debit"]').val('');
		// 			$('#formSimpanContainer').find('input[name="nominal_debit"]').val('');
		// 			$(this).find('input').val('');
		// 			updateHargaOnKeyup();
		// 		});
		// 	}
		// });

		// $(document).on('keyup', '#inputNominalTunai', function(event) {
		// 	event.preventDefault();
		// 	var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
		// 	if (isNaN(nominal_tunai)) nominal_tunai = 0;

		// 	$(this).val(nominal_tunai.toLocaleString());
		// 	$('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
		// 	updateHargaOnKeyup();
		// });

		// $(document).on('change', 'select[name="bank_id"]', function(event) {
		// 	event.preventDefault();
			
		// 	var id = $(this).val();

		// 	$('input[name="bank"]').val(id);
		// });

		// $(document).on('keyup', '#inputNoDebit', function(event) {
		// 	event.preventDefault();
		// 	var no_debit = $(this).val();

		// 	$('input[name="no_debit"]').val(no_debit);
		// });

		// $(document).on('keyup', '#inputNominalDebit', function(event) {
		// 	event.preventDefault();
		// 	var nominal_debit = parseInt($(this).val().replace(/\D/g, ''), 10);
		// 	if (isNaN(nominal_debit)) nominal_debit = 0;

		// 	$(this).val(nominal_debit.toLocaleString());
		// 	$('#formSimpanContainer').find('input[name="nominal_debit"]').val(nominal_debit);
		// 	updateHargaOnKeyup();
		// });

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			
			var kode         = $(this).parents('tr').data('id');
			var tr           = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
			var subtotal     = parseInt(tr.children().last().find('input').val().replace(/\D/g, ''), 10);
			var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
			// var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
			// var kembali      = parseInt($('#inputTotalKembali').val().replace(/\D/g, ''), 10);

			harga_total -= subtotal;
			// kembali = jumlah_bayar - harga_total;

			$('#inputHargaTotal').val(harga_total.toLocaleString());
			// $('#inputTotalKembali').val(kembali.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			// $('input[name="kembali"]').val(kembali);

			tr.remove();
			$('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
		});
		
	</script>
@endsection
