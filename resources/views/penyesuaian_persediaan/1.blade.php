@extends('layouts.admin')

@section('title')
	<title>EPOS | Transaksi Eceran</title>
@endsection

@section('style')
	<style media="screen">
		#btnDetail, #btnTambah, #btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		#btnTambah {
			margin-right: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.form-danger, 
		.form-danger:focus {
			border-color: red;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Rekap Transaksi Penjualan Eceran</h2>
				<a href="{{ url('transaksi/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah">
					<i class="fa fa-plus"></i> Tambah
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tabelTransaksiPenjualan">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Kode Transaksi</th>
							<th>Total</th>
							<th>Kasir</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transaksis as $i => $transaksi)
						<tr data-id="{{ $i++ }}">
							<td class="tengah-v">{{ $i++ }}</td>
							{{-- <td>{{ $transaksi->created_at->format('d-m-Y H:i:s') }}</td> --}}
							<td class="tengah-v">{{ $transaksi->created_at->format('d-m-Y') }}</td>
							<td class="tengah-v">{{ $transaksi->kode_transaksi }}</td>
							<td class="tengah-v">Rp {{ \App\Util::duit($transaksi->harga_total) }}</td>
							<td class="tengah-v">{{ $transaksi->user->nama }}</td>
							<td class="tengah-v">
								<a href="{{ url('transaksi/'.$transaksi->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Penjualan">
									<i class="fa fa-eye"></i>
								</a>
								<a href="{{ url('transaksi/'.$transaksi->id.'/retur') }}" class="btn btn-xs btn-warning" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Lihat Retur">
									<i class="fa fa-sign-out"></i>
								</a>
								<a href="{{ url('transaksi/'.$transaksi->id.'/retur/create') }}" class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Tambah Retur">
									<i class="fa fa-sign-out"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('#tabelTransaksiPenjualan').DataTable({
			// 'order': [[2, 'desc'], [1, 'desc']]
		});
	</script>
	{{-- @if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Eceran berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Eceran gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Eceran berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Eceran gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Eceran berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Eceran gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script>
		$('#tabelTransaksiPenjualan').DataTable();

		$(document).ready(function() {
			$(".select2_single").select2({
				allowClear: true
			});
		});
		
	</script> --}}
@endsection

