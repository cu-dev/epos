@extends('layouts.admin')

@section('title')
    <title>EPOS | Beranda</title>
@endsection

@section('style')
    <style media="screen">
        .thumbnail {
            padding: 20px;
        }
        .jarak {
            padding-bottom: 15px;
        }
        .jarak i {
            font-size: 0.75em;
        }
        .jarak_kiri {
            margin-left: 10px;
        }
        .avatar-view {
            margin-top: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="jumbotron col-md-12">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>EPOS - {{ $data->nama }}</h1>
                    </div>
                </div>
                <div class="row" style="padding-top: 3%">
                    <div class="col-md-4">
                         <img class="img-responsive avatar-view" src="{{ URL::to('/images/'.$data->logo) }}" alt="Avatar" title="Change the avatar">
                    </div>
                    <div class="col-md-8" style="padding-left: 10%">
                        <h3 class="jarak"><i class="fa fa-map-marker"></i> {{ $data->alamat }}</h3>
                        <h3 class="jarak"><i class="fa fa-envelope"></i> {{ $data->email }}</h3>
                        <h3 class="jarak"><i class="fa fa-print"></i> {{ $data->telepon }}</h3>
                        <h3 class="jarak"><i class="fa fa-phone"></i> {{ $data->wa_perusahaan }}</h3>
                        <h3 class="jarak"><i class="fa fa-book"></i> {{ $data->npwp }} 
                            @if($data->tanggal_npwp != '0000-00-00')
                                - {{ \App\Util::date($data->tanggal_npwp) }}
                            @endif
                        </h3>
                        <h3 class="jarak"><i class="fa fa-list-alt"></i> {{ $data->pkp }}
                            @if($data->tanggal_pkp != '0000-00-00')
                                - {{ \App\Util::date($data->tanggal_pkp) }}
                            @endif
                        </h3>
                    </div>
                </div>
                <div class="row" style="padding-top: 5%" id="hutang_piutang">
                    {{-- <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="count">Hutang</div>
                            <h3>New Sign ups</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="count">Piutang</div>
                            <h3>New Sign ups</h3>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses'))
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Anda berhasil login!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif
    <script type="text/javascript">
        $(window).on('load', function(event) {
            event.preventDefault();

            var data = '{{ json_encode($angka) }}';
            data = data.replace(/&quot;/g, '"');
            data = JSON.parse(data);
            if(data.status == 1){
                var hutang_total = 0;
                var piutang_total = 0;

                var hutang_dagang = parseFloat(data.hutang_dagang);
                var hutang_bank = parseFloat(data.hutang_bank);
                var piutang_dagang = parseFloat(data.piutang_dagang);
                var piutang_lain = parseFloat(data.piutang_lain);

                if(isNaN(hutang_dagang)) hutang_dagang = 0;
                if(isNaN(hutang_bank)) hutang_bank = 0;
                if(isNaN(piutang_dagang)) piutang_dagang = 0;
                if(isNaN(piutang_lain)) piutang_lain = 0;

                var hutang_total = hutang_dagang + hutang_bank;
                var piutang_total = piutang_dagang + piutang_lain;

                $('#hutang_piutang').append(`
                    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="count">Hutang</div>
                            <h3>Total Hutang</h3>
                            <h4 class="jarak_kiri">Rp`+ hutang_total.toLocaleString(['ban', 'id']) +`</h4>
                            <h3>Hutang Dagang</h3>
                            <h4 class="jarak_kiri">Rp`+ hutang_dagang.toLocaleString(['ban', 'id']) +`</h4>
                            <h3>Hutang Bank</h3>
                            <h4 class="jarak_kiri">Rp`+ hutang_bank.toLocaleString(['ban', 'id']) +`</h4>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="count">Piutang</div>
                            <h3>Total Piutang</h3>
                            <h4 class="jarak_kiri">Rp`+ piutang_total.toLocaleString(['ban', 'id']) +`</h4>
                            <h3>Piutang Dagang</h3>
                            <h4 class="jarak_kiri">Rp`+ piutang_dagang.toLocaleString(['ban', 'id']) +`</h4>
                            <h3>Piutang Bank</h3>
                            <h4 class="jarak_kiri">Rp`+ piutang_lain.toLocaleString(['ban', 'id']) +`</h4>
                        </div>
                    </div>
                    `);
            }
        });
    </script>
@endsection