@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Pesanan Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
        .no-border {
            border: none;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
                <h2>Detail Pesanan Pembelian</h2>
                <a href="{{ url('po-pembelian/'.$po_pembelian->id.'/beli') }}" class="btn btn-sm btn-success pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Beli Pesanan Pembelian">
                    <i class="fa fa-plus"></i>
                </a>
                <a href="{{ url('cetak/po-pembelian/'.$po_pembelian->id) }}" class="btn btn-sm btn-cetak pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Cetak Pesanan Pembelian">
                    <i class="fa fa-print"></i>
                </a>
                <a href="{{ url('po-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="content invoice">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="2" style="text-align: left;">Detail Transaksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Kode Transaksi</td>
                                            <td>{{ $po_pembelian->kode_transaksi }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pemasok</td>
                                            <td>{{ $po_pembelian->suplier->nama }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left;">Nama Item</th>
                                            <th style="text-align: left;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($relasi_po_pembelian as $num => $relasi)
                                            <tr>
                                                <td>{{ $relasi->item->nama }}</td>
                                                {{-- <td>{{ $relasi->jumlah }}</td> --}}
                                                <td>
                                                @foreach ($relasi->jumlah as $j => $jumlah)
                                                    <span class="label label-info">{{ $jumlah['jumlah'] }} {{ $jumlah['satuan']->kode }}</span>
                                                @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function() {
            var url = "{{ url('po-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
        });

    </script>
@endsection
