@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Pesanan Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        td > .input-group {
            margin-bottom: 0;
        }
        #btnKembali {
            margin: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-6">
                        <h2 id="formSimpanTitle">Tambah Pesanan Pembelian</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    {{-- <div class="col-md-6 pull-right">
                        <a href="{{ url('po-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div> --}}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Pemasok</label>
                        <select name="suplier_id" class="select2_single form-control">
                            <option id="default" value="">Pilih Pemasok</option>
                            @foreach($supliers as $suplier)
                            <option value="{{ $suplier->id }}">{{$suplier->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('po-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th width="70%" style="text-align: left;">Item</th>
                                    <th width="30%" style="text-align: left;">Stok</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Pembelian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <table class="table" id="tabelKeranjang">
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <table class="table">
                            <tbody>
                                <tr>
                                    {{-- <td class="invisible" style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td> --}}
                                    {{-- <td clas="invisible"><h3><small></small></h3><h3></h3></td> --}}
                                    <td style="width: 100%;">
                                        <div id="formSimpanContainer">
                                            <form id="form-simpan" action="{{ url('po-pembelian') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <input type="hidden" name="kode_transaksi" value="" />
                                                <input type="hidden" name="suplier_id" />
                                                <input type="hidden" name="po" value="1" />

                                                <div id="append-section"></div>
                                                <div class="clearfix">
                                                    <div class="form-group pull-right">
                                                        <button type="submit" id="submit" class="form-control btn btn-success pull-right" style="width: 100px; margin-right: 0;">
                                                            <i class="fa fa-check"></i> OK
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-sm-6 col-xs-6 pull-right">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('po-pembelian') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="suplier_id" />
                                        <input type="hidden" name="po" value="1" />

                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                            <div class="form-group pull-left">
                                                <button type="submit" id="submit" class="form-control btn btn-success pull-right" style="width: 100px;">
                                                    <i class="fa fa-check"></i> OK
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function() {
            var url = "{{ url('po-pembelian/create') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                allowClear: false,
                width: '100%'
            });

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            });
        });

        // Buat ambil nomer transaksi terakhir
        /*$(window).on('load', function(event) {
            var url = "{{ url('transaksi-pembelian/last.json') }}";

            $.get(url, function(data) {
                var kode = 1;
                if (data.transaksi_pembelian !== null) {
                    var kode_transaksi = data.transaksi_pembelian.kode_transaksi;
                    kode = parseInt(kode_transaksi.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_transaksi = kode + '/TRAB/' + tanggal;

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });*/

        $(document).on('change', 'select[name="suplier_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('transaksi-pembelian') }}" + '/' + id + '/items.json';

            $('input[name="suplier_id"]').val(id);
            $('#tabelKeranjang > tbody').empty();
            $('#append-section').empty();

            var $select = $('select[name="item_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Item</option>'));

            $.get(url, function(data) {
                var items = data.items;
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    $select.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
                }
            });
        });

        /* $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var id = $('select[name="item_id"]').val();
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

            $.get(url, function(data) {
                // console.log(data);
                var pcs = data.pcs;
                var item = data.item;
                var nama = item.nama;
                var stok = item.stoktotal;
                var satuans = item.satuan_pembelians;

                var stoks = '';
                var temp = stok;
                if (satuans.length > 0) {
                    for (var i = 0; i < satuans.length; i++) {
                        var satuan = satuans[i];
                        var hasil = parseInt(temp / satuan.konversi);
                        if (hasil > 0) {
                            temp %= satuan.konversi;
                            stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                        }
                    }
                } else {
                    stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                }

                $('#tabelInfo').find('tbody').empty();
                $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

                if (tr === undefined) {
                    // $('#tabelKeranjang').find('tbody').append('<tr data-id="' + id + '"><td><i class="fa fa-times" id="remove" style="color: tomato; cursor: pointer;"></i></td><td>' + nama + '</td><td><input type="text" name="inputJumlah" id="inputJumlah" class="form-control input-sm angka"></td></tr>');
                    var divs = '';
                    for (var i = 0; i < satuans.length; i++) {
                        var satuan = satuans[i];
                        divs += '<div class="form-group">'+
                                    '<label class="control-label">Jumlah '+satuan.satuan.kode+'</label>'+
                                    '<input type="text" name="inputJumlah" id="inputJumlah" class="form-control input-sm angka" konversi="'+satuan.konversi+'">'+
                                '</div>';
                    }

                    var tr = '<tr data-id="' + id + '">'+
                                '<input type="hidden" name="jumlah">'+
                                '<td><i class="fa fa-times" id="remove" style="color: tomato; cursor: pointer;"></i></td>'+
                                '<td>'+nama+'</td>'+
                                '<td>'+
                                    divs+
                                '</td>'+
                            '</tr>';

                    $('#tabelKeranjang').find('tbody').append($(tr));
                }
            });
        }); */

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var id = $('select[name="item_id"]').val();
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

            $.get(url, function(data) {
                // console.log(data);
                var pcs = data.pcs;
                var item = data.item;
                var kode = item.kode;
                var nama = item.nama;
                var stok = item.stoktotal;
                var satuans = item.satuan_pembelians;

                var stoks = '';
                var temp = stok;
                if (satuans.length > 0) {
                    for (var i = 0; i < satuans.length; i++) {
                        var satuan = satuans[i];
                        var hasil = parseInt(temp / satuan.konversi);
                        if (hasil > 0) {
                            temp %= satuan.konversi;
                            stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                        }
                    }
                } else {
                    stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                }

                $('#tabelInfo').find('tbody').empty();
                $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

                if (tr === undefined) {
                    var divs = '<div class="form-group pull-right">'+
                                    '<label class="control-label">Jumlah</label>';

                    if (satuans.length > 0) {
                        for (var i = 0; i < satuans.length; i++) {
                            var satuan = satuans[i];
                            divs += '<div class="input-group text-center">' +
                                        '<input type="text" id="inputJumlah" name="inputJumlah" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'">' +
                                        '<div class="input-group-addon kode_satuan" style="width: 62px; text-align: right;">'+satuan.satuan.kode+'</div>' +
                                    '</div>';
                        }
                        divs += '</div>';
                    } else {
                        divs += '<div class="input-group">' +
                                    '<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
                                    '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+pcs.kode+'</div>' +
                                '</div>' +
                            '</div>';
                    }

                    var tr = '<tr data-id="' + id + '">'+
                                '<input type="hidden" name="jumlah">'+
                                '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3>'+
                                '</td>'+
                                '<td style="width: 600px;">'+
                                    divs+
                                '</td>'+
                            '</tr>';

                    $('#tabelKeranjang').find('tbody').append($(tr));
                }
            });
        });

        $(document).on('keyup', '#inputJumlah', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $jumlah = $tr.find('input[name="jumlah"]');

            var jumlah = 0;
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            });

            if (isNaN(jumlah)) jumlah = 0;

            $jumlah.val(jumlah);
            
            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').data('id');
            $('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
            $('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();
            
            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
        });

    </script>
@endsection
