@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }

        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }

        .header_grosir{
            font-size: 10px;
            line-height: 1;
        }

        /*.invisible {
            visibility: visible;
        }*/

        .stempel p {
            font-size: 10px;
        }
    </style>
@endsection

@section('app.body')
    <img class="logo kmk" src="{{ URL::to('images/logo1.png') }}" style="margin-top: 1px; width: 175px; height: 90px; display: none;">
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>
    <script type="text/javascript" charset="utf-8" async defer>
        var catatans = [];
        var pkp = '';
        var npwp = '';
        var log = null;
        var pelanggan = null;
        var pembayarans = null;
        var catatans = null;
        var data_perusahaan = null;
        var operator1 = '';
        var operator2 = '';

        var $kmk = $('.kmk').clone();
        $kmk.show();
        $('.kmk').remove();

        $(document).ready(function() {
            data_perusahaan = '{{ json_encode($data_perusahaan) }}';
            data_perusahaan = data_perusahaan.replace(/&quot;/g, '"');
            data_perusahaan = JSON.parse(data_perusahaan);

            log = '{{ json_encode($log) }}';
            log = log.replace(/&quot;/g, '"');
            log = JSON.parse(log);

            pelanggan = '{{ json_encode($pelanggan) }}';
            pelanggan = pelanggan.replace(/&quot;/g, '"');
            pelanggan = JSON.parse(pelanggan);

            pembayarans = '{{ json_encode($pembayarans) }}';
            pembayarans = pembayarans.replace(/&quot;/g, '"');
            pembayarans = JSON.parse(pembayarans);

            catatans = '{{ json_encode($catatans) }}';
            catatans = catatans.replace(/&quot;/g, '"');
            catatans = JSON.parse(catatans);

            operator1 = '{{ json_encode($operator1) }}';
            operator1 = operator1.replace(/&quot;/g, '');

            operator2 = '{{ json_encode($operator2) }}';
            operator2 = operator2.replace(/&quot;/g, '');

            if(data_perusahaan.npwp == null || data_perusahaan.npwp == undefined || data_perusahaan.npwp == '') {
                npwp = '-';
            } else {
                npwp = data_perusahaan.npwp;
            }

            if(data_perusahaan.pkp == null || data_perusahaan.pkp == undefined || data_perusahaan.pkp == '') {
                pkp = '-';
            } else {
                pkp = data_perusahaan.pkp;
            }

            var min_item = 10;
            var max_item = 20;
            var jumlah_item = 30;
            var jumlah_halaman = 1;

            var nomor_item = 1;
            var table_head = ``+
            `<table width="100%" class="table-head">
                <tr>
                    <td class="kmkContainer" rowspan="7" align="left" style="width: 80mm;" valign="top" style="margin-right: 0">
                    </td>
                    <td rowspan="7" align="left" style="width: 100mm;" valign="bottom">
                        <p style="font-weight: bold; font-size: 12px; text-transform: uppercase; line-height:1;">Toko Eceran & Grosir</p>
                        <p style="font-weight: bold; font-size: 14px; text-transform: uppercase; line-height:1.2;">${data_perusahaan.nama}</p>
                        <p style="line-height:1;">${data_perusahaan.alamat}</p>
                        <p style="text-decoration:underline; line-height:1;margin-bottom:2px;">${data_perusahaan.email}</p>
                        <p style="line-height:1;">Telp. ${data_perusahaan.telepon} | WA ${data_perusahaan.wa_penjualan}</p>
                        <p style="line-height:1;">No. NPWP ${npwp} | Tgl. SK ${ymd2dmyMiring(data_perusahaan.tanggal_npwp)}</p>
                        <p style="line-height:1;">No. PKP ${pkp} | Tgl. SK ${ymd2dmyMiring(data_perusahaan.tanggal_pkp)}</p>
                    </td>
                    <td colspan="3">
                        <p style="font-weight: bold; font-size: 14px; margin-bottom: 2px; z-index: 10;">DEPOSITO PELANGGAN</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 40mm;">
                        <p class="header_grosir"> Kode Transaksi </p>
                    </td>
                    <td>
                        <p class="header_grosir"> : </p>
                    </td>
                    <td style="width: 60mm;">
                        <p class="header_grosir"> `+log.kode_transaksi+` </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="header_grosir"> Tanggal & Waktu </p>
                    </td>
                    <td>
                        <p class="header_grosir"> : </p>
                    </td>
                    <td>
                        <p class="header_grosir"> `+log.timestamp+` </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="header_grosir"> Pelanggan </p>
                    </td>
                    <td>
                        <p class="header_grosir"> : </p>
                    </td>
                    <td>
                        <p class="header_grosir"> `+pangkasNama(pelanggan.nama)+` </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="header_grosir"> Toko </p>
                    </td>
                    <td>
                        <p class="header_grosir"> : </p>
                    </td>
                    <td>
                        <p class="header_grosir"> `+pelanggan.toko+` </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="header_grosir"> Telepon </p>
                    </td>
                    <td>
                        <p class="header_grosir"> : </p>
                    </td>
                    <td>
                        <p class="header_grosir"> `+pelanggan.telepon+` </p>
                    </td>
                </tr>
                <tr class="invisible"></tr>
            </table>`;

            $('body').append($(table_head));

            var tr= '';
            var panjang_baris = pembayarans.length + 2;
            for(var i = 0; i < pembayarans.length; i++) {
                if (i == 0) {
                    tr += ``+
                        `<tr>
                            <td>`+(i+1)+`</td>
                            <td>`+pembayarans[i]['metode']+`</td>
                            <td>`+pembayarans[i]['bank']+`</td>
                            <td>`+pembayarans[i]['jenis']+`</td>
                            <td>`+pembayarans[i]['nomor']+`</td>
                            <td class="text-right">`+pembayarans[i]['jumlah']+`</td>
                            <td rowspan="`+panjang_baris+`">`+log.keterangan+`</td>
                        </tr>`
                } else {
                    tr += ``+
                    `<tr>
                        <td>`+(i+1)+`</td>
                        <td>`+pembayarans[i]['metode']+`</td>
                        <td>`+pembayarans[i]['bank']+`</td>
                        <td style="text-transform: capitalize">`+pembayarans[i]['jenis']+`</td>
                        <td>`+pembayarans[i]['nomor']+`</td>
                        <td class="text-right">`+pembayarans[i]['jumlah']+`</td>
                    </tr>`
                }
            }

            tr += ``+
                `<tr>
                    <td  rowspan="2" colspan="4" style="text-transform: uppercase"><b>terbilang:</b> #`+terbilang(log.jumlah_bayar)+`#</td>
                    <td style="font-weight: bold">Saldo Deposito Sebelumnya</td>
                    <td class="text-right">`+log.deposito_sebelum+`,-</td>
                </tr>`;

            tr += ``+
                `<tr>
                    <td style="font-weight: bold">Saldo Deposito Saat Ini</td>
                    <td class="text-right">`+log.total_deposito+`,-</td>
                </tr>`;

            thead = `<tr>
                <td colspan="1" width="4%" style="vertical-align: bottom; text-align: center; font-weight: bold;">No</td>
                <td colspan="1" width="10%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Metode Pembayaran</td>
                <td colspan="1" width="10%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Bank</td>
                <td colspan="1" width="5%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Jenis Kartu</td>
                <td colspan="1" width="20%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Nomor</td>
                <td colspan="1" width="12%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Nominal</td>
                <td colspan="1" width="20%" style="vertical-align: bottom; text-align: center; font-weight: bold;">Keterangan</td>
            </tr>`;

            var table_body = ``+
                `<table width="100%" class="table table-bordered" style="margin-top:10px; border-top: none; border-bottom: none; border-right: none;">`+
                    `<tbody id="data">`+
                        thead+
                        tr+
                    `</tbody>
                </table>`;

            $('body').append($(table_body));

            var max_char = 28;
            var p1 = '';
            var p2 = '';
            var p3 = '';
            var p1_text = '';
            var p2_text = '';
            var p3_text = '';

            if (catatans[0].catatan.length <= max_char) {
                p1_text = catatans[0].catatan;
            } else {
                var catatan = catatans[0].catatan.split(' ');
                for (var j = 0; j < catatan.length; j++) {
                    if (p1_text.length + catatan[j].length < max_char) {
                        p1_text += catatan[j] + ' ';
                    } else if (p1_text.length + catatan[j].length == max_char) {
                        p1_text += catatan[j];
                    } else if (p2_text.length + catatan[j].length < max_char) {
                        p2_text += catatan[j] + ' ';
                    } else if (p2_text.length + catatan[j].length == max_char) {
                        p2_text += catatan[j];
                    } else if (p3_text.length + catatan[j].length < max_char) {
                        p3_text += catatan[j] + ' ';
                    } else if (p3_text.length + catatan[j].length == max_char) {
                        p3_text += catatan[j];
                    }
                }
            }
            if (p1_text.length > 0) {
                p1 = '<span>- </span>'+p1_text;
            }
            if (p2_text.length > 0) {
                p2 = '<span class="invisible">- </span>'+p2_text;
            }
            if (p3_text.length > 0) {
                p3 = '<span class="invisible">- </span>'+p3_text;
            }

            if (catatans[1].catatan.length <= max_char) {
                if (p2_text == '') {
                    p2_text = catatans[1].catatan;
                } else {
                    p3_text = catatans[1].catatan;
                }
            } else {
                var catatan = catatans[1].catatan.split(' ');
                if (p2_text == '') {
                    for (var j = 0; j < catatan.length; j++) {
                        if (p2_text.length + catatan[j].length < max_char) {
                            p2_text += catatan[j] + ' ';
                        } else if (p2_text.length + catatan[j].length == max_char) {
                            p2_text += catatan[j];
                        } else if (p3_text.length + catatan[j].length < max_char) {
                            p3_text += catatan[j] + ' ';
                        } else if (p3_text.length + catatan[j].length == max_char) {
                            p3_text += catatan[j];
                        }
                    }
                } else {
                    for (var j = 0; j < catatan.length; j++) {
                        if (p3_text.length + catatan[j].length < max_char) {
                            p3_text += catatan[j] + ' ';
                        } else if (p3_text.length + catatan[j].length == max_char) {
                            p3_text += catatan[j];
                        }
                    }
                }
            }
            if (p2_text.length > 0 && p2.length <= 0) {
                p2 = '<span>- </span>'+p2_text;
                if (p3_text.length > 0) {
                    p3 = '<span class="invisible">- </span>'+p3_text;
                }
            }
            if (p3_text.length > 0 && p3.length <= 0) {
                p3 = '<span>- </span>'+p3_text;
            }

            if (catatans[2].catatan.length <= max_char) {
                if (p3_text == '') {
                    p3_text = catatans[2].catatan;
                }
            } else {
                var catatan = catatans[2].catatan.split(' ');
                if (p3_text == '') {
                    for (var j = 0; j < catatan.length; j++) {
                        if (p3_text.length + catatan[j].length < max_char) {
                            p3_text += catatan[j] + ' ';
                        } else if (p3_text.length + catatan[j].length == max_char) {
                            p3_text += catatan[j];
                        }
                    }
                } else {
                }
            }

            if (p3_text.length > 0 && p3.length <= 0) {
                p3 = '<span>- </span>'+p3_text;
            }

            var table_foot = ``+
                `<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
                    <tbody>
                        <tr>
                            <td width="31%" style="padding-top: 5px; border: 1px solid; font-weight: bold">
                                    <p style="font-size: 10px;">CATATAN :</p>
                                    <p style="font-size: 10px; text-transform: uppercase;">`+p1+`</p>
                                    <p style="font-size: 10px; text-transform: uppercase;">`+p2+`</p>
                                    <p style="font-size: 10px; text-transform: uppercase;">`+p3+`</p>
                                    <p class="invisible" style="font-size: 10px;">KMK MANAGEMENT</p>
                                    <p style="font-size: 10px;">KMK MANAGEMENT</p>
                            </td>
                            <td class="stempel" width="23%" align="right" style="border-top:0;">
                                <p class="invisible" style="width: 95%; text-align: center;">Penerima</p>
                                <p style="width: 95%; text-align: center; margin-top: 43px; border-bottom: solid 1px #000;">&nbsp;`+pangkasNama(log.penyerah)+`</p>
                                <p style="width: 95%; text-align: center;">Penyerah (Nama & Stampel)</p>
                            </td>
                            <td class="stempel" width="23%" align="right" style="border-top:0;">
                                <p class="invisible" style="width: 95%; text-align: center;">Penjual</p>
                                <p style="width: 95%; text-align: center; margin-top: 43px; border-bottom: solid 1px #000;">&nbsp;`+pangkasNama(operator1)+`</p>
                                <p style="width: 95%; text-align: center;">Penerima (Nama & Stampel)</p>
                            </td>
                            <td class="stempel" width="23%" align="right" style="border-top:0;">
                                <p class="invisible" style="width: 95%; text-align: center;">Penjual</p>
                                <p style="width: 95%; text-align: center; margin-top: 43px; border-bottom: solid 1px #000;">&nbsp;`+pangkasNama(operator2)+`</p>
                                <p style="width: 95%; text-align: center;">Eksekutor (Nama & Stampel)</p>
                            </td>
                        </tr>
                    </tbody>
                </table>`;

            $('body').append($(table_foot));

            $('.kmkContainer').append($kmk);
            window.print();
            var url = '{{ url('pelanggan/titipan/') }}'+ '/' +pelanggan.id;
            var ww = window.open(url, '_self');
        });

    </script>
@endsection
