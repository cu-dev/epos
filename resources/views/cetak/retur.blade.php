@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }
        
        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }
    </style>

    <style type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        @font-face {
            font-family: "RobotoCondensed-Light";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Light.ttf') }}");
        }
        @font-face {
            font-family: "RobotoCondensed-Regular";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Regular.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @page {
            size: 8cm 100cm;
            margin: 0mm 0mm 0mm 0mm;
        }

        @media print {
            margin-top: 0;
        }

        .zero{
            margin: 0;
            padding: 0;
            font-size: 10px;
            line-height: 1.2;
        }
        #cetak {
            font-size: 10px;
        }

        #toko {
            font-family: 'ClarendonBT';
            font-size: 15.6px;
            font-weight: bold;
        }

        .text-center {
            text-align: center;
            margin: 0;
        }

        #content td {
            font-size: 9.5px;
            text-transform: uppercase;
        }
        #cetak, #content {
            font-family: 'Arial';
            font-size: 9px;
        }

        table tr td.ket{
            font-size: 10px;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('app.body')
    <div id="cetak">
        <div id="header" style="margin-bottom: 10px;">
            <center>
                {{-- <img align="middle" src="{{ URL::to('images/logo.png') }}" class="profile_img" style="width: 150px; height: 54px;"> --}}
                <p class="zero" style="font-size: 12px;">RETUR PENJUALAN</p>
                <p id="toko" class="zero" style="font-size: 24px;">KENCANA MULYA</p>
                <p class="zero" style="font-size: 15.4px;">KARANGDUWUR - PETANAHAN</p>
                {{-- <p class="zero">JL. PURING-PETANAHAN NO. 3 KARANGDUWUR</p>
                <p class="zero">kencanamulyakarangduwur@gmail.com</p>
                <p class="zero">(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p> --}}
            </center>
        </div>
        {{-- <hr style="margin-bottom: 0"> --}}
        <table style="width: 100%; border-top: 0.5px solid; border-bottom: 0.5px solid;">
            <tr>
                {{-- <td class="ket">0001/TRAJ/01/11/2017</td> --}}
                <td class="ket">{{ $retur_penjualan->kode_retur }}</td>
                {{-- <td class="ket" style="text-align: right;">Kasir : Sutinem</td> --}}
                <td class="ket" style="text-align: right;">Operator : {{ $kasir }}</td>
            </tr>
            <tr>
                {{-- <td class="ket">01/11/2017 09:30:11</td> --}}
                <td class="ket">{{ $retur_penjualan->timestamp }}</td>
                <td class="ket" style="text-align:right; ">KMK MANAGEMENT</td>
            </tr>
        </table>
        {{-- <hr style="margin: 0"> --}}
        <div id="content">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td colspan="3">MASUK :</td>
                    </tr>
                    @php ($index = 1)
                    @foreach ($relasi_retur_penjualan as $a => $relasi)
                    @if ($relasi->retur == 0)
                        <tr>
                            <td width="5%">{{ $index }}</td>
                            <td width="50%">{{ $relasi->item->nama_pendek }}</td>
                            <td width="25%" style="text-align: left; ">
                                @foreach($hasil[$a] as $x => $jumlah)
                                    {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                @endforeach
                            </td>
                            <td width="20%" style="text-align: right;">
                                {{ \App\Util::angka($relasi->subtotal) }}
                            </td>
                        </tr>
                        @php ($index++)
                    @endif
                    @endforeach
                    <tr>
                        <td colspan="3">KELUAR :</td>
                    </tr>
                    @if($retur_penjualan->status == 'sama')
                        @php ($index = 1)
                        @foreach ($relasi_retur_penjualan as $a => $relasi)
                        @if ($relasi->retur == 0)
                            <tr>
                                <td width="5%">{{ $index }}</td>
                                <td width="50%">{{ $relasi->item->nama_pendek }}</td>
                                <td width="25%" style="text-align: left; ">
                                    @foreach($hasil[$a] as $x => $jumlah)
                                        {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                    @endforeach
                                </td>
                                <td width="20%" style="text-align: right;">
                                    {{ \App\Util::angka($relasi->subtotal) }}
                                </td>
                            </tr>
                            @php ($index++)
                        @endif
                        @endforeach
                    @elseif($retur_penjualan->status == 'lain')
                        @php ($index = 1)
                        @foreach ($relasi_retur_penjualan as $a => $relasi)
                        @if ($relasi->retur == 1)
                            <tr>
                                <td width="5%">{{ $index }}</td>
                                <td width="50%">{{ $relasi->item->nama_pendek }}</td>
                                <td width="25%" style="text-align: left; ">
                                    @foreach($hasil[$a] as $x => $jumlah)
                                        {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                    @endforeach
                                </td>
                                <td width="20%" style="text-align: right;">
                                    {{ \App\Util::angka($relasi->subtotal) }}
                                </td>
                            </tr>
                            @php ($index++)
                        @endif
                        @endforeach
                    @elseif($retur_penjualan->status == 'uang')
                        <tr>
                            <td width="5%">1</td>
                            <td width="50%">ganti uang</td>
                            <td width="25%" style="text-align: left; ">
                            </td>
                            <td width="20%" style="text-align: right;">
                                {{ \App\Util::angka($subtotal) }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td style="text-align: right;" colspan="3">Jumlah Bayar :</td>
                        <td colspan="1" class="duit" style="text-align: right; border-top:0.5px solid;">{{ \App\Util::angka($jumlah_bayar) }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="3">Grand Total :</td>
                        <td colspan="1" class="duit" style="text-align: right;">{{ \App\Util::angka($kekurangan) }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="3">Kembalian :</td>
                        <td colspan="1" style="text-align: right; tex">
                        @if($kembali > 0)
                            {{ \App\Util::angka($kembali) }}
                        @else
                            {{-- {{ \App\Util::angka($kembali*-1) }} --}}
                            0
                        @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top: 20px; margin-bottom: 0; padding-bottom: 0;">
                @foreach ($catatans as $i => $catatan)
                    <P style="text-align: center; text-transform: uppercase;" class="zero">
                        {{ $catatan->catatan }}
                    </P>
                @endforeach
            </div>
            {{-- <P style="text-align: center; margin-top: 20px; margin-bottom: 0; padding-bottom: 0;" class="zero">BEKERJA IKHLAS TANPA BEBAN</P> --}}
        </div>
    </div>
@endsection

@section('app.script')
    <script type="text/javascript">

       
        window.print();
        var url = '{{ url('transaksi/'.$transaksi_penjualan->id.'/retur/'.$retur_penjualan->id) }}';
        var ww = window.open(url, '_self');

    </script>
@endsection
