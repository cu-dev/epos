@extends('layouts.admin')

@section('title')
    <title>EPOS | Bank</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnUbah {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Akun Bank</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('bank') }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="control-label">Nama Bank</label>
                        <input class="form-control" type="text" name="nama_bank" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nomor Rekening</label>
                        <input class="form-control" type="text" name="no_rekening" required="">
                        <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Pemilik Rekening</label>
                        <input class="form-control" type="text" name="atas_nama" required="">
                    </div>
                    <div class="form-group" id=nominal>
                        <label class="control-label">Nominal</label>
                        <input class="form-control text-right" type="text" name="nominal_i" value="0,00" required="">
                        <input type="hidden" name="nominal" value="0">
                        <label class="control-label pull-right" id="nominal_v">0,00</label>
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <button class="btn btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                        <button class="btn btn-default" id="btnReset" type="button">
                            <i class="fa fa-refresh"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- kolom kanan -->
    <div class="col-md-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Akun Bank</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('bank/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="nama_bank">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableBank" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="nama_bank">No</th>
                                <th class="sorting" field="nama_bank">Nama Bank</th>
                                <th class="sorting" field="no_rekening">Nomor Rekening</th>
                                <th class="sorting" field="atas_nama">Pemilik Rekening</th>
                                <th class="sorting" field="nominal">Nominal</th>
                                <th class="sorting" field="users.nama">Operator</th>
                                <th class="sorting" field="nama_bank" style="width: 40px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Akun Bank berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Akun Bank gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Akun Bank berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Akun Bank gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Akun Bank berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Akun Bank gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('bank/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var bank = data[i];
                        var buttons = bank.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Akun Bank">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${bank.id}">
                                    <td>${nomor}</td>
                                    <td>${bank.nama_bank}</td>
                                    <td>${bank.no_rekening}</td>
                                    <td>${bank.atas_nama}</td>
                                    <td class="text-right">${bank.nominal}</td>
                                    <td>${bank.operator}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).on('click', '#btnUbah', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_bank = $tr.find('td').first().next().text();
            var no_rekening = $tr.find('td').first().next().next().text();
            var atas_nama = $tr.find('td').first().next().next().next().text();
            var nominal_get = $tr.find('td').first().next().next().next().next().text();
            var nominal_set = nominal_get.substr(2, nominal_get.length);
            var a = '';
            var nominal = nominal_set.split('.').join(a);

            var nominal_h = parseFloat(nominal.replace(',', '.'));
            // console.log(nominal, nominal_get, nominal_set, nominal_h);
            
            $('input[name="nominal"]').val(nominal_h);
            $('#nominal_v').text(nominal_get);
            $('input[name="nama_bank"]').val(nama_bank);
            $('input[name="no_rekening"]').val(no_rekening);
            $('input[name="atas_nama"]').val(atas_nama);
            $('input[name="nominal_i"]').val(nominal);
            $('input[name="nominal_i"]').prop('readonly', true);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("bank") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Akun bank');
        });

        $(document).on('keyup', 'input[name="nominal_i"]', function(event) {
            event.preventDefault();
            var nominals = $(this).val().split(',');
            var nominal = 0;
            if(nominals.length > 1){
                nominal = $(this).val();
                if(nominals[0].length < 1){
                    nominals[0] = 0;
                    $(this).val(nominals[0] + ',' + nominals[1]);
                }
                else if(nominals[1].length > 2){
                    nominal = $(this).val().slice(0,-1);
                    $(this).val(nominal);
                    // console.log(nominal.toLocaleString(['ban', 'id']));
                }else if(nominals[1].length < 2){
                    nominal = parseFloat(nominal.replace(',', '.'));
                    nominal = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
                    nominal = nominal.replace('.', '');
                    $(this).val(nominal);
                }
            }else{
                nominal = $(this).val();
                var bel_koma = nominal.substr(nominal.length -2);
                var depan_koma = nominal.substr(0, nominal.length -2);

                $(this).val(depan_koma + ',' + bel_koma);
            }

            var nominal_h = parseFloat(nominal.replace(',', '.'));
            var nominal_v = nominal_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

            $('#nominal_v').text('Rp'+nominal_v);
            $('input[name="nominal"]').val(nominal_h);
        }); 

        $(document).on('click', '#btnReset', function() {
            $('input[name="nominal"]').val('');
            $('#nominal_v').text();
            $('input[name="nama_bank"]').val('');
            $('input[name="no_rekening"]').val('');
            $('input[name="atas_nama"]').val('');
            $('input[name="nominal_i"]').val('0,00');
            $('input[name="nominal_i"]').prop('readonly', false);
            $('input[name="nominal_i"]').trigger('keyup');

            $('#formSimpanContainer').find('form').attr('action', '{{ url("bank") }}');
            $('#formSimpanContainer').find('input[name="_method"]').val('post');
            $('#btnSimpan span').text('Tambah');

            $('#formSimpanTitle').text('Tambah Akun bank');
        });

        $(document).on('keyup', 'input[name="no_rekening"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
