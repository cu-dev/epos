@extends('layouts.admin')

@section('title')
	<title>EPOS | Jurnal Umum</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
		/*.debet {
			background: #f9f9f9;
		}
		.kredit {
			padding-left: 30px;
		}*/
		table > thead > tr > th {
			text-align: center;
		}
		table > tbody > tr > td.tengah-hv{
			vertical-align: middle;
			text-align: center;	
			background: white;
		}
	</style>
@endsection

@section('content')
<div class="col-md-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
				@if($status==1)
					<h2 id="header_tanggal">Data Jurnal Umum {{ \App\Util::tanggal($newAwal->format('d m Y')) }}</h2>
				@else
					<h2 id="header_tanggal">Data Jurnal Umum {{ \App\Util::tanggal($newAwal->format('d m Y')) }} s/d {{ \App\Util::tanggal($akhir_show->format('d m Y')) }} </h2>
				@endif
			<!-- <h2>Data Jurnal Umum</h2> -->
			<div class="clearfix"></div>
		</div>
		<div class="x_title col-md-4" id="formPilihan">
			<form method="post" action="{{ url('jurnal/tampil') }}" class="form-horizontal">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="_method" value="post">
				<div class="form-group col-sm-12 col-xs-12">
					<label class="control-label" style="padding-bottom: 15px">Pilih Rentang Data</label>
					<fieldset>
					<div class="control-group col-md-10">
						<div class="controls">

							<div class="input-prepend input-group">
								<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
								<input type="text" style="width: 100%" name="data" id="rentang_tanggal" class="form-control active tanggal-putih" readonly="">
								<input type="hidden" name="awal">
								<input type="hidden" name="akhir">
							</div>
						</div>
					</div>
					<div class="form-group col-lg-2 pull-right">
						<button class="btn btn-sm btn-success" id="btnUbah" type="submit">
							<span style="color:white">Tampil</span>
						</button>
					</div>
					</fieldset>
				</div>
			</form>
		</div>

		<div class="x_content">
			<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableJurnal">
				<thead>
					<tr>
						{{-- <th>Tahun</th>
						<th>Bulan</th>
						<th>Tanggal</th> --}}
						<th colspan="3">Tanggal</th>
						<th>Kode Akun</th>
						<th>Rekening</th>
						<th>Referensi</th>
						<th>Keterangan</th>
						<th width="15%">Debit</th>
						<th width="15%">Kredit</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($jurnals as $i => $tahunan)
						@foreach ($tahunan['isi'] as $j => $bulanan)
							@foreach ($bulanan['isi'] as $k => $harian)
								@foreach ($harian['isi'] as $l => $jurnal)
									@if ($jurnal->awal_ref)
										<tr>
											<td colspan="6" style="background: white;"></td>
										</tr>
									@endif
									<tr class="{{ $jurnal->kredit > 0 ? 'kredit' : 'debet'}}">
										@if ($jurnal->awal_tahun)
											<td  class="tengah-hv" rowspan="{{ $tahunan['row'] }}">{{ $i }}</td>
										@else 
											{{-- <td style="display: none;"></td> --}}
										@endif
										@if ($jurnal->awal_bulan)
											<td class="tengah-hv" rowspan="{{ $bulanan['row'] }}">{{ $j }}</td>
										@else 
											{{-- <td style="display: none;"></td> --}}
										@endif
										@if ($jurnal->awal_hari)
											<td class="tengah-hv" rowspan="{{ $harian['row'] }}">{{ $k }}</td>
										@else 
											{{-- <td style="display: none;"></td> --}}
										@endif
										<td align="center">{{ $jurnal->kode_akun }}</td>
										@if ($jurnal->kredit > 0)
											<td style="padding-left: 30px">{{ $jurnal->akun->nama }}</td>
										@else
											<td>{{ $jurnal->akun->nama }}</td>
										@endif
										<td>{{ $jurnal->referensi }}</td>
										@if($jurnal->keterangan==NULL)
											<td class="text-center">-</td>
										@else
											<td>{{ $jurnal->keterangan }}</td>
										@endif
										<td class="text-right">{{ \App\Util::DK($jurnal->debet) }}</td>
										<td class="text-right">{{ \App\Util::DK($jurnal->kredit) }}</td>
										<td class="text-right sembunyi debet_value">{{ $jurnal->debet }}</td>
										<td class="text-right sembunyi kredit_value">{{ $jurnal->kredit }}</td>
									</tr>
								@endforeach
							@endforeach
						@endforeach
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7" class="text-right">Jumlah</td>
						<td id="debet_total" class="text-right"></td>
						<td id="kredit_total" class="text-right"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@endsection


@section('script')
  <script type="text/javascript">
  	kredit_total = 0;
    debet_total = 0;
     $(document).ready(function() {
     	var url = "{{ url('jurnal') }}";
		var a = $('a[href="' + url + '"]');
		a.parent().addClass('current-page');
		a.parent().parent().show();
		a.parent().parent().parent().addClass('active');
        $('.right_col').css('min-height', $('.left_col').css('height'));

  //       $('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
		//   var mulai = (start.toISOString()).substring(0,10);
		//   console.log(mulai);
		//   var akhir = (end.toISOString()).substring(0,10);
		//   console.log(akhir);
		//   $('#formPilihan').find('input[name="awal"]').val(mulai);
		//   $('#formPilihan').find('input[name="akhir"]').val(akhir);
		// });

        $('#rentang_tanggal').daterangepicker({
            autoApply: true,
            calender_style: "picker_2",
            showDropdowns: true,
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
            singleDatePicker: false
        }, 
        function(start, end, label) {
            var mulai = (start.toISOString()).substring(0,10);
            var akhir = (end.toISOString()).substring(0,10);
            $('#formPilihan').find('input[name="awal"]').val(mulai);
            $('#formPilihan').find('input[name="akhir"]').val(akhir);
        });

    });    

    $(window).on('load', function(event) {
    	$('.debet_value').each(function(index, el){
			var debet = $(el).text();
			debet = parseFloat(debet);
			if(isNaN(debet)) debet = 0;
			debet_total += debet;
		});

		$('.kredit_value').each(function(index, el){
			var kredit = $(el).text();
			kredit = parseFloat(kredit);
			if(isNaN(kredit)) kredit = 0;
			// console.log(kredit);
			kredit_total += kredit;
		})
		// kredit_total = kredit.toFixed(2);
		// kredit_total = kredit.toFixed(2);
		$('#debet_total').text('Rp'+debet_total.toLocaleString(['ban', 'id'], {minimumFractionDigits:2}));
		$('#kredit_total').text('Rp'+kredit_total.toLocaleString(['ban', 'id'], {minimumFractionDigits:2}));
	});
  </script>
@endsection