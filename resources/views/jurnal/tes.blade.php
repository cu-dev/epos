@extends('layouts.admin')

@section('title')
	<title>EPOS | Jurnal</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
		/*.debet {
			background: #f9f9f9;
		}
		.kredit {
			padding-left: 30px;
		}*/
		table > thead > tr > th {
			text-align: center;
		}
		table > tbody > tr > td.tengah-hv{
			vertical-align: middle;
			text-align: center;	
			background: white;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Jurnal Umum</h2>
				<a href="{{ url('jurnal/create') }}" class="btn btn-sm btn-success pull-right" id="btnUbah">
					<i class="fa fa-plus"></i> Tambah
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_title col-md-4" id="formPilihan">
				<form method="post" action="{{ url('jurnal/tampil') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label" style="padding-bottom: 15px">Pilih Range Data</label>
						<fieldset>
						<div class="control-group col-md-10">
							<div class="controls">

								<div class="input-prepend input-group">
									<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
									<input type="text" style="width: 100%" name="data" id="rentang_tanggal" class="form-control active">
									<input type="hidden" name="awal">
									<input type="hidden" name="akhir">
								</div>
							</div>
						</div>
						<div class="form-group col-lg-2 pull-right">
							<button class="btn btn-sm btn-success" id="btnUbah" type="submit">
								<span style="color:white">Tampil</span>
							</button>
						</div>
						</fieldset>
					</div>
				</form>
			</div>

			<div class="x_content">
				<table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tableJurnal">
					<thead>
						<tr>
							{{-- <th>Tahun</th>
							<th>Bulan</th>
							<th>Tanggal</th> --}}
							<th colspan="3">Tanggal</th>
							<th>Kode Akun</th>
							<th>Rekening</th>
							<th>Referensi</th>
							<th>Keterangan</th>
							<th>Debit</th>
							<th>Kredit</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($jurnals as $i => $tahunan)
							@foreach ($tahunan['isi'] as $j => $bulanan)
								@foreach ($bulanan['isi'] as $k => $harian)
									@foreach ($harian['isi'] as $l => $jurnal)
										@if ($jurnal->awal_ref)
											<tr>
												<td colspan="6" style="background: white;"></td>
											</tr>
										@endif
										<tr class="{{ $jurnal->kredit > 0 ? 'kredit' : 'debet'}}">
											@if ($jurnal->awal_tahun)
												<td  class="tengah-hv" rowspan="{{ $tahunan['row'] }}">{{ $i }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											@if ($jurnal->awal_bulan)
												<td class="tengah-hv" rowspan="{{ $bulanan['row'] }}">{{ $j }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											@if ($jurnal->awal_hari)
												<td class="tengah-hv" rowspan="{{ $harian['row'] }}">{{ $k }}</td>
											@else 
												{{-- <td style="display: none;"></td> --}}
											@endif
											<td align="center">{{ $jurnal->kode_akun }}</td>
											@if ($jurnal->kredit > 0)
												<td style="padding-left: 30px">{{ $jurnal->akun->nama }}</td>
											@else
												<td>{{ $jurnal->akun->nama }}</td>
											@endif
											<td>{{ $jurnal->referensi }}</td>
											@if($jurnal->keterangan==NULL)
												<td class="text-center">-</td>
											@else
												<td>{{ $jurnal->keterangan }}</td>
											@endif
											<td class="text-right">{{ \App\Util::DK($jurnal->debet) }}</td>
											<td class="text-right">{{ \App\Util::DK($jurnal->kredit) }}</td>
										</tr>
									@endforeach
								@endforeach
							@endforeach
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

<!-- @section('script')
	<script type="text/javascript">
		$('#tableJurnal').DataTable();

		$(document).ready(function() {
			$('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
			  var mulai = (start.toISOString()).substring(0,10);
			  console.log(mulai);
			  var akhir = (end.toISOString()).substring(0,10);
			  console.log(akhir);


			  // $('#formPilihan').find('input[name="awal"]').val(mulai);
			  // $('#formPilihan').find('input[name="akhir"]').val(akhir);
			});

			$('#reservation').daterangepicker(null, function(start, end, label) {
	          console.log(start.toISOString(), end.toISOString(), label);
	        });
		});


		// $(document).on('click', '#btnHapus', function() {
		// 	var $tr = $(this).parents('tr').first();
		// 	var id = $tr.attr('id');
		// 	var nama_item = $tr.find('td').first().next().text();
		// 	var nama_suplier = $tr.find('td').first().next().next().text();
		// 	$('input[name="id"]').val(id);

		// 	swal({
		// 		title: 'Hapus?',
		// 		text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
		// 		type: 'warning',
		// 		showCancelButton: true,
		// 		confirmButtonColor: '#009688',
		// 		cancelButtonColor: '#ff5252',
		// 		confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
		// 		cancelButtonText: '<i class="fa fa-close"></i> Batal'
		// 	}, function(isConfirm) {
		// 		if (isConfirm) {
		// 			// Confirmed
		// 			$('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-pembelian") }}' + '/' + id);
		// 			$('#formHapusContainer').find('form').submit();
		// 		} else {
		// 			// Canceled
		// 		}
		// 	});
		// });
	</script>
@endsection -->

@section('script')
  <script type="text/javascript">
     $(document).ready(function() {
        $('#reservation').daterangepicker(null, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });    

  </script>
@endsection