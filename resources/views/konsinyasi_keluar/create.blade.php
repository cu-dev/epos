@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Konsinyasi Keluar</title>
@endsection

@section('style')
	<style media="screen">
		#btnKembali {
			margin-right: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
		#metodePembayaranButtonGroup {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-6">
						<h2 id="formSimpanTitle">Konsinyasi Keluar</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					{{-- <div class="col-md-6 pull-right">
						<a href="{{ url('konsinyasi-keluar') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                			<i class="fa fa-long-arrow-left"></i>
						</a>
					</div> --}}
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Suplier</label>
						<select name="suplier_id" class="select2_single form-control">
							<option id="default">Pilih Suplier</option>
							@foreach ($supliers as $suplier)
								<option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Sales</label>
						<select name="seller_id" class="select2_single form-control">
							<option id="default">Pilih Sales</option>
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Item</label>
						<select name="item_id" class="select2_single form-control">
							<option id="default">Pilih Item</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Informasi Item</h2>
				<a href="{{ url('konsinyasi-keluar') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Stok</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Konsinyasi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<div class="row">
							<table class="table" id="tabelKeranjang">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Metode Pembayaran</label>
								<div class="input-group">
									<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<button type="button" id="btnTunai" class="btn btn-default">
												<i class="fa fa-check" style="display: none;"></i> Tunai
											</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnDebit" class="btn btn-default">
												<i class="fa fa-check" style="display: none;"></i> Debit
											</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnCek" class="btn btn-default">
												<i class="fa fa-check" style="display: none;"></i> Cek
											</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnBG" class="btn btn-default">
												<i class="fa fa-check" style="display: none;"></i> BG
											</button>
										</div>
									</div>
								</div>
							</div>
							<div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Nominal Tunai</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" id="inputNominalTunai" class="form-control angka">
								</div>
							</div>
							<div id="inputDebitContainer" class="form-group col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nomor Debit</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" id="inputNoDebit" class="form-control">
										</div>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Debit</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalDebit" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
							<div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nomor Cek</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" id="inputNoCek" class="form-control">
										</div>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Cek</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalCek" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
							<div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nomor BG</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" id="inputNoBG" class="form-control">
										</div>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal BG</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalBG" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" value="0" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka" value="0">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total + Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control angka" value="0" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Jumlah Bayar</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" value="0" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Kurang</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputUtang" id="inputUtang" class="form-control angka" value="0" disabled="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div id="formSimpanContainer">
									<form id="form-simpan" action="{{ url('konsinyasi-keluar') }}" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="kode_transaksi" value="" />
										<input type="hidden" name="suplier_id" />
										<input type="hidden" name="seller_id" />
										<input type="hidden" name="po" value="0" />

										<input type="hidden" name="harga_total" />
										<input type="hidden" name="jumlah_bayar" />
										<input type="hidden" name="utang" />

										<input type="hidden" name="ongkos_kirim" />
										
										<input type="hidden" name="nominal_tunai" />
										
										<input type="hidden" name="no_debet" />
										<input type="hidden" name="nominal_debet" />
										
										<input type="hidden" name="no_cek" />
										<input type="hidden" name="nominal_cek" />
										
										<input type="hidden" name="no_bg" />
										<input type="hidden" name="nominal_bg" />
										
										<div id="append-section"></div>
										<div class="clearfix">
											<div class="form-group pull-left">
												<button type="submit" class="form-control btn btn-success" id="submit" style="width: 100px;">
													<i class="fa fa-check"></i> OK
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		var selected_items = [];

		function updateHargaOnKeyup() {
			var $harga_total = $('#inputHargaTotal');
			var $ongkos_kirim = $('#inputOngkosKirim');
			var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
			var $jumlah_bayar = $('#inputJumlahBayar');
			var $utang = $('#inputUtang');

			var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
			var nominal_debet = $('#formSimpanContainer').find('input[name="nominal_debet"]').val();
			var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
			var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
			var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();

			nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
			nominal_debet = parseInt(nominal_debet.replace(/\D/g, ''), 10);
			nominal_cek = parseInt(nominal_cek.replace(/\D/g, ''), 10);
			nominal_bg = parseInt(nominal_bg.replace(/\D/g, ''), 10);
			ongkos_kirim = parseInt(ongkos_kirim.replace(/\D/g, ''), 10);

			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			if (isNaN(nominal_debet)) nominal_debet = 0;
			if (isNaN(nominal_cek)) nominal_cek = 0;
			if (isNaN(nominal_bg)) nominal_bg = 0;
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			
			var harga_total_plus_ongkos_kirim = harga_total;
			if (!isNaN(parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10)))
				harga_total_plus_ongkos_kirim += parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10);
			var jumlah_bayar = nominal_tunai + nominal_debet + nominal_cek + nominal_bg;
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;

			if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
			if (isNaN(harga_total)) harga_total = 0;
			if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			if (isNaN(utang)) utang = 0;
			if (utang < 0) utang = 0;

			// if (ongkos_kirim == 0) $ongkos_kirim.val('');
			// else $ongkos_kirim.val(ongkos_kirim.toLocaleString());
			$ongkos_kirim.val(ongkos_kirim.toLocaleString());
			$harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString());
			$harga_total.val(harga_total.toLocaleString());
			$jumlah_bayar.val(jumlah_bayar.toLocaleString());
			$utang.val(utang.toLocaleString());

			$('input[name="ongkos_kirim"]').val(ongkos_kirim);
			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
			$('input[name="utang"]').val(utang);
		}

		$(document).ready(function() {
			var url = "{{ url('konsinyasi-keluar') }}";
			// var url = "{{ url('konsinyasi-keluar') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$(".select2_single").select2();

			$('#inputTunaiContainer').hide();
			$('#inputDebitContainer').hide();
			$('#inputDebitContainer').find('input').val('');
			$('#inputCekContainer').hide();
			$('#inputCekContainer').find('input').val('');
			$('#inputBGContainer').hide();
			$('#inputBGContainer').find('input').val('');
		});

		// Buat ambil nomer transaksi terakhir
		$(window).on('load', function(event) {
			var url = "{{ url('konsinyasi-keluar/last.json') }}";
			
			$.get(url, function(data) {
				var kode = 1;
				if (data.konsinyasi_keluar !== null) {
					var kode_transaksi = data.konsinyasi_keluar.kode_transaksi;
					kode = parseInt(kode_transaksi.split('/')[0]);
					kode++;
				}

				kode = int4digit(kode);
				var tanggal = printTanggalSekarang('dd/mm/yyyy');
				kode_transaksi = kode + '/KNK/' + tanggal;

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-danger');
				$(this).find('i').show('fast');
				$('#inputTunaiContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-danger')) {
				$(this).removeClass('btn-danger');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputTunaiContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnDebit', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-warning');
				$(this).find('i').show('fast');
				$('#inputDebitContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-warning')) {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputDebitContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_debet"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_debet"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnCek', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
				$(this).find('i').show('fast');
				$('#inputCekContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-success')) {
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputCekContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnBG', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-BG');
				$(this).find('i').show('fast');
				$('#inputBGContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-BG')) {
				$(this).removeClass('btn-BG');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputBGContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_bg"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('keyup', '#inputNominalTunai', function(event) {
			event.preventDefault();
			var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			$('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoDebit', function(event) {
			event.preventDefault();
			var no_debet = $(this).val();
			$('#formSimpanContainer').find('input[name="no_debet"]').val(no_debet);
		});

		$(document).on('keyup', '#inputNominalDebit', function(event) {
			event.preventDefault();
			var nominal_debet = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_debet)) nominal_debet = 0;
			$('#formSimpanContainer').find('input[name="nominal_debet"]').val(nominal_debet);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoCek', function(event) {
			event.preventDefault();
			var no_cek = $(this).val();
			$('#formSimpanContainer').find('input[name="no_cek"]').val(no_cek);
		});

		$(document).on('keyup', '#inputNominalCek', function(event) {
			event.preventDefault();
			var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_cek)) nominal_cek = 0;
			$('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoBG', function(event) {
			event.preventDefault();
			var no_bg = $(this).val();
			$('#formSimpanContainer').find('input[name="no_bg"]').val(no_bg);
		});

		$(document).on('keyup', '#inputNominalBG', function(event) {
			event.preventDefault();
			var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_bg)) nominal_bg = 0;
			$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputOngkosKirim', function(event) {
			event.preventDefault();
			var ongkos_kirim = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
			$('#formSimpanContainer').find('input[name="ongkos_kirim"]').val(ongkos_kirim);
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="suplier_id"]', function(event) {
			event.preventDefault();
			
			var id = $(this).val();
			var url_items = "{{ url('konsinyasi-keluar') }}" + '/' + id + '/items.json';
			var url_sellers = "{{ url('konsinyasi-keluar') }}" + '/' + id + '/sellers.json';

			$('input[name="suplier_id"]').val(id);
			$('#tabelKeranjang > tbody').empty();
			$('#append-section').empty();
			$('#formSimpanContainer').find('input[name="harga_total"]').val('');
			$('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
			$('#formSimpanContainer').find('input[name="utang"]').val('');

			var $select_item = $('select[name="item_id"]');
			$select_item.empty();
			$select_item.append($('<option value="">Pilih Item</option>'));

			$.get(url_items, function(data) {
				var items = data.items;
				for (var i = 0; i < items.length; i++) {
					var item = items[i];
					$select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
				}
			});

			var $select_sellers = $('select[name="seller_id"]');
			$select_sellers.empty();
			$select_sellers.append($('<option value="">Pilih Sales</option>'));

			$('#spinner').show();
			$.get(url_sellers, function(data) {
				var sellers = data.sellers;
				for (var i = 0; i < sellers.length; i++) {
					var seller = sellers[i];
					$select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
				}
				$('#spinner').hide();
			});
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();
			
			var id = $('select[name="item_id"]').val();
			var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
			var url = "{{ url('konsinyasi-keluar') }}" + '/items/' + id + '/json';

			if (!selected_items.includes(parseInt(id))) {
				if (!isNaN(id)) {
					selected_items.push(parseInt(id));
				}

				$('#spinner').show();
				$.get(url, function(data) {
					var item = data.item;
					var kode = item.kode;
					var nama = item.nama;
					var satuans = item.satuans;

					var jumlah_sisa = parseInt(data.stok);
					var pcs = data.pcs;
					
					var relasi = data.relasi;
					var harga_beli = parseInt(relasi.harga);
					var jumlah_beli = parseInt(relasi.jumlah);
					var jumlah_jual = jumlah_beli - jumlah_sisa;
					var jumlah_bayar = harga_beli * jumlah_jual;

					var stoks = '';
					var temp = jumlah_sisa;
					if (satuans.length > 0) {
						for (var i = 0; i < satuans.length; i++) {
							var satuan = satuans[i];
							var hasil = parseInt(temp / satuan.konversi);
							if (hasil > 0) {
								temp %= satuan.konversi;
								stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
							}
						}
					} else {
						stoks += '<span class="label label-info">'+jumlah_sisa+' '+pcs.kode+'</span>';
					}

					$('#tabelInfo').find('tbody').empty();
					$('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

					if (tr === undefined) {
						var temp = jumlah_sisa;
						var div_jumlah_sisa = '<div class="form-group">'+
										'<label class="control-label">Jumlah Sisa</label>';
						if (satuans.length > 0) {
							for (var i = 0; i < satuans.length; i++) {
								var satuan = satuans[i];
								var hasil = parseInt(temp / satuan.konversi);
								// if (hasil > 0) {
									temp %= satuan.konversi;
									div_jumlah_sisa += '<div class="input-group">' +
											'<input type="text" id="inputJumlahSisa" name="inputJumlahSisa" class="form-control input-sm angka" konversi="'+satuan.konversi+'" value="'+hasil+'">' +
											'<div class="input-group-addon">'+satuan.satuan.kode+'</div>' +
										'</div>';
								// }
							}
							div_jumlah_sisa += '</div>';
						} else {
							div_jumlah_sisa += '<div class="input-group">' +
										'<input type="text" id="inputJumlahSisa" name="inputJumlahSisa" class="form-control input-sm angka" konversi="'+1+'" value="'+jumlah_sisa+'">' +
										'<div class="input-group-addon">'+pcs.kode+'</div>' +
									'</div>' +
								'</div>';
						}

						var tr = '<tr data-id="' + id + '">'+
									'<input type="hidden" name="jumlah_beli" value="'+jumlah_beli+'">'+
									'<input type="hidden" name="jumlah_sisa" value="'+jumlah_sisa+'">'+
									'<input type="hidden" name="jumlah_jual" value="'+jumlah_jual+'">'+
									'<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
									'<td width="40%"><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3></td>'+
									'<td>'+
										div_jumlah_sisa+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">Sub Total</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" value="'+jumlah_bayar.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">Harga</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" value="'+harga_beli.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">HPP</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" value="'+harga_beli.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">PPN</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" value="0" disabled="">'+
											'</div>'+
										'</div>'+
									'</td>'+
								'</tr>';

						$('#tabelKeranjang').find('tbody').append($(tr));
				
						$('#form-simpan').find('input[id="item-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_beli-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_sisa-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_jual-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="harga-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="ppn-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="subtotal-' + item.id + '"]').remove();
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + item.id + '" value="' + item.id + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_beli[]" id="jumlah_beli-' + item.id + '" value="' + jumlah_beli + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_sisa[]" id="jumlah_sisa-' + item.id + '" value="' + jumlah_sisa + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_jual[]" id="jumlah_jual-' + item.id + '" value="' + jumlah_jual + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + item.id + '" value="' + harga_beli + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + item.id + '" value="' + 0 + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + item.id + '" value="' + jumlah_bayar + '" />');

						var $harga_total = $('#inputHargaTotal');
						var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');

						var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);
						harga_total += jumlah_bayar;

						$harga_total.val(harga_total.toLocaleString());
						$harga_total_plus_ongkos_kirim.val(harga_total.toLocaleString());
					}

					$('#spinner').hide();
				});
			}
		});

		$(document).on('change', 'select[name="seller_id"]', function(event) {
			event.preventDefault();
			var id = $(this).val();
			$('input[name="seller_id"]').val(id);
		});

		$(document).on('keyup', 'input[name="inputJumlahSisa"]', function(event) {
			event.preventDefault();

			var konversi = $(this).attr('konversi');

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $jumlah_beli = $tr.find('input[name="jumlah_beli"]');
			var $jumlah_sisa = $tr.find('input[name="jumlah_sisa"]');
			var $jumlah_jual = $tr.find('input[name="jumlah_jual"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $subtotal = $tr.find('input[name="inputSubTotal"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var jumlah_beli = parseInt($jumlah_beli.val().replace(/\D/g, ''), 10);
			var harga = parseInt($harga.val().replace(/\D/g, ''), 10);
			var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
			var hpp = parseInt($hpp.val().replace(/\D/g, ''), 10);
			var ppn = parseInt($ppn.val().replace(/\D/g, ''), 10);

			var jumlah_sisa = 0;
			$(this).parents('tr').first().find('input[name="inputJumlahSisa"]').each(function(index, el) {
				var value = parseInt($(el).val().replace(/\D/g, ''), 10);
				var konversi = parseInt($(el).attr('konversi'));
				value *= konversi;
				if (isNaN(value)) value = 0;
				jumlah_sisa += value;
			});
			var jumlah_jual = jumlah_beli - jumlah_sisa;
			// if (jumlah_jual > jumlah_beli) {
			// 	jumlah_jual = jumlah_beli;
			// }

			if (isNaN(jumlah_beli)) jumlah_beli = 0;
			if (isNaN(jumlah_sisa)) jumlah_sisa = 0;
			if (isNaN(jumlah_jual)) jumlah_jual = 0;
			if (isNaN(harga)) harga = 0;
			if (isNaN(subtotal)) subtotal = 0;
			subtotal = harga * jumlah_jual;
			if (isNaN(hpp)) hpp = 0;
			if (isNaN(ppn)) ppn = 0;

			$jumlah_beli.val(jumlah_beli);
			$jumlah_sisa.val(jumlah_sisa);
			$jumlah_jual.val(jumlah_jual);
			$subtotal.val(subtotal.toLocaleString());
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_beli-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_sisa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_jual-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_beli[]" id="jumlah_beli-' + id + '" value="' + jumlah_beli + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_sisa[]" id="jumlah_sisa-' + id + '" value="' + jumlah_sisa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_jual[]" id="jumlah_jual-' + id + '" value="' + jumlah_jual + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + hpp + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + id + '" value="' + ppn + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString());
			updateHargaOnKeyup();
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();

			var id = $(this).parents('tr').data('id');

			var index = selected_items.indexOf(parseInt(id));
			if (index >= -1) {
				selected_items.splice(index, 1);
			}

			$('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
			$('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();

			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_beli-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_sisa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_jual-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			$('input[name="inputHargaTotal"]').val(harga_total.toLocaleString());

			var ongkos_kirim = parseInt($('input[name="inputOngkosKirim"]').val().replace(/\D/g, ''), 10);
			var harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
			$('input[name="inputHargaTotalPlusOngkosKirim"]').val(harga_total_plus_ongkos_kirim.toLocaleString());

			var jumlah_bayar = parseInt($('input[name="inputJumlahBayar"]').val().replace(/\D/g, ''), 10);
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
			if (utang <= 0 || isNaN(utang)) utang = 0;
			$('input[name="inputUtang"]').val(utang.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
		});

	</script>
@endsection
