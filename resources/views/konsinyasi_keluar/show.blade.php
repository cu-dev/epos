@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Konsinyasi Keluar</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnCairkan {
            margin-right: 0;
        }
        #btnCairkan, #btnRetur, #btnKembali, #btnCetakNota, #btnBayarPiutang {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
            <input type="hidden" name="show" value="1">
            <input type="hidden" name="ids" value="{{ $konsinyasi_keluar->id }}">
        </form>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
				<h2>Detail Konsinyasi Keluar</h2>
				<a href="{{ url('konsinyasi-keluar') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
	                <i class="fa fa-long-arrow-left"></i>
				</a>
				@if($konsinyasi_keluar->aktif_bg == 1)
					<a id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right">
	                    <!-- <i class="fa fa-money"></i> -->BG
	                </a>
				@endif
				@if($konsinyasi_keluar->aktif_cek == 1)
					<a id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right">
	                    <!-- <i class="fa fa-money"></i> -->CEK
	                </a>
				@endif
				<div class="clearfix"></div>
			</div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $konsinyasi_keluar->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $konsinyasi_keluar->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Kode Faktur Pembayaran</th>
                                    <td style="width: 60%;">{{ $konsinyasi_keluar->nota }}</td>
                                </tr>
                                <tr>
                                    <th>Pemasok</th>
                                    <td style="width: 60%; text-transform: capitalize;">{{ $konsinyasi_keluar->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $konsinyasi_keluar->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        @if ($konsinyasi_keluar->harga_total != null && $konsinyasi_keluar->harga_total > 0)
	                        <table class="table table-bordered table-striped table-hover">
	                            <thead>
	                                <th class="text-left" colspan="2">Rincian</th>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <th>Sub Total</th>
	                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->harga_total) }}</td>
	                                </tr>
	                                @if ($konsinyasi_keluar->jumlah_bayar != null && $konsinyasi_keluar->jumlah_bayar > 0)
		                                <tr>
		                                    <th>Jumlah Bayar</th>
		                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->jumlah_bayar - $konsinyasi_keluar->nominal_titipan) }}</td>
		                                </tr>
		                            @endif
	                            </tbody>
	                        </table>
	                    @endif

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($konsinyasi_keluar->nominal_tunai != null && $konsinyasi_keluar->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->bank_transfers->nama_bank }} [{{ $konsinyasi_keluar->bank_transfers->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->nominal_transfer != null && $konsinyasi_keluar->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    @if ($konsinyasi_keluar->jenis_kartu == 'debet')
                                        <td class="text-right" style="width: 60%;">Debit</td>
                                    @else
                                        <td class="text-right" style="width: 60%;">Kredit</td>
                                    @endif
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->bank_kartus->nama_bank }} [{{ $konsinyasi_keluar->bank_kartus->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->nominal_kartu != null && $konsinyasi_keluar->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->bank_ceks->nama_bank }} [{{ $konsinyasi_keluar->bank_ceks->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->nominal_cek != null && $konsinyasi_keluar->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_keluar->bank_bgs->nama_bank }} [{{ $konsinyasi_keluar->bank_bgs->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_keluar->nominal_bg != null && $konsinyasi_keluar->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_keluar->nominal_bg) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_konsinyasi_keluar as $i => $relasi)
	                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item_kode }}">
	                                    <td>{{ ++$i }}</td>
	                                    <td>{{ $relasi->item->nama }}</td>
		                                <td>
											@if($relasi->jumlah > 0)
												@foreach ($relasi->jumlahs as $j => $jumlah)
													{{ $jumlah['jumlah'] }} {{ $jumlah['satuan']->kode }}
												@endforeach
											@else
												-
											@endif
										</td>
										<td style="text-align: right;">{{ \App\Util::duit0($relasi->harga) }}</td>
										<td style="text-align: right;">{{ \App\Util::duit0($relasi->subtotal)}}</td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
	@if (session('sukses') == 'cek')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran Cek berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('sukses') == 'bg')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran BG berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@endif

	<script type="text/javascript">
        $('#tabel-item').DataTable();
		$(document).ready(function() {
			var url = "{{ url('konsinyasi-keluar') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});

		$(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            var id = '{{ $konsinyasi_keluar->id }}';
            var url = '{{ url('konsinyasi-keluar/cek') }}' + '/' + id;

            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            var id = '{{ $konsinyasi_keluar->id }}';
            var url = '{{ url('konsinyasi-keluar/bg') }}' + '/' + id;

            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    // console.log('transaksi-pembelian/' + trapem_id + '/cairkan/bg');
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
	</script>
@endsection
