@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Konsinyasi Keluar</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnTambah {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Konsinyasi Keluar</h2>
                {{-- <a href="{{ url('konsinyasi-keluar/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah">
                    <i class="fa fa-plus"></i> Tambah
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Transaksi</th>
                            <th>Pemasok</th>
                            <th>Jumlah Bayar</th>
                            <th style="width: 100px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($konsinyasi_keluars as $i => $konsinyasi_keluar)
                        <tr id="{{ $konsinyasi_keluar->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $konsinyasi_keluar->created_at->format('d-m-Y H:i:s') }}</td>
                            <td>{{ $konsinyasi_keluar->kode_transaksi }}</td>
                            <td>{{ $konsinyasi_keluar->suplier->nama }}</td>
                            <td>{{ \App\Util::duit($konsinyasi_keluar->jumlah_bayar) }}</td>
                            <td>
                                <a href="{{ url('konsinyasi-keluar/'.$konsinyasi_keluar->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pembelian">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($konsinyasi_keluar->aktif_cek == 1)
                                    <a class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                        CEK
                                    </a>
                                @endif
                                @if($konsinyasi_keluar->aktif_bg == 1)
                                    <a class="btn btn-xs btn-BG" id="btnDetail" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                        BG
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Konsinyasi Keluar berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Konsinyasi Keluar gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Konsinyasi Keluar berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Konsinyasi Keluar gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Konsinyasi Keluar berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Konsinyasi Keluar gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == '404')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Halaman tidak ditemukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableItemMasuk').DataTable({
            // 'order': [[1, 'desc'], [2, 'desc']]
        });

        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            var nama_suplier = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("konsinyasi-keluar") }}' + '/' + id);
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });
    </script>
@endsection
