@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Konsinyasi Masuk</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnCairkan {
            margin-right: 0;
        }
        #btnCairkan, #btnRetur, #btnKembali, #btnCetakNota, #btnBayarPiutang {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div id="formHapusContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="delete">
            <input type="hidden" name="show" value="1">
            <input type="hidden" name="ids" value="{{ $konsinyasi_masuk->id }}">
        </form>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="padding-right: 0;">
				<h2>Detail Konsinyasi Masuk</h2>
				@if ($konsinyasi_masuk->status_hutang == 1)
				<a href="{{ url('konsinyasi-masuk/'.$konsinyasi_masuk->id.'/keluar') }}" class="btn btn-sm btn-primary pull-right" id="btnKeluar" data-toggle="tooltip" data-placement="top" title="Keluar">
					<i class="fa fa-sign-out"></i>
				</a>
				@endif
				<a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
					<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $konsinyasi_masuk->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $konsinyasi_masuk->kode_transaksi }}</td>
                                </tr>
                                <tr>
									<th>Kode Faktur</th>
									<td>{{ $konsinyasi_masuk->nota }}</td>
								</tr>
                                <tr>
									<th>Pemasok</th>
									<td>{{ $konsinyasi_masuk->suplier->nama }}</td>
								</tr>
								<tr>
									<th>Penjual</th>
									<td>
										@if($konsinyasi_masuk->seller_id !==NULL)
											{{ $konsinyasi_masuk->seller->nama }}
										@endif
									</td>
								</tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $konsinyasi_masuk->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Rincian</th>
                            </thead>
                            <tbody>
                                @if ($konsinyasi_masuk->harga_total != null && $konsinyasi_masuk->harga_total > 0)
	                                <tr>
	                                    <th>Sub Total</th>
	                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->harga_total) }}</td>
	                                </tr>
                                @endif
                            </tbody>
                        </table>

                        {{-- <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($konsinyasi_masuk->nominal_tunai != null && $konsinyasi_masuk->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->banktransfer->nama_bank }} [{{ $konsinyasi_masuk->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->nominal_transfer != null && $konsinyasi_masuk->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    @if ($konsinyasi_masuk->jenis_kartu == 'debet')
                                        <td class="text-right" style="width: 60%;">Debit</td>
                                    @else
                                        <td class="text-right" style="width: 60%;">Kredit</td>
                                    @endif
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->bankkartu->nama_bank }} [{{ $konsinyasi_masuk->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->nominal_kartu != null && $konsinyasi_masuk->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->bankcek->nama_bank }} [{{ $konsinyasi_masuk->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->nominal_cek != null && $konsinyasi_masuk->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $konsinyasi_masuk->bankbg->nama_bank }} [{{ $konsinyasi_masuk->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($konsinyasi_masuk->nominal_bg != null && $konsinyasi_masuk->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->nominal_bg) }}</td>
                                </tr>
                                @endif
                                @if ($konsinyasi_masuk->jumlah_bayar != null && $konsinyasi_masuk->jumlah_bayar > 0)
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($konsinyasi_masuk->jumlah_bayar - $konsinyasi_masuk->nominal_titipan) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table> --}}

                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_konsinyasi_masuk as $i => $relasi)
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item_kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
	                                <td>
	                                    @foreach ($relasi->jumlah as $j => $jumlah)
											{{ $jumlah['jumlah'] }} {{ $jumlah['satuan']->kode }}
										@endforeach
									</td>
									</td>
									<td style="text-align: right;">{{ \App\Util::duit0($relasi->harga) }}</td>
									<td style="text-align: right;">{{ \App\Util::duit0($relasi->subtotal) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
	<script type="text/javascript">
        $('#tabel-item').DataTable();

		$(document).ready(function() {
			var url = "{{ url('konsinyasi-masuk') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			// var harga_total = parseInt($('#harga_total').text().replace(/\D/g, ''), 10);
			// var subtotal = parseInt($('#subtotal').text().replace(/\D/g, ''), 10);
			// $('#harga_total').text(harga_total.toLocaleString());
			// $('#subtotal').text(subtotal.toLocaleString());
		});
	</script>
@endsection

