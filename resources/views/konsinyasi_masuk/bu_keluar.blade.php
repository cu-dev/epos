@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Konsinyasi Keluar</title>
@endsection

@section('style')
	<style media="screen">
		#btnKembali {
			margin-right: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
		#metodePembayaranButtonGroup {
			width: 100%;
		}
		.line {
			height: 10px;
			width: 100%;
			border-top: 2px solid #E6E9ED;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-6">
						<h2 id="formSimpanTitle">Konsinyasi Keluar</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					<div class="col-md-6 pull-right">
						<a href="{{ url('konsinyasi-masuk/') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                			<i class="fa fa-long-arrow-left"></i>
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Suplier</label>
						<select name="suplier_id" id="pilihSuplier" class="select2_single form-control">
							<option id="default">Pilih Suplier</option>
							@foreach ($supliers as $suplier)
								<option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Sales</label>
						<select name="seller_id" id="pilihSales" class="select2_single form-control">
							<option id="default">Pilih Sales</option>
							@foreach ($sellers as $seller)
								<option value="{{ $seller->id }}">{{ $seller->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Item</label>
						<select name="item_id" id="pilihItem" class="select2_single form-control">
							<option id="default">Pilih Item</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Jumlah Pembelian Barang</h2>
				<a href="{{ url('konsinyasi-masuk/') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Stok</th>
						</tr>
						<tr>
							@foreach($relasi_konsinyasi_masuk as $a => $item)
							<tr>
								<td>{{ $item->item->nama }}</td>
								<td>
									{{-- {{ $item->jumlah_beli }} --}}
									@foreach($hasil[$a] as $x => $jumlah)
                                        {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                        @if ($x != count($hasil[$a]) - 1)
                                        <br>
                                        @endif
                                    @endforeach
								</td>
							</tr>
							@endforeach
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Konsinyasi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<div class="row">
							<table class="table" id="tabelKeranjang">
								<tbody>
									{{-- @foreach ($relasi_konsinyasi_masuk as $i => $relasi)
										<tr data-id="{{ $relasi->transaksi_pembelian->id }}">
											<input type="hidden" name="jumlah_beli" value="{{ $relasi->jumlah_beli }}">
											<input type="hidden" name="jumlah_sisa" value="{{ $relasi->jumlah_sisa }}">
											<input type="hidden" name="jumlah_jual" value="{{ $relasi->jumlah_jual }}">
											<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>
											<td width="40%"><h3>{{ $relasi->item->nama }}</h3></td>
											<td>
												<div class="form-group">
													<label class="control-label">Jumlah Sisa</label>
													@foreach ($relasi->jumlah as $j => $jumlah)
														<div class="input-group">
															<input type="text" id="inputJumlahSisa" name="inputJumlahSisa" class="form-control input-sm angka" konversi="{{ $jumlah['konversi'] }}" value="{{ $jumlah['jumlah'] }}">
															<div class="input-group-addon">{{ $jumlah['satuan']->kode }}</div>
														</div>
													@endforeach
												</div>
											</td>
											<td>
												<div class="form-group">
													<label class="control-label">Sub Total</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" value="0">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label">Harga</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" value="{{ \App\Util::duit($relasi->harga) }}" disabled="">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label">HPP</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" value="{{ \App\Util::duit($relasi->harga) }}" disabled="">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label">PPN</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" value="{{ \App\Util::duit(0) }}" disabled="">
													</div>
												</div>
											</td>
										</tr>
									@endforeach --}}
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-8 col-xs-8">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Metode Pembayaran</label>
								<div class="input-group">
									<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                        </div>
                                       {{--  <div class="btn-group" role="group">
                                            <button type="button" id="btnPiutang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Piutang</button>
                                        </div> --}}
                                    </div>
								</div>
							</div>
							<div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Nominal Tunai</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" id="inputNominalTunai" class="form-control angka" style="height: 38px;">
								</div>
							</div>
							<div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_transfer">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNoTraKartu" id="inputNoTraKartu" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Dibayarkan</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Cek</label>
                                        <select class="form-control select2_single" name="cek_id">
                                            <option value="">Pilih Cek</option>
                                            <option value="cek_baru">Buat Cek Baru</option>
                                            @foreach ($ceks as $cek)
                                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [{{ \App\Util::ewon($cek->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row" id="cek_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_cek">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih BG</label>
                                        <select class="form-control select2_single" name="bg_id">
                                            <option value="">Pilih BG</option>
                                            <option value="bg_baru">Buat BG Baru</option>
                                            @foreach ($bgs as $bg)
                                            <option value="{{ $bg->id }}">{{ $bg->nomor }} [{{ \App\Util::ewon($bg->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row" id="bg_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_bg">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control" />
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
						</div>
					</div>
					<div class="col-sm-4 col-xs-4">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" value="0" disabled="" style="height: 38px;">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka" value="0" disabled="" style="height: 38px;">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total + Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control angka" value="0" disabled="" style="height: 38px;">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Jumlah Bayar</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" value="0" disabled="" style="height: 38px;">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Kurang</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputUtang" id="inputUtang" class="form-control angka" value="0" disabled="" style="height: 38px;">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div id="formSimpanContainer">
									<form id="form-simpan" action="{{ url('konsinyasi-keluar') }}" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="kode_transaksi" value="" />
										<input type="hidden" name="kode_konsinyasi" value="{{ $konsinyasi_masuk->kode_transaksi }}" />
										<input type="hidden" name="suplier_id" />
										<input type="hidden" name="seller_id" />
										<input type="hidden" name="po" value="0" />
										<input type="hidden" name="transaksi_pembelian_id" value="{{ $konsinyasi_masuk->id }}" />

										<input type="hidden" name="harga_total" />
										<input type="hidden" name="jumlah_bayar" />
										<input type="hidden" name="utang" />

										<input type="hidden" name="ongkos_kirim" />
										
										<input type="hidden" name="nominal_tunai" />
										
										<input type="hidden" name="bank_transfer" />
                                        <input type="hidden" name="no_tansfer" />
                                        <input type="hidden" name="nominal_transfer" />
                                        
                                        <input type="hidden" name="cek_id" />
                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="bank_cek" />
                                        <input type="hidden" name="nominal_cek" />
                                        
                                        <input type="hidden" name="bg_id" />
                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="bank_bg" />
                                        <input type="hidden" name="nominal_bg" />
                                        
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="NoTraKartu" />
										
										<div id="append-section"></div>
										<div class="clearfix">
											<div class="form-group pull-left">
												<button type="submit" class="l btn btn-success" id="submit" style="width: 100px; height: 38px;" disabled="">
													<i class="fa fa-check"></i> OK
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		showSpinner = true;
		var selected_items = [];
		var konsinyasi_masuk = null;
		var relasi_konsinyasi_masuk = null;

		function isSubmitButtonDisabled() {
			var harga_total = parseFloat($('input[name="harga_total"]').val());
			var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
			var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
			var id_bank = $('input[name="id_bank"]').val();
			var no_transfer = $('input[name="no_transfer"]').val();
			var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
			var no_cek = $('input[name="no_cek"]').val();
			var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
			var no_bg = $('input[name="no_bg"]').val();
			var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());

			// console.log(harga_total, jumlah_bayar);
			if (isNaN(harga_total) || harga_total < 0) return true;

			if (isNaN(jumlah_bayar) || jumlah_bayar < 0) return true;

			// console.log('btnTunai');
			if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai < 0) return true;

			// console.log('btnTransferBank', id_bank, no_transfer, nominal_transfer);
			if ($('#btnTransferBank').hasClass('btn-warning') && (id_bank == '' || no_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

			// console.log('btnCek');
			if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

			// console.log('btnBG');
			if ($('#btnBG').hasClass('btn-primary') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

			// console.log('==');
			if (jumlah_bayar != harga_total) return true;

			return false;
		}

		function updateHargaOnKeyup() {
			// console.log('I\'m updated.');
			var $harga_total = $('#inputHargaTotal');
			var $ongkos_kirim = $('#inputOngkosKirim');
			var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
			var $jumlah_bayar = $('#inputJumlahBayar');
			var $utang = $('#inputUtang');

			var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
			var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();
			var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
			var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
			var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();

			nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
			nominal_transfer = parseFloat(nominal_transfer.replace(/\D/g, ''), 10);
			nominal_cek = parseFloat(nominal_cek.replace(/\D/g, ''), 10);
			nominal_bg = parseFloat(nominal_bg.replace(/\D/g, ''), 10);
			ongkos_kirim = parseFloat(ongkos_kirim.replace(/\D/g, ''), 10);

			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			if (isNaN(nominal_transfer)) nominal_transfer = 0;
			if (isNaN(nominal_cek)) nominal_cek = 0;
			if (isNaN(nominal_bg)) nominal_bg = 0;
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseFloat($(el).val().replace(/\D/g, ''), 10) / 100;
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			
			var harga_total_plus_ongkos_kirim = harga_total;
			if (!isNaN(parseFloat($ongkos_kirim.val().replace(/\D/g, ''), 10)))
				harga_total_plus_ongkos_kirim += parseFloat($ongkos_kirim.val().replace(/\D/g, ''), 10);
			var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg;
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;

			if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
			if (isNaN(harga_total)) harga_total = 0;
			if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			// if (isNaN(utang)) utang = 0;
			if (utang < 0) utang = 0;

			// if (ongkos_kirim == 0) $ongkos_kirim.val('');
			// else $ongkos_kirim.val(ongkos_kirim.toLocaleString());
			$ongkos_kirim.val(ongkos_kirim.toLocaleString());
			$harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString());
			$harga_total.val(harga_total.toLocaleString());
			$jumlah_bayar.val(jumlah_bayar.toLocaleString());
			$utang.val(utang.toLocaleString());

			$('input[name="ongkos_kirim"]').val(ongkos_kirim);
			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
			$('input[name="utang"]').val(utang);

			// console.log('Till here', isSubmitButtonDisabled());
			$('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
		}

		function handleSuplierChange(suplier_id, custom_seller, custom_item) {
			var url_items = "{{ url('konsinyasi-keluar') }}" + '/' + suplier_id + '/items.json';
			var url_sellers = "{{ url('konsinyasi-keluar') }}" + '/' + suplier_id + '/sellers.json';

			$('input[name="suplier_id"]').val(suplier_id);
			$('#tabelKeranjang > tbody').empty();
			$('#append-section').empty();
			$('#formSimpanContainer').find('input[name="harga_total"]').val('');
			$('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
			$('#formSimpanContainer').find('input[name="utang"]').val('');

			var $select_sellers = $('select[name="seller_id"]');
			$select_sellers.empty();
			$select_sellers.append($('<option value="">Pilih Sales</option>'));

			$.get(url_sellers, function(data) {
				var sellers = data.sellers;
				for (var i = 0; i < sellers.length; i++) {
					var seller = sellers[i];
					$select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
				}

				if (custom_seller) {
					$("#pilihSales").val(konsinyasi_masuk.seller.id).trigger('change');
				}

				setTimeout(function() {
					$('#spinner').hide();
					$('#inputNominalTunai').trigger('keyup');
				}, 1500);
			});

			var $select_item = $('select[name="item_id"]');
			$select_item.empty();
			$select_item.append($('<option value="">Pilih Item</option>'));

			$.get(url_items, function(data) {
				var items = data.items;
				for (var i = 0; i < items.length; i++) {
					var item = items[i];
					$select_item.append($('<option value="' + item.id + '">' + item.nama + '</option>'));
				}

				if (custom_item) {
					for (var i = 0; i < relasi_konsinyasi_masuk.length; i++) {
						var relasi = relasi_konsinyasi_masuk[i];
						$("#pilihItem").val(relasi.item.id).trigger('change');
					}
				}

				setTimeout(function() {
					$('#spinner').hide();
					$('#inputNominalTunai').trigger('keyup');
				}, 1500);
			});
		}

		function handleItemChange(id) {
			var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
			var pembelian_id = {{ $konsinyasi_masuk->id }};
			var url = "{{ url('konsinyasi-keluar') }}" + '/items/' + id + '/json/'+pembelian_id;

			if (!selected_items.includes(parseInt(id))) {
				if (!isNaN(id)) {
					selected_items.push(parseInt(id));
				}

				$.get(url, function(data) {
					var item = data.item;
					var nama = item.nama;
					var satuans = item.satuans;

					var jumlah_sisa = parseInt(data.stok);
					var pcs = data.pcs;
					
					var relasi = data.relasi;
					var harga_beli = parseFloat(relasi.harga);
					var jumlah_beli = parseInt(relasi.jumlah);
					var jumlah_jual = jumlah_beli - jumlah_sisa;
					var jumlah_bayar = parseFloat(Math.round(harga_beli * jumlah_jual));

					var stoks = '';
					var temp = jumlah_sisa;
					if (satuans.length > 0) {
						for (var i = 0; i < satuans.length; i++) {
							var satuan = satuans[i];
							var hasil = parseInt(temp / satuan.konversi);
							if (hasil > 0) {
								temp %= satuan.konversi;
								stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
							}
						}
					} else {
						stoks += '<span class="label label-info">'+jumlah_sisa+' '+pcs.kode+'</span>';
					}

					// $('#tabelInfo').find('tbody').empty();
					// $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

					if (tr === undefined) {
						var temp = jumlah_sisa;
						var div_jumlah_sisa = '<div class="form-group">'+
										'<label class="control-label">Jumlah Sisa</label>';
						if (satuans.length > 0) {
							for (var i = (satuans.length)-1; i >= 0; i--) {
								var satuan = satuans[i];
								var hasil = parseInt(temp / satuan.konversi);
								// if (hasil > 0) {
									temp %= satuan.konversi;
									div_jumlah_sisa += '<div class="input-group">' +
											'<input type="text" id="inputJumlahSisa" name="inputJumlahSisa" class="form-control input-sm angka" konversi="'+satuan.konversi+'" value="'+hasil+'">' +
											'<div class="input-group-addon">'+satuan.satuan.kode+'</div>' +
										'</div>';
								// }
							}
							div_jumlah_sisa += '</div>';
						} else {
							div_jumlah_sisa += '<div class="input-group">' +
										'<input type="text" id="inputJumlahSisa" name="inputJumlahSisa" class="form-control input-sm angka" konversi="'+1+'" value="'+jumlah_sisa+'">' +
										'<div class="input-group-addon">'+pcs.kode+'</div>' +
									'</div>' +
								'</div>';
						}

						var tr = '<tr data-id="' + id + '">'+
									'<input type="hidden" name="jumlah_beli" value="'+jumlah_beli+'">'+
									'<input type="hidden" name="jumlah_sisa" value="'+jumlah_sisa+'">'+
									'<input type="hidden" name="jumlah_jual" value="'+jumlah_jual+'">'+
									'<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
									'<td width="40%"><h3>'+nama+'</h3></td>'+
									'<td>'+
										div_jumlah_sisa+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">Sub Total</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" value="'+jumlah_bayar.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">Harga</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" value="'+harga_beli.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">HPP</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" value="'+harga_beli.toLocaleString()+'" disabled="">'+
											'</div>'+
										'</div>'+
										// '<div class="form-group">'+
										// 	'<label class="control-label">PPN</label>'+
										// 	'<div class="input-group">'+
										// 		'<div class="input-group-addon">Rp</div>' +
										// 		'<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" value="0" disabled="">'+
										// 	'</div>'+
										'</div>'+
									'</td>'+
								'</tr>';

						$('#tabelKeranjang').find('tbody').append($(tr));
				
						$('#form-simpan').find('input[id="item-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_beli-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_sisa-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="jumlah_jual-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="harga-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="ppn-' + item.id + '"]').remove();
						$('#form-simpan').find('input[id="subtotal-' + item.id + '"]').remove();

						$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + item.id + '" value="' + item.id + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_beli[]" id="jumlah_beli-' + item.id + '" value="' + jumlah_beli + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_sisa[]" id="jumlah_sisa-' + item.id + '" value="' + jumlah_sisa + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_jual[]" id="jumlah_jual-' + item.id + '" value="' + jumlah_jual + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + item.id + '" value="' + harga_beli + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + item.id + '" value="' + 0 + '" />');
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + item.id + '" value="' + jumlah_bayar + '" />');

						var $harga_total = $('#inputHargaTotal');
						var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');

						var harga_total = parseFloat($harga_total.val().replace(/\D/g, ''), 10) / 100;
						harga_total += jumlah_bayar;

						// if(harga_total == 0){
						// 	$('input[name="jumlah_bayar"]').val(0);
						// 	$('#formSimpanContainer').find('button[type="submit"]').prop('disabled', false);
						// 	console.log('a');
						// }

						$harga_total.val(harga_total.toLocaleString());
						$harga_total_plus_ongkos_kirim.val(harga_total.toLocaleString());
						var harga_total = $('input[name="harga_total"]').val();
					}
				});
			}
		}

		$(document).ready(function() {
			var url = "{{ url('konsinyasi-masuk') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$(".select2_single").select2();

			konsinyasi_masuk = '{{ $konsinyasi_masuk }}';
			konsinyasi_masuk = konsinyasi_masuk.replace(/&quot;/g, '"');
			konsinyasi_masuk = JSON.parse(konsinyasi_masuk);

			relasi_konsinyasi_masuk = '{{ $relasi_konsinyasi_masuk }}';
			relasi_konsinyasi_masuk = relasi_konsinyasi_masuk.replace(/&quot;/g, '"');
			relasi_konsinyasi_masuk = JSON.parse(relasi_konsinyasi_masuk);

			$('#pilihSuplier').val(konsinyasi_masuk.suplier.id);
			handleSuplierChange(konsinyasi_masuk.suplier.id, true, true);

			// $('#inputTunaiContainer').hide();
			$('#inputTransferBankContainer').hide();
			$('#inputTransferBankContainer').find('input').val('');
			$('#inputKartuContainer').hide();
			$('#inputKartuContainer').find('input').val('');
			$('#inputCekContainer').hide();
			$('#inputCekContainer').find('input').val('');
			$('#inputBGContainer').hide();
			$('#inputBGContainer').find('input').val('');
			$('#inputTunaiContainer').addClass('sembunyi');

			var harga_total = $('input[name="harga_total"]').val();
			console.log(harga_total);
			// $('input[name="jumlah_bayar"]').val(jumlah_bayar);
			// updateHargaOnKeyup();
			// $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
		});

		// Buat ambil nomer transaksi terakhir
		$(window).on('load', function(event) {
			var url = "{{ url('konsinyasi-keluar/last.json') }}";
			
			$.get(url, function(data) {
				var kode = 1;
				if (data.konsinyasi_keluar !== null) {
					var kode_transaksi = data.konsinyasi_keluar.kode_transaksi;
					kode = parseInt(kode_transaksi.split('/')[0]);
					kode++;
				}

				kode = int4digit(kode);
				var tanggal = printTanggalSekarang('dd/mm/yyyy');
				kode_transaksi = kode + '/KNK/' + tanggal;

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		$(document).on('change', 'select[name="suplier_id"]', function(event) {
			event.preventDefault();
			
			var id = $(this).val();
			handleSuplierChange(id, false, false);
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();
			
			var id = $('select[name="item_id"]').val();
			handleItemChange(id);
		});

		$(document).on('change', 'select[name="seller_id"]', function(event) {
			event.preventDefault();
			var id = $(this).val();
			$('input[name="seller_id"]').val(id);
		});

		$(document).on('keyup', 'input[name="inputJumlahSisa"]', function(event) {
			event.preventDefault();

			var konversi = $(this).attr('konversi');

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $jumlah_beli = $tr.find('input[name="jumlah_beli"]');
			var $jumlah_sisa = $tr.find('input[name="jumlah_sisa"]');
			var $jumlah_jual = $tr.find('input[name="jumlah_jual"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $subtotal = $tr.find('input[name="inputSubTotal"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var jumlah_beli = parseInt($jumlah_beli.val().replace(/\D/g, ''), 10);
			var harga = parseInt($harga.val().replace(/\D/g, ''), 10) / 100;
			var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10) / 100;
			var hpp = parseInt($hpp.val().replace(/\D/g, ''), 10) / 100;
			var ppn = parseInt($ppn.val().replace(/\D/g, ''), 10) / 100;

			var jumlah_sisa = 0;
			$(this).parents('tr').first().find('input[name="inputJumlahSisa"]').each(function(index, el) {
				var value = parseInt($(el).val().replace(/\D/g, ''), 10);
				var konversi = parseInt($(el).attr('konversi'));
				value *= konversi;
				if (isNaN(value)) value = 0;
				jumlah_sisa += value;
			});
			var jumlah_jual = jumlah_beli - jumlah_sisa;
			// if (jumlah_jual > jumlah_beli) {
			// 	jumlah_jual = jumlah_beli;
			// }

			if (isNaN(jumlah_beli)) jumlah_beli = 0;
			if (isNaN(jumlah_sisa)) jumlah_sisa = 0;
			if (isNaN(jumlah_jual)) jumlah_jual = 0;
			if (isNaN(harga)) harga = 0;
			if (isNaN(subtotal)) subtotal = 0;
			subtotal = Math.round(harga * jumlah_jual);
			if (isNaN(hpp)) hpp = 0;
			if (isNaN(ppn)) ppn = 0;

			$jumlah_beli.val(jumlah_beli);
			$jumlah_sisa.val(jumlah_sisa);
			$jumlah_jual.val(jumlah_jual);
			$subtotal.val(subtotal.toLocaleString());
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_beli-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_sisa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_jual-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_beli[]" id="jumlah_beli-' + id + '" value="' + jumlah_beli + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_sisa[]" id="jumlah_sisa-' + id + '" value="' + jumlah_sisa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_jual[]" id="jumlah_jual-' + id + '" value="' + jumlah_jual + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + hpp + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + id + '" value="' + ppn + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10) / 100;
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString());
			updateHargaOnKeyup();
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-danger');
				$(this).find('i').show('fast');
				$('#inputTunaiContainer').removeClass('sembunyi');
				// ('fast', function() {
				// 	$(this).find('input').first().trigger('focus');
				// });
			} else if ($(this).hasClass('btn-danger')) {
				$(this).removeClass('btn-danger');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputTunaiContainer').addClass('sembunyi');
				// $('#inputTunaiContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
					$('#inputTunaiContainer').find('input').val('');
					updateHargaOnKeyup();
				// });
			}
		});

		$(document).on('click', '#btnTransfer', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-warning');
				$(this).find('i').show('fast');
				$('#inputTransferBankContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-warning')) {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputTransferBankContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_transfer"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();
            console.log('==');
            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="NoTraKartu"]').val('');
                    $('select[name="jenis_kartu"]').val('').change();
                    $('select[name="bank_kartu"]').val('').change();
                    
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

		$(document).on('click', '#btnCek', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
				$(this).find('i').show('fast');
				$('#inputCekContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-success')) {
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputCekContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnBG', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-primary');
				$(this).find('i').show('fast');
				$('#inputBGContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-primary')) {
				$(this).removeClass('btn-primary');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputBGContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_bg"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('keyup', '#inputNominalTunai', function(event) {
			event.preventDefault();
			var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			$('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="bank_transfer"]', function(event) {
			event.preventDefault();
			var id_bank = $(this).val();
			$('#formSimpanContainer').find('input[name="id_bank"]').val(id_bank);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoTransfer', function(event) {
			event.preventDefault();
			var no_transfer = $(this).val();
			$('#formSimpanContainer').find('input[name="no_transfer"]').val(no_transfer);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNominalTransfer', function(event) {
			event.preventDefault();
			console.log('a');
			var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_transfer)) nominal_transfer = 0;
			$('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="cek_id"]', function(event) {
			event.preventDefault();

			var utang = $('input[name="inputUtang"]').val();
			utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10)) / 100;

			var cek_id = $(this).val();
			if (cek_id != '') {
				var cek_text = $(this).select2('data')[0].text;
				var nominal_text = cek_text.split('Rp ')[1].split(']')[0];
				var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
				nominal = parseFloat(nominal);
				var nomor = nominal_text.split(' [')[0];
				
				if (nominal <= utang) {
					// Success
					$('#inputCekContainer').find('p').hide();
					$('#inputNominalCek').val(nominal_text);
					$('#formSimpanContainer').find('input[name="no_cek"]').val(nomor);
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
					// Button
				} else {
					// Error
					$('#inputCekContainer').find('p').show();
					$('#inputNominalCek').val('');
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					// Button
				}
			} else {
				$('#inputNominalCek').val('');
				$('#formSimpanContainer').find('input[name="no_cek"]').val('');
				$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
				// Button
			}
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="bg_id"]', function(event) {
			event.preventDefault();
			
			var utang = $('input[name="inputUtang"]').val();
			utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10)) / 100;

			var bg_id = $(this).val();
			if (bg_id != '') {
				var bg_text = $(this).select2('data')[0].text;
				var nominal_text = bg_text.split('Rp ')[1].split(']')[0];
				var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
				nominal = parseFloat(nominal);
				var nomor = nominal_text.split(' [')[0];

				if (nominal <= utang) {
					// Success
					$('#inputBGContainer').find('p').hide();
					$('#inputNominalBG').val(nominal_text);
					$('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
				} else {
					$('#inputBGContainer').find('p').show();
					$('#inputNominalBG').val('');
					$('#formSimpanContainer').find('input[name="no_bg"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
				}
			} else {
				$('#inputNominalBG').val('');
				$('#formSimpanContainer').find('input[name="no_bg"]').val('');
				$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
				// Button
			}
			updateHargaOnKeyup();
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();

			var id = $(this).parents('tr').data('id');

			var index = selected_items.indexOf(parseInt(id));
			if (index >= -1) {
				selected_items.splice(index, 1);
			}

			$('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
			$('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();

			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_beli-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_sisa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah_jual-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			$('input[name="inputHargaTotal"]').val(harga_total.toLocaleString());

			var ongkos_kirim = parseInt($('input[name="inputOngkosKirim"]').val().replace(/\D/g, ''), 10);
			var harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
			$('input[name="inputHargaTotalPlusOngkosKirim"]').val(harga_total_plus_ongkos_kirim.toLocaleString());

			var jumlah_bayar = parseInt($('input[name="inputJumlahBayar"]').val().replace(/\D/g, ''), 10);
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
			if (utang <= 0 || isNaN(utang)) utang = 0;
			$('input[name="inputUtang"]').val(utang.toLocaleString());

			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);

			updateHargaOnKeyup();
		});

	</script>
@endsection
