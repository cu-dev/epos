@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Konsinyasi Masuk</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-12">
                        <h2 id="formSimpanTitle">Tambah Konsinyasi Masuk</h2>
                        {{-- <span id="kodeTransaksiTitle"></span> --}}
                    </div>
                    <!-- <div class="col-md-6 pull-right">
                        <a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div> -->
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label" style="">Kode Faktur</label>
                        <input type="text" id="nota" name="nota" class="form-control" style="height: 39px;" placeholder="Kode Faktur">
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Pemasok</label>
                        <select name="suplier_id" class="select2_single form-control">
                            <option value="" id="default">Pilih Pemasok</option>
                            @foreach ($supliers as $suplier)
                                <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Penjual</label>
                        <select name="seller_id" class="select2_single form-control">
                            <option value="" id="default">Pilih Penjual</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option value="" id="default">Pilih Item</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo">
                    <thead>
                        <tr>
                            <th style="text-align: left;">Item</th>
                            <th style="text-align: left;">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Pembelian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table" id="tabelKeranjang">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6 pull-right">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Sub Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('konsinyasi-masuk') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="nota" />
                                        <input type="hidden" name="suplier_id" />
                                        <input type="hidden" name="seller_id" />
                                        <input type="hidden" name="po" value="0" />
                                        <input type="hidden" name="harga_total" />
                                        
                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                            <div class="form-group pull-left">
                                                <button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
                                                    <i class="fa fa-check"></i> OK
                                                </button>
                                            </div>
                                            <div class="form-group pull-left">
                                                <div type="div" id="infoTransaksi">
                                                    TOTAL 0 ITEM
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var selected_items = [];

        function isSubmitButtonDisabled() {
         //    var seller = $('input[name="seller_id"]').val();
         //    if (seller === '') {
         //        return true;
            // }

            var jumlah = 0;
            $('input[name="jumlah[]"]').each(function(index, el) {
                var val = parseInt($(el).val());
                jumlah += val;
            });
            if (isNaN(jumlah) || jumlah <= 0) return true;

            var subtotal = 0;
            $('input[name="subtotal[]"]').each(function(index, el) {
                var val = parseFloat($(el).val());
                subtotal += val;
            });
            if (isNaN(subtotal) || subtotal <= 0) return true;

            var harga_total = parseFloat($('input[name="harga_total"]').val());
            if (isNaN(harga_total) || harga_total <= 0) return true;

            return false;
        }

        function updateInfoTransaksi() {
            $('#infoTransaksi').text(`TOTAL ${selected_items.length} ITEM`);
        }

        function updateHargaOnKeyup() {
            $('input[name="harga[]"]').each(function(index, el) {
                var id = $(el).attr('item_id');
                var harga_beli = parseFloat($(el).val());
                var harga_jual = parseFloat($('#harga_termurah-'+id).val());

                if (isNaN(harga_beli)) harga_beli = 0;
                if (isNaN(harga_jual)) harga_jual = 0;
                // console.log(harga_beli, harga_jual);

                if (harga_beli < harga_jual) {
                    $('#tabelKeranjang tr').find('#checkHargaJual').prop('disabled', false);
                } else {
                    $('#tabelKeranjang tr').find('#checkHargaJual').prop('disabled', true);
                }
            });

            var $harga_total = $('#inputHargaTotal');
            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });

            $harga_total.val(harga_total.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $('input[name="harga_total"]').val(harga_total);

            updateInfoTransaksi();
            // console.log('Till here', isSubmitButtonDisabled());
            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
        }

        $(document).ready(function() {
            // var url = "{{ url('konsinyasi-masuk') }}";
            // var a = $('a[href="' + url + '"]');
            // a.parent().addClass('current-page');
            // a.parent().parent().show();
            // a.parent().parent().parent().addClass('active');
            $('#nota').val('');
            $('#formSimpanContainer').find('input[name="nota"]').val('');

            $(".select2_single").select2();

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 
        });

        // Buat ambil nomer transaksi terakhir
        $(window).on('load', function(event) {
            var url = "{{ url('konsinyasi-masuk/last.json') }}";
            
            $.get(url, function(data) {
                var kode = 1;
                if (data.konsinyasi_masuk !== null) {
                    var kode_transaksi = data.konsinyasi_masuk.kode_transaksi;
                    kode = parseInt(kode_transaksi.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_transaksi = kode + '/KNM/' + tanggal;

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });

        $(document).on('change', 'select[name="suplier_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url_items = "{{ url('konsinyasi-masuk') }}" + '/' + id + '/items.json';
            var url_sellers = "{{ url('konsinyasi-masuk') }}" + '/' + id + '/sellers.json';
            // console.log(url_items);

            if (id == '') {
                $('#tabelInfo tbody').empty();
            }
            else {

                $('input[name="suplier_id"]').val(id);
                $('#tabelKeranjang > tbody').empty();
                $('#append-section').empty();
                $('#formSimpanContainer').find('input[name="harga_total"]').val('');

                var $select_item = $('select[name="item_id"]');
                $select_item.empty();
                $select_item.append($('<option value="">Pilih Item</option>'));

                $.get(url_items, function(data) {
                    // console.log(data);
                    var items = data.items;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        $select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
                    }
                });

                var $select_sellers = $('select[name="seller_id"]');
                $select_sellers.empty();
                $select_sellers.append($('<option value="">Pilih Penjual</option>'));

                $('#spinner').show();
                $.get(url_sellers, function(data) {
                    var sellers = data.sellers;
                    for (var i = 0; i < sellers.length; i++) {
                        var seller = sellers[i];
                        $select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
                    }
                    $('#spinner').hide();
                    updateHargaOnKeyup();
                });

            }
        });

        $(document).on('change', 'select[name="seller_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="seller_id"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var id = $('select[name="item_id"]').val();
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('konsinyasi-masuk') }}" + '/items/' + id + '/json';

            if (id == '') {
                $('#tabelInfo').find('tbody').empty();
            } else if (!selected_items.includes(parseInt(id))) {
                if (!isNaN(id)) {
                    selected_items.push(parseInt(id));
                }

                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(url, data);
                    var pcs = data.pcs;
                    var item = data.item;
                    var kode = item.kode;
                    var nama = item.nama;
                    var stok = item.stoktotal;
                    var satuans = item.satuan_pembelians;
                    // console.log(pcs);
                    var harga_lama = $('input[name="inputHargaTotal"]').val();
                    // var tunai_lama = $('input[name="inputJumlahBayar"]').val();
                    // var kembali_lama = $('input[name="inputUtang"]').val();
                    var pertama_beli = item.pertama_beli;
                    var harga_termurah = item.harga_termurah;

                    var stoks = '';
                    var temp = stok;
                    if (satuans.length > 0) {
                        for (var i = 0; i < satuans.length; i++) {
                            var satuan = satuans[i];
                            var hasil = parseInt(temp / satuan.konversi);
                            if (hasil > 0) {
                                temp %= satuan.konversi;
                                stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                            }
                        }
                    } else {
                        stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                    }

                    $('#tabelInfo').find('tbody').empty();
                    $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

                    if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;
                    // if (tunai_lama === '' || tunai_lama === undefined) tunai_lama = 0;
                    // if (kembali_lama === '' || kembali_lama === undefined) kembali_lama = 0;

                    if (tr === undefined) {
                        $('#tabelKeranjang').find('tr[id="totalharga"]').remove();
                        // $('#tabelKeranjang').find('tr[id="totalbayar"]').remove();
                        // $('#tabelKeranjang').find('tr[id="totalkembali"]').remove();

                        var divs = '<div class="form-group">'+
                                        '<label class="control-label">Jumlah</label>';

                        if (satuans.length > 0) {
                            for (var i = 0; i < satuans.length; i++) {
                                var satuan = satuans[i];
                                divs += '<div class="input-group text-center">' +
                                            '<input type="text" id="inputJumlah" name="inputJumlah" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'">' +
                                            '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+satuan.satuan.kode+'</div>' +
                                        '</div>';
                            }
                            divs += '</div>';
                        } else {
                            divs += '<div class="input-group">' +
                                        '<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
                                        '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+pcs.kode+'</div>' +
                                    '</div>' +
                                '</div>';
                        }

                        var tr = '<tr data-id="' + id + '">'+
                                    '<input type="hidden" name="jumlah">'+
                                    '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                    '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3>'+
                                        `<div class="form-group" id="anak-kadaluarsa-` + id + `" jkal="0">
                                                <label class="control-label">Kadaluarsa</label>
                                                <i class="fa fa-plus" id="tambah_kadaluarsa" style="color: green; cursor: pointer; margin-left: 10px;"></i>
                                            </div>`+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        divs+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Total</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group hidden">'+
                                            '<label class="control-label">Harga</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group" style="width: 209%;">'+
                                            '<label class="control-label">Harga Jual</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">'+
                                                    '<input name="checkHargaJual" id="checkHargaJual" type="checkbox" '+(pertama_beli?'disabled=""':'')+'>'+
                                                '</div>' +
                                                '<input type="text" name="inputHargaJual" id="inputHargaJual" class="form-control input-sm angka" disabled="" value="Harga jual seperti biasa">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">HPP</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group hidden">'+
                                            '<label class="control-label">PPN</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                '</tr>';

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="" item_id="'+id+'" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga_biasa[]" id="harga_biasa-' + id + '" value="0" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga_termurah[]" id="harga_termurah-' + id + '" value="'+harga_termurah+'" />');

                        $('#tabelKeranjang').find('tbody').append($(tr));
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                    }

                    $('#spinner').hide();
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('focus', '.inputKadaluarsa', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // $(this).daterangepicker({
            //     singleDatePicker: true,
            //     calender_style: "picker_2",
            //     format: 'DD-MM-YYYY'
            // }, function(start) {
            //     var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
            //         .find('input[name="inputKadaluarsa"]').val();

            //     $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-0"]').val(kadaluarsa);
            // });

            $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            calender_style: "picker_2",
            format: 'DD-MM-YYYY',
            locale: {
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Awal",
                "toLabel": "Akhir",
                "customRangeLabel": "Custom",
                "weekLabel": "M",
                "daysOfWeek": [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                "monthNames": [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                "firstDay": 1
            },
        }, function(start) {
            var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
                .find('input[name="inputKadaluarsa"]').val();

            $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-0"]').val(kadaluarsa);
        });

        });

        $(document).on('focus', '.inputKadaluarsaStok', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            var kol_kadal = $(this).parents('.kol-kadal');
            // console.log(jkal);
            // $(this).daterangepicker({
            //     singleDatePicker: true,
            //     calender_style: "picker_2",
            //     format: 'DD-MM-YYYY'
            // }, function(start) {
            //     var kadaluarsa = kol_kadal.find('input[name="inputKadaluarsa"]').val();
            //     $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-'+jkal+'"]').val(kadaluarsa);
            // });

            $(this).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start) {
                var kadaluarsa = kol_kadal.find('input[name="inputKadaluarsa"]').val();
                $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-'+jkal+'"]').val(kadaluarsa);
            });
        });

        $(document).on('change', '#checkKadaluarsa', function(event) {
            event.preventDefault();

            $(this).parent().next().val('');
            var checked = $(this).prop('checked');
            if (checked) {
                $(this).parent().next().prop('disabled', false);
                $(this).parent().next().removeClass('bg-disabled');
            } else {
                $(this).parent().next().prop('disabled', true);
                $(this).parent().next().addClass('bg-disabled');
                id = $(this).parents('tr').first().data('id');
                $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
                $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="" />');
            }
        });

        $(document).on('keyup', 'input[name="inputJumlah"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $subtotal = $tr.find('input[name="inputSubTotal"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
            var jumlah = 0;
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            });
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;
            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            var ppn = 0;
            var hpp = harga;
            if (isNaN(hpp)) hpp = 0;

            $jumlah.val(jumlah);
            $harga.val(harga.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $hpp.val(hpp.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $ppn.val(ppn.toLocaleString(undefined, {maximumFractionDigits: 2}));

            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);

            var skal = $('#kadaluarsa-'+id).attr('skal');
            if (skal == 0) {
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            } else {
                var total_hasil = 0;
                $('.jumlah-stok-'+id).each(function(index, el) {
                    var jumlah_stok = parseInt($(el).val());
                    if (isNaN(jumlah_stok)) jumlah_stok = 0;
                    total_hasil += jumlah_stok;
                })

                if (parseInt(total_hasil) != parseInt(jumlah)) {
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).addClass('has-error');
                    });
                } else {
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).removeClass('has-error');
                    });
                }
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });

            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(undefined, {maximumFractionDigits: 2}));
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputSubTotal', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;
            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            var ppn = 0;
            var hpp = harga;
            if (isNaN(hpp)) hpp = 0;

            $harga.val(harga.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $hpp.val(hpp.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $ppn.val(ppn.toLocaleString(undefined, {maximumFractionDigits: 2}));

            hpp = hpp.toFixed(2);
            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);

            var skal = $('#kadaluarsa-'+id).attr('skal');
            if(skal == 0){
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });

            var hpp_stok = $('#form-simpan').find('input[id="harga-'+id+'"]').val()
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp_stok);
            })

            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(undefined, {maximumFractionDigits: 2}));
            updateHargaOnKeyup();
        });

        $(document).on('change', '#checkHargaJual', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('data-id');
            var checked = $(this).prop('checked');
            if (checked) {
                $('#harga_biasa-'+id).val(1);
            } else {
                $('#harga_biasa-'+id).val(0);
            }
        });

        $(document).on('click', '#tambah_kadaluarsa', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // var satuan = [[],[]];
            var satuan = [];
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                // console.log(index);
                var konversi = parseInt($(el).attr('konversi'));
                var kode_satuan = $(el).next().text();
                satuan.push({'konversi' : konversi, 'kode' : kode_satuan});
            });
            var satuan_pilihan = '';
            for (var i = 0; i < satuan.length; i++) {
                satuan_pilihan = satuan_pilihan+`<div class="row">
                            <div class="col-md-12">
                                <div class="input-group text-center">
                                    <input id="inputJumlahExp" name="inputJumlahExp" class="pull-right form-control input-sm angka" konversi="`+satuan[i]['konversi']+`"  type="text">
                                    <div class="input-group-addon" style="width: 60px; text-align: right;">`+satuan[i]['kode']+`
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }

            var jumlah_kadaluarsa = 0;
            $('#anak-kadaluarsa-'+id+' .m-kadal').each(function(index, el) {
                jumlah_kadaluarsa++;
            });
            jumlah_kadaluarsa++;

            $('#form-simpan').find('#append-section').append('<input type="hidden" item-id="'+id+'" name="item_stok['+id+'][]" id="item-stok-' + id + '-'+jumlah_kadaluarsa+'" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" item-id="'+id+'" name="hpp_stok['+id+'][]" id="hpp-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="hpp-stok-' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" item-id="'+id+'" name="jumlah_stok['+id+'][]" id="jumlah-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="jumlah-stok-' + id + '"/>');
            $('#form-simpan').find('#append-section').append('<input type="hidden" item-id="'+id+'" name="kadaluarsa_stok['+id+'][]" id="kadaluarsa-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" />');

            $('#anak-kadaluarsa-'+id).append(`
                <div class="line"></div>
                    <div class="row kol-kadal m-kadal" kol-kadal="`+jumlah_kadaluarsa+`">
                        <div class="col-md-12">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Jumlah
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Kadaluarsa
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0;">
                                `+satuan_pilihan+`
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input name="checkKadaluarsa" id="checkKadaluarsa" type="checkbox">
                                    </span>
                                    <input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok tanggal-putih bg-disabled" disabled="" value="" type="text" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>`);

            // merah kalau jumlah total dengan jumlah kadaluarsa berbeda
            var jumlah = 0;
            $(this).parents('td').first().next().find('input[name="inputJumlah"]').each(function(index, el) {
                var temp_jumlah = parseFloat($(el).val().replace(/\D/g, ''), 10);
                var temp_konversi = parseFloat($(el).attr('konversi'));

                if (isNaN(temp_jumlah)) temp_jumlah = 0;
                if (isNaN(temp_konversi)) temp_konversi = 0;
                jumlah += (temp_jumlah * temp_konversi);
            });
            if (isNaN(jumlah)) jumlah = 0;

            var jumlah_kadaluarsa = 0;
            $('input[name="jumlah_stok['+id+'][]"]').each(function(index, el) {
                var temp_jumlah = parseFloat($(el).val());
                if (isNaN(temp_jumlah)) temp_jumlah = 0;
                jumlah_kadaluarsa += temp_jumlah;
            });

            if (jumlah_kadaluarsa != jumlah) {
                $(this).parents('tr').first().find('.m-kadal').addClass('has-error');
            } else {
                $(this).parents('tr').first().find('.m-kadal').removeClass('has-error');
            }

            updateHargaOnKeyup();
        });

        $(document).on('keyup', 'input[name="inputJumlahExp"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');
            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var jumlah = 0;
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            // console.log(jkal);
            var kol_kadal = $(this).parents('.kol-kadal').first().find('input[name="inputJumlahExp"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            });

            var hpp = $('#form-simpan').find('input[id="harga-'+id+'"]').val();
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp);
            })
            // console.log(jumlah);
            $('#form-simpan').find('input[id="jumlah-stok-' + id + '-'+jkal+'"]').val(jumlah);
            var jumlah_total = $('#form-simpan').find('input[id="jumlah-'+id+'"]').val();

            var total_hasil = 0;
            $('.jumlah-stok-'+id).each(function(index, el) {
                var jumlah_stok = parseInt($(el).val());
                if (isNaN(jumlah_stok)) jumlah_stok = 0;
                total_hasil += jumlah_stok;
            })

            if (parseInt(total_hasil) != parseInt(jumlah_total)) {
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).addClass('has-error');
                });
            } else {
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).removeClass('has-error');
                });
            }
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').data('id');

            var index = selected_items.indexOf(parseInt(id));
            if (index >= -1) {
                selected_items.splice(index, 1);
                updateHargaOnKeyup();
            }

            $('select[name="item_id"]').val('').trigger('change');
            $('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();

            $('input[item-id="'+id+'"]').remove();

            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('input[id="harga-' + id + '"]').remove();
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });
            $('input[name="inputHargaTotal"]').val(harga_total.toLocaleString(undefined, {maximumFractionDigits: 2}));

            $('input[name="inputHargaTotal"]').val(harga_total.toLocaleString(undefined, {maximumFractionDigits: 2}));
            $('input[name="harga_total"]').val(harga_total);

            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#nota', function(event) {
            event.preventDefault();

            var nota = $(this).val();
            $('#formSimpanContainer').find('input[name="nota"]').val(nota);
        });

    </script>
@endsection
