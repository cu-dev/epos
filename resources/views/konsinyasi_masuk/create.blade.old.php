@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Konsinyasi Masuk</title>
@endsection

@section('style')
	<style media="screen">
		#btnKembali {
			margin-right: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
		#metodePembayaranButtonGroup {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-6">
						<h2 id="formSimpanTitle">Konsinyasi Masuk</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					<div class="col-md-6 pull-right">
						<a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                			<i class="fa fa-long-arrow-left"></i>
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Pilih Suplier</label>
						<select name="suplier_id" class="select2_single form-control">
							<option value="" id="default">Pilih Suplier</option>
							@foreach ($supliers as $suplier)
								<option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Pilih Sales</label>
						<select name="seller_id" class="select2_single form-control">
							<option value="" id="default">Pilih Sales</option>
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Pilih Item</label>
						<select name="item_id" class="select2_single form-control">
							<option value="" id="default">Pilih Item</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Informasi Item</h2>
				<a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Stok</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Pembelian</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<div class="row">
							<table class="table" id="tabelKeranjang">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" disabled="">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div id="formSimpanContainer">
									<form id="form-simpan" action="{{ url('konsinyasi-masuk') }}" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="kode_transaksi" value="" />
										<input type="hidden" name="suplier_id" />
										<input type="hidden" name="seller_id" />
										<input type="hidden" name="po" value="0" />
										<input type="hidden" name="harga_total" />
										
										<div id="append-section"></div>
										<div class="clearfix">
											<div class="form-group pull-left">
												<button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
													<i class="fa fa-check"></i> OK
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		var selected_items = [];

		function isSubmitButtonDisabled() {
		 //    var seller = $('input[name="seller_id"]').val();
		 //    if (seller === '') {
		 //        return true;
			// }

			var jumlah = 0;
			$('input[name="jumlah[]"]').each(function(index, el) {
				var val = parseInt($(el).val());
				jumlah += val;
			});
			if (isNaN(jumlah) || jumlah <= 0) return true;

			var subtotal = 0;
			$('input[name="subtotal[]"]').each(function(index, el) {
				var val = parseFloat($(el).val());
				subtotal += val;
			});
			if (isNaN(subtotal) || subtotal <= 0) return true;

			var harga_total = parseFloat($('input[name="harga_total"]').val());
			if (isNaN(harga_total) || harga_total <= 0) return true;

			return false;
		}

		function updateHargaOnKeyup() {
			var $harga_total = $('#inputHargaTotal');

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});

			$harga_total.val(harga_total.toLocaleString());
			$('input[name="harga_total"]').val(harga_total);

			// console.log('Till here', isSubmitButtonDisabled());
			$('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
		}

		$(document).ready(function() {
			// var url = "{{ url('konsinyasi-masuk') }}";
			// var a = $('a[href="' + url + '"]');
			// a.parent().addClass('current-page');
			// a.parent().parent().show();
			// a.parent().parent().parent().addClass('active');

			$(".select2_single").select2();

			$('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 
		});

		// Buat ambil nomer transaksi terakhir
		$(window).on('load', function(event) {
			var url = "{{ url('konsinyasi-masuk/last.json') }}";
			
			$.get(url, function(data) {
				var kode = 1;
				if (data.konsinyasi_masuk !== null) {
					var kode_transaksi = data.konsinyasi_masuk.kode_transaksi;
					kode = parseInt(kode_transaksi.split('/')[0]);
					kode++;
				}

				kode = int4digit(kode);
				var tanggal = printTanggalSekarang('dd/mm/yyyy');
				kode_transaksi = kode + '/KNM/' + tanggal;

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		$(document).on('change', 'select[name="suplier_id"]', function(event) {
			event.preventDefault();
			
			var id = $(this).val();
			var url_items = "{{ url('konsinyasi-masuk') }}" + '/' + id + '/items.json';
			var url_sellers = "{{ url('konsinyasi-masuk') }}" + '/' + id + '/sellers.json';

			$('input[name="suplier_id"]').val(id);
			$('#tabelKeranjang > tbody').empty();
			$('#append-section').empty();
			$('#formSimpanContainer').find('input[name="harga_total"]').val('');

			var $select_item = $('select[name="item_id"]');
			$select_item.empty();
			$select_item.append($('<option value="">Pilih Item</option>'));

			$.get(url_items, function(data) {
				var items = data.items;
				for (var i = 0; i < items.length; i++) {
					var item = items[i];
					$select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
				}
			});

			var $select_sellers = $('select[name="seller_id"]');
			$select_sellers.empty();
			$select_sellers.append($('<option value="">Pilih Sales</option>'));

			$('#spinner').show();
			$.get(url_sellers, function(data) {
				var sellers = data.sellers;
				for (var i = 0; i < sellers.length; i++) {
					var seller = sellers[i];
					$select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
				}
				$('#spinner').hide();
				updateHargaOnKeyup();
			});
		});

		$(document).on('change', 'select[name="seller_id"]', function(event) {
			event.preventDefault();
			var id = $(this).val();
			$('input[name="seller_id"]').val(id);
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();
			
			var id = $('select[name="item_id"]').val();
			var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
			var url = "{{ url('konsinyasi-masuk') }}" + '/items/' + id + '/json';

			if (!selected_items.includes(parseInt(id))) {
				if (!isNaN(id)) {
					selected_items.push(parseInt(id));
				}

				$('#spinner').show();
				$.get(url, function(data) {
					var pcs = data.pcs;
					var item = data.item;
					var kode = item.kode;
					var nama = item.nama;
					var stok = item.stoktotal;
					var satuans = item.satuans;
					// console.log(pcs);
					var harga_lama = $('input[name="inputHargaTotal"]').val();

					var stoks = '';
					var temp = stok;
					if (satuans.length > 0) {
						for (var i = 0; i < satuans.length; i++) {
							var satuan = satuans[i];
							var hasil = parseInt(temp / satuan.konversi);
							if (hasil > 0) {
								temp %= satuan.konversi;
								stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
							}
						}
					} else {
						stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
					}

					$('#tabelInfo').find('tbody').empty();
					$('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

					if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;

					if (tr === undefined) {
						$('#tabelKeranjang').find('tr[id="totalharga"]').remove();

						var divs = '<div class="form-group">'+
										'<label class="control-label">Jumlah</label>';

						if (satuans.length > 0) {
							for (var i = 0; i < satuans.length; i++) {
								var satuan = satuans[i];
								divs += '<div class="input-group">' +
											'<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+satuan.konversi+'">' +
											'<div class="input-group-addon">'+satuan.satuan.kode+'</div>' +
										'</div>';
							}
							divs += '</div>';
						} else {
							divs += '<div class="form-group">'+
										'<div class="input-group">' +
											'<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
											'<div class="input-group-addon">'+pcs.kode+'</div>' +
										'</div>' +
									'</div>';
						}

						var tr = '<tr data-id="' + id + '">'+
									'<input type="hidden" name="jumlah">'+
									'<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
									'<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3></td>'+
									'<td>'+
										divs+
										'<div class="form-group">'+
											'<label class="control-label">Kadaluarsa</label>'+
											'<div class="input-group">'+
												'<span class="input-group-addon">'+
													'<input type="checkbox" name="checkKadaluarsa" id="checkKadaluarsa">'+
												'</span>'+
												'<input type="text" name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsa" disabled="" value="">'+
											'</div>'+
										'</div>'+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">Sub Total</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">Harga</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">HPP</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">PPN</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
									'</td>'+
								'</tr>';

						$('#tabelKeranjang').find('tbody').append($(tr));
					}

					$('.inputKadaluarsa').daterangepicker({
						singleDatePicker: true,
						calender_style: "picker_2",
						format: 'DD-MM-YYYY'
					}, function(start) {
						var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
							.find('input[name="inputKadaluarsa"]').val();

						$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
					});

					$('#spinner').hide();
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('change', 'input[type="checkbox"]', function(event) {
			event.preventDefault();

			$(this).parent().next().val('');
			var checked = $(this).prop('checked');
			if (checked) {
				$(this).parent().next().prop('disabled', false);
			} else {
				$(this).parent().next().prop('disabled', true);
				id = $(this).parents('tr').first().data('id');
				$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
				$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="" />');
			}
		});

		$(document).on('keyup', 'input[name="inputJumlah"]', function(event) {
			event.preventDefault();

			var konversi = $(this).attr('konversi');

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $kadaluarsa = $tr.find('.inputKadaluarsa');
			var $jumlah = $tr.find('input[name="jumlah"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $subtotal = $tr.find('input[name="inputSubTotal"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
			var jumlah = 0;
			$(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
				var value = parseInt($(el).val().replace(/\D/g, ''), 10);
				var konversi = parseInt($(el).attr('konversi'));
				value *= konversi;
				if (isNaN(value)) value = 0;
				jumlah += value;
			});
			var kadaluarsa = $kadaluarsa.val();

			if (isNaN(subtotal)) subtotal = 0;
			if (isNaN(jumlah)) jumlah = 0;
			var harga = subtotal / jumlah;
			if (isNaN(harga) || harga === Infinity) harga = 0;
			var ppn = 0;
			var hpp = harga;
			if (isNaN(hpp)) hpp = 0;

			$jumlah.val(jumlah);
			$harga.val(harga.toLocaleString());
			$hpp.val(hpp.toLocaleString());
			$ppn.val(ppn.toLocaleString());
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + harga + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString());
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputSubTotal', function(event) {
			event.preventDefault();

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $kadaluarsa = $tr.find('.inputKadaluarsa');
			var $jumlah = $tr.find('input[name="jumlah"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var subtotal = parseInt($(this).val().replace(/\D/g, ''), 10);
			var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
			var kadaluarsa = $kadaluarsa.val();

			if (isNaN(subtotal)) subtotal = 0;
			if (isNaN(jumlah)) jumlah = 0;
			var harga = subtotal / jumlah;
			if (isNaN(harga) || harga === Infinity) harga = 0;
			var ppn = 0;
			var hpp = harga;
			if (isNaN(hpp)) hpp = 0;

			$harga.val(harga.toLocaleString());
			$hpp.val(hpp.toLocaleString());
			$ppn.val(ppn.toLocaleString());
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + harga + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString());
			updateHargaOnKeyup();
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			
			var id = $(this).parents('tr').data('id');

			var index = selected_items.indexOf(parseInt(id));
			if (index >= -1) {
				selected_items.splice(index, 1);
			}

			$('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
			$('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});

			$('input[name="inputHargaTotal"]').val(harga_total.toLocaleString());
			$('input[name="harga_total"]').val(harga_total);

			updateHargaOnKeyup();
		});

	</script>
@endsection
