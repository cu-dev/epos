@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Konsinyasi Keluar</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnKembali {
			margin-bottom: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		.no-border {
			border: none;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Konsinyasi Masuk</h2>
				<a href="{{ url('konsinyasi-masuk/'.$konsinyasi_masuk->id) }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<section class="content invoice">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Detail Transaksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Kode Transaksi</td>
											<td>{{ $konsinyasi_masuk->kode_transaksi }}</td>
										</tr>
										<tr>
											<td>Nama Suplier</td>
											<td>{{ $konsinyasi_masuk->suplier->nama }}</td>
										</tr>
										<tr>
											<td>Nama Sales</td>
											<td>{{ $konsinyasi_masuk->seller->nama }}</td>
										</tr>
										<tr>
											<td>Harga Total</td>
											<td>{{ \App\Util::duit($konsinyasi_masuk->harga_total) }}</td>
										</tr>
										<tr>
											<td>Ongkos Kirim</td>
											<td>{{ \App\Util::duit($konsinyasi_masuk->ongkos_kirim) }}</td>
										</tr>
										<tr>
											<td>Jumlah Bayar</td>
											<td>{{ \App\Util::duit($konsinyasi_masuk->jumlah_bayar) }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;">Nama Item</th>
											<th style="text-align: left;">Jumlah</th>
											<th style="text-align: right;">Harga</th>
											<th style="text-align: right;">HPP</th>
											<th style="text-align: right;">PPN</th>
											<th style="text-align: right;">Sub Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($relasi_konsinyasi_masuk as $i => $relasi)
											<tr>
												<td>{{ $relasi->item->nama }}</td>
												<td>
												@foreach ($relasi->jumlah as $j => $jumlah)
													<span class="label label-info">{{ $jumlah['jumlah'] }} {{ $jumlah['satuan']->kode }}</span>
												@endforeach
												</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->harga) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->harga) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit(0) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->subtotal)}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Form Konsinyasi Keluar</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<div class="row">
							<table class="table" id="tabelKeranjang">
								<tbody>
									@foreach ($relasi_konsinyasi_masuk as $i => $relasi)
										<tr data-id="{{ $relasi->transaksi_pembelian->id }}">
											<input type="hidden" name="jumlah">
											<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>
											<td width="40%"><h3>{{ $relasi->item->nama }}</h3></td>
											<td>
												<div class="form-group">
													<label class="control-label">Sisa</label>
													@foreach ($relasi->jumlah as $j => $jumlah)
														<div class="input-group">
															<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="{{ $jumlah['konversi'] }}">
															<div class="input-group-addon">{{ $jumlah['satuan']->kode }}</div>
														</div>
													@endforeach
												</div>
												<div class="form-group">
													<label class="control-label">Sub Total</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">
													</div>
												</div>
											</td>
											<td>
												<div class="form-group">
													<label class="control-label">Harga</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" value="{{ \App\Util::duit($relasi->harga) }}" disabled="">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label">HPP</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" value="{{ \App\Util::duit($relasi->harga) }}" disabled="">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label">PPN</label>
													<div class="input-group">
														<div class="input-group-addon">Rp</div>
														<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" value="{{ \App\Util::duit(0) }}" disabled="">
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('konsinyasi-masuk') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
