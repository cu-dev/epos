@extends('layouts.admin')

@section('title')
    <title>EPOS | Bayar Hutang</title>
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="col-md-4">
			<div class="x_panel">
				<div class="x_title">
					<h2>Bayar Hutang</h2>
					<a href="{{ url('hutang-suplier') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
	            	</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
				<div id="formSimpanContainer">
					<form method="post" action="{{ url('hutang-suplier') }}" class="form-horizontal">
						<input type="hidden" name="metode_pembayaran" value="tunai">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label">Nama Pemasok</label>
							<input type="hidden" name="suplier_id" value="{{ $suplier->id }}">
							<input class="form-control" type="text" name="suplier" value="{{ $suplier->nama }}" disabled>
						</div>
						<div class="form-group">
							<label class="control-label">Metode Pembayaran</label>
							<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
								<div class="btn-group" role="group">
									<button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
								</div>
								<div class="btn-group" role="group">
									<button type="button" id="btnDebit" class="btn btn-default">Debit</button>
								</div>
							</div>
						</div>
						<div class="form-group" id="inputNoTransferContainer">
							<label class="control-label">Nomor Transfer</label>
							<input class="form-control" id="inputNoTransfer" type="text" name="no_transfer">
						</div>
						<div class="form-group">
							<label class="control-label">Jumlah Bayar</label>
							<input class="form-control angka" type="text" name="jumlah">
							<input type="hidden" name="jumlah_bayar">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-money"></i> Bayar</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="x_panel">
				<div class="x_title">
					<h2>Data Log Hutang</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
						<thead>
							<tr>
								<th>No</th>
								<th>Transaksi ID</th>
								<th>Tanggal Pembayaran</th>
								<th>Jumlah Dibayar</th>
							</tr>
						</thead>
						<tbody>
							@foreach($log_hutangs as $num => $log)
							<tr id="{{$log->id}}">
								<td>{{ $num+1 }}</td>
								<td>{{ $log->id }}</td>
								<td>{{ $log->created_at}}</td>
								<td>{{ number_format($log->jumlah_bayar) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil dibayar!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Hutang gagal dibayar!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();

		$(document).ready(function() {
			var url = "{{ url('hutang-suplier') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$('#inputNoTransferContainer').hide();
			$('#inputNoTransferContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();

			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnDebit').removeClass('btn-success');
			$('#btnDebit').addClass('btn-default');

			$('#inputNoTransferContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('tunai');
			$('#formSimpanContainer').find('input[name="no_transfer"]').remove();
		});

		$(document).on('click', '#btnDebit', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#inputNoTransferContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('debit');
			$('#formSimpanContainer > form').append('<input type="hidden" name="no_transfer" />');
		});

		$(document).on('keyup', '#inputNoTransfer', function(event) {
			event.preventDefault();
			var no_transfer = $(this).val();
			$('#formSimpanContainer').find('input[name="no_transfer"]').val(no_transfer);
		});
	</script>
@endsection
