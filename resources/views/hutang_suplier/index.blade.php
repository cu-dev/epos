@extends('layouts.admin')

@section('title')
    <title>EPOS | Hutang</title>
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Hutang/Transaksi</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Pemasok</th>
							<th>Jumlah Hutang</th>
							<th>Sisa Hutang</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($hutangs as $num => $hutang)
						<tr id="{{$hutang->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $hutang->suplier->nama }}</td>
							<td class="text-right">{{ \App\Util::duit($hutang->total_hutang) }}</td>
							<td class="text-right">{{ \App\Util::duit($hutang->sisa_hutang) }}</td>
							<td>
								<a href="{{ url('hutang-suplier/show/'.$hutang->suplier_id) }}" class="btn btn-xs btn-primary" id="btnBayar">
									<i class="fa fa-money"></i> Bayar
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'lunas')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil lunas!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();
	</script>
@endsection
