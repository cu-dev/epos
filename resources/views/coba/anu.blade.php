@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12" id="formSimpanContainer">
	<div class="x_panel">
	  <div class="x_title">
		<h2 id="formSimpanTitle">Tambah Peralatan</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <div class="pull-right">
					<li> 

					<button data-target="#collapse-peralatan" data-toggle="collapse" class="sembunyi"> <i class="fa fa-chevron-down"> </i> </button> </li>
				  </div>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<a href="#myModal" data-target="#myModal" data-toggle="modal">coba modal</a>
			</div>

			<div class="modal fade" id="myModal" role="dialog">
			  	<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<form method="post" action="{{ url('coba/rusak/') }}" class="form-horizontal">
					  		<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Modal Header</h4>
					  		</div>
					  		<div class="modal-body">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<input type="hidden" name="_method" value="put">
								<input type="hidden" name="id" value="{{$user->id}}">
								<div class="form-group">
									<label class="control-label">Keterangan - {{ $user->nama }}</label>
									<input class="form-control" type="text" name="keterangan">
								</div>
					  		</div>
						  	<div class="modal-footer">
								<button type="submit" name="submit" class="btn btn-default">Simpan</button>
						  	</div>
					  	</form>
					</div>
			  	</div>
			</div>

		  </div>
	  </div>
	</div>
  </div>
</div>

@endsection


@section('script')
  <script type="text/javascript">
	 $(document).ready(function() {
		$('#reservation').daterangepicker(null, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
	  });    

  </script>
@endsection