@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }
        
        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }
    </style>

    <style type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        @font-face {
            font-family: "RobotoCondensed-Light";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Light.ttf') }}");
        }
        @font-face {
            font-family: "RobotoCondensed-Regular";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Regular.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @page {
            size: 8cm 100cm;
            margin: 0mm 0mm 0mm 0mm;
        }

        @media print {
            margin-top: 0;
        }

        .zero{
            margin: 0;
            padding: 0;
            font-size: 10px;
            line-height: 1.2;
        }
        #cetak {
            font-size: 10px;
        }

        #toko {
            font-family: 'ClarendonBT';
            font-size: 15.6px;
            font-weight: bold;
        }

        .text-center {
            text-align: center;
            margin: 0;
        }

        #content td {
            font-size: 10px;
            text-transform: uppercase;
        }
        #cetak, #content {
            font-family: 'Arial';
        }

        table tr td.ket{
            font-size: 10px;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('app.body')
    <div id="cetak">
        <div id="header" style="margin-bottom: 10px;">
            <center>
                {{-- <img align="middle" src="{{ URL::to('images/logo.png') }}" class="profile_img" style="width: 150px; height: 54px;"> --}}
                <p class="zero" style="font-size: 12px;">PEMERIKSAAN PENGAMBILAN BARANG</p>
                <p id="toko" class="zero" style="font-size: 24px;">KENCANA MULYA</p>
                <p class="zero" style="font-size: 15.4px;">KARANGDUWUR - PETANAHAN</p>
                {{-- <p class="zero">JL. PURING-PETANAHAN NO. 3 KARANGDUWUR</p>
                <p class="zero">kencanamulyakarangduwur@gmail.com</p>
                <p class="zero">(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p> --}}
            </center>
        </div>
        {{-- <hr style="margin-bottom: 0"> --}}
        <table style="width: 100%; border-top: 0.5px solid; border-bottom: 0.5px solid;">
            <tr>
                <td class="ket">0001/TRAJ/01/11/2017</td>
                <td class="ket" style="text-align: right;">Kasir : Sutinem</td>
            </tr>
            <tr>
                <td class="ket">01/11/2017 09:30:11</td>
                <td class="ket" style="text-align:right; ">KMK MANAGEMENT</td>
            </tr>
        </table>
        {{-- <hr style="margin: 0"> --}}
        <div id="content">
            <table style="width: 100%; border-bottom: 0.5px solid;">
                <tbody>
                    <tr>
                        <td colspan="1" style="padding-right: 5px">1</td>
                        <td colspan="6">mogu mogu cconut</td>
                        <td colspan="1" style="text-align: right;">3 SLPY 2 PLPL</td>
                        <td colspan="2" style="text-align: right; border: width: 20px;"></td>
                        <td colspan="2" style="text-align: right; border: 0.5px solid; width: 20px;"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-right: 5px">2</td>
                        <td colspan="6">mogu mogu cconut</td>
                        <td colspan="1" style="text-align: right;">13 SLPY 2 PLPL</td>
                        <td colspan="2" style="text-align: right; width: 20px;"></td>
                        <td colspan="2" style="text-align: right; border: 0.5px solid; width: 20px;"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-right: 5px">3</td>
                        <td colspan="6">mogu mogu cconut</td>
                        <td colspan="1" style="text-align: right;">13 SLPY 2 PLPL</td>
                        <td colspan="2" style="text-align: right; width: 20px;"></td>
                        <td colspan="2" style="text-align: right; border: 0.5px solid; width: 20px;"></td>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-right: 5px">4</td>
                        <td colspan="6">mogu mogu cconut</td>
                        <td colspan="1" style="text-align: right;">13 SLPY 2 PLPL</td>
                        <td colspan="2" style="text-align: right; width: 20px;"></td>
                        <td colspan="2" style="text-align: right; border: 0.5px solid; width: 20px;"></td>
                    </tr>
                    {{-- <tr>
                        <td style="text-align: right;" colspan="10">Potongan Harga :</td>
                        <td colspan="2" style="text-align: right; border-top:0.5px solid;">(500)</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="10">Jumlah Total :</td>
                        <td colspan="2" style="text-align: right;">45.500</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="10">Tunai :</td>
                        <td colspan="2" style="text-align: right;">50.000</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="10">Kembalian :</td>
                        <td colspan="2" style="text-align: right;">4.500</td>
                    </tr> --}}
                </tbody>
            </table>
            <P style="text-align: center; margin-top: 10px; margin-bottom: 0; padding-bottom: 0;" class="zero">BEKERJA IKHLAS TANPA BEBAN</P>
        </div>
    </div>
@endsection

@section('app.script')
    {{-- <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script> --}}
    <script type="text/javascript">
        // $('p').each(function(index, el) {
        //  $(el).text($(el).text().toUpperCase());
        // });
        window.print();
    </script>

@endsection
