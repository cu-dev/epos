@extends('layouts.print')
@section('app.style')
	<style type="text/css">
		@font-face {
		    font-family: "Arial";
		    src: url("{{ asset('fonts/Arial.ttf') }}");
		}

		@font-face {
		    font-family: "ClarendonBT";
		    src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
		}

		p {
			margin-top: 0;
			margin-bottom: 0;
			font-size: 12px;
		}

		@page WordSection1
		{
			size:24.13cm 13.97cm;
			margin:0;
			margin-left: -20px;
			margin-right: 0;
			margin-bottom: 0;
			margin-top: -10px;
		}

		div.WordSection1
		{ 
			page:WordSection1;
		    font-family: "Arial";
		}
		
		.table {
		    border-collapse: collapse !important;
		}

		.table-bordered th,
		.table-bordered td {
		    border: 1px solid #000 !important;
		}

		.table {
		 	width: 100%;
			max-width: 100%;
			margin-bottom: 5px;
		}

		.table > thead > tr > th,
		.table > tbody > tr > th,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > tbody > tr > td,
		.table > tfoot > tr > td {
			padding-left: 3px;
			padding-right: 3px;
			padding-bottom: 0.5px;
			padding-top: 0.5px;
			line-height: 12px;
			vertical-align: top;
			border-top: 1px solid #000;
			font-size: 10px;
			word-wrap: break-word;
		}

		.table-head > thead > tr > th,
		.table-head > tbody > tr > th,
		.table-head > tfoot > tr > th,
		.table-head > thead > tr > td,
		.table-head > tbody > tr > td,
		.table-head > tfoot > tr > td {
			padding-left: 3px;
			padding-right: 3px;
			padding-bottom: 1px;
			padding-top: 1px;
			vertical-align: top;
			line-height: 14px;
			font-size: 12px;
		}
	</style>
@endsection	
@section('app.body')
		<table width="100%" class="table-head">
			<tr>
				<!-- <td><img width=152 height=55 src="{{ URL::to('images/logo.png') }}"></td> -->
				<td align="left"  style="width: 30mm" valign="top" style="margin-right: 0">
					<img width=100 height=52 src="{{ URL::to('images/logoBW2.png') }}" style="margin-top: 4px">
				</td>
				<td align="left" style="width: 80mm" valign="top">
					<p style="font-weight: bold; font-size: 14px">Toko Eceran dan Grosir</p>
					<p>Jl. Puring-Petanahan No. 3 Karangduwur</p>
					<p>kencanamulyakarangduwur@gmail.com</p>
					<p>(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
				</td>
				<td valign="top" style="width: 23mm">
					<p style="font-weight: bold; font-family: Arial; font-size: 14px; margin-bottom: 2px">INVOICE</p>
					<p> No. Transaksi </p>
					<p> Tanggal </p>
					<p> Kasir </p>
					<p> Pembeli </p>
				</td>
				<td valign="top" style="width: 1mm" style="padding: 0">
					<p style="font-weight: bold; font-family: Arial; font-size: 14px; margin-bottom: 2px; color: transparent;">I</p>
					<p> : </p>
					<p> : </p>
					<p> : </p>
					<p> : </p>
				</td>
				<td valign="top" style="width: 50mm" style="padding: 0">
					<p style="font-weight: bold; font-family: Arial; font-size: 14px; margin-bottom: 2px; color: transparent;">I</p>
					<p> 0001/TRAJ/01/11/2017 </p>
					<p> 01/11/2017 </p>
					<p> Sutinem </p>
					<p> Parsidi </p>
				</td>
			</tr>
		</table>

		<table width="100%" class="table table-bordered" style="margin-top: 10px">
			<thead>
				<tr>
					<th colspan="1">No</th>
					<th colspan="2">Kode Item</th>
					<th colspan="4">Nama Item</th>
					<th colspan="1">Jumlah</th>
					<th colspan="2">Harga</th>
					<th colspan="2">Total</th>
				</tr>
			</thead>
			<tbody id="data">
			</tbody>
		</table>
		<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
			<tbody>
					<td width="654mm" style="padding-top: 5px; text-transform: uppercase; border: 1px solid">
							<p>CATATAN :</p>
							<p>1. JANGAN LUPA BAYAR HUTANG TEPAT WAKTU :)</p>
					</td>
					<td width="300mm" align="center" style="padding-top: 65px; border-top:0; text-decoration: overline;">Pembeli (Nama & Stampel)</td>
					<td width="300mm" align="right"  style="padding-top: 65px; border-top:0; text-decoration: overline;">Penjual (Nama & Stampel)
				</tr>
			</tbody>
		</table>
	</div>
@endsection
@section('app.script')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {
		for(var i=0; i<15;i++){
			$('#data').append(`<tr>
						<th colspan="1">1</th>
						<td colspan="2">8886008101336</td>
						<td colspan="4">Indomie Goreng Special Duo</td>
						<td colspan="1">2 KRT 3 BKS</td>
						<td colspan="2" align="right">65.500</td>
						<td colspan="3" align="right">1.131.000</td>
					</tr>`);
		}
		$('#data').append(`<tr>
			<td colspan="7" rowspan="4" style="text-transform: uppercase;">
				<p style="font-weight: bold; padding-top: 5px">TERBILANG :</p>
				<p># {{ App\Util::terbilang(51250500)}} RUPIAH #</p>
			</td>
			<td colspan="3" style="font-weight: bold">Sub Total</td>
			<td colspan="3" align="right">1.131.000</td>
		</tr>
		<tr>
			<td colspan="3" style="font-weight: bold">Potongan</td>
			<td colspan="3" align="right">1.131.000</td>
		</tr>
		<tr>
			<td colspan="3" style="font-weight: bold">Uang Muka</td>
			<td colspan="3" align="right">1.131.000</td>
		</tr>
		<tr>
			<td colspan="3" style="font-weight: bold">Grand Total</td>
			<td colspan="3" align="right">1.131.000</td>
		</tr>`);
	});
</script>


@endsection
