<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>KENCANA MULYA > login</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
<style type="text/css">
body {color: #737373; font-size: 10px; font-family: verdana;}

textarea,input,select {
background-color: #FDFBFB;
border: 1px solid #BBBBBB;
padding: 2px;
margin: 1px;
font-size: 14px;
color: #808080;
}

a, a:link, a:visited, a:active { color: #AAAAAA; text-decoration: none; font-size: 10px; }
a:hover { border-bottom: 1px dotted #c1c1c1; color: #AAAAAA; }
img {border: none;}
td { font-size: 14px; color: #7A7A7A; }
</style>

</head>

<body>
$(if chap-id)
	<form name="sendin" action="$(link-login-only)" method="post">
		<input type="hidden" name="username" />
		<input type="hidden" name="password" />
		<input type="hidden" name="dst" value="$(link-orig)" />
		<input type="hidden" name="popup" value="true" />
	</form>
	
	<script type="text/javascript" src="/md5.js"></script>
	<script type="text/javascript">
	<!--
	    function doLogin() {
		document.sendin.username.value = document.login.username.value;
		document.sendin.password.value = hexMD5('$(chap-id)' + document.login.password.value + '$(chap-challenge)');
		document.sendin.submit();
		return false;
	    }
	//-->
	</script>
$(endif)

<!-- <div align="center">
<a href="$(link-login-only)?target=lv&amp;dst=$(link-orig-esc)">Latviski</a>
</div> -->

<table width="100%" style="margin-top: 10%;">
	<tr>
	<td align="center" valign="middle">
		<!-- <div class="notice" style="color: #c1c1c1; font-size: 9px">Please log on to use the internet hotspot service
			<br />
			$(if trial == 'yes')Free trial available, 
			<a style="color: #FF8080"href="$(link-login-only)?dst=$(link-orig-esc)&amp;username=T-$(mac-esc)">click here</a>.$(endif)
		</div>
		<br /> -->


		<div class="notice" style="color: #8C8585; font-size: 9px">
			Silahkan masukkan username dan password <br/>
			yang telah anda dapatkan untuk menikmati Voucher Internet<br/>
			
		</div><br />
		<table width="280" height="280" style="border: 1px solid #cccccc; padding: 0px;" cellpadding="0" cellspacing="0">
			<tr style="margin-bottom: 0; ">
				<td align="center" style="padding-top: 5px" >
					<h2 style="text-transform: uppercase;">Voucher Internet</h2>
					<h5 style="margin-top: -25px; color: red; text-transform: uppercase;">Akses Internet <span style="font-size: 18px">3X</span> Lebih Cepat</h5>
					$(if trial == 'yes')<p style="color: #8C8585; font-size: 9px; margin:0; padding: 0;">UNTUK UJI COBA <span style="font-weight: 900"> GRATIS </span> <a style="color: #FF8080"href="$(link-login-only)?dst=$(link-orig-esc)&amp;username=T-$(mac-esc)"> KLIK DISINI</a></p>$(endif)
				</td>
			</tr>
			<tr>
				<td align="center" valign="bottom" colspan="2">
					<form name="login" action="$(link-login-only)" method="post"
					    $(if chap-id) onSubmit="return doLogin()" $(endif)>
						<input type="hidden" name="dst" value="$(link-orig)" />
						<input type="hidden" name="popup" value="true" />
						
							<table width="100" style="background-color: #ffffff">
								<tr><td align="right">Username</td>
										<td><input style="width: 80px" name="username" type="text" value="$(username)"/></td>
								</tr>
								<tr><td align="right">Password</td>
										<td><input style="width: 80px" name="password" type="password"/></td>
								</tr>
								<tr><td>&nbsp;</td>
										<td><input type="submit" value="OK" /></td>
								</tr>
							</table>
					</form>
				</td>
			</tr>
			<tr><td align="center" ><img src="/img/logo.png" alt="mikrotik" width="70%" style="padding: 10px; margin-top: 15px; padding-bottom: 25px"/></td></tr>
		</table>
		<!-- <div class="notice" style="color: #8C8585; font-size: 9px; margin-top: 15px">Terima Kasih. <br />Saran dan Masukkan kirim ke <br /><strong>kencanamulyakarangduwur@gmail.com</strong></div><br /> -->
		<div class="notice" style="color: #8C8585; font-size: 9px; margin-top: 15px">Saran dan Masukkan kirim ke <br /><strong>kencanamulyakarangduwur@gmail.com</strong><br />Terima Kasih<br /></div><br />
	
	<!-- <br />
	<div style="color: #c1c1c1; font-size: 9px">Powered by MikroTik RouterOS</div> -->

	$(if error)<br /><div style="color: #FF8080; font-size: 9px">$(error)</div>$(endif)
	</td>
	</tr>
</table>

<script type="text/javascript">
<!--
  document.login.username.focus();
//-->
</script>
</body>
</html>
