@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }
        
        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }

        .header_grosir{
            font-size: 10px;
        }
    </style>
@endsection

@section('app.body')
    <table width="100%" class="table-head">
        <tr>
            <!-- <td><img width=152 height=55 src="{{ URL::to('images/logo.png') }}"></td> -->
            <td align="left"  style="width: 93mm" valign="top" style="margin-right: 0">
                <img width=173 height=63 src="{{ URL::to('images/logo1.png') }}" style="margin-top: 4px">
            </td>
            <td align="left" style="width: 100mm" valign="top">
                <p style="font-weight: bold; font-size: 12px; text-transform: uppercase;">Toko Eceran & Grosir</p>
                <p style="font-weight: bold; font-size: 14px; text-transform: uppercase;">KENCANA MULYA</p>
                <p>Jl. Puring-Petanahan No. 3 Karangduwur</p>
                <p>kencanamulyakarangduwur@gmail.com</p>
                <p>Telp. (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
            </td>
            <td valign="top" style="width: 40mm;">
                <p style="font-weight: bold; font-size: 14px; margin-bottom: 2px">INVOICE</p>
                <p class="header_grosir"> No. Transaksi </p>
                <p class="header_grosir"> Tanggal & Waktu </p>
                <p class="header_grosir"> Kasir </p>
                <p class="header_grosir"> Pembeli </p>
            </td>
            <td valign="top" style="width: 1mm" style="padding: 0">
                <p class="invisible" style="font-weight: bold; font-size: 14px; margin-bottom: 2px; color: white;">I</p>
                <p class="header_grosir"> : </p>
                <p class="header_grosir"> : </p>
                <p class="header_grosir"> : </p>
                <p class="header_grosir"> : </p>
            </td>
            <td valign="top" style="width: 60mm" style="padding: 0">
                <p class="invisible" style="font-weight: bold; font-size: 14px; margin-bottom: 2px; color: white;">I</p>
                <p class="header_grosir"> 0001/TRAJ/01/11/2017 </p>
                <p class="header_grosir"> 01/11/2017 09:30:11</p>
                <p class="header_grosir"> Sutinem </p>
                <p class="header_grosir"> Parsidi </p>
            </td>
        </tr>
    </table>

    <table width="100%" class="table table-bordered" style="margin-top: 10px">
        <thead>
            <tr>
                <th width="4%" style="text-align: center;">No</th>
                <th width="16%" style="text-align: center;">Kode Item</th>
                <th width="37%" style="text-align: center;">Nama Item</th>
                <th width="12%" style="text-align: center;">Jumlah</th>
                <th width="14%" style="text-align: center;">Harga</th>
                <th width="16%" style="text-align: center;">Total</th>
            </tr>
        </thead>
        <tbody id="data">
        </tbody>
    </table>
    <table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
        <tbody>
                <td width="654mm" style="padding-top: 5px; border: 1px solid">
                        <p style="font-size: 10px;">CATATAN :</p>
                        <p style="font-size: 10px;">NOTES</p>
                        <p style="font-size: 10px; margin-top: 30px;">KMK Management</p>
                </td>
                <td width="300mm" align="center" style="border-top:0; text-decoration: overline;">
                    <p style="margin-top: 65px; font-size: 11px;">Pembeli (Nama & Stampel)</p>
                </td>
                <td width="300mm" align="right" style="border-top:0; text-decoration: overline;">
                    <p style="margin-top: 65px; font-size: 11px;">Penjual (Nama & Stampel)</p>
                </td>
            </tr>
        </tbody>
    </table>
@endsection

@section('app.script')
    <script type="text/javascript" charset="utf-8" async defer>
        $(document).ready(function() {
            var table_head = ''+
                `<table width="100%" class="table-head">
                    <tr>
                        <!-- <td><img width=152 height=55 src="{{ URL::to('images/logo.png') }}"></td> -->
                        <td align="left"  style="width: 93mm" valign="top" style="margin-right: 0">
                            <img width=173 height=63 src="{{ URL::to('images/logo1.png') }}" style="margin-top: 4px">
                        </td>
                        <td align="left" style="width: 100mm" valign="top">
                            <p style="font-weight: bold; font-size: 12px; text-transform: uppercase;">Toko Eceran & Grosir</p>
                            <p style="font-weight: bold; font-size: 14px; text-transform: uppercase;">KENCANA MULYA</p>
                            <p>Jl. Puring-Petanahan No. 3 Karangduwur</p>
                            <p>kencanamulyakarangduwur@gmail.com</p>
                            <p>Telp. (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
                        </td>
                        <td valign="top" style="width: 40mm;">
                            <p style="font-weight: bold; font-size: 14px; margin-bottom: 2px">INVOICE</p>
                            <p class="header_grosir"> No. Transaksi </p>
                            <p class="header_grosir"> Tanggal & Waktu </p>
                            <p class="header_grosir"> Kasir </p>
                            <p class="header_grosir"> Pembeli </p>
                        </td>
                        <td valign="top" style="width: 1mm" style="padding: 0">
                            <p class="invisible" style="font-weight: bold; font-size: 14px; margin-bottom: 2px; color: white;">I</p>
                            <p class="header_grosir"> : </p>
                            <p class="header_grosir"> : </p>
                            <p class="header_grosir"> : </p>
                            <p class="header_grosir"> : </p>
                        </td>
                        <td valign="top" style="width: 60mm" style="padding: 0">
                            <p class="invisible" style="font-weight: bold; font-size: 14px; margin-bottom: 2px; color: white;">I</p>
                            <p class="header_grosir"> 0001/TRAJ/01/11/2017 </p>
                            <p class="header_grosir"> 01/11/2017 09:30:11</p>
                            <p class="header_grosir"> Sutinem </p>
                            <p class="header_grosir"> Parsidi </p>
                        </td>
                    </tr>
                </table>`;

            var table_body = ''+
                `<table width="100%" class="table table-bordered" style="margin-top: 10px">
                    <thead>
                        <tr>
                            <th width="4%" style="text-align: center;">No</th>
                            <th width="16%" style="text-align: center;">Kode Item</th>
                            <th width="37%" style="text-align: center;">Nama Item</th>
                            <th width="12%" style="text-align: center;">Jumlah</th>
                            <th width="14%" style="text-align: center;">Harga</th>
                            <th width="16%" style="text-align: center;">Total</th>
                        </tr>
                    </thead>
                    <tbody id="data">
                    </tbody>
                </table>`;

            var tr_sum = ''+
                `<tr>
                    <td colspan="3" rowspan="4" style="text-transform: uppercase;">
                        <p style="font-weight: bold; padding-top: 5px; font-size:10px">TERBILANG :</p>
                        <p style="font-size:10px"># {{ App\Util::terbilang(51250500)}} RUPIAH #</p>
                    </td>
                    <td colspan="2" style="font-weight: bold">Sub Total</td>
                    <td  align="right">1.131.000</td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold">Potongan Harga</td>
                    <td  align="right">(1.131.000)</td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold">Deposito Pelanggan</td>
                    <td  align="right">(1.131.000)</td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold">Grand Total</td>
                    <td  align="right">1.131.000</td>
                </tr>`;

            var table_foot = ''+
                `<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
                    <tbody>
                            <td width="654mm" style="padding-top: 5px; border: 1px solid">
                                    <p style="font-size: 10px;">CATATAN :</p>
                                    <p style="font-size: 10px;">NOTES</p>
                                    <p style="font-size: 10px; margin-top: 30px;">KMK Management</p>
                            </td>
                            <td width="300mm" align="center" style="border-top:0; text-decoration: overline;">
                                <p style="margin-top: 65px; font-size: 11px;">Pembeli (Nama & Stampel)</p>
                            </td>
                            <td width="300mm" align="right" style="border-top:0; text-decoration: overline;">
                                <p style="margin-top: 65px; font-size: 11px;">Penjual (Nama & Stampel)</p>
                            </td>
                        </tr>
                    </tbody>
                </table>`;

            for (var i = 0; i < 25; i++) {
                $('#data').append(`
                        <tr>
                            <td>`+ (i+1) +`</td>
                            <td >8886008101336</td>
                            <td >Indomie Goreng Special Duo</td>
                            <td >2 KRT 3 BKS</td>
                            <td align="right">Rp1.150.500,-</td>
                            <td align="right">Rp1.000.150.500,-</td>
                        </tr>`);
            }

            $('#data').append();
        });
    </script>

@endsection
