 	<html>

	<head>
		<style type="text/css">
			@font-face {
			    font-family: "Arial";
			    src: url("{{ asset('fonts/Arial.ttf') }}");
			}

			@font-face {
			    font-family: "ClarendonBT";
			    src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
			}

			p {
				margin-top: 0;
				margin-bottom: 0;
				font-size: 12px;
			}

			@page WordSection1
			{
				size:24.13cm 13.97cm;
				margin:0;
				margin-left: -20px;
				margin-right: 0;
				margin-bottom: 0;
				margin-top: -10px;
			}

			div.WordSection1
			{ 
				page:WordSection1;
			    font-family: "Arial";
			}
			
			.table {
			    border-collapse: collapse !important;
			}

			.table-bordered th,
			.table-bordered td {
			    border: 1px solid #000 !important;
			}

			.table {
			 	width: 100%;
				max-width: 100%;
				margin-bottom: 10px;
			}

			.table > thead > tr > th,
			.table > tbody > tr > th,
			.table > tfoot > tr > th,
			.table > thead > tr > td,
			.table > tbody > tr > td,
			.table > tfoot > tr > td {
				padding-left: 5px;
				padding-right: 5px;
				padding-bottom: 2px;
				padding-top: 2px;
				line-height: 1.42857143;
				vertical-align: top;
				border-top: 1px solid #000;
				font-size: 12px;
			}

		</style>
	</head>
	<body>
		<div class=WordSection1>
			<table>
				<tr>
					<!-- <td><img width=152 height=55 src="{{ URL::to('images/logo.png') }}"></td> -->
					<td align="left" colspan="1" valign="top" style="margin-right: 0">
						<img width=113 height=41 src="{{ URL::to('images/logo1.png') }}" style="margin-top: 4px">
					</td>
					<td align="left" colspan="7" valign="top">
						<p style="font-weight: bold; font-family: ClarendonBT; font-size: 14px">KENCANA MULYA</p>
						<p style="font-weight: bold;">Toko Grosir dan Eceran</p>
						<p>Karangduwur - Petanahan</p>
						<p>Jl. Puring-Petanahan No. 3 Karangduwur</p>
						<p>kencanamulyakarangduwur@gmail.com</p>
						<p>(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
					</td>
					<td valign="top" colspan="1" style="padding-left: 85px">
						<p style="font-weight: bold; font-family: ClarendonBT; font-size: 14px; margin-bottom: 2px color: #fff">INVOICE</p>
						<p> No. Transaksi </p>
						<p> Tanggal </p>
						<p> Kasir </p>
						<p> Pelanggan </p>
					</td>
					<td valign="top" colspan="1">
						<p style="font-weight: bold; font-family: ClarendonBT; font-size: 14px; margin-bottom: 2px; color: #fff">I</p>
						<p> : </p>
						<p> : </p>
						<p> : </p>
						<p> : </p>
					</td>
					<td valign="top" colspan="2">
						<p style="font-weight: bold; font-family: ClarendonBT; font-size: 14px; margin-bottom: 2px; color: #fff">INVOICE</p>
						<p> 0001/TRAJ/01/11/2017 </p>
						<p> 01/11/2017 </p>
						<p> Sutinem </p>
						<p> Parsidi </p>
					</td>
				</tr>
			</table>

			<table width="100%" class="table table-bordered" style="margin-top: 20px">
				<thead>
					<tr align="left">
						<th colspan="2">Kode Item</th>
						<th colspan="4">Nama Item</th>
						<th colspan="1">Qty</th>
						<th colspan="2" align="right">Harga</th>
						<th colspan="3" align="right">Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2">8886008101336</td>
						<td colspan="4">Aqua 300ml</td>
						<td colspan="1">2 KRT</td>
						<td colspan="2" align="right">Rp65.500</td>
						<td colspan="3" align="right">Rp131.000</td>
					</tr>
					<tr>
						<td colspan="2">8886008101336</td>
						<td colspan="4">Indomie Goreng Special Duo</td>
						<td colspan="1">2 KRT</td>
						<td colspan="2" align="right">65.500</td>
						<td colspan="3" align="right">1.131.000</td>
					</tr>
					<tr>
						<td colspan="6" rowspan="2"></td>
						<td colspan="3">Grand Total</td>
						<td colspan="3" align="right">1.131.000</td>
					</tr>
					<tr>
						<td colspan="3">Potongan</td>
						<td colspan="3" align="right">1.131.000</td>
					</tr>
				</tbody>
			</table>


		</div>
	</body>
	</html>
