<html>

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=Generator content="Microsoft Word 15 (filtered)">
	<style type="text/css">
		@font-face {
		    font-family: "Arial";
		    src: url("{{ asset('fonts/Arial.ttf') }}");
		}
		p {
			margin-top: 0;
			margin-bottom: 0;
		}
		@page WordSection1
		{
			size:684.1pt 396.05pt;
			margin:0;
		}
		div.WordSection1
		{ 
			page:WordSection1;
		    font-family: "Arial";
		    line-height: 100%;
		    font-size: 16pt;
		}
		
		.table {
		    border-collapse: collapse !important;
		}

		.table-bordered th,
		.table-bordered td {
		    border: 1px solid #000 !important;
		}

		.table {
		 	width: 100%;
			max-width: 100%;
			margin-bottom: 10px;
		}

		.table > thead > tr > th,
		.table > tbody > tr > th,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > tbody > tr > td,
		.table > tfoot > tr > td {
			padding-left: 5px;
			padding-right: 5px;
			padding-bottom: 2px;
			padding-top: 2px;
			line-height: 1.42857143;
			vertical-align: top;
			border-top: 1px solid #ddd;
		}


	</style>
</head>
<body>
	<div class=WordSection1>
		<table width="100%">
			<tr>
				<!-- <td><img width=152 height=55 src="{{ URL::to('images/logo.png') }}"></td> -->
				<td align="left" colspan="6">
					<p><img width=113 height=41 src="{{ URL::to('images/logo.png') }}"></p>
					<p style="font-weight: bold; margin-top: 5px">KENCANA MULYA</p>
					<p style="font-size: 12pt">Karangduwur - Petanahan</p>
					<p style="font-size: 12pt">Jl. Puring-Petanahan No. 3 Karangduwur</p>
					<p style="font-size: 12pt">kencanamulyakarangduwur@gmail.com</p>
					<p style="font-size: 12pt">(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
				</td>
				<td valign="top" colspan="2" style="padding-left: 85px">
					<p> No. Transaksi </p>
					<p> Tanggal </p>
					<p> Kasir </p>
					<p> Pelanggan </p>
				</td>
				<td valign="top" colspan="1">
					<p> : </p>
					<p> : </p>
					<p> : </p>
					<p> : </p>
				</td>
				<td valign="top" colspan="3">
					<p> 0001/TRAJ/01/11/2017 </p>
					<p> 01/11/2017 </p>
					<p> Sutinem </p>
					<p> Parsidi </p>
				</td>
			</tr>
		</table>

		<table width="100%" class="table">
			<thead>
				<tr align="left">
					<th colspan="2" class="table-bordered">Kode Item</th>
					<th colspan="4" class="table-bordered">Nama Item</th>
					<th colspan="1" class="table-bordered">Qty</th>
					<th colspan="2" class="table-bordered">Harga</th>
					<th colspan="3" align="right" class="table-bordered">Subtotal</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2" class="table-bordered">8886008101336</td>
					<td colspan="4" class="table-bordered">Aqua 300ml</td>
					<td colspan="1" class="table-bordered">2 KRT</td>
					<td colspan="2" align="right" class="table-bordered">Rp65.500</td>
					<td colspan="3" align="right" class="table-bordered">Rp131.000</td>
				</tr>
				<tr>
					<td colspan="2" class="table-bordered">8886008101336</td>
					<td colspan="4" class="table-bordered">Indomie Goreng Special Duo</td>
					<td colspan="1" class="table-bordered">2 KRT</td>
					<td colspan="2" class="table-bordered" align="right">65.500</td>
					<td colspan="3" class="table-bordered" align="right">1.131.000</td>
				</tr>
			</tbody>
		</table>


	</div>
</body>
</html>
