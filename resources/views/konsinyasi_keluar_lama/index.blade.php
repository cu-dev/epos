@extends('layouts.admin')

@section('title')
    <title>EPOS | Konsinyasi Keluar</title>
@endsection

@section('style')
    <style media="screen">
    	#btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Konsinyasi Keluar</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemKeluar">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Item</th>
							<th>Nama Suplier</th>
							<th>Jumlah</th>
							<th>Keterangan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($konsinyasi_keluars as $num => $konsinyasi)
						<tr id="{{$konsinyasi->id}}">
							<td>{{ $num+1 }}</td>
							<td>{{ $konsinyasi->item_masuk->item->nama }}</td>
							<td>{{ $konsinyasi->item_masuk->suplier->nama }}</td>
							<td>{{ $konsinyasi->jumlah }}</td>
							<td>{{ $konsinyasi->keterangan }}</td>
							<td>
								<a href="{{ url('konsinyasi-keluar/show/'.$konsinyasi->id) }}" class="btn btn-xs btn-info" id="btnDetail">
									<i class="fa fa-eye"></i> Detail
								</a>
								<a href="{{ url('konsinyasi-keluar/edit/'.$konsinyasi->id) }}" class="btn btn-xs btn-primary" id="btnUbah">
									<i class="fa fa-edit"></i> Ubah
								</a>
								<!-- <button class="btn btn-xs btn-danger" id="btnHapus">
									<i class="fa fa-trash"></i> Hapus
								</button> -->
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Keluar berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Keluar gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Keluar berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Keluar gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi Keluar berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi Keluar gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItemKeluar').DataTable();

		$(document).on('click', '#btnHapus', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama_item = $tr.find('td').first().next().text();
			var nama_suplier = $tr.find('td').first().next().next().text();
			$('input[name="id"]').val(id);

			swal({
				title: 'Hapus?',
				text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#009688',
				cancelButtonColor: '#ff5252',
				confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
				cancelButtonText: '<i class="fa fa-close"></i> Batal'
			}, function(isConfirm) {
				if (isConfirm) {
					// Confirmed
					$('#formHapusContainer').find('form').attr('action', '{{ url("item-keluar") }}' + '/' + id);
					$('#formHapusContainer').find('form').submit();
				} else {
					// Canceled
				}
			});
		});
	</script>
@endsection
