@extends('layouts.admin')

@section('title')
    <title>EPOS | Setoran Buka</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="col-md-4 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 id="formSimpanTitle">Tambah Setoran Buka</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content" id="formSimpanContainer">
            <form method="post" action="{{ url('setoran-buka') }}" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="control-label">Nominal</label>
                    <input class="form-control angka" type="text" name="nominal_show" id="nominal">
                    <input type="hidden" name="nominal">
                </div>
                <div class="form-group">
                    <label class="control-label">Kepada</label>
                    <select class="form-control select2_single" name="user">
                        <option value="">--Pilih Kasir--</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">
                                {{ $user->nama }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="margin-bottom: 0;">
                    <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                        <i class="fa fa-save"></i> <span>Tambah</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- kolom kanan -->
<div class="col-md-8 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Data Setoran Buka</h2>
            <p class="pull-right">Uang di Laci Grosir : {{ \App\Util::ewon($laci_grosir) }}</p>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranKasir">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Nominal</th>
                        <th>Kasir</th>
                        <th>Operator</th>
                        {{-- <th>Aksi</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($setorans as $i => $setoran)
                    <tr id="{{ $setoran->id }}">
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $setoran->created_at->format('H:i') }}</td>
                        <td class="text-right">{{ \App\Util::ewon($setoran->nominal) }}</td>
                        <td>{{ $setoran->user->nama }}</td>
                        <td>{{ $setoran->user1->nama }}</td>
                        {{-- <td>
                            <button class="btn btn-xs btn-primary" id="btnUbah">
                                <i class="fa fa-edit"></i> Ubah
                            </button>
                            <button class="btn btn-xs btn-danger" id="btnHapus">
                                <i class="fa fa-trash"></i> Hapus
                            </button>
                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Buka berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Buka berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Buka berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableSetoranKasir').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2({
                width: '100%'
                // allowClear: true
            });
        });

        $(document).on('keyup', '#nominal', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci_grosir }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpan').prop('disabled', false);
            } else {
                $('#btnSimpan').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });
    </script>
@endsection
