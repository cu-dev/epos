@extends('layouts.admin')

@section('title')
	<title>EPOS | Batas Syarat Retur</title>
@endsection

@section('style')
	<style media="screen">
		.btnSimpan {
			margin: 0;
		}
		.lead {
			margin-bottom: 10px;
		}
	</style>
@endsection

@section('content')
<!-- <div class="row"> -->
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Batas Syarat Retur</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row" id="formSimpanContainer">
					<form class="form-horizontal" action="{{url('/rule_retur')}}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group col-xs-9">
							<input type="text" name="jumlah" class="form-control" required="" placeholder="3 hari">
						</div>
						<div class="form-group col-xs-3">
							<button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Ubah</span>
							</button>
						</div>
					</form>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h2>Batas Syarat Retur Saat Ini adalah {{ $rule->syarat }} hari</h2>
						<h2>Operator : {{ $rule->user->nama }}</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Batas Syarat Retur Saat Ini</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row" id="formSimpanContainer">
					<div class="col-xs-12">
							<p class="lead">{{ $rule->syarat }} hari</p>
					</div>
					{{-- s --}}
				</div>
			</div>
		</div>
	</div> -->
<!-- </div> -->
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Money Limit berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Money Limit gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Batas Retur berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Money Limit gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Money Limit berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Money Limit gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif
@endsection

