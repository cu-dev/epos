@extends('layouts.admin')

@section('title')
	<title>EPOS | Akun</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Akun</h2>
				{{-- <a href="{{ url('akun/create') }}" class="btn btn-sm btn-success pull-right" id="btnUbah">
					<i class="fa fa-plus"></i> Tambah
				</a> --}}
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-bordered table-hover table-striped" style="margin-bottom: 0;" id="tableJurnal">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Akun</th>
							<th>Rekening</th>
							<th>Debit</th>
							<th>Kredit</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($akuns as $i => $akun)
							<tr>
								<td style="text-align: center;">{{ $i + 1 }}</td>
								<td style="text-align: center;">{{ $akun->kode }}</td>
								@if ($akun->kode == \App\Akun::Aset ||
									$akun->kode == \App\Akun::AsetLancar ||
									$akun->kode == \App\Akun::AsetTetap ||
									$akun->kode == \App\Akun::Kewajiban ||
									$akun->kode == \App\Akun::HutangJangkaPendek ||
									$akun->kode == \App\Akun::HutangJangkaPanjang ||
									$akun->kode == \App\Akun::Modal ||
									$akun->kode == \App\Akun::Pendapatan ||
									$akun->kode == \App\Akun::LabaRugi ||
									$akun->kode == \App\Akun::Beban
								)
									<td><strong>{{ $akun->nama }}</strong></td>
								@else
									<td style="padding-left: 30px">{{ $akun->nama }}</td>
								@endif

								<td style="text-align: right;">
									@if($akun->debet < 0)
										({{ \App\Util::duit($akun->debet * -1) }})
									@else
										{{ \App\Util::duit($akun->debet) }}
									@endif
								</td>
								<td style="text-align: right;">
									@if($akun->kredit < 0)
										({{ \App\Util::duit($akun->kredit * -1) }})
									@else
										{{ \App\Util::duit($akun->kredit) }}
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Transaksi Pembelian berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Transaksi Pembelian gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJurnal').DataTable();

		$(document).ready(function() {
			$('#date_picker').daterangepicker(null, function(start, end, label) {
			  var mulai = (start.toISOString()).substring(0,10);
			  console.log(mulai);
			  var akhir = (end.toISOString()).substring(0,10);
			  console.log(akhir);

			  $('#formPilihan').find('input[name="awal"]').val(mulai);
			  $('#formPilihan').find('input[name="akhir"]').val(akhir);
			});
		  });


		$(document).on('click', '#btnHapus', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama_item = $tr.find('td').first().next().text();
			var nama_suplier = $tr.find('td').first().next().next().text();
			$('input[name="id"]').val(id);

			swal({
				title: 'Hapus?',
				text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#009688',
				cancelButtonColor: '#ff5252',
				confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
				cancelButtonText: '<i class="fa fa-close"></i> Batal'
			}, function(isConfirm) {
				if (isConfirm) {
					// Confirmed
					$('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-pembelian") }}' + '/' + id);
					$('#formHapusContainer').find('form').submit();
				} else {
					// Canceled
				}
			});
		});
	</script>
@endsection
