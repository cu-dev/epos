@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Item Keluar</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnKembali {
    		margin-bottom: 0;
    	}
    </style>
@endsection

@section('content')

<div class="col-md-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Detail Item Keluar</h2>
            <a href="{{ url('item-keluar/'.$item_keluar->id.'/edit') }}" class="btn btn-sm btn-primary pull-right" id="btnUbah">
                <i class="fa fa-edit"></i> Ubah
            </a>
            <a href="{{ url('item-keluar') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                <i class="fa fa-long-arrow-left"></i>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <section class="content invoice">
                <div class="row">
                        <div class="col-xs-6">
                            <p class="lead">{{ $item_keluar->item_masuk->item->nama }} </p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th style="width:50%">Kode Item</th>
                                            <td>{{ $item_keluar->item_masuk->item->kode }}</td>
                                        </tr>
                                        <tr>
                                            <th>Nama Item</th>
                                            <td>{{ $item_keluar->item_masuk->item->nama }}</td>
                                        </tr>
                                        <tr>
                                            <th>Nama Suplier</th>
                                            <td>{{ $item_keluar->item_masuk->suplier->nama }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jumlah</th>
                                            <td>{{ $item_keluar->jumlah }}</td>
                                        </tr>
                                        <tr>
                                            <th>Keterangan</th>
                                            <td>{{ $item_keluar->keterangan }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <p class="lead pull-right">{{ $item_keluar->created_at }} </p>
                        </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            var url = "{{ url('item-keluar') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });
    </script>
@endsection
