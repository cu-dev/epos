@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambahRetur, #btnLihatRetur, #btnSesuaikanCek, #btnSesuaikanBG, #btnKembali {
            margin-bottom: 0;
        }
        .no-border {
            border: none;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Transaksi Pembelian</h2>
                <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/create') }}" id="btnTambahRetur" data-toggle="tooltip" data-placement="top" title="Retur" class="btn btn-sm btn-success pull-right" style="margin-right: 0;">
                    <i class="fa fa-mail-reply"></i>
                </a>
                <!-- <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" id="btnLihatRetur" data-toggle="tooltip" data-placement="top" title="Lihat Retur" class="btn btn-sm btn-warning pull-right">
                    <i class="fa fa-sign-in"></i>
                </a> -->
                @if($transaksi_pembelian->status_hutang > 0)
                    <a href="{{ url('hutang/show/'.$transaksi_pembelian->id) }}" data-toggle="tooltip" data-placement="top" title="Bayar Hutang" class="btn btn-sm btn-primary pull-right">
                        <i class="fa fa-money"></i>
                    </a>
                @endif
                @if(in_array(Auth::user()->level_id, [1, 2]))
                    {{-- <a  href="{{ url('transaksi-pembelian/edit/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-warning" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Ubah Transaksi Pembelian">
                        <i class="fa fa-edit"></i>
                    </a> --}}
                    <a  href="{{ url('transaksi-pembelian/create/'.$transaksi_pembelian->id) }}" class="btn btn-sm btn-Sesuai pull-right" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Penyesuaian Transaksi Pembelian">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a  href="{{ url('transaksi-pembelian/edit/'.$transaksi_pembelian->id) }}" class="btn btn-sm btn-warning pull-right" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Ubah Transaksi Pembelian">
                        <i class="fa fa-edit"></i>
                    </a>
                @endif
                @if($transaksi_pembelian->aktif_bg == 1)
                    <a id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right">
                        <!-- <i class="fa fa-money"></i> -->BG
                    </a>
                @endif
                @if($transaksi_pembelian->aktif_cek == 1)
                    <a id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right">
                        <!-- <i class="fa fa-money"></i> -->CEK
                    </a>
                @endif
                <a href="{{ url('transaksi-pembelian') }}" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_pembelian->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Kode Faktur</th>
                                    <td style="width: 60%;">
                                        @if($transaksi_pembelian->nota != NULL)
                                        {{ $transaksi_pembelian->nota }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Pemasok</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Penjual</th>
                                    @if ($transaksi_pembelian->seller_id != null)
                                    <td>{{ $transaksi_pembelian->seller->nama }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Tanggal Pembelian</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->created_at->format('d-m-Y') }}</td>
                                </tr>
                                @if($transaksi_pembelian->jatuh_tempo != NULL)
                                    <tr>
                                        <th>Tanggal Jatuh Tempo</th>
                                        <td style="width: 60%;" class="tanggal">{{ $transaksi_pembelian->jatuh_tempo }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        @if(in_array(Auth::user()->level_id, [1, 2]) && $transaksi_pembelian->jatuh_tempo != NULL)
                        <form action="{{url('transaksi-pembelian/'.$transaksi_pembelian->id.'/jatuh-tempo')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="_method" value="post" />
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="form-group col-sm-8 col-xs-8">
                                    <label class="control-label">Ubah Jatuh Tempo</label>
                                    <input type="hidden" name="jatuh_tempo_baru_" value="{{$transaksi_pembelian->jatuh_tempo}}" />
                                    <input type="text" id="jatuh_tempo_baru" name="jatuh_tempo_baru" class="form-control jatuh-tempo tanggal-putih" placeholder="Edit Jatuh Tempo" value="{{$transaksi_pembelian->jatuh_tempo}}" readonly="" />
                                </div>
                                <div class="form-group col-sm-4 col-xs-4">
                                    <label class="control-label invisible">Ubah</label>
                                    <button type="submit" class="btn btn-success btn-block">
                                        <i class="fa fa-save"></i> Ubah
                                    </button>
                                </div>
                            </div>
                        </form>
                        @endif

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Rincian</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Sub Total</th>
                                    @if ($transaksi_pembelian->harga_total != null && $transaksi_pembelian->harga_total > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->ppn_total) }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Potongan Pembelian</th>
                                    @if ($transaksi_pembelian->potongan_pembelian != null && $transaksi_pembelian->potongan_pembelian > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->potongan_pembelian) }}</td>
                                    @else
                                    <td class="text-right">-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Ongkos Kirim</th>
                                    @if ($transaksi_pembelian->ongkos_kirim != null && $transaksi_pembelian->ongkos_kirim > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ongkos_kirim) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                @if ($transaksi_pembelian->ppn_total != null && $transaksi_pembelian->ppn_total > 0)
                                    <tr>
                                        <th>PPN Total</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ppn_total) }}</td>
                                    </tr>
                                @endif
                                

                                <tr>
                                    @if ($transaksi_pembelian->utang > 0)
                                        <th>Pengurang Tagihan</th>
                                    @else
                                        <th>Jumlah Bayar</th>
                                    @endif
                                    @if ($transaksi_pembelian->jumlah_bayar != null && $transaksi_pembelian->jumlah_bayar > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->jumlah_bayar) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                @if ($transaksi_pembelian->utang > 0)
                                    <tr>
                                        {{-- <th>Jumlah Bayar Saat Ini</th> --}}
                                        <th>Pengurang Tagihan Saat Ini</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->potongan_pembelian - $transaksi_pembelian->sisa_utang
                                         + $transaksi_pembelian->ongkos_kirim) }}</td>
                                    </tr>
                                @endif

                                @if ($transaksi_pembelian->sisa_utang > 0)
                                    <tr>
                                        {{-- <th>Kurang</th> --}}
                                        <th>Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->sisa_utang) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($transaksi_pembelian->utang != null && $transaksi_pembelian->utang > 0)
                                <tr>
                                    <th>Hutang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->utang) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_tunai != null && $transaksi_pembelian->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_transfer != null && $transaksi_pembelian->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_transfer) }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_transfers->nama_bank }} [{{ $transaksi_pembelian->bank_transfers->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_kartu != null && $transaksi_pembelian->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_kartu) }}</td>
                                </tr>
                                <tr>
                                    <th>Nomor Transaksi Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_kartu }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-right" style="width: 60%;text-transform: capitalize;">{{ $transaksi_pembelian->jenis_kartu }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_kartus->nama_bank }} [{{ $transaksi_pembelian->bank_kartus->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_cek != null && $transaksi_pembelian->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">
                                    @if($transaksi_pembelian->aktif_cek == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_cek != null)
                                    <tr>
                                        <th>Bank Cek</th>
                                        <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_ceks->nama_bank }} [{{ $transaksi_pembelian->bank_ceks->no_rekening }}]</td>
                                    </tr>
                                @endif

                                @if ($transaksi_pembelian->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_bg != null && $transaksi_pembelian->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">@if($transaksi_pembelian->aktif_bg == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_bg != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_bgs->nama_bank }} [{{ $transaksi_pembelian->bank_bgs->no_rekening }}]</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>

                        @if($cashback != null)
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="text-align: left">Kembalian Uang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($cashback->nominal_tunai != null && $cashback->nominal_tunai > 0)
                                    <tr>
                                        <th>Nominal Tunai</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($cashback->nominal_tunai) }}</td>
                                    </tr>
                                    @endif

                                    @if ($cashback->no_transfer != null)
                                    <tr>
                                        <th>No Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ $cashback->no_transfer }}</td>
                                    </tr>
                                    @endif

                                    @if ($cashback->nominal_transfer != null && $cashback->nominal_transfer > 0)
                                    <tr>
                                        <th>Nominal Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($cashback->nominal_transfer) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bank Transfer</th>
                                        <td class="text-right" style="width: 60%;">{{ $cashback->bank_transfers->nama_bank }} [{{ $cashback->bank_transfers->no_rekening }}]</td>
                                    </tr>
                                    @endif

                                    @if ($cashback->nominal_kartu != null && $cashback->nominal_kartu > 0)
                                    <tr>
                                        <th>Nominal Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($cashback->nominal_kartu) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Transaksi Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ $cashback->no_kartu }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kartu</th>
                                        <td class="text-right" style="width: 60%; text-transform: capitalize;">{{ $cashback->jenis_kartu }}</td>
                                    </tr>
                                    <tr>
                                        <th>Bank Kartu</th>
                                        <td class="text-right" style="width: 60%;">{{ $cashback->bank_kartus->nama_bank }} [{{ $cashback->bank_kartus->no_rekening }}]</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif

                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama Item</th>
                                    <th class="text-center">Jumlah</th>
                                    <!-- <th class="text-right">HPP</th>
                                    <th class="text-right">PPN</th>
                                    <th class="text-right">Harga</th> -->
                                    <th class="text-center">Total</th>
                                    @if (in_array(Auth::user()->level_id, [1,2]))
                                        <th class="text-center" width="25px">Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_pembelian as $i => $relasi)
                                {{-- <span style="display: none"> {{ $a = $i }} </span> --}}
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item->kode }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="text-left jumlah">
                                        {{-- @foreach($hasil[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        {{-- @foreach($hasil[$i] as $x => $jumlah)
                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                            @if ($x != count($hasil[$a]) - 1)
                                            <br>
                                            @endif
                                        @endforeach --}}
                                        {{ $relasi->jumlah }}
                                    </td>
                                    <!-- <td class="text-right">{{ \App\Util::duit($relasi->harga) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 10) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga * 1.1) }}</td> -->
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
                                    @if (in_array(Auth::user()->level_id, [1,2]))
                                        <td class="text-center"><a href="{{ url('item/show/'.$relasi->item->kode_barang) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Item">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="x_title">
                            <h2>Stok</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-stok">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Kadaluarsa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stoks as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item->kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="jumlah">
                                        {{-- @foreach($hasil_stoks[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        {{-- @if($hasil_stoks[$a])
                                            @foreach($hasil_stoks[$a] as $x => $jumlah)
                                                {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                                @if ($x != count($hasil_stoks[$a]) - 1)
                                                <br>
                                                @endif
                                            @endforeach
                                        @endif --}}
                                        {{ $relasi->jumlah_beli }}
                                    </td>
                                    <td>{{ \App\Util::date($relasi->kadaluarsa) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'cek')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran Cek Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'bg')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran BG Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'jatuh_tempo')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Jatuh Tempo Berhasil Diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah1')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian Berhasil Diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">

        var transaksi_pembelian = '{{ json_encode($transaksi_pembelian) }}';
        transaksi_pembelian = transaksi_pembelian.replace(/&quot;/g, '"');
        transaksi_pembelian = JSON.parse(transaksi_pembelian);

        var relasi_transaksi_pembelian = '{{ json_encode($relasi_transaksi_pembelian) }}';
        relasi_transaksi_pembelian = relasi_transaksi_pembelian.replace(/&quot;/g, '"');
        relasi_transaksi_pembelian = JSON.parse(relasi_transaksi_pembelian);

        var items = transaksi_pembelian.items;
        var trapem_id = '';

        $(document).ready(function() {
            var url = "{{ url('transaksi-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            trapem_id = transaksi_pembelian.id;

            $('.tanggal').each(function(index, el) {
                var tanggal = ymd2dmy($(el).text());
            });

            $('.jatuh-tempo').each(function(index, el) {
                var tanggal = ymd2dmy($(el).val());
                // console.log(tanggal);
                $(el).val(tanggal);
            });

            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);

                var satuan_item = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }
                // console.log(satuan_item, jumlah);

                var jumlah1 = '';
                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        // if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                        //     jumlah2 += jumlah_jual;
                        //     jumlah2 += ' ';
                        //     jumlah2 += satuan.kode;
                        //     temp_jumlah = temp_jumlah % satuan.konversi;
                        // }
                        if (jumlah_jual > 0) {
                            if (jumlah1 == '') {
                                jumlah1 += jumlah_jual;
                                jumlah1 += ' ';
                                jumlah1 += satuan.kode;
                                temp_jumlah = temp_jumlah % satuan.konversi;
                            } else {
                                if (temp_jumlah % satuan.konversi > 0) {
                                    continue;
                                } else {
                                    jumlah2 += jumlah_jual;
                                    jumlah2 += ' ';
                                    jumlah2 += satuan.kode;
                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                }
                            }
                        }
                    }
                }
                // console.log(jumlah2);

                $(el).text(jumlah1 + ' ' + jumlah2);
                // if (jumlah2 == '') $(el).text(jumlah);
            });

            $('#tabel-item').DataTable();
            $('#tabel-stok').DataTable();
        });

        $(document).on('focus', '#jatuh_tempo_baru', function(event) {
            event.preventDefault();

            $('#jatuh_tempo_baru').daterangepicker({
                    autoApply: true,
                    showDropdowns: true,
                    calender_style: "picker_2",
                    format: 'DD-MM-YYYY',
                    locale: {
                        "applyLabel": "Pilih",
                        "cancelLabel": "Batal",
                        "fromLabel": "Awal",
                        "toLabel": "Akhir",
                        "customRangeLabel": "Custom",
                        "weekLabel": "M",
                        "daysOfWeek": [
                            "Min",
                            "Sen",
                            "Sel",
                            "Rab",
                            "Kam",
                            "Jum",
                            "Sab"
                        ],
                        "monthNames": [
                            "Januari",
                            "Februari",
                            "Maret",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "Agustus",
                            "September",
                            "Oktober",
                            "November",
                            "Desember"
                        ],
                        "firstDay": 1
                    },
                    singleDatePicker: true
            }, function(start) {
                var tanggal = (start.toISOString()).substring(0,10);
                $('input[name="jatuh_tempo_baru_"]').val(tanggal);
            });
        });

        $(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    // console.log('transaksi-pembelian/' + trapem_id + '/cairkan/cek');
                    location.href = trapem_id + '/cairkan/cek';
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    // console.log('transaksi-pembelian/' + trapem_id + '/cairkan/bg');
                    location.href = trapem_id + '/cairkan/bg';
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
    </script>
@endsection
