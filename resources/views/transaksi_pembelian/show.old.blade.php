@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Transaksi Pembelian</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnKembali {
			margin-bottom: 0;
		}
		.no-border {
			border: none;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Transaksi Pembelian</h2>
				<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/create') }}" class="btn btn-sm btn-success pull-right" style="margin-right: 0;">
					<i class="fa fa-sign-in"></i> Tambah Retur
				</a>
				<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/transaksi') }}" class="btn btn-sm btn-warning pull-right">
					<i class="fa fa-sign-in"></i> Lihat Retur
				</a>
				<a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<section class="content invoice">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Detail Transaksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Kode Transaksi</td>
											<td>{{ $transaksi_pembelian->kode_transaksi }}</td>
										</tr>
										<tr>
											<td>Nama Suplier</td>
											<td>{{ $transaksi_pembelian->suplier->nama }}</td>
										</tr>
										<tr>
											<td>Nama Sales</td>
											@if ($transaksi_pembelian->seller_id != null)
											<td>{{ $transaksi_pembelian->seller->nama }}</td>
											@else
											<td>-</td>
											@endif
										</tr>
										<tr>
											<td>Harga Total</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->harga_total) }}</td>
										</tr>
										<tr>
											<td>Ongkos Kirim</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->ongkos_kirim) }}</td>
										</tr>
										<tr>
											<td>Jumlah Bayar</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->jumlah_bayar) }}</td>
										</tr>
									</tbody>
								</table>
								
								@if ($transaksi_pembelian->utang > 0)
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Hutang</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nominal Hutang</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->utang) }}</td>
										</tr>
									</tbody>
								</table>
								@endif
								
								@if ($transaksi_pembelian->nominal_tunai)
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Tunai</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nominal Tunai</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->nominal_tunai) }}</td>
										</tr>
									</tbody>
								</table>
								@endif
								
								@if ($transaksi_pembelian->nominal_debet)
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Debit</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nomor Debit</td>
											<td>{{ $transaksi_pembelian->no_debet }}</td>
										</tr>
										<tr>
											<td>Nominal Debit</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->nominal_debet) }}</td>
										</tr>
									</tbody>
								</table>
								@endif
								
								@if ($transaksi_pembelian->nominal_cek)
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">Cek</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nomor Cek</td>
											<td>{{ $transaksi_pembelian->no_cek }}</td>
										</tr>
										<tr>
											<td>Nominal Cek</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}</td>
										</tr>
									</tbody>
								</table>
								@endif
								
								@if ($transaksi_pembelian->nominal_bg)
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;" colspan="2">BG</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Nomor BG</td>
											<td>{{ $transaksi_pembelian->no_bg }}</td>
										</tr>
										<tr>
											<td>Nominal BG</td>
											<td>{{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}</td>
										</tr>
									</tbody>
								</table>
								@endif
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th style="text-align: left;">Nama Item</th>
											<th style="text-align: left;">Jumlah</th>
											<th style="text-align: right;">Harga</th>
											<th style="text-align: right;">HPP</th>
											<th style="text-align: right;">PPN</th>
											<th style="text-align: right;">Sub Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($relasi_transaksi_pembelian as $i => $relasi)
											<tr>
												<td>{{ $relasi->item->nama }} {{ $relasi->jumlah }}</td>
												<td style="text-align: left;">
												@foreach ($relasi->jumlahs as $j => $jumlah)
													<span class="label label-info">{{ $jumlah['jumlah'] }} {{ $jumlah['satuan']->kode }}</span>
												@endforeach
												</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->subtotal / $relasi->jumlah) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->harga) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->harga / 10) }}</td>
												<td style="text-align: right;">{{ \App\Util::duit($relasi->subtotal)}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
@endsection


@section('script')
	<script type="text/javascript">
        $('#tabel-item').DataTable();
        
		$(document).ready(function() {
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
