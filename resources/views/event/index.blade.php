@extends('layouts.admin')

@section('title')
	<title>EPOS | Layout Invoice</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.panel_toolbox li {
			cursor: pointer;
		}
	</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12" id="formSimpanContainer">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Tambah Contect Layout Invoice</h2>		
				<ul class="nav navbar-right panel_toolbox">
					<div class="pull-right">
                		<li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                	</div>
                </ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<form method="post" action="{{ url('event') }}" class="form-horizontal" id="formEvent">
						<div class="col-md-12 col-xs-12">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
							<div class="x_content" style="display: block;">
							  	<textarea name="content" id="content"></textarea>
								@ckeditor('content', ['height' => 200])
							</div>
							<label class="control-label" style="padding-bottom: 15px; margin-left: 5px">Pilih Jangka Waktu Pengaktifan</label>
							<fieldset>
							<div class="control-group col-md-4">
								<div class="controls">
									<div class="input-prepend input-group">
										<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
										<input type="text" style="width: 100%" name="data" id="rentang_tanggal" class="form-control active">
										<input type="hidden" name="awal">
										<input type="hidden" name="akhir">
									</div>
								</div>
							</div>
							</fieldset>
						</div>
							<div class="form-group" style="margin-bottom: 0; padding-top: 30px">
								<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> <span>Tambah</span>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tablePiutangBank').DataTable();

		$(document).ready(function() {
			$(".select2_single").select2();
			
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
			var mulai = (start.toISOString()).substring(0,10);
			console.log(mulai);
			var akhir = (end.toISOString()).substring(0,10);
			console.log(akhir);
			$('#formEvent').find('input[name="awal"]').val(mulai);
			$('#formEvent').find('input[name="akhir"]').val(akhir);
		});
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
		});

		$(window).on('load', function(event) {

			var url = "{{ url('hutang_bank/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			

			$.get(url, function(data) {
				if (data.hutang_bank === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/HB/' + tanggal;
					console.log('kode_transaksi');
				} else {
					var kode_transaksi = data.hutang_bank.kode_transaksi;
					var mm_transaksi = kode_transaksi.split('/')[2];
					var yyyy_transaksi = kode_transaksi.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/HB/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/HB/' + tanggal_transaksi;				}
				}
				// console.log(kode_transaksi);
				$('input[name="kode_transaksi"]').val(kode_transaksi);
			});
		});
	</script>
@endsection
