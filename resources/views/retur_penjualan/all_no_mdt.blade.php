@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Retur Penjualan</title>
    <!-- index retur penjualan -->
@endsection

@section('style')
    <style type="text/css">
        #tableReturPenjualan .btn {
            margin-bottom: 0;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Retur Penjualan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-striped table-hover" id="tableReturPenjualan" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Kode Retur</th>
                            <th>Kode Transaksi</th>
                            {{-- <th>Operator</th> --}}
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($retur_penjualan as $i => $retur)
                        <tr data-id="{{ $retur->id }}" kode="{{ $a = $i }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $retur->created_at->format('d-m-Y') }}</td>
                            <td>{{ $retur->kode_retur }}</td>
                            <td>{{ $retur->transaksi_penjualan->kode_transaksi }}</td>
                            {{-- <td>{{ $retur->user->nama }}</td> --}}
                            <td>
                                <a href="{{ url('transaksi/'.$retur->transaksi_penjualan->id.'/retur/'.$retur->id) }}" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fa fa-eye"></i>
                                </a>
                                {{-- @if($pembayaran_data[$i] != NULL)
                                    @if($pembayaran_data[$i]->cek_lunas == 0)
                                        <a class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                            CEK
                                        </a>
                                    @endif
                                    @if($pembayaran_data[$i]->bg_lunas == 0)
                                        <a class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                            BG
                                        </a>
                                    @endif
                                @endif --}}
                                @if($bayar[$a] == 1)
                                    @if($pembayaran_data[$a]->cek_lunas == 0 || $pembayaran_data[$a]->bg_lunas == 0)
                                        @if($pembayaran_data[$a]->cek_lunas == 0 && $pembayaran_data[$a]->nominal_cek > 0 )
                                            <a class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                                CEK
                                            </a>
                                        @endif
                                        @if($pembayaran_data[$a]->bg_lunas == 0 && $pembayaran_data[$a]->nominal_bg > 0)
                                            <a class="btn btn-xs btn-BG" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                                BG
                                            </a>
                                        @endif
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableReturPenjualan').DataTable({
                'order': [[1, 'desc']]
            });
        });
    </script>
@endsection
