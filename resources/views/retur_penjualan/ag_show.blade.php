@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Retur Transaksi Penjualan</title>
@endsection

@section('style')
	<style type="text/css">
		#btnKembali {
			margin: 0;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}

		.dataTables_filter input { 
			/*width: 50px !important*/
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-4">
						<h2 id="formSimpanTitle">Detail Retur Penjualan</h2>
						{{-- <span id="kodeTransaksiTitle">{{ $retur_penjualan->kode_retur }}</span> --}}
					</div>
					<div class="col-md-8"> 
						<!-- <a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" style="margin-right: 0;">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a> -->
						<a href="{{ url('retur-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" style="margin-right: 0;" data-toggle="tooltip" data-placement="top" title="Kembali">
							<i class="fa fa-long-arrow-left"></i>
						</a>
						@if($bayar == 1)
							@if($pembayaran->bg_lunas == 0 && $pembayaran->nominal_bg > 0)
								<a href="{{ url('retur-penjualan/bg').'/'.$pembayaran->id }}" id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right" data="{{ $pembayaran->id }}">
                        			<!-- <i class="fa fa-money"></i> -->BG
								</a>
							@endif
							@if($pembayaran->cek_lunas == 0 && $pembayaran->nominal_cek > 0 )
								<a href="{{ url('retur-penjualan/cek').'/'.$pembayaran->id }}" id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right" data="{{ $pembayaran->id }}">
                        			<!-- <i class="fa fa-money"></i> -->CEK
								</a>
							@endif
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<section class="content invoice">
					<div class="row">
	                    <div class="col-md-5 col-xs-12">
	                        <div class="x_title">
	                            <h2>{{ $transaksi_penjualan->created_at->format('d-m-Y H:i:s') }}</h2>
	                            <div class="clearfix"></div>
	                        </div>
	                        <table class="table table-bordered table-striped table-hover">
	                            <tbody>
	                                <tr>
	                                    <tr>
	                                    <th>Kode Retur</th>
	                                    <td style="width: 60%;">{{ $retur_penjualan->kode_retur }}</td>
	                                </tr>
	                                    <th>Kode Transaksi</th>
	                                    <td style="width: 60%;">{{ $retur_penjualan->transaksi_penjualan->kode_transaksi }}</td>
	                                </tr>
	                                @if ($retur_penjualan->pelanggan_id != null)
		                                <tr>
		                                    <th>Pelanggan</th>
		                                    <td style="width: 60%;">{{ $retur_penjualan->pelanggan->nama }}</td>
		                                </tr>
		                              @endif
	                                <tr>
										<th>Status Retur</th>
										<td>
											@if($retur_penjualan->status == 'sama')
												Retur Barang Sama
											@elseif($retur_penjualan->status == 'lain')
												Retur Barang Lain
											@elseif($retur_penjualan->status == 'uang')
												Ganti Uang
											@endif
										</td>
									</tr>
	                                <tr>
	                                    <th>Operator</th>
	                                    <td style="width: 60%;">{{ $retur_penjualan->user->nama }}</td>
	                                </tr>
	                            </tbody>
	                        </table>

	                        <table class="table table-bordered table-striped table-hover">
	                            <thead>
	                                <tr>
	                                    <th colspan="2" class="text-center">Rincian</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    @if ($retur_penjualan->status == 'lain')
	                                        <th>Sub Total (Masuk)</th>
	                                    @else
	                                        <th>Sub Total</th>
	                                    @endif

	                                	@if ($retur_penjualan->harga_total != null && $retur_penjualan->harga_total > 0)
	                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($retur_penjualan->harga_total) }}</td>
	                                    @else
	                                    <td>-</td>
	                                	@endif
	                                </tr>

	                                @if ($retur_penjualan->total_uang_masuk != null && $retur_penjualan->total_uang_masuk > 0 && $retur_penjualan->status == 'uang')
	                                    @if ($selisih < 0)
	                                        <tr>
	                                            <th>Keuntungan Retur</th>
	                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih * -1) }}</td>
	                                        </tr>
	                                    @endif
	                                    <tr>
	                                        <th>Jumlah Uang Diterima</th>
	                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($retur_penjualan->total_uang_masuk) }}</td>
	                                    </tr>
	                                    @if ($selisih > 0)
	                                        <tr>
	                                            <th>Kerugian Retur</th>
	                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($selisih * -1) }}</td>
	                                        </tr>
	                                    @endif
	                                @endif    
	                                
	                                @if ($retur_penjualan->status == 'lain')
	                               		@if ($retur_penjualan->total_uang_masuk > 0)
		                                    <tr>
		                                        <th>Kekurangan</th>
		                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($retur_penjualan->total_uang_masuk) }}</td>
		                                    </tr>
		                                @endif
	                                    <tr>
	                                        <th>Sub Total (Keluar)</th>
	                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($retur_penjualan->total_uang_keluar) }}</td>
	                                    </tr>
	                                @endif
	                            </tbody>
	                        </table>

							<table class="table table-bordered table-striped table-hover">
								@if ($retur_penjualan->status == 'lain')
									<thead>
										<th class="text-center" colspan="2">Pembayaran</th>
									</thead>
									<tbody>
										@if($pembayaran->nominal_tunai_in > 0)
										<tr>
											<th class="text-left">Nominal Tunai</th>
											<td class="text-right">{{ \App\Util::duit0($pembayaran->nominal_tunai_in) }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_titipan_in > 0)
										<tr>
											<th class="text-left">Menambah Deposito Pelanggan</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_titipan_in) }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_transfer_in > 0)
										<tr>
											<th class="text-left">Nominal Transfer</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_transfer_in) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Transfer</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_transfer_in }}</td>
										</tr>
										<tr>
											<th class="text-left">Bank Transfer</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->bank_transferp_in->nama_bank }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_kartu_in > 0)
										<tr>
											<th class="text-left">Nominal Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_transfer_in) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_kartu_in }}</td>
										</tr>
										<tr>
											<th class="text-left">Jenis Kartu</th>
											@if ($pembayaran->jenis_kartu_in == 'debet')
												<td class="text-right" style="width: 60%;">Debit</td>
											@else
												<td class="text-right" style="width: 60%;">Kredit</td>
											@endif
										</tr>
										<tr>
											<th class="text-left">Bank Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->bank_kartup_in->nama_bank }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_cek_in > 0)
										<tr>
											<th class="text-left">Nominal Cek</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_cek_in) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Cek</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_cek_in }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_bg_in> 0)
										<tr>
											<th class="text-left">Nominal BG</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_bg_in) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor BG</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_bg_in }}</td>
										</tr>
										@endif
									</tbody>
								@elseif ($retur_penjualan->status == 'uang')
									<thead>
										<th class="text-center" colspan="2">Pembayaran</th>
									</thead>
									<tbody>
										@if($pembayaran->nominal_tunai > 0)
										<tr>
											<th class="text-left">Nominal Tunai</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_tunai) }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_titipan > 0)
										<tr>
											<th class="text-left">Menambah Deposito Pelanggan</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_titipan) }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_piutang > 0)
										<tr>
											<th class="text-left">Membayar Hutang Transaksi</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_piutang) }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_transfer > 0)
										<tr>
											<th class="text-left">Nominal Transfer</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_transfer) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Transfer</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_transfer }}</td>
										</tr>
										<tr>
											<th class="text-left">Bank Transfer</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->bank_transferp->nama_bank }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_kartu > 0)
										<tr>
											<th class="text-left">Nominal Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_kartu) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_kartu }}</td>
										</tr>
										<tr>
											<th class="text-left">Jenis Kartu</th>
											@if ($pembayaran->jenis_kartu == 'debet')
												<td class="text-right" style="width: 60%;">Debit</td>
											@else
												<td class="text-right" style="width: 60%;">Kredit</td>
											@endif
										</tr>
										<tr>
											<th class="text-left">Bank Transfer Kartu</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->bank_kartup->nama_bank }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_cek > 0)
										<tr>
											<th class="text-left">Nominal Cek</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_cek) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor Cek</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_cek }}</td>
										</tr>
										@endif
										@if($pembayaran->nominal_bg > 0)
										<tr>
											<th class="text-left">Nominal BG</th>
											<td class="text-right" style="width: 60%;">{{ \App\Util::duit0($pembayaran->nominal_bg) }}</td>
										</tr>
										<tr>
											<th class="text-left">Nomor BG</th>
											<td class="text-right" style="width: 60%;">{{ $pembayaran->no_bg }}</td>
										</tr>
										@endif
									</tbody>
								@endif
							</table>

	                    </div>
                    	<div class="col-md-7">
                    		<div class="row">
		                        <div class="col-md-12 col-xs-12">
		                            <div class="x_title">
		                                <h2>Item Retur</h2>
		                                <div class="clearfix"></div>
		                            </div>
		                            {{-- <table class="table table-bordered table-striped table-hover" id="tabel-item"> --}}
		                            <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tabel-item">
		                                <thead>
		                                    <tr>
		                                        <th class="text-left">No</th>
		                                        <th class="text-left">Nama Item</th>
		                                        <th class="text-left">Jumlah</th>
		                                        <th class="text-left">Harga</th>
		                                        <th class="text-left">Total</th>
		                                        {{-- <th class="text-left">Keterangan</th> --}}
		                                    </tr>
		                                </thead>
		                                <tbody>
											@foreach ($relasi_retur_penjualan as $a => $relasi)
											@if($relasi->retur == 0)
												<tr id="{{ $relasi->id }}" data="{{ $x = $a+1 }}">
													<td>{{ $x }}</td>
													<td>{{ $relasi->item->nama }}</td>
													<td class="text-left">
			                                        @foreach($hasil[$a] as $x => $jumlah)
			                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
			                                            @if ($x != count($hasil[$a]) - 1)
			                                            <br>
			                                            @endif
			                                        @endforeach
			                                    </td>
			                                    <td class="text-right">{{ \App\Util::duit0($hasil[$a]['harga']) }}</td>
												<td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
												</tr>
											@endif
											@endforeach
										</tbody>
		                            </table>
		                        </div>
		                    </div>
		                    @if($retur_penjualan->status == 'lain')
		                    <hr>
							<div class="row">
								<div class="col-md-12">
									<div class="x_title">
										<h2>Item Pengganti Retur</h2>
										<div class="clearfix"></div>
									</div>
									<div>
										<table class="table table-bordered table-striped table-hover" id="tabel-item-retur">
											<thead>
			                                    <tr>
			                                        <th class="text-left">No</th>
			                                        <th class="text-left">Nama Item</th>
			                                        <th class="text-left">Jumlah</th>
			                                        <th class="text-left">Harga</th>
			                                        <th class="text-left">Total</th>
			                                        {{-- <th class="text-left">Keterangan</th> --}}
			                                    </tr>
			                                </thead>
											<tbody data="{{ $index = 1 }}">
												@foreach ($relasi_retur_penjualan as $a => $relasi)
												@if($relasi->retur == 1)
													<tr id="{{ $relasi->id }}" data="{{ $x = 0 }}">
														<td>{{ $index++ }}</td>
														<td>{{ $relasi->item->nama }}</td>
														<td class="text-left">
				                                            {{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}
				                                        @foreach($hasil[$a] as $x => $jumlah)
				                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
				                                            @if ($x != count($hasil[$a]) - 1)
				                                            <br>
				                                            @endif
				                                        @endforeach
				                                    	</td>
				                                    	<td class="text-right">{{ \App\Util::duit0($hasil[$a]['harga']) }}</td>
														<td>{{ \App\Util::duit0($relasi->subtotal) }}</td>
													</tr>
												@endif
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
							@endif
	                	</div>
				</section>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'cek')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran Cek berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('sukses') == 'bg')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran BG berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tabel-item').DataTable();
        $('#tabel-item-retur').DataTable();

		$(document).ready(function() {
			var url = "{{ url('retur-penjualan') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
		});

		$(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('retur-penjualan/cek/') }}' + '/' + id;
            console.log(url);
            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('retur-penjualan/bg/') }}' + '/' + id;
            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
	</script>
@endsection
