@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Retur Penjualan</title>
@endsection

@section('style')
	<style type="text/css">
		/*#btnKembali {
			margin-right: 0;
		}*/

		.full-width {
			width: 100%;
		}

		table > tbody > tr > td.pad-top-13 {
			padding-top: 13px;
		}

		table > tbody > tr > td.pad-top-10 {
			padding-top: 10px;
		}

		.select2-container {
			width: 100% !important;
			padding: 0;
		}

		.modal-content {
			border-radius: 0;
		}

		.input-err,
		.input-err:hover,
		.input-err:focus {
			border: 1px solid #d9534f;
		}

		#pilihanReturButtonGroup > label.active {
			background: #26b99a;
			border: 1px solid #169f85;
			color: white;
			font-weight: bold;
			border-radius: 0;
		}

	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-6">
						<h2 class="full-width">Tambah Retur Penjualan</h2>
						<span id="kodeReturTitle" class="full-width"></span>
					</div>
					<div class="col-md-6">
						<a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur') }}" class="btn btn-sm btn-warning pull-right" style="margin-right: 0;">
							<i class="fa fa-sign-in"></i> Lihat Retur
						</a>
						<a href="{{ url('transaksi/'.$transaksi_penjualan->id) }}" class="btn btn-sm btn-info pull-right">
							<i class="fa fa-eye"></i> Detail Penjualan
						</a>
						<a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                			<i class="fa fa-long-arrow-left"></i>
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-6">
						<div class="x_title">
							<h2>Detail Transaksi</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table">
								<tbody>
									<tr>
										<th style="width: 35%; border-top: none;">Kode Transaksi</th>
										<td style="border-top: none;">{{ $transaksi_penjualan->kode_transaksi }}</td>
									</tr>
									<tr>
										<th>Total Harga</th>
										<td>Rp {{ number_format($transaksi_penjualan->harga_total) }}</td>
									</tr>
									<tr>
										<th>Tanggal Transaksi</th>
										<td>{{ $transaksi_penjualan->created_at->format('d-m-Y') }}</td>
									</tr>
									<tr>
										<th>Kasir</th>
										<td>{{ $transaksi_penjualan->user->nama }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="x_title">
							<h2>Detail Item</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table" id="tabelDetailItem">
								<thead>
									<th style="text-align: left;">Item</th>
									<th style="text-align: left;">Jumlah (pcs)</th>
									<th style="text-align: left;">Max Retur (pcs)</th>
									<th style="text-align: left;">Aksi</th>
								</thead>
								<tbody>
									@foreach ($relasi_transaksi_penjualan as $i => $relasi)
									<tr data-id="{{ $relasi->item_kode }}">
										<td class="pad-top-10">{{ $relasi->item->nama }}</td>
										<td class="pad-top-10">{{ $relasi->jumlah }}</td>
										<td class="pad-top-10">{{ $relasi->max_retur }}</td>
										<td>
											@if ($relasi->max_retur > 0)
											<button type="button" id="btnRetur" class="btn btn-xs btn-warning">
												<i class="fa fa-sign-in"></i> Retur
											</button>
											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Retur</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				 <table class="table" id="tabelRetur">
				 	<thead>
				 		<tr>
				 			<th></th>
				 			<th>Nama Item</th>
				 			<th>Jumlah (pcs)</th>
				 			<th>Keterangan</th>
				 			<th style="width: 25%;">Pilihan Retur</th>
				 			<th></th>
				 		</tr>
				 	</thead>
				 	<tbody></tbody>
				 </table>
				 <div id="formSimpanContainer">
				 	<form id="form-simpan" method="post" action="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur') }}">
				 		{{ csrf_field() }}
				 		<input type="hidden" name="kode_retur">
				 		<input type="hidden" name="transaksi_penjualan_id" value="{{ $transaksi_penjualan->id }}" />
				 		<div id="append-section"></div>
				 		<div class="form-group">
				 			<button type="submit" class="btn btn-success pull-right sr-only"><i class="fa fa-check"></i> OK</button>
				 		</div>
				 	</form>
				 </div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="popup-ganti-item">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Retur Item</h4>
				</div>
				<div class="modal-body">
					<div class="form-group col-xs-12 col-md-12">
						<label class="control-label">Item Retur</label>
						<input type="text" id="item-retur" value="" class="form-control" readonly="readonly" />
						<input type="hidden" id="item-id-retur" />
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label class="control-label">Pilih Item</label>
						<select class="form-control select2_single" name="item_id" id="item_id">
							<option value="">Pilih Item</option>
							@foreach ($items as $item)
							<option value="{{ $item->kode }}" data-title="{{ $item->nama }}">{{ $item->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<div id="inputJumlahItemContainer">
							<label class="control-label">Jumlah (pcs)</label>
							<input type="text" id="inputJumlahItem" class="form-control" />
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-check"></i> OK</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
				</div>
			</div>
		</div>
	</div>

	{{-- Popup detail untuk retur tunai --}}
	<div class="modal fade" id="detailTunaiModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Tunai title</h4>
				</div>
				<div class="modal-body">
					<table class="table">
						<tbody>
							<tr>
								<th style="border-top: none;">Aksi Retur</th>
								<td style="border-top: none;">Ganti Uang</td>
							</tr>
							<tr>
								<th>Item Retur</th>
								<td id="popupGantiUangItem"></td>
							</tr>
							<tr>
								<th>Jumlah Retur (pcs)</th>
								<td id="popupGantiUangJumlah"></td>
							</tr>
							<tr>
								<th>Keterangan</th>
								<td id="popupGantiUangKeterangan"></td>
							</tr>
							{{-- <tr>
								<th>Subtotal</th>
								<td></td>
							</tr> --}}
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<i class="fa fa-times"></i> <span>Close</span>
					</button>
				</div>
			</div>
		</div>
	</div>

	{{-- Popup detail untuk retur ganti --}}
	<div class="modal fade" id="detailItemModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Detail Item Retur</h4>
				</div>
				<div class="modal-body">
					<table class="table">
						<tbody>
							<tr>
								<th style="border-top: none;">Aksi Retur</th>
								<td style="border-top: none;">Ganti Item</td>
							</tr>
							<tr>
								<th>Item Retur</th>
								<td id="popupGantiItemItemRetur"></td>
							</tr>
							<tr>
								<th>Jumlah Retur (pcs)</th>
								<td id="popupGantiItemJumlahRetur"></td>
							</tr>
							<tr>
								<th>Item Ganti</th>
								<td id="popupGantiItemItemGanti"></td>
							</tr>
							<tr>
								<th>Jumlah Item (pcs)</th>
								<td id="popupGantiItemJumlahGanti"></td>
							</tr>
							<tr>
								<th>Keterangan</th>
								<td id="popupGantiItemKeterangan"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<i class="fa fa-times"></i> <span>Close</span>
					</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('retur-penjualan') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
			
			$(".select2_single").select2();
		});

		$(window).on('load', function(event) {

			var url = "{{ url('transaksi/'.$transaksi_penjualan->id.'/retur/last/json') }}";
			var tanggal = printTanggalSekarang('dd/mm/yyyy');			

			$.get(url, function(data) {
				if (data.retur_penjualan === null) {
					var kode = int4digit(1);
					var kode_retur = kode + '/TRAJR/' + tanggal;
				} else {
					var kode_retur = data.retur_penjualan.kode_retur;
					var dd_retur = kode_retur.split('/')[2];
					var mm_retur = kode_retur.split('/')[3];
					var yyyy_retur = kode_retur.split('/')[4];
					var tanggal_retur = dd_retur + '/' + mm_retur + '/' + yyyy_retur;

					if (tanggal != tanggal_retur) {
						var kode = int4digit(1);
						kode_retur = kode + '/TRAJR/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_retur.split('/')[0]) + 1);
						kode_retur = kode + '/TRAJR/' + tanggal_retur;
					}
				}

				$('input[name="kode_retur"]').val(kode_retur);
				$('#kodeReturTitle').text(kode_retur);
			});
		});

		$(document).on('click', '#btnRetur', function(event) {
			event.preventDefault();
			
			var kode = $(this).parents('tr').data('id');
			var nama = $(this).parents('tr').first().children().first().text();
			var tr   = $('#tabelRetur').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

			if (tr === undefined) {
				$('#append-section').append('<input type="hidden" name="aksi_retur[]" id="aksi_retur-'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="item_retur[]" id="item_retur-'+kode+'" value="'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="jumlah_retur[]" id="jumlah_retur-'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="keterangan_retur[]" id="keterangan_retur-'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="item_ganti[]" id="item_ganti-'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="jumlah_ganti[]" id="jumlah_ganti-'+kode+'" />');
				$('#append-section').append('<input type="hidden" name="harga_ganti[]" id="harga_ganti-'+kode+'" />');

				$('#tabelRetur').find('tbody').append('<tr data-id="'+kode+'"><td class="pad-top-13" style="color: tomato; cursor: pointer;"><i class="fa fa-times" id="remove" title="Hapus Item"></i></td><td class="pad-top-13" id="inputNamaItem">'+nama+'</td><td><input type="text" name="inputJumlahRetur" id="inputJumlahRetur" class="form-control input-sm" /></td><td><input type="text" name="inputKeteranganRetur" id="inputKeteranganRetur" class="form-control input-sm" /></td><td><div id="pilihanReturButtonGroup" class="btn-group btn-group-sm btn-group-justified" role="group"><div class="btn-group btn-group-sm" role="group"><button type="button" class="btn btn-sm btn-default" id="btnGantiUang"><i class="fa fa-money"></i> Ganti Uang</button></div><div class="btn-group btn-group-sm" role="group"><button type="button" class="btn btn-sm btn-default" id="btnGantiItem"><i class="fa fa-cubes"></i> Ganti Item</button></div></div></td><td><button type="button" id="btnDetail" class="btn btn-sm btn-info full-width" disabled="disabled"><i class="fa fa-eye"></i> Detail</button></td></tr>');
			}

			$('#form-simpan').find('button').removeClass('sr-only');
		});

		// Jumlah item yang dikembalikan
		$(document).on('keyup', '#inputJumlahRetur', function(event) {
			event.preventDefault();
			
			var item_kode = $(this).parents('tr').data('id');
			var transaksi_penjualan_id = $('input[name="transaksi_penjualan_id"]').val();
			var url = "{{ url('transaksi') }}"+'/'+transaksi_penjualan_id+'/retur/harga/json/'+item_kode;

			var jumlah = $(this).val();
			var jml = $('#tabelDetailItem').find('tr[data-id="'+item_kode+'"]').first().children().first().next().text();
			var harga = 0;
			var subtotal = 0;

			if (jumlah > parseInt(jml)) {
				$(this).addClass('input-err');
				$(this).parent().next().find('input').attr('disabled', 'disabled');
				$(this).parent().next().next().find('button').attr('disabled', 'disabled');
				$('#form-simpan').find('button').attr('disabled', 'disabled');
			} else {
				$(this).removeClass('input-err');
				$(this).parent().next().find('input').removeAttr('disabled');
				$(this).parent().next().next().find('button').removeAttr('disabled');
				$('#form-simpan').find('button').removeAttr('disabled');
			}

			$('#jumlah_retur-'+item_kode).val(jumlah);

			$.get(url, function(data) {
				harga    = parseInt(data.relasi_transaksi_penjualan.harga.replace(/\D/g, ''), 10) / 100;
				subtotal = harga * parseInt(jumlah);

				$('#harga_ganti-'+item_kode).val(subtotal);
			});
		});

		$(document).on('change', '#inputKeteranganRetur', function(event) {
			event.preventDefault();
			
			var kode       = $(this).parents('tr').data('id');
			var keterangan = $(this).val();

			$('#keterangan_retur-'+kode).val(keterangan);
		});

		$(document).on('click', '#btnGantiUang', function(event) {
			event.preventDefault();

			var kode = $(this).parents('tr').data('id');
			var btn  = $(this).parents('td').next().find('#btnDetail');
			
			$(this).parent().next().find('button').removeClass('btn-success');
			$(this).addClass('btn-success');

			$('#aksi_retur-'+kode).val('ganti_uang');

			btn.removeAttr('disabled').attr({
				'data-toggle': 'modal',
				'data-target': '#detailTunaiModal'
			});
		});

		$(document).on('click', '#btnGantiItem', function(event) {
			event.preventDefault();

			var kode = $(this).parents('tr').data('id');
			var nama = $(this).parents('tr').first().children().first().next().text();
			var btn  = $(this).parents('td').next().find('#btnDetail');
			
			$(this).parent().prev().find('button').removeClass('btn-success');
			$(this).addClass('btn-success');

			$('#aksi_retur-'+kode).val('ganti_item');

			$('#popup-ganti-item').modal('show').on('shown.bs.modal', function(event) {
				event.preventDefault();
				
				$(this).find('.modal-body').find('#item-retur').val(nama);
				$(this).find('.modal-body').find('#item-id-retur').val(kode);
				$(this).find('.modal-footer').find('button').first().on('click', function(event) {
					event.preventDefault();

					var item_ganti   = $(this).parents('.modal-content').find('.modal-body').find('#item_id').val(); 
					var jumlah_ganti = $(this).parents('.modal-content').find('.modal-body').find('#inputJumlahItem').val();

					$('#item_ganti-'+kode).val(item_ganti);
					$('#jumlah_ganti-'+kode).val(jumlah_ganti);
					
					btn.removeAttr('disabled').attr({
						'data-toggle': 'modal',
						'data-target': '#detailItemModal'
					});
				});
			});
		});

		// Jumlah item yang diganti
		$(document).on('keyup', '#inputJumlahItem', function(event) {
			event.preventDefault();
			
			var btn         = $(this);
			var kode        = $('#item-id-retur').val();
			var jumlah      = btn.val();
			var url         = "{{ url('transaksi') }}"+'/'+kode+'/retur/harga/json/1/'+jumlah;
			var harga_retur = $('#harga_ganti-'+kode).val();
			var harga       = 0;
			var subtotal    = 0;

			$.get(url, function(data) {
				harga    = data.harga.harga;
				subtotal = parseInt(harga) * parseInt(jumlah);

				if (subtotal > parseInt(harga_retur)) {
					btn.addClass('input-err').focus();
					btn.parents('.modal-body').next().find('button').first().attr('disabled', 'disabled');
				} else {
					btn.removeClass('input-err');
					btn.parents('.modal-body').next().find('button').first().removeAttr('disabled');
				}
			});
		});

		$(document).on('click', '#btnDetail', function(event) {
			event.preventDefault();
			var $tr = $(this).parents('tr');
			var nama = $tr.find('#inputNamaItem').text();
			var jumlah = $tr.find('#inputJumlahRetur').val();
			var keterangan = $tr.find('#inputKeteranganRetur').val();
			$('#popupGantiUangItem').text(nama);
			$('#popupGantiUangJumlah').text(jumlah);
			$('#popupGantiUangKeterangan').text(keterangan);
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			
			var kode = $(this).parents('tr').data('id');

			$('#tabelRetur').find('tr[data-id="'+kode+'"]').remove();
			$('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
		});
	</script>
@endsection
