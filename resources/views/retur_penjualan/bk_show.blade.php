@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Retur Transaksi Penjualan</title>
@endsection

@section('style')
	<style type="text/css">
		#btnKembali {
			margin: 0;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}

		.dataTables_filter input { 
			/*width: 70% !important*/
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-4">
						<h2 id="formSimpanTitle">Detail Retur Penjualan</h2>
						{{-- <span id="kodeTransaksiTitle">{{ $retur_penjualan->kode_retur }}</span> --}}
					</div>
					<div class="col-md-8"> 
						<!-- <a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" style="margin-right: 0;">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a> -->
						<a href="{{ url('retur-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" style="margin-right: 0;" data-toggle="tooltip" data-placement="top" title="Kembali">
							<i class="fa fa-long-arrow-left"></i>
						</a>
						@if($bayar == 1)
							@if($pembayaran->bg_lunas == 0 && $pembayaran->nominal_bg > 0)
								<a href="{{ url('retur-penjualan/bg').'/'.$pembayaran->id }}" id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right" data="{{ $pembayaran->id }}">
                        			<!-- <i class="fa fa-money"></i> -->BG
								</a>
							@endif
							@if($pembayaran->cek_lunas == 0 && $pembayaran->nominal_cek > 0 )
								<a href="{{ url('retur-penjualan/cek').'/'.$pembayaran->id }}" id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right" data="{{ $pembayaran->id }}">
                        			<!-- <i class="fa fa-money"></i> -->CEK
								</a>
							@endif
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<section class="content invoice">
					<div class="col-md-12 col-xs-12">
						<div class="row">
					        {{-- <div class="col-md-12"> --}}
					            <div class="x_title">
					                <h2>Item Retur</h2>
					                <div class="clearfix"></div>
					            </div>
					            <div class="x_content">
						            <table class="table table-bordered table-striped table-hover" id="tabel-item">
						                <thead>
						                    <tr>
						                        <th class="text-left">No</th>
						                        <th class="text-left">Nama Item</th>
						                        <th class="text-left">Jumlah</th>
						                        <th class="text-left">Harga</th>
						                        <th class="text-left">Total</th>
						                        {{-- <th class="text-left">Keterangan</th> --}}
						                    </tr>
						                </thead>
						                <tbody>
											@foreach ($relasi_retur_penjualan as $a => $relasi)
											@if($relasi->retur == 0)
												<tr id="{{ $relasi->id }}" data="{{ $x = $a+1 }}">
													<td>{{ $x }}</td>
													<td>{{ $relasi->item->nama }}</td>
													<td class="text-left">
						                            @foreach($hasil[$a] as $x => $jumlah)
						                                {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
						                                @if ($x != count($hasil[$a]) - 1)
						                                <br>
						                                @endif
						                            @endforeach
						                        </td>
						                        <td class="text-right">{{ \App\Util::duit0($hasil[$a]['harga']) }}</td>
												<td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
												</tr>
											@endif
											@endforeach
										</tbody>
						            </table>

									<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tabel-item-retur">
				                        <thead>
				                            <tr>
				                                <th>No</th>
				                                <th>Kode Item</th>
				                                <!-- <th>Kode Item</th> -->
				                                <th>Nama Item</th>
				                                <th>Jenis Item</th>
				                                <th>Stok</th>
				                                <th>Validasi Satuan</th>
				                                <th style="width: 110px;">Aksi</th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <th>No</th>
			                                <th>Kode Item</th>
			                                <!-- <th>Kode Item</th> -->
			                                <th>Nama Item</th>
			                                <th>Jenis Item</th>
			                                <th>Stok</th>
			                                <th>Validasi Satuan</th>
			                                <th style="width: 110px;">Aksi</th>
				                        </tbody>
				                    </table>
				                </div>
					        {{-- </div> --}}
					    </div>
					</div>
	                    
				</section>
			</div>
		</div>
	</div>

	
@endsection

@section('script')
	@if (session('sukses') == 'cek')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran Cek berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('sukses') == 'bg')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Pembayaran BG berhasil Diproses!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tabel-item').DataTable();
        $('#tabel-item-retur').DataTable();

		$(document).ready(function() {
			var url = "{{ url('retur-penjualan') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
		});

		$(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('retur-penjualan/cek/') }}' + '/' + id;
            console.log(url);
            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('retur-penjualan/bg/') }}' + '/' + id;
            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
	</script>
@endsection
