function int4digit(i) {
    if (i < 10) {
        return '000' + i;
    } else if (i < 100) {
        return '00' + i;
    } else if (i < 1000) {
        return '0' + i;
    } else {
        return i;
    }
}

function int6digit(i) {
    if (i < 10) {
        return '00000' + i;
    } else if (i < 100) {
        return '0000' + i;
    } else if (i < 1000) {
        return '000' + i;
    } else if (i < 10000) {
        return '00' + i;
    } else if (i < 100000) {
        return '0' + i;
    } else {
        return i;
    }
}

function printTanggalSekarang(format) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    if (format == 'dd/mm/yyyy') {
        return dd + '/' + mm + '/' + yyyy;
    }

    return yyyy + '-' + mm + '-' + dd;
}

function printBulanSekarang(format) {
    var today = new Date();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (mm < 10) {
        mm = '0' + mm;
    }

    if (format == 'mm/yyyy') {
        return mm + '/' + yyyy;
    }
}

function terbilang(x) {
    var bil = [
        '',
        'satu',
        'dua',
        'tiga',
        'empat',
        'lima',
        'enam',
        'tujuh',
        'delapan',
        'sembilan',
        'sepuluh',
        'sebelas',
    ];

    if (x < 12) {
        return ' ' + bil[x];
    } else if (x < 20) {
        console.log(x, 'oi');
        return terbilang(x - 10) + ' belas';
    } else if (x < 100) {
        return terbilang(parseInt(x / 10)) + ' puluh' + terbilang(x % 10);
    } else if (x < 200) {
        return ' seratus' + terbilang(x - 100);
    } else if (x < 1000) {
        return terbilang(parseInt(x / 100)) + ' ratus' + terbilang(x % 100);
    } else if (x < 2000) {
        return " seribu" + terbilang(x - 1000);
    } else if (x < 1000000) {
        return terbilang(parseInt(x / 1000)) + " ribu" + terbilang(x % 1000); 
    } else if (x < 1000000000) {
        return terbilang(parseInt(x / 1000000)) + " juta" + terbilang(x % 1000000);
    }
}

function ymd2dmy(ymd) {
    var yyyy = ymd.split('-')[0];
    var mm = ymd.split('-')[1];
    var dd = ymd.split('-')[2];
    return dd + '-' + mm + '-' + yyyy;
}

function ymd2dmyMiring(ymd) {
    if (ymd == '0000-00-00') return '--/--/----';

    var yyyy = ymd.split('-')[0];
    var mm = ymd.split('-')[1];
    var dd = ymd.split('-')[2];
    return dd + '/' + mm + '/' + yyyy;
}

Date.prototype.addDays = function(lama) {
    this.setDate(this.getDate() + parseInt(lama));
    return this;
};

function getJatuhTempo(lama) {
    var date = new Date().addDays(lama);
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return yyyy + '-' + mm + '-' + dd;
}

function pangkasNama(nama) {
    if (nama == null) return '-';
    var namas = nama.split(' ');
    var hasil = namas[0];
    for (var i = 1; i < namas.length; i++) {
        if (i == 1) hasil += ' ';
        hasil += namas[i][0];
    }
    return hasil;
}

function CapitalFirst(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}