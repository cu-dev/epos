<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modal extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nominal', 'keterangan'
	];

}
