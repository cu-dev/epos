<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiBundle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_parent', 'item_child', 'jumlah'
    ];

    public function bundle()
    {
        return $this->belongsTo('App\Item', 'item_parent', 'kode');
    }

    public function itemxx()
    {
        return $this->belongsTo('App\Item', 'item_child', 'kode');
    }
}
