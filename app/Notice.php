<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaksi_pembelian_id', 'aktif'
    ];

    public function transaksi_pembelian()
    {
        return $this->belongsTo('App\TransaksiPembelian');
    }
}
