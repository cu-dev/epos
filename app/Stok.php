<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id', 'transaksi_pembelian_id', 'item_kode', 'jumlah', 'harga', 'rusak', 'retur', 'kadaluarsa'
    ];

    protected $appends = [
        // 'transaksi_pembelian'
    ];

    // public function getTransaksiPembelianAttribute()
    // {
    //     return \App\TransaksiPembelian::find($this->transaksi_pembelian_id);
    // }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function items()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function transaksi_pembelian()
    {
        return $this->belongsTo('App\TransaksiPembelian');
    }
}
