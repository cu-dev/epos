<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOfNum extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_son',
        'user_id',
    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'relasi_stock_of_nums')
            ->withPivot('stok_sistem', 'stok_fisik', 'keterangan')
            ->withTimestamps();
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }    
}
