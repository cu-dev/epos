<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenyesuaianPersediaan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_transaksi',
        'nominal',
        'user_id',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
