<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiStokPenjualan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stok_id', 'relasi_transaksi_penjualan_id', 'jumlah', 'hpp'
    ];

    public function stok()
    {
        return $this->belongsTo('App\Stok');
    }

    public function transaksi_penjualan()
    {
        return $this->belongsTo('App\Stok');
    }
}
