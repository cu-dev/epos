<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiPembelian extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_transaksi',
        'nota',
        'suplier_id',
        'seller_id',
        'po',
        'harga_total',
        'potongan_pembelian',
        'ongkos_kirim',
        'jumlah_bayar',
        'utang',
        'sisa_utang',
        'status_hutang',

        'nominal_tunai',

        'no_transfer',
        'nominal_transfer',
        'bank_transfer',

        'no_cek',
        'nominal_cek',
        'bank_cek',
        'aktif_cek',

        'no_bg',
        'nominal_bg',
        'bank_bg',
        'aktif_bg',

        'no_kartu',
        'jenis_kartu',
        'nominal_kartu',
        'bank_kartu',

        'piutang',

        'ppn_total',
        'jatuh_tempo',
        'user_id',
    ];

    protected $appends = [
        'mdt_suplier'
    ];

    public function getMdtSuplierAttribute()
    {
        return \App\Suplier
            ::where('id', $this->suplier_id)
            ->first();
    }

    // protected $appends = [
    //     'items'
    // ];

    // public function getItemsAttribute()
    // {
    //     return \App\Item::>where('item_kode', $this->kode)->get();
    // }

    public function suplier()
    {
        return $this->belongsTo('App\Suplier');
    }

    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function kredit()
    {
        return $this->belongsTo('App\Kredit');
    }

    public function hutangs()
    {
        return $this->hasMany('App\Hutang')->orderBy('created_at', 'asc');
    }

    public function returs()
    {
        return $this->hasMany('App\ReturPembelian');
        // return $this->hasMany('App\ReturPembelian', 'relasi_retur_pembelians')
        //     ->withPivot('jumlah', 'keterangan')
        //     ->withTimestamps();
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'relasi_transaksi_pembelians')
            ->withPivot('jumlah')
            ->withTimestamps();
    }

    public function stoks()
    {
        return $this->belongsToMany('App\Stok', 'stoks', 'transaksi_pembelian_id')
            ->withPivot('jumlah')
            ->withTimestamps();
    }

    public function bank_transfers()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bank_kartus()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }

    public function bank_ceks()
    {
        return $this->belongsTo('App\Bank', 'bank_cek');
    }

    public function bank_bgs()
    {
        return $this->belongsTo('App\Bank', 'bank_bg');
    }
}
