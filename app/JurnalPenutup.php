<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalPenutup extends Model
{
    public function akun()
    {
    	return $this->belongsTo('App\Akun', 'kode_akun', 'kode');
    }
}
