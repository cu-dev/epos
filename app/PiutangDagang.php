<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiutangDagang extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_transaksi',
        'transaksi_penjualan_id',
        'nominal',
        'sisa'
    ];

    public function transaksi_penjualan()
    {
        return $this->belongsTo('App\TransaksiPenjualan');
    }

    public function bayar_piutang_dagang()
    {
        return $this->hasMany('App\BayarPiutangDagang');
    }
}
