<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $fillable = [
    	'nama'
    ];

    public function kabupaten()
    {
    	return $this->belongsTo('App\Kabupaten');
    }

    public function desas()
    {
    	return $this->hasMany('App\Desa');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
