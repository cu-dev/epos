<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $fillable = [
    	'nama'
    ];

    public function kabupaten()
    {
    	return $this->hasMany('App\Kabupaten');
    }

    public function kecamatans()
    {
        return $this->hasManyThrough('App\Kecamatan', 'App\Kabupaten');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
