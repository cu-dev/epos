<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CetakNota extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaksi_penjualan_id', 'cetak', 'user_id'
    ];

    public function transaksi_penjualan() {
        return $this->belongsTo('App\TransaksiPenjualan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
