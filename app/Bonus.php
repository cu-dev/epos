<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode', 'nama', 'jenis_item_id', 'satuan_id', 'konversi', 'harga_dasar', 'stok'
    ];

    public function relasis_bonuses()
    {
        return $this->hasMany('App\RelasiBonus');
    }
}
