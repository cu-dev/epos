<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetoranBuka extends Model
{
    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function user1()
	{
		return $this->belongsTo('App\User', 'operator', 'id');
	}
}
