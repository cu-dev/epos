<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = [
        'status_identitas',
        'ktp',
        'nama',
        'toko',
        'alamat',
        'desa_id',
        'telepon',
        'fax',
        'email',
        'no_rekening',
        'atas_nama',
        'bank',
        'level',
        'time_change',
        'aktif',
        'diskon_persen',
        'potongan',
        'titipan',
        'limit_jumlah_piutang',
        'jatuh_tempo',
        'user_id',
    ];

    public function desa()
    {
        return $this->belongsTo('App\Desa');
    }

    public function transaksi()
    {
        return $this->hasMany('App\TransaksiPenjualan');
    }

    public function deposito()
    {
        return $this->hasMany('App\PembayaranDeposito');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
