<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Date;

use App\Item;
use App\Satuan;
use App\StockOfNum;
use App\RelasiSatuan;

use Illuminate\Http\Request;

class StockOfNumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $sons = StockOfNum::orderBy('kode_son', 'desc')->get();
        return view('stock_of_num.index', compact('sons'));
    } */

    public function index()
    {
        return view('stock_of_num.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'stock_of_nums.' . $field;
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $sons = StockOfNum
            ::select(
                'stock_of_nums.id',
                'stock_of_nums.created_at',
                'stock_of_nums.kode_son',
                'users.nama as operator'
            )
            ->leftJoin('users', 'users.id', 'stock_of_nums.user_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('stock_of_nums.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('stock_of_nums.kode_son', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = StockOfNum
            ::select(
                'stock_of_nums.id'
            )
            ->leftJoin('users', 'users.id', 'stock_of_nums.user_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('stock_of_nums.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('stock_of_nums.kode_son', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($sons as $i => $son) {

            $buttons['detail'] = ['url' => url('stock-of-num/'.$son->id)];

            // return $buttons;
            $son->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $sons,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function lastJson()
    {
        $stock_of_num = StockOfNum::all()->last();
        return response()->json([
            'stock_of_num' => $stock_of_num
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::select(
                DB::raw('id'),
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                // DB::raw('sum(stoktotal) as stok')
                DB::raw('stoktotal as stok')
            )
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('nama', 'asc')->get();

        foreach ($items as $i => $item) {
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $item->kode)
                ->where('konversi', '>', 0)
                ->orderBy('konversi', 'desc')->get();

            if (count($relasi_satuan) > 0) {
                $temp = $item->stok;
                $jumlah = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) $temp = $temp % $rs->konversi;
                    array_push($jumlah, array(
                        'jumlah' => $hasil,
                        'satuan' => $rs->satuan,
                        'konversi' => $rs->konversi
                    ));
                }
            // } else {
            //     $jumlah = array();
            //     $jumlah[0] = [
            //         'jumlah' => intval($item->stok),
            //         'satuan' => Satuan::where('kode', 'BJ')->first(),
            //         'konversi' => 1
            //     ];
            }

            $item->jumlah = $jumlah;
        }

        $tanggal = date('Y-m-d');
        // return sizeof($items[0]->jumlah);
        return view('stock_of_num.create', compact('items', 'tanggal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        return [
            count($request->item_id),
            count($request->stok_sistem),
            count($request->stok_fisik),
            count($request->keterangan),
        ];
        $son = new StockOfNum;
        $son->kode_son = $request->kode_son;
        $son->user_id = Auth::id();

        $data = array();
        foreach ($request->item_id as $i => $id) {
            if ($request->stok_sistem[$i] != '' || $request->stok_sistem[$i] != null) {
                $stok_sistem = $request->stok_sistem[$i];
            } else {
                $stok_sistem = 0;
            }

            if ($request->stok_fisik[$i] != '' || $request->stok_fisik[$i] != null) {
                $stok_fisik = $request->stok_fisik[$i];
            } else {
                $stok_fisik = 0;
            }
            $data[''.$id] = [
                'stok_sistem' => $stok_sistem,
                'stok_fisik' => $stok_fisik,
                'keterangan' => $request->keterangan[$i]
            ];
        }

        if ($son->save()) {
            $son->items()->sync($data);
            return redirect('/stock-of-num')->with('sukses', 'tambah');
        } else {
            return redirect('/stock-of-num')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $son = StockOfNum::with('items')->find($id);
        foreach ($son->items as $i => $item) {
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $item->kode)
                ->where('konversi', '>', 0)
                ->orderBy('konversi', 'desc')->get();
            // return $relasi_satuan;
            $item->sistem_stok = $item->pivot->stok_sistem;
            $item->fisik_stok = $item->pivot->stok_fisik;
            if (count($relasi_satuan) > 0) {
                $temp = $item->pivot->stok_sistem;
                $stok_sistem = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) {
                        $temp = $temp % $rs->konversi;
                        array_push($stok_sistem, array(
                            'jumlah' => $hasil,
                            'satuan' => $rs->satuan,
                            'konversi' => $rs->konversi
                        ));
                    }
                }

                $temp = $item->pivot->stok_fisik;
                $stok_fisik = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) {
                        $temp = $temp % $rs->konversi;
                        array_push($stok_fisik, array(
                            'jumlah' => $hasil,
                            'satuan' => $rs->satuan,
                            'konversi' => $rs->konversi
                        ));
                    }
                }
            } else {
                $stok_sistem = array();
                $stok_sistem[0] = [
                    'jumlah' => intval($item->pivot->stok_sistem),
                    'satuan' => Satuan::where('kode', 'BIJI')->first(),
                    'konversi' => 1
                ];

                $stok_fisik = array();
                $stok_fisik[0] = [
                    'jumlah' => intval($item->pivot->stok_fisik),
                    'satuan' => Satuan::where('kode', 'BIJI')->first(),
                    'konversi' => 1
                ];
            }

            $item->stok_sistem = $stok_sistem;
            $item->stok_fisik = $stok_fisik;
        }
        // return $son;

        return view('stock_of_num.show', compact('son'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
