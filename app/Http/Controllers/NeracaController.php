<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use Datetime;

use App\BukaTahun;
use App\Saldo;
use App\Akun;
use App\Util;
use Illuminate\Http\Request;

class NeracaController extends Controller
{
    public function index()
    {   
    	$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();
    	Util::akun();
		$data = $this->cek();
		// return $data;
    	return view('neraca.index', compact('data', 'saldo'));
    }

    public function cek()
	{
		$akuns['Aset'] = Akun::where('kode', Akun::Aset)->first()->debet;
		$akuns['AsetLancar'] = Akun::where('kode', Akun::AsetLancar)->first()->debet;

		$akuns['KasTunai'] = Akun::where('kode', Akun::KasTunai)->first()->debet;
		$akuns['KasKecil'] = Akun::where('kode', Akun::KasKecil)->first()->debet;
		$akuns['KasBank'] = Akun::where('kode', Akun::KasBank)->first()->debet;
		$akuns['KasCek'] = Akun::where('kode', Akun::KasCek)->first()->debet;
		$akuns['KasBG'] = Akun::where('kode', Akun::KasBG)->first()->debet;

		$akuns['Persediaan'] = Akun::where('kode', Akun::Persediaan)->first()->debet;
		$akuns['Perlengkapan'] = Akun::where('kode', Akun::Perlengkapan)->first()->debet;
		$akuns['PiutangDagang'] = Akun::where('kode', Akun::PiutangDagang)->first()->debet;
		$akuns['PiutangKaryawan'] = Akun::where('kode', Akun::PiutangKaryawan)->first()->debet;
		$akuns['PiutangLain'] = Akun::where('kode', Akun::PiutangLain)->first()->debet;
		$akuns['PiutangTakTertagih'] = Akun::where('kode', Akun::PiutangTakTertagih)->first()->debet;
		$akuns['AsuransiDimuka'] = Akun::where('kode', Akun::AsuransiDimuka)->first()->debet;
		$akuns['SewaDimuka'] = Akun::where('kode', Akun::SewaDimuka)->first()->debet;
		$akuns['PPNMasukan'] = Akun::where('kode', Akun::PPNMasukan)->first()->debet;

		$akuns['AsetTetap'] = Akun::where('kode', Akun::AsetTetap)->first()->debet;

		$akuns['Tanah'] = Akun::where('kode', Akun::Tanah)->first()->debet;
		$akuns['Bangunan'] = Akun::where('kode', Akun::Bangunan)->first()->debet;
		$akuns['Peralatan'] = Akun::where('kode', Akun::Peralatan)->first()->debet;
		$akuns['Kendaraan'] = Akun::where('kode', Akun::Kendaraan)->first()->debet;

		$akuns['ADBangunan'] = Akun::where('kode', Akun::ADBangunan)->first()->kredit;
		$akuns['ADPeralatan'] = Akun::where('kode', Akun::ADPeralatan)->first()->kredit;
		$akuns['ADKendaraan'] = Akun::where('kode', Akun::ADKendaraan)->first()->kredit;

		$akuns['HutangJangkaPendek'] = Akun::where('kode', Akun::HutangJangkaPendek)->first()->kredit;

		$akuns['KartuKredit'] = Akun::where('kode', Akun::KartuKredit)->first()->kredit;
		$akuns['HutangDagang'] = Akun::where('kode', Akun::HutangDagang)->first()->kredit;
		$akuns['HutangPajak'] = Akun::where('kode', Akun::HutangPajak)->first()->kredit;
		$akuns['TaksiranUtangGaransi'] = Akun::where('kode', Akun::TaksiranUtangGaransi)->first()->kredit;
		$akuns['HutangKonsinyasi'] = Akun::where('kode', Akun::HutangKonsinyasi)->first()->kredit;
		$akuns['PendapatanDimuka'] = Akun::where('kode', Akun::PendapatanDimuka)->first()->kredit;
		$akuns['PembayaranProses'] = Akun::where('kode', Akun::PembayaranProses)->first()->kredit;

		$akuns['Kewajiban'] = Akun::where('kode', Akun::Kewajiban)->first()->kredit;
		$akuns['HutangJangkaPanjang'] = Akun::where('kode', Akun::HutangJangkaPanjang)->first()->kredit;

		$akuns['HutangBank'] = Akun::where('kode', Akun::HutangBank)->first()->kredit;
		
		$akuns['PPNKeluaran'] = Akun::where('kode', Akun::PPNKeluaran)->first()->kredit;
		$akuns['HutangPPN'] = Akun::where('kode', Akun::HutangPPN)->first()->kredit;

		$akuns['Modal'] = Akun::where('kode', Akun::Modal)->first()->kredit;
		$akuns['LabaTahan'] = Akun::where('kode', Akun::LabaTahan)->first()->kredit;
		$akuns['Prive'] = Akun::where('kode', Akun::Prive)->first()->debet;
		$akuns['BagiHasil'] = Akun::where('kode', Akun::BagiHasil)->first()->kredit;

		$akuns['Equitas'] = $akuns['Modal'] - $akuns['Prive'] + $akuns['LabaTahan'] + $akuns['BagiHasil'];

		$akuns['TotalKE'] = $akuns['Equitas'] + $akuns['Kewajiban'];		
		foreach ($akuns as $x => $akun) {
			if($akun < 0){
				$akun = $akun * -1;
				$akuns[$x] = '(Rp'.number_format($akun, 2, ',', '.').')';

			}else{
				$akuns[$x] = 'Rp'.number_format($akun, 2, ',', '.');
			}
		}

		return $akuns;
	}

	public function tampil(Request $request)
	{	
		// return $request->all();
		$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();

		$tanggal = $request->data;
		$tanggal_data = '';
  		foreach ($tanggal as $i => $date) {
			$ex = explode("-", $date);
  			$hari = Carbon::create($ex[0], $ex[1], $ex[2]);
			$tanggal[$i] = $hari->format('d-m-Y');
			if($i == 0){
				$tanggal_data .= $tanggal[$i];
			}else{
				$tanggal_data .= ','.$tanggal[$i];
			}
		}
		// return $tanggal_data;
		$size = sizeof($request->data);
		for($i=0; $i<$size; $i++){
			// $x = Saldo::where('created_at', 'like', $request->data[0].'%')->get();
			$akuns['Aset'][$i] = Saldo::where('kode_akun', Akun::Aset)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['AsetLancar'][$i] = Saldo::where('kode_akun', Akun::AsetLancar)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['KasTunai'][$i] = Saldo::where('kode_akun', Akun::KasTunai)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasKecil'][$i] = Saldo::where('kode_akun', Akun::KasKecil)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasBank'][$i] = Saldo::where('kode_akun', Akun::KasBank)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasCek'][$i] = Saldo::where('kode_akun', Akun::KasCek)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasBG'][$i] = Saldo::where('kode_akun', Akun::KasBG)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['Persediaan'][$i] = Saldo::where('kode_akun', Akun::Persediaan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Perlengkapan'][$i] = Saldo::where('kode_akun', Akun::Perlengkapan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangDagang'][$i] = Saldo::where('kode_akun', Akun::PiutangDagang)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangKaryawan'][$i] = Saldo::where('kode_akun', Akun::PiutangKaryawan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangLain'][$i] = Saldo::where('kode_akun', Akun::PiutangLain)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangTakTertagih'][$i] = Saldo::where('kode_akun', Akun::PiutangTakTertagih)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['AsuransiDimuka'][$i] = Saldo::where('kode_akun', Akun::AsuransiDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['SewaDimuka'][$i] = Saldo::where('kode_akun', Akun::SewaDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PPNMasukan'][$i] = Saldo::where('kode_akun', Akun::PPNMasukan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['AsetTetap'][$i] = Saldo::where('kode_akun', Akun::AsetTetap)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['Tanah'][$i] = Saldo::where('kode_akun', Akun::Tanah)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Bangunan'][$i] = Saldo::where('kode_akun', Akun::Bangunan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Peralatan'][$i] = Saldo::where('kode_akun', Akun::Peralatan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Kendaraan'][$i] = Saldo::where('kode_akun', Akun::Kendaraan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['ADBangunan'][$i] = Saldo::where('kode_akun', Akun::ADBangunan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;
			$akuns['ADPeralatan'][$i] = Saldo::where('kode_akun', Akun::ADPeralatan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;
			$akuns['ADKendaraan'][$i] = Saldo::where('kode_akun', Akun::ADKendaraan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;
			// return $akuns['ADBangunan'][$i];	
			$akuns['HutangJangkaPendek'][$i] = Saldo::where('kode_akun', Akun::HutangJangkaPendek)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['KartuKredit'][$i] = Saldo::where('kode_akun', Akun::KartuKredit)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangDagang'][$i] = Saldo::where('kode_akun', Akun::HutangDagang)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangPajak'][$i] = Saldo::where('kode_akun', Akun::HutangPajak)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['TaksiranUtangGaransi'][$i] = Saldo::where('kode_akun', Akun::TaksiranUtangGaransi)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangKonsinyasi'][$i] = Saldo::where('kode_akun', Akun::HutangKonsinyasi)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['PendapatanDimuka'][$i] = Saldo::where('kode_akun', Akun::PendapatanDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangPPN'][$i] = Saldo::where('kode_akun', Akun::HutangPPN)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['PPNKeluaran'][$i] = Saldo::where('kode_akun', Akun::PPNKeluaran)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Kewajiban'][$i] = Saldo::where('kode_akun', Akun::Kewajiban)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangJangkaPanjang'][$i] = Saldo::where('kode_akun', Akun::HutangJangkaPanjang)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['HutangBank'][$i] = Saldo::where('kode_akun', Akun::HutangBank)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Modal'][$i] = Saldo::where('kode_akun', Akun::Modal)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['LabaTahan'][$i] = Saldo::where('kode_akun', Akun::LabaTahan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['Prive'][$i] = Saldo::where('kode_akun', Akun::Prive)->where('created_at', 'like', $request->data[0].'%')->first()->kredi;
			$akuns['BagiHasil'][$i] = Saldo::where('kode_akun', Akun::BagiHasil)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Equitas'][$i] = $akuns['Modal'][$i] - $akuns['Prive'][$i] + $akuns['LabaTahan'][$i] + $akuns['BagiHasil'][$i];

			$akuns['Prive'][$i] = $akuns['Prive'][$i] *-1;

			$akuns['TotalKE'][$i] = $akuns['Equitas'][$i] + $akuns['Kewajiban'][$i];		
			
			// return $akuns;
			foreach ($akuns as $x => $akun) {
				// $akuns[$x][$i] = number_format($akun[$i], 2, ',', '.');

				if($akun[$i] < 0){
					$akun[$i] = $akun[$i] * -1;
					$akuns[$x][$i] = '(Rp'.number_format($akun[$i], 2, ',', '.').')';
				}else{
					$akuns[$x][$i] = 'Rp'.number_format($akun[$i], 2, ',', '.');
				}
			}
		}
		// return $tanggal;
		// return $akuns;
		return view('neraca.tampil', compact('akuns', 'size', 'tanggal', 'saldo', 'tanggal_data'));
	}

	public function cetak(Request $request)
	{
		// return $request->all();
		$dates = explode(",", $request->date);

		$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();

		$tanggal = $dates;
			// return $tanggal;
  		foreach ($tanggal as $i => $date) {
			$ex = explode("-", $date);
			// return $ex;
  			$hari = Carbon::create($ex[2], $ex[1], $ex[0]);
			$tanggal[$i] = $hari->format('d-m-Y');	
		}

		$size = sizeof($tanggal);
		for($i=0; $i<$size; $i++){
			// $x = Saldo::where('created_at', 'like', $request->data[0].'%')->get();
			$akuns['Aset'][$i] = Saldo::where('kode_akun', Akun::Aset)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['AsetLancar'][$i] = Saldo::where('kode_akun', Akun::AsetLancar)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['KasTunai'][$i] = Saldo::where('kode_akun', Akun::KasTunai)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasKecil'][$i] = Saldo::where('kode_akun', Akun::KasKecil)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasBank'][$i] = Saldo::where('kode_akun', Akun::KasBank)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasCek'][$i] = Saldo::where('kode_akun', Akun::KasCek)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['KasBG'][$i] = Saldo::where('kode_akun', Akun::KasBG)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['Persediaan'][$i] = Saldo::where('kode_akun', Akun::Persediaan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Perlengkapan'][$i] = Saldo::where('kode_akun', Akun::Perlengkapan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangDagang'][$i] = Saldo::where('kode_akun', Akun::PiutangDagang)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangKaryawan'][$i] = Saldo::where('kode_akun', Akun::PiutangKaryawan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangLain'][$i] = Saldo::where('kode_akun', Akun::PiutangLain)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PiutangTakTertagih'][$i] = Saldo::where('kode_akun', Akun::PiutangTakTertagih)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['AsuransiDimuka'][$i] = Saldo::where('kode_akun', Akun::AsuransiDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['SewaDimuka'][$i] = Saldo::where('kode_akun', Akun::SewaDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['PPNMasukan'][$i] = Saldo::where('kode_akun', Akun::PPNMasukan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['AsetTetap'][$i] = Saldo::where('kode_akun', Akun::AsetTetap)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['Tanah'][$i] = Saldo::where('kode_akun', Akun::Tanah)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Bangunan'][$i] = Saldo::where('kode_akun', Akun::Bangunan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Peralatan'][$i] = Saldo::where('kode_akun', Akun::Peralatan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;
			$akuns['Kendaraan'][$i] = Saldo::where('kode_akun', Akun::Kendaraan)->where('created_at', 'like', $request->data[0].'%')->first()->debet;

			$akuns['ADBangunan'][$i] = Saldo::where('kode_akun', Akun::ADBangunan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;
			$akuns['ADPeralatan'][$i] = Saldo::where('kode_akun', Akun::ADPeralatan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;
			$akuns['ADKendaraan'][$i] = Saldo::where('kode_akun', Akun::ADKendaraan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit * -1;

			$akuns['HutangJangkaPendek'][$i] = Saldo::where('kode_akun', Akun::HutangJangkaPendek)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['KartuKredit'][$i] = Saldo::where('kode_akun', Akun::KartuKredit)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangDagang'][$i] = Saldo::where('kode_akun', Akun::HutangDagang)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangPajak'][$i] = Saldo::where('kode_akun', Akun::HutangPajak)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['TaksiranUtangGaransi'][$i] = Saldo::where('kode_akun', Akun::TaksiranUtangGaransi)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangKonsinyasi'][$i] = Saldo::where('kode_akun', Akun::HutangKonsinyasi)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['PendapatanDimuka'][$i] = Saldo::where('kode_akun', Akun::PendapatanDimuka)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangPPN'][$i] = Saldo::where('kode_akun', Akun::HutangPPN)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['PPNKeluaran'][$i] = Saldo::where('kode_akun', Akun::PPNKeluaran)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Kewajiban'][$i] = Saldo::where('kode_akun', Akun::Kewajiban)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['HutangJangkaPanjang'][$i] = Saldo::where('kode_akun', Akun::HutangJangkaPanjang)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['HutangBank'][$i] = Saldo::where('kode_akun', Akun::HutangBank)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Modal'][$i] = Saldo::where('kode_akun', Akun::Modal)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['LabaTahan'][$i] = Saldo::where('kode_akun', Akun::LabaTahan)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['Prive'][$i] = Saldo::where('kode_akun', Akun::Prive)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;
			$akuns['BagiHasil'][$i] = Saldo::where('kode_akun', Akun::BagiHasil)->where('created_at', 'like', $request->data[0].'%')->first()->kredit;

			$akuns['Equitas'][$i] = $akuns['Modal'][$i] - $akuns['Prive'][$i] + $akuns['LabaTahan'][$i] + $akuns['BagiHasil'][$i];

			$akuns['Prive'][$i] = $akuns['Prive'][$i] * -1;

			$akuns['TotalKE'][$i] = $akuns['Equitas'][$i] + $akuns['Kewajiban'][$i];		
			
			foreach ($akuns as $x => $akun) {
				// $akuns[$x][$i] = number_format($akun[$i], 2, ',', '.');

				if($akun[$i] < 0){
					$akun[$i] = $akun[$i] * -1;
					$akuns[$x][$i] = '(Rp'.number_format($akun[$i], 2, ',', '.').')';

				}else{
					$akuns[$x][$i] = 'Rp'.number_format($akun[$i], 2, ',', '.');
				}
			}
		}

		return view('neraca.cetak', compact('akuns', 'size', 'tanggal', 'saldo'));
		// return $akuns;
	}
}
