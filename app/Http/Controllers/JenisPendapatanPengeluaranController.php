<?php

namespace App\Http\Controllers;

use App\JenisPendapatan;
use App\JenisPengeluaran;

use Illuminate\Http\Request;

class JenisPendapatanPengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis_pendapatans = JenisPendapatan::all();
        $jenis_pengeluarans = JenisPengeluaran::all();
        return view ('jenis_pendapatan_pengeluaran.index', compact('jenis_pendapatans', 'jenis_pengeluarans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeJenisPendapatan(Request $request)
    {
        $this->validate($request,[
            'jenis_pendapatan' => 'required|max:255',
        ]);

        $jenis_pendapatan = new JenisPendapatan();
        $jenis_pendapatan->nama = $request->jenis_pendapatan;
        if ($jenis_pendapatan->save()) {
            return redirect()->back()->with('sukses', 'tambah');
        } else {
            return redirect()->back()->with('gagal', 'tambah');
        }
    }

    public function storeJenisPengeluaran(Request $request)
    {
       $this->validate($request,[
            'jenis_pengeluaran' => 'required|max:255',
        ]);

        $jenis_pengeluaran = new JenisPengeluaran();
        $jenis_pengeluaran->nama = $request->jenis_pengeluaran;
        if ($jenis_pengeluaran->save()) {
            return redirect()->back()->with('sukses', 'tambah');
        } else {
            return redirect()->back()->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateJenisPendapatan(Request $request, $id)
    {
        $this->validate($request,[
            'jenis_pendapatan' => 'required|max:255',
        ]);

        $jenis_pendapatan = JenisPendapatan::find($id);
        $jenis_pendapatan->nama = $request->jenis_pendapatan;
        if ($jenis_pendapatan->update()) {
            return redirect()->back()->with('sukses', 'ubah');
        } else {
            return redirect()->back()->with('gagal', 'ubah');
        }
    }

    public function updateJenisPengeluaran(Request $request, $id)
    {   
        $this->validate($request,[
            'jenis_pengeluaran' => 'required|max:255',
        ]);

        $jenis_pengeluaran = JenisPengeluaran::find($id);
        $jenis_pengeluaran->nama = $request->jenis_pengeluaran;
        if ($jenis_pengeluaran->update()) {
            return redirect()->back()->with('sukses', 'ubah');
        } else {
            return redirect()->back()->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyJenisPendapatan($id)
    {
        try {
            $jenis_pendapatan = JenisPendapatan::find($id);
            if ($jenis_pendapatan->delete()) {
                return redirect('/jenis_pendapatan_pengeluaran')->with('sukses', 'hapus');
            } else {
                return redirect('/jenis_pendapatan_pengeluaran')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/jenis_pendapatan_pengeluaran')->with('gagal', 'hapus');
        }
    }

    public function destroyJenisPengeluaran($id)
    {
        try {
            $jenis_pengeluaran = JenisPengeluaran::find($id);
            if ($jenis_pengeluaran->delete()) {
                return redirect('/jenis_pendapatan_pengeluaran')->with('sukses', 'hapus');
            } else {
                return redirect('/jenis_pendapatan_pengeluaran')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/jenis_pendapatan_pengeluaran')->with('gagal', 'hapus');
        }
    }
}
