<?php

namespace App\Http\Controllers;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Jurnal;
use App\Bangunan;
use Illuminate\Http\Request;

class HartaTetapController extends Controller
{
	public function lastJson()
    {
        $harta_tetap = Jurnal::where('referensi', 'like', '%%%%/TDB/%%%%%%%')
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['harta_tetap' => $harta_tetap]);
    }

    public function index(){
        $harta_tetap = Akun::orWhere('kode', Akun::Tanah)->where('debet', '=', 0)->orWhere('kode', Akun::Bangunan)->where('debet', '=', 0)->get();
        $tambah_aset = Akun::orWhere('kode', Akun::Tanah)->orWhere('kode', Akun::Bangunan)->get();

        $tanah = Akun::where('kode', Akun::Tanah)->first();
        $bangunan = Bangunan::find(1);
        // $kases = Akun::where('kode', 'like',  '111%%%')->get();
        $kases = Akun::where('kode', 'like',  Akun::KasTunai)->orWhere('kode', 'like', Akun::KasBank)->get();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', '=', '1')->get();
        $BGs = BG::where('aktif', '=', '1')->get();
        
        // return count($tambah_aset);
        return view('harta_tetap.index', compact('kases', 'harta_tetap', 'tanah', 'bangunan', 'banks', 'ceks', 'BGs', 'tambah_aset'));
    }

    public function store(Request $request){
        // return $request->all();
        $jurnal_tnb = new Jurnal();
        $jurnal_kas = new Jurnal();
        $akun_tnb = Akun::where('kode', $request->akun)->first();

        $akun_tnb->debet += $request->nominal;
        $akun_tnb->created_at = $request->tanggal;
        $akun_tnb->update();

        if($request->akun == Akun::Bangunan){
            $tabel_bangunan = Bangunan::find(1);
            $tabel_bangunan->umur = $request->umur;
            $tabel_bangunan->harga = $request->nominal;
            $tabel_bangunan->nominal = $request->nominal;
            $tabel_bangunan->residu = $request->residu;
            $tabel_bangunan->created_at = $request->tanggal;
            $tabel_bangunan->update();
        }

        $jurnal_tnb->kode_akun = $request->akun;
        $jurnal_tnb->referensi = $request->kode_transaksi;
        $jurnal_tnb->debet = $request->nominal;
        $jurnal_tnb->keterangan = $request->keterangan;
        $jurnal_tnb->save();

        if($request->sumber_kas==Akun::KasTunai){
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->nominal;
            $akun_tunai->update();

            $jurnal_kas->kode_akun = Akun::KasTunai;
            $jurnal_kas->referensi = $request->kode_transaksi;
            $jurnal_kas->kredit = $request->nominal;
            $jurnal_kas->keterangan = $request->keterangan;
        }else{
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->nominal;
            $akun_bank->update();

            $bank = Bank::where('id', $request->bank_tujuan)->first();
            $bank->nominal -= $request->nominal;
            $bank->update();

            $jurnal_kas->kode_akun = Akun::KasBank;
            $jurnal_kas->referensi = $request->kode_transaksi;
            $jurnal_kas->kredit = $request->nominal;
            $jurnal_kas->keterangan = $request->keterangan;
        }

        $jurnal_kas->save();

        Arus::create([
            'nama' => Arus::BeliAset,
            'nominal' => $request->nominal
        ]);

        return redirect('/harta_tetap')->with('sukses', 'tambah');
    }

    public function storeAset(Request $request){
        // return $request->all();
        $jurnal_tnb = new Jurnal();
        $jurnal_kas = new Jurnal();
        $akun_tnb = Akun::where('kode', $request->akun)->first();

        $akun_tnb->debet += $request->nominal;
        $akun_tnb->created_at = $request->tanggal;
        $akun_tnb->update();

        if($request->akun == Akun::Bangunan){
            $tabel_bangunan = Bangunan::find(1);
            $tabel_bangunan->umur = $request->umur;
            $tabel_bangunan->harga += $request->nominal;
            $tabel_bangunan->nominal += $request->nominal;
            // $tabel_bangunan->residu = $request->residu;
            $tabel_bangunan->update();
        }

        $jurnal_tnb->kode_akun = $request->akun;
        $jurnal_tnb->referensi = $request->kode_transaksi;
        $jurnal_tnb->debet = $request->nominal;
        $jurnal_tnb->keterangan = $request->keterangan;
        $jurnal_tnb->save();

        if($request->sumber_kas==Akun::KasTunai){
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->nominal;
            $akun_tunai->update();

            $jurnal_kas->kode_akun = Akun::KasTunai;
            $jurnal_kas->referensi = $request->kode_transaksi;
            $jurnal_kas->kredit = $request->nominal;
            $jurnal_kas->keterangan = $request->keterangan;
        }else{
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->nominal;
            $akun_bank->update();

            $bank = Bank::where('id', $request->bank_tujuan)->first();
            $bank->nominal -= $request->nominal;
            $bank->update();

            $jurnal_kas->kode_akun = Akun::KasBank;
            $jurnal_kas->referensi = $request->kode_transaksi;
            $jurnal_kas->kredit = $request->nominal;
            $jurnal_kas->keterangan = $request->keterangan;
        }

        $jurnal_kas->save();

        Arus::create([
            'nama' => Arus::BeliAset,
            'nominal' => $request->nominal
        ]);

        return redirect('/harta_tetap')->with('sukses', 'update');
    }
}
