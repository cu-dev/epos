<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Jurnal;
use App\Paralaba;
use App\JurnalPenutup;

class BagiHasilController extends Controller
{
    public function lastJson()
    {
        $bagi_hasil = Jurnal::where('kode_akun', Akun::BagiHasil)
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['bagi_hasil' => $bagi_hasil]);
    }

    /* public function index()
    {
        $akun = Akun::where('kode', Akun::LabaTahan)->first();
        $jurnals = JurnalPenutup::where('kode_akun', Akun::BagiHasil)->get();
        $tariks = Jurnal::where('kode_akun', Akun::BagiHasil)->get();
        $banks = Bank::all();
        $ppns = Akun::where('kode', Akun::PPNMasukan)->orWhere('kode', Akun::PPNKeluaran)->get();

        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        
        $nominal_bank = array();
        foreach ($banks as $i => $bank) {
            $nominal_bank[$bank->id] = $bank->nominal;
        }

        return view('bagi_hasil.index', compact('akun', 'jurnals', 'banks', 'tariks', 'ppns', 'akun_tunai', 'akun_bank', 'nominal_bank'));
    } */

    public function index()
    {
        $akun = Akun::where('kode', Akun::LabaTahan)->first();
        $jurnals = JurnalPenutup::where('kode_akun', Akun::BagiHasil)->get();
        $banks = Bank::all();
        $ppns = Akun::where('kode', Akun::PPNMasukan)->orWhere('kode', Akun::PPNKeluaran)->get();

        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        
        $nominal_bank = array();
        foreach ($banks as $i => $bank) {
            $nominal_bank[$bank->id] = $bank->nominal;
        }

        return view('bagi_hasil.index', compact('akun', 'jurnals', 'banks', 'ppns', 'akun_tunai', 'akun_bank', 'nominal_bank'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'jurnals.' . $field;
        if ($request->field == 'akun_nama') {
            $field = 'akuns.nama';
        }

        $kode_akuns = [
            Akun::BagiHasil,
        ];

        $bebans = Jurnal
            ::select(
                'jurnals.id',
                'jurnals.debet',
                'jurnals.referensi',
                'jurnals.kode_akun',
                'akuns.nama as nama_akun'
            )
            ->leftJoin('akuns', 'akuns.kode', 'jurnals.kode_akun')
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.kode_akun', 'like', '%'.$request->search_query.'%')
                ->orWhere('akuns.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Jurnal
            ::select(
                'jurnals.id'
            )
            ->leftJoin('akuns', 'akuns.kode', 'jurnals.kode_akun')
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.kode_akun', 'like', '%'.$request->search_query.'%')
                ->orWhere('akuns.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($bebans as $i => $beban) {

            $beban->debet = Util::dk($beban->debet);

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bebans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request){
        // return $request->all();
        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $request->kode_transaksi.' - LBH',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BagiHasil,
            'referensi' => $request->kode_transaksi.' - LBH',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);
        
        Jurnal::create([
            'kode_akun' => Akun::BagiHasil,
            'referensi' => $request->kode_transaksi,
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        //update akun beban
        $akun_bH = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_bH->kredit -= $request->nominal;
        $akun_bH->update();

        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->nominal;
            $akun_tunai->update();
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->keterangan;
            $tunai->kredit = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
        }else{
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal -= $request->nominal;
            $bank->update();
            // create baris akun tunai
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode_transaksi;
            $jurnal_bank->keterangan = $request->keterangan.' Via Bank - '.$bank->nama_bank;
            $jurnal_bank->kredit = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
        }

        Arus::create([
            'nama' => Arus::BagiHasil,
            'nominal' => $request->nominal,
        ]);

        return redirect('/bagi_hasil')->with('sukses', 'tambah'); 
    }
}
