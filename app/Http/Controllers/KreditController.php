<?php

namespace App\Http\Controllers;

use Auth;
use App\Kredit;
use Illuminate\Http\Request;

class KreditController extends Controller
{
    public function index()
    {
        $kredits = Kredit::all();
        return view('kredit.index', compact('kredits'));
    }

    public function create()
    {
        
    }


    public function store(Request $request)
    {
        // return $request->all();
        $kredit = new Kredit();
        $kredit->nama = $request->atas_nama;
        $kredit->nomor = $request->nomor_kredit;
        $kredit->validasi = $request->validasi;
        $kredit->vcc = $request->vcc;
        $kredit->user_id = Auth::id();
        
        if ($kredit->save()) {
            return redirect('/bank')->with('sukses', 'tambah');
        } else {
            return redirect('/bank')->with('gagal', 'tambah');
        }
    }


    public function show(Kredit $kredit)
    {
        //
    }


    public function edit(Kredit $kredit)
    {
        //
    }

    public function update(Request $request, Kredit $kredit)
    {
        //
    }

    public function destroy(Kredit $kredit)
    {
        //
    }
}
