<?php

namespace App\Http\Controllers;

use App\Seller;
use App\Suplier;
use App\TransaksiPembelian;

use Illuminate\Http\Request;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $sellers = Seller::where('aktif', 1)->get();
        $seller_off = Seller::where('aktif', 0)->get();
        $supliers = Suplier::all();
        return view('seller.index', compact('sellers', 'supliers', 'seller_off'));
    } */

    public function index()
    {
        // apakah seharusnya hanya suplier yang aktif saja?
        $supliers = Suplier::all();
        return view('seller.index', compact('supliers'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'sellers.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $sellers = Seller
            ::select(
                'sellers.id',
                'sellers.nama',
                'sellers.alamat',
                'sellers.telepon',
                'sellers.email',
                'supliers.id as suplier_id',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'sellers.suplier_id')
            ->where('sellers.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('sellers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Seller
            ::select(
                'sellers.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'sellers.suplier_id')
            ->where('sellers.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('sellers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($sellers as $i => $seller) {

            $buttons['ubah'] = ['url' => ''];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $seller->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $sellers,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'sellers.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $sellers = Seller
            ::select(
                'sellers.id',
                'sellers.nama',
                'sellers.alamat',
                'sellers.telepon',
                'sellers.email',
                'supliers.id as suplier_id',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'sellers.suplier_id')
            ->where('sellers.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('sellers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Seller
            ::select(
                'sellers.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'sellers.suplier_id')
            ->where('sellers.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('sellers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('sellers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($sellers as $i => $seller) {
            $buttons['aktifkan'] = ['url' => ''];

            // return $buttons;
            $seller->buttons = $buttons;
        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $sellers,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supliers = Suplier::all();
        return view('seller.create', compact('supliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seller = new Seller;
        $seller->nama = $request->nama;
        $seller->alamat = $request->alamat;
        $seller->telepon = $request->telepon;
        $seller->email = $request->email;
        $seller->suplier_id = $request->suplier_id;

        if ($seller->save()) {
            return redirect('/seller')->with('sukses', 'tambah');
        } else {
            return redirect('/seller')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::find($id);
        $supliers = Suplier::all();
        return view('seller.edit', compact('seller', 'supliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::find($id);
        $seller->nama = $request->nama;
        $seller->alamat = $request->alamat;
        $seller->telepon = $request->telepon;
        $seller->email = $request->email;
        $seller->suplier_id = $request->suplier_id;

        if ($seller->save()) {
            return redirect('/seller')->with('sukses', 'ubah');
        } else {
            return redirect('/seller')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        // $status = 0;
        // try {
        //     $seller = Seller::find($id);
        //     $seller->delete();
        //     $status = 1;
        // } catch(\Illuminate\Database\QueryException $e) {
        //     return redirect('/seller')->with('gagal', 'hapus');
        // }

        // if($status == 1){
        //     return redirect('/seller')->with('sukses', 'hapus');
        // }else{
        //     return redirect('/seller')->with('gagal', 'hapus');
        // }
        // return $id;
        $seller = Seller::find($id);
        $cek_seller = TransaksiPembelian::where('seller_id', $id)->get();
        // return sizeof($cek_seller);
        if (sizeof($cek_seller) == 0 ) {
            $seller->aktif = 0;
            $seller->update();
            return redirect('/seller')->with('sukses', 'hapus');
        } else {
            return redirect('/seller')->with('gagal', 'hapus');
        }
    }

    public function aktif($id) {
        $seller = Seller::find($id);
        $seller->aktif = 1;
        $seller->update();
        
        return redirect('/seller')->with('sukses', 'aktif');
    }
}
