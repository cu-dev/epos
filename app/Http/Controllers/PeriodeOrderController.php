<?php

namespace App\Http\Controllers;

use Auth;
use App\PeriodeOrder;

use Illuminate\Http\Request;

class PeriodeOrderController extends Controller
{
    public function index()
	{
		$periode = PeriodeOrder::find(1);
		// return $periode;
		// return view('periode.index', compact('periode'));
		return view('periode.index', compact('periode'));
	}

	public function store(Request $request)
	{
		$periode = PeriodeOrder::find(1);
		$periode->user_id = Auth::id();
		$periode->bulan_down = $request->bulan_down;
		$periode->jumlah_down = $request->jumlah_down;
		$periode->bulan_up = $request->bulan_up;
		$periode->jumlah_up = $request->jumlah_up;

		if ($periode->update()) {
			return redirect('/periode')->with('sukses', 'ubah');
		} else {
			return redirect('/periode')->with('gagal', 'ubah');
		}
	}
}
