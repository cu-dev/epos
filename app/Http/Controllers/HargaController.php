<?php

namespace App\Http\Controllers;

use Auth;

use App\Item;
use App\Stok;
use App\Harga;
use App\RelasiSatuan;

use Illuminate\Http\Request;

class HargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();

        $harga_cek= Harga::where('item_kode', $request->item_kode)
                    ->where('jumlah', $request->jumlah)
                    ->where('satuan_id', $request->satuan_id)
                    ->where('harga', $request->harga)
                    ->where('status', '1')
                    ->count();

        if ($harga_cek > 0) {
            return redirect()->back()->with('harga_gagal', 'tambah');
        } else {
            $harga = new Harga();
            $harga->item_kode = $request->item_kode;
            $harga->jumlah = $request->jumlah;
            $harga->satuan_id = $request->satuan_id;
            $harga->harga = $request->harga;
            $harga->status = '1';
            if ($harga->save()) {
                return redirect()->back()->with('sukses', 'tambah');
            } else {
                return redirect()->back()->with('gagal', 'tambah');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ubah(Request $request, $id)
    {
        // return $request->all();
        $harga = Harga::find($id);
        $satuan_id = $harga->satuan_id;
        $satuan_terbesar = RelasiSatuan::where('item_kode', $request->item_kode)->orderBy('konversi', 'dsc')->first();
        $satuan_terkecil = RelasiSatuan::where('item_kode', $request->item_kode)->orderBy('konversi', 'asc')->first();
        $item = $harga->item;
        $items = Item::where('kode_barang', $item->kode_barang)->distinct()->get(['kode']);
        $limit_grosir = Item::where('kode_barang', $item->kode_barang)->first()->limit_grosir;
        $plus_grosir = Item::where('kode', $request->item_kode)->first()->plus_grosir;

        $satuan_eceran_grosir = 0;
        $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
        if($limit_grosir != 0){
            foreach ($relasi_satuan_item as $i => $relasi) {
                if($limit_grosir % $relasi->konversi == 0){
                    $satuan_eceran_grosir = $relasi->satuan_id;
                    break;
                }
            }
        }

        // return $satuan_eceran_grosir;
        if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
            $stoks = Stok::where('item_kode', $request->item_kode)->get();
            foreach ($stoks as $i => $stok) {
                if ($stok->aktif == 0) {
                    $stok->aktif = 1;
                    $stok->update();
                }
            }
        }

        foreach ($items as $i => $item) {
            $harga = Harga::where('item_kode', $item->kode)->where('satuan_id', $satuan_id)->first();

            $satuan = RelasiSatuan::where('item_kode', $item->kode)->WhereNotIn('satuan_id', [$harga->satuan_id])->orderBy('konversi', 'dsc')->get();

            if ($harga->satuan_id == $satuan_terbesar->satuan_id) {
                // Update harga grosir
                foreach ($satuan as $key => $satu) {
                    $konversi = $satuan_terbesar->konversi / $satu->konversi;
                    $harga_konversi = $request->grosir / $konversi;
                    $harga_konversi = ceil($harga_konversi / 100) * 100;

                    // return $satu;
                    $harga_satuan = Harga::where('item_kode', $item->kode)->where('satuan_id', $satu->satuan_id)->first();
                    $harga_satuan->grosir = $harga_konversi;
                    $harga_satuan->update();
                }
                $harga->eceran = $request->eceran;
                $harga->grosir = $request->grosir;
                $harga->update();
            } else if ($harga->satuan_id == $satuan_terkecil->satuan_id) {
                // Update harga eceran
                $is_grosir = 0;
                $satuan = RelasiSatuan::where('item_kode', $item->kode)->WhereNotIn('satuan_id', [$harga->satuan_id])->orderBy('konversi', 'asc')->get();
                // return $satuan;
                // if($satuan_eceran_grosir == $harga->satuan_id && $request->eceran <= $request->grosir ){
                //     $is_grosir = 1;
                //     $harga->eceran = $request->grosir + $plus_grosir;
                //     $harga->grosir = $request->grosir;
                //     $harga->update();
                // }else{
                //     $harga->eceran = $request->eceran;
                //     $harga->grosir = $request->grosir;
                //     $harga->update();
                // }

                $harga->eceran = $request->eceran;
                $harga->grosir = $request->grosir;
                $harga->update();
                // return [$satuan];

                $is_grosir = 0;
                // if($satuan_eceran_grosir == $harga->satuan_id){
                //     $is_grosir = 1;
                // }
                foreach ($satuan as $key => $satu) {
                    // $konversi = $satuan_terbesar->konversi / $satu->konversi;
                    $harga_konversi = $request->eceran * $satu->konversi;
                    $harga_satuan = Harga::where('item_kode', $item->kode)->where('satuan_id', $satu->satuan_id)->first();

                    if($satuan_eceran_grosir == $satu->satuan_id){
                        $is_grosir = 1;
                    }

                    if($is_grosir == 1 && $satuan_id != $harga_satuan->satuan_id){
                        $harga_satuan->eceran = $harga_satuan->grosir + $plus_grosir;
                        $harga_satuan->update();
                    }else{
                        $harga_satuan->eceran = $harga_konversi;
                        $harga_satuan->update();
                    }
                }
            } else {
                // Update harga tengah :D
                $harga->eceran = $request->eceran;
                $harga->grosir = $request->grosir;
                $harga->update();
            }

            $satuan_temp = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi', 'asc')->get();

            foreach ($satuan_temp as $key => $satu) {
                $harga_satuan = Harga::where('item_kode', $item->kode)->where('satuan_id', $satu->satuan_id)->first();

                if($harga_satuan->eceran < $harga_satuan->grosir){
                    $harga_satuan->eceran = $harga_satuan->grosir + $plus_grosir;
                    $harga_satuan->update();
                }
            }
        }

        return redirect()->back()->with('sukses', 'ubah_harga');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $harga = Harga::find($id);
        $harga->status = '0';
        if($harga->update()){
            return redirect()->back()->with('sukses', 'hapus');
        } else {
            return redirect()->back()->with('gagal', 'hapus');
        }
    }
}
