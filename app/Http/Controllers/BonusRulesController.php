<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Item;
use App\Util;
use App\Satuan;
use App\BonusRule;
use App\RelasiSatuan;
use App\RelasiBonusRule;

class BonusRulesController extends Controller
{
    /* public function index()
    {
        $rules = BonusRule::all();
        return view('bonus_rules.index', compact('rules'));
    } */

    public function index()
    {
        return view('bonus_rules.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'bonus_rules.' . $field;
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $status = strtolower($request->search_query);
        if (strpos($status, 'semua') !== false) {
            $status = 0;
        } else if (strpos($status, 'eceran') !== false) {
            $status = 1;
        } else if (strpos($status, 'groosir') !== false) {
            $status = 2;
        }

        $aktif = strtolower($request->search_query);
        if (strpos($aktif, 'aktif') !== false) {
            $aktif = 1;
        } else if (strpos($aktif, 'tidak') !== false) {
            $aktif = 0;
        }

        $bonus_rules = BonusRule
            ::select(
                'bonus_rules.nama',
                'bonus_rules.nominal',
                'bonus_rules.kelipatan',
                'bonus_rules.profit',
                'bonus_rules.jumlah',
                'bonus_rules.status',
                'bonus_rules.aktif',
                'users.nama as operator'
            )
            ->leftJoin('users', 'users.id', 'bonus_rules.user_id')
            ->where('bonus_rules.aktif', 1)
            ->where(function($query) use ($request, $status, $aktif) {
                $query
                ->where('bonus_rules.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.kelipatan', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.profit', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.jumlah', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.status', 'like', '%'.$status.'%')
                ->orWhere('bonus_rules.aktif', 'like', '%'.$aktif.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = BonusRule
            ::select(
                'bonus_rules.id'
            )
            ->leftJoin('users', 'users.id', 'bonus_rules.user_id')
            ->where('bonus_rules.aktif', 1)
            ->where(function($query) use ($request, $status, $aktif) {
                $query
                ->where('bonus_rules.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.kelipatan', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.profit', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.jumlah', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.status', 'like', '%'.$status.'%')
                ->orWhere('bonus_rules.aktif', 'like', '%'.$aktif.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($bonus_rules as $i => $bonus_rule) {

            $bonus_rule->mdt_nominal = Util::ewon($bonus_rule->nominal);
            $bonus_rule->mdt_profit = Util::ewon($bonus_rule->profit);
            $bonus_rule->mdt_jumlah = Util::angka($bonus_rule->jumlah);

            if ($bonus_rule->status == 0) {
                $bonus_rule->mdt_status = 'Semua Status Pelanggan';
            } else if ($bonus_rule->status == 1) {
                $bonus_rule->mdt_status = 'Eceran';
            } else if ($bonus_rule->status == 2) {
                $bonus_rule->mdt_status = 'Grosir';
            }

            if ($bonus_rule->kelipatan == 1) {
                $bonus_rule->mdt_kelipatan = 'Aktif';
            } else if ($bonus_rule->kelipatan == 0) {
                $bonus_rule->mdt_kelipatan = 'Tidak Aktif';
            }

            $buttons['detail'] = null;
            $buttons['ubah'] = null;
            $buttons['nonaktifkan'] = null;
            // $buttons['aktifkan'] = null;

            $buttons['detail'] = ['url' => url('rule-bonus/'.$bonus_rule->id.'/show')];
            $buttons['ubah'] = ['url' => ''];
            $buttons['nonaktifkan'] = ['url' => ''];
            // $buttons['aktifkan'] = ['url' => ''];

            // if ($bonus_rule->aktif == 1) {
            //     $buttons['nonaktifkan'] = ['url' => ''];
            // } else if ($bonus_rule->aktif == 0) {
            //     $buttons['aktifkan'] = ['url' => ''];
            // }

            // return $buttons;
            $bonus_rule->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bonus_rules,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'bonus_rules.' . $field;
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $status = strtolower($request->search_query);
        if (strpos($status, 'semua') !== false) {
            $status = 0;
        } else if (strpos($status, 'eceran') !== false) {
            $status = 1;
        } else if (strpos($status, 'groosir') !== false) {
            $status = 2;
        }

        $aktif = strtolower($request->search_query);
        if (strpos($aktif, 'aktif') !== false) {
            $aktif = 1;
        } else if (strpos($aktif, 'tidak') !== false) {
            $aktif = 0;
        }

        $bonus_rules = BonusRule
            ::select(
                'bonus_rules.nama',
                'bonus_rules.nominal',
                'bonus_rules.kelipatan',
                'bonus_rules.profit',
                'bonus_rules.jumlah',
                'bonus_rules.status',
                'bonus_rules.aktif',
                'users.nama as operator'
            )
            ->leftJoin('users', 'users.id', 'bonus_rules.user_id')
            ->where('bonus_rules.aktif', 0)
            ->where(function($query) use ($request, $status, $aktif) {
                $query
                ->where('bonus_rules.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.kelipatan', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.profit', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.jumlah', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.status', 'like', '%'.$status.'%')
                ->orWhere('bonus_rules.aktif', 'like', '%'.$aktif.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = BonusRule
            ::select(
                'bonus_rules.id'
            )
            ->leftJoin('users', 'users.id', 'bonus_rules.user_id')
            ->where('bonus_rules.aktif', 0)
            ->where(function($query) use ($request, $status, $aktif) {
                $query
                ->where('bonus_rules.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.kelipatan', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.profit', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.jumlah', 'like', '%'.$request->search_query.'%')
                ->orWhere('bonus_rules.status', 'like', '%'.$status.'%')
                ->orWhere('bonus_rules.aktif', 'like', '%'.$aktif.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($bonus_rules as $i => $bonus_rule) {

            $bonus_rule->mdt_nominal = Util::ewon($bonus_rule->nominal);
            $bonus_rule->mdt_profit = Util::ewon($bonus_rule->profit);
            $bonus_rule->mdt_jumlah = Util::angka($bonus_rule->jumlah);

            if ($bonus_rule->status == 0) {
                $bonus_rule->mdt_status = 'Semua Status Pelanggan';
            } else if ($bonus_rule->status == 1) {
                $bonus_rule->mdt_status = 'Eceran';
            } else if ($bonus_rule->status == 2) {
                $bonus_rule->mdt_status = 'Grosir';
            }

            if ($bonus_rule->kelipatan == 1) {
                $bonus_rule->mdt_kelipatan = 'Aktif';
            } else if ($bonus_rule->kelipatan == 0) {
                $bonus_rule->mdt_kelipatan = 'Tidak Aktif';
            }

            $buttons['detail'] = null;
            $buttons['ubah'] = null;
            // $buttons['nonaktifkan'] = null;
            $buttons['aktifkan'] = null;

            $buttons['detail'] = ['url' => url('rule-bonus/'.$bonus_rule->id.'/show')];
            $buttons['ubah'] = ['url' => ''];
            // $buttons['nonaktifkan'] = ['url' => ''];
            $buttons['aktifkan'] = ['url' => ''];

            // if ($bonus_rule->aktif == 1) {
            //     $buttons['nonaktifkan'] = ['url' => ''];
            // } else if ($bonus_rule->aktif == 0) {
            //     $buttons['aktifkan'] = ['url' => ''];
            // }

            // return $buttons;
            $bonus_rule->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bonus_rules,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request)
    {
        // return $request->all();
        $taken_name = BonusRule::where('nama', $request->nama)->get();
        if (count($taken_name) > 0) {
            return redirect('/rule-bonus')->with('gagal', 'tambah');
        }

        BonusRule::create([
            'nama' => $request->nama,
            'nominal' => $request->nominal,
            'kelipatan' => $request->kelipatan,
            'profit' => $request->profit,
            'jumlah' => $request->jumlah,
            'status' => $request->status,
            'kelipatan' => $request->kelipatan,
            'user_id' => Auth::user()->id,
            'aktif' => 0,
        ]);

        return redirect('/rule-bonus')->with('sukses', 'tambah');
    }

    public function delete($id)
    {
        $rule = BonusRule::find($id);
        $rule->aktif = 0;
        $rule->update();

        return redirect('/rule-bonus')->with('sukses', 'hapus');
    }

    public function aktif($id)
    {
        $rule = BonusRule::find($id);
        $rule->aktif = 1;
        $rule->update();

        $rules = BonusRule::whereNotIn('id', [$id])->where('aktif', 1)->get();
        foreach ($rules as $i => $rule) {
            $rule->aktif = 0;
            $rule->update();
        }

        return redirect('/rule-bonus')->with('sukses', 'aktif');
    }

    public function update($id, Request $request)
    {
        // return $request->all();
        $rule = BonusRule::find($id);
        $rule->nama = $request->nama;
        $rule->nominal = $request->nominal;
        $rule->kelipatan = $request->kelipatan;
        $rule->profit = $request->profit;
        $rule->jumlah = $request->jumlah;
        $rule->status = $request->status;
        $rule->kelipatan = $request->kelipatan;
        $rule->user_id = Auth::user()->id;

        $rule->update();

        return redirect('/rule-bonus')->with('sukses', 'ubah');
    }

    public function show($id)
    {
        $rule = BonusRule::find($id);
        $items = Item::where('bonus', 1)->where('konsinyasi', 3)->where('aktif', 1)->groupBy('kode_barang')->get();
        // $bonuses = Item::where('aktif', 1)->where('bonus', 1)->orWhere('konsinyasi', 3)->get();

        $relasis = RelasiBonusRule::where('bonus_rules_id', $id)->get();
        $satuans = Satuan::all();

        $relasi_satuan = array();
        foreach ($relasis as $i => $relasi) {
            $item = Item::find($relasi->bonus_id);
            $satuan_relasi = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi', 'dsc')->get();

            foreach ($satuan_relasi as $j => $satuan) {
                if($relasi->jumlah % $satuan->konversi == 0){
                    $hasil = $relasi->jumlah / $satuan->konversi;

                    $relasi_satuan[$i]['jumlah'] = $hasil;
                    $relasi_satuan[$i]['satuan'] = $satuan->satuan->kode;
                    $relasi_satuan[$i]['konversi'] = $satuan->konversi;
                    $relasi_satuan[$i]['satuan_id'] = $satuan->satuan_id;
                    break;
                }
            }
        };

        // return $satuans;
        return view('bonus_rules.show', compact('relasis', 'items', 'rule', 'satuans', 'relasi_satuan'));
    }

    public function ItemJson($rule, $id){
        $item_list = array();
        $satuan_list = array();

        $item = Item::find($id);
        $relasi_item = RelasiBonusRule::where('bonus_rules_id', $rule)->get();

        foreach ($relasi_item as $i => $val) {
            array_push($item_list, $val->bonus_id);
            $satuans = RelasiSatuan::where('item_kode', $val->bonus->kode)->orderBy('konversi', 'dsc')->get();

            foreach ($satuans as $j => $satuan) {
                if($val->jumlah % $satuan->konversi == 0){
                    array_push($satuan_list, $satuan->satuan_id);
                    break;
                }
            }
        }

        return response()->json([
            'item' => $item,
            'item_list' => $item_list,
            'satuan_list' => $satuan_list,
        ]);
    }

    public function storeRelasi(Request $request)
    {
        // return $request->all();
        $relasi = RelasiBonusRule::where('bonus_rules_id', $request->rule_id)->where('bonus_id', $request->item_id)->first();

        if($relasi == null){
            RelasiBonusRule::create([
                'bonus_rules_id' => $request->rule_id,
                'bonus_id' => $request->item_id,
                'jumlah' => $request->sum,
            ]);

            return redirect('/rule-bonus/'.$request->rule_id.'/show')->with('sukses', 'tambah');
        }else{
            return redirect('/rule-bonus/'.$request->rule_id.'/show')->with('gagal', 'tambah');
        }
    }

    public function deleteRelasi($id)
    {
        // return $id;
        $relasi = RelasiBonusRule::find($id);
        $rule_id = $relasi->bonus_rules_id;
        $relasi->delete();

        return redirect('/rule-bonus/'.$rule_id.'/show')->with('sukses', 'hapus');
    }

    public function updateRelasi($id, Request $request)
    {
        // return $request->all();
        $relasi = RelasiBonusRule::find($id);
        $rule_id = $relasi->bonus_rules_id;
        $relasi->jumlah = $request->sum;
        $relasi->update();

        return redirect('/rule-bonus/'.$rule_id.'/show')->with('sukses', 'ubah');
    }
}
