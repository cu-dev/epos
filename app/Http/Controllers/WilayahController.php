<?php

namespace App\Http\Controllers;

use Auth;

use App\Desa;
use App\Item;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Pelanggan;
use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class WilayahController extends Controller
{    

    public function index()
    {
        $provinsis = Provinsi::orderBy('nama')->get();
        $kabupatens = Kabupaten::orderBy('nama')->get();
        $kecamatans = Kecamatan::orderBy('nama')->get();
        $desas = Desa::orderBy('nama')->get();
        return view('wilayah.index', compact('provinsis', 'kabupatens', 'kecamatans', 'desas'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'desas.' . $field;
        if ($request->field == 'kecamatan_nama') {
            $field = 'kecamatans.nama';
        }
        if ($request->field == 'kabupaten_nama') {
            $field = 'kabupatens.nama';
        }
        if ($request->field == 'provinsi_nama') {
            $field = 'provinsis.nama';
        }
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $desas = Desa
            ::select(
                'desas.id as desa_id',
                'desas.nama as desa_nama',
                'kecamatans.id as kecamatan_id',
                'kecamatans.nama as kecamatan_nama',
                'kabupatens.id as kabupaten_id',
                'kabupatens.nama as kabupaten_nama',
                'provinsis.id as provinsi_id',
                'provinsis.nama as provinsi_nama',
                'users.nama as operator'
            )
            ->leftJoin('kecamatans', 'kecamatans.id', 'desas.kecamatan_id')
            ->leftJoin('kabupatens', 'kabupatens.id', 'kecamatans.kabupaten_id')
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'desas.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('desas.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kecamatans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Desa
            ::select(
                'desas.id'
            )
            ->leftJoin('kecamatans', 'kecamatans.id', 'desas.kecamatan_id')
            ->leftJoin('kabupatens', 'kabupatens.id', 'kecamatans.kabupaten_id')
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'desas.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('desas.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kecamatans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($desas as $i => $desa) {

            if ($desa->operator == null) {
                $desa->operator = ' - ';
            }

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $desa->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $desas,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'kecamatans.' . $field;
        if ($request->field == 'kabupaten_nama') {
            $field = 'kabupatens.nama';
        }
        if ($request->field == 'provinsi_nama') {
            $field = 'provinsis.nama';
        }
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $kecamatans = Kecamatan
            ::select(
                'kecamatans.id as kecamatan_id',
                'kecamatans.nama as kecamatan_nama',
                'kabupatens.id as kabupaten_id',
                'kabupatens.nama as kabupaten_nama',
                'provinsis.id as provinsi_id',
                'provinsis.nama as provinsi_nama',
                'users.nama as operator'
            )
            ->leftJoin('kabupatens', 'kabupatens.id', 'kecamatans.kabupaten_id')
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'kecamatans.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('kecamatans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Kecamatan
            ::select(
                'kecamatans.id'
            )
            ->leftJoin('kabupatens', 'kabupatens.id', 'kecamatans.kabupaten_id')
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'kecamatans.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('kecamatans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($kecamatans as $i => $kecamatan) {

            if ($kecamatan->operator == null) {
                $kecamatan->operator = ' - ';
            }

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $kecamatan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $kecamatans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt3(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'kabupatens.' . $field;
        if ($request->field == 'provinsi_nama') {
            $field = 'provinsis.nama';
        }
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $kabupatens = Kabupaten
            ::select(
                'kabupatens.id as kabupaten_id',
                'kabupatens.nama as kabupaten_nama',
                'provinsis.id as provinsi_id',
                'provinsis.nama as provinsi_nama',
                'users.nama as operator'
            )
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'kabupatens.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Kabupaten
            ::select(
                'kabupatens.id'
            )
            ->leftJoin('provinsis', 'provinsis.id', 'kabupatens.provinsi_id')
            ->leftJoin('users', 'users.id', 'kabupatens.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('kabupatens.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($kabupatens as $i => $kabupaten) {

            if ($kabupaten->operator == null) {
                $kabupaten->operator = ' - ';
            }

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $kabupaten->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $kabupatens,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt4(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'provinsis.' . $field;
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $provinsis = Provinsi
            ::select(
                'provinsis.id as provinsi_id',
                'provinsis.nama as provinsi_nama',
                'users.nama as operator'
            )
            ->leftJoin('users', 'users.id', 'provinsis.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Provinsi
            ::select(
                'provinsis.id'
            )
            ->leftJoin('users', 'users.id', 'provinsis.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('provinsis.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($provinsis as $i => $provinsi) {

            if ($provinsi->operator == null) {
                $provinsi->operator = ' - ';
            }

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $provinsi->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $provinsis,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function kabJson($id)
    {
        $kabupaten = Kabupaten::where('provinsi_id', $id)->get();
        return response()->json([
            'kabupaten' => $kabupaten
        ]);
    }

    public function kecJson($id)
    {
        $kecamatan = Kecamatan::where('kabupaten_id', $id)->get();
        return response()->json([
            'kecamatan' => $kecamatan
        ]);
    }

    public function desaJson($id)
    {
        $desa = Desa::where('kecamatan_id', $id)->get();
        return response()->json([
            'desa' => $desa
        ]);
    }

    public function camatJson($id)
    {
        $kecamatan = Kecamatan::find($id);
        return response()->json([
            'kecamatan' => $kecamatan
        ]);
    }

    public function patenJson($id)
    {
        $kabupaten = Kabupaten::find($id);
        return response()->json([
            'kabupaten' => $kabupaten
        ]);
    }    

    public function storeProvinsi(Request $request) {
        // return $request->all();
        $this->validate($request,[
            'provinsi' => 'required|max:255',
        ]);

        $provinsi = new Provinsi();
        $provinsi->nama = $request->provinsi;
        $provinsi->user_id = Auth::user()->id;
        if ($provinsi->save()) {
            return redirect('/wilayah')->with('sukses', 'tambah');
        } else {
            return redirect('/wilayah')->with('gagal', 'tambah');
        }
    }

    public function updateProvinsi(Request $request, $id) {
        // return $request->all();
        $this->validate($request,[
            'provinsi' => 'required|max:255',
        ]);

        $provinsi = Provinsi::find($id);
        $provinsi->nama = $request->provinsi;
        $provinsi->user_id = Auth::user()->id;
        if ($provinsi->update()) {
            return redirect('/wilayah')->with('sukses', 'ubah');
        } else {
            return redirect('/wilayah')->with('gagal', 'ubah');
        }
    }

    public function destroyProvinsi($id)
    {
        $provinsi = Provinsi::find($id);
        try {
            if ($provinsi->delete()) {
                return redirect('/wilayah')->with('sukses', 'hapus');
            } else {
                return redirect('/wilayah')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/wilayah')->with('gagal', 'hapus');
        }
    }

    public function storeKabupaten(Request $request) {
        // return $request->all();
        $this->validate($request,[
            'provinsi' => 'required|max:255',
            'kabupaten' => 'required|max:255'
        ]);

        $kabupaten = new Kabupaten();
        $kabupaten->provinsi_id = $request->provinsi;
        $kabupaten->nama = $request->kabupaten;
        $kabupaten->user_id = Auth::user()->id;
        if ($kabupaten->save()) {
            return redirect('/wilayah')->with('sukses', 'tambah');
        } else {
            return redirect('/wilayah')->with('gagal', 'tambah');
        }
    }

    public function updateKabupaten(Request $request, $id) {
        // return $request->all();
        $this->validate($request,[
            'provinsi' => 'required|max:255',
            'kabupaten' => 'required|max:255'
        ]);

        $kabupaten = Kabupaten::find($id);
        $kabupaten->provinsi_id = $request->provinsi;
        $kabupaten->nama = $request->kabupaten;
        $kabupaten->user_id = Auth::user()->id;
        if ($kabupaten->update()) {
            return redirect('/wilayah')->with('sukses', 'ubah');
        } else {
            return redirect('/wilayah')->with('gagal', 'ubah');
        }
    }

    public function destroyKabupaten($id)
    {
        $kabupaten = Kabupaten::find($id);
        try {
            if($kabupaten->delete()) {
                return redirect('/wilayah')->with('sukses', 'hapus');
            } else {
                return redirect('/wilayah')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/wilayah')->with('gagal', 'hapus');
        }
    }

    public function storeKecamatan(Request $request) {
        // return $request->all();
        $this->validate($request,[
            'kabupaten' => 'required|max:255',
            'kecamatan' => 'required|max:255'

        ]);

        $kecamatan = new Kecamatan();
        $kecamatan->kabupaten_id = $request->kabupaten;
        $kecamatan->nama = $request->kecamatan;
        $kecamatan->user_id = Auth::user()->id;
        if ($kecamatan->save()) {
            return redirect('/wilayah')->with('sukses', 'tambah');
        } else {
            return redirect('/wilayah')->with('gagal', 'tambah');
        }
    }

    public function updateKecamatan(Request $request, $id) {
        // return $request->all();
        $this->validate($request,[
            'kabupaten' => 'required|max:255',
            'kecamatan' => 'required|max:255'

        ]);

        $kecamatan = Kecamatan::find($id);
        $kecamatan->kabupaten_id = $request->kabupaten;
        $kecamatan->nama = $request->kecamatan;
        $kecamatan->user_id = Auth::user()->id;
        if ($kecamatan->update()) {
            return redirect('/wilayah')->with('sukses', 'tambah');
        } else {
            return redirect('/wilayah')->with('gagal', 'tambah');
        }
    }

    public function destroyKecamatan($id)
    {
        $kecamatan = Kecamatan::find($id);
        try {
            if($kecamatan->delete()) {
                return redirect('/wilayah')->with('sukses', 'hapus');
            } else {
                return redirect('/wilayah')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/wilayah')->with('gagal', 'hapus');
        }
    }

    public function storeDesa(Request $request) {
        // return $request->all();
        $this->validate($request,[
            'kecamatan' => 'required|max:255',
            'desa' => 'required|max:255'
        ]);

        $desa = new Desa();
        $desa->kecamatan_id = $request->kecamatan;
        $desa->nama = $request->desa;
        $desa->user_id = Auth::user()->id;
        if ($desa->save()) {
            return redirect('/wilayah')->with('sukses', 'tambah');
        } else {
            return redirect('/wilayah')->with('gagal', 'tambah');
        }
    }

    public function updateDesa(Request $request, $id) {
        // return $request->all();
        $this->validate($request,[
            'kecamatan' => 'required|max:255',
            'desa' => 'required|max:255'
        ]);

        $desa = Desa::find($id);
        $desa->kecamatan_id = $request->kecamatan;
        $desa->nama = $request->desa;
        $desa->user_id = Auth::user()->id;
        if ($desa->update()) {
            return redirect('/wilayah')->with('sukses', 'ubah');
        } else {
            return redirect('/wilayah')->with('gagal', 'ubah');
        }
    }

    public function destroyDesa($id)
    {
        $desa = Desa::find($id);
        try {
            if($desa->delete()) {
                return redirect('/wilayah')->with('sukses', 'hapus');
            } else {
                return redirect('/wilayah')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/wilayah')->with('gagal', 'hapus');
        }
    }
}
