<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use Datetime;

use App\BukaTahun;
use App\Saldo;
use App\Modal;
use App\Akun;
use App\Arus;
use App\Util;
use App\Jurnal;
use App\Perkap;
use App\Kendaraan;

use Illuminate\Http\Request;

class ArusController extends Controller
{
	public function index()
    {   
    	Util::akun();
    	$today = new Datetime();

    	$tanggal = (new Datetime())->modify('-1 month');

    	$saldo = Saldo::all();
    	$status = 0;

    	if(sizeof($saldo) == 0){
    		// $modal_awal = Modal::orderBy('created_at', 'asc')->first();
    		// $modal = Modal::whereMonth('created_at', $tanggal)->sum('nominal');
    		$sebelum = 'Awal Buka';
    		$kas_awal = 0;
    	}else{
    		$status = 1;
    		$kas_tunai_awal = Saldo::where('kode_akun', Akun::KasTunai)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
    		$kas_kecil_awal = Saldo::where('kode_akun', Akun::KasKecil)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
    		$kas_bank_awal = Saldo::where('kode_akun', Akun::KasBank)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
    		$kas_cek_awal = Saldo::where('kode_akun', Akun::KasCek)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
    		$kas_bg_awal = Saldo::where('kode_akun', Akun::KasBG)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;

    		$kas_awal = $kas_tunai_awal + $kas_kecil_awal + $kas_bank_awal + $kas_cek_awal + $kas_bg_awal;
    		$tanggalan = new Datetime();
    		$sebelum = $tanggalan->format('01/m/Y');
    	}
  		
  		$kas_tunai_now = Akun::where('kode', Akun::KasTunai)->first()->debet;
		$kas_kecil_now = Akun::where('kode', Akun::KasKecil)->first()->debet;
		$kas_bank_now = Akun::where('kode', Akun::KasBank)->first()->debet;
		$kas_cek_now = Akun::where('kode', Akun::KasCek)->first()->debet;
		$kas_bg_now = Akun::where('kode', Akun::KasBG)->first()->debet;

		$kas_akhir = $kas_tunai_now + $kas_kecil_now + $kas_bank_now + $kas_cek_now + $kas_bg_now;

		$data = $this->hitung($today->format('m'));

		$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();

    	// return $kas_awal;
    	return view('arus.index', compact('data', 'kas_awal', 'kas_akhir', 'today', 'saldo', 'sebelum'));
    }    

    public function hitung($date){
  //   	$penjualan = Jurnal::where('kode_akun', Akun::Penjualan)->whereMonth('created_at', $date)->sum('kredit');
	
		// $data['kenaikan'] = $data['penjualan'] + $data['retur_pembelian'] + $data['bayar_piutang'] + $data['jual_aset'] + $data['titipan'] + 
		// 					$data['modal'] + $data['pinjam_bank'] - $data['pembelian'] - $data['pembelian_aset'] - $data['bayar_kewajiban'] - 
		// 					$data['prive'] - $data['asuransi_dimuka'] - $data['sewa_dimuka'] - $data['piutang'] - $data['beban'] - $data['bagi_hasil'] - 
		// 					$data['perlengkapan'];

		$data['Modal'] = Arus::where('nama', Arus::Modal)->whereMonth('created_at', $date)->sum('nominal');
		$data['PendapatanLain'] = Arus::where('nama', Arus::PendapatanLain)->whereMonth('created_at', $date)->sum('nominal');
		$data['Deposito'] = Arus::where('nama', Arus::Deposito)->whereMonth('created_at', $date)->sum('nominal');
		$data['JBD'] = Arus::where('nama', Arus::JBD)->whereMonth('created_at', $date)->sum('nominal');
        $data['JualAset'] = Arus::where('nama', Arus::JualAset)->whereMonth('created_at', $date)->sum('nominal');
        $data['ReturPembelian'] = Arus::where('nama', Arus::ReturPembelian)->whereMonth('created_at', $date)->sum('nominal');
		$data['PembayaranPiutang'] = Arus::where('nama', Arus::PembayaranPiutang)->whereMonth('created_at', $date)->sum('nominal');

		$data['total_masuk'] = $data['Modal'] + $data['PendapatanLain'] + $data['Deposito'] + $data['JBD'] + $data['JualAset'] + $data['ReturPembelian'] + $data['PembayaranPiutang'];

		$data['BeliAset'] = Arus::where('nama', Arus::BeliAset)->whereMonth('created_at', $date)->sum('nominal');
		$data['Beban'] = Arus::where('nama', Arus::Beban)->whereMonth('created_at', $date)->sum('nominal');
		$data['PBD'] = Arus::where('nama', Arus::PBD)->whereMonth('created_at', $date)->sum('nominal');
		$data['PiutangLain'] = Arus::where('nama', Arus::PiutangLain)->whereMonth('created_at', $date)->sum('nominal');
		$data['BagiHasil'] = Arus::where('nama', Arus::BagiHasil)->whereMonth('created_at', $date)->sum('nominal');
		$data['Prive'] = Arus::where('nama', Arus::Prive)->whereMonth('created_at', $date)->sum('nominal');
        $data['ReturPenjualan'] = Arus::where('nama', Arus::ReturPenjualan)->whereMonth('created_at', $date)->sum('nominal');
        $data['Kewajiban'] = Arus::where('nama', Arus::Kewajiban)->whereMonth('created_at', $date)->sum('nominal');

		$data['total_keluar'] = $data['BeliAset'] + $data['Beban'] + $data['PBD'] + $data['PiutangLain'] + $data['BagiHasil'] + $data['Prive'] + $data['ReturPenjualan'] + $data['Kewajiban'];

		$data['kenaikan'] = $data['total_masuk'] - $data['total_keluar'];

		return $data;
    }

    public function tampil(Request $request)
    {   
    	// return $request->all();
    	$date = $request->data;
    	$tanggal = new Datetime($request->data);
    	$tanggal_cetak = (new Datetime($request->data))->format('m Y');
    	$akhir = (new Datetime($request->data))->modify('+1 month');
    	$cek_awal = (new Datetime($request->data))->modify('-1 month');

    	$saldo = Saldo::whereMonth('created_at', $cek_awal)->get();
    	$status = 0;

    	if(sizeof($saldo) == 0){
    		// $modal_awal = Modal::orderBy('created_at', 'asc')->first();
    		// $modal = Modal::whereMonth('created_at', $tanggal)->sum('nominal');
    		$sebelum = 'Awal Buka';
    		$kas_awal = 0;
    	}else{
    		$status = 1;
    		$kas_tunai_awal = Saldo::where('kode_akun', Akun::KasTunai)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_kecil_awal = Saldo::where('kode_akun', Akun::KasKecil)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_bank_awal = Saldo::where('kode_akun', Akun::KasBank)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_cek_awal = Saldo::where('kode_akun', Akun::KasCek)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_bg_awal = Saldo::where('kode_akun', Akun::KasBG)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;

    		$kas_awal = $kas_tunai_awal + $kas_kecil_awal + $kas_bank_awal + $kas_cek_awal + $kas_bg_awal;
    		$sebelum = $tanggal->format('01/m/Y');
    	}

  		$kas_tunai_akhir = Saldo::where('kode_akun', Akun::KasTunai)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_kecil_akhir = Saldo::where('kode_akun', Akun::KasKecil)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_bank_akhir = Saldo::where('kode_akun', Akun::KasBank)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_cek_akhir = Saldo::where('kode_akun', Akun::KasCek)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_bg_akhir = Saldo::where('kode_akun', Akun::KasBG)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;

		$tanggal_akhir = $tanggal->format('t/m/Y');
		$kas_akhir = $kas_tunai_akhir + $kas_kecil_akhir + $kas_bank_akhir + $kas_cek_akhir + $kas_bg_akhir;

		$data = $this->hitung($tanggal->format('m'));
		// return $data;
		$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();

    	// return $data;
    	return view('arus.tampil', compact('data', 'kas_awal', 'kas_akhir', 'tanggal_akhir', 'saldo', 'sebelum', 'tanggal_cetak', 'date'));
    }

    public function cetak(Request $request){
    	// return $request->all();

    	$date = $request->data;
    	$tanggal = new Datetime($request->data);
    	$tanggal_cetak = (new Datetime($request->data))->format('m Y');
    	$akhir = (new Datetime($request->data))->modify('+1 month');
    	$cek_awal = (new Datetime($request->data))->modify('-1 month');

    	$saldo = Saldo::whereMonth('created_at', $cek_awal)->get();
    	$status = 0;

    	if(sizeof($saldo) == 0){
            // $modal_awal = Modal::orderBy('created_at', 'asc')->first();
            // $modal = Modal::whereMonth('created_at', $tanggal)->sum('nominal');
            $sebelum = 'Awal Buka';
            $kas_awal = 0;
    	}else{
    		$status = 1;
    		$kas_tunai_awal = Saldo::where('kode_akun', Akun::KasTunai)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_kecil_awal = Saldo::where('kode_akun', Akun::KasKecil)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_bank_awal = Saldo::where('kode_akun', Akun::KasBank)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_cek_awal = Saldo::where('kode_akun', Akun::KasCek)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;
    		$kas_bg_awal = Saldo::where('kode_akun', Akun::KasBG)->whereMonth('created_at', $cek_awal->format('m'))->first()->debet;

    		$kas_awal = $kas_tunai_awal + $kas_kecil_awal + $kas_bank_awal + $kas_cek_awal + $kas_bg_awal;
    		$sebelum = $tanggal->format('01/m/Y');
    	}

  		$kas_tunai_akhir = Saldo::where('kode_akun', Akun::KasTunai)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_kecil_akhir = Saldo::where('kode_akun', Akun::KasKecil)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_bank_akhir = Saldo::where('kode_akun', Akun::KasBank)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_cek_akhir = Saldo::where('kode_akun', Akun::KasCek)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;
		$kas_bg_akhir = Saldo::where('kode_akun', Akun::KasBG)->whereMonth('created_at', $tanggal->format('m'))->first()->debet;

		$tanggal_akhir = $tanggal->format('t/m/Y');
		$kas_akhir = $kas_tunai_akhir + $kas_kecil_akhir + $kas_bank_akhir + $kas_cek_akhir + $kas_bg_akhir;

		$data = $this->hitung($tanggal->format('m'));

		$saldo = Saldo::select(DB::raw("DATE_FORMAT(created_at, '%d %m %Y') date_space, DATE_FORMAT(created_at, '%Y-%m-%d') date_strip"),
    					DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
    					->groupby('year','month')
    					->get();

    	// return $data;
    	return view('arus.cetak', compact('data', 'kas_awal', 'kas_akhir', 'tanggal_akhir', 'saldo', 'sebelum', 'tanggal_cetak', 'date'));
    }
}
