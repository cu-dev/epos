<?php

namespace App\Http\Controllers;

use Image;

use App\DataPerusahaan;

use Illuminate\Http\Request;

class DataPerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_perusahaan = DataPerusahaan::where('id', '1')->get()->last();
        // dd($data_perusahaan);
        return view('data_perusahaan.index', compact('data_perusahaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data_perusahaan = DataPerusahaan::find($request->id);

        $file_name = time().'.'.$request->logo->getClientOriginalExtension();
        $path = public_path('images/' . $file_name);

        Image::make($request->logo)->save($path);

        $data_perusahaan->logo = $file_name;

        if ($data_perusahaan->update()) {
            return back()->with('sukses', 'upload');
        } else {
            return back()->with('gagal', 'upload');
        }

    }
    
    public function edit()
    {
        $data_perusahaan = DataPerusahaan::where('id', '1')->get()->last();
        return view('data_perusahaan.edit', compact('data_perusahaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return $request->all();
         $this->validate($request,[
            'nama' => 'required|max:255',
            'alamat' => 'max:255',
            'email' => 'required|max:255',
            'telepon' => 'max:255',
            'npwp' => 'max:255',
        ]);

        // return $request->all();
        $data_perusahaan = DataPerusahaan::where('id', '1')->get()->last();

        $data_perusahaan->nama = $request->nama;
        $data_perusahaan->alamat = $request->alamat;
        $data_perusahaan->email = $request->email;

        $data_perusahaan->telepon = $request->telepon;
        $data_perusahaan->wa_perusahaan = $request->wa_perusahaan;
        $data_perusahaan->wa_penjualan = $request->wa_penjualan;

        $data_perusahaan->npwp = $request->npwp;
        $data_perusahaan->tanggal_npwp = $request->tanggal_npwp;

        $data_perusahaan->pkp = $request->pkp;
        $data_perusahaan->tanggal_pkp = $request->tanggal_pkp;

        if ($data_perusahaan->update()) {
            return redirect('/data_perusahaan')->with('sukses', 'ubah');
        } else {
            return redirect('/data_perusahaan')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
