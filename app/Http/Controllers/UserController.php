<?php

namespace App\Http\Controllers;

use Hash;
use DateTime;

use App\User;
use App\CashDrawer;
use App\Level;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        // $levels = Level::orderBy('kode')->get();
        // $user_on = User::where('status', 1)->orderBy('level_id')->get();
        // $user_off = User::where('status', 0)->get();
        $user_on = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $user_off = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 0)->get();

        // return view ('user.index', compact('user_on', 'user_off', 'levels'));
        return view ('user.index', compact('user_on', 'user_off'));
    } */

    public function index()
    {
        return view ('user.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'users.' . $field;
        if ($request->field == 'level_nama') {
            $field = 'levels.nama';
        }

        $users = User
            ::select(
                'users.id',
                'users.nama',
                'users.username',
                'users.telepon',
                'levels.nama as level_nama'
            )
            ->leftJoin('levels', 'levels.id', 'users.level_id')
            ->where('users.status', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('users.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.username', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('levels.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = User
            ::select(
                'users.id'
            )
            ->leftJoin('levels', 'levels.id', 'users.level_id')
            ->where('users.status', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('users.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.username', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('levels.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($users as $i => $user) {

            $buttons['detail'] = ['url' => url('user/show/'.$user->id)];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $user->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $users,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'users.' . $field;
        if ($request->field == 'level_nama') {
            $field = 'levels.nama';
        }

        $users = User
            ::select(
                'users.id',
                'users.nama',
                'users.username',
                'users.telepon',
                'levels.nama as level_nama'
            )
            ->leftJoin('levels', 'levels.id', 'users.level_id')
            ->where('users.status', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('users.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.username', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('levels.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = User
            ::select(
                'users.id'
            )
            ->leftJoin('levels', 'levels.id', 'users.level_id')
            ->where('users.status', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('users.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.username', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('levels.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($users as $i => $user) {

            $buttons['detail'] = ['url' => url('user/show/'.$user->id)];
            $buttons['aktifkan'] = ['url' => ''];

            // return $buttons;
            $user->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $users,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::orderBy('kode')->get();
        $users = User::all();
        return view ('user.create', compact('users', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
        $this->validate($request, [
            'email' => 'required||unique:users|string|max:255',
            'username' => 'required||unique:users|string|max:255',
        ]);
        // return $request->all();
        $user = new User();
        
        $user->nama = $request->nama;
        $user->username = $request->username;
        $password = $request->username.'12345';
        $user->password = Hash::make($password);
        $user->email = $request->email;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->telepon = $request->telepon;
        $user->alamat = $request->alamat;
        $user->foto = \App\User::foto_default;
        $user->level_id = $request->level_id;

        if ($user->save()) {
            $akun = User::latest()->first();
            if($request->level_id == 3);
                $cash_drawer = new CashDrawer();
                $cash_drawer->user_id = $akun->id;
                $cash_drawer->nominal = 0;
                $cash_drawer->save();

            return redirect('/user')->with('sukses', 'tambah');
        } else {
            return redirect('/user')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $levels = Level::orderBy('kode')->get();
        $user = User::find($id);
        return view ('user.show', compact('user', 'levels'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $levels = Level::orderBy('kode')->get();
        $user = User::find($id);
        $request->session()->forget(['sukses', 'error']);
        return view ('user.edit', compact('user', 'levels'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $this->validate($request,[
            'nama' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|max:255',
            'tanggal_lahir' => 'required|max:255',
            'telepon' => 'max:255',
            'alamat' => 'max:255',
            'level_id' => 'max:255',
        ]);

        $user = User::find($id);

        $user->nama = $request->nama;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->telepon = $request->telepon;
        $user->alamat = $request->alamat;
        $user->level_id = $request->level_id;

        if ($user->update()) {
            return redirect('/user')->with('sukses', 'ubah');
        } else {
            return redirect('/user')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id == 1){
            return redirect('/user')->with('gagal_admin', 'hapus');
        }else{
            $user = User::find($id);
            $user->status = 0;

            if ($user->update()) {
                return redirect('/user')->with('sukses', 'hapus');
            } else {
                return redirect('/user')->with('gagal', 'hapus');
            }
        }
    }

    public function reAktif($id)
    {
        $user = User::find($id);
        $user->status = 1;

        if ($user->update()) {
            return redirect('/user')->with('sukses', 'aktif');
        } else {
            return redirect('/user')->with('gagal', 'aktif');
        }
    }

    public function reset($id) {
        $user = User::find($id);
        $user->password =  Hash::make($user->username.'12345');
        if ($user->update()) {
            return redirect('/user')->with('sukses', 'reset');
        } else {
            return redirect('/user')->with('gagal', 'reset');
        }
    }
}

