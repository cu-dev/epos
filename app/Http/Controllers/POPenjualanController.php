<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;

use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Piutang;
use App\Pelanggan;
use App\CashDrawer;
use App\MoneyLimit;
use App\RelasiBundle;
use App\RelasiSatuan;
use App\TransaksiPenjualan;
use App\RelasiBonusPenjualan;
use App\RelasiTransaksiPenjualan;
use App\RelasiBonusRulesPenjualan;

use Illuminate\Http\Request;

class POPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        // $po_penjualans = TransaksiPenjualan::whereIn('status', ['po', 'vip'])->orderBy('created_at', 'desc')->get();
        $user = Auth::user();
        if (in_array($user->level_id, [3])) {
            $po_penjualans = TransaksiPenjualan::whereIn('status', ['po_eceran', 'po_grosir'])->where('user_id', $user->id)->orderBy('created_at', 'desc')->orderBy('id', 'desc')->get();
        } elseif (in_array($user->level_id, [4])) {
            $po_penjualans = TransaksiPenjualan::whereIn('status', ['po_grosir', 'po_vip'])->orderBy('created_at', 'desc')->orderBy('id', 'desc')->get();
        } else {
            $po_penjualans = TransaksiPenjualan::whereIn('status', ['po_eceran', 'po_grosir', 'po_vip'])->orderBy('created_at', 'desc')->orderBy('id', 'desc')->get();
        }

        return view('po_penjualan.index', compact('po_penjualans'));
    } */

    public function index()
    {
        return view('po_penjualan.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_penjualans.' . $field;
        if ($request->field == 'pelanggan_nama') {
            $field = 'pelanggans.nama';
        }

        $user = Auth::user();
        $po_penjualans = [];
        $count = 0;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $status = strtolower($request->search_query);
        if (strpos($status, 'prioritas') !== false) {
            $status = 'vip';
        }

        // Untuk kasir eceran
        if (in_array($user->level_id, [3])) {

            $po_penjualans = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id',
                    'transaksi_penjualans.created_at',
                    'transaksi_penjualans.kode_transaksi',
                    'transaksi_penjualans.status',
                    'pelanggans.nama as pelanggan_nama',
                    'pelanggans.level as pelanggan_level'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_eceran', 'po_grosir'])
                ->where('transaksi_penjualans.user_id', $user->id)
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->limit($request->data_per_halaman)
                ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
                ->orderBy($field, $order)
                ->get();

            $count = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_eceran', 'po_grosir'])
                ->where('transaksi_penjualans.user_id', $user->id)
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->count();

        }
        // Untuk kasir grosir
        else if (in_array($user->level_id, [4])) {

            $po_penjualans = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id',
                    'transaksi_penjualans.created_at',
                    'transaksi_penjualans.kode_transaksi',
                    'transaksi_penjualans.status',
                    'pelanggans.nama as pelanggan_nama',
                    'pelanggans.level as pelanggan_level'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_grosir', 'po_vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->limit($request->data_per_halaman)
                ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
                ->orderBy($field, $order)
                ->get();

            $count = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_grosir', 'po_vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->count();

        }
        // Untuk admin & owner
        else {

            $po_penjualans = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id',
                    'transaksi_penjualans.created_at',
                    'transaksi_penjualans.kode_transaksi',
                    'transaksi_penjualans.status',
                    'pelanggans.nama as pelanggan_nama',
                    'pelanggans.level as pelanggan_level'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_eceran', 'po_grosir', 'po_vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->limit($request->data_per_halaman)
                ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
                ->orderBy($field, $order)
                ->get();

            $count = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['po_eceran', 'po_grosir', 'po_vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%');
                })
                ->count();

        }

        foreach ($po_penjualans as $i => $po_penjualan) {

            if ($po_penjualan->pelanggan_nama != null) {
                $po_penjualan->pelanggan_nama = ucwords($po_penjualan->pelanggan_nama);
            } else {
                $po_penjualan->pelanggan_nama = ' - ';
            }

            if ($po_penjualan->status == 'po_vip') {
                $po_penjualan->mdt_status = 'Prioritas';
            } else {
                $po_penjualan->mdt_status = ucfirst(substr($po_penjualan->status, 3));
            }

            $buttons['detail'] = ['url' => url('po-penjualan/'.$po_penjualan->id)];

            if (in_array($user->level_id, [1, 2]) && $po_penjualan->status == 'po_grosir' && $po_penjualan->pelanggan_level == 'grosir') {
                $buttons['ganti_vip'] = ['url' => ''];
            } else {
                $buttons['ganti_vip'] = null;
            }

            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $po_penjualan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $po_penjualans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::distinct()->select('kode', 'nama')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::all();
        return view('po_penjualan.create', compact('items', 'pelanggans', 'satuans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        foreach ($request->item_kode as $i => $kode) {
            $data[$kode] = [
                'jumlah' => $request->jumlah[$i],
                'satuan' => $request->satuan_id[$i],
                'harga' => $request->harga[$i],
                'subtotal' => $request->subtotal[$i]
            ];
        }

        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $request->kode_transaksi;
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->status = 'po';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan_id;
        $transaksi_penjualan->user_id = Auth::id();

        if ($transaksi_penjualan->save()) {
            $transaksi = TransaksiPenjualan::all()->last();

            foreach ($request->item_kode as $i => $kode) {
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();

                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi->id;
                $relasi_transaksi_penjualan->jumlah = $data[$kode]['jumlah'];
                $relasi_transaksi_penjualan->satuan_id = $data[$kode]['satuan'];
                $relasi_transaksi_penjualan->harga = $data[$kode]['harga'];
                $relasi_transaksi_penjualan->subtotal = $data[$kode]['subtotal'];

                $relasi_transaksi_penjualan->save();
            }

            return redirect('/po-penjualan')->with('sukses', 'tambah');
        } else {
            return redirect('/po-penjualan')->with('gagal', 'tambah');
        }
    }

    public function tambahItem($id, Request $request)
    {
        $relasi_po_penjualans = new RelasiTransaksiPenjualan();
        $po_penjualans = TransaksiPenjualan::find($id);
        $item = Item::where('item_kode', $request->kode)->first();
        $bonus = Bonus::find($item->bonus_id);

        $relasi_po_penjualans->item_kode = $request->kode;
        $relasi_po_penjualans->transaksi_penjualan_id = $id;
        $relasi_po_penjualans->jumlah = $request->jumlah;
        $relasi_po_penjualans->satuan_id = $request->satuan;
        $relasi_po_penjualans->jumlah_bonus = floor($request->jumlah/$item->syarat_bonus);
        $relasi_po_penjualans->harga = $request->subtotal / $request->jumlah;
        $relasi_po_penjualans->subtotal = $request->subtotal;

        $po_penjualans->harga_total += $request->subtotal;

        $bonus->stok -= floor($request->jumlah/$item->syarat_bonus);

        $relasi_po_penjualans->save();
        $po_penjualans->update();
        $bonus->save();
    }

    public function updatePO($id, Request $request)
    {
        return $request->all();

        $transaksi = TransaksiPenjualan::find($id);

        $transaksi->status = 'grosir';
        $transaksi->jumlah_bayar = $request->jumlah_bayar;

        $transaksi->no_debit = $request->no_debit;
        $transaksi->no_cek = $request->no_cek;
        $transaksi->no_bg = $request->no_bg;

        $transaksi->nominal_tunai = $request->nominal_tunai;
        $transaksi->nominal_debit = $request->nominal_debit;
        $transaksi->nominal_cek = $request->nominal_cek;
        $transaksi->nominal_bg = $request->nominal_bg;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $banks = Bank::all();
        // $items = Item::distinct()->select('kode', 'nama')->get();
        $po_penjualan = TransaksiPenjualan::with('pelanggan')->where('id', $id)->whereIn('status', ['po_eceran', 'po_grosir', 'po_vip'])->first();
        if ($po_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $today = new DateTime();
            $today = $today->format('Y-m-d');

            $is_setoran_buka = false;
            if (Auth::user()->level_id == 3) {
                $cek_setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->where('created_at', 'like', $today.'%')->first();
                if(sizeof($cek_setoran_buka) > 0) $is_setoran_buka = true;
            }

            $satuans = Satuan::all();
            $relasi_po_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
            $item_list = Item::select(
                    DB::raw('kode'),
                    DB::raw('nama'),
                    DB::raw('diskon'),
                    DB::raw('aktif'),
                    DB::raw('konsinyasi'))
                ->where('konsinyasi', '!=', 2)
                // ->where('konsinyasi', 1)
                ->where('aktif', 1)
                ->groupBy('kode')
                ->orderBy('kode', 'asc')->get();

            $jumlah_item_asli = 0;
            $show_relasi_bundles = array();
            $index_show_relasi_bundles = 0;
            $is_nego = false;
            foreach ($relasi_po_penjualan as $i => $relasi) {
                if ($relasi->nego > 0) {
                    $is_nego = true;
                }
                if ($relasi->item->konsinyasi < 2) {
                    $jumlah_item_asli++;
                } else {
                    $relasi_bundles = RelasiBundle::where('item_parent', $relasi->item_kode)->get();
                    $jumlah_item_asli += $relasi_bundles->count();
                    $show_relasi_bundles[$index_show_relasi_bundles] = $relasi_bundles;
                    $index_show_relasi_bundles++;
                }

                $relasi_bonus_penjualan = RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $relasi->id)->get();
                if ($relasi_bonus_penjualan->count() <= 0) {
                    $relasi->bonuses = null;
                } else {
                    $bonuses = array();
                    foreach ($relasi_bonus_penjualan as $j => $rbj) {
                        // $bonuses[$j] = [
                        //     'bonus' => $rbj->bonus,
                        //     'jumlah' => $rbj->jumlah
                        // ];
                        $item_kode =  Item::find($rbj->bonus_id)->kode;
                        $satuan_bonus = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();
                        // return $satuan_bonus;
                        $text_jumlah = '';
                        $temp_jumlah = $rbj->jumlah;
                        foreach ($satuan_bonus as $a => $satuan) {
                            if ($temp_jumlah > 0) {
                                $jumlah_ = intval($temp_jumlah / $satuan->konversi);
                                if ($jumlah_ > 0) {
                                    $text_jumlah .= $jumlah_;
                                    $text_jumlah .= ' ';
                                    $text_jumlah .= $satuan->satuan->kode;
                                    $temp_jumlah = $temp_jumlah % $satuan->konversi;

                                    if ($a != sizeof($satuan_bonus) - 1 && $temp_jumlah > 0) $text_jumlah .= ' ';
                                }
                            }
                        }

                        $bonuses[$j] = [
                            'bonus' => $rbj->bonus,
                            'jumlah' => $text_jumlah
                        ];
                    }
                    $relasi->bonuses = $bonuses;
                }
            }

            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $id)->get();

            $kembali = 0;
            $kembali = $po_penjualan->jumlah_bayar - ($po_penjualan->nego_total>0?$po_penjualan->nego_total:$po_penjualan->harga_total) + $po_penjualan->potongan_penjualan - $po_penjualan->ongkos_kirim;

            $text_bayar = 'Bayar Pesanan Penjualan';
            if ($kembali < 0) $text_bayar = 'Kreditkan Pesanan Penjualan';

            $boleh_bayar = false;
            if (Auth::user()->level_id == 3) {
                if ($po_penjualan->pelanggan_id == null ||
                    ($po_penjualan->pelanggan->level == 'eceran' && $po_penjualan->status == 'po_eceran')) {
                    $boleh_bayar = true;
                }
            } else if (Auth::user()->level_id == 4) {
                if ($po_penjualan->pelanggan_id != null){
                    if ($po_penjualan->pelanggan->level == 'grosir' ||
                        ($po_penjualan->pelanggan->level == 'eceran' && $po_penjualan->status == 'po_grosir')) {
                        $boleh_bayar = true;
                    }
                }else{
                    $boleh_bayar = false;
                }
            }

            $pelanggan = $po_penjualan->pelanggan;
            if ($pelanggan != null) {
                $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $pelanggan->id)->get();
                $sisa_piutang = 0;
                $lewat_jatuh_tempo = 0;
                foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                    $temp_sisa_piutang = 0;
                    foreach ($transaksi_penjualan->piutang_dagang as $k => $piutang_dagang) {
                        $sisa_piutang += $piutang_dagang->sisa;
                        $temp_sisa_piutang += $piutang_dagang->sisa;
                    }

                    if ($lewat_jatuh_tempo <= 0 && $temp_sisa_piutang > 0) {
                        $jatuh_tempo = $transaksi_penjualan->jatuh_tempo;
                        $jatuh_tempo = str_replace('-', '', $jatuh_tempo);
                        $jatuh_tempo = intval($jatuh_tempo);

                        $hari_ini = new DateTime('NOW');
                        $hari_ini = $hari_ini->format('Y-m-d');
                        $hari_ini = str_replace('-', '', $hari_ini);
                        $hari_ini = intval($hari_ini);

                        if ($jatuh_tempo < $hari_ini) {
                            $lewat_jatuh_tempo = 1;
                        }
                    }
                }

                $batas_piutang = $pelanggan->limit_jumlah_piutang - $sisa_piutang;
                if ($batas_piutang < 0) {
                    $batas_piutang = 0;
                }

                $pelanggan->batas_piutang = $batas_piutang;
                $pelanggan->lewat_jatuh_tempo = $lewat_jatuh_tempo;
                $po_penjualan->pelanggan = $pelanggan;
            }

            return view('po_penjualan.show', compact('satuans', 'po_penjualan', 'relasi_po_penjualan', 'relasi_bonus_rules_penjualan', 'is_nego', 'kembali', 'jumlah_item_asli', 'show_relasi_bundles', 'item_list', 'boleh_bayar', 'is_setoran_buka', 'pelanggan', 'text_bayar'));
        }
    }

    public function gantiStatus($transaksi_penjualan_id)
    {
        $po_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->where('status', 'po_grosir')->first();
        if ($po_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $po_penjualan->status = 'po_vip';
            $po_penjualan->update();
            return redirect('po-penjualan/'.$transaksi_penjualan_id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateItem(Request $request, $transaksi_penjualan_id, $id)
    {
        $relasi_po_penjualans = RelasiTransaksiPenjualan::where('item_kode', $id)->where('transaksi_penjualan_id', $transaksi_penjualan_id)->first();
        $po_penjualans = TransaksiPenjualan::find($transaksi_penjualan_id);
        $item = Item::where('item_kode', $request->kode)->first();
        $bonus = Bonus::find($item->bonus_id);

        $selisih_subtotal = $relasi_po_penjualans->subtotal - $request->subtotal;
        $po_penjualans->harga_total -= $selisih_subtotal;

        $selisih_bonus = floor($request->jumlah/$item->syarat_bonus) - $relasi_po_penjualans->jumlah_bonus;

        $relasi_po_penjualans->jumlah = $request->jumlah;
        $relasi_po_penjualans->satuan_id = $request->satuan;
        $relasi_po_penjualans->jumlah_bonus = $selisih_bonus;
        $relasi_po_penjualans->harga = $request->subtotal / $request->jumlah;
        $relasi_po_penjualans->subtotal = $request->subtotal;

        $bonus->stok -= $selisih_bonus;

        $relasi_po_penjualans->update();
        $po_penjualans->update();
        $bonus->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hapusItem($transaksi_penjualan_id, $id)
    {
        $relasi_po_penjualans = RelasiTransaksiPenjualan::where('item_kode', $id)->where('transaksi_penjualan_id', $transaksi_penjualan_id)->first();
        $po_penjualans = TransaksiPenjualan::find($transaksi_penjualan_id);

        $po_penjualans->harga_total -= $relasi_po_penjualans->subtotal;

        $relasi_po_penjualans->delete();
        $po_penjualans->update();
    }

    public function hapus($transaksi_penjualan_id)
    {
        // return $transaksi_penjualan_id;
        $relasi_penjualans = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();

        foreach ($relasi_penjualans as $i => $relasi_penjualan) {
            $relasi_penjualan->delete();
        }

        TransaksiPenjualan::find($transaksi_penjualan_id)->delete();
        return redirect('po-penjualan')->with('sukses', 'hapus');
    }
}
