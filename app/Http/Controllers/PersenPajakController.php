<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\PersenPajak;

class PersenPajakController extends Controller
{
    public function index()
    {
    	$persen = PersenPajak::find(1);
		return view('persen_pajak.index', compact('persen'));
    }

    public function  update(Request $request){
    	// return $request->all();
    	$temp = $request->persen;
    	$temp = str_replace(',', '.', $temp);
    	$persen = PersenPajak::find(1);
        $persen->persen = $temp;
    	$persen->user_id = Auth::user()->id;
    	$persen->update();

		return redirect('/persen_pajak')->with('sukses', 'ubah');
    }
}
