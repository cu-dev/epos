<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\User;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\BonusRule;
use App\Pelanggan;
use App\CashDrawer;
use App\MoneyLimit;
use App\RelasiBonus;
use App\RelasiSatuan;
use App\RelasiBundle;
use App\PiutangDagang;
use App\TransaksiPenjualan;
use App\RelasiBonusPenjualan;
use App\RelasiTransaksiPenjualan;
use App\RelasiBonusRulesPenjualan;

use Illuminate\Http\Request;

class TransaksiGrosirVIPController extends Controller
{
    /*public function hargaJson($kode, $satuanId1, $jumlah1, $satuanId2, $jumlah2, $konversi1, $konversi2)
    {
        $item = Item::where('kode', $kode)->first();
        $limit_grosir = $item->limit_grosir;

        $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;
        $jumlah1 = 0;
        $jumlah2 = 0;
        $satuan1 = 0;
        $satuan2 = 0;
        $konversi1 = 0;
        $konversi2 = 0;
        foreach ($item->satuan_pembelians as $i => $satuan) {
            if ($total > 0) {
                if ($total >= $satuan->konversi) {
                    if ($jumlah1 <= 0) {
                        $jumlah1 = intval($total / $satuan->konversi);
                        $satuan1 = $satuan->satuan->id;
                        $konversi1 = $satuan->konversi;
                    } else {
                        $jumlah2 = intval($total / $satuan->konversi);
                        $satuan2 = $satuan->satuan->id;
                        $konversi2 = $satuan->konversi;
                    }

                    $total = $total % $satuan->konversi;
                }
            } else {
                break;
            }
        }

        $harga = null;
        $konversi = 0;
        if ($konversi1 > $konversi2) {
            if ($jumlah1 > 0) {
                $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1)->orderBy('jumlah', 'asc')->get()->last();
                $harga_eceran = $harga->eceran / $konversi1;
                $harga_grosir = $harga->grosir / $konversi1;
                $konversi = $konversi1;
            } else if ($jumlah2 > 0) {
                $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah2)->where('satuan_id', $satuan2)->orderBy('jumlah', 'asc')->get()->last();
                $harga_eceran = $harga->eceran / $konversi2;
                $harga_grosir = $harga->grosir / $konversi2;
                $konversi = $konversi2;
            }
        } else {
            if ($jumlah2 > 0) {
                $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah2)->where('satuan_id', $satuan2)->orderBy('jumlah', 'asc')->get()->last();
                $harga_eceran = $harga->eceran / $konversi2;
                $harga_grosir = $harga->grosir / $konversi2;
                $konversi = $konversi2;
            } else if ($jumlah1 > 0) {
                $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1)->orderBy('jumlah', 'asc')->get()->last();
                $harga_eceran = $harga->eceran / $konversi1;
                $harga_grosir = $harga->grosir / $konversi1;
                $konversi = $konversi1;
            }
        }

        // nggolet nego_min
        $nego_min = 0;
        $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
        foreach ($stoks as $i => $stok) {
            if ($total > 0) {
                if ($stok->jumlah < $total) {
                    $nego_min += round(1.1 * $stok->harga * $stok->jumlah, 2);
                    $total -= $stok->jumlah;
                } else {
                    $nego_min += round(1.1 * $stok->harga * $total, 2);
                    $total = 0;
                }
            } else {
                break;
            }
        }

        // nyari bonus
        $index_bonus = 0;
        $bonus = array();
        $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
        foreach ($relasi_bonus as $i => $relasi) {
            $syarat = $relasi->syarat;
            if ($total >= $syarat) {
                if ($index_bonus == 0) {
                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                } else {
                    if ($syarat != $bonus[$index_bonus - 1]['syarat']) {
                        $bonus = array();
                        $index_bonus = 0;
                    }

                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                }
            }
        }

        return response()->json(compact('limit_grosir', 'nego_min', 'bonus', 'harga', 'harga_eceran', 'harga_grosir', 'konversi'));
    }*/

    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->get()->first();
        // $hpp = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->first();
        if ($item == null) {
            return response()->json(compact('item'));
        }

        $hpp = null;
        $satuan = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi')->first()->satuan;
        $harga = Harga::where('item_kode', $item->kode)
                    ->where('jumlah', 1)
                    ->where('satuan_id', $satuan->id)
                    ->get()->last();
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $item->kode)->get();
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
        $today = date('Y-m-d');
        $stoktotal = 0;
        foreach ($stoks as $j => $stok) {
            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                $stoktotal += $stok->jumlah;
                if ($hpp == null) $hpp = $stok;
            }
        }
        // return $stoktotal;
        $item->stoktotal = $stoktotal;

        // return response()->json(['item' => $item, 'hpp' => $hpp, 'harga' => $harga]);
        return response()->json(compact('item', 'hpp', 'harga', 'relasi_bonus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::all();
        // $users = User::where('status', 1)->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $items = array();
        $temp_items = Item::distinct()->select('kode', 'nama', 'konsinyasi', 'aktif', 'bonus_jual')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $bonus_rules = BonusRule::where('aktif', 1)->get();

        $is_setoran_buka = false;
        if (Auth::user()->level_id == 3) {
            $cek_setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->where('created_at', 'like', $today.'%')->first();
            if(sizeof($cek_setoran_buka) > 0) $is_setoran_buka = true;
        }

        foreach ($temp_items as $i => $item) {
            if($item->aktif == 1) {
                $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('aktif', 1)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                    }
                }

                if ($stoktotal > 0 && $item->konsinyasi != 3) {
                    array_push($items, $item);
                }

                //cek item bonusnya boleh terpisah egak
                if ($stoktotal > 0 && $item->konsinyasi == 3) {
                    if ($item->bonus_jual == 1) {
                        array_push($items, $item);
                    } elseif ($item->bonus_jual == 0) {
                        $items_bonuses = Item::where('kode', $item->kode)->get();
                        $is_allowed = false;
                        $cek_bonus = [];
                        foreach ($items_bonuses as $i => $item_bonus) {
                            $cek_relasi_bonus = RelasiBonus::where('bonus_id', $item_bonus->id)->first();
                            if ($cek_relasi_bonus != null) {
                                $cek_bonus = $cek_relasi_bonus;
                                break;
                            }
                        }
                        
                        if ($cek_bonus != null) {
                            $item_cek_asli = Item::where('kode', $cek_bonus->item_kode)->first();
                            if($item_cek_asli->stoktotal <= 0 ) {
                                array_push($items, $item);
                            }
                        }
                    }
                }

                // Cari stoktotal untuk item bundle
                // Jika item bundle, cari stoktotal item bundle minimal
                if ($item->konsinyasi == 2) {
                    $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $i => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                $stoktotal += $stok->jumlah;
                            }
                        }
                        if ($stoktotal > 0) {
                            array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                        }
                    }
                    // return [$stoktotals, $relasi_bundles];

                    if (count($stoktotals) > 0 && count($stoktotals) == count($relasi_bundles)) {
                        // Cari stoktotal terkecil
                        $stoktotal_terkecil = $stoktotals[0];
                        foreach ($stoktotals as $i => $stoktotal) {
                            if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                                $stoktotal_terkecil = $stoktotal;
                            }
                        }
                        array_push($items, $item);
                    }
                }
            }
        }

        foreach ($pelanggans as $i => $pelanggan) {
            $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $pelanggan->id)->get();
            $sisa_piutang = 0;
            $lewat_jatuh_tempo = 0;
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                $temp_sisa_piutang = 0;
                foreach ($transaksi_penjualan->piutang_dagang as $k => $piutang_dagang) {
                    $sisa_piutang += $piutang_dagang->sisa;
                    $temp_sisa_piutang += $piutang_dagang->sisa;
                }

                if ($lewat_jatuh_tempo <= 0 && $temp_sisa_piutang > 0) {
                // if ($lewat_jatuh_tempo <= 0) {
                    $jatuh_tempo = $transaksi_penjualan->jatuh_tempo;
                    $jatuh_tempo = str_replace('-', '', $jatuh_tempo);
                    $jatuh_tempo = intval($jatuh_tempo);

                    $hari_ini = new Datetime('NOW');
                    $hari_ini = $hari_ini->format('Y-m-d');
                    $hari_ini = str_replace('-', '', $hari_ini);
                    $hari_ini = intval($hari_ini);

                    if ($jatuh_tempo < $hari_ini) {
                        $lewat_jatuh_tempo = 1;
                    }
                }
            }

            $batas_piutang = $pelanggan->limit_jumlah_piutang - $sisa_piutang;
            if ($batas_piutang < 0) {
                $batas_piutang = 0;
            }

            $pelanggan->batas_piutang = $batas_piutang;
            $pelanggan->lewat_jatuh_tempo = $lewat_jatuh_tempo;
        }

        return view('transaksi_grosir_vip.create', compact('items', 'users', 'satuans', 'pelanggans', 'banks', 'bonus_rules'));
    }

    public function createAgain($id)
    {
        $banks = Bank::all();
        // $users = User::all();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        // $users = User::where('status', 1)->get();
        $items = array();
        $temp_items = Item::distinct()->select('kode', 'nama', 'konsinyasi', 'aktif', 'bonus_jual')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $bonus_rules = BonusRule::where('aktif', 1)->get();

        $is_setoran_buka = false;
        if (Auth::user()->level_id == 3) {
            $cek_setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->where('created_at', 'like', $today.'%')->first();
            if(sizeof($cek_setoran_buka) > 0) $is_setoran_buka = true;
        }

        foreach ($temp_items as $i => $item) {
            if($item->aktif == 1) {
                $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('aktif', 1)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                    }
                }

                if ($stoktotal > 0 && $item->konsinyasi != 3) {
                    array_push($items, $item);
                }

                //cek item bonusnya boleh terpisah egak
                if ($stoktotal > 0 && $item->konsinyasi == 3) {
                    if ($item->bonus_jual == 1) {
                        array_push($items, $item);
                    } elseif ($item->bonus_jual == 0) {
                        $items_bonuses = Item::where('kode', $item->kode)->get();
                        $is_allowed = false;
                        $cek_bonus = [];
                        foreach ($items_bonuses as $i => $item_bonus) {
                            $cek_relasi_bonus = RelasiBonus::where('bonus_id', $item_bonus->id)->first();
                            if ($cek_relasi_bonus != null) {
                                $cek_bonus = $cek_relasi_bonus;
                                break;
                            }
                        }
                        
                        if ($cek_bonus != null) {
                            $item_cek_asli = Item::where('kode', $cek_bonus->item_kode)->first();
                            if($item_cek_asli->stoktotal <= 0 ) {
                                array_push($items, $item);
                            }
                        }
                    }
                }

                // Cari stoktotal untuk item bundle
                // Jika item bundle, cari stoktotal item bundle minimal
                if ($item->konsinyasi == 2) {
                    $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $i => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                $stoktotal += $stok->jumlah;
                            }
                        }
                        if ($stoktotal > 0) {
                            array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                        }
                    }
                    // return [$stoktotals, $relasi_bundles];

                    if (count($stoktotals) > 0 && count($stoktotals) == count($relasi_bundles)) {
                        // Cari stoktotal terkecil
                        $stoktotal_terkecil = $stoktotals[0];
                        foreach ($stoktotals as $i => $stoktotal) {
                            if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                                $stoktotal_terkecil = $stoktotal;
                            }
                        }
                        array_push($items, $item);
                    }
                }
            }
        }

        foreach ($pelanggans as $i => $pelanggan) {
            $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $pelanggan->id)->get();
            $sisa_piutang = 0;
            $lewat_jatuh_tempo = 0;
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                $temp_sisa_piutang = 0;
                foreach ($transaksi_penjualan->piutang_dagang as $k => $piutang_dagang) {
                    $sisa_piutang += $piutang_dagang->sisa;
                    $temp_sisa_piutang += $piutang_dagang->sisa;
                }

                if ($lewat_jatuh_tempo <= 0 && $temp_sisa_piutang > 0) {
                // if ($lewat_jatuh_tempo <= 0) {
                    $jatuh_tempo = $transaksi_penjualan->jatuh_tempo;
                    $jatuh_tempo = str_replace('-', '', $jatuh_tempo);
                    $jatuh_tempo = intval($jatuh_tempo);

                    $hari_ini = new Datetime('NOW');
                    $hari_ini = $hari_ini->format('Y-m-d');
                    $hari_ini = str_replace('-', '', $hari_ini);
                    $hari_ini = intval($hari_ini);

                    if ($jatuh_tempo < $hari_ini) {
                        $lewat_jatuh_tempo = 1;
                    }
                }
            }

            $batas_piutang = $pelanggan->limit_jumlah_piutang - $sisa_piutang;
            if ($batas_piutang < 0) {
                $batas_piutang = 0;
            }

            $pelanggan->batas_piutang = $batas_piutang;
            $pelanggan->lewat_jatuh_tempo = $lewat_jatuh_tempo;
        }

        $transaksi_penjualan = TransaksiPenjualan::where('id', $id)->where('status', 'po_vip')->first();
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            return view('transaksi_grosir_vip.create_again', compact('banks', 'users', 'items', 'satuans', 'pelanggans', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'bonus_rules'));
        }
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_penjualan_terakhir = TransaksiPenjualan::all()->last();
        if ($transaksi_penjualan_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPN/'.$tanggal;
        } else {
            $kode_transaksi_penjualan_terakhir = $transaksi_penjualan_terakhir->kode_transaksi;
            $tanggal_transaksi_penjualan_terakhir = substr($kode_transaksi_penjualan_terakhir, 9, 10);
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_penjualan_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_penjualan_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPN/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPN/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    public function simpanPO(Request $request)
    {
       // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        if ($request->nego_total_view > 0) $transaksi_penjualan->nego_total = $request->nego_total_view;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = 'po_vip';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        // $transaksi_penjualan->user_id = Auth::id();

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->jumlah_bayar != null) {
            $harga_total = $request->nego_total == null ? (float) $request->harga_total : (float) $request->nego_total;
            $ongkos_kirim = (float) $request->ongkos_kirim;
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            // if ($jumlah_bayar > $harga_total) {
            //     $nominal_tunai = $harga_total - $nominal_transfer - $nominal_cek - $nominal_bg - $nominal_kredit - $nominal_titipan;
            // }

            $transaksi_penjualan->ongkos_kirim = $ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        if ($transaksi_penjualan->save()) {
            $nego_total = 0;
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_grosir[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                if ($request->nego[$i] > 0) $nego_total += $request->nego[$i];
                else $nego_total += $request->subtotal[$i];
                
                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    if ($total >= $syarat) {
                        $jumlah_bonus = intval($total / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonus[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            if ($request->nego_total_view <= 0 || ($request->nego_total_view > 0 && $request->nego_total_view > $request->harga_akhir)) {
                $transaksi_penjualan->nego_total = $nego_total;
                $transaksi_penjualan->update();
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function ubahPO(Request $request, $id)
    {
       // return $request->all();
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        if ($request->nego_total_view > 0) $transaksi_penjualan->nego_total = $request->nego_total_view;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        // $transaksi_penjualan->user_id = Auth::id();

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->jumlah_bayar != null) {
            // $harga_total = $request->nego_total == null ? (float) $request->harga_total : (float) $request->nego_total;
            $ongkos_kirim = (float) $request->ongkos_kirim;
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            // if ($jumlah_bayar > $harga_total) {
            //     $nominal_tunai = $harga_total - $nominal_transfer - $nominal_cek - $nominal_bg - $nominal_kredit - $nominal_titipan;
            // }

            $transaksi_penjualan->ongkos_kirim = $ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $rtj) {
            RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $rtj->id)->delete();
            $rtj->delete();
        }

        if ($transaksi_penjualan->update()) {
            $nego_total = 0;
            foreach ($request->item_kode as $i => $kode) {
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_grosir[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                if ($request->nego[$i] > 0) $nego_total += $request->nego[$i];
                else $nego_total += $request->subtotal[$i];
                
                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    if ($total >= $syarat) {
                        $jumlah_bonus = intval($total / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonus[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            if ($request->nego_total_view <= 0 || ($request->nego_total_view > 0 && $request->nego_total_view > $request->harga_akhir)) {
                $transaksi_penjualan->nego_total = $nego_total;
                $transaksi_penjualan->update();
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            // return redirect('/po-penjualan')->with('sukses', 'ubah');
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'ubah');
        } else {
            // return redirect('/po-penjualan')->with('gagal', 'ubah');
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah');
        }
    }
}
