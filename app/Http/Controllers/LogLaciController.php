<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;
use DateInterval;

use App\Laci;
use App\User;
use App\LogLaci;

use Illuminate\Http\Request;

class LogLaciController extends Controller
{
    public function index()
    {
        $today = new DateTime();
        $logs = LogLaci::where('created_at', 'like', '%'.$today->format('Y-m-d').'%')->get();

        $user_owner = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 1)->orWhere('level_id', 2)->get();
        $owner_array = array();
        foreach ($user_owner as $i => $user) {
            array_push($owner_array, $user->id);
        }

        $user_eceran = User::where('level_id', 3)->get();
        $user_grosir = User::where('level_id', 4)->get();
        $user_gudang = User::where('level_id', 5)->get();

        $eceran_array = array();
        foreach ($user_eceran as $i => $user) {
            array_push($eceran_array, $user->id);
        }

        $grosir_array = array();
        foreach ($user_grosir as $i => $user) {
            array_push($grosir_array, $user->id);
        }

        $gudang_array = array();
        foreach ($user_gudang as $i => $user) {
            array_push($gudang_array, $user->id);
        }

        $option_array = array();
        $option_array = [
            0 => ['value' => 6, 'text' => 'Pembeli', 'id' => '', 'class' => ''],
            1 => ['value' => 1, 'text' => 'Owner/Admin', 'id' => 'CheckBoxList', 'class' => 'penjualan'],
            2 => ['value' => 3, 'text' => 'Kasir Eceran', 'id' => '', 'class' => 'penjualan'],
            3 => ['value' => 4, 'text' => 'Kasir Grosir', 'id' => '', 'class' => 'penjualan'],
            4 => ['value' => 5, 'text' => 'Gudang', 'id' => '', 'class' => ''],
            5 => ['value' => 7, 'text' => 'Laci Owner', 'id' => '', 'class' => '']
        ];

        // return $option_array;
        return view('log_laci.index', compact('logs', 'today', 'penjualan_array', 'owner_array', 'option_array', 'gudang_array', 'eceran_array', 'grosir_array'));
    }

    public function tampil(Request $request)
    {
        // return $request->all();
        $awal = new DateTime($request->awal);
        $akhir = new DateTime($request->akhir);
        $akhir = $akhir->add(new DateInterval('P1D'));
        $tanggal = $request->data;
        $checkbox = $request->user;

        $user_array = array();
        $users = User::whereIn('level_id', $request->user)->get();
        
        $user_owner = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 1)->orWhere('level_id', 2)->get();
        $owner_array = array();
        foreach ($user_owner as $i => $user) {
            array_push($owner_array, $user->id);
        }

        $user_eceran = User::where('level_id', 3)->get();
        $user_grosir = User::where('level_id', 4)->get();
        $user_gudang = User::where('level_id', 5)->get();

        $eceran_array = array();
        foreach ($user_eceran as $i => $user) {
            array_push($eceran_array, $user->id);
        }

        $grosir_array = array();
        foreach ($user_grosir as $i => $user) {
            array_push($grosir_array, $user->id);
        }

        $gudang_array = array();
        foreach ($user_gudang as $i => $user) {
            array_push($gudang_array, $user->id);
        }
        
        foreach ($users as $i => $val) {
            array_push($user_array, $val->id);
        }
        if(in_array(6, $request->user) || in_array(7, $request->user)){
            array_push($user_array, NULL);
        }

        $option_array = array();
        $option_array = [
            0 => ['value' => 6, 'text' => 'Pembeli', 'id' => '', 'class' => ''],
            1 => ['value' => 1, 'text' => 'Owner/Admin', 'id' => 'CheckBoxList', 'class' => 'penjualan'],
            2 => ['value' => 3, 'text' => 'Kasir Eceran', 'id' => '', 'class' => 'penjualan'],
            3 => ['value' => 4, 'text' => 'Kasir Grosir', 'id' => '', 'class' => 'penjualan'],
            4 => ['value' => 5, 'text' => 'Gudang', 'id' => '', 'class' => ''],
            5 => ['value' => 7, 'text' => 'Laci Owner', 'id' => '', 'class' => '']
        ];

        if($user_array == NULL){
            $user_array = [NULL, 1,2,3,4,5];
        }

        // dd($awal, $akhir);
        $logs = LogLaci::WhereIn('pengirim', $user_array)->orWhereIn('penerima', $user_array)->whereBetween('created_at', [$awal, $akhir])->orderBy('created_at', 'asc')->get();
        // return $logs;

        return view('log_laci.tampil', compact('logs', 'tanggal', 'penjualan_array', 'owner_array', 'option_array', 'checkbox', 'gudang_array', 'transfer_bank_array', 'gudang_array', 'eceran_array', 'grosir_array'));
    }

}
