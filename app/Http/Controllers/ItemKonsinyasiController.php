<?php

namespace App\Http\Controllers;

use DB;

use App\Item;
use App\Stok;
use App\Bonus;
use App\Satuan;
use App\Suplier;
use App\JenisItem;
use App\RelasiSatuan;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class ItemKonsinyasiController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$items = Item::select(
				DB::raw('kode'),
				DB::raw('nama'),
				DB::raw('sum(stoktotal) as stok')
			)
			->where('konsinyasi', 1)
			->groupBy('kode')
			->orderBy('kode', 'asc')->get();

		$jenis_items = JenisItem::all();
		$supliers = Suplier::all();
		return view('item_konsinyasi.index', compact('items',  'jenis_items', 'supliers'));
	}

	public function indexJson()
	{
		$items = Item::all();
		return $items->toJson();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$bonuses = Bonus::all();
		$jenis_items = JenisItem::all();
		$supliers = Suplier::all();
		return view('item_konsinyasi.create', compact('jenis_items','items', 'bonuses', 'supliers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// return $request->all();
		$item = new Item();
		
		$item->kode = $request->kode;
		$item->nama = $request->nama;
		$item->suplier_id = $request->suplier_id;
		$item->jenis_item_id = $request->jenis_item_id;
		$item->konsinyasi = $request->konsinyasi;
		$item->stoktotal = 0;
		$item->stok_limit = 0;
		$item->satuan_limit = 0;
		$item->diskon = 0;

		if ($item->save()) {
			return redirect('/item-konsinyasi')->with('sukses', 'tambah');
		} else {
			return redirect('/item-konsinyasi')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$satuans = Satuan::all();
		$relasi_satuans = RelasiSatuan::where('item_kode', $id)->get();
		$bonuses = Bonus::all();
		$item = Item::where('kode', $id)->first();
		$items = Item::where('kode', $id)->get();
		$stoks = Stok::where('item_kode', $id)->get();
		$stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode')->having('kode', '=', $id)->get()->first();
		return view('item_konsinyasi.show', compact('item', 'items', 'stok', 'satuans', 'relasi_satuans', 'stoks'));
		// return $stoks;
		// ,'satuans', 'bonuses', 
	}

	public function showJson($id)
	{
		$item = Item::find($id);
		return $item->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request, $id)
	{
		$bonuses = Bonus::all();
		$supliers = Suplier::all();
		$jenis_items = JenisItem::all();
		$item = Item::where('kode', $id)->first();
		$request->session()->forget(['sukses', 'error']);
		return view('item_konsinyasi.edit', compact('item', 'jenis_items', 'bonuses', 'supliers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function ubah(Request $request, $id)
	{
		$this->validate($request,[
			'kode' => 'required|max:255',
			'nama' => 'required|max:255',
			'jenis_item_id' => 'required|max:255',
			'konsinyasi' => 'max:255',
		]);

		if($request->bonus_id==NULL){
			$request->syarat_bonus=0;
		}

		$items = Item::where('kode', $id)->get();
		// return $items;
		foreach ($items as $a => $item) {
			$item->kode = $request->kode;
			$item->nama = $request->nama;
			$item->jenis_item_id = $request->jenis_item_id;
			$item->konsinyasi = $request->konsinyasi;

			$item->update();
		}
		return redirect('/item-konsinyasi')->with('sukses', 'ubah');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	// public function destroy($id)
	// {
	// 	$item = Item::find($id);
	// 	if ($item->delete()) {
	// 		return redirect('/item-konsinyasi')->with('sukses', 'hapus');
	// 	} else {
	// 		return redirect('/item-konsinyasi')->with('gagal', 'hapus');
	// 	}
	// }

	public function tambah_suplier($id)
	{
		$items = Item::where('kode', $id)->get();
		// $item = Item::where('kode', $id)->first();
		$suplier_id = array();

		foreach ($items as $a => $item) {
			array_push($suplier_id, $item->suplier->id);
		}

		$supliers = Suplier::whereNotIn('id', $suplier_id)->get();

		return view('item_konsinyasi.tambah_suplier', compact('items', 'item', 'supliers'));
	}

	public function store_suplier(Request $request, $item_kode){
		$item = Item::where('kode', $item_kode)->first();

		$new_item = new Item();

		$new_item->kode = $item->kode;
		$new_item->nama = $item->nama;
		$new_item->suplier_id = $request->suplier;
		$new_item->jenis_item_id = $item->jenis_item_id;
		$new_item->konsinyasi = $item->konsinyasi;

		if ($new_item->save()) {
			return redirect('/item-konsinyasi')->with('sukses', 'tambah_suplier');
		} else {
			return redirect('/item-konsinyasi')->with('gagal', 'tambah_suplier');
		}		
	}

	public function historyHargaBeli($id)
	{
		$items = Item::where('kode', $id)->get();
		$suplier_id = array();
		foreach ($items as $i => $item) {
			array_push($suplier_id, $item->suplier_id);
		}

		$supliers = Suplier::whereIn('id', $suplier_id)->get();
		$relasis = array();
		foreach ($supliers as $i => $suplier) {
			$item = Item::where('suplier_id', $suplier->id)->where('kode', $id)->first();
			$suplier->item = $item;
			$suplier->relasis = RelasiTransaksiPembelian::where('item_id', $item->id)->get();
		}

		return view('item_konsinyasi.history_harga_beli', compact('supliers'));
	}

}
