<?php

namespace App\Http\Controllers;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Modal;
use App\Jurnal;

use Illuminate\Http\Request;

class PriveController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$prives = Modal::where('nominal', '<', 0)->get();
		$banks = Bank::all();
		return view('prive.index', compact('prives', 'banks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'nominal' => 'required|max:255',
			'kas_id' => 'required|max:5',
			'keterangan' => 'required|max:255'
		]);

		$akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
		$akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
		// $akun_modal = Akun::where('kode', Akun::Modal)->first();
		$akun_prive = Akun::where('kode', Akun::Prive)->first();

		$modal = new Modal();
		$modal->nominal = $request->nominal * -1;
		$modal->keterangan = $request->keterangan;
		$modal->save();

		$jenis_kas = $request->kas_id == 'tunai' ? 'tunai' : 'bank';

		if ($jenis_kas == 'tunai') {
			$akun_kas_tunai->debet -= $request->nominal;
		} else if ($jenis_kas == 'bank') {
			$akun_kas_bank->debet -= $request->nominal;
		}
		// $akun_modal->kredit -= $request->nominal;
		$akun_prive->debet += $request->nominal;

		$jumlah_modal = Modal::where('nominal', '<', 0)->count();
		$referensi = Util::int4digit($jumlah_modal + 1) . '/PRVT';
		$created_at = substr($modal->created_at, 0, 10);
		$created_at = explode('-', $created_at);
		$referensi .= '/' . $created_at[2] . '/' . $created_at[1] . '/' . $created_at[0];

		Jurnal::create([
			'kode_akun' => $akun_prive->kode,
			'referensi' => $referensi,
			'keterangan' => 'Penambahan Prive',
			'debet' => $request->nominal
		]);

		if ($jenis_kas == 'tunai') {
			Jurnal::create([
				'kode_akun' => $akun_kas_tunai->kode,
				'referensi' => $referensi,
				'keterangan' => 'Penambahan Prive',
				'kredit' => $request->nominal
			]);
		} else if ($jenis_kas == 'bank') {
			$bank = Bank::find($request->kas_id);
			$bank->nominal -= $request->nominal;
			$bank->update();

			Jurnal::create([
				'kode_akun' => $akun_kas_bank->kode,
				'referensi' => $referensi,
				'keterangan' => 'Penambahan Prive via - '.$bank->nama_bank,
				'kredit' => $request->nominal
			]);
		}

		Arus::create([
			'nama' => Arus::Prive,
			'nominal' => $request->nominal
		]);

		if ($akun_kas_tunai->update() &&
			$akun_kas_bank->update() &&
			$akun_prive->update()) {
			return redirect('/prive')->with('sukses', 'tambah');
		} else {
			return redirect('/prive')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
