<?php

namespace App\Http\Controllers;

use Datetime;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Util;
use App\Laci;
use App\Jurnal;
use App\LogLaci;
use App\PiutangDagang;
use App\BayarPiutangDagang;
use App\TransaksiPenjualan;
use App\RelasiTransaksiPenjualan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PiutangDagangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $piutangs = PiutangDagang::where('sisa', '>', 0)->orderBy('created_at', 'desc')->get();
        $lunas_piutangs = PiutangDagang::where('sisa', '=', 0)->orderBy('created_at', 'desc')->get();

        return view('piutang_dagang.index', compact('piutangs', 'lunas_piutangs'));
    } */

    public function index()
    {
        return view('piutang_dagang.index');
    }

    public function mdt1(Request $request)
    {
        $piutangs = PiutangDagang
            ::where('sisa', '>', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = PiutangDagang
            ::where('sisa', '>', 0)
            ->count();

        foreach ($piutangs as $i => $piutang) {
            $piutang->nominal = Util::ewon($piutang->nominal);
            $piutang->sisa = Util::ewon($piutang->sisa);

            $piutang->kode_transaksi = $piutang->transaksi_penjualan->kode_transaksi;

            if ($piutang->transaksi_penjualan->pelanggan_id == null) {
                $piutang->pelanggan = ['nama' => ' - '];
            } else {
                $piutang->pelanggan = $piutang->transaksi_penjualan->pelanggan;
            }

            // if ($piutang->status == 'lunas') {
            //     $buttons['detail'] = ['url' => url('piutang-dagang/'.$piutang->id)];
            //     $buttons['bayar'] = null;
            //     $buttons['transfer'] = null;
            // } else {
                // $buttons['detail'] = null;
                $buttons['bayar'] = ['url' => url('piutang-dagang/'.$piutang->id)];
                $buttons['transfer'] = ['url' => url('piutang-dagang/transfer/'.$piutang->id)];
            // }

            // return $buttons;
            $piutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $piutangs,
        ]);
    }

    public function mdt2(Request $request)
    {
        $piutangs = PiutangDagang
            ::where('sisa', '=', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = PiutangDagang
            ::where('sisa', '=', 0)
            ->count();

        foreach ($piutangs as $i => $piutang) {
            $piutang->nominal = Util::ewon($piutang->nominal);

            $piutang->kode_transaksi = $piutang->transaksi_penjualan->kode_transaksi;

            if ($piutang->transaksi_penjualan->pelanggan_id == null) {
                $piutang->pelanggan = ['nama' => ' - '];
            } else {
                $piutang->pelanggan = $piutang->transaksi_penjualan->pelanggan;
            }

            $buttons['riwayat'] = ['url' => url('piutang-dagang/'.$piutang->id)];

            // return $buttons;
            $piutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $piutangs,
        ]);
    }

    public function lastJson()
    {
        $piutang = BayarPiutangDagang::all()->last();
        return response()->json(['piutang' => $piutang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        // return $request->all();
        $akuns = array();

        $bayar_piutang = new BayarPiutangDagang();
        $bayar_piutang->kode_transaksi = $request->kode_transaksi;
        $bayar_piutang->piutang_dagang_id = $id;
        $bayar_piutang->user_id = Auth::id();
        $bayar_piutang->jumlah_bayar = $request->jumlah_bayar;

        $bayar_piutang->bank_transfer = $request->bank_id;
        $bayar_piutang->bank_kartu = $request->bank_id;
        
        $bayar_piutang->no_transfer = $request->no_debit;
        $bayar_piutang->no_kartu = $request->no_kartu;
        $bayar_piutang->no_cek = $request->no_cek;
        $bayar_piutang->no_bg = $request->no_bg;

        $bayar_piutang->nominal_tunai = $request->nominal_tunai;
        $bayar_piutang->nominal_transfer = $request->nominal_debit;
        $bayar_piutang->nominal_kartu = $request->nominal_kartu;
        $bayar_piutang->nominal_cek = $request->nominal_cek;
        $bayar_piutang->nominal_bg = $request->nominal_bg;

        $bayar_piutang->jenis_kartu = $request->jenis_kartu;
        
        $piutang = PiutangDagang::find($id);
        $piutang->sisa -= $request->jumlah_bayar;

        $laci = Laci::find(1);
        if($request->nominal_tunai > 0){
            if (Auth::user()->level_id == 4) {
                $log = new LogLaci();
                $log->penerima = Auth::user()->id;
                $log->approve = 1;
                $log->status = 17;
                $log->nominal = $request->nominal_tunai;

                $laci->grosir += $request->nominal_tunai;
                $log->save();
                $laci->update();
            }elseif(Auth::user()->level_id == 2 || Auth::user()->level_id == 1){
                $log = new LogLaci();
                $log->penerima = Auth::user()->id;
                $log->approve = 1;
                $log->status = 17;
                $log->nominal = $request->nominal_tunai;

                $laci->owner += $request->nominal_tunai;
                $log->save();
                $laci->update();
            }
        }

        if($request->no_cek != NULL && $request->nominal_cek!=NULL){
            $xcek = new Cek();
            $xcek->nomor = $request->no_cek;
            $xcek->nominal = $request->nominal_cek;
            $xcek->aktif = 1;
            $xcek->save();
        }

        if($request->no_bg != NULL && $request->nominal_bg!=NULL){
            $xbg = new BG();
            $xbg->nomor = $request->no_bg;
            $xbg->nominal = $request->nominal_bg;
            $xbg->aktif = 1;
            $xbg->save();
        }

        if (!empty($request->bank_id)) {
            $banks = Bank::find($request->bank_id);
            $banks->nominal += $request->nominal_debit;
            $banks->update();
        }

        $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
        $akun_kas_tunai      = Akun::where('kode', Akun::KasTunai)->first();
        $akun_kas_bank       = Akun::where('kode', Akun::KasBank)->first();
        $akun_kas_cek        = Akun::where('kode', Akun::KasCek)->first();
        $akun_kas_bg         = Akun::where('kode', Akun::KasBG)->first();

        $akuns = [
            0 => ['kode_akun' => $akun_kas_tunai['kode'], 'nominal' => $request->nominal_tunai],
            1 => ['kode_akun' => $akun_kas_bank['kode'], 'nominal' => $request->nominal_debit],
            1 => ['kode_akun' => $akun_kas_bank['kode'], 'nominal' => $request->nominal_kartu],
            2 => ['kode_akun' => $akun_kas_cek['kode'], 'nominal' => $request->nominal_cek],
            3 => ['kode_akun' => $akun_kas_bg['kode'], 'nominal' => $request->nominal_bg],
            4 => ['kode_akun' => $akun_piutang_dagang['kode'], 'nominal' => -$request->jumlah_bayar],
        ];

        if ($bayar_piutang->save() && $piutang->update()) {
            for ($i = 0; $i < count($akuns); $i++) {
                if (!empty($akuns[$i]['nominal'])) {
                    $jurnal = new Jurnal();

                    $jurnal->kode_akun = $akuns[$i]['kode_akun'];
                    $jurnal->referensi = $request->kode_transaksi;
                    $jurnal->keterangan = 'Bayar Piutang Dagang';

                    $kode = substr($akuns[$i]['kode_akun'], 0, 1);
                    if ($kode == 1 || $kode == 5) {
                        if($akuns[$i]['nominal'] > 0) {
                            $jurnal->debet = abs($akuns[$i]['nominal']);
                            $akun = Akun::where('kode', $akuns[$i]['kode_akun'])->first();
                            $akun->debet += abs($akuns[$i]['nominal']);
                            $akun->update();
                        }else{
                            $jurnal->kredit = abs($akuns[$i]['nominal']);
                            $akun = Akun::where('kode', $akuns[$i]['kode_akun'])->first();
                            $akun->debet -= abs($akuns[$i]['nominal']);
                            $akun->update();
                        }
                    } else {
                        ($akuns[$i]['nominal'] > 0) ? $jurnal->kredit = abs($akuns[$i]['nominal']) : $jurnal->debet = abs($akuns[$i]['nominal']);
                    }

                    $jurnal->save();
                }
            }

            return redirect('/piutang-dagang/'.$id)->with('sukses', 'tambah');
        } else {
            return redirect('/piutang-dagang/'.$id)->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();
        $piutang = PiutangDagang::find($id);
        $bayar_piutangs = BayarPiutangDagang::where('piutang_dagang_id', $id)->get();
        // return 'aw';
        $laci = Laci::find(1);
        return view('piutang_dagang.show', compact('banks', 'bayar_piutangs', 'piutang', 'ceks', 'bgs', 'laci'));
    }

    public function transfer($id)
    {
        $piutang = PiutangDagang::find($id);

        return view('piutang_dagang.transfer', compact('piutang'));
    }

    public function transferStore(Request $request)
    {
        $piutang = PiutangDagang::find($request->piutang_id);
        $piutang->sisa -= $request->nominal;
        $piutang->update();

        $akun_piutang = Akun::where('kode', Akun::PiutangDagang)->first();
        $akun_piutang->debet -= $request->nominal;
        $akun_piutang->update();

        $laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $laba_ditahan->kredit -= $request->nominal;
        $laba_ditahan->update();

        Jurnal::create([
            'kode_akun' => Akun::BebanKerugianPiutang,
            'referensi' => $piutang->kode_transaksi,
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::PiutangDagang,
            'referensi' => $piutang->kode_transaksi,
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $piutang->kode_transaksi.' L/R',
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'debet' => $request->nominal
        ]);
        Jurnal::create([
            'kode_akun' => Akun::BebanKerugianPiutang,
            'referensi' => $piutang->kode_transaksi.' L/R',
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $piutang->kode_transaksi.' LDT',
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'debet' => $request->nominal
        ]);
        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $piutang->kode_transaksi.' LDT',
            'keterangan' => 'Transfer Piutang '.$piutang->kode_transaksi,
            'kredit' => $request->nominal
        ]);

        return redirect('/piutang-dagang')->with('sukses', 'transfer');
    }

    public function viewLog($piutang_id, $id)
    {
        $bayar_piutang = BayarPiutangDagang::find($id);
        return view('piutang_dagang.view_log', compact('bayar_piutang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
