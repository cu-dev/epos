<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use DateTime;
use DateInterval;

use App\Akun;
use App\Stok;
use App\Laci;
use App\User;
use App\Util;
use App\Saldo;
use App\Config;
use App\Jurnal;
use App\Perkap;
use App\Bangunan;
use App\Kendaraan;
use App\CashDrawer;
use App\BukaTahun;
use App\Http\Requests;
use App\PersediaanAwal;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class AuthController extends Controller
{
    public function getLogin()
    {        //depresiasi akhir bulan
        $today_ = new DateTime();
        $today = $today_->format('Y-m-d');
        $akhir = $today_->format('Y-m-t');

        // if ($today == $akhir) {
        //     Util::depresiasi_akhir();
        // }
        //depresiasi per/bulan
        Util::depresiasi();
        //buka tahun
        Util::fix_aset();
        //downgrade pelanggan
        Util::down_pelanggan();
        //upgrade pelanggan
        // Util::up_pelanggan();
        //untuk bikin saldo
        Util::make_saldo();

        $users = User::all();
        // foreach ($users as $i => $user) {
        //     $user->cetak_nota = 1;
        //     $user->update();
        // }

        if (Auth::check() && !session('sukses') && !session('gagal')) {
            return redirect('/');
        } else {
            return view('auth/login');
        }
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ]);

        $username = $request->username;
        $password = $request->password;

        $user = User::where('username', $username)->first();
        if ($user) {
            if ($user->status == 0 || $user->level_id == 6) {
                return redirect('/login')->with([
                    'gagal' => 'not_found',
                ]);
            } elseif (Hash::check($password, $user->password)) {
                Auth::login($user, false);
                // $this->persediaanAwal();
                return redirect('/login')->with('sukses', 'Berhasil');
            } else {
                return redirect('/login')->with([
                    'gagal' => 'not matched',
                    'username' => $username,
                    'password' => $password
                ]);
            }
        } else {
            return redirect('/login')->with([
                'gagal' => 'not found',
                'username' => $username,
                'password' => $password
            ]);
        }
    }

    public function getLogout()
    {
        // if(Auth::user->level_id == 3){
        //     $cash = CashDrawer::where();
        // }

            Auth::logout();
            return redirect('/');
    }

    private function persediaanAwal()
    {
        $date = date('m-d');
        $check = PersediaanAwal::all()->count();
        if (($check == 0) && ($date == '01-01')) {
            $jumlah_persediaan = Stok::all()->sum('harga');
            $persediaan_awal = new PersediaanAwal();
            $persediaan_awal->jumlah = $jumlah_persediaan;
            $persediaan_awal->save();
        } else {
            return;
        }
    }

    public function json()
    {
        $user = Auth::user();
        $cash_drawer = CashDrawer::where('user_id', $user->id)->first();
        $laci = Laci::find(1);
        return response()->json(compact('user', 'cash_drawer', 'laci'));
    }

}
