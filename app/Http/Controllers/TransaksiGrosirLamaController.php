<?php

namespace App\Http\Controllers;

use DB;
use Auth;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Pelanggan;
use App\CashDrawer;
use App\MoneyLimit;
use App\RelasiSatuan;
use App\PiutangDagang;
use App\RelasiBonus;
use App\ReturPenjualan;
use App\TransaksiPenjualan;
use App\RelasiBonusPenjualan;
use App\RelasiReturPenjualan;
use App\RelasiTransaksiPenjualan;

use Illuminate\Http\Request;

class TransaksiGrosirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksis = TransaksiPenjualan::where('status', 'grosir')->orderBy('created_at', 'desc')->get();
        return view('transaksi_grosir.index', compact('transaksis'));
    }

    public function lastJson()
    {
        $transaksi_penjualan = TransaksiPenjualan::all()->last();
        return response()->json(['transaksi_penjualan' => $transaksi_penjualan]);
    }

    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->get()->first();
        $hpp = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->first();
        $satuan = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi')->first()->satuan;
        $harga = Harga::where('item_kode', $item->kode)
                    ->where('jumlah', 1)
                    ->where('satuan_id', $satuan->id)
                    ->where('status', 1)->get()->last();
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $item->kode)->get();

        // return response()->json(['item' => $item, 'hpp' => $hpp, 'harga' => $harga]);
        return response()->json(compact('item', 'hpp', 'harga', 'relasi_bonus'));
    }

    public function hargaJson($kode, $satuanId1, $jumlah1, $satuanId2, $jumlah2)
    {
        $item = Item::where('kode', $kode)->first();
        $limit_grosir = $item->limit_grosir;

        $satuan1 = Satuan::find($satuanId1);
        $konversi1 = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $satuan1->id)->first();
        $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1->id)->where('status', 1)->orderBy('jumlah', 'asc')->get()->last();

        // nggolet nego_min
        $nego_min = 0;
        $total = $jumlah1 * $konversi1->konversi;
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->get();
        foreach ($stoks as $i => $stok) {
            if ($total > 0) {
                if ($stok->jumlah < $total) {
                    $nego_min += round(1.1 * $stok->harga * $stok->jumlah, 2);
                    $total -= $stok->jumlah;
                } else {
                    $nego_min += round(1.1 * $stok->harga * $total, 2);
                    $total = 0;
                }
            } else {
                break;
            }
        }

        // nyari bonus
        $index_bonus = 0;
        $bonus = array();
        $total = $jumlah1 * $konversi1->konversi;
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
        foreach ($relasi_bonus as $i => $relasi) {
            $syarat = $relasi->syarat;
            if ($total >= $syarat) {
                if ($index_bonus == 0) {
                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                } else {
                    if ($syarat != $bonus[$index_bonus - 1]['syarat']) {
                        $bonus = array();
                        $index_bonus = 0;
                    }

                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                }
            }
        }

        // return response()->json(['harga' => $harga, 'konversi1' => $konversi1, 'nego_min' => $nego_min]);
        return response()->json(compact('limit_grosir', 'harga', 'konversi1', 'nego_min', 'bonus'));
    }

    /*public function hargaJson($kode, $satuan, $jumlah)
    {
        $item = Item::where('kode', $kode)->first();
        $satuan = Satuan::find($satuan);
        $konversi = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $satuan->id)->first();
        $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah)->where('satuan_id', $satuan->id)->where('status', 1)->get()->last();
        $limit_grosir = $item->limit_grosir;

        // nggolet nego_min
        $nego_min = 0;
        $total = $jumlah * $konversi->konversi;
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->get();
        foreach ($stoks as $i => $stok) {
            if ($total > 0) {
                if ($stok->jumlah < $total) {
                    $nego_min += round(1.1 * $stok->harga * $stok->jumlah, 2);
                    $total -= $stok->jumlah;
                } else {
                    $nego_min += round(1.1 * $stok->harga * $total, 2);
                    $total = 0;
                }
            } else {
                break;
            }
        }

        // nyari bonus
        $index_bonus = 0;
        $bonus = array();
        $total = $jumlah * $konversi->konversi;
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
        foreach ($relasi_bonus as $i => $relasi) {
            $syarat = $relasi->syarat;
            if ($total >= $syarat) {
                if ($index_bonus == 0) {
                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                } else {
                    if ($syarat != $bonus[$index_bonus - 1]['syarat']) {
                        $bonus = array();
                        $index_bonus = 0;
                    }

                    $bonus[$index_bonus] = [
                        'bonus' => $relasi->bonus,
                        'syarat' => $syarat,
                        'jumlah' => intval($total / $syarat)
                    ];
                    $index_bonus++;
                }
            }
        }

        // return response()->json(['harga' => $harga, 'konversi' => $konversi, 'nego_min' => $nego_min]);
        return response()->json(compact('limit_grosir', 'harga', 'konversi', 'nego_min', 'bonus'));
    }*/

    public function hppJson($kode)
    {
        $stoks = Stok::where('item_kode', $kode)->get();
        return response()->json(['stoks' => $stoks]);
    }

    public function pelanggansJson()
    {
        $pelanggans = Pelanggan::all('id', 'kode', 'nama', 'titipan');
        return response()->json(compact('pelanggans'));
    }

    public function pelangganJson($id)
    {
        $pelanggan = Pelanggan::find($id);
        return response()->json(['pelanggan' => $pelanggan]);
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_penjualan_terakhir = TransaksiPenjualan::all()->last();
        if ($transaksi_penjualan_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TRAJ/'.$tanggal;
        } else {
            $kode_transaksi_penjualan_terakhir = $transaksi_penjualan_terakhir->kode_transaksi;
            $tanggal_transaksi_penjualan_terakhir = substr($kode_transaksi_penjualan_terakhir, 10, 10);
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_penjualan_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_penjualan_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TRAJ/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TRAJ/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::all();
        $items = Item::distinct()->select('kode', 'nama')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::all();

        return view('transaksi_grosir.create', compact('items', 'satuans', 'pelanggans', 'banks'));
    }

    public function createAgain($id)
    {
        $banks = Bank::all();
        $items = Item::distinct()->select('kode', 'nama')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::all();
        $transaksi_penjualan = TransaksiPenjualan::where('id', $id)->whereIn('status', ['po_eceran', 'po_grosir'])->first();
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            return view('transaksi_grosir.create_again', compact('banks', 'items', 'satuans', 'pelanggans', 'transaksi_penjualan', 'relasi_transaksi_penjualan'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request->all();

        $data  = array();
        $akuns = array();

        $laba = 0;
        $harga_beli = 0;
        $harga_jual = 0;
        $ppn_keluar = 0;
        $netto_jual = 0;

        $akun_aset = Akun::where('kode', Akun::Aset)->first();

        foreach ($request->item_kode as $i => $kode) {
            $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
            $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

            $hargas = Harga::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->where('jumlah', '<=', $request->jumlah[$i])->where('status', 1)->get()->last();
            $tmp_jual = $hargas->harga * $request->jumlah[$i];
            $harga_jual += $tmp_jual;

            for ($j = 0; $j < $jumlah; $j++) {
                $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
                $stoks->jumlah -= 1;
                $harga_beli += $stoks->harga;

                $items = Item::where('kode', $kode)->where('stoktotal', '>', 0)->first();
                $items->stoktotal -= 1;

                $stoks->save();
                $items->save();
            }

            $laba = $harga_jual - $harga_beli;

            $item = Item::where('kode', $kode)->first();
            $jumlah_bonus = floor($jumlah/$item->syarat_bonus);

            $bonus = Bonus::find($item->bonus_id);
            $bonus->stok -= $jumlah_bonus;
            $bonus->save();

            $data[$kode] = [
                'jumlah' => $jumlah,
                'jumlah_bonus' => $jumlah_bonus,
                'harga' => $request->harga[$i],
                'subtotal' => $request->subtotal[$i]
            ];
        }

        $transaksi_grosir = new TransaksiPenjualan();

        $transaksi_grosir->kode_transaksi = $request->kode_transaksi;
        $transaksi_grosir->harga_total = $request->harga_total;
        $transaksi_grosir->jumlah_bayar = $request->jumlah_bayar;
        $transaksi_grosir->pelanggan_id = $request->pelanggan;
        $transaksi_grosir->status = 'grosir';
        $transaksi_grosir->user_id = Auth::id();

        $transaksi_grosir->no_debit = $request->no_debit;
        $transaksi_grosir->no_cek = $request->no_cek;
        $transaksi_grosir->no_bg = $request->no_bg;

        $transaksi_grosir->nominal_tunai = $request->nominal_tunai;
        $transaksi_grosir->nominal_debit = $request->nominal_debit;
        $transaksi_grosir->nominal_cek = $request->nominal_cek;
        $transaksi_grosir->nominal_bg = $request->nominal_bg;

        $ppn_keluar = $harga_jual / 11;
        $netto_jual = $harga_jual - $ppn_keluar;

        $total_piutang = $request->harga_total - $request->jumlah_bayar;

        $akuns = [
            0 => ['kode_akun' => Akun::KasTunai, 'nominal' => $request->nominal_tunai],
            1 => ['kode_akun' => Akun::KasBank, 'nominal' => $request->nominal_debit],
            2 => ['kode_akun' => Akun::KasCek, 'nominal' => $request->nominal_cek],
            3 => ['kode_akun' => Akun::KasBG, 'nominal' => $request->nominal_bg],
            4 => ['kode_akun' => Akun::PiutangDagang, 'nominal' => $total_piutang],         
            5 => ['kode_akun' => Akun::Penjualan, 'nominal' => $netto_jual],
            6 => ['kode_akun' => Akun::PPNKeluaran, 'nominal' => $ppn_keluar],
            7 => ['kode_akun' => Akun::HPP, 'nominal' => -$harga_beli],
            8 => ['kode_akun' => Akun::Persediaan, 'nominal' => -$harga_beli]
        ];

        if (!empty($request->bank)) {
            $banks = Bank::find($request->bank);
            $banks->nominal += intval($request->nominal_debit);
            $banks->update();
        }

        if ($transaksi_grosir->save()) {
            $transaksi = TransaksiPenjualan::all()->last();

            foreach ($request->item_kode as $i => $kode) {
                $relasi_transaksi_grosir = new RelasiTransaksiPenjualan();
                $relasi_transaksi_grosir->item_kode = $kode;
                $relasi_transaksi_grosir->transaksi_penjualan_id = $transaksi->id;
                $relasi_transaksi_grosir->harga = $data[$kode]['harga'];
                $relasi_transaksi_grosir->jumlah = $data[$kode]['jumlah'];
                $relasi_transaksi_grosir->jumlah_bonus = $data[$kode]['jumlah_bonus'];
                $relasi_transaksi_grosir->subtotal = $data[$kode]['subtotal'];

                $relasi_transaksi_grosir->save();
            }

            if ($total_piutang <= 0) {
                $total_piutang = null;
            } else {
                $piutang = PiutangDagang::all()->last();

                if (empty($piutang)) {
                    $kode_piutang = '0001/PD/'.Util::printBulanSekarang('mm/yyyy');
                } else {
                    $mm_piutang = explode('/', $piutang->kode_transaksi)[2];
                    $yyyy_piutang = explode('/', $piutang->kode_transaksi)[3];
                    $bulan_piutang = $mm_piutang.'/'.$yyyy_piutang;

                    if ($bulan_piutang != Util::printBulanSekarang('mm/yyyy')) {
                        $kode_piutang = '0001/PD/'.Util::printBulanSekarang('mm/yyyy');
                    } else {
                        $nomor = intval(explode('/', $piutang->kode_transaksi)[0]) + 1;
                        $kode_piutang = Util::int4digit($nomor).'/PD/'.Util::printBulanSekarang('mm/yyyy');
                    }
                }

                $piutangs = new PiutangDagang();

                $piutangs->kode_transaksi = $kode_piutang;
                $piutangs->transaksi_penjualan_id = $transaksi->id;
                $piutangs->nominal = $total_piutang;
                $piutangs->sisa = $total_piutang;

                $piutangs->save();
            }

            for ($i = 0; $i < count($akuns); $i++) {
                if ($akuns[$i]['nominal'] != null) {
                    $jurnal = new Jurnal();
                    $akun = Akun::where('kode', $akuns[$i]['kode_akun'])->first();

                    $jurnal->kode_akun = $akuns[$i]['kode_akun'];

                    if ($i == 4) {
                        $jurnal->referensi = $kode_piutang;
                        $jurnal->keterangan = 'Piutang Dagang';
                    } else {
                        $jurnal->referensi = $request->kode_transaksi;
                        $jurnal->keterangan = 'Transaksi Grosir';
                    }

                    $kode = substr($akuns[$i]['kode_akun'], 0, 1);
                    if ($kode == 1 || $kode == 5) {
                        if ($akuns[$i]['nominal'] > 0) {
                            $jurnal->debet = abs($akuns[$i]['nominal']);
                            $akun->debet += abs($akuns[$i]['nominal']);
                            $akun_aset->debet += abs($akuns[$i]['nominal']);
                        } else {
                            $jurnal->kredit = abs($akuns[$i]['nominal']);
                            $akun->kredit += abs($akuns[$i]['nominal']);
                            $akun_aset->kredit += abs($akuns[$i]['nominal']);
                        }

                        $akun_aset->update();
                    } else {
                        if ($akuns[$i]['nominal'] > 0) {
                            $jurnal->kredit = abs($akuns[$i]['nominal']);
                            $akun->kredit += abs($akuns[$i]['nominal']);
                        } else {
                            $jurnal->debet = abs($akuns[$i]['nominal']);
                            $akun->debet += abs($akuns[$i]['nominal']);
                        }
                    }

                    $jurnal->save();
                    $akun->update();
                }
            }

            return redirect('/transaksi-grosir')->with('sukses', 'tambah');
        } else {
            return redirect('/transaksi-grosir')->with('gagal', 'tambah');
        }
    }

    public function simpanPO(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->nego_total == null ? $request->harga_total : $request->nego_total;
        $transaksi_penjualan->status = 'po_grosir';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->user_id = Auth::id();

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->harga[$i];
                $relasi_transaksi_penjualan->subtotal = $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = $request->nego[$i];
                $relasi_transaksi_penjualan->save();
                
                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    if ($jumlah >= $syarat) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function simpanPOEceran(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->nego_total == null ? $request->harga_total : $request->nego_total;
        $transaksi_penjualan->status = 'po_eceran';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->user_id = Auth::id();

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->harga[$i];
                $relasi_transaksi_penjualan->subtotal = $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = $request->nego[$i];
                $relasi_transaksi_penjualan->save();
                
                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    if ($jumlah >= $syarat) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function simpanPOPelangganBaru(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->nego_total == null ? $request->harga_total : $request->nego_total;
        $transaksi_penjualan->status = 'po_grosir';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->user_id = Auth::id();

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->harga[$i];
                $relasi_transaksi_penjualan->subtotal = $request->subtotal[$i];
                $relasi_transaksi_penjualan->save();
                
                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    if ($jumlah >= $syarat) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }
            return redirect('pelanggan/create/'.$transaksi_penjualan->id);
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function ubahPO(Request $request, $id)
    {
       // return $request->all();
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $transaksi_penjualan->harga_total = $request->nego_total == null ? $request->harga_total : $request->nego_total;
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->user_id = Auth::id();

        if ($request->jumlah_bayar != null) {
            $harga_total = $request->nego_total == null ? (float) $request->harga_total : (float) $request->nego_total;
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_kredit = (float) $request->nominal_kredit;
            $nominal_titipan = (float) $request->nominal_titipan;

            if ($jumlah_bayar > $harga_total) {
                $nominal_tunai = $harga_total - $nominal_transfer - $nominal_cek - $nominal_bg - $nominal_kredit - $nominal_titipan;
            }

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->bank_id = $request->bank_id;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->no_kredit = $request->no_kredit;
            $transaksi_penjualan->nominal_kredit = $nominal_kredit;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $rtj) {
            RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $rtj->id)->delete();
            $rtj->delete();
        }

        if ($transaksi_penjualan->update()) {
            foreach ($request->item_kode as $i => $kode) {
                $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->harga[$i];
                $relasi_transaksi_penjualan->subtotal = $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = $request->nego[$i];
                $relasi_transaksi_penjualan->save();
            }
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'ubah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah');
        }
    }

    public function bayar(Request $request, $id)
    {
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $transaksi_penjualan->status = 'grosir';
        $transaksi_penjualan->update();

        // ngurangi jumlah pada stoks dan stoktotal pada items
        $nominal_persediaan = 0;
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $relasi) {
            $items = Item::where('kode', $relasi->item_kode)->get();
            $jumlah = $relasi->jumlah;

            $stoks = Stok::where('item_kode', $relasi->item_kode)->get();
            foreach ($stoks as $j => $stok) {
                if ($jumlah > 0) {
                    if ($stok->jumlah <= $jumlah) {
                        // masih loop
                        $stok->jumlah = 0;
                        $stok->update();

                        $nominal_persediaan += $stok->harga * $stok->jumlah;
                        $jumlah -= $stok->jumlah;
                    } else {
                        // loop berhenti
                        $stok->jumlah -= $jumlah;
                        $stok->update();
                        
                        $nominal_persediaan += $stok->harga * $jumlah;
                        $jumlah = 0;
                    }
                } else {
                    break;
                }
            }

            foreach ($items as $j => $item) {
                $item->stoktotal -= $jumlah;
                $item->update();
            }
        }

        // variabel akun
        $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $akun_kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
        $akun_pendapatan_dimuka = Akun::where('kode', Akun::PendapatanDimuka)->first();
        $akun_ppn_keluaran = Akun::where('kode', Akun::PPNKeluaran)->first();
        $akun_penjualan = Akun::where('kode', Akun::Penjualan)->first();
        $akun_penjualan_kredit = Akun::where('kode', Akun::PenjualanKredit)->first();
        $akun_hpp = Akun::where('kode', Akun::HPP)->first();
        $pelanggan = Pelanggan::find($transaksi_penjualan->pelanggan_id);

        // variabel perhitungan
        $harga_total = $transaksi_penjualan->harga_total;
        $ongkos_kirim = $transaksi_penjualan->ongkos_kirim;
        $bank_id = $transaksi_penjualan->bank_id;
        $no_transfer = $transaksi_penjualan->no_transfer;
        $no_cek = $transaksi_penjualan->no_cek;
        $no_bg = $transaksi_penjualan->no_bg;
        $no_kredit = $transaksi_penjualan->no_kredit;
        $nominal_tunai = $transaksi_penjualan->nominal_tunai;
        if ($nominal_tunai == null) $nominal_tunai = 0;
        $nominal_transfer = $transaksi_penjualan->nominal_transfer;
        if ($nominal_transfer == null) $nominal_transfer = 0;
        $nominal_cek = $transaksi_penjualan->nominal_cek;
        if ($nominal_cek == null) $nominal_cek = 0;
        $nominal_bg = $transaksi_penjualan->nominal_bg;
        if ($nominal_bg == null) $nominal_bg = 0;
        $nominal_kredit = $transaksi_penjualan->nominal_kredit;
        if ($nominal_kredit == null) $nominal_kredit = 0;
        $nominal_titipan = $transaksi_penjualan->nominal_titipan;
        if ($nominal_titipan == null) $nominal_titipan = 0;
        $jumlah_bayar = $nominal_tunai + $nominal_transfer + $nominal_cek + $nominal_bg + $nominal_kredit + $nominal_titipan;
        $nominal_ppn_keluaran = $nominal_persediaan / 10;
        $nominal_utang = 0;
        $nominal_penjualan = $harga_total - $nominal_kredit - $nominal_ppn_keluaran;
        $nominal_penjualan_kredit = $nominal_kredit;

        $bank = null;
        $cek = null;
        $bg = null;
        $piutang_dagang = null;
        $keterangan = 'Transaksi Penjualan Grosir';

        // proses update saldo akun -> insert/update pembayaran
        if ($nominal_tunai > 0) {
            $akun_kas_tunai->debet += $nominal_tunai;
            $akun_kas_tunai->update();
        }

        if ($nominal_transfer > 0) {
            $akun_kas_bank->debet += $nominal_transfer;
            $akun_kas_bank->update();
            // update saldo bank
            $bank = Bank::find($bank_id);
            $bank->nominal += $nominal_transfer;
            $bank->update();
            // update keterangan
            $keterangan .= "\n" . $bank->nama_bank . ' [' . $bank->no_rekening . '] - ' . $no_transfer;
        }

        if ($nominal_cek > 0) {
            $akun_kas_cek->debet += $nominal_cek;
            $akun_kas_cek->update();
            // insert cek
            $cek = Cek::create([
                'nomor' => $no_cek,
                'nominal' => $nominal_cek,
                'aktif' => 1
            ]);
        }

        if ($nominal_bg > 0) {
            $akun_kas_bg->debet += $nominal_bg;
            $akun_kas_bg->update();
            // insert bg
            $bg = BG::create([
                'nomor' => $no_bg,
                'nominal' => $nominal_bg,
                'aktif' => 1
            ]);
        }

        if ($nominal_kredit > 0) {
            $akun_piutang_dagang->debet += $nominal_kredit;
            $akun_piutang_dagang->update();
            // insert piutang_dagang
            $piutang_dagang = PiutangDagang::create([
                'kode_transaksi' => $transaksi_penjualan->kode_transaksi,
                'transaksi_penjualan_id' => $transaksi_penjualan->id,
                'nominal' => $nominal_kredit,
                'sisa' => $nominal_kredit
            ]);
        }

        if ($nominal_titipan > 0) {
            $akun_pendapatan_dimuka->kredit -= $nominal_titipan;
            $akun_pendapatan_dimuka->update();
            $pelanggan->titipan -= $nominal_titipan;
            $pelanggan->update();
        }

        $akun_hpp->debet += $nominal_persediaan;
        $akun_hpp->update();

        $akun_penjualan->kredit += $nominal_penjualan;
        $akun_penjualan->update();

        $akun_penjualan_kredit->kredit += $nominal_penjualan_kredit;
        $akun_penjualan_kredit->update();

        $akun_ppn_keluaran->kredit += $nominal_ppn_keluaran;
        $akun_ppn_keluaran->update();

        $akun_persediaan->debet -= $nominal_persediaan;
        $akun_persediaan->update();

        $nominal_utang = 0;
        if ($jumlah_bayar - $nominal_kredit < $harga_total) {
            $nominal_utang = $harga_total - $jumlah_bayar;
            $akun_piutang_dagang->debet += $nominal_utang;
            $akun_piutang_dagang->update();
            // insert/update piutang_dagang
            if ($piutang_dagang == null) {
                // insert
                $piutang_dagang = PiutangDagang::create([
                    'kode_transaksi' => $transaksi_penjualan->kode_transaksi,
                    'transaksi_penjualan_id' => $transaksi_penjualan->id,
                    'nominal' => $nominal_utang,
                    'sisa' => $nominal_utang
                ]);
            } else {
                // update
                $piutang_dagang->nominal += $nominal_utang;
                $piutang_dagang->sisa += $nominal_utang;
                $piutang_dagang->update();
            }
        }

        // proses pembuatan jurnal
        // jurnal untuk kas tunai
        if ($nominal_tunai > 0) {
            Jurnal::create([
                'kode_akun' => $akun_kas_tunai->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_tunai
            ]);
        }

        // jurnal untuk kas bank
        if ($nominal_transfer > 0) {
            Jurnal::create([
                'kode_akun' => $akun_kas_bank->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_transfer
            ]);
        }
        
        // jurnal untuk kas cek
        if ($nominal_cek > 0) {
            Jurnal::create([
                'kode_akun' => $akun_kas_cek->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_cek
            ]);
        }
        
        // jurnal untuk kas bg
        if ($nominal_bg > 0) {
            Jurnal::create([
                'kode_akun' => $akun_kas_bg->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_bg
            ]);
        }
        
        // jurnal untuk kredit (piutang dagang)
        if ($nominal_kredit > 0) {
            Jurnal::create([
                'kode_akun' => $akun_piutang_dagang->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_kredit
            ]);
        }

        // jurnal untuk titipan (pendapatan dimuka)
        if ($nominal_titipan > 0) {
            Jurnal::create([
                'kode_akun' => $akun_pendapatan_dimuka->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_titipan
            ]);
        }

        // jurnal untuk utang (piutang dagang)
        if ($nominal_utang > 0) {
            Jurnal::create([
                'kode_akun' => $akun_piutang_dagang->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_utang
            ]);
        }

        // jurnal untuk hpp
        Jurnal::create([
            'kode_akun' => $akun_hpp->kode,
            'referensi' => $transaksi_penjualan->kode_transaksi,
            'keterangan' => $keterangan,
            'debet' => $nominal_persediaan
        ]);

        // jurnal untuk penjualan
        Jurnal::create([
            'kode_akun' => $akun_penjualan->kode,
            'referensi' => $transaksi_penjualan->kode_transaksi,
            'keterangan' => $keterangan,
            'kredit' => $nominal_penjualan
        ]);
        
        // jurnal untuk penjualan kredit
        if ($nominal_kredit > 0) {
            Jurnal::create([
                'kode_akun' => $akun_penjualan_kredit->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_penjualan_kredit
            ]);
        }

        // jurnal untuk ppn keluaran
        Jurnal::create([
            'kode_akun' => $akun_ppn_keluaran->kode,
            'referensi' => $transaksi_penjualan->kode_transaksi,
            'keterangan' => $keterangan,
            'kredit' => $nominal_ppn_keluaran
        ]);

        // jurnal untuk persediaan
        Jurnal::create([
            'kode_akun' => $akun_persediaan->kode,
            'referensi' => $transaksi_penjualan->kode_transaksi,
            'keterangan' => $keterangan,
            'kredit' => $nominal_persediaan
        ]);

        return redirect('transaksi-grosir/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $satuans = Satuan::all();
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $id)->first();
            $relasi_transaksi_grosir = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();

            // return view('transaksi_grosir.show', compact('transaksi_grosir', 'relasi_transaksi_grosir'));
            return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'piutang_dagang', 'relasi_transaksi_grosir'));
        }
    }

    public function formCairkanKredit($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $id)->first();
            if ($piutang_dagang->sisa > 0) {
                $banks = Bank::all();
                return view('transaksi_grosir.form_cairkan_kredit', compact('transaksi_grosir', 'banks'));
            } else {
                return redirect('transaksi-grosir');
            }
        }
    }

    public function SimpanCairkanKredit(Request $request, $id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            // return [$request->all(), $transaksi_grosir];
            $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $transaksi_grosir->id)->first();

            $nominal_kredit = $transaksi_grosir->nominal_kredit;
            $keterangan = $request->keterangan;
            $tujuan = $request->tujuan;

            if ($tujuan == 'tunai') {
                $akun_kas_tunai->debet += $nominal_kredit;
                $akun_kas_tunai->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_tunai->kode,
                    'referensi' => $transaksi_grosir->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_kredit
                ]);
            } else if ($tujuan == 'bank') {
                $akun_kas_bank->debet += $nominal_kredit;
                $akun_kas_bank->update();

                $bank = Bank::find($request->bank_id);
                $bank->nominal += $nominal_kredit;
                $bank->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_bank->kode,
                    'referensi' => $transaksi_grosir->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_kredit
                ]);
            }

            $piutang_dagang->sisa -= $nominal_kredit;
            $piutang_dagang->update();

            $akun_piutang_dagang->debet -= $nominal_kredit;
            $akun_piutang_dagang->update();

            Jurnal::create([
                'kode_akun' => $akun_piutang_dagang->kode,
                'referensi' => $transaksi_grosir->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_kredit
            ]);

             return redirect('transaksi-grosir/'.$id)->with('sukses', 'kredit');
        }
    }

    public function returIndex($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $retur_penjualans = ReturPenjualan::where('transaksi_penjualan_id', $transaksi_grosir->id)->get();
            foreach ($retur_penjualans as $i => $retur_penjualan) {
                $relasi_retur_penjualan = RelasiReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->get();
                $retur_penjualan->relasi_retur_penjualan = $relasi_retur_penjualan;
            }
            return [
                $transaksi_grosir,
                $retur_penjualans
            ];

            return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'relasi_transaksi_grosir'));
        }
    }

    public function returCreate($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            // buat kode_retur_baru
            $tanggal = date('d/m/Y');
            $kode_retur_baru = '';
            $retur_penjualan_terakhir = ReturPenjualan::all()->last();
            if ($retur_penjualan_terakhir == null) {
                // buat kode_retur_baru
                $kode_retur_baru = '0001/RTJ/'.$tanggal;
            } else {
                $kode_retur_penjualan_terakhir = $retur_penjualan_terakhir->kode_retur;
                $tanggal_retur_penjualan_terakhir = substr($kode_retur_penjualan_terakhir, 9, 10);
                // buat kode_retur dari kode_retur terakhir + 1
                if ($tanggal_retur_penjualan_terakhir == $tanggal) {
                    $kode_terakhir = substr($kode_retur_penjualan_terakhir, 0, 4);
                    $kode_baru = intval($kode_terakhir) + 1;
                    $kode_retur_baru = Util::int4digit($kode_baru).'/RTJ/'.$tanggal;
                } else {
                    // buat kode_retur_baru
                    $kode_retur_baru = '0001/RTJ/'.$tanggal;
                }
            }

            // $satuans = Satuan::all();
            // $relasi_transaksi_grosir = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();

            // // return view('transaksi_grosir.show', compact('transaksi_grosir', 'relasi_transaksi_grosir'));
            // return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'relasi_transaksi_grosir'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
