<?php
namespace App\Http\Controllers;

use App\Http\Requests;

use DB;
use Auth;

use App\HutangBank;
use App\PiutangLain;
use App\PiutangDagang;
use App\DataPerusahaan;
use App\TransaksiPembelian;

use Illuminate\Http\Request;

class BerandaController extends Controller
{
    public function get()
    {
    	$data = DataPerusahaan::find(1);
    	$angka = array();
    	$angka['hutang_dagang'] = TransaksiPembelian::select(DB::raw('SUM(sisa_utang) as jumlah'))->first()->jumlah;
    	$angka['hutang_bank'] = HutangBank::select(DB::raw('SUM(sisa_hutang) as jumlah'))->first()->jumlah;

    	$angka['piutang_dagang'] = PiutangDagang::select(DB::raw('SUM(sisa) as jumlah'))->first()->jumlah;
    	$angka['piutang_lain'] = PiutangLain::select(DB::raw('SUM(sisa) as jumlah'))->first()->jumlah;

    	if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
    		$angka['status'] = 1;
    	}else{
    		$angka['status'] = 0;
    	}

        // return $angka;
    	// dd($hutang_dagang, $hutang_bank, $piutang_dagang, $piutang_lain);
        // return view('beranda', compact('data', 'hutang_dagang', 'hutang_bank', 'piutang_dagang', 'piutang_lain', 'status'));
        return view('beranda', compact('data', 'angka'));
    }
}
