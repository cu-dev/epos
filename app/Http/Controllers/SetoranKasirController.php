<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;

use App\Laci;
use App\User;
use App\Level;
use App\LogLaci;
use App\CashDrawer;
use App\MoneyLimit;
use App\SetoranKasir;

use Illuminate\Http\Request;

class SetoranKasirController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $setorans = SetoranKasir::latest()->get();
        $today = new DateTime();
        $today = $today->format('Y-m-d');

        $users = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 3)->get();
        $user_array = array();
        foreach ($users as $i => $user) {
            array_push($user_array, $user->id);
        }

        // $setorans = SetoranKasir::where('created_at', '>=', $today)->where('user_id', Auth::user()->id)->latest()->get();
        // $setoran_kasir_a = SetoranKasir::where('user_id', Auth::user()->id)->latest()->get();
        // $setoran_kasir = SetoranKasir::where('user_id', Auth::user()->id)->latest()->get();
        // $setors = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $user_array)->latest()->get();
        
        $laci = Laci::find(1);
        $cash_drawer = CashDrawer::where('user_id', Auth::user()->id)->first();
        $log_laci_in = LogLaci::where('penerima', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $log_laci_out = LogLaci::where('pengirim', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $all_receiver = User::whereIn('level_id', [1,2,4])->where('status', 1)->get();

        // return $cash_drawer;
        return view('setoran_kasir.index', compact('setorans', 'cash_drawer', 'setoran_kasir', 'setoran_kasir_a', 'log_laci_in', 'log_laci_out', 'all_receiver'));
        // if(Auth::user()->level_id != 3 && Auth::user()->level_id != 5){
        //     return view('setoran_kasir.approve', compact('setors', 'laci'));
        // }elseif(Auth::user()->level_id == 3){
        //     return view('setoran_kasir.index', compact('setorans', 'cash_drawer', 'setoran_kasir', 'setoran_kasir_a'));
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $limit = MoneyLimit::latest()->first()->nominal;
        $cash_drawer = CashDrawer::where('user_id', Auth::user()->id)->first();

        $setoran = new SetoranKasir;
        $setoran->nominal = $request->nominal;
        $setoran->user_id = Auth::user()->id;
        $setoran->status = 0;       
        $setoran->save();

        // $cash_drawer->nominal -= $request->nominal;
        // $cash_drawer->update();

        // $laci = LaciGrosir::find(1);
        // $laci->nominal += $request->nominal;
        // $laci->update();

        return redirect('/setoran-kasir')->with('sukses', 'tambah');
    }

    public function storeGrosir(Request $request)
    {
        $setoran = new SetoranKasir;
        $setoran->nominal = $request->nominal;
        $setoran->user_id = Auth::user()->id;
        $setoran->status = 0;       
        $setoran->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function setuju($id){
        // return $id;
        $setoran = SetoranKasir::find($id);
        $setoran->status = 1;
        $setoran->operator = Auth::user()->id;

        $cash_drawer = CashDrawer::where('user_id', $setoran->user_id)->first();

        $cash_drawer->nominal -= $setoran->nominal;
        $cash_drawer->update();

        $laci = Laci::find(1);
        $laci->grosir += $setoran->nominal;
        $laci->update();
        $setoran->update();

        $penerima = User::where('level_id', 4)->first()->id;

        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $penerima;
        $log->nominal = $setoran->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setuju_kasir');
    }

    /*public function approveGrosir($id){
        // return $id;
        $setoran = SetoranKasir::find($id);
        $setoran->status = 1;
        $setoran->operator = Auth::user()->id;

        $laci = Laci::find(1);
        $laci->grosir -= $setoran->nominal;
        $laci->gudang += $setoran->nominal;
        $laci->update();
        $setoran->update();

        $penerima = User::where('level_id', 5)->first()->id;

        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $penerima;
        $log->nominal = $setoran->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function approveGudang($id){
        // return $id;
        $setoran = SetoranKasir::find($id);
        $setoran->status = 1;
        $setoran->operator = Auth::user()->id;

        $laci = Laci::find(1);
        $laci->owner += $setoran->nominal;
        $laci->gudang -= $setoran->nominal;
        $laci->update();
        $setoran->update();

        $log = new LogLaci();
        $log->pengirim = $setoran->user_id;
        $log->penerima = Auth::user()->id;
        $log->nominal = $setoran->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setuju');
    }*/

    public function json()
    {
        $user = Auth::user();
        $cash_drawer = CashDrawer::where('user_id', $user->id)->first();
        return response()->json(compact('user', 'cash_drawer'));
    }
}
