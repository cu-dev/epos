<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;

use App\User;
use App\Laci;
use App\LogLaci;
use App\CashDrawer;
use App\SetoranBuka;

use Illuminate\Http\Request;

class SetoranBukaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('level_id', '3')->get();
        $today = new DateTime();
        $today = $today->format('Y-m-d');
        $setorans = SetoranBuka::where('created_at', '>=', $today)->orderBy('created_at', 'desc')->get();
        $laci_grosir = Laci::find(1)->grosir;
        // $cash_drawer = CashDrawer::where('user_id', Auth::user()->id)->first();
        // return $setorans->user1->nama;
        return view('setoran_buka.index', compact('setorans', 'users', 'laci_grosir'));
    }

    public function store(Request $request)
    {
        // $cash_drawer = CashDrawer::where('user_id', $request->user)->first();        
        // $cash_drawer->nominal += $request->nominal;
        // $cash_drawer->update();

        $setoran = new SetoranBuka;
        $setoran->nominal = $request->nominal;
        $setoran->user_id = $request->user;
        $setoran->operator = Auth::user()->id;;
        $setoran->save();

        // $laci = Laci::find(1);
        // $laci->grosir -= $request->nominal;
        // $laci->update();

        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $request->user;
        $log->nominal = $request->nominal;
        $log->approve = 0;
        $log->save();

        return redirect('/laci')->with('sukses', 'setor_buka');
    }
}
