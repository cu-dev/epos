<?php

namespace App\Http\Controllers;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Jurnal;
use App\HutangBank;
use App\BayarHutangBank;

use Illuminate\Http\Request;

class HutangBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lastJson()
    {
        $hutang_bank = HutangBank::all()->last();
        return response()->json(['hutang_bank' => $hutang_bank]);
    }

    /* public function index()
    {
        $hutang_banks = HutangBank::where('sisa_hutang', '>', 0)->get();
        $hutang_lunass = HutangBank::where('sisa_hutang', '=', 0)->get();
        $banks = Bank::all();
       
        return view('hutang_bank.index', compact('hutang_banks', 'banks', 'hutang_lunass'));
    } */

    public function index()
    {
        $banks = Bank::all();
        return view('hutang_bank.index', compact('banks'));
    }

    public function mdt1(Request $request)
    {
        $hutangs = HutangBank
            ::with('bank')
            ->where('sisa_hutang', '>', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = HutangBank
            ::with('bank')
            ->where('sisa_hutang', '>', 0)
            ->count();

        foreach ($hutangs as $i => $hutang) {
            $hutang->nominal = Util::duit($hutang->nominal);
            $hutang->sisa_hutang = Util::duit($hutang->sisa_hutang);

            if ($hutang->keterangan == null) {
                $hutang->keterangan = ' - ';
            }

            $buttons['bayar'] = ['url' => url('hutang_bank/show/'.$hutang->id)];

            // return $buttons;
            $hutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $hutangs,
        ]);
    }

    public function mdt2(Request $request)
    {
        $hutangs = HutangBank
            ::with('bank')
            ->where('sisa_hutang', '=', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = HutangBank
            ::with('bank')
            ->where('sisa_hutang', '=', 0)
            ->count();

        foreach ($hutangs as $i => $hutang) {
            $hutang->nominal = Util::duit($hutang->nominal);

            if ($hutang->keterangan == null) {
                $hutang->keterangan = ' - ';
            }

            $buttons['bayar'] = ['url' => url('hutang_bank/show/'.$hutang->id)];

            // return $buttons;
            $hutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $hutangs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        // $this->validate($request, [
        // 	'nama' => 'required|max:255',
        // 	'nominal' => 'required|max:255',
        // 	'kas' => 'required|max:255',
        // ]);
        $this->validate($request, [
            'kode_transaksi' => 'required|max:255',
            'bank_id' => 'required|max:255',
            'nominal' => 'required|max:255',
            'kas' => 'required|max:255',
            'no_transaksi' => 'required|max:255',
        ]);

        $hutang_bank = new HutangBank();
        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_hutang_bank = Akun::where('kode', Akun::HutangBank)->first();
        
        // $akun_harta_lancar = Akun::where('kode', Akun::HartaLancar)->first();
        // $akun_harta_lancar += $request->nominal;
        // $akun_harta_lancar->update();
        
        // $akun_aset = Akun::where('kode', Akun::Aset)->first();
        // $akun_aset += $request->nominal;
        // $akun_aset->update();

        // $akun_kewajiban = Akun::where('kode', Akun::Kewajiban)->first();
        // $akun_kewajiban -= $request->nominal;
        // $akun_kewajiban->update();        

        // $akun_hutang_jangka_panjang = Akun::where('kode', Akun::HutangJangkaPanjang)->first();
        // $akun_hutang_jangka_panjang -= $request->nominal;
        // $akun_hutang_jangka_panjang->update();

        $hutang_bank->kode_transaksi = $request->kode_transaksi;
        $hutang_bank->bank_id = $request->bank_id;
        $hutang_bank->nominal = $request->nominal;
        $hutang_bank->sisa_hutang = $request->nominal;
        $hutang_bank->no_transaksi = $request->no_transaksi;
        $hutang_bank->keterangan = $request->keterangan;
        // update akun hutang bank
        $akun_hutang_bank->kredit = $akun_hutang_bank->kredit + $request->nominal;
        $akun_hutang_bank->update();
        // create jurnal akun hutang bank
        $jurnal_hutang_bank = new Jurnal();
        $jurnal_hutang_bank->kode_akun = Akun::HutangBank;
        $jurnal_hutang_bank->referensi = $request->kode_transaksi;
        $jurnal_hutang_bank->keterangan = $request->keterangan;
        $jurnal_hutang_bank->kredit = $request->nominal;

        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai->debet = $akun_tunai->debet + $request->nominal;
            $akun_tunai->update();
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->keterangan;
            $tunai->debet = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
        }else{
            // update nominal di akun bank
            $akun_bank->debet = $akun_bank->debet + $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal + $request->nominal;
            $bank->update();
            // create baris akun tunai
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode_transaksi;
            $jurnal_bank->keterangan = $request->keterangan;
            $jurnal_bank->debet = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
        }

        Arus::create([
            'nama' => Arus::Modal,
            'nominal' => $request->nominal,
        ]);

        if ($hutang_bank->save() && $jurnal_hutang_bank->save()) {
            return redirect('/hutang_bank')->with('sukses', 'tambah');
        } else {
            return redirect('/hutang_bank')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hutang_bank = HutangBank::where('id', $id)->first();
        $bayar = BayarHutangBank::where('hutang_bank_id', $id)->get();
        $banks = Bank::all();

        return view('hutang_bank.show', compact('bayar', 'hutang_bank', 'banks'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
