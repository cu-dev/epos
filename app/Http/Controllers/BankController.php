<?php

namespace App\Http\Controllers;

use Auth;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Modal;
use App\Jurnal;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $banks = Bank::all();
        return view('bank.index', compact('banks'));
    } */

    public function index()
    {
        return view('bank.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'banks.' . $field;
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $banks = Bank
            ::select(
                'banks.id',
                'banks.nama_bank',
                'banks.no_rekening',
                'banks.atas_nama',
                'banks.nominal',
                'users.nama as operator'
            )
            ->leftJoin('users', 'users.id', 'banks.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('banks.nama_bank', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.no_rekening', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Bank
            ::select(
                'banks.id'
            )
            ->leftJoin('users', 'users.id', 'banks.user_id')
            ->where(function($query) use ($request) {
                $query
                ->where('banks.nama_bank', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.no_rekening', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('banks.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($banks as $i => $bank) {

            $bank->nominal = Util::duit($bank->nominal);

            $buttons['ubah'] = ['url' => ''];

            // return $buttons;
            $bank->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $banks,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_bank' => 'required|max:255',
            'no_rekening' => 'required|max:255',
            'atas_nama' => 'required|max:255'
        ]);

        $bank = new Bank();
        $bank->nama_bank = $request->nama_bank;
        $bank->no_rekening = $request->no_rekening;
        $bank->atas_nama = $request->atas_nama;
        $bank->nominal = $request->nominal;
        $bank->user_id = Auth::id();

        if($request->nominal > 0){
            $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_kas_bank->debet += $request->nominal;
            $akun_kas_bank->update();

            $akun_modal = Akun::where('kode', Akun::Modal)->first();
            $akun_modal->kredit += $request->nominal;
            $akun_modal->update();

            $modal = new Modal();
            $modal->nominal = $request->nominal;
            $modal->keterangan = 'Penambahan Modal dari Kas Bank';
            $modal->save();

            $jumlah_modal = Modal::where('nominal', '>', 0)->count();
            $referensi = Util::int4digit($jumlah_modal + 1) . '/MDLT';
            $created_at = substr($modal->created_at, 0, 10);
            $created_at = explode('-', $created_at);
            $referensi .= '/' . $created_at[2] . '/' . $created_at[1] . '/' . $created_at[0];

            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $referensi,
                'keterangan' => 'Penambahan Modal',
                'debet' => $request->nominal
            ]);

            Jurnal::create([
                'kode_akun' => Akun::Modal,
                'referensi' => $referensi,
                'keterangan' => 'Penambahan Modal',
                'kredit' => $request->nominal
            ]);

            Arus::create([
                'nama' => Arus::Modal,
                'nominal' => $request->nominal
            ]);
        }

        if ($bank->save()) {
            return redirect('/bank')->with('sukses', 'tambah');
        } else {
            return redirect('/bank')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $this->validate($request,[
            'nama_bank' => 'required|max:255',
            'no_rekening' => 'required|max:255',
            'atas_nama' => 'required|max:255'
        ]);

        $bank = Bank::find($id);
        $bank->nama_bank = $request->nama_bank;
        $bank->no_rekening = $request->no_rekening;
        $bank->atas_nama = $request->atas_nama;
        // $bank->nominal = $request->nominal;
        $bank->user_id = Auth::id();
        if ($bank->update()) {
            return redirect('/bank')->with('sukses', 'ubah');
        } else {
            return redirect('/bank')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
