<?php

namespace App\Http\Controllers;

use Auth;
use DB;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Laci;
use App\Stok;
use App\Item;
use App\Hutang;
use App\Jurnal;
use App\Kredit;
use App\Satuan;
use App\Suplier;
use App\LogLaci;
use App\RelasiSatuan;
use App\ReturPembelian;
use App\TransaksiPembelian;
use App\RelasiReturPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class ReturPembelianController extends Controller
{
    public function indexFromTransaksi($id)
    {
        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'items', 'returs')->find($id);
        return view('retur_pembelian.index_from_transaksi', compact('transaksi_pembelian'));
    }

    /*public function itemJson($kode, $transaksi)
    {
        $item = Item::where('kode', $kode)->first();
        $stok = $item->stoktotal;
        // $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode')->having('kode', '=', $item->kode)->get()->first();
        // $harga = RelasiTransaksiPembelian::where('item_id', $kode)->where('transaksi_pembelian_id', $transaksi)->first();
        // $stok_relasi = Stok::where('harga', $harga->harga)->where('created_at', $harga->created_at)->first();
        // return response()->json(['item' => $item, 'stok' => $stok, 'harga' => $harga]);
        return response()->json(['item' => $item, 'stok' => $stok]);
    }*/

    public function itemJson($item_id, $transaksi_pembelian_id)
    {
        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $transaksi_pembelian_id)->where('item_id', $item_id)->first();
        $item = Item::find($item_id);
        $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
        $today = date('Y-m-d');
        $stoktotal = 0;
        foreach ($stoks as $j => $stok) {
            if ($stok->kadaluarsa == null || $stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                $stoktotal += $stok->jumlah;
            }
        }
        // return $stoktotal;
        $item->stoktotal = $stoktotal;

        $stoks = Stok::where('item_id', $item_id)->where('transaksi_pembelian_id', $transaksi_pembelian_id)->where('jumlah', '>', 0)->get();
        // $pcs = Satuan::where('kode', 'PCS')->first();

        return response()->json(compact('item', 'relasi_transaksi_pembelian', 'stoks', 'pcs'));
    }

    public function returItemJson($kode)
    {
        $item = Item::find($kode);
        return response()->json(['item' => $item]);
    }

    public function hutangJson($kode)
    {
        $transaksi = TransaksiPembelian::find($kode);
        return response()->json(['transaksi' => $transaksi]);
    }

    public function KonversiJson($kode, $satuan)
    {
        $item = Item::find($kode);
        $satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $satuan)->first();                              
        return response()->json(['satuan' => $satuan]);
    }

    public function countJson($kode, $jumlah, $transaksi)
    {
        // $subtotal = 0;
        // $hitung = 0;
        // $status = 0;
        $item = Item::find($kode);
        $harga = RelasiTransaksiPembelian::where('item_id', $kode)->where('transaksi_pembelian_id', $transaksi)->first();
        $stok_relasi = Stok::where('item_kode', $item->kode)
                        ->where('harga', $harga->harga)
                        ->whereDate('created_at', '=', ($harga->created_at)->format('Y-m-d'))
                        ->first();
        $subtotal = $harga->harga * $jumlah;
        $harga_satuan = $harga->harga;
        
        return response()->json(['jumlah' => $jumlah, 'stok_relasi' => $stok_relasi, 'subtotal' => $subtotal, 'harga_satuan' => $harga_satuan]);
    }

    public function regaItemJson($kode, $jumlah, $satuan, $nilai)
    {
        // dd($kode, $jumlah, $satuan, $nilai);
        $item = Item::find($kode);
        $relasi_satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $satuan)->first()->konversi;
        $rega['hasil'] = $nilai / $relasi_satuan;
        $rega['view'] = number_format($rega['hasil'], 3, ',', '.');
        $rega['hitung'] = number_format($rega['hasil'], 3, '.', '');

        return response()->json(['rega' => $rega]);
    }

    /* public function index()
    {
        // return 'oyoi';
        $retur_pembelians = ReturPembelian::with('transaksi_pembelian')->orderBy('created_at', 'desc')->get();
        return view('retur_pembelian.index_from_retur', compact('retur_pembelians'));
    } */

    public function index()
    {
        return view('retur_pembelian.index_from_retur');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'retur_pembelians.' . $field;
        if ($request->field == 'kode_transaksi') {
            $field = 'transaksi_pembelians.kode_transaksi';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $retur_pembelians = ReturPembelian
            ::select(
                'retur_pembelians.id',
                'retur_pembelians.created_at',
                'retur_pembelians.kode_retur',
                'transaksi_pembelians.kode_transaksi'
            )
            ->leftJoin('transaksi_pembelians', 'transaksi_pembelians.id', 'retur_pembelians.transaksi_pembelian_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('retur_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('retur_pembelians.kode_retur', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = ReturPembelian
            ::select(
                'retur_pembelians.id'
            )
            ->leftJoin('transaksi_pembelians', 'transaksi_pembelians.id', 'retur_pembelians.transaksi_pembelian_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('retur_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('retur_pembelians.kode_retur', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($retur_pembelians as $i => $retur_pembelian) {

            $buttons['detail'] = ['url' => url('retur-pembelian/'.$retur_pembelian->id)];

            if ($retur_pembelian->cek_lunas == 0 && $retur_pembelian->bank_cek != null) {
                $buttons['cek'] = ['url' => ''];
            } else {
                $buttons['cek'] = null;
            }

            if ($retur_pembelian->bg_lunas == 0 && $retur_pembelian->bank_bg != null) {
                $buttons['bg'] = ['url' => ''];
            } else {
                $buttons['bg'] = null;
            }

            // return $buttons;
            $retur_pembelian->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $retur_pembelians,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);

        return view('retur_pembelian.index_from_retur', compact('retur_pembelians'));
    }

    public function lastJson()
    {
        $retur_pembelian = ReturPembelian::all()->last();
        // return $retur_pembelian;
        return response()->json([
            // 'retur_pembelian' => '$retur_pembelian'
            'retur_pembelian' => $retur_pembelian
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // return $this->kodeTransaksiBaru();
        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'seller', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();

        // $stoks = Stok::where('transaksi_pembelian_id', $transaksi_pembelian->id)->where('jumlah', '>', 0)
        //     ->orderBy('item_id')
        //     ->get();
        $stoks = Stok::where('transaksi_pembelian_id', $transaksi_pembelian->id)
            ->where('retur_pembelian_id', null)
            ->orderBy('id')
            ->get();

        // return $transaksi_pembelian;
        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)
            ->orderBy('id')
            ->get();
            // return $stoks;
        $item_returs = Item::where('suplier_id', $transaksi_pembelian->suplier_id)->groupBy('kode')->get();
        $hutangs = TransaksiPembelian::where('suplier_id',  $transaksi_pembelian->suplier_id)->where('sisa_utang', '>', 0)->get();

        $pilih_items = array();
        $pilih_items_id = array();
        foreach ($stoks as $i => $_stok) {
            if (!in_array($_stok->item->id, $pilih_items_id) && $_stok->jumlah > 0) {
                array_push($pilih_items, $_stok);
                array_push($pilih_items_id, $_stok->item->id);
            }
        }

        $items = array();
        $hasil = array();
        $sisa_hasil = array();
        // return $relasi_transaksi_pembelian;
        foreach ($relasi_transaksi_pembelian as $i => $data) {
            $items[$i] = Item::find($data->item_id);
            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$i]->kode)->orderBy('konversi', 'dsc')->get();
            $stok_relasi = Stok::where('item_kode', $items[$i]->kode)->where('harga', $data->harga)->whereDate('created_at', '=', ($data->created_at)->format('Y-m-d'))->first();
            // return $stok_relasi;
            $sisa_jumlah[$i] = $data->jumlah;
            $sisa_stok[$i] = $stok_relasi->jumlah;
            $a=-1;
            $b=-1;
            for ($x = 0; $x < count($satuans[$i]); $x++) {
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                $sisa = explode('.', $sisa_stok[$i] / $satuans[$i][$x]->konversi)[0];
                if ($hitung > 0) {
                    $a = $a + 1;
                    $hasil[$i][$a]['item_kode'] = $items[$i]->id;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }

                if ($sisa > 0) {
                    $b = $b + 1;
                    $sisa_hasil[$i][$b]['item_kode'] = $items[$i]->id;
                    $sisa_hasil[$i][$b]['jumlah'] = $sisa;
                    $sisa_hasil[$i][$b]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }

                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
                $sisa_stok[$i] = $sisa_stok[$i] % $satuans[$i][$x]->konversi;
            }
        }
        // return $items;

        $item_stoks = array();
        $hasil_stoks = array();
        foreach ($stoks as $i => $data) {
            $item_stoks[$i] = Item::find($data->item_id)->kode;
            $satuans[$i] = RelasiSatuan::where('item_kode', $item_stoks[$i])->orderBy('konversi', 'desc')->get();
            $sisa_jumlah[$i] = $data->jumlah_beli;
            $a=-1;

            for ($x = 0; $x < count($satuans[$i]); $x++) {
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
            // return $i;
                if ($hitung > 0) {
                    $a = $a + 1;

                    $hasil_stoks[$i][$a]['jumlah'] = $hitung;
                    $hasil_stoks[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }
        // return $hasil_stoks;
        
        $laci = Laci::find(1);
        $kredits = Kredit::all();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();

        return view('retur_pembelian.create', compact('transaksi_pembelian', 'relasi_transaksi_pembelian', 'hasil', 'kredits', 'banks', 'ceks', 'bgs', 'item_returs', 'hutangs', 'stok_relasi', 'sisa_hasil', 'stoks', 'hasil_stoks', 'laci', 'pilih_items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $stock_masuk_n = array();
        $index_stok = 0;
        $kode_retur = $this->kodeTransaksiBaru();
        $transaksi_pembelian = TransaksiPembelian::find($request->transaksi_pembelian_id);

        $kode = explode("/",$kode_retur);
        $kode[1] = $kode[1].'I';
        $kode_retur_in = $kode[0].'/'.$kode[1].'/'.$kode[2].'/'.$kode[3].'/'.$kode[4];

        $retur_pembelian = new ReturPembelian();
        $retur_pembelian->kode_retur = $kode_retur;
        $retur_pembelian->nota = $request->nota;
        $retur_pembelian->transaksi_pembelian_id = $request->transaksi_pembelian_id;
        $retur_pembelian->suplier_id = $transaksi_pembelian->suplier_id;
        $retur_pembelian->status = $request->status;
        $retur_pembelian->harga_total = $request->harga_total;
        $retur_pembelian->user_id = Auth::id();

        $data = array();
        $hpp_keluar = 0;
        //stock_out
        foreach ($request->item_kode as $i => $id) {
            $jumlah_retur = 0;
            foreach($request->relasi_stok_penjualan[$id] as $a => $stok_id){
                $stok_out = Stok::find($stok_id);
                $jumlah_stok = $request->jumlah_rusak[$id][$a] * $request->konversi_rusak[$id][$a];
                $jumlah_retur += $jumlah_stok;
                $hpp_keluar += $jumlah_stok * $stok_out->harga;
                $stok_out->jumlah -= $jumlah_stok;
                $stok_out->update();
    
                if ($request->status != 'sama') {
                    $item = Item::find($id);
                    $items = Item::where('kode_barang', $item->kode_barang)->get();
                    foreach ($items as $j => $item) {
                        $item->stoktotal -= $jumlah_stok;
                        $item->update();
                    }
                }
            }

            $data[''.$id] = [
                'retur' => 0,
                'jumlah' => $jumlah_retur,
                'harga' => $request->harga_rusak[$id][$a],
                'subtotal' => $request->subtotal_rusak[$id][$a],
                'keterangan' => $request->keterangan_rusak[$id][$a],
            ];
        }

        // return $data;
        $harga_total = $request->harga_total;
        $ppn_out = $harga_total - $hpp_keluar;

        // $jumlah_total_masuk_lagi = $request->nominal_tunai_in + $request->nominal_transfer_in + $request->nominal_kartu_in + $request->nominal_cek_in + $request->nominal_bg_in + $request->harga_total_in;
        // $hpp_masuk = 0;
        // foreach ($request->item_kode_in as $i => $id) {
        //     $item = Item::find($id);
        //     $ppn = ceil($request->harga_in[$i] / 11 * 100)/100;
        //     $harga = $request->harga_in[$i] - $ppn;
        //     foreach($request->item_stok[$id] as $a => $item_id){
        //         $hpp_masuk += $request->jumlah_stok[$id][$a] * $harga;
        //     }
        // }

        // $harga_masuk = $jumlah_total_masuk_lagi;
        // $ppn_masuk = $request->harga_total_in - $hpp_masuk;
        // $selisih_masuk_keluar = $request->harga_total_in - $harga_total - $request->jumlah_bayar_in;
        // return [$harga_total, $hpp_keluar, $ppn_out, $harga_masuk, $hpp_masuk, $ppn_masuk, $request->harga_total_in, $request->jumlah_bayar_in, $selisih_masuk_keluar];
        // return $hpp_keluar;

        Jurnal::create([
            'kode_akun' => Akun::ReturPembelian,
            'referensi' => $kode_retur,
            'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
            'debet' => $harga_total
        ]);
        if($request->status == 'lain'){
            Jurnal::create([
                'kode_akun' => Akun::PPNMasukan,
                'referensi' => $kode_retur,
                'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi.' (Barang Lain)',
                'kredit' => $ppn_out
            ]);
        }else{
            Jurnal::create([
                'kode_akun' => Akun::PPNMasukan,
                'referensi' => $kode_retur,
                'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                'kredit' => $ppn_out
            ]);
        }
        Jurnal::create([
            'kode_akun' => Akun::Persediaan,
            'referensi' => $kode_retur,
            'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
            'kredit' => $hpp_keluar
        ]);

        if($request->status == 'uang'){
            $retur_pembelian->total_uang = $request->jumlah_bayar;
            //persediaan out
            $persediaan = Akun::where('kode', Akun::Persediaan)->first();
            $persediaan->debet -= $hpp_keluar;
            $persediaan->update();

            $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
            $akun_ppn_masuk->debet -= $ppn_out;
            $akun_ppn_masuk->update();

            if ($request->nominal_tunai > 0) {
                $retur_pembelian->nominal_tunai = $request->nominal_tunai;
                Jurnal::create([
                    'kode_akun' => Akun::KasTunai,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $request->nominal_tunai
                ]);

                $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
                $kas_tunai->debet += $request->nominal_tunai;
                $kas_tunai->update();

                Arus::create([
                    'nama' => Arus::ReturPembelian,
                    'nominal' => $request->nominal_tunai,
                ]);

                if(Auth::user()->level_id == 5 && $request->nominal_tunai > 0){
                    $log = new LogLaci();
                    $log->penerima = Auth::user()->id;
                    // $log->penerima = $request->penerima;
                    $log->approve = 1;
                    $log->status = 11;
                    $log->nominal = $request->nominal_tunai;
                    $log->save();

                    $laci = Laci::find(1);
                    $laci->gudang += $request->nominal_tunai;
                    $laci->update();
                }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                {
                    if($request->nominal_tunai > 0){
                        $log = new LogLaci();
                        $log->penerima = Auth::user()->id;
                        // $log->penerima = $request->penerima;
                        $log->approve = 1;
                        $log->status = 11;
                        $log->nominal = $request->nominal_tunai;
                        $log->save();

                        $laci = Laci::find(1);
                        $laci->owner += $request->nominal_tunai;
                        $laci->update();
                    }
                }
            }

            if ($request->nominal_transfer > 0) {
                $retur_pembelian->no_transfer = $request->no_transfer;
                $retur_pembelian->nominal_transfer = $request->nominal_transfer;
                $retur_pembelian->bank_transfer = $request->bank_transfer;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal += $request->nominal_transfer;
                $bank->update();

                Arus::create([
                    'nama' => Arus::ReturPembelian,
                    'nominal' => $request->nominal_transfer,
                ]);

                $keterangan = 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi.' Transfer Bank '.$bank->nama_bank;

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $kode_retur_in,
                    'keterangan' => $keterangan,
                    'debet' => $request->nominal_transfer
                ]);

                $kas = Akun::where('kode', Akun::KasBank)->first();
                $kas->debet += $request->nominal_transfer;
                $kas->update();
            }

            if ($request->nominal_kartu > 0) {
                $retur_pembelian->no_kartu = $request->no_kartu;
                $retur_pembelian->nominal_kartu = $request->nominal_kartu;
                $retur_pembelian->bank_kartu = $request->bank_kartu;
                $retur_pembelian->jenis_kartu = $request->jenis_kartu;

                $bank = Bank::find($request->bank_kartu);
                $bank->nominal += $request->nominal_kartu;
                $bank->update();

                Arus::create([
                    'nama' => Arus::ReturPembelian,
                    'nominal' => $request->nominal_kartu,
                ]);

                $keterangan = 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi.' Transfer Bank '.$bank->nama_bank;

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $kode_retur_in,
                    'keterangan' => $keterangan,
                    'debet' => $request->nominal_kartu
                ]);

                $kas = Akun::where('kode', Akun::KasBank)->first();
                $kas->debet += $request->nominal_kartu;
                $kas->update();
            }

            if ($request->nominal_cek > 0) {
                $retur_pembelian->no_cek = $request->no_cek;
                $retur_pembelian->nominal_cek = $request->nominal_cek;

                $cek = new Cek();
                $cek->nomor = $request->no_cek;
                $cek->nominal = $request->nominal_cek;
                $cek->aktif = 1;
                $cek->save();

                Jurnal::create([
                    'kode_akun' => Akun::KasCek,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $request->nominal_cek
                ]);

                Arus::create([
                    'nama' => Arus::ReturPembelian,
                    'nominal' => $request->nominal_cek,
                ]);

                $kas = Akun::where('kode', Akun::KasCek)->first();
                $kas->debet += $request->nominal_cek;
                $kas->update();
            }

            if ($request->nominal_bg > 0) {
                $retur_pembelian->no_bg = $request->no_bg;
                $retur_pembelian->nominal_bg = $request->nominal_bg;

                $bg = new BG();
                $bg->nomor = $request->no_bg;
                $bg->nominal = $request->nominal_bg;
                $bg->aktif = 1;
                $bg->save();

                Jurnal::create([
                    'kode_akun' => Akun::KasBG,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $request->nominal_bg
                ]);

                Arus::create([
                    'nama' => Arus::ReturPembelian,
                    'nominal' => $request->nominal_bg,
                ]);

                $kas = Akun::where('kode', Akun::KasBG)->first();
                $kas->debet += $request->nominal_bg;
                $kas->update();
            }

            if ($request->nominal_piutang > 0) {
                $retur_pembelian->nominal_piutang = $request->nominal_piutang;

                $suplier = Suplier::find($transaksi_pembelian->suplier_id);
                $suplier->piutang += $request->nominal_piutang;
                $suplier->update();

                $kas = Akun::where('kode', Akun::PiutangDagang)->first();
                $kas->debet += $request->nominal_piutang;
                $kas->update();

                Jurnal::create([
                    'kode_akun' => Akun::PiutangDagang,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $request->nominal_piutang
                ]);
            }

            if ($request->nominal_hutang > 0) {
                $retur_pembelian->nominal_hutang = $request->nominal_hutang;
                // $retur_pembelian->hutang_id = $request->hutangID;

                Jurnal::create([
                    'kode_akun' => Akun::HutangDagang,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $request->nominal_hutang
                ]);

                $log_hutang = new Hutang();
                $log_hutang->transaksi_pembelian_id = $request->transaksi_pembelian_id;
                $log_hutang->jumlah_bayar = $request->nominal_hutang;
                $log_hutang->user_id = Auth::user()->id;
                $log_hutang->save();

                $transaksi = TransaksiPembelian::find($request->transaksi_pembelian_id);
                $transaksi->sisa_utang -= $request->nominal_hutang;

                if ($transaksi->sisa_utang == 0) {
                    $transaksi->status_hutang = 0;
                }
                $transaksi->update();

                $kas = Akun::where('kode', Akun::HutangDagang)->first();
                $kas->kredit -= $request->nominal_hutang;
                $kas->update();
            }

            $selisih = $request->jumlah_bayar - $request->harga_total;
            if($selisih < 0){
                Jurnal::create([
                    'kode_akun' => Akun::BebanRugiBeli,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Kerugian Retur '.$kode_retur_in,
                    'debet' => ($selisih * -1)
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::ReturPembelian,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => $harga_total
                ]);

                //jurnal L/R selisih
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kode_retur_in.' - L/R',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'debet' => ($selisih * -1)
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::BebanRugiBeli,
                    'referensi' => $kode_retur_in.' - L/R',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => ($selisih * -1)
                ]);
                //jurnal laba tahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $kode_retur_in.' - LDT',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'debet' => ($selisih * -1)
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kode_retur_in.' - LDT',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => ($selisih * -1)
                ]);

                $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $laba_tahan->kredit += $selisih;
                $laba_tahan->update();
            }elseif($selisih > 0){
                Jurnal::create([
                    'kode_akun' => Akun::PendapatanLain,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Keuntungan Retur '.$kode_retur_in,
                    'kredit' => $selisih
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::ReturPembelian,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => $harga_total
                ]);

                //jurnal L/R selisih
                Jurnal::create([
                    'kode_akun' => Akun::PendapatanLain,
                    'referensi' => $kode_retur_in.' - L/R',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'debet' => $selisih
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kode_retur_in.' - L/R',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => $selisih
                ]);
                //jurnal laba tahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kode_retur_in.' - LDT',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'debet' => $selisih
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $kode_retur_in.' - LDT',
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => $selisih
                ]);
                $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $laba_tahan->kredit += $selisih;
                $laba_tahan->update();
            }else{
                Jurnal::create([
                    'kode_akun' => Akun::ReturPembelian,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                    'kredit' => $harga_total
                ]);
            }
        }else{
            //stock_in and jurnal
            if($request->status == 'lain'){
                $retur_pembelian->total_uang = $request->harga_total_in;
                $persediaan = Akun::where('kode', Akun::Persediaan)->first();
                $persediaan->debet -= $hpp_keluar;
                $persediaan->update();

                $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
                $akun_ppn_masuk->debet -= $ppn_out;
                $akun_ppn_masuk->update();

                $hpp_masuk = 0;
                foreach ($request->item_kode_in as $i => $id) {
                    $item = Item::find($id);
                    $ppn = ceil($request->harga_in[$i] / 11 * 100)/100;
                    $harga = $request->harga_in[$i] - $ppn;
                    foreach($request->item_stok[$id] as $a => $item_id){
                        $kadaluarsa ='0000-00-00';
                        if($request->kadaluarsa_stok[$id][$a] != NULL){
                            $kadaluarsa = explode("-", $request->kadaluarsa_stok[$id][$a]);
                            $kadaluarsa = $kadaluarsa[2].'-'.$kadaluarsa[1].'-'.$kadaluarsa[0];
                        }
                        
                        $stock_masuk_n[$index_stok]['item_id'] = $id;
                        $stock_masuk_n[$index_stok]['item_kode'] = $item->kode;
                        $stock_masuk_n[$index_stok]['harga'] = $harga;
                        $stock_masuk_n[$index_stok]['transaksi_pembelian_id'] = $transaksi_pembelian->id;
                        $stock_masuk_n[$index_stok]['jumlah'] = $request->jumlah_stok[$id][$a];
                        $stock_masuk_n[$index_stok]['kadaluarsa'] = $kadaluarsa;

                        $index_stok += 1;
                        $hpp_masuk += $request->jumlah_stok[$id][$a] * $harga;
                        
                        // tambahan ngitung stoktotal
                        $item_x = Item::find($id);
                        $items_x = Item::where('kode_barang', $item_x->kode_barang)->get();

                        foreach ($items_x as $j => $item) {
                            $item->stoktotal += $request->jumlah_stok[$id][$a];
                            $item->update();
                        }
                    }
                    $data[''.$id] = [
                        'retur' => 1,
                        'jumlah' => $request->jumlah_in[$i],
                        'harga' => $request->harga_in[$i],
                        'subtotal' => $request->subtotal_in[$i],
                        // 'keterangan' => $request->keterangan[$id][$a],
                    ];
                }

                // $jumlah_total_masuk_lagi = $request->nominal_tunai_in + $request->nominal_transfer_in + $request->nominal_kartu_in + $request->nominal_cek_in + $request->nominal_bg_in + $request->harga_total_in;

                // $harga_masuk = $jumlah_total_masuk_lagi;
                $harga_masuk = $request->harga_total_in;
                $ppn_masuk = $request->harga_total_in - $hpp_masuk;

                //cari selisih barang masuk dan keluar
                $selisih_masuk_keluar = $request->harga_total_in - $harga_total - $request->jumlah_bayar_in;
                $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $laba_tahan->kredit += $selisih_masuk_keluar;
                $laba_tahan->update();
               
                //jurnal jika $selisih_masuk_keluar minus
                if ($selisih_masuk_keluar < 0) {
                    Jurnal::create([
                        'kode_akun' => Akun::BebanRugiBeli,
                        'referensi' => $kode_retur_in,
                        'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                        'debet' => $selisih_masuk_keluar * -1
                    ]);
                }
                //jurnal barang masuk
                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $hpp_masuk
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::PPNMasukan,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi.' (Barang Lain)',
                    'debet' => $ppn_masuk
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::ReturPembelian,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'kredit' => $harga_total
                ]);
                $persediaan = Akun::where('kode', Akun::Persediaan)->first();
                $persediaan->debet += $hpp_masuk;
                $persediaan->update();

                $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
                $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
                $akun_kas_cek = Akun::where('kode', Akun::KasCek)->first();
                $akun_kas_bg = Akun::where('kode', Akun::KasBG)->first();
                // TUNAI
                if ($request->nominal_tunai_in > 0) {
                    $retur_pembelian->nominal_tunai = $request->nominal_tunai_in;

                    $akun_kas_tunai->debet -= $request->nominal_tunai_in;
                    $akun_kas_tunai->update();
                    Jurnal::create([
                        'kode_akun' => $akun_kas_tunai->kode,
                        'referensi' => $kode_retur_in,
                        'keterangan' => $transaksi_pembelian->kode_transaksi,
                        'kredit' => $request->nominal_tunai_in
                    ]);

                    Arus::create([
                        'nama' => Arus::PBD,
                        'nominal' => $request->nominal_tunai_in,
                    ]);

                    if(Auth::user()->level_id == 5 && $request->nominal_tunai_in > 0){
                        $log = new LogLaci();
                        $log->pengirim = Auth::user()->id;
                        // $log->pengirim = $request->pengirim;
                        $log->approve = 1;
                        $log->status = 11;
                        $log->nominal = $request->nominal_tunai_in;
                        $log->save();

                        $laci = Laci::find(1);
                        $laci->gudang -= $request->nominal_tunai_in;
                        $laci->update();
                    }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                    {
                        if($request->nominal_tunai_in > 0){
                            $log = new LogLaci();
                            $log->pengirim = Auth::user()->id;
                            // $log->penerima = $request->penerima;
                            $log->approve = 1;
                            $log->status = 11;
                            $log->nominal = $request->nominal_tunai_in;
                            $log->save();

                            $laci = Laci::find(1);
                            $laci->owner -= $request->nominal_tunai_in;
                            $laci->update();
                        }
                    }
                }

                // TRANSER DAN KARTU
                if ($request->nominal_transfer_in > 0 || $request->nominal_kartu_in > 0) {
                    $keterangan_bank = $transaksi_pembelian->kode_transaksi;
                    $nominal_bank = 0;
                    if ($request->nominal_transfer_in > 0) {
                        $retur_pembelian->no_transfer = $request->no_transfer_in;
                        $retur_pembelian->bank_transfer = $request->bank_transfer_in;
                        $retur_pembelian->nominal_transfer = $request->nominal_transfer_in;
                        $akun_kas_bank->debet -= $request->nominal_transfer_in;
                        $akun_kas_bank->update();
                        $bank = Bank::find($request->bank_transfer_in);
                        $bank->nominal -= $request->nominal_transfer_in;
                        $bank->update();

                        $keterangan_bank = $keterangan_bank.' - '.$request->nominal_transfer.' via '.$bank->nama_bank;
                        $nominal_bank += $request->nominal_transfer_in;
                    }

                    if ($request->nominal_kartu_in > 0) {
                        // $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;
                        $retur_pembelian->no_kartu = $request->no_kartu_in;
                        $retur_pembelian->bank_kartu = $request->bank_kartu_in;
                        $retur_pembelian->jenis_kartu = $request->jenis_kartu_in;
                        $retur_pembelian->nominal_kartu = $request->nominal_kartu_in;
                        $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                        if ($request->jenis_kartu_in == 'kredit') {
                            $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                            $akun_kartu->kredit += $request->nominal_kartu_in;
                            $akun_kartu->update();

                            $bank = Bank::find($request->bank_kartu_in);
                            $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu_in.' via '.$bank->nama_bank;

                            Jurnal::create([
                                'kode_akun' => Akun::KartuKredit,
                                'referensi' => $kode_retur_in,
                                'keterangan' => $keterangan_bank,
                                'kredit' => $request->nominal_kartu_in
                            ]);
                        } else {
                            $akun_kas_bank->debet -= $request->nominal_kartu_in;
                            $akun_kas_bank->update();
                            $bank = Bank::find($request->bank_kartu_in);
                            $bank->nominal -= $request->nominal_kartu_in;
                            $bank->update();

                            $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu_in.' via '.$bank->nama_bank;

                            $nominal_bank += $request->nominal_kartu_in;
                        }
                    }

                    Jurnal::create([
                        'kode_akun' => Akun::KasBank,
                        'referensi' => $kode_retur_in,
                        'keterangan' => $keterangan_bank,
                        'kredit' => $nominal_bank
                    ]);

                    Arus::create([
                        'nama' => Arus::PBD,
                        'nominal' => $nominal_bank,
                    ]);
                }

                // CEK
                if ($request->nominal_cek_in > 0) {
                    if ($request->bank_cek_in == NULL) {
                        // Pilih cek
                        $retur_pembelian->no_cek = $request->no_cek_in;
                        $retur_pembelian->nominal_cek = $request->nominal_cek_in;
                        $akun_kas_cek->debet -= $request->nominal_cek_in;
                        $akun_kas_cek->update();

                        $cek = Cek::where('nomor', $request->no_cek_in)->where('nominal', $request->nominal_cek_in)->where('aktif', 1)->first();
                        // return $cek;
                        $cek->aktif = 0;
                        $cek->update();

                        Jurnal::create([
                            'kode_akun' => Akun::KasCek,
                            'referensi' => $kode_retur_in,
                            'keterangan' => $transaksi_pembelian->kode_transaksi,
                            'kredit' => $request->nominal_cek_in
                        ]);

                        Arus::create([
                            'nama' => Arus::PBD,
                            'nominal' => $request->nominal_cek_in,
                        ]);
                    } else {
                        // Cek baru
                        $retur_pembelian->no_cek = $request->no_cek_in;
                        $retur_pembelian->bank_cek = $request->bank_cek_in;
                        $retur_pembelian->nominal_cek = $request->nominal_cek_in;
                        $retur_pembelian->cek_lunas = 0;

                        $bank = Bank::find($request->bank_cek_in);
                        $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                        $akun->kredit += $request->nominal_cek_in;
                        $akun->update();

                        $cek = new Cek();
                        $cek->nomor = $request->no_cek_in;
                        $cek->nominal = $request->nominal_cek_in;
                        $cek->aktif = 0;
                        $cek->save();

                        Jurnal::create([
                            'kode_akun' => Akun::PembayaranProses,
                            'referensi' => $kode_retur_in,
                            'keterangan' => $transaksi_pembelian->kode_transaksi.' - via Cek '.$bank->nama_bank,
                            'kredit' => $request->nominal_cek_in
                        ]);
                    }
                }

                // BG
                if ($request->nominal_bg_in > 0) {
                    if ($request->bank_bg_in == NULL) {
                        // Pilih BG
                        $retur_pembelian->no_bg = $request->no_bg_in;
                        $retur_pembelian->nominal_bg = $request->nominal_bg_in;
                        $akun_kas_bg->debet -= $request->nominal_bg_in;
                        $akun_kas_bg->update();

                        $bg = BG::where('nomor', $request->no_bg_in)->where('nominal', $request->nominal_bg_in)->where('aktif', 1)->first();
                        $bg->aktif = 0;
                        $bg->update();

                        Jurnal::create([
                            'kode_akun' => Akun::KasBG,
                            'referensi' => $kode_retur_in,
                            'keterangan' => $transaksi_pembelian->kode_transaksi,
                            'kredit' => $request->nominal_bg_in
                        ]);

                        Arus::create([
                            'nama' => Arus::PBD,
                            'nominal' => $request->nominal_bg_in,
                        ]);
                    } else {
                        // BG Baru
                        $retur_pembelian->no_bg = $request->no_bg_in;
                        $retur_pembelian->bank_bg = $request->bank_bg_in;
                        $retur_pembelian->nominal_bg = $request->nominal_bg_in;
                        $retur_pembelian->bg_lunas = 0;

                        $bank = Bank::find($request->bank_bg_in);
                        $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                        $akun->kredit += $request->nominal_bg_in;
                        $akun->update();

                        $bg = new BG();
                        $bg->nomor = $request->no_bg_in;
                        $bg->nominal = $request->nominal_bg_in;
                        $bg->aktif = 0;
                        $bg->save();

                        Jurnal::create([
                            'kode_akun' => Akun::PembayaranProses,
                            'referensi' => $kode_retur_in,
                            'keterangan' => $transaksi_pembelian->kode_transaksi.' - via BG '.$bank->nama_bank,
                            'kredit' => $request->nominal_bg_in
                        ]);
                    }
                }

                if ($selisih_masuk_keluar > 0) {
                    Jurnal::create([
                        'kode_akun' => Akun::PendapatanLain,
                        'referensi' => $kode_retur_in,
                        'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                        'kredit' => $selisih_masuk_keluar
                    ]);

                    //jurnal L/R selisih_masuk_keluar
                    Jurnal::create([
                        'kode_akun' => Akun::PendapatanLain,
                        'referensi' => $kode_retur_in.' - L/R',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'debet' => $selisih_masuk_keluar
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur_in.' - L/R',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'kredit' => $selisih_masuk_keluar
                    ]);
                    //jurnal laba tahan
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur_in.' - LDT',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'debet' => $selisih_masuk_keluar
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur_in.' - LDT',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'kredit' => $selisih_masuk_keluar
                    ]);
                } elseif($selisih_masuk_keluar < 0) {
                    //jurnal L/R selisih_masuk_keluar
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur_in.' - L/R',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'debet' => ($selisih_masuk_keluar * -1)
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::BebanRugiBeli,
                        'referensi' => $kode_retur_in.' - L/R',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'kredit' => ($selisih_masuk_keluar * -1)
                    ]);
                    //jurnal laba tahan
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur_in.' - LDT',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'debet' => ($selisih_masuk_keluar * -1)
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur_in.' - LDT',
                        'keterangan' => 'Retur Transaksi '.$kode_retur_in,
                        'kredit' => ($selisih_masuk_keluar * -1)
                    ]);
                }

                $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
                $akun_ppn_masuk->debet += $ppn_masuk;
                $akun_ppn_masuk->update();
            }

            if($request->status == 'sama'){
                foreach ($request->item_kode_in_sama as $i => $id) {
                    $item = Item::find($id);
                    // $satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $request->satuan_id[$a])->first()->konversi;
                    foreach($request->item_stok_sama[$id] as $a => $item_id){
                        $kadaluarsa = '0000-00-00';
                        if($request->kadaluarsa_stok_sama[$id][$a] != NULL){
                            $kadaluarsa = explode("-", $request->kadaluarsa_stok_sama[$id][$a]);
                            $kadaluarsa = $kadaluarsa[2].'-'.$kadaluarsa[1].'-'.$kadaluarsa[0];
                        }

                        $stock_masuk_n[$index_stok]['item_id'] = $id;
                        $stock_masuk_n[$index_stok]['item_kode'] = $item->kode;
                        $stock_masuk_n[$index_stok]['harga'] = $request->harga_in_sama[$i];
                        $stock_masuk_n[$index_stok]['transaksi_pembelian_id'] = $transaksi_pembelian->id;
                        $stock_masuk_n[$index_stok]['jumlah'] = $request->jumlah_stok_sama[$id][$a];
                        $stock_masuk_n[$index_stok]['kadaluarsa'] = $kadaluarsa;

                        $index_stok += 1;
                    }
                }

                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $hpp_keluar
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::PPNMasukan,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'debet' => $ppn_out
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::ReturPembelian,
                    'referensi' => $kode_retur_in,
                    'keterangan' => 'Retur Transaksi '.$transaksi_pembelian->kode_transaksi,
                    'kredit' => $harga_total
                ]);
            }
        }
        
        if ($retur_pembelian->save()) {
            $retur_pembelian->items()->sync($data);
            $result = ReturPembelian::get()->last();
            foreach ($stock_masuk_n as $r => $data) {
                $stok_baru = new Stok();
                $stok_baru->item_id = $data['item_id'];
                $stok_baru->item_kode = $data['item_kode'];
                $stok_baru->transaksi_pembelian_id = $data['transaksi_pembelian_id'];
                $stok_baru->retur_pembelian_id = $result->id;
                $stok_baru->jumlah = $data['jumlah'];
                $stok_baru->harga = $data['harga'];
                $stok_baru->kadaluarsa = $data['kadaluarsa'];
                $stok_baru->save();
            }
            return redirect('/retur-pembelian')->with('sukses', 'tambah');
        } else {
            return redirect('/retur-pembelian')->with('gagal', 'tambah');
        }
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_retur_terakhir = ReturPembelian::all()->last();
        // return $transaksi_retur_terakhir;
        if ($transaksi_retur_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPMR/'.$tanggal;
        } else {
            $kode_transaksi_retur_terakhir = $transaksi_retur_terakhir->kode_retur;
            $tanggal_transaksi_retur_terakhir = substr($kode_transaksi_retur_terakhir, 11, 10);
            // return $tanggal_transaksi_retur_terakhir;
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_retur_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_retur_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPMR/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPMR/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showFromTransaksi($transaksi_pembelian_id, $retur_pembelian_id)
    {
        // $transaksi_pembelian = TransaksiPembelian::find($transaksi_pembelian_id);
        // $retur_pembelian = ReturPembelian::with('items')->find($retur_pembelian_id);
        
        // return view('retur_pembelian.show_from_transaksi', compact('transaksi_pembelian', 'retur_pembelian'));

        return $this->show($retur_pembelian_id);
        // $kode_retur = $this->kodeTransaksiBaru();

    }

    public function show($id)
    {
        $retur_pembelian = ReturPembelian::with('items')->find($id);
        // return $id;
        if ($retur_pembelian == null) {
            return redirect('/transaksi-pembelian/'.$id);
        }
        $selisih_keluar_masuk = 0;
        $transaksi_pembelian = $retur_pembelian->transaksi_pembelian;
        if($retur_pembelian->total_uang != NULL && $retur_pembelian->total_uang > 0){
            $selisih = $retur_pembelian->harga_total - $retur_pembelian->total_uang;
        }

        $item_out = RelasiReturPembelian::where('retur_pembelian_id', $retur_pembelian->id)->where('retur', 0)->get();

        $items = array();
        $hasil = array();
        foreach ($item_out as $i => $data) {
            $items[$i] = Item::find($data->item_id);
            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$i]->kode)->orderBy('konversi', 'dsc')->get();
            // return $stok_relasi;
            $sisa_jumlah[$i] = $data->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];

                if($hitung > 0){
                    $a = $a + 1;
                    $hasil[$i][$a]['item_kode'] = $items[$i]->id;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }

                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }

        if($retur_pembelian->status == 'lain'){
            $item_in = RelasiReturPembelian::where('retur_pembelian_id', $retur_pembelian->id)->where('retur', 1)->get();
            $stok_masuk = Stok::where('retur_pembelian_id', $id)->get();
            $items_in = array();
            $hasil_in = array();
            $stok_in = array();
            foreach ($item_in as $i => $data) {
                $items_in[$i] = Item::find($data->item_id);
                $satuans[$i] = RelasiSatuan::where('item_kode', $items_in[$i]->kode)->orderBy('konversi', 'dsc')->get();
                // return $stok_relasi;
                $sisa_jumlah[$i] = $data->jumlah;
                $a=-1;
                for($x=0; $x < count($satuans[$i]); $x++){
                    $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];

                    if($hitung > 0){
                        $a = $a + 1;
                        $hasil_in[$i][$a]['item_kode'] = $items_in[$i]->id;
                        $hasil_in[$i][$a]['jumlah'] = $hitung;
                        $hasil_in[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                    }

                    $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
                }
            }

            foreach ($stok_masuk as $i => $data) {
                $kode = Item::find($data->item_id)->kode;
                $satuans[$i] = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'dsc')->get();
                // return $stok_relasi;
                $sisa_jumlah[$i] = $data->jumlah;
                $a=-1;
                for($x=0; $x < count($satuans[$i]); $x++){
                    $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];

                    if($hitung > 0){
                        $a = $a + 1;
                        $stok_in[$i][$a]['item_kode'] = $items_in[$i]->id;
                        $stok_in[$i][$a]['jumlah'] = $hitung;
                        $stok_in[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                    }

                    $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
                }
            }

            $jumlah_uang = $retur_pembelian->nominal_tunai_in 
                            + $retur_pembelian->nominal_kartu_in
                            + $retur_pembelian->nominal_transfer_in
                            + $retur_pembelian->nominal_cek_in
                            + $retur_pembelian->nominal_bg_in;

            $selisih_keluar_masuk = $retur_pembelian->total_uang - $retur_pembelian->harga_total - $jumlah_uang;
            // kalo minus rugi
            // kalo plus laba

        }

        // return $hasil_in;
        return view('retur_pembelian.show_from_retur', compact('transaksi_pembelian', 'retur_pembelian', 'selisih', 'item_out', 'item_in', 'hasil_in', 'hasil', 'stok_masuk', 'stok_in', 'selisih_keluar_masuk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CekReturLunas($id)
    {
        $retur_pembelian = ReturPembelian::find($id);
        $retur_pembelian->cek_lunas = 1;
        // $retur_pembelian->sisa_utang -= $retur_pembelian->nominal_cek;
        $retur_pembelian->update();

        $bank = Bank::find($retur_pembelian->bank_cek);
        $bank->nominal -= $retur_pembelian->nominal_cek;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $retur_pembelian->nominal_cek;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $retur_pembelian->nominal_cek;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $retur_pembelian->kode_retur.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Cek via Bank '.$bank->nama_bank,
            'debet' => $retur_pembelian->nominal_cek,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $retur_pembelian->kode_retur.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Cek via Bank '.$bank->nama_bank,
            'kredit' => $retur_pembelian->nominal_cek,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $retur_pembelian->nominal_cek,
        ]);
        return redirect('/retur-pembelian/'.$id)->with('sukses', 'cek');
    }

    public function BGReturLunas($id)
    {
        $retur_pembelian = ReturPembelian::find($id);
        $retur_pembelian->bg_lunas = 1;
        // return $retur_pembelian;
        // $retur_pembelian->sisa_utang -= $retur_pembelian->nominal_bg;
        $retur_pembelian->update();

        $bank = Bank::find($retur_pembelian->bank_bg);
        $bank->nominal -= $retur_pembelian->nominal_bg;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $retur_pembelian->nominal_bg;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $retur_pembelian->nominal_bg;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $retur_pembelian->kode_retur.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran BG via Bank '.$bank->nama_bank,
            'debet' => $retur_pembelian->nominal_bg,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $retur_pembelian->kode_retur.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran BG via Bank '.$bank->nama_bank,
            'kredit' => $retur_pembelian->nominal_bg,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $retur_pembelian->nominal_bg,
        ]);
        return redirect('/retur-pembelian/'.$id)->with('sukses', 'bg');
    }
}
