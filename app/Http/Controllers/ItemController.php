<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Yajra\DataTables\Datatables; 

use App\Item;
use App\Stok;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Satuan;
use App\Suplier;
use App\JenisItem;
use App\RelasiBonus;
use App\RelasiBundle;
use App\RelasiSatuan;
use App\TransaksiPembelian;
use App\TransaksiPenjualan;
use App\RelasiTransaksiPembelian;
use App\RelasiTransaksiPenjualan;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    /* public function index()
    {
        $items = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('jenis_item_id'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('valid_konversi'),
                // DB::raw('sum(stoktotal) as stok'))
                DB::raw('stoktotal as stok'))
            ->whereIn('konsinyasi', [0])
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        // return $items;
        $konsinyasi = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('jenis_item_id'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('jenis_item_id'),
                // DB::raw('sum(stoktotal) as stok'))
                DB::raw('stoktotal as stok'))
            ->where('konsinyasi', 1)
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        $bundle = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('jenis_item_id'),
                // DB::raw('sum(stoktotal) as stok'))
                DB::raw('stoktotal as stok'))
            ->where('konsinyasi', 2)
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        $item_bonus = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('jenis_item_id'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('valid_konversi'),
                // DB::raw('sum(stoktotal) as stok'))
                DB::raw('stoktotal as stok'))
            ->whereIn('konsinyasi', [3])
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        $item_off = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('jenis_item_id'))
                // DB::raw('sum(stoktotal) as stok'))
            ->where('aktif', 0)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        $jenis_items = JenisItem::whereNotIn('id', [1])->get();
        $supliers = Suplier::whereNotIn('id', [Suplier::notSuplier])->get();

        // return $items;
        return view('item.index', compact('items', 'jenis_items', 'supliers', 'konsinyasi', 'item_off', 'bundle', 'item_bonus'));
    } */

    public function index()
    {
        $jenis_items = JenisItem::whereNotIn('id', [1])->get();
        $supliers = Suplier::whereNotIn('id', [Suplier::notSuplier])->get();

        return view('item.index', compact('jenis_items', 'supliers'));
    }

    // Item Biasa
    private static function mdt1Stok($item)
    {
        $stok = '';
        $item_kode = $item->kode;
        $jumlah = $item->stok;

        $satuan_item = [];
        $satuan_pembelians = RelasiSatuan
            ::where('item_kode', $item->kode)
            ->orderBy('konversi', 'asc')->get();
        for ($i = 0; $i < count($satuan_pembelians); $i++) {
            $satuan = [
                'kode' => $satuan_pembelians[$i]->satuan->kode,
                'konversi' => $satuan_pembelians[$i]->konversi,
            ];
            array_push($satuan_item, $satuan);
        }

        $text_stoktotal = '-';
        $temp_stoktotal = $jumlah;
        for ($i = 0; $i < count($satuan_item); $i++) {
            if ($temp_stoktotal > 0) {
                $satuan = $satuan_item[$i];
                $jumlah_stok = intval($temp_stoktotal / $satuan['konversi']);
                if ($jumlah_stok > 0) {
                    if ($text_stoktotal == '-') $text_stoktotal = '';
                    $text_stoktotal .= $jumlah_stok;
                    $text_stoktotal .= ' ';
                    $text_stoktotal .= $satuan['kode'];

                    $temp_stoktotal = $temp_stoktotal % $satuan['konversi'];
                    if ($i != count($satuan_item) - 1 && $temp_stoktotal > 0) $text_stoktotal .= ' ';
                }
            }
        }

        $stok = $text_stoktotal;
        if ($jumlah > 0 && $text_stoktotal == '-') {
            $stok = $jumlah;
        }

        return $stok;
    }

    // Item Biasa
    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'items.' . $field;
        if ($request->field == 'jenis_item_nama') {
            $field = 'jenis_items.nama';
        }

        $items = Item
            ::select(
                'items.id',
                'items.kode',
                'items.kode_barang',
                'items.nama',
                'items.diskon',
                'jenis_items.nama as jenis_item_nama',
                'items.aktif',
                'items.konsinyasi',
                'items.valid_konversi',
                'items.stoktotal as stok'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [0])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->groupBy('items.kode_barang')
            ->orderBy($field, $order)
            ->get();

        $count = Item
            ::select(
                'items.id'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [0])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            // ->groupBy('kode_barang')
            // ->get();
            ->distinct('items.kode_barang')
            ->count('items.kode_barang');

        foreach ($items as $i => $item) {

            $item->mdt_stok = self::mdt1Stok($item);

            if ($item->valid_konversi == 1) {
                $item->mdt_valid_konversi = 'Valid';
            } else {
                $item->mdt_valid_konversi = 'Belum Valid';
            }

            $buttons['history_harga_beli'] = ['url' => url('item/history-harga-beli/'.$item->kode_barang)];
            $buttons['history_harga_jual'] = ['url' => url('item/history-harga-jual/'.$item->kode_barang)];
            $buttons['detail'] = ['url' => url('item/show/'.$item->kode_barang)];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $item->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $items,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    // Item Bonus
    private static function mdt2Stok($item)
    {
        return self::mdt1Stok($item);
    }

    // Item Bonus
    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'items.' . $field;
        if ($request->field == 'jenis_item_nama') {
            $field = 'jenis_items.nama';
        }

        $bonuses = Item
            ::select(
                'items.id',
                'items.kode',
                'items.kode_barang',
                'items.nama',
                'items.diskon',
                'jenis_items.nama as jenis_item_nama',
                'items.aktif',
                'items.konsinyasi',
                'items.valid_konversi',
                'items.stoktotal as stok'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [3])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->groupBy('items.kode_barang')
            ->orderBy($field, $order)
            ->get();

        $count = Item
            ::select(
                'items.id'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [3])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            // ->groupBy('kode_barang')
            // ->get();
            ->distinct('items.kode_barang')
            ->count('items.kode_barang');

        foreach ($bonuses as $i => $bonus) {

            $bonus->mdt_stok = self::mdt1Stok($bonus);

            if ($bonus->valid_konversi == 1) {
                $bonus->mdt_valid_konversi = 'Valid';
            } else {
                $bonus->mdt_valid_konversi = 'Belum Valid';
            }

            $buttons['history_harga_beli'] = ['url' => url('item/history-harga-beli/'.$bonus->kode_barang)];
            $buttons['history_harga_jual'] = ['url' => url('item/history-harga-jual/'.$bonus->kode_barang)];
            $buttons['detail'] = ['url' => url('item/show/'.$bonus->kode_barang)];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $bonus->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bonuses,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    // Item Konsinyasi
    private static function mdt3Stok($item)
    {
        return self::mdt1Stok($item);
    }

    // Item Konsinyasi
    public function mdt3(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'items.' . $field;
        if ($request->field == 'jenis_item_nama') {
            $field = 'jenis_items.nama';
        }

        $konsinyasis = Item
            ::select(
                'items.id',
                'items.kode',
                'items.kode_barang',
                'items.nama',
                'items.diskon',
                'jenis_items.nama as jenis_item_nama',
                'items.aktif',
                'items.konsinyasi',
                'items.valid_konversi',
                'items.stoktotal as stok'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [1])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->groupBy('items.kode_barang')
            ->orderBy($field, $order)
            ->get();

        $count = Item
            ::select(
                'items.id'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [1])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.stoktotal', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.valid_konversi', 'like', '%'.$request->search_query.'%');
            })
            // ->groupBy('kode_barang')
            // ->get();
            ->distinct('items.kode_barang')
            ->count('items.kode_barang');

        foreach ($konsinyasis as $i => $konsinyasi) {

            $konsinyasi->mdt_stok = self::mdt1Stok($konsinyasi);

            if ($konsinyasi->valid_konversi == 1) {
                $konsinyasi->mdt_valid_konversi = 'Valid';
            } else {
                $konsinyasi->mdt_valid_konversi = 'Belum Valid';
            }

            $buttons['history_harga_beli'] = ['url' => url('item/history-harga-beli/'.$konsinyasi->kode_barang)];
            $buttons['history_harga_jual'] = ['url' => url('item/history-harga-jual/'.$konsinyasi->kode_barang)];
            $buttons['detail'] = ['url' => url('item/show/'.$konsinyasi->kode_barang)];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $konsinyasi->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $konsinyasis,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    // Item Bundle
    public function mdt4(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'items.' . $field;
        if ($request->field == 'jenis_item_nama') {
            $field = 'jenis_items.nama';
        }

        $bundles = Item
            ::select(
                'items.id',
                'items.kode',
                'items.kode_barang',
                'items.nama',
                'items.diskon',
                'jenis_items.nama as jenis_item_nama',
                'items.aktif',
                'items.konsinyasi',
                'items.stoktotal as stok'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [2])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->groupBy('items.kode_barang')
            ->orderBy($field, $order)
            ->get();

        $count = Item
            ::select(
                'items.id'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->whereIn('items.konsinyasi', [2])
            ->where('items.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            // ->groupBy('kode_barang')
            // ->get();
            ->distinct('items.kode_barang')
            ->count('items.kode_barang');

        foreach ($bundles as $i => $bundle) {

            // $buttons['history_harga_beli'] = ['url' => url('item/history-harga-beli/'.$bundle->kode_barang)];
            $buttons['history_harga_jual'] = ['url' => url('item/history-harga-jual/'.$bundle->kode_barang)];
            $buttons['detail'] = ['url' => url('item-bundle/show/'.$bundle->kode)];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $bundle->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bundles,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    // Item History
    public function mdt5(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'items.' . $field;
        if ($request->field == 'jenis_item_nama') {
            $field = 'jenis_items.nama';
        }

        $items = Item
            ::select(
                'items.id',
                'items.kode',
                'items.kode_barang',
                'items.nama',
                'items.diskon',
                'jenis_items.nama as jenis_item_nama',
                'items.aktif',
                'items.konsinyasi',
                'items.stoktotal as stok'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->where('items.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->groupBy('items.kode_barang')
            ->orderBy($field, $order)
            ->get();

        $count = Item
            ::select(
                'items.id'
            )
            ->leftJoin('jenis_items', 'jenis_items.id', 'items.jenis_item_id')
            ->where('items.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('items.kode_barang', 'like', '%'.$request->search_query.'%')
                ->orWhere('items.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            // ->groupBy('kode_barang')
            // ->get();
            ->distinct('items.kode_barang')
            ->count('items.kode_barang');

        foreach ($items as $i => $item) {

            if ($item->konsinyasi == 2) {
                $buttons['detail'] = ['url' => url('item-bundle/show/'.$item->kode)];
            } else {
                $buttons['detail'] = ['url' => url('item/show/'.$item->kode_barang)];
            }

            // $buttons['history_harga_beli'] = ['url' => url('item/history-harga-beli/'.$item->kode_barang)];
            // $buttons['history_harga_jual'] = ['url' => url('item/history-harga-jual/'.$item->kode_barang)];
            $buttons['aktifkan'] = ['url' => ''];

            // return $buttons;
            $item->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;


        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $items,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function indexJson()
    {
        $items = Item::all();
        return $items->toJson();
    }

    public function stokJson($id)
    {
        $stok = stok::where('item_kode', $id)->get();
        $relasi_satuan = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'desc')->get();
        return response()->json([
            'stok' => $stok,
            'relasi_satuan' => $relasi_satuan
        ]);
    }

    public function SatuanJson($id)
    {
        $relasi_satuan = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'desc')->get();
        return response()->json([
            'relasi_satuan' => $relasi_satuan
        ]);
    }

    public function create()
    {
        $bonuses = Bonus::all();
        $jenis_items = JenisItem::all();
        $supliers = Suplier::whereNotIn('id', [Suplier::notSuplier])->get();;
        return view('item.create', compact('jenis_items','items', 'bonuses', 'supliers'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        if ($request->kode != null) {
            $check_item = Item::where('kode', $request->kode)->get();
            if (count($check_item) > 0) {
                return redirect('/item')->with('gagal', 'tambah');
            }
        }

        if ($request->nama != null) {
            $check_item = Item::where('nama', $request->nama)->get();
            if (count($check_item) > 0) {
                return redirect('/item')->with('gagal', 'tambah');
            }
        }

        $item = new Item();
        $item->kode = $request->kode;
        $item->nama = strtoupper($request->nama);
        $item->nama_pendek = strtoupper($request->nama_pendek);
        $item->suplier_id = $request->suplier_id;
        $item->jenis_item_id = $request->jenis_item_id;
        $item->konsinyasi = $request->konsinyasi == null ? 3 : $request->konsinyasi;
        $item->stoktotal = 0;
        $item->limit_stok = 0;
        $item->limit_grosir = 0;
        // $item->satuan_limit = 0;
        $item->diskon = 0;
        // $item->profit = 1;
        $item->aktif = 1;

        if ($item->konsinyasi == 3) {
            $item->bonus = 1;
        }

        if ($item->save()) {
            $kode_barang = 'KMK-';
            $kode_barang .= $item->jenis_item->kode . '-';
            $kode_barang .= Util::int4digit($item->id);

            if ($item->kode == '') {
                $kode_new = '';
                while (true) {
                    $kode_new = 'KMK'.Util::generateRandomString(10);
                    $temp_item = Item::where('kode', $kode_new)->first();
                    if ($temp_item == null) break;
                }
                
                $item->kode = $kode_new;
            }
            $item->kode_barang = $kode_barang;
            $item->update();

            if (in_array($item->konsinyasi, [3])) {
                $relasi_satuan = new RelasiSatuan();
                $relasi_satuan->item_kode = $item->kode;
                $relasi_satuan->satuan_id = 1;
                $relasi_satuan->konversi = 1;
                $relasi_satuan->save();

                $relasi_harga = new Harga();
                $relasi_harga->item_kode = $item->kode;
                $relasi_harga->jumlah = 1;
                $relasi_harga->satuan_id = 1;
                $relasi_harga->eceran = 0;
                $relasi_harga->grosir = 0;
                $relasi_harga->save();
            }

            if (in_array($item->konsinyasi, [2])) {
                $relasi_satuan = new RelasiSatuan();
                $relasi_satuan->item_kode = $item->kode;
                $relasi_satuan->satuan_id = 99;
                $relasi_satuan->konversi = 1;
                $relasi_satuan->save();

                $relasi_harga = new Harga();
                $relasi_harga->item_kode = $item->kode;
                $relasi_harga->jumlah = 1;
                $relasi_harga->satuan_id = 99;
                $relasi_harga->eceran = 0;
                $relasi_harga->grosir = 0;
                $relasi_harga->save();
            }
            $kode = $item->kode;

            if($request->konsinyasi == 2){
                return redirect('/item-bundle/show/'.$kode)->with('sukses', 'tambah');
            }else{
                return redirect('/item/show/'.$kode_barang)->with('sukses', 'tambah');
            }
        } else {
            // return redirect('/item/show')->with('gagal', 'tambah');
            return redirect('/item')->with('gagal', 'tambah');
        }
    }

    public function show($kode_barang)
    {
        // return $kode_barang;
        $satuans = Satuan::all();
        $bonuses = Item::where('aktif', 1)->where('bonus', 1)->where('konsinyasi', 3)->groupBy('kode_barang')->get();
        $item = Item::where('kode_barang', $kode_barang)->first();
        // return $item;
        $kode = $item->kode;
        $relasi_satuans = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'desc')->get();
        $stoks = Stok::where('item_kode', $kode)->orderBy('created_at', 'dsc')->get();
        // $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode_barang')->having('kode_barang', '=', $kode_barang)->get()->first();
        $stok = Item::select(DB::raw('stoktotal as stok'))->groupBy('kode_barang')->having('kode_barang', '=', $kode_barang)->get()->first();
        $relasi_bonuses = RelasiBonus::where('item_kode', $kode)->orderBy('syarat', 'asc')->get();

        // $item_kodes = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->distinct()->get(['kode']);
        // $items = array();
        // foreach ($item_kodes as $i => $item_kode) {
        //     $temp_items = Item::where('kode', $item_kode)->get();
        //     foreach ($temp_items as $j => $temp_item) {
        //         array_push($items, $temp_item);
        //     }
        // }

        $suplier_ids = Item::where('kode_barang', $kode_barang)->whereNotIn('suplier_id', [Suplier::notSuplier])->distinct()->get(['suplier_id']);
        // return $suplier_ids;
        $supliers = array();
        foreach ($suplier_ids as $i => $suplier_id) {
            // if ($suplier_id != '999') {
            array_push($supliers, Suplier::find($suplier_id->suplier_id));
            // }
        }
        // return $supliers;

        // $item_suplier = Item::(where)

        $satuan_bonuses = array();
        foreach ($relasi_bonuses as $key => $relasi_bonus) {
            $satuan_bonuse_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            foreach ($satuan_bonuse_relasi as $i => $sbr) {
                $konversi = $sbr->konversi;
                if ($relasi_bonus->syarat >= $konversi && $relasi_bonus->syarat % $konversi == 0) {
                    $satuan_bonuses[$key]['konversi'] = $konversi;
                    $satuan_bonuses[$key]['satuan'] = $sbr->satuan;
                }
            }
        }

        $limit_stok = array();
        if($item->limit_stok != NULL && $item->limit_stok != 0) {
            $limit_stok_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            foreach ($limit_stok_relasi as $i => $lsr) {
                $konversi = $lsr->konversi;
                if ($item->limit_stok >= $konversi && $item->limit_stok % $konversi == 0) {
                    $limit_stok['jumlah'] = $item->limit_stok / $konversi;
                    $limit_stok['satuan'] = Satuan::find($lsr->satuan_id)->nama;
                }
            }
        }

        $limit_grosir = array();
        $in_grosir = array();
        if($item->limit_grosir != NULL && $item->limit_grosir != 0) {
            $limit_grosir_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            $_is_grosir = false;
            $satuan_grosir = 0;
            // return $limit_grosir_relasi;
            foreach ($limit_grosir_relasi as $i => $lsk) {
                $konversi = $lsk->konversi;
                if ($item->limit_grosir >= $konversi && $item->limit_grosir % $konversi == 0) {
                    $limit_grosir['jumlah'] = $item->limit_grosir / $konversi;
                    $limit_grosir['satuan'] = Satuan::find($lsk->satuan_id)->nama;
                    $satuan_grosir = $lsk->satuan_id;
                }
            }

            foreach ($limit_grosir_relasi as $i => $lsk) {
                if ($lsk->satuan_id == $satuan_grosir) {
                    $_is_grosir = true;
                }

                if($_is_grosir) array_push($in_grosir, $lsk->satuan_id);
            }

        }

        $jumlah_total = $stok->stok;
        $bonus = $item->syarat_bonus;
        $stoklimit = $item->limit_stok;

        $stok_total = array();
        $stok_pcs = array();
        $bonus_arr = array();
        $stoklimit_arr = array();
        $harga_satuan = array();

        for ($i = 0; $i < count($stoks); $i++) {
            $jumlah_pcs = $stoks[$i]->jumlah;
            $harga_dasar = $stoks[$i]->harga;

            for ($j = 0; $j < count($relasi_satuans); $j++) {
                $hasil_pcs = explode('.', $jumlah_pcs / $relasi_satuans[$j]->konversi)[0];
                $sisa_pcs  = $jumlah_pcs % $relasi_satuans[$j]->konversi;

                $jumlah_pcs = $sisa_pcs;
                $harga = $harga_dasar * $relasi_satuans[$j]->konversi;

                $harga_satuan[$i][$j] = [
                    'harga' => $harga,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];

                $stok_pcs[$i][$j] = [
                    'jumlah' => $hasil_pcs,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];
            }
        }

        for ($i = 0; $i < count($relasi_satuans); $i++) {
            $hasil_total = explode('.', $jumlah_total / $relasi_satuans[$i]->konversi)[0];
            $sisa_total  = $jumlah_total % $relasi_satuans[$i]->konversi;

            $bonus_total = explode('.', $bonus / $relasi_satuans[$i]->konversi)[0];
            $bonus_sisa  = $bonus % $relasi_satuans[$i]->konversi;

            $stoklimit_total = explode('.', $stoklimit / $relasi_satuans[$i]->konversi)[0];
            $stoklimit_sisa  = $stoklimit % $relasi_satuans[$i]->konversi;

            $jumlah_total = $sisa_total;
            $bonus = $bonus_sisa;
            $stoklimit = $stoklimit_sisa;

            $stok_total[$i] = ['jumlah' => $hasil_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $bonus_arr[$i] = ['jumlah' => $bonus_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $stoklimit_arr[$i] = ['jumlah' => $stoklimit_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
        }

        // $hargas = Harga::where('item_kode', $kode)->orderBy('grosir', 'dsc')->get();
        $hargas = array();
        $nego = [];
        $harga_stok = 0;
        if (sizeof($stoks) > 0)  {
            $harga_stok = $stoks[0]->harga * 1.1;
        } 
        // return $harga_stok;
        foreach ($relasi_satuans as $i => $relasi_satuan) {
            $harga = Harga::where('item_kode', $kode)->where('satuan_id', $relasi_satuan->satuan_id)->first();
            array_push($hargas, $harga);
            
            $harga_uji = ceil($harga_stok * $relasi_satuan->konversi * (1 + $item->profit / 100) / 100) * 100;
            $harga_cek_ecer = ceil($harga_stok * $relasi_satuan->konversi * (1 + $item->profit_eceran / 100) / 100) * 100;

            $nego_allow = false;
            $eceran_plus = false;
            if (($harga->grosir - $harga_uji) > 100) {
                $nego_allow = true;
                $persen_nego = ($harga->grosir - $harga_uji) / $harga_uji * 100;
            } else {
                $persen_nego = 0;
            }
            if (($harga->eceran - $harga_cek_ecer) > 0) $eceran_plus = true;

            // return [$harga->eceran, $harga_cek_ecer];
            array_push($nego, ['eceran' => $eceran_plus, 'grosir' => $nego_allow, 'persen' => $persen_nego]);
        }

        // return $nego;
        // return [$kode, $relasi_satuans, $hargas];
        // return $item;

        $satuan_stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'dsc')->get();
        $disable_form_satuan = false;
        if (count($satuan_stoks) > 0) $disable_form_satuan = true;

        // return $stok_total;
        if ($item->aktif == 1) {
            $item_kodes = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->distinct()->get(['kode']);
            $items = array();
            foreach ($item_kodes as $i => $item_kode) {
                $temp_items = Item::where('kode', $item_kode)->get();
                foreach ($temp_items as $j => $temp_item) {
                    array_push($items, $temp_item);
                }
            }
            return view('item.show', compact('item', 'items', 'item_kodes', 'supliers', 'stok', 'satuans', 'relasi_satuans', 'stoks', 'stok_total', 'bonus_arr', 'stoklimit_arr', 'stok_pcs', 'harga_satuan', 'bonuses', 'relasi_bonuses', 'satuan_bonuses', 'limit_stok', 'limit_grosir', 'hargas', 'disable_form_satuan', 'in_grosir', 'nego'));
        } else {
            $item_kodes = Item::where('kode_barang', $kode_barang)->where('aktif', 0)->distinct()->get(['kode']);
            $items = array();
            foreach ($item_kodes as $i => $item_kode) {
                $temp_items = Item::where('kode', $item_kode)->get();
                foreach ($temp_items as $j => $temp_item) {
                    array_push($items, $temp_item);
                }
            }
            return view('item.show_off', compact('item', 'items', 'item_kodes', 'supliers', 'stok', 'satuans', 'relasi_satuans', 'stoks', 'stok_total', 'bonus_arr', 'stoklimit_arr', 'stok_pcs', 'harga_satuan', 'bonuses', 'relasi_bonuses', 'satuan_bonuses', 'limit_stok', 'limit_grosir', 'hargas', 'disable_form_satuan', 'nego'));
        }
    }

    public function showBundle($kode)
    {
        $item = Item::where('kode', $kode)->first();
        $is_aktif = false;
        if ($item->aktif == 1) {
            $is_aktif = true;
        }
        // return $item;
        $item_detail = $item;
        $id = $kode;
        $satuans = Satuan::all();
        $relasi_satuans = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'desc')->get();
        // $bonuses = Item::where('bonus', 1)->get();
        $bonuses = Item::where('aktif', 1)->where('bonus', 1)->where('konsinyasi', 3)->groupBy('kode_barang')->get();
        $item_kode_super = $item->kode;
        $kode_barang_super = $item->kode_barang;
        $items = Item::where('kode', $id)->get();
        $temp_item_list = Item::select(
                DB::raw('kode_barang'))
            ->where('konsinyasi', '!=', 2)
            // ->where('konsinyasi', 1)
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            // ->orderBy('kode', 'asc')
            ->get();
        $item_kodes = Item::where('kode_barang', $item->kode_barang)->distinct()->get(['kode']);
        $item_list = array();
        foreach ($temp_item_list as $i => $temp_item) {
            $temp_items = Item::where('kode_barang', $temp_item->kode_barang)->get();
            if (count($temp_items) == 1) {
                $push_item = Item::select(
                    DB::raw('kode'),
                    DB::raw('nama'),
                    DB::raw('diskon'),
                    DB::raw('aktif'),
                    DB::raw('konsinyasi'))
                ->where('konsinyasi', '!=', 2)
                // ->where('konsinyasi', 1)
                ->where('kode_barang', $temp_item->kode_barang)
                ->where('aktif', 1)
                ->groupBy('kode')
                ->orderBy('kode', 'asc')->first();
                array_push($item_list, $push_item);
            } else {
                $push_item = Item::select(
                    DB::raw('kode'),
                    DB::raw('nama'),
                    DB::raw('diskon'),
                    DB::raw('aktif'),
                    DB::raw('konsinyasi'))
                ->where('konsinyasi', '!=', 2)
                // ->where('konsinyasi', 1)
                ->where('kode_barang', $temp_item->kode_barang)
                ->where('aktif', 1)
                ->groupBy('kode')
                ->orderBy('kode', 'asc')->latest()->first();
                array_push($item_list, $push_item);
            }
        }
        // return $item_list;
        $relasi_bundles = RelasiBundle::where('item_parent', $id)->get();
        // Item::where('konsinyasi', 0)->get();
        $stoks = Stok::where('item_kode', $id)->orderBy('created_at', 'dsc')->get();
        $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode')->having('kode', '=', $id)->get()->first();
        $relasi_bonuses = RelasiBonus::where('item_kode', $id)->orderBy('syarat', 'asc')->get();

        $satuan_bonuses = array();
        foreach ($relasi_bonuses as $key => $relasi_bonus) {
            $satuan_bonuse_relasi = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'asc')->get();
            foreach ($satuan_bonuse_relasi as $i => $sbr) {
                $konversi = $sbr->konversi;
                if ($relasi_bonus->syarat >= $konversi && $relasi_bonus->syarat % $konversi == 0) {
                    $satuan_bonuses[$key]['konversi'] = $konversi;
                    $satuan_bonuses[$key]['satuan'] = $sbr->satuan;
                }
            }
        }

        $limit_stok = array();
        if($item->limit_stok != NULL && $item->limit_stok != 0) {
            $limit_stok_relasi = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'asc')->get();
            foreach ($limit_stok_relasi as $i => $lsr) {
                $konversi = $lsr->konversi;
                if ($item->limit_stok >= $konversi && $item->limit_stok % $konversi == 0) {
                    $limit_stok['jumlah'] = $item->limit_stok / $konversi;
                    $limit_stok['satuan'] = Satuan::find($lsr->satuan_id)->nama;
                }
            }
        }

        $limit_grosir = array();
        if($item->limit_grosir != NULL && $item->limit_grosir != 0) {
            $limit_grosir_relasi = RelasiSatuan::where('item_kode', $id)->orderBy('konversi', 'asc')->get();
            foreach ($limit_grosir_relasi as $i => $lsk) {
                $konversi = $lsk->konversi;
                if ($item->limit_grosir >= $konversi && $item->limit_grosir % $konversi == 0) {
                    $limit_grosir['jumlah'] = $item->limit_grosir / $konversi;
                    $limit_grosir['satuan'] = Satuan::find($lsk->satuan_id)->nama;
                }
            }
        }

        $jumlah_total = $stok->stok;
        $bonus = $item->syarat_bonus;
        $stoklimit = $item->limit_stok;

        $stok_total = array();
        $stok_pcs = array();
        $bonus_arr = array();
        $stoklimit_arr = array();
        $harga_satuan = array();

        for ($i = 0; $i < count($stoks); $i++) {
            $jumlah_pcs = $stoks[$i]->jumlah;
            $harga_dasar = $stoks[$i]->harga;

            for ($j = 0; $j < count($relasi_satuans); $j++) {
                $hasil_pcs = explode('.', $jumlah_pcs / $relasi_satuans[$j]->konversi)[0];
                $sisa_pcs  = $jumlah_pcs % $relasi_satuans[$j]->konversi;

                $jumlah_pcs = $sisa_pcs;
                $harga = $harga_dasar * $relasi_satuans[$j]->konversi;

                $harga_satuan[$i][$j] = [
                    'harga' => $harga,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];

                $stok_pcs[$i][$j] = [
                    'jumlah' => $hasil_pcs,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];
            }
        }

        for ($i = 0; $i < count($relasi_satuans); $i++) {
            $hasil_total = explode('.', $jumlah_total / $relasi_satuans[$i]->konversi)[0];
            $sisa_total  = $jumlah_total % $relasi_satuans[$i]->konversi;

            $bonus_total = explode('.', $bonus / $relasi_satuans[$i]->konversi)[0];
            $bonus_sisa  = $bonus % $relasi_satuans[$i]->konversi;

            $stoklimit_total = explode('.', $stoklimit / $relasi_satuans[$i]->konversi)[0];
            $stoklimit_sisa  = $stoklimit % $relasi_satuans[$i]->konversi;

            $jumlah_total = $sisa_total;
            $bonus = $bonus_sisa;
            $stoklimit = $stoklimit_sisa;

            $stok_total[$i] = ['jumlah' => $hasil_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $bonus_arr[$i] = ['jumlah' => $bonus_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $stoklimit_arr[$i] = ['jumlah' => $stoklimit_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
        }

        // $jumlah_bundles= array();
        // foreach ($relasi_bundles as $i => $relasi_bundle) {
        //     $satuans = RelasiSatuan::where('item_kode', $relasi_bundle->item_child)->orderBy('konversi', 'asc')->get();
        //     foreach ($satuans as $j => $satuan) {
        //         $konversi = $satuan->konversi;
        //         if ($relasi_bundle->jumlah >= $konversi && $relasi_bundle->jumlah % $konversi == 0) {
        //             $jumlah_bundles[$i]['jumlah'] = $relasi_bundle->jumlah / $konversi;
        //             $jumlah_bundles[$i]['satuan'] = Satuan::find($satuan->satuan_id)->kode;
        //             $jumlah_bundles[$i]['konversi'] = $satuan->konversi;
        //         }
        //     }
        // }
        // return $jumlah_bundles;

        $hargas = Harga::where('item_kode', $id)->orderBy('grosir', 'dsc')->get();
        // return $item;

        $stok_childs = array();
        foreach ($relasi_bundles as $i => $relasi_bundle) {
            $item_kode = $relasi_bundle->item_child;
            $items = Item::where('kode', $item_kode)->get();
            
            $item_ids = array();
            $index_stok_childs = 0;
            foreach ($items as $j => $item) {
                array_push($item_ids, $item->id);
            }

            $relasi_transaksi_pembelians = RelasiTransaksiPembelian::whereIn('item_id', $item_ids)->orderBy('created_at', 'dsc')->limit(3)->get();
            foreach ($relasi_transaksi_pembelians as $k => $relasi_transaksi_pembelian) {
                $stok_childs[$index_stok_childs][$i] = [
                    'kode' => $items[0]->kode,
                    'nama' => $items[0]->nama,
                    'harga' => $relasi_transaksi_pembelian->harga * 1.1 * $relasi_bundle->jumlah,
                    'jumlah' => $relasi_bundle->jumlah
                ];
                $index_stok_childs++;
            }

            if ($index_stok_childs == 0) {
                for ($k = 0; $k < 3; $k++) {
                    $stok_childs[$k][$i] = [
                        'kode' => $items[0]->kode,
                        'nama' => $items[0]->nama,
                        'harga' => 0,
                        'jumlah' => $relasi_bundle->jumlah
                    ];
                }
            } else if ($index_stok_childs < 3) {
                for ($k = $index_stok_childs; $k < 3; $k++) {
                    $stok_childs[$k][$i] = [
                        'kode' => $items[0]->kode,
                        'nama' => $items[0]->nama,
                        'harga' => $stok_childs[$k - 1][$i]['harga'],
                        'jumlah' => $stok_childs[$k - 1][$i]['jumlah']
                    ];
                }
            }
        }
        // return $item_detail;
        if ($is_aktif) {
            // return '1';
            return view('item.show_bundle', compact('item', 'items', 'item_kodes', 'stok', 'satuans', 'relasi_satuans', 'stoks', 'stok_total', 'bonus_arr', 'stoklimit_arr', 'stok_pcs', 'harga_satuan', 'bonuses', 'relasi_bonuses', 'satuan_bonuses', 'limit_stok', 'limit_grosir', 'hargas', 'item_list', 'relasi_bundles', 'jumlah_bundles', 'stok_childs', 'item_kode_super', 'kode_barang_super', 'item_detail'));
        } else {
            // return '0';
            return view('item.show_bundle_off', compact('item', 'items', 'item_kodes', 'stok', 'satuans', 'relasi_satuans', 'stoks', 'stok_total', 'bonus_arr', 'stoklimit_arr', 'stok_pcs', 'harga_satuan', 'bonuses', 'relasi_bonuses', 'satuan_bonuses', 'limit_stok', 'limit_grosir', 'hargas', 'item_list', 'relasi_bundles', 'jumlah_bundles', 'stok_childs', 'item_kode_super', 'kode_barang_super', 'item_detail'));
        }
    }

    public function showHistory($kode_barang)
    {
        $satuans = Satuan::all();
        $bonuses = Bonus::all();
        $item = Item::where('kode_barang', $kode_barang)->first();
        $kode = $item->kode;
        $relasi_satuans = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'desc')->get();
        $stoks = Stok::where('item_kode', $kode)->orderBy('created_at', 'dsc')->get();
        $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode_barang')->having('kode_barang', '=', $kode_barang)->get()->first();
        $relasi_bonuses = RelasiBonus::where('item_kode', $kode)->orderBy('syarat', 'asc')->get();

        $item_kodes = Item::where('kode_barang', $kode_barang)->where('aktif', 0)->distinct()->get(['kode']);
        $items = array();
        foreach ($item_kodes as $i => $item_kode) {
            $temp_items = Item::where('kode', $item_kode)->get();
            foreach ($temp_items as $j => $temp_item) {
                array_push($items, $temp_item);
            }
        }

        $suplier_ids = Item::where('kode_barang', $kode_barang)->whereNotIn('suplier_id', [Suplier::notSuplier])->distinct()->get(['suplier_id']);
        $supliers = array();
        foreach ($suplier_ids as $i => $suplier_id) {
            // if ($suplier_id != '999') {
            array_push($supliers, Suplier::find($suplier_id->suplier_id));
            // }
        }

        $satuan_bonuses = array();
        foreach ($relasi_bonuses as $key => $relasi_bonus) {
            $satuan_bonuse_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            foreach ($satuan_bonuse_relasi as $i => $sbr) {
                $konversi = $sbr->konversi;
                if ($relasi_bonus->syarat >= $konversi && $relasi_bonus->syarat % $konversi == 0) {
                    $satuan_bonuses[$key]['konversi'] = $konversi;
                    $satuan_bonuses[$key]['satuan'] = $sbr->satuan;
                }
            }
        }

        $limit_stok = array();
        if($item->limit_stok != NULL && $item->limit_stok != 0) {
            $limit_stok_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            foreach ($limit_stok_relasi as $i => $lsr) {
                $konversi = $lsr->konversi;
                if ($item->limit_stok >= $konversi && $item->limit_stok % $konversi == 0) {
                    $limit_stok['jumlah'] = $item->limit_stok / $konversi;
                    $limit_stok['satuan'] = Satuan::find($lsr->satuan_id)->nama;
                }
            }
        }

        $limit_grosir = array();
        if($item->limit_grosir != NULL && $item->limit_grosir != 0) {
            $limit_grosir_relasi = RelasiSatuan::where('item_kode', $kode)->orderBy('konversi', 'asc')->get();
            foreach ($limit_grosir_relasi as $i => $lsk) {
                $konversi = $lsk->konversi;
                if ($item->limit_grosir >= $konversi && $item->limit_grosir % $konversi == 0) {
                    $limit_grosir['jumlah'] = $item->limit_grosir / $konversi;
                    $limit_grosir['satuan'] = Satuan::find($lsk->satuan_id)->nama;
                }
            }
        }

        $jumlah_total = $stok->stok;
        $bonus = $item->syarat_bonus;
        $stoklimit = $item->limit_stok;

        $stok_total = array();
        $stok_pcs = array();
        $bonus_arr = array();
        $stoklimit_arr = array();
        $harga_satuan = array();

        for ($i = 0; $i < count($stoks); $i++) {
            $jumlah_pcs = $stoks[$i]->jumlah;
            $harga_dasar = $stoks[$i]->harga;

            for ($j = 0; $j < count($relasi_satuans); $j++) {
                $hasil_pcs = explode('.', $jumlah_pcs / $relasi_satuans[$j]->konversi)[0];
                $sisa_pcs  = $jumlah_pcs % $relasi_satuans[$j]->konversi;

                $jumlah_pcs = $sisa_pcs;
                $harga = $harga_dasar * $relasi_satuans[$j]->konversi;

                $harga_satuan[$i][$j] = [
                    'harga' => $harga,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];

                $stok_pcs[$i][$j] = [
                    'jumlah' => $hasil_pcs,
                    'satuan' => $relasi_satuans[$j]->satuan->kode
                ];
            }
        }

        for ($i = 0; $i < count($relasi_satuans); $i++) {
            $hasil_total = explode('.', $jumlah_total / $relasi_satuans[$i]->konversi)[0];
            $sisa_total  = $jumlah_total % $relasi_satuans[$i]->konversi;

            $bonus_total = explode('.', $bonus / $relasi_satuans[$i]->konversi)[0];
            $bonus_sisa  = $bonus % $relasi_satuans[$i]->konversi;

            $stoklimit_total = explode('.', $stoklimit / $relasi_satuans[$i]->konversi)[0];
            $stoklimit_sisa  = $stoklimit % $relasi_satuans[$i]->konversi;

            $jumlah_total = $sisa_total;
            $bonus = $bonus_sisa;
            $stoklimit = $stoklimit_sisa;

            $stok_total[$i] = ['jumlah' => $hasil_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $bonus_arr[$i] = ['jumlah' => $bonus_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            $stoklimit_arr[$i] = ['jumlah' => $stoklimit_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
        }

        $hargas = Harga::where('item_kode', $kode)->orderBy('grosir', 'dsc')->get();
        // return $hargas;

        return view('item.show_history', compact('item', 'items', 'item_kodes', 'supliers', 'stok', 'satuans', 'relasi_satuans', 'stoks', 'stok_total', 'bonus_arr', 'stoklimit_arr', 'stok_pcs', 'harga_satuan', 'bonuses', 'relasi_bonuses', 'satuan_bonuses', 'limit_stok', 'limit_grosir', 'hargas'));
    }

    public function showJson($id)
    {
        $item = Item::find($id);
        return $item->toJson();
    }

    public function edit(Request $request, $kode_barang)
    {
        $items = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->get();
        $kodes = [];
        foreach ($items as $i => $item) {
            if (!in_array($item->kode, $kodes)) {
                array_push($kodes, $item->kode);
            }
        }

        $temp_kode = '';
        foreach ($kodes as $i => $kode) {
            $temp_kode .= $kode;
            $temp_kode .= ' ';
        }

        $item = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->first();
        $item->kode = $temp_kode;
        $supliers = Suplier::whereNotIn('id', [Suplier::notSuplier])->get();
        // $jenis_items = JenisItem::all();
        // if ($item->konsinyasi != 1) {
        //     $jenis_items = JenisItem::all();
        // } else{
        //     $jenis_items = JenisItem::whereNotIn('id', [1])->get();
        // }
        $jenis_items = JenisItem::whereNotIn('id', [1])->get();

        $request->session()->forget(['sukses', 'error']);
        // return $item;

        $satuans = RelasiSatuan::where('item_kode', $kode_barang)->orderBy('konversi', 'desc')->get();

        return view('item.edit', compact('item', 'jenis_items', 'bonuses', 'supliers', 'satuans'));
    }

    public function editBundle(Request $request, $kode)
    {
        $item = Item::where('kode', $kode)->first();
        $kode_barang = $item->kode;
        $supliers = Suplier::whereNotIn('id', [Suplier::notSuplier])->get();
        $jenis_items = JenisItem::whereNotIn('id', [1])->get();
        // $jenis_items = JenisItem::all();
        $request->session()->forget(['sukses', 'error']);
        // return $item;

        $satuans = RelasiSatuan::where('item_kode', $kode_barang)->orderBy('konversi', 'desc')->get();

        return view('item.edit', compact('item', 'jenis_items', 'bonuses', 'supliers', 'satuans'));
    }

    public function ubah(Request $request, $kode_barang)
    {
        // return $request->all();
        $items = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->get();
        $jenis_item_id = $items[0]->jenis_item_id;
        $temp_rentang = $items[0]->rentang;
        $temp_profit = $items[0]->profit;
        $temp_profit_ecer = $items[0]->profit_ecer;
        $profit_eceran = $request->profit_eceran;
        $profit_eceran = floatval(str_replace(',', '.', $profit_eceran));

        $profit = $request->profit;
        $profit = floatval(str_replace(',', '.', $profit));
        $is_reset_harga = false;
        $is_not_konsinyasi = true;
        $is_bundle = false;
        if ($items[0]->konsinyasi == 1) {
            $is_not_konsinyasi = false;
        } elseif($items[0]->konsinyasi == 2) {
            $is_not_konsinyasi = false;
            $is_bundle = true;
            $item_kode = $items[0]->kode;
        }
        $kode_barang_go = $kode_barang;

        if($temp_rentang != $request->rentang || 
            $temp_profit != $profit ||
            $temp_profit_ecer != $profit_eceran) {
            $is_reset_harga = true;
        }

        foreach ($items as $a => $item) {
            $item->nama = $request->nama;
            $item->nama_pendek = $request->nama_pendek;
            $item->jenis_item_id = $request->jenis_item_id;
            // $item->konsinyasi = $request->konsinyasi;
            $item->retur = $request->retur;
            $item->rentang = $request->rentang;
            $item->plus_grosir = $request->plus_grosir;
            $item->bonus_jual = $request->bonus_jual;
            $item->user_id = Auth::user()->id;

            if($jenis_item_id != $request->jenis_item_id){
                $kodes = explode('-', $item->kode_barang);
                $kode_new = $kodes[0].'-'.JenisItem::find($item->jenis_item_id)->kode.'-'.$kodes[2];

                $kode_barang_go = $kode_new;
                $item->kode_barang = $kode_new;
            }

            if($request->konsinyasi == 3){
                $item->bonus = 1;
                $item->konsinyasi = 3;
                $cek = RelasiBonus::where('item_kode', $item->kode)->delete();
            }elseif($request->konsinyasi ==0){
                $item->bonus = 0;
                $item->konsinyasi = 0;
            }

            $item->profit = $profit;
            $item->profit_eceran = $profit_eceran;

            $item->update();
        }

        if ($is_reset_harga) self::resetHargas($kode_barang_go);
            
        if ($is_bundle) {
            return redirect('/item-bundle/show/'.$item_kode)->with('sukses', 'ubah_item');
        } else {
            return redirect('/item/show/'.$kode_barang_go)->with('sukses', 'ubah_item');
        }
    }

    public function tambah_suplier($kode_barang)
    {
        $items = Item::where('kode_barang', $kode_barang)->groupBy('suplier_id')->get();
        $item = $items[0];
        $suplier_id = array();
        array_push($suplier_id, '999');

        foreach ($items as $a => $l_item) {
            array_push($suplier_id, $l_item->suplier->id);
        }

        $supliers = Suplier::whereNotIn('id', $suplier_id)->where('aktif', 1)->get();

        return view('item.tambah_suplier', compact('items', 'item', 'supliers'));
    }

    public function store_suplier(Request $request, $kode_barang)
    {
        $item = Item::where('kode_barang', $kode_barang)->latest()->first();
        $items = Item::where('kode_barang', $kode_barang)->where('suplier_id', $item->suplier_id)->get();
        $new_item_count = 0;
        foreach ($items as $i => $item) {
            $new_item = new Item();
            $new_item->kode = $item->kode;
            $new_item->kode_barang = $item->kode_barang;
            $new_item->nama = $item->nama;
            $new_item->nama_pendek = $item->nama_pendek;
            $new_item->suplier_id = $request->suplier;
            $new_item->jenis_item_id = $item->jenis_item_id;
            $new_item->stoktotal = $item->stoktotal;
            $new_item->konsinyasi = $item->konsinyasi;
            $new_item->aktif = $item->aktif;
            $new_item->retur = $item->retur;
            $new_item->limit_stok = $item->limit_stok;
            $new_item->limit_grosir = $item->limit_grosir;
            $new_item->diskon = $item->diskon;
            $new_item->profit = $item->profit;
            $new_item->profit_eceran = $item->profit_eceran;
            $new_item->rentang = $item->rentang;
            $new_item->valid_konversi = $item->valid_konversi;
            $new_item->user_id = $item->user_id;
            $new_item->valid_id = $item->valid_id;
            $new_item->save();
            $new_item_count++;
        }

        if ($new_item_count == count($items)) {
            // return redirect('/item')->with('sukses', 'tambah_suplier');
            return redirect('/item/show/'.$kode_barang)->with('sukses', 'tambah_suplier');
        } else {
            return redirect('/item/show/'.$kode_barang)->with('gagal', 'tambah_suplier');
            // return redirect('/item')->with('gagal', 'tambah_suplier');
        }
    }

    public function tambah_barcode($kode_barang)
    {
        $items = Item::where('kode_barang', $kode_barang)->where('aktif', 1)->groupBy('kode')->get();
        $item = $items[0];
        return view('item.tambah_barcode', compact('items', 'item'));
    }

    public function store_barcode(Request $request, $kode_barang)
    {
        $available_items = Item::where('kode', $request->kode)->get();
        if (count($available_items) > 0) {
            return redirect('/item/tambah_barcode/'.$kode_barang)->with('gagal', 'tambah_kode');
        }

        $items = Item::where('kode_barang', $kode_barang)->get();
        $item_kode = $items[0]->kode;
        $new_item_count = 0;
        foreach ($items as $i => $item) {
            $new_item = new Item();
            $new_item->kode = $request->kode;
            $new_item->kode_barang = $item->kode_barang;
            $new_item->nama = $item->nama;
            $new_item->nama_pendek = $item->nama_pendek;
            $new_item->suplier_id = $item->suplier_id;
            $new_item->jenis_item_id = $item->jenis_item_id;
            $new_item->stoktotal = $item->stoktotal;
            $new_item->konsinyasi = $item->konsinyasi;
            $new_item->aktif = $item->aktif;
            $new_item->retur = $item->retur;
            $new_item->bonus = $item->bonus;
            $new_item->valid_konversi = $item->valid_konversi;
            $new_item->limit_stok = $item->limit_stok;
            $new_item->limit_grosir = $item->limit_grosir;
            $new_item->diskon = $item->diskon;
            $new_item->profit = $item->profit;
            $new_item->profit_eceran = $item->profit_eceran;
            $new_item->rentang = $item->rentang;
            $new_item->valid_konversi = $item->valid_konversi;
            $new_item->user_id = $item->user_id;
            $new_item->valid_id = $item->valid_id;
            $new_item->save();
            $new_item_count++;
        }

        if ($new_item_count == count($items)) {
            // Tambah relasi_satuans
            $relasi_satuans = RelasiSatuan::where('item_kode', $item_kode)->get();
            foreach ($relasi_satuans as $i => $relasi_satuan) {
                RelasiSatuan::create([
                    'item_kode' => $new_item->kode,
                    'satuan_id' => $relasi_satuan->satuan_id,
                    'konversi' => $relasi_satuan->konversi
                ]);
            }

            // Tambah hargas
            $hargas = Harga::where('item_kode', $item_kode)->get();
            foreach ($hargas as $i => $harga) {
                Harga::create([
                    'item_kode' => $new_item->kode,
                    'satuan_id' => $harga->satuan_id,
                    'jumlah' => $harga->jumlah,
                    'eceran' => $harga->eceran,
                    'grosir' => $harga->grosir
                ]);
            }

            // Tambah relasi_bonuses
            $relasi_bonuses = RelasiBonus::where('item_kode', $item_kode)->get();
            foreach ($relasi_bonuses as $i => $relasi_bonus) {
                RelasiBonus::create([
                    'item_kode' => $new_item->kode,
                    'bonus_id' => $relasi_bonus->bonus_id,
                    'syarat' => $relasi_bonus->syarat
                ]);
            }

            return redirect('/item/show/'.$kode_barang)->with('sukses', 'tambah_kode');
        } else {
            return redirect('/item/tambah_barcode/'.$kode_barang)->with('gagal', 'tambah_kode');
        }
    }

    public function historyHargaBeli($id)
    {
        $items = Item::where('kode_barang', $id)->get();
        $item = Item::where('kode_barang', $id)->first();
        // return $items;
        $nama_item = $items[0]->nama;
        $item_ids = [];
        foreach ($items as $i => $item) {
            array_push($item_ids, $item->id);
        }
        // $suplier_id = array();
        // foreach ($items as $i => $item) {
        //     array_push($suplier_id, $item->suplier_id);
        // }

        // $supliers = Suplier::whereIn('id', $suplier_id)->get();
        // $relasis = array();
        // foreach ($supliers as $i => $suplier) {
        //     $item = Item::where('suplier_id', $suplier->id)->where('kode_barang', $id)->first();
        //     $suplier->item = $item;
        //     $suplier->relasis = RelasiTransaksiPembelian::where('item_id', $item->id)->latest()->get();
        // }
        // return $items[0]->kode;
        $satuans = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
        $harga_beli = RelasiTransaksiPembelian::whereIn('item_id', $item_ids)->get();

        // return [$harga_beli, $satuans];
        // return view('item.history_harga_beli', compact('supliers', 'satuans'));

        return view('item.history_harga_beli', compact('nama_item', 'harga_beli', 'satuans', 'item'));
    }

    public function historyHargaJual($id)
    {
        $data = array();

        $item = Item::where('kode_barang', $id)->first();
        $relasis = RelasiTransaksiPenjualan::where('item_kode', $item->kode)->latest()->get();

        $prices = array();
        for ($i = 0; $i < count($relasis); $i++) {
            if ($relasis[$i]->nego > 0) {
                $harga = ceil($relasis[$i]->nego/$relasis[$i]->jumlah*100)/100;
            } else {
                $harga = $relasis[$i]->harga;
            }

            if ($relasis[$i]->subtotal < $relasis[$i]->nego) $harga = ceil($relasis[$i]->subtotal/$relasis[$i]->jumlah*100)/100;

            $transaksi = TransaksiPenjualan::find($relasis[$i]->transaksi_penjualan_id);

            $prices[$i]['harga'] = $harga;
            $prices[$i]['waktu'] = $relasis[$i]->created_at->format('d-m-Y h:m:s');
            $prices[$i]['kode_transaksi'] = $transaksi->kode_transaksi;
            $prices[$i]['jumlah'] = $relasis[$i]->jumlah;
            $prices[$i]['id_transaksi'] = $relasis[$i]->transaksi_penjualan_id;
            if ($transaksi->pelanggan_id != NULL) {
                $pelanggan = $transaksi->pelanggan->nama.' | '.$transaksi->pelanggan->toko;
                $prices[$i]['pelanggan'] = $pelanggan;
            } else {
                $prices[$i]['pelanggan'] = '-';
            }
        }

        $satuans = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi', 'dsc')->get();
        // return $prices[0]['waktu'];
        return view('item.history_harga_jual', compact('item', 'relasis', 'data', 'prices', 'satuans'));
    }

    public function delete($id)
    {
        $items = Item::where('kode_barang', $id)->get();
        if (count($items) > 0) {
            $stoks = Stok::where('item_kode', $items[0]->kode)->where('jumlah', '>', 0)->orderBy('created_at', 'dsc')->get();
            if (count($stoks) > 0) {
                return redirect('/item')->with('gagal', 'hapus');
            }
        }

        $status = 0;
        foreach ($items as $item) {
            try {
                $item->aktif = 0;
                $item->update();
                $status = 1;
            } catch (\Illuminate\Database\QueryException $e) {
                return redirect('/item')->with('gagal', 'hapus');
            }
        }

        if ($status == 1) {
            return redirect('/item')->with('sukses', 'hapus');
        } else {
            return redirect('/item')->with('gagal', 'hapus');
        }
    }

    public function deleteByKode($item_kode)
    {
        $items = Item::where('kode', $item_kode)->get();
        $kode_barang = $items[0]->kode_barang;
        
        $stoks = Stok::where('item_kode', $item_kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();

        if (count($stoks) > 0) {
            return redirect('/item/show/'.$kode_barang)->with('gagal', 'hapus');
        }

        $status = 0;
        foreach ($items as $item) {
            $stok = Stok::where('item_kode', $item_kode)->where('jumlah', '>', 0)->first();
            if($stok == NULL){
                try {
                    $item->aktif = 0;
                    $item->update();
                    $status = 1;
                } catch (\Illuminate\Database\QueryException $e) {
                    return redirect('/item/show/'.$kode_barang)->with('gagal', 'hapus');
                }
            }
        }

        if ($status == 1) {
            return redirect('/item/show/'.$kode_barang)->with('sukses', 'hapus');
        } else {
            return redirect('/item/show/'.$kode_barang)->with('gagal', 'hapus');
        }
    }

    public function store_bonus(Request $request)
    {
        // return $request->all();
        $item = Item::where('kode', $request->item_kode)->first();
        $kode_barang = $item->kode_barang;
        $items = Item::where('kode_barang', $kode_barang)->distinct()->get(['kode']);
        foreach ($items as $i => $item) {
            $relasi_satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $request->satuan_id)->first();
            $syarat = $request->syarat * $relasi_satuan->konversi;

            $new_relasi_bonus = new RelasiBonus();

            $new_relasi_bonus->item_kode = $item->kode;
            $new_relasi_bonus->bonus_id = $request->bonus;
            $new_relasi_bonus->syarat = $syarat;
            // $new_relasi_bonus->satuan_id = $request->satuan_id;
            $new_relasi_bonus->save();
        }

        if ($request->bundle != NULL) {
            return redirect('/item-bundle/show/'.$request->item_kode)->with('sukses', 'tambah_bonus');
        } else {
            return redirect('/item/show/'.$kode_barang)->with('sukses', 'tambah_bonus');
        }
    }

    public function update_bonus(Request $request, $id)
    {
        // return $request->all();
        $item = Item::where('kode', $request->item_kode)->first();
        $kode_barang = $item->kode_barang;
        $items = Item::where('kode_barang', $kode_barang)->distinct()->get(['kode']);
        $relasi_bonus = RelasiBonus::find($id);
        $syarat_lama = $relasi_bonus->syarat;
        foreach ($items as $i => $item) {
            $relasi_satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $request->satuan_id)->first();
            $syarat = $request->syarat * $relasi_satuan->konversi;

            $relasi_bonus = RelasiBonus::where('item_kode', $item->kode)->where('syarat', $syarat_lama)->first();

            $relasi_bonus->item_kode = $item->kode;
            $relasi_bonus->bonus_id = $request->bonus;
            $relasi_bonus->syarat = $syarat;
            // $relasi_bonus->satuan_id = $request->satuan_id;
            $relasi_bonus->update();
        }

        // return redirect('/item/show/'.$request->item_kode)->with('sukses', 'update_bonus');
        if ($request->bundle != NULL) {
            return redirect('/item-bundle/show/'.$request->item_kode)->with('sukses', 'update_bonus');
        } else {
            return redirect('/item/show/'.$kode_barang)->with('sukses', 'update_bonus');
        }
    }

    public function delete_bonus($id, $kode)
    {
        $relasi_bonus = RelasiBonus::find($id);
        $kode_barang = $relasi_bonus->item->kode_barang;
        $items = Item::where('kode_barang', $kode_barang)->distinct()->get(['kode']);
        foreach ($items as $i => $item) {
            RelasiBonus::where('item_kode', $item->kode)->where('bonus_id', $relasi_bonus->bonus_id)->delete();
        }

        if ($items[0]->konsinyasi == 2) {
            return redirect('/item-bundle/show/'.$request->item_kode)->with('sukses', 'delete_bonus');
        } else {
            return redirect('/item/show/'.$kode_barang)->with('sukses', 'delete_bonus');
        }
    }

    public function update_diskon(Request $request)
    {
        $bundle = 0;
        $items = Item::where('kode_barang', $request->kode_barang)->get();
        foreach ($items as $item) {
            $persen = $request->persen;
            $persen = floatval(str_replace(',', '.', $persen));
            $item->diskon = $persen;
            $item->update();
            if ($item->konsinyasi == 2) {
                $bundle = 1;
            }
        }

        // return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'update_diskon');
        if ($bundle == 1) {
            return redirect('/item-bundle/show/'.$items[0]->kode)->with('sukses', 'update_diskon');
        } else {
            return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'update_diskon');
        }
    }

    public function update_limit_stok(Request $request)
    {
        // return $request->all();
        $bundle = 0;
        $items = Item::where('kode_barang', $request->kode_barang)->get();
        $item_kode = $items[0]->kode;
        $konversi = RelasiSatuan::where('item_kode', $item_kode)->where('satuan_id', $request->satuan_id_limit)->first()->konversi;
        $limit = $request->jumlah * $konversi;
        // return $limit;
        foreach ($items as $item) {
            $item->limit_stok = $limit;
            // $item->satuan_limit = $request->satuan_id_limit;
            $item->update();
            if ($item->konsinyasi == 2) {
                $bundle = 1;
            }
        }

        // return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'update_limit_stok');

        if ($bundle == 1) {
            return redirect('/item-bundle/show/'.$items[0]->kode)->with('sukses', 'update_limit_stok');
        } else {
            return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'update_limit_stok');
        }
    }

    public function update_limit_grosir(Request $request)
    {
        // return $request->all();
        $bundle = 0;
        $items = Item::where('kode_barang', $request->kode_barang)->get();
        $item_kode = $items[0]->kode;
        $plus_grosir = $items[0]->plus_grosir;
        $konversi = RelasiSatuan::where('item_kode', $item_kode)->where('satuan_id', $request->satuan_id_limit)->first()->konversi;
        $limit = $request->jumlah * $konversi;

        foreach ($items as $item) {
            $item->limit_grosir = $limit;
            $item->update();
            if ($item->konsinyasi == 2) {
                $bundle = 1;
            }
        }

        //ganti harga setalah set limit grosir
        $is_grosir = 0;
        $hargas = Harga::where('item_kode', $item_kode)->orderBy('eceran', 'asc')->get();

        foreach ($hargas as $key => $harga) {
            if($request->satuan_id_limit == $harga->satuan_id){
                $is_grosir = 1;
            }

            if($is_grosir == 1){
                $harga->eceran = $harga->grosir + $plus_grosir;
                $harga->update();
            }else{
                $harga->eceran = $harga->eceran;
                $harga->update();
            }
        }

        if ($bundle == 1) {
            return redirect('/item-bundle/show/'.$items[0]->kode)->with('sukses', 'update_limit_grosir');
        } else {
            return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'update_limit_grosir');
        }
    }

    public function BundleStore(Request $request)
    {
        // return $request->all();
        $cek = RelasiBundle::where('item_parent', $request->item_kode)->where('item_child', $request->item_list)->first();

        if ($cek == NULL) {
            $relasi_bundle = new RelasiBundle();
            $relasi_bundle->item_parent = $request->item_kode;
            $relasi_bundle->item_child = $request->item_list;
            $relasi_bundle->jumlah = $request->jumlah * $request->konversi;
            $relasi_bundle->save();

            self::ubahHargaBundle($request->item_kode);
            return redirect('/item-bundle/show/'.$request->item_kode)->with('sukses', 'tambah_bundle');
        } else {
            return redirect('/item-bundle/show/'.$request->item_kode)->with('gagal', 'tambah_bundle');
        }
    }

    public function ubahBundle(Request $request, $id)
    {
        // return $request->all();
        $bundle = RelasiBundle::find($id);

        $cek = RelasiBundle::where('item_parent', $request->item_kode)->where('item_child', $request->item_list)->first();

        // if($cek == NULL){
            // $bundle = new RelasiBundle();
            // $bundle->item_parent = $request->item_kode;
            // $bundle->item_child = $request->item_list;
            $bundle->jumlah = $request->jumlah * $request->konversi;
            $bundle->update();

            self::ubahHargaBundle($request->item_kode);
            return redirect('/item-bundle/show/'.$request->item_kode)->with('sukses', 'ubah_bundle');
        // }else{
        //     return redirect('/item-bundle/show/'.$request->item_kode)->with('gagal', 'ubah_bundle');
        // }
    }

    public function delete_bundle($id, $kode)
    {
        // return $id;
        $relasi_bundle = RelasiBundle::find($id);
        $item_parent = $relasi_bundle->item_parent;
        $relasi_transaksi_penjualans = RelasiTransaksiPenjualan::where('item_kode', $item_parent)->get();
        if (count($relasi_transaksi_penjualans) > 0) {
            return redirect('/item-bundle/show/'.$item_parent)->with('gagal', 'delete_bundle');
        }

        $bundle = $relasi_bundle->delete();
        self::ubahHargaBundle($request->item_kode);
        return redirect('/item-bundle/show/'.$item_parent)->with('sukses', 'delete_bundle');
    }

    /*public function item_data_json(){
        $items = Item::select(
                DB::raw('kode'),
                DB::raw('kode_barang'),
                DB::raw('nama'),
                DB::raw('diskon'),
                DB::raw('jenis_item_id'),
                DB::raw('aktif'),
                DB::raw('konsinyasi'),
                DB::raw('sum(stoktotal) as stok'))
            ->whereIn('konsinyasi', [0, 3])
            ->where('aktif', 1)
            ->groupBy('kode_barang')
            ->orderBy('kode_barang', 'asc')->get();

        return Datatables::of($items)->make(true);
        // return Datatables::of(Item::query())->make(true);
    }*/

    public function fixKonversiSatuan($item_kode)
    {
        $items = Item::where('kode', $item_kode)->get();
        foreach ($items as $i => $item) {
            $item->valid_konversi = 1;
            $item->valid_id = Auth::user()->id;
            $item->update();
        }

        return redirect('/item/show/'.$items[0]->kode_barang)->with('sukses', 'fix_konversi_satuan');
    }

    public function validKonversi(Request $request, $item_kode)
    {
        // return 'aw';
        $items = Item::where('kode_barang', $item_kode)->get();
        foreach ($items as $i => $item) {
            $item->valid_konversi = 0;
            $item->valid_id = Auth::user()->id;
            $item->update();
        }

        return response()->json([
            'error' => false
        ]);
    }

    public function re_aktif($id)
    {
        $items = Item::where('kode_barang', $id)->get();
        $status = 0;
        foreach ($items as $item) {
            try {
                $item->aktif = 1;
                $item->user_id = Auth::user()->id;
                $item->update();
                $status = 1;
            } catch (\Illuminate\Database\QueryException $e) {
                return redirect('/item')->with('gagal', 're_aktif');
            }
        }

        if ($status == 1) {
            return redirect('/item')->with('sukses', 're_aktif');
        } else {
            return redirect('/item')->with('gagal', 're_aktif');
        }
    }

    public function resetHarga($id) {
        self::resetHargas($id);
        return redirect('/item/show/'.$id)->with('sukses', 'reset_harga');
    }

    public function resetHargaBundle($id) {
        self::ubahHargaBundle($id);
        return redirect('/item-bundle/show/'.$id)->with('sukses', 'reset_harga');
    }

    public static function resetHargas($id) {
        $items = Item::where('kode_barang', $id)->get();
        $profit_ecer = $items[0]->profit_eceran;
        $profit_grosir= $items[0]->profit;

        $is_bundle = false;
        if ($items[0]->konsinyasi == 2) {
            $is_bundle = true;
        }

        $limit_grosir = $items[0]->limit_grosir;
        $satuan_eceran_grosir = 0;
        $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();

        if ($limit_grosir != 0) {
            foreach ($relasi_satuan_item as $i => $relasi) {
                if($limit_grosir % $relasi->konversi == 0){
                    $satuan_eceran_grosir = $relasi->satuan_id;
                    break;
                }
            }
        }

        $plus_grosir = $items[0]->plus_grosir;

        $item_ids = [];

        foreach ($items as $i => $item) {
            array_push($item_ids, $item->id);
        }

        $stok = Stok::whereIn('item_id', $item_ids)->orderBy('created_at', 'dsc')->first();
        foreach ($items as $i => $item) {
            $satuans = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi', 'asc')->get();

            $is_grosir = false;
            foreach ($satuans as $i => $satuan) {
                $harga = Harga::where('satuan_id', $satuan->satuan_id)->where('item_kode', $item->kode)->first();
                if ($stok != null) {
                    $harga_dasar = $stok->harga * $satuan->konversi * 1.1;
                    $harga->eceran = ceil(($harga_dasar * (1 + $profit_ecer / 100) / 100)) * 100;
                    $harga->grosir = ceil(($harga_dasar * (1 + $profit_grosir / 100) / 100)) * 100;
                    
                    if ($satuan_eceran_grosir == $satuan->satuan_id) {
                        $is_grosir = true;
                    }

                    if ($is_grosir) {
                        $harga->eceran = $harga->grosir + $plus_grosir;
                    }
                } else {
                    $harga->eceran = 0;
                    $harga->grosir = 0;
                }
                    $harga->update();
            }
        }
    }

    public function hapusPemasok(Request $request) {
        $item_supliers = Item::where('kode_barang', $request->kode_barang)->where('suplier_id', $request->suplier_id)->get();
        foreach ($item_supliers as $i => $item) {
            $item->suplier_id = Suplier::notSuplier;
            $item->update();
        }

        return redirect('/item/show/'.$request->kode_barang)->with('sukses', 'hapus_suplier');
    }

    public static function ubahHargaBundle($id) {
        $item = Item::where('kode', $id)->first();
        $harga = Harga::where('item_kode', $id)->orderBy('grosir', 'dsc')->first();
        $relasi_bundles = RelasiBundle::where('item_parent', $id)->get();
        $harga_baru = 0;

        foreach ($relasi_bundles as $i => $relasi_bundle) {
            $item_kode = $relasi_bundle->item_child;
            $items = Item::where('kode', $item_kode)->get();
            
            $item_ids = array();
            $index_stok_childs = 0;
            foreach ($items as $j => $item) {
                array_push($item_ids, $item->id);
            }

            $relasi_transaksi_pembelian = RelasiTransaksiPembelian::whereIn('item_id', $item_ids)->orderBy('created_at', 'dsc')->first();
            $harga_baru += ($relasi_transaksi_pembelian->harga * $relasi_bundle->jumlah * 1.1);
        }

        $harga_baru = ceil($harga_baru * (1 + $item->profit / 100) / 100) * 100;

        $harga->eceran = $harga_baru + $item->plus_grosir;
        $harga->grosir = $harga_baru;
        $harga->update();
    }
}
