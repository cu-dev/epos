<?php

namespace App\Http\Controllers;

use Auth;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Item;
use App\Laci;
use App\Stok;
use App\Util;
use App\Harga;
use App\Jurnal;
use App\Notice;
use App\Satuan;
use App\Seller;
use App\Suplier;
use App\LogLaci;
use App\RelasiSatuan;
use App\TransaksiPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class KonsinyasiMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $temp_konsinyasi_masuks = TransaksiPembelian::where('kode_transaksi', 'like', '%TPMK%')
            ->orderBy('created_at', 'desc')->get();
        $konsinyasi_masuks = array();

        foreach ($temp_konsinyasi_masuks as $i => $data) {
            if(explode('/', $data->kode_transaksi)[1] != 'TPMKP'){
                array_push($konsinyasi_masuks, $data);
            }
        }
        return view('konsinyasi_masuk.index', compact('konsinyasi_masuks'));
    } */

    public function index()
    {
        return view('konsinyasi_masuk.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_pembelians.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $konsinyasi_masuks = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id',
                'transaksi_pembelians.created_at',
                'transaksi_pembelians.kode_transaksi',
                'transaksi_pembelians.harga_total',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPMK/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPMK/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($konsinyasi_masuks as $i => $konsinyasi_masuk) {

            $konsinyasi_masuk->harga_total = Util::duit0($konsinyasi_masuk->harga_total);

            $buttons['detail'] = ['url' => url('konsinyasi-masuk/'.$konsinyasi_masuk->id)];

            // return $buttons;
            $konsinyasi_masuk->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $konsinyasi_masuks,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function lastJson()
    {
        $konsinyasi_masuk = TransaksiPembelian::where('kode_transaksi', 'like', '%TPMK%')->get()->last();
        return response()->json([
            'konsinyasi_masuk' => $konsinyasi_masuk
        ]);
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_pembelian_terakhir = TransaksiPembelian::where('kode_transaksi', 'like', '%TPMK%')->get()->last();
        if ($transaksi_pembelian_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPMK/'.$tanggal;
        } else {
            $kode_transaksi_pembelian_terakhir = $transaksi_pembelian_terakhir->kode_transaksi;
            $tanggal_transaksi_pembelian_terakhir = substr($kode_transaksi_pembelian_terakhir, 10, 10);
            // return [$kode_transaksi_pembelian_terakhir, $tanggal_transaksi_pembelian_terakhir];
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_pembelian_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_pembelian_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPMK/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPMK/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supliers = Suplier::whereHas('items', function ($query) {
            $query->where('konsinyasi', 1);
        })->get();
        return view('konsinyasi_masuk.create', compact('supliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $jumlah_stok = 0;
        $data = array();

        $persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $hutang_konsinyasi = Akun::where('kode', Akun::HutangKonsinyasi)->first();

        $temp_persediaan = 0;
        $temp_hutang_konsinyasi = 0;

        $stok_masuk = array();
        $perlu_notice = false;
        foreach ($request->item_id as $i => $id) {
            $cari_item = Item::find($id);
            $k_item = $cari_item->kode;
            // $items = Item::where('kode', $k_item)->get();
            $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
            foreach ($items as $j => $item) {
                $item->stoktotal += $request->jumlah[$i];
                $item->update();
            }

            $temp_persediaan += $request->harga[$i] * $request->jumlah[$i];

            $data[''.$id] = [
                'subtotal' => $request->subtotal[$i],
                'harga' => $request->harga[$i],
                'jumlah' => $request->jumlah[$i]
            ];

            $aktif = ($request->harga_biasa[$i] == 0) ? false : true;
            // return $aktif;
            if (!$aktif) $perlu_notice = true;
            $temp_subtotal = 0;
            // return $request->item_stok[$id];
            foreach ($request->item_stok[$id] as $j => $item) {
                $kadaluarsa = '';
                if ($request->kadaluarsa_stok[$id][$j] != '') {
                    $kadaluarsas = explode('-', $request->kadaluarsa_stok[$id][$j]);
                    $dd = $kadaluarsas[0];
                    $mm = $kadaluarsas[1];
                    $yyyy = $kadaluarsas[2];
                    $kadaluarsa = join('-', [$yyyy, $mm, $dd]);
                }

                $temp_subtotal += $request->hpp_stok[$id][$j] * $request->jumlah_stok[$id][$j];

                array_push($stok_masuk, [
                    'item_id' => $id,
                    'item_kode' => $cari_item->kode,
                    'harga' => $request->hpp_stok[$id][$j],
                    'jumlah' => $request->jumlah_stok[$id][$j],
                    'aktif' => $aktif,
                    'rusak' => 0,
                    'kadaluarsa' => $kadaluarsa,
                ]);
            }
            // return $stok_masuk;
            if ( sizeof(explode('.', $temp_subtotal)) != 1){
                $temp_subtotal = (float) ceil($temp_subtotal * 100) / 100;
            }

            $selisih_subtotal = $request->subtotal[$i] - $temp_subtotal;
            $selisih_subtotal = (float) (number_format($selisih_subtotal, 2));

            if($selisih_subtotal != 0){
                $index_stok_trakhir = count($stok_masuk) - 1;
                $stok_masuk[$index_stok_trakhir]['jumlah'] -= 1;
                $harga = $stok_masuk[$index_stok_trakhir]['harga'] + $selisih_subtotal;
                // $harga = ceil(($stok_masuk[$index_stok_trakhir]['harga'] + $selisih_subtotal)*100)/100;
                array_push($stok_masuk, [
                    'item_id' => $stok_masuk[$index_stok_trakhir]['item_id'],
                    'item_kode' => $stok_masuk[$index_stok_trakhir]['item_kode'],
                    'harga' => $harga,
                    'jumlah' => 1,
                    'aktif' => $stok_masuk[$index_stok_trakhir]['aktif'],
                    'rusak' => 0,
                    'kadaluarsa' => $stok_masuk[$index_stok_trakhir]['kadaluarsa'],
                ]);
            }
        }

        // return $stok_masuk;
        $konsinyasi_masuk = new TransaksiPembelian();
        $konsinyasi_masuk->kode_transaksi = self::kodeTransaksiBaru();
        $konsinyasi_masuk->nota = $request->nota;
        $konsinyasi_masuk->suplier_id = $request->suplier_id;
        $konsinyasi_masuk->seller_id = $request->seller_id;
        $konsinyasi_masuk->po = $request->po;
        $konsinyasi_masuk->harga_total = $request->harga_total;
        $konsinyasi_masuk->utang = $request->harga_total;
        $konsinyasi_masuk->sisa_utang = $request->harga_total;
        $konsinyasi_masuk->status_hutang = 1;
        $konsinyasi_masuk->user_id = Auth::id();

        $temp_hutang_konsinyasi += $request->harga_total;

        // $selisih = $temp_persediaan - $temp_hutang_konsinyasi;
        /*if($selisih < 0){
            Jurnal::create([
                'kode_akun' => Akun::BebanKerugianPersediaan,
                'referensi' => $request->kode_transaksi,
                'keterangan' => 'Penitipan Konsinyasi',
                'debet' => $selisih * -1
            ]);
        }*/

        $persediaan->debet += $request->harga_total;
        Jurnal::create([
            'kode_akun' => $persediaan->kode,
            'referensi' => $request->kode_transaksi,
            'keterangan' => 'Penitipan Konsinyasi',
            'debet' => $request->harga_total
            // 'debet' => $temp_persediaan
        ]);

        /*if($selisih > 0){
            Jurnal::create([
                'kode_akun' => Akun::UntungPersediaan,
                'referensi' => $request->kode_transaksi,
                'keterangan' => 'Penitipan Konsinyasi',
                'kredit' => $selisih
            ]);
        }*/
        
        $hutang_konsinyasi->kredit += $request->harga_total;
        Jurnal::create([
            'kode_akun' => $hutang_konsinyasi->kode,
            'referensi' => $request->kode_transaksi,
            'keterangan' => 'Penitipan Konsinyasi',
            'kredit' => $temp_hutang_konsinyasi
            // 'kredit' => $temp_hutang_konsinyasi
        ]);

        /*if ($selisih < 0) {
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanKerugianPersediaan,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih * -1
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih * -1
            ]);
        }
        if ($selisih > 0) {
            Jurnal::create([
                'kode_akun' => Akun::UntungPersediaan,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih
            ]);
        }*/

        /*$akun_LDT = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_LDT->kredit += $selisih;
        $akun_LDT->update();*/

        if ($konsinyasi_masuk->save() &&
            $persediaan->update() &&
            $hutang_konsinyasi->update()) {
            $konsinyasi_masuk->items()->sync($data);

            $transaksi = TransaksiPembelian::latest()->first();
            // return $transaksi;
            foreach ($stok_masuk as $key => $value) {
                $stok = new Stok();
                $stok->item_id = $value['item_id'];
                $stok->item_kode = $value['item_kode'];
                $stok->harga = $value['harga'];
                $stok->transaksi_pembelian_id = $transaksi->id;
                $stok->jumlah = $value['jumlah'];
                $stok->aktif = $value['aktif'];
                $stok->rusak = $value['rusak'];
                $stok->kadaluarsa = $value['kadaluarsa'];

                $stok->save();
            }

            if ($perlu_notice) {
                // Insert new notice
                $notice = new Notice();
                $notice->transaksi_pembelian_id = $transaksi->id;
                $notice->aktif = 1;
                $notice->save();
            }

            return redirect('/konsinyasi-masuk')->with('sukses', 'tambah');
        } else {
            return redirect('/konsinyasi-masuk')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $konsinyasi_masuk = TransaksiPembelian::with('suplier', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();

        if ($konsinyasi_masuk == null ||
            strtolower(explode('/', $konsinyasi_masuk->kode_transaksi)[1]) != 'tpmk') {
            return redirect('/konsinyasi-masuk')->with('gagal', '404');
        }

        $relasi_konsinyasi_masuk = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)->get();

        foreach ($relasi_konsinyasi_masuk as $i => $relasi) {
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $relasi->item->kode)
                ->orderBy('konversi', 'desc')->get();

            if (count($relasi_satuan) > 0) {
                $temp = $relasi->jumlah;
                $jumlah = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) $temp = $temp % $rs->konversi;
                    array_push($jumlah, array(
                        'jumlah' => $hasil,
                        'satuan' => $rs->satuan
                    ));
                }
            } else {
                $jumlah[0] = [
                    'jumlah' => $relasi->jumlah,
                    'satuan' => Satuan::where('kode', 'PCS')->first()
                ];
            }
            
            $relasi->jumlah = $jumlah;
        }

        return view('konsinyasi_masuk.show', compact('konsinyasi_masuk', 'relasi_konsinyasi_masuk'));
    }

    /*public function showHHB($transaksi_pembelian_id, $item_id)
    {
     $konsinyasi_masuk = TransaksiPembelian::with('suplier', 'items')->find($transaksi_pembelian_id);
     $relasi_transaksi_pembelian = RelasiTransaksiPembelian::with('item', 'konsinyasi_masuk')
         ->where('transaksi_pembelian_id', $transaksi_pembelian_id)->get();
     $item = Item::find($item_id);
     return view('konsinyasi_masuk.show', compact('konsinyasi_masuk', 'relasi_transaksi_pembelian', 'item'));
    }*/

    public function showItemJson($id)
    {
        $item = Item::find($id);
        $stok_terakhir = Stok::where('item_id', $id)->latest()->get();
        if (count($stok_terakhir) > 0) $item->pertama_beli = false;
        else $item->pertama_beli = true;

        $satuan_terkecil = RelasiSatuan::where('item_kode', $item->kode)
            ->orderBy('konversi', 'asc')
            ->first()
            ->satuan;
        // return $satuan_terkecil;

        $harga_jual = Harga::where('item_kode', $item->kode)
            ->where('satuan_id', $satuan_terkecil->id)
            ->first();
        // return $harga_jual;

        $harga_termurah = $harga_jual->eceran;
        if ($harga_jual->grosir < $harga_termurah) {
            $harga_termurah = $harga_jual->grosir;
        }
        $item->harga_termurah = $harga_termurah;

        $pcs = Satuan::where('kode', 'BIJI')->first();
        return response()->json([
            'item' => $item,
            'pcs' => $pcs
        ]);
    }

    public function showSuplierItemsJson($id)
    {
        $item_ = Item::where('suplier_id', $id)->where('konsinyasi', 1)->where('aktif', 1)->where('valid_konversi', 1)->get();
        $items = [];

        foreach ($item_ as $i => $item) {
            $cek_relasi = RelasiSatuan::where('item_kode', $item->kode)->get();
            if (sizeof($cek_relasi) > 0 ) array_push($items, $item);
        }

        return response()->json([
            'items' => $items
        ]);
    }

    public function showSuplierSellersJson($id)
    {
        $sellers = Seller::where('suplier_id', $id)->get();
        return response()->json([
            'sellers' => $sellers
        ]);
    }

    public function keluar($id)
    {
        $laci = Laci::find(1);
        $konsinyasi_masuk = TransaksiPembelian::with('suplier', 'seller', 'items')
            ->where('id', $id)
            ->where('status_hutang', 1)
            ->where('po', 0)
            ->first();

        if ($konsinyasi_masuk == null ||
            strtolower(explode('/', $konsinyasi_masuk->kode_transaksi)[1]) != 'tpmk') {
            return redirect('/konsinyasi-masuk')->with('gagal', '404');
        }

        $relasi_konsinyasi_masuk = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)->get();

        $supliers = Suplier::whereHas('items', function ($query) {
            $query->where('konsinyasi', 1);
        })->get();

        $suplier = '';
        foreach ($relasi_konsinyasi_masuk as $i => $relasi) {
            $suplier = $relasi->item->suplier;
            $jumlah_beli = $relasi->jumlah;
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $relasi->item->kode)
                ->orderBy('konversi', 'desc')->get();

            if (count($relasi_satuan) > 0) {
                $temp = $relasi->jumlah;
                $jumlah = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) {
                        $temp = $temp % $rs->konversi;
                        array_push($jumlah, array(
                            'jumlah' => $hasil,
                            'konversi' => $rs->konversi,
                            'satuan' => $rs->satuan
                        ));
                    }
                }
            } else {
                $jumlah[0] = [
                    'jumlah' => $relasi->jumlah,
                    'konversi' => 1,
                    'satuan' => Satuan::where('kode', 'PCS')->first()
                ];
            }
            $relasi->jumlah = $jumlah;
            $relasi->jumlah_beli = $jumlah_beli;
            $relasi->jumlah_sisa = Stok::where('item_kode',  $relasi->item->kode)->sum('jumlah');
            $relasi->jumlah_jual = $relasi->jumlah_beli - $relasi->jumlah_sisa;
        }

        $sellers = $suplier->sellers();
        $items = $suplier->items();

        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();

        $hasil = array();
        for ($i = 0; $i < sizeof($relasi_konsinyasi_masuk); $i++) {
            $item = Item::find($relasi_konsinyasi_masuk[$i]->item_id)->kode;
            $satuans = RelasiSatuan::where('item_kode', $item)->orderBy('konversi', 'desc')->get();

            // return $satuans;
            $sisa_jumlah = $relasi_konsinyasi_masuk[$i]->jumlah_beli;
            $a=-1;
            for ($x = 0; $x < count($satuans); $x++) {
                $hitung = explode('.', $sisa_jumlah / $satuans[$x]->konversi)[0];
                if ($hitung > 0) {
                    $a = $a + 1;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$x]->satuan_id)->kode;
                }
                $sisa_jumlah = $sisa_jumlah % $satuans[$x]->konversi;
            }
        }
        // return $hasil;
        return view('konsinyasi_masuk.keluar', compact('konsinyasi_masuk', 'relasi_konsinyasi_masuk', 'supliers', 'suplier', 'sellers', 'items', 'banks', 'ceks', 'bgs', 'hasil', 'laci'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
