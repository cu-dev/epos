<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Akun;
use App\Jurnal;
use App\Paralaba;
use App\PersenPajak;
use Illuminate\Http\Request;

class LabaRugiController extends Controller
{
    public function index()
    {
        $now = new DateTime();
        $sekarang = $now->format('m Y');

        $awal_ = strtotime('first day of this month', time());
        $awal = date('Y-m-d', $awal_);

        $akhir_str = strtotime('last day of this month', time());
        $akhir_1 = new DateTime(date('Y-m-d', $akhir_str));
        $akhir_1->add(new DateInterval('P1D'));
        $akhir = $akhir_1->format('Y-m-d');
        // dd($awal, $akhir);
        $data = $this->hitung($awal, $akhir);
        // return $data;
        $persen_pajak = PersenPajak::find(1)->persen;
        return view('laba_rugi.index', compact('sekarang', 'data', 'persen_pajak'));
    }

    public function tampil(Request $request)
    {
        // return $request->all();
        $awal_ = new DateTime($request->awal);
        $awal_->add(new DateInterval('P1D'));
        $awal = $awal_->format('Y-m-d');

        $akhir = new DateTime($request->akhir);
        $akhir->add(new DateInterval('P1D'));
        $akhir = $akhir->format('Y-m-d');
        
        $status = 0;
        $newAwal = explode("-", $awal);
        $newAkhir = explode("-", $akhir);
        $newAwal = Carbon::create($newAwal[0], $newAwal[1], $newAwal[2]);
        $newAkhir = Carbon::create($newAkhir[0], $newAkhir[1], $newAkhir[2]);

        $beda_hari = $newAkhir->diffInDays($newAwal);

        if($beda_hari == 1){
            $status = 1;
        }   
        $newAkhir = $newAkhir->subDays(1);

        // return $awal.'='.$akhir;
        $data = $this->hitung($awal, $akhir);
        // return $data;
        $persen_pajak = PersenPajak::find(1)->persen;
        return view('laba_rugi.tampil', compact('data', 'awal', 'akhir', 'newAwal', 'newAkhir', 'status', 'persen_pajak'));
    }

    public function sekarang()
    {
        $awal_ = (new DateTime())->modify('-1 day');
        $awal = $awal_->format('Y-m-d');

        $akhir_ = new DateTime();
        $akhir = $akhir_->format('Y-m-d');

        // return $awal.'='.$akhir;
        $data = $this->hitung($awal_, $akhir_);

        $awal_start = (new DateTime());
        $awal = $awal_start->format('Y-m-d');

        $akhir_end = new DateTime();
        $akhir_end->add(new DateInterval('P1D'));
        $akhir_end = $akhir_end->format('Y-m-d');

        $newAwal = explode("-", $awal);
        $newAkhir = explode("-", $akhir_end);
        $newAwal = Carbon::create($newAwal[0], $newAwal[1], $newAwal[2]);
        $newAkhir = Carbon::create($newAkhir[0], $newAkhir[1], $newAkhir[2]);

        $beda_hari = $newAkhir->diffInDays($newAwal);

        if($beda_hari == 1){
            $status = 1;
        }   
        $newAkhir = $newAkhir->subDays(1);
        
        $persen_pajak = PersenPajak::find(1)->persen;
        return view('laba_rugi.sekarang', compact('data', 'awal', 'akhir', 'awal_', 'akhir_', 'newAwal', 'newAkhir', 'status', 'akhir_end', 'persen_pajak'));
    }

    public function hitung($awal, $akhir)
    {
        return self::mhitung($awal, $akhir);
    }

    public static function mhitung($awal, $akhir)
    {
        $akuns['penjualan'] = Jurnal::where('kode_akun', Akun::Penjualan)
                                    ->where('keterangan', 'like', '%Transaksi Penjualan%')
                                    ->whereBetween('created_at', [$awal, $akhir])
                                    ->sum('kredit');

        $akuns['retur_penjualan_plus'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->whereBetween('created_at', [$awal, $akhir])
                        ->sum('debet');
        $akuns['retur_penjualan_minus'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');
        $akuns['retur_penjualan_out'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');
        $akuns['retur_penjualan_in'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');
        $akuns['retur_penjualan_LR'] = Jurnal::where('kode_akun', Akun::LabaRugi)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');
        $akuns['retur_penjualan'] = $akuns['retur_penjualan_plus'] - $akuns['retur_penjualan_minus'] - $akuns['retur_penjualan_out'] + $akuns['retur_penjualan_in'] + $akuns['retur_penjualan_LR'];
        if($akuns['retur_penjualan'] < 0) $akuns['retur_penjualan'] = 0;
        $akuns['pot_penjualan'] = Jurnal::where('kode_akun', Akun::PotonganPenjualan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');
        $akuns['bonus_penjualan'] = Jurnal::where('kode_akun', Akun::BonusPenjualan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        // $akuns['pot_pembelian'] = Jurnal::where('kode_akun', Akun::PotonganPembelian)
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->where('referensi', 'not like', '%[REV]%')
        //             ->sum('debet');

        $akuns['pot_pembelian'] = Jurnal::where('kode_akun', Akun::PotonganPembelian)
                        ->whereBetween('created_at', [$awal, $akhir])
                        ->sum('kredit');

        $akuns['pot_beli_kurang'] = Jurnal::where('kode_akun', Akun::PotonganPembelian)
                    ->where('referensi', 'like', '%[REV] L/R-PP%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');

        $akuns['pot_pembelian'] -= ($akuns['pot_beli_kurang'] * 2);
        // $akuns['pot_pembelian'] = 687434;

        // $akuns['penjualan_bersih'] = $akuns['penjualan'] + $akuns['retur_pembelian'] + $akuns['pot_pembelian'] - $akuns['retur_penjualan'] - $akuns['pot_penjualan'];
        $akuns['penjualan_bersih'] = $akuns['penjualan'] - $akuns['retur_penjualan'] - $akuns['pot_penjualan'] + $akuns['pot_pembelian'] - $akuns['bonus_penjualan'];

        $hpp_debet = Jurnal::where('kode_akun', Akun::HPP)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $hpp_kredit = Jurnal::where('kode_akun', Akun::HPP)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');

        $akuns['HPP'] = $hpp_debet - $hpp_kredit;

        $akuns['beban_ongkir'] = Jurnal::where('kode_akun', Akun::BebanOngkir)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_ongkir_kurang'] = Jurnal::where('kode_akun', Akun::BebanOngkir)
                    ->where('referensi', 'like', '%[REV] L/R-BO%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_ongkir'] -= ($akuns['beban_ongkir_kurang'] * 2);
        // $akuns['beban_ongkir'] = 1000;

        $akuns['laba_kotor'] = $akuns['penjualan_bersih'] - $akuns['HPP'] - $akuns['beban_ongkir'];

        $akuns['beban_iklan'] = Jurnal::where('kode_akun', Akun::BebanIklan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_sewa'] = Jurnal::where('kode_akun', Akun::BebanSewa)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_beli'] = Jurnal::where('kode_akun', Akun::BebanRugiBeli)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_perlengkapan'] = Jurnal::where('kode_akun', Akun::BebanPerlengkapan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_rawat'] = Jurnal::where('kode_akun', Akun::BebanPerawatan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_rugi_aset'] = Jurnal::where('kode_akun', Akun::BebanRugiAset)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        //noted ????
        $akuns['rugi_piutang'] = Jurnal::where('kode_akun', Akun::BebanKerugianPiutang)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_dep_alat'] = Jurnal::where('kode_akun', Akun::BebanDepPeralatan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_dep_kendaraan'] = Jurnal::where('kode_akun', Akun::BebanDepKendaraan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_asuransi'] = Jurnal::where('kode_akun', Akun::BebanAsuransi)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_gaji'] = Jurnal::where('kode_akun', Akun::BebanGaji)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_adm_bank'] = Jurnal::where('kode_akun', Akun::BebanAdministrasiBank)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_denda'] = Jurnal::where('kode_akun', Akun::BebanDenda)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_utilitas'] = Jurnal::where('kode_akun', Akun::BebanUtilitas)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_listrik'] = Jurnal::where('kode_akun', Akun::BebanListrik)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_lain'] = Jurnal::where('kode_akun', Akun::BebanLain)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['rugi_persediaan'] = Jurnal::where('kode_akun', Akun::BebanKerugianPersediaan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['dep_bangunan'] = Jurnal::where('kode_akun', Akun::BebanDepBangunan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['transportasi'] = Jurnal::where('kode_akun', Akun::BebanTransportasi)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['pajak'] = Jurnal::where('kode_akun', Akun::BebanPajak)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        // $akuns['PPNMasuk'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', 'Retur%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('kredit');

        // $akuns['PPNMasukP'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', '%Barang Sama%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('debet');

        // $akuns['PPNMasukOut'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', '%Barang Lain%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('kredit');

        // $akuns['PPNMasukIn'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', '%Barang Lain%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('debet');

        // $akuns['pajak'] -= $akuns['PPNMasuk'] - $akuns['PPNMasukP'];
        // $akuns['pajak'] -= $akuns['PPNMasukP'] + $akuns['PPNMasukOut'] - $akuns['PPNMasukIn'];

        $akuns['bunga'] = Jurnal::where('kode_akun', Akun::BebanBunga)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['konsumsi'] = Jurnal::where('kode_akun', Akun::BebanKonsumsi)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['sosial'] = Jurnal::where('kode_akun', Akun::BebanSosial)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['total_beban'] = $akuns['beban_iklan'] 
                                + $akuns['beban_sewa'] 
                                + $akuns['beban_perlengkapan'] 
                                + $akuns['beban_rawat'] 
                                + $akuns['beban_rugi_aset'] 
                                + $akuns['rugi_piutang'] 
                                + $akuns['beban_dep_alat'] 
                                + $akuns['beban_dep_kendaraan'] 
                                + $akuns['beban_asuransi'] 
                                + $akuns['beban_gaji'] 
                                + $akuns['beban_adm_bank'] 
                                + $akuns['beban_denda'] 
                                + $akuns['beban_utilitas'] 
                                + $akuns['beban_lain'] 
                                + $akuns['dep_bangunan'] 
                                + $akuns['transportasi']
                                // + $akuns['garansi'] 
                                + $akuns['rugi_persediaan']
                                + $akuns['pajak'] 
                                + $akuns['konsumsi'] 
                                + $akuns['sosial']
                                + $akuns['beban_beli'] 
                                + $akuns['beban_listrik']
                                + $akuns['bunga'];

        $akuns['laba_beban'] = $akuns['laba_kotor'] - $akuns['total_beban'];
    
        $akuns['pendapatan_lain_plus'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');
        $akuns['pendapatan_lain_min'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');
        $akuns['pendapatan_lain_retur'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');     
        $akuns['pendapatan_lain'] = $akuns['pendapatan_lain_plus'] - $akuns['pendapatan_lain_min'] - $akuns['pendapatan_lain_retur'];

        $akuns['untung_persediaan'] = Jurnal::where('kode_akun', Akun::UntungPersediaan)
                    ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('kredit');

        $akuns['laba_sebelum_pajak'] = $akuns['laba_kotor'] + $akuns['pendapatan_lain'] + $akuns['untung_persediaan'] - $akuns['total_beban']; 

        // $value = $akuns;
        foreach ($akuns as $x => $akun) {
            $akuns[$x] = number_format($akun, 2, ',', '.');
        }

        return $akuns;
    }

    public static function todayHitung($tanggal, $jam)
    {
        $akuns['penjualan'] = Jurnal::where('kode_akun', Akun::Penjualan)
                                    ->where('keterangan', 'like', '%Transaksi Penjualan%')
                                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                                    ->sum('kredit');

        $akuns['retur_penjualan_plus'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                        ->sum('debet');
        $akuns['retur_penjualan_minus'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');
        $akuns['retur_penjualan_out'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');
        $akuns['retur_penjualan_in'] = Jurnal::where('kode_akun', Akun::ReturPenjualan)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');
        $akuns['retur_penjualan_LR'] = Jurnal::where('kode_akun', Akun::LabaRugi)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');
        $akuns['retur_penjualan'] = $akuns['retur_penjualan_plus'] - $akuns['retur_penjualan_minus'] - $akuns['retur_penjualan_out'] + $akuns['retur_penjualan_in'] + $akuns['retur_penjualan_LR'];

        if($akuns['retur_penjualan'] < 0) $akuns['retur_penjualan'] = 0;
        $akuns['pot_penjualan'] = Jurnal::where('kode_akun', Akun::PotonganPenjualan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['bonus_penjualan'] = Jurnal::where('kode_akun', Akun::BonusPenjualan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    // ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['pot_pembelian'] = Jurnal::where('kode_akun', Akun::PotonganPembelian)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    // ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        // $akuns['penjualan_bersih'] = $akuns['penjualan'] + $akuns['retur_pembelian'] + $akuns['pot_pembelian'] - $akuns['retur_penjualan'] - $akuns['pot_penjualan'];
        $akuns['penjualan_bersih'] = $akuns['penjualan'] - $akuns['retur_penjualan'] - $akuns['pot_penjualan'] + $akuns['pot_pembelian'] - $akuns['bonus_penjualan'];

        $hpp_debet = Jurnal::where('kode_akun', Akun::HPP)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $hpp_kredit = Jurnal::where('kode_akun', Akun::HPP)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');

        $akuns['HPP'] = $hpp_debet - $hpp_kredit;

        $akuns['beban_ongkir'] = Jurnal::where('kode_akun', Akun::BebanOngkir)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['laba_kotor'] = $akuns['penjualan_bersih'] - $akuns['HPP'] - $akuns['beban_ongkir'];

        $akuns['beban_iklan'] = Jurnal::where('kode_akun', Akun::BebanIklan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_sewa'] = Jurnal::where('kode_akun', Akun::BebanSewa)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_beli'] = Jurnal::where('kode_akun', Akun::BebanRugiBeli)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_perlengkapan'] = Jurnal::where('kode_akun', Akun::BebanPerlengkapan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_rawat'] = Jurnal::where('kode_akun', Akun::BebanPerawatan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_rugi_aset'] = Jurnal::where('kode_akun', Akun::BebanRugiAset)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        //noted ????
        $akuns['rugi_piutang'] = Jurnal::where('kode_akun', Akun::BebanKerugianPiutang)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_dep_alat'] = Jurnal::where('kode_akun', Akun::BebanDepPeralatan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_dep_kendaraan'] = Jurnal::where('kode_akun', Akun::BebanDepKendaraan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_asuransi'] = Jurnal::where('kode_akun', Akun::BebanAsuransi)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_gaji'] = Jurnal::where('kode_akun', Akun::BebanGaji)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_adm_bank'] = Jurnal::where('kode_akun', Akun::BebanAdministrasiBank)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_denda'] = Jurnal::where('kode_akun', Akun::BebanDenda)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_utilitas'] = Jurnal::where('kode_akun', Akun::BebanUtilitas)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['beban_listrik'] = Jurnal::where('kode_akun', Akun::BebanListrik)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    // ->whereBetween('created_at', [$awal, $akhir])
                    ->sum('debet');

        $akuns['beban_lain'] = Jurnal::where('kode_akun', Akun::BebanLain)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['rugi_persediaan'] = Jurnal::where('kode_akun', Akun::BebanKerugianPersediaan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['dep_bangunan'] = Jurnal::where('kode_akun', Akun::BebanDepBangunan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['transportasi'] = Jurnal::where('kode_akun', Akun::BebanTransportasi)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['pajak'] = Jurnal::where('kode_akun', Akun::BebanPajak)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        // $akuns['PPNMasuk'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', 'Retur%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('kredit');

        // $akuns['PPNMasukP'] = Jurnal::where('kode_akun', Akun::PPNMasukan)
        //             ->where('keterangan', 'like', 'Retur%')
        //             ->whereBetween('created_at', [$awal, $akhir])
        //             ->sum('debet');

        // $akuns['pajak'] -= $akuns['PPNMasuk'] - $akuns['PPNMasukP'];

        $akuns['bunga'] = Jurnal::where('kode_akun', Akun::BebanBunga)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['konsumsi'] = Jurnal::where('kode_akun', Akun::BebanKonsumsi)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['sosial'] = Jurnal::where('kode_akun', Akun::BebanSosial)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');

        $akuns['total_beban'] = $akuns['beban_iklan'] 
                                + $akuns['beban_sewa'] 
                                + $akuns['beban_beli']
                                + $akuns['beban_perlengkapan'] 
                                + $akuns['beban_rawat'] 
                                + $akuns['beban_rugi_aset'] 
                                + $akuns['rugi_piutang'] 
                                + $akuns['beban_dep_alat'] 
                                + $akuns['beban_dep_kendaraan'] 
                                + $akuns['beban_asuransi'] 
                                + $akuns['beban_gaji'] 
                                + $akuns['beban_adm_bank'] 
                                + $akuns['beban_denda'] 
                                + $akuns['beban_utilitas'] 
                                + $akuns['beban_lain'] 
                                + $akuns['dep_bangunan']
                                + $akuns['transportasi']
                                // + $akuns['garansi'] 
                                + $akuns['rugi_persediaan']
                                + $akuns['pajak'] 
                                + $akuns['konsumsi'] 
                                + $akuns['sosial'] 
                                + $akuns['beban_listrik']
                                + $akuns['bunga'];

        $akuns['laba_beban'] = $akuns['laba_kotor'] - $akuns['total_beban'];
    
        $akuns['pendapatan_lain_plus'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');
        $akuns['pendapatan_lain_min'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');
        $akuns['pendapatan_lain_retur'] = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('keterangan', 'like', '%TPNRetur%')
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('debet');     
        $akuns['pendapatan_lain'] = $akuns['pendapatan_lain_plus'] - $akuns['pendapatan_lain_min'] - $akuns['pendapatan_lain_retur'];

        $akuns['untung_persediaan'] = Jurnal::where('kode_akun', Akun::UntungPersediaan)
                    ->where('created_at', 'like', $tanggal.' '.$jam.'%')
                    ->sum('kredit');

        $akuns['laba_sebelum_pajak'] = $akuns['laba_kotor'] + $akuns['pendapatan_lain'] + $akuns['untung_persediaan'] - $akuns['total_beban']; 

        // $value = $akuns;
        foreach ($akuns as $x => $akun) {
            $akuns[$x] = number_format($akun, 2, ',', '.');
        }

        return $akuns;
    }

    public function cetak(Request $request)
    {
        // return $request->all();
        $awal_ = new DateTime($request->awal);
        $awal = $awal_->format('Y-m-d');

        $akhir_ = new DateTime($request->akhir);
        $akhir = $akhir_->format('Y-m-d');

        $status = 0;
        $newAwal = new Carbon($request->awal);
        $newAkhir = new Carbon($request->newAkhir);
        $akhiri = new Carbon($request->akhir);

        // dd($newAkhir);

        $beda_hari = $akhiri->diffInDays($newAwal);

        if($beda_hari == 1){
            $status = 1;
        }   

        if($request->status != NULL){
            $status = 2;
            $awal_ = new DateTime($request->awal);
            $awal = $awal_->format('Y-m-d');

            $akhir_ = (new DateTime($request->akhir))->modify('+1 day');
            $akhir = $akhir_->format('Y-m-d');
        }

        // return $awal.'='.$akhir;
        $data = $this->hitung($awal, $akhir);

        return view('laba_rugi.cetak', compact('data', 'awal', 'akhir', 'newAwal', 'newAkhir', 'status'));
    }

    public function penutup(Request $request)
    {
        // return $request->all();
        $status = 0;
        $newAwal = new Carbon($request->awal);
        $newAkhir = new Carbon($request->newAkhir);
        $akhiri = new Carbon($request->akhir);

        $beda_hari = $akhiri->diffInDays($newAwal);

        if($beda_hari == 1){
            $status = 1;
        }

        return view('laba_rugi.penutup', compact('data', 'status', 'newAwal', 'newAkhir', 'akhiri', 'request'));
    }

    public function penutup_save(Request $request){
        return $request->all();
    }

    public function pajak(Request $request)
    {
        $awal_ = new DateTime($request->awal);
        $awal = $awal_->format('Y-m-d');

        $akhir_ = new DateTime($request->akhir);
        $akhir = $akhir_->format('Y-m-d');

        $status = 0;
        $newAwal = new Carbon($request->awal);
        $newAkhir = new Carbon($request->newAkhir);
        $akhiri = new Carbon($request->akhir);

        // dd($newAkhir);

        $beda_hari = $akhiri->diffInDays($newAwal);

        if($beda_hari == 1){
            $status = 1;
        }   

        if($request->status != NULL){
            $status = 2;
            $awal_ = new DateTime($request->awal);
            $awal = $awal_->format('Y-m-d');

            $akhir_ = (new DateTime($request->akhir))->modify('+1 day');
            $akhir = $akhir_->format('Y-m-d');
        }

        $data = $this->hitung($awal, $akhir);
        // return $data;
        $cek_laba = Paralaba::get()->last();
        if($cek_laba == NULL){
            $cek_penjualan = Jurnal::where('kode_akun', Akun::Penjualan)->orWhere('kode_akun', Akun::PenjualanKredit)->first();
            if($cek_penjualan == NULL){
                $cek_akun = Akun::where('kode', Akun::Pendapatan)->first();
                $start = ($cek_akun->updated_at)->format('d m Y');
                $end_str = $cek_akun->updated_at;
            }else{
                $start = ($cek_penjualan->created_at)->format('d m Y');
                $end_str = $cek_penjualan->created_at;
            }
        }else{
            $end_str = new DateTime(date($cek_laba->akhir));
            $end_str->add(new DateInterval('P1D'));
            $start = $end_str->format('d m Y');
        }

        return view('laba_rugi.pajak', compact('data', 'awal', 'akhir', 'akhiri', 'newAwal', 'newAkhir', 'status', 'start'));
    }
}
