<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Akun;
use App\Arus;
use App\Jurnal;
use App\HutangBank;
use App\BayarHutangBank;
use Illuminate\Http\Request;

class BayarHutangBankController extends Controller
{
    public function lastJson()
    {
        $bayar_hutang_bank = BayarHutangBank::all()->last();
        return response()->json(['bayar_hutang_bank' => $bayar_hutang_bank]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $bayar_hutang_bank = new BayarHutangBank();
        $hutang_bank = HutangBank::where('id', $request->hutang_bank_id)->first();
        $jurnal_hutang_bank = new Jurnal();

        // isi $hutang_bank;
        $bayar_hutang_bank->hutang_bank_id = $request->hutang_bank_id;
        $bayar_hutang_bank->nominal = $request->nominal;
        $bayar_hutang_bank->kode_transaksi = $request->kode_transaksi;
        $bayar_hutang_bank->no_transaksi = $request->no_transaksi;
        //update sisa hutang
        $hutang_bank->sisa_hutang = $hutang_bank->sisa_hutang - $request->nominal;
        $hutang_bank->update();
        //jurnal hutang bank
        $jurnal_hutang_bank->kode_akun = Akun::HutangBank;
        $jurnal_hutang_bank->referensi = $request->kode_transaksi;
        $jurnal_hutang_bank->debet = $request->nominal;
        $jurnal_hutang_bank->keterangan = 'Angsuran '.$hutang_bank->kode_transaksi;
        $jurnal_hutang_bank->save();

        if($request->bunga != NULL){
            //jurnal bunga kalo ada
            $jurnal_bunga = new Jurnal();
            $jurnal_bunga->kode_akun = Akun::BebanBunga;
            $jurnal_bunga->referensi = $request->kode_transaksi;
            $jurnal_bunga->debet = $request->bunga;
            $jurnal_bunga->keterangan = 'Angsuran '.$hutang_bank->kode_transaksi;
            $jurnal_bunga->save();
        }

        // update akun hutang bank
        $akun_hutang_bank = Akun::where('kode', Akun::HutangBank)->first();
        $akun_hutang_bank->kredit -= $request->nominal;
        $akun_hutang_bank->update();

        if($request->kas=='tunai'){
            //update akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->total;
            $akun_tunai->update();
            //metode pembayaran
            $bayar_hutang_bank->metode_pembayaran = 'tunai';
            //jurnal tunai
            $jurnal_tunai = new Jurnal();
            $jurnal_tunai->kode_akun = Akun::KasTunai;
            $jurnal_tunai->referensi = $request->kode_transaksi;
            $jurnal_tunai->kredit = $request->total;
            $jurnal_tunai->keterangan = 'Angsuran '.$hutang_bank->kode_transaksi;
            $jurnal_tunai->save();
        }else{
            //update akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -=$request->total;
            $akun_bank->update();
            //metode pembayaran
            $bayar_hutang_bank->metode_pembayaran = 'debit';
            //update bank nominal
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal - $request->total;
            $bank->update();
            //jurnal akun bank
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode_transaksi;
            $jurnal_bank->kredit = $request->total;
            $jurnal_bank->keterangan = 'Angsuran '.$hutang_bank->kode_transaksi.' - '.$bank->nama_bank;
            $jurnal_bank->save();
        }

        if($request->bunga != NULL){
            $jurnal_l_r = new Jurnal();
            $jurnal_l_r->kode_akun = Akun::LabaRugi;
            $jurnal_l_r->referensi = $request->kode_transaksi.' - L/R';
            $jurnal_l_r->debet = $request->bunga;
            $jurnal_l_r->keterangan = 'Laba Rugi '.$hutang_bank->kode_transaksi;
            $jurnal_l_r->save();

            $jurnal_bunga = new Jurnal();
            $jurnal_bunga->kode_akun = Akun::BebanBunga;
            $jurnal_bunga->referensi = $request->kode_transaksi.' - L/R';
            $jurnal_bunga->kredit = $request->bunga;
            $jurnal_bunga->keterangan = 'Laba Rugi '.$hutang_bank->kode_transaksi;
            $jurnal_bunga->save();

            $jurnal_bunga = new Jurnal();
            $jurnal_bunga->kode_akun = Akun::LabaTahan;
            $jurnal_bunga->referensi = $request->kode_transaksi.' - LDT';
            $jurnal_bunga->debet = $request->bunga;
            $jurnal_bunga->keterangan = 'Laba Rugi '.$hutang_bank->kode_transaksi;
            $jurnal_bunga->save();

            $jurnal_l_r_t = new Jurnal();
            $jurnal_l_r_t->kode_akun = Akun::LabaRugi;
            $jurnal_l_r_t->referensi = $request->kode_transaksi.' - LDT';
            $jurnal_l_r_t->kredit = $request->bunga;
            $jurnal_l_r_t->keterangan = 'Laba Rugi '.$hutang_bank->kode_transaksi;
            $jurnal_l_r_t->save();

            $akun_LDT = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_LDT->kredit -= $request->bunga;
            $akun_LDT->update();
            

            Arus::create([
                'nama' => Arus::Beban,
                'nominal' => $request->bunga,
            ]);
        }

        Arus::create([
            'nama' => Arus::Kewajiban,
            'nominal' => $request->nominal
        ]);
        if ($bayar_hutang_bank->save()) {
            return redirect('/hutang_bank/show/'.$request->hutang_bank_id)->with('sukses', 'tambah');
        } else {
            return redirect('/hutang_bank/show/'.$request->hutang_bank_id)->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
