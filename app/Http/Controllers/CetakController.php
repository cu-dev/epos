<?php

namespace App\Http\Controllers;

use Auth;
use App\Item;
use App\Harga;
use App\Satuan;
use App\Catatan;
use App\CetakNota;
use App\RelasiBundle;
use App\RelasiSatuan;
use App\Pelanggan;
use App\ReturPenjualan;
use App\DataPerusahaan;
use App\PembayaranDeposito;
use App\TransaksiPenjualan;
use App\TransaksiPembelian;
use App\RelasiReturPenjualan;
use App\PembayaranReturPenjualan;
use App\RelasiTransaksiPembelian;
use App\RelasiTransaksiPenjualan;
use App\RelasiBonusRulesPenjualan;

use Illuminate\Http\Request;

class CetakController extends Controller
{
    /*public function pengambilan($transaksi_penjualan_id)
    {
        $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->whereIn('status', ['po_eceran', 'po_grosir', 'po_vip'])->first();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $temp_relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            $relasi_transaksi_penjualan = array();
            foreach ($temp_relasi_transaksi_penjualan as $i => $relasi) {
                if ($relasi->item->konsinyasi < 2) {
                    $relasi->nama_pendek = $relasi->item->nama_pendek;
                    array_push($relasi_transaksi_penjualan, $relasi);
                } else {
                    $relasi_bundles = RelasiBundle::where('item_parent', $relasi->item_kode)->get();
                    foreach ($relasi_bundles as $j => $relasi_bundle) {
                        $temp_rtp = null;
                        $temp_rtp = clone $relasi;
                        $temp_rtp->nama_pendek = $relasi_bundle->itemxx->nama_pendek;
                        $temp_rtp->item_kode = $relasi_bundle->item_child;
                        $temp_rtp->jumlah = $relasi->jumlah * $relasi_bundle->jumlah;
                        array_push($relasi_transaksi_penjualan, $temp_rtp);
                    }
                }
            }

            $kasir_eceran = null;
            $kasir_grosir = null;
            if ($transaksi_penjualan->user_id != null) {
                $kasir_eceran = ucfirst($transaksi_penjualan->user->nama);
            }
            if ($transaksi_penjualan->grosir_id != null) {
                $kasir_grosir = ucfirst($transaksi_penjualan->grosir->nama);
            }
            if ($kasir_eceran == null) $kasir_eceran = $kasir_grosir;

            $created_at = $transaksi_penjualan->created_at;
            $tanggal = explode(' ', $created_at)[0];
            $waktu = explode(' ', $created_at)[1];
            $tanggals = explode('-', $tanggal);
            $yyyy = $tanggals[0];
            $mm = $tanggals[1];
            $dd = $tanggals[2];
            $tanggal = $dd.'/'.$mm.'/'.$yyyy;
            $transaksi_penjualan->timestamp = $tanggal.' '.$waktu;

            return view('cetak.pengambilan', compact('transaksi_penjualan', 'relasi_transaksi_penjualan', 'kasir_eceran'));
        }
    }*/

    public function pengambilan($transaksi_penjualan_id)
    {
        // $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->whereIn('status', ['po_eceran', 'po_grosir', 'po_vip'])->first();
        $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->first();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $relasi_transaksi_penjualan = array();
            $temp_relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            // untuk bonus transaksi
            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            foreach ($relasi_bonus_rules_penjualan as $i => $relasi) {
                $bonus = $relasi->bonus;
                $satuan_pembelians = array();
                foreach ($bonus->satuan_pembelians as $j => $satuan) {
                    array_push($satuan_pembelians, [
                        'konversi' => $satuan->konversi,
                        'satuan' => [
                            'id' => $satuan->satuan->id,
                            'kode' => $satuan->satuan->kode
                        ]
                    ]);
                }

                array_push($relasi_transaksi_penjualan, [
                    'absen' => false,
                    'item_kode' => $bonus->kode,
                    'jumlah' => $relasi->jumlah,
                    'harga' => 0,
                    'subtotal' => 0,
                    'item' => [
                        'kode' => $bonus->kode,
                        'nama' => $bonus->nama,
                        'nama_pendek' => $bonus->nama_pendek,
                        'satuan_pembelians' => $satuan_pembelians,
                        'bundle' => $bonus->konsinyasi
                    ]
                ]);
            }

            foreach ($temp_relasi_transaksi_penjualan as $i => $relasi) {
                // satuan pembelian untuk item biasa & bundle
                $satuan_pembelians = array();
                foreach ($relasi->item->satuan_pembelians as $j => $satuan) {
                    array_push($satuan_pembelians, [
                        'konversi' => $satuan->konversi,
                        'satuan' => [
                            'id' => $satuan->satuan->id,
                            'kode' => $satuan->satuan->kode
                        ]
                    ]);
                }

                // item biasa & bundle
                array_push($relasi_transaksi_penjualan, [
                    'absen' => true,
                    'item_kode' => $relasi->item_kode,
                    'jumlah' => $relasi->jumlah,
                    'harga' => $relasi->harga,
                    'subtotal' => $relasi->subtotal,
                    'item' => [
                        'kode' => $relasi->item->kode,
                        'nama' => $relasi->item->nama,
                        'nama_pendek' => $relasi->item->nama_pendek,
                        'satuan_pembelians' => $satuan_pembelians,
                        'bundle' => $relasi->item->konsinyasi
                    ]
                ]);

                if ($relasi->item->konsinyasi == 2) {
                    // $relasi_bundles = RelasiBundle::where('item_parent', $relasi->item_kode)->get();
                    foreach ($relasi->relasi_bundle as $j => $rb) {
                        // satuan pembelian untuk item child bundle
                        $satuan_pembelians = array();
                        foreach ($rb->itemxx->satuan_pembelians as $j => $satuan) {
                            array_push($satuan_pembelians, [
                                'konversi' => $satuan->konversi,
                                'satuan' => [
                                    'id' => $satuan->satuan->id,
                                    'kode' => $satuan->satuan->kode
                                ]
                            ]);
                        }

                        // item child bundle
                        array_push($relasi_transaksi_penjualan, [
                            'absen' => false,
                            'item_kode' => $rb->itemxx->kode,
                            'jumlah' => $rb->jumlah,
                            'harga' => 0,
                            'subtotal' => 0,
                            'item' => [
                                'kode' => $rb->itemxx->kode,
                                'nama' => $rb->itemxx->nama,
                                'nama_pendek' => $rb->itemxx->nama_pendek,
                                'satuan_pembelians' => $satuan_pembelians,
                                'bundle' => $rb->itemxx->konsinyasi
                            ]
                        ]);
                    }
                }

                // untuk item bonus
                foreach ($relasi->relasi_bonus_penjualan as $j => $rbp) {
                    // satuan pembelian untuk item bonus
                    $satuan_pembelians = array();
                    foreach ($rbp->bonus->satuan_pembelians as $j => $satuan) {
                        array_push($satuan_pembelians, [
                            'konversi' => $satuan->konversi,
                            'satuan' => [
                                'id' => $satuan->satuan->id,
                                'kode' => $satuan->satuan->kode
                            ]
                        ]);
                    }

                    // item bonus
                    array_push($relasi_transaksi_penjualan, [
                        'absen' => false,
                        'item_kode' => $rbp->bonus->kode,
                        'jumlah' => $rbp->jumlah,
                        'harga' => 0,
                        'subtotal' => 0,
                        'item' => [
                            'kode' => $rbp->bonus->kode,
                            'nama' => $rbp->bonus->nama,
                            'nama_pendek' => $rbp->bonus->nama_pendek,
                            'satuan_pembelians' => $satuan_pembelians,
                            'bundle' => $rbp->bonus->konsinyasi
                        ]
                    ]);
                }
            }

            $kasir_eceran = '';
            $kasir_grosir = '';
            if ($transaksi_penjualan->user_id != null) {
                $temp_nama = explode(' ', $transaksi_penjualan->user->nama);
                for ($a = 0; $a < sizeof($temp_nama); $a++) {
                    if ($a == 0) {
                        $kasir_eceran .= $temp_nama[$a].' ';
                    } else {
                        $kasir_eceran .= $temp_nama[$a][0];
                    }

                }
                // $kasir_eceran = ucfirst($transaksi_penjualan->user->nama);
            }
            if ($transaksi_penjualan->grosir_id != null) {
                // $kasir_grosir = ucfirst($transaksi_penjualan->grosir->nama);
                $temp_nama = explode(' ', $transaksi_penjualan->grosir->nama);
                for ($a = 0; $a < sizeof($temp_nama); $a++) {
                    if ($a == 0) {
                        $kasir_grosir .= $temp_nama[$a].' ';
                    } else {
                        $kasir_grosir .= $temp_nama[$a][0];
                    }

                }
            }
            if ($kasir_eceran == '') $kasir_eceran = $kasir_grosir;

            $created_at = $transaksi_penjualan->created_at;
            $tanggal = explode(' ', $created_at)[0];
            $waktu = explode(' ', $created_at)[1];
            $tanggals = explode('-', $tanggal);
            $yyyy = $tanggals[0];
            $mm = $tanggals[1];
            $dd = $tanggals[2];
            $tanggal = $dd.'/'.$mm.'/'.$yyyy;
            $transaksi_penjualan->timestamp = $tanggal.' '.$waktu;
            $catatans = Catatan::where('jenis_catatan', 'struk')->get();

            // return $relasi_transaksi_penjualan[0]['item']['bundle'];
            return view('cetak.pengambilan', compact('transaksi_penjualan', 'relasi_transaksi_penjualan', 'kasir_eceran', 'catatans'));
        }
    }

    public function eceran($transaksi_penjualan_id)
    {
        // khusus eceran
        // $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->whereIn('status', ['eceran'])->first();
        $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->first();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $data_perusahaan = DataPerusahaan::find(1);
            $catatans = Catatan::where('jenis_catatan', 'struk')->get();
            // $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            $relasi_transaksi_penjualan = array();
            $temp_relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            // untuk bonus transaksi
            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            foreach ($relasi_bonus_rules_penjualan as $i => $relasi) {
                $bonus = $relasi->bonus;
                $satuan_pembelians = array();
                foreach ($bonus->satuan_pembelians as $j => $satuan) {
                    array_push($satuan_pembelians, [
                        'konversi' => $satuan->konversi,
                        'satuan' => [
                            'id' => $satuan->satuan->id,
                            'kode' => $satuan->satuan->kode
                        ]
                    ]);
                }

                array_push($relasi_transaksi_penjualan, [
                    'absen' => false,
                    'item_kode' => $bonus->kode,
                    'jumlah' => $relasi->jumlah,
                    'harga' => 0,
                    'subtotal' => 0,
                    'item' => [
                        'kode' => $bonus->kode,
                        'nama' => $bonus->nama,
                        'nama_pendek' => $bonus->nama_pendek,
                        'satuan_pembelians' => $satuan_pembelians
                    ]
                ]);
            }
            
            foreach ($temp_relasi_transaksi_penjualan as $i => $relasi) {
                // satuan pembelian untuk item biasa & bundle
                $satuan_pembelians = array();
                foreach ($relasi->item->satuan_pembelians as $j => $satuan) {
                    array_push($satuan_pembelians, [
                        'konversi' => $satuan->konversi,
                        'satuan' => [
                            'id' => $satuan->satuan->id,
                            'kode' => $satuan->satuan->kode
                        ]
                    ]);
                }

                // item biasa & bundle
                array_push($relasi_transaksi_penjualan, [
                    'absen' => true,
                    'item_kode' => $relasi->item_kode,
                    'jumlah' => $relasi->jumlah,
                    'harga' => $relasi->harga,
                    'subtotal' => $relasi->subtotal,
                    'item' => [
                        'kode' => $relasi->item->kode,
                        'nama' => $relasi->item->nama,
                        'nama_pendek' => $relasi->item->nama_pendek,
                        'satuan_pembelians' => $satuan_pembelians
                    ]
                ]);

                if ($relasi->item->konsinyasi == 2) {
                    // $relasi_bundles = RelasiBundle::where('item_parent', $relasi->item_kode)->get();
                    foreach ($relasi->relasi_bundle as $j => $rb) {
                        // satuan pembelian untuk item child bundle
                        $satuan_pembelians = array();
                        foreach ($rb->itemxx->satuan_pembelians as $j => $satuan) {
                            array_push($satuan_pembelians, [
                                'konversi' => $satuan->konversi,
                                'satuan' => [
                                    'id' => $satuan->satuan->id,
                                    'kode' => $satuan->satuan->kode
                                ]
                            ]);
                        }

                        // item child bundle
                        array_push($relasi_transaksi_penjualan, [
                            'absen' => false,
                            'item_kode' => $rb->itemxx->kode,
                            'jumlah' => $rb->jumlah,
                            'harga' => 0,
                            'subtotal' => 0,
                            'item' => [
                                'kode' => $rb->itemxx->kode,
                                'nama' => $rb->itemxx->nama,
                                'nama_pendek' => $rb->itemxx->nama_pendek,
                                'satuan_pembelians' => $satuan_pembelians
                            ]
                        ]);
                    }
                }

                // untuk item bonus
                foreach ($relasi->relasi_bonus_penjualan as $j => $rbp) {
                    // satuan pembelian untuk item bonus
                    $satuan_pembelians = array();
                    foreach ($rbp->bonus->satuan_pembelians as $j => $satuan) {
                        array_push($satuan_pembelians, [
                            'konversi' => $satuan->konversi,
                            'satuan' => [
                                'id' => $satuan->satuan->id,
                                'kode' => $satuan->satuan->kode
                            ]
                        ]);
                    }

                    // item bonus
                    array_push($relasi_transaksi_penjualan, [
                        'absen' => false,
                        'item_kode' => $rbp->bonus->kode,
                        'jumlah' => $rbp->jumlah,
                        'harga' => 0,
                        'subtotal' => 0,
                        'item' => [
                            'kode' => $rbp->bonus->kode,
                            'nama' => $rbp->bonus->nama,
                            'nama_pendek' => $rbp->bonus->nama_pendek,
                            'satuan_pembelians' => $satuan_pembelians
                        ]
                    ]);
                }
            }

            $kasir_eceran = '';
            $kasir_grosir = '';
            if ($transaksi_penjualan->user_id != null) {
                $temp_nama = explode(' ', $transaksi_penjualan->user->nama);
                for ($a = 0; $a < sizeof($temp_nama); $a++) {
                    if ($a == 0) {
                        $kasir_eceran .= $temp_nama[$a].' ';
                    } else {
                        $kasir_eceran .= $temp_nama[$a][0];
                    }

                }
                // $kasir_eceran = ucfirst($transaksi_penjualan->user->nama);
            }
            if ($transaksi_penjualan->grosir_id != null) {
                // $kasir_grosir = ucfirst($transaksi_penjualan->grosir->nama);
                $temp_nama = explode(' ', $transaksi_penjualan->grosir->nama);
                for ($a = 0; $a < sizeof($temp_nama); $a++) {
                    if ($a == 0) {
                        $kasir_grosir .= $temp_nama[$a].' ';
                    } else {
                        $kasir_grosir .= $temp_nama[$a][0];
                    }

                }
            }
            if ($kasir_eceran == '') $kasir_eceran = $kasir_grosir;

            $created_at = $transaksi_penjualan->created_at;
            $tanggal = explode(' ', $created_at)[0];
            $waktu = explode(' ', $created_at)[1];
            $tanggals = explode('-', $tanggal);
            $yyyy = $tanggals[0];
            $mm = $tanggals[1];
            $dd = $tanggals[2];
            $tanggal = $dd.'/'.$mm.'/'.$yyyy;
            $transaksi_penjualan->timestamp = $tanggal.' '.$waktu;
            // $catatan = Catatan::where('jenis_catatan', 'struk')->first();

            $potongan_penjualan = $transaksi_penjualan->potongan_penjualan;
            $nominal_tunai = $transaksi_penjualan->nominal_tunai;
            if ($nominal_tunai == null) $nominal_tunai = 0;
            $nominal_transfer = $transaksi_penjualan->nominal_transfer;
            if ($nominal_transfer == null) $nominal_transfer = 0;
            $nominal_kartu = $transaksi_penjualan->nominal_kartu;
            if ($nominal_kartu == null) $nominal_kartu = 0;
            $nominal_cek = $transaksi_penjualan->nominal_cek;
            if ($nominal_cek == null) $nominal_cek = 0;
            $nominal_bg = $transaksi_penjualan->nominal_bg;
            if ($nominal_bg == null) $nominal_bg = 0;
            $nominal_titipan = $transaksi_penjualan->nominal_titipan;
            if ($nominal_titipan == null) $nominal_titipan = 0;

            $harga_total = $transaksi_penjualan->nego_total < $transaksi_penjualan->harga_total && $transaksi_penjualan->nego_total > 0 ? $transaksi_penjualan->nego_total : $transaksi_penjualan->harga_total; // harga total setelah nego
            $jumlah_bayar = $nominal_tunai + $nominal_transfer + $nominal_kartu + $nominal_cek + $nominal_bg + $nominal_titipan;
            // $grand_total = $harga_total - $potongan_penjualan;
            // $kembali = $jumlah_bayar + $potongan_penjualan - $harga_total;
            $grand_total = $harga_total - $potongan_penjualan;
            $kembali = $jumlah_bayar + $potongan_penjualan - $harga_total;

            self::simpanCetakNota($transaksi_penjualan_id);

            return view('cetak.eceran', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'potongan_penjualan', 'harga_total', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'catatans'));
        }
    }

    public function grosir($transaksi_penjualan_id)
    {
        // khusus grosir
        // $transaksi_penjualan = TransaksiPenjualan::with('user', 'pelanggan')->where('id', $transaksi_penjualan_id)->whereIn('status', ['grosir', 'vip'])->first();
        $transaksi_penjualan = TransaksiPenjualan::with('user', 'pelanggan')->where('id', $transaksi_penjualan_id)->first();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $data_perusahaan = DataPerusahaan::find(1);
            $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::with('item')->where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::with('bonus')->where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            // return $relasi_transaksi_penjualan;
            /*$temp_relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->get();
            $relasi_transaksi_penjualan = array();
            foreach ($temp_relasi_transaksi_penjualan as $i => $relasi) {
                $satuan_pembelians = array();
                foreach ($relasi->item->satuan_pembelians as $j => $satuan) {
                    array_push($satuan_pembelians, [
                        'konversi' => $satuan->konversi,
                        'satuan' => [
                            'id' => $satuan->satuan->id,
                            'kode' => $satuan->satuan->kode
                        ]
                    ]);
                }

                array_push($relasi_transaksi_penjualan, [
                    'item_kode' => $relasi->item_kode,
                    'jumlah' => $relasi->jumlah,
                    'harga' => $relasi->harga,
                    'subtotal' => $relasi->subtotal,
                    'item' => [
                        'kode' => $relasi->item->kode,
                        'nama' => $relasi->item->nama,
                        'nama_pendek' => $relasi->item->nama_pendek,
                        'satuan_pembelians' => $satuan_pembelians
                    ]
                ]);

                foreach ($relasi->relasi_bonus_penjualan as $j => $rbp) {
                    $satuan_pembelians = array();
                    foreach ($rbp->bonus->satuan_pembelians as $j => $satuan) {
                        array_push($satuan_pembelians, [
                            'konversi' => $satuan->konversi,
                            'satuan' => [
                                'id' => $satuan->satuan->id,
                                'kode' => $satuan->satuan->kode
                            ]
                        ]);
                    }

                    array_push($relasi_transaksi_penjualan, [
                        'item_kode' => $rbp->bonus->kode,
                        'jumlah' => $rbp->jumlah,
                        'harga' => 0,
                        'subtotal' => 0,
                        'item' => [
                            'kode' => $rbp->bonus->kode,
                            'nama' => $rbp->bonus->nama,
                            'nama_pendek' => $rbp->bonus->nama_pendek,
                            'satuan_pembelians' => $satuan_pembelians
                        ]
                    ]);
                }
            }*/

            $catatans = Catatan::orderBy('id', 'asc')->get();
            $hargas = Harga::orderBy('jumlah', 'desc')->get();

            foreach ($hargas as $i => $harga) {
                $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                if ($relasi_satuan == null) {
                   return [$harga->item_kode, $harga->satuan_id, $harga->id];
                }
                $harga->konversi = $relasi_satuan->konversi;
            }

            $created_at = $transaksi_penjualan->created_at;
            $tanggal = explode(' ', $created_at)[0];
            $waktu = explode(' ', $created_at)[1];
            $tanggals = explode('-', $tanggal);
            $yyyy = $tanggals[0];
            $mm = $tanggals[1];
            $dd = $tanggals[2];
            $tanggal = $dd.'/'.$mm.'/'.$yyyy;
            $transaksi_penjualan->timestamp = $tanggal.' '.$waktu;

            $potongan_penjualan = $transaksi_penjualan->potongan_penjualan;
            $ongkos_kirim = $transaksi_penjualan->ongkos_kirim;
            if ($ongkos_kirim == null) $ongkos_kirim = 0;
            $nominal_tunai = $transaksi_penjualan->nominal_tunai;
            if ($nominal_tunai == null) $nominal_tunai = 0;
            $nominal_transfer = $transaksi_penjualan->nominal_transfer;
            if ($nominal_transfer == null) $nominal_transfer = 0;
            $nominal_kartu = $transaksi_penjualan->nominal_kartu;
            if ($nominal_kartu == null) $nominal_kartu = 0;
            $nominal_cek = $transaksi_penjualan->nominal_cek;
            if ($nominal_cek == null) $nominal_cek = 0;
            $nominal_bg = $transaksi_penjualan->nominal_bg;
            if ($nominal_bg == null) $nominal_bg = 0;
            $nominal_titipan = $transaksi_penjualan->nominal_titipan;
            if ($nominal_titipan == null) $nominal_titipan = 0;

            // $harga_total = $transaksi_penjualan->nego_total < $transaksi_penjualan->harga_total && $transaksi_penjualan->nego_total > 0 ? $transaksi_penjualan->nego_total : $transaksi_penjualan->harga_total; // harga total setelah nego
            // $jumlah_bayar = $nominal_tunai + $nominal_transfer + $nominal_kartu + $nominal_cek + $nominal_bg + $nominal_titipan;
            // $grand_total = $harga_total - $potongan_penjualan + $ongkos_kirim;
            // $kembali = $jumlah_bayar + $potongan_penjualan - $harga_total;

            $harga_total = $transaksi_penjualan->harga_total;
            $jumlah_bayar = $nominal_tunai + $nominal_transfer + $nominal_kartu + $nominal_cek + $nominal_bg + $nominal_titipan;
            $grand_total = $harga_total - $potongan_penjualan + $ongkos_kirim;
            $kembali = $jumlah_bayar - $grand_total;
            // return [$jumlah_bayar, $grand_total, $harga_total, $kembali];
            $kasir_eceran = null;
            $kasir_grosir = null;
            if ($transaksi_penjualan->user_id != null) {
                $kasir_eceran = ucfirst($transaksi_penjualan->user->nama);
            }
            if ($transaksi_penjualan->grosir_id != null) {
                $kasir_grosir = ucfirst($transaksi_penjualan->grosir->nama);
            }
            if ($kasir_eceran == null) $kasir_eceran = $kasir_grosir;

            if ($transaksi_penjualan->pelanggan != null) $transaksi_penjualan->pelanggan->nama = ucfirst($transaksi_penjualan->pelanggan->nama);
            // if ($transaksi_penjualan->pengirim == null) $transaksi_penjualan->pengirim = '&nbsp;';

            // $kembali = 0;
            // if ($transaksi_penjualan->jumlah_bayar != null && $transaksi_penjualan->jumlah_bayar > 0) {
            //     $kembali = $transaksi_penjualan->jumlah_bayar - ($transaksi_penjualan->nego_total>0?$transaksi_penjualan->nego_total:$transaksi_penjualan->harga_total) + $transaksi_penjualan->potongan_penjualan - $transaksi_penjualan->ongkos_kirim;
            // }
            // return [$kasir_eceran, $kasir_grosir];

            // if ($data_perusahaan->tanggal_npwp == '0000-00-00') {
            //     $data_perusahaan->tanggal_npwp = '--/--/----';
            // }
            // if ($data_perusahaan->tanggal_pkp == '0000-00-00') {
            //     $data_perusahaan->tanggal_pkp = '--/--/----';
            // }

            self::simpanCetakNota($transaksi_penjualan_id);

            // $kurang = $grand_total - $transaksi_penjualan->jumlah_bayar;
            $kurang = $grand_total - $transaksi_penjualan->jumlah_bayar;
            // return view('cetak.faktur_debit', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir'));
            // return view('cetak.faktur_debit_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir'));
            // return view('cetak.faktur_kredit', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir', 'kurang'));
            // return view('cetak.faktur_kredit_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir', 'kurang'));
            
            // return $transaksi_penjualan->pengirim;
            if($transaksi_penjualan->jumlah_bayar > 0 && $transaksi_penjualan->jumlah_bayar >= $grand_total){
                if ($transaksi_penjualan->pengirim == null){
                    // return 'aw';
                    return view('cetak.faktur_debit', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir'));
                } else {
                    // return 'bw';
                    return view('cetak.faktur_debit_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir'));
                }
            }else{
                // $kurang = $grand_total - $transaksi_penjualan->jumlah_bayar;
                // return $grand_total - $transaksi_penjualan->jumlah_bayar;
                if ($transaksi_penjualan->pengirim == null){
                    // return 'cw';
                    return view('cetak.faktur_kredit', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir', 'kurang'));
                } else {
                    // return 'dw';
                    return view('cetak.faktur_kredit_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali', 'kasir_eceran', 'kasir_grosir', 'kurang'));
                }
            }

            /*if ($transaksi_penjualan->jumlah_bayar <= 0 || $kembali < 0) {
                // Belum lunas
                // Jika belum lunas cetak FAKTUR
                if ($transaksi_penjualan->pengirim == null) {
                    return view('cetak.faktur_debit', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali'));
                } else {
                    return view('cetak.faktur_debit_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali'));
                }
            } else {
                // Lunas
                // Jika lunas cetak INVOICE
                if ($transaksi_penjualan->pengirim == null) {
                    return view('cetak.invoice', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali'));
                } else {
                    return view('cetak.invoice_dikirim', compact('data_perusahaan', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'catatans', 'hargas', 'potongan_penjualan', 'grand_total', 'jumlah_bayar', 'kembali'));
                }
            }*/
        }
    }

    public function po_pembelian($transaksi_pembelian_id)
    {
        $transaksi_pembelian = TransaksiPembelian::find($transaksi_pembelian_id);
        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $transaksi_pembelian_id)->get();

        $items = array();
        $hasil = array();
        foreach ($relasi_transaksi_pembelian as $i => $relasi) {
            $items[$i] = Item::find($relasi->item_id)->kode;
            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$i])->orderBy('konversi', 'dsc')->get();
            $sisa_jumlah[$i] = $relasi->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                if($hitung > 0){
                    $a = $a + 1;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }
        
        $catatans = Catatan::where('jenis_catatan', 'po_beli')->get();

        // return $hasil[0];
        return view('cetak.po_pembelian', compact('transaksi_pembelian', 'relasi_transaksi_pembelian', 'hasil', 'catatans'));
    }

    private function simpanCetakNota($transaksi_penjualan_id)
    {
        $transaksi_penjualan = TransaksiPenjualan::find($transaksi_penjualan_id);
        try {
            $cetak_nota = CetakNota::create([
                'transaksi_penjualan_id' => $transaksi_penjualan_id,
                'cetak' => 1,
                'user_id' => Auth::user()->id
            ]);

            // return response()->json(['error' => false, 'cetak_nota']);
        } catch (\Exception $e) {
            // return response()->json(['error' => true, 'error_' => $e]);
        }
    }

    public function titipan($id)
    {
        $log = PembayaranDeposito::find($id);
        $log->cetak += 1;
        $log->update();

        $pelanggan = pelanggan::find($log->pelanggan_id);
        $pembayarans = [];
        $catatans = Catatan::where('jenis_catatan', 'deposito')->get();
        $data_perusahaan = DataPerusahaan::find(1);
        $operator1 = $log->user->nama;
        $operator2 = $log->grosir->nama;

        $created_at = $log->created_at;
        $tanggal = explode(' ', $created_at)[0];
        $waktu = explode(' ', $created_at)[1];
        $tanggals = explode('-', $tanggal);
        $yyyy = $tanggals[0];
        $mm = $tanggals[1];
        $dd = $tanggals[2];
        $tanggal = $dd.'/'.$mm.'/'.$yyyy;
        $log->timestamp = $tanggal.' '.$waktu;
        $log->total_deposito = \App\Util::duit0($log->jumlah_bayar);
        $deposito_sebelum = $pelanggan->titipan - $log->jumlah_bayar;
        if($deposito_sebelum < 0) $deposito_sebelum = 0;
        $log->deposito_sebelum = \App\Util::duit0($deposito_sebelum);

        if ($log->nominal_tunai > 0) {
            array_push($pembayarans, [
                'id' => $log->id,
                'created_at' => $log->created_at,
                'jumlah' => \App\Util::duit0($log->nominal_tunai).',-',
                'metode' => 'Tunai',
                'bank' => '',
                'nomor' => '',
                'jenis' => ''
            ]);
        }

        if ($log->nominal_transfer > 0) {
            array_push($pembayarans, [
                'id' => $log->id,
                'created_at' => $log->created_at,
                'jumlah' => \App\Util::duit0($log->nominal_transfer).',-',
                'metode' => 'Transfer',
                'bank' => $log->banktransfer->nama_bank,
                'nomor' => $log->no_transfer,
                'jenis' => ''
            ]);
        }

        if ($log->nominal_kartu > 0) {
            array_push($pembayarans, [
                'id' => $log->id,
                'created_at' => $log->created_at,
                'jumlah' => \App\Util::duit0($log->nominal_kartu).',-',
                'metode' => 'Kartu',
                'bank' => $log->bankkartu->nama_bank,
                'nomor' => $log->no_kartu,
                'jenis' => $log->jenis_kartu
            ]);
        }

        if ($log->nominal_cek > 0) {
            array_push($pembayarans, [
                'id' => $log->id,
                'created_at' => $log->created_at,
                'jumlah' => \App\Util::duit0($log->nominal_cek).',-',
                'nomor' => $log->no_cek,
                'metode' => 'Cek',
                'bank' => '',
                'jenis' => ''
            ]);
        }

        if ($log->nominal_bg > 0) {
            array_push($pembayarans, [
                'id' => $log->id,
                'created_at' => $log->created_at,
                'jumlah' => \App\Util::duit0($log->nominal_bg).',-',
                'nomor' => $log->no_bg,
                'metode' => 'BG',
                'bank' => '',
                'jenis' => ''
            ]);
        }

        return view('cetak.titipan', compact('log', 'pelanggan', 'pembayarans', 'catatans', 'data_perusahaan', 'operator1', 'operator2'));
    }

    public function retur($transaksi_penjualan_id, $retur_penjualan_id)
    {
        $data_perusahaan = DataPerusahaan::find(1);
        $catatans = Catatan::where('jenis_catatan', 'retur')->get();

        $bayar = 0;
        $retur_penjualan = ReturPenjualan::find($retur_penjualan_id);
        $retur_penjualan->cetak += 1;
        $retur_penjualan->update();
        
        $transaksi_penjualan = TransaksiPenjualan::find($transaksi_penjualan_id);
        $relasi_retur_penjualan = RelasiReturPenjualan::where('retur_penjualan_id', $retur_penjualan_id)->get();
        if($retur_penjualan->total_uang_keluar > 0){
            $bayar = 1;
            $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
        }elseif($retur_penjualan->total_uang_masuk > 0){
            $bayar = 1;
            $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
        }

        $items = array();
        $hasil = array();
        foreach ($relasi_retur_penjualan as $i => $data) {
            $satuans[$i] = RelasiSatuan::where('item_kode', $data->item_kode)->orderBy('konversi', 'dsc')->get();
            $sisa_jumlah[$i] = $data->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                if($hitung > 0){
                    $a = $a + 1;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                    $hasil[$i]['harga'] = $data->subtotal / $hitung;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }

        // return [$retur_penjualan, $transaksi_penjualan, $relasi_retur_penjualan, $hasil, $pembayaran, $bayar];
        $kasir = '';
        if ($retur_penjualan->user_id != null) {
            $temp_nama = explode(' ', $retur_penjualan->user->nama);
            for ($a = 0; $a < sizeof($temp_nama); $a++) {
                if ($a == 0) {
                    $kasir .= $temp_nama[$a].' ';
                } else {
                    $kasir .= $temp_nama[$a][0];
                }
            }
        }

        $created_at = $retur_penjualan->created_at;
        $tanggal = explode(' ', $created_at)[0];
        $waktu = explode(' ', $created_at)[1];
        $tanggals = explode('-', $tanggal);
        $yyyy = $tanggals[0];
        $mm = $tanggals[1];
        $dd = $tanggals[2];
        $tanggal = $dd.'/'.$mm.'/'.$yyyy;
        $retur_penjualan->timestamp = $tanggal.' '.$waktu;


        $subtotal = $retur_penjualan->harga_total;
        $subtotal_keluar = $retur_penjualan->total_uang_keluar;
        $kekurangan = $subtotal_keluar - $subtotal;
        $kembali = $retur_penjualan->total_uang_masuk - $kekurangan;

        $jumlah_bayar = 0;
        if($retur_penjualan->status == 'lain') {
            $jumlah_bayar = $retur_penjualan->total_uang_masuk;
        } else {
            $kekurangan = 0;
            $kembali = 0;
        }

        // self::simpanCetakNota($transaksi_penjualan_id);
        // return 'aw';
        return view('cetak.retur', compact('retur_penjualan', 'transaksi_penjualan', 'relasi_retur_penjualan', 'hasil', 'pembayaran', 'bayar', 'catatans', 'subtotal', 'subtotal_keluar', 'kekurangan', 'kembali', 'kasir', 'jumlah_bayar'));
        // }
    }
}
