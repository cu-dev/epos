<?php

namespace App\Http\Controllers;

use App\Item;
use App\Util;
// use App\Bonus;

use Illuminate\Http\Request;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // $bonuses = Bonus::all();
        $bonuses = Item::where('bonus', 1)->get();
        return view('bonus.index', compact('bonuses'));
    }

    public function store(Request $request) {
        // $this->validate($request,[
        //     'kode' => 'required|max:255',
        //     'nama' => 'required|max:255',
        //     'stok' => 'required|max:255'
        // ]);

        // $bonus = new Bonus();
        // $bonus = new Item();
        // $bonus->bonus = 1;
        // $bonus->kode = $request->kode;
        // $bonus->nama = $request->nama;
        // $bonus->stok = $request->stok;
        // if ($bonus->save()) {
        //     return redirect('/bonus')->with('sukses', 'tambah');
        // } else {
        //     return redirect('/bonus')->with('gagal', 'tambah');
        // }

        $item = new Item();
        $item->bonus =  1;
        $item->kode = strtoupper($request->kode);
        $item->nama = strtoupper($request->nama);
        $item->nama_pendek = strtoupper($request->nama_pendek);
        $item->suplier_id = 1;
        $item->jenis_item_id = 1;

        if ($item->save()) {
            $kode_barang = 'KMK-';
            $kode_barang .= $item->jenis_item->kode . '-';
            $kode_barang .= Util::int4digit($item->id);

            if ($item->kode == '') $item->kode = $kode_barang;
            $item->kode_barang = $kode_barang;
            $item->update();

            if ($request->konsinyasi == 2) {
                $relasi_satuan = new RelasiSatuan();
                $relasi_satuan->item_kode = $request->kode;
                $relasi_satuan->satuan_id = 1;
                $relasi_satuan->konversi = 1;
                $relasi_satuan->save();

                $relasi_harga = new Harga();
                $relasi_harga->item_kode = $request->kode;
                $relasi_harga->jumlah = 1;
                $relasi_harga->satuan_id = 1;
                $relasi_harga->eceran = 0;
                $relasi_harga->grosir = 0;
                $relasi_harga->save();
            }
            return redirect('/bonus')->with('sukses', 'tambah');
        } else {
            return redirect('/bonus')->with('gagal', 'tambah');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255',
            'stok' => 'required|max:255'
        ]);

        // $bonus = Bonus::find($id);
        $bonus->kode = $request->kode;
        $bonus->nama = $request->nama;
        $bonus->stok = $request->stok;
        if ($bonus->save()) {
            return redirect('/bonus')->with('sukses', 'ubah');
        } else {
            return redirect('/bonus')->with('gagal', 'ubah');
        }
    }

    public function destroy($id)
    {
        // $bonus = Bonus::find($id);
        try {
            if ($bonus->delete()) {
                return redirect('/bonus')->with('sukses', 'hapus');
            } else {
                return redirect('/bonus')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/bonus')->with('gagal', 'hapus');
        }
    }
}
