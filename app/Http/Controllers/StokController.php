<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\User;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Pelanggan;
use App\CashDrawer;
use App\MoneyLimit;
use App\RelasiBonus;
use App\RelasiSatuan;
use App\PiutangDagang;
use App\ReturPenjualan;
use App\TransaksiPembelian;
use App\RelasiBonusPenjualan;
use App\RelasiReturPenjualan;
use App\RelasiTransaksiPenjualan;
class StokController extends Controller
{
    public function itemJson($kode)
    {
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->get();
        $stok_pakai = array();
        foreach ($stoks as $key => $stok) {
            $transaksi = TransaksiPembelian::find($stok->transaksi_pembelian_id)->kode_transaksi;
            $stok_pakai[$key]['id'] = $stok->id;
            $stok_pakai[$key]['transaksi_pembelian_id'] = $stok->transaksi_pembelian_id;
            $stok_pakai[$key]['kode_transaksi'] = $transaksi;
            $stok_pakai[$key]['jumlah'] = $stok->jumlah;
            $stok_pakai[$key]['rusak'] = $stok->rusak;
            $stok_pakai[$key]['kadaluarsa'] = $stok->kadaluarsa;
            $stok_pakai[$key]['keterangan'] = $stok->keterangan;
        }
        $item = Item::where('kode', $kode)->get()->first();
        return response()->json(compact('item', 'stoks', 'stok_pakai'));
        // return response()->json(['stoks' => $stoks, 'satuans' => $satuans, 'item' => $item]);
    }

    public function index()
    {
        $items = Item::distinct()->select('kode', 'nama', 'stoktotal')->where('stoktotal', '>', 0)->get();
        return view('stok.index', compact('items'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        for($i=0; $i<count($request->stok_id); $i++){
            $stok = Stok::find($request->stok_id[$i]);

            if ($request->jumlah[$i] > 0) {
                $stok->jumlah -= $request->jumlah[$i];
                $stok->update();

                $new = new Stok();
                $new->item_id = $stok->item_id;
                $new->transaksi_pembelian_id = $stok->transaksi_pembelian_id;
                $new->item_kode = $stok->item_kode;
                $new->kadaluarsa = \App\Util::date($request->kadaluarsa[$i]);
                $new->jumlah = $request->jumlah[$i];
                $new->rusak = 1;
                $new->keterangan = $request->keterangan[$i];
                $new->save();
            } else {
                $stok->kadaluarsa = \App\Util::date($request->kadaluarsa[$i]);
                $stok->update();
            }
        }

        return redirect('/penyesuaian_stok')->with('sukses', 'tambah');
    }
}
