<?php

namespace App\Http\Controllers;

use App\BG;
use App\Cek;
use App\Akun;
use App\Bank;
use App\Jurnal;
use App\LogTransferBank;
use Illuminate\Http\Request;

class KasController extends Controller
{
    public function lastJson()
    {
        $kas = Jurnal::where('referensi', 'like', '%%%%/TFK/%%%%%%%')
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['kas' => $kas]);
    }

    public function cekJson($id)
    {
        $cek = Cek::find($id);
        return response()->json(['cek' => $cek]);
    }

    public function bgJson($id)
    {
        $bg = BG::find($id);
        return response()->json(['bg' => $bg]);
    }

    public function index()
    {
        $kases = Akun::where('kode', 'like',  '111%%%')->get();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', '=', '1')->get();
        $BGs = BG::where('aktif', '=', '1')->get();
        $non_ceks = Cek::where('aktif', '=', '0')->get();
        $non_BGs = BG::where('aktif', '=', '0')->get();
        // return $kases;
        return view('kas.index', compact('kases', 'banks', 'ceks', 'BGs', 'non_ceks', 'non_BGs'));
    }

   public function transfer(Request $request)
   {
        // $this->validate($request, [
        //     'nominal' => 'required|min:1',
        // ]);
        
        // return $request->all();
        $sumber = new Jurnal();
        $tujuan = new Jurnal();
        $akun_sumber = Akun::where('kode', $request->sumber_kas)->first();
        $akun_tujuan = Akun::where('kode', $request->kas_tujuan)->first();

        //ngurangi akun sumber
        $akun_sumber->debet -= $request->nominal;
        $akun_sumber->update();

        //nambahi akun sumber
        $akun_tujuan->debet += $request->nominal;
        $akun_tujuan->update();

        //jurnal kas tujuan
        $tujuan->kode_akun = $request->kas_tujuan;
        $tujuan->referensi = $request->kode_transaksi;
        $tujuan->debet = $request->nominal;
        $tujuan->save();

        //jurnal kas sumber
        $sumber->kode_akun = $request->sumber_kas;
        $sumber->referensi = $request->kode_transaksi;
        $sumber->kredit = $request->nominal;
        $sumber->save();

        if ($request->sumber_kas == Akun::KasBank) {
            $bank = Bank::find($request->bank_asal);
            $bank->nominal -= $request->nominal;
            $bank->update();
        } else if ($request->sumber_kas == Akun::KasCek) {
            $cek = Cek::find($request->cek_asal);
            $cek->aktif = 0;
            $cek->update();
        } else if ($request->sumber_kas == Akun::KasBG) {
            $bg = BG::find($request->bg_asal);
            $bg->aktif = 0;
            $bg->update();
        }

        if ($request->kas_tujuan == Akun::KasBank) {
            $bank_t = Bank::find($request->bank_tujuan);
            $bank_t->nominal += $request->nominal;
            $bank_t->update();
        } else if ($request->kas_tujuan == Akun::KasCek) {
            $kas_cek = new Cek();
            $kas_cek->nomor = $request->no_cek;
            $kas_cek->nominal = $request->nominal;
            $kas_cek->aktif = 1;
            $kas_cek->save();
        } else if ($request->kas_tujuan == Akun::KasBG) {
            $kas_bg = new BG();
            $kas_bg->nomor = $request->no_bg;
            $kas_bg->nominal = $request->nominal;
            $kas_bg->aktif = 1;
            $kas_bg->save();
        }

        return redirect('/kas')->with('sukses', 'tambah');
   }
}
