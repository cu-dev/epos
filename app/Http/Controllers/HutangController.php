<?php

namespace App\Http\Controllers;

use DB;
use Auth;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Laci;
use App\Util;
use App\Hutang;
use App\Jurnal;
use App\Suplier;
use App\LogLaci;
use App\TransaksiPembelian;

use Illuminate\Http\Request;

class HutangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $hutangs = TransaksiPembelian::where('status_hutang', 1)->where('sisa_utang', '>', 0)->orderBy('created_at', 'desc')->get();
        // return $hutangs[0]->hutangs;
        $lunas_hutangs = TransaksiPembelian::where('status_hutang', 0)->orderBy('created_at', 'desc')->get();

        foreach ($hutangs as $i => $hutang) {
            foreach ($hutang->hutangs as $j => $pembayaran) {
                // return $pembayaran->lunas_cek;
                if($pembayaran->lunas_cek != null && $pembayaran->lunas_cek == 0) $hutang->cek = 1;
                if($pembayaran->lunas_bg != null && $pembayaran->lunas_bg == 0) $hutang->bg = 1;
            }
        }

        // return $hutangs;

        return view('hutang_transaksi.index', compact('hutangs', 'lunas_hutangs'));
    } */

    /* public function index()
    {
        $hutangs = TransaksiPembelian::where('status_hutang', 1)->where('sisa_utang', '>', 0)->orderBy('created_at', 'desc')->get();
        // return $hutangs[0]->hutangs;
        $lunas_hutangs = TransaksiPembelian::where('status_hutang', 0)->orderBy('created_at', 'desc')->get();

        foreach ($hutangs as $i => $hutang) {
            foreach ($hutang->hutangs as $j => $pembayaran) {
                // return $pembayaran->lunas_cek;
                if($pembayaran->lunas_cek != null && $pembayaran->lunas_cek == 0) $hutang->cek = 1;
                if($pembayaran->lunas_bg != null && $pembayaran->lunas_bg == 0) $hutang->bg = 1;
            }
        }

        // return $hutangs;

        return view('hutang_transaksi.index', compact('hutangs', 'lunas_hutangs'));
    } */

    public function index()
    {
        return view('hutang_transaksi.index');
    }

    public function mdt1(Request $request)
    {
        $hutangs = TransaksiPembelian
            ::where('po', 0)
            ->where('status_hutang', 1)
            ->where('sisa_utang', '>', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = TransaksiPembelian
            ::where('po', 0)
            ->where('status_hutang', 1)
            ->where('sisa_utang', '>', 0)
            ->count();

        foreach ($hutangs as $i => $hutang) {
            $hutang->utang = Util::ewon($hutang->utang);
            $hutang->sisa_utang = Util::ewon($hutang->sisa_utang);

            $buttons['bayar'] = ['url' => url('hutang/show/'.$hutang->id)];

            if ($hutang->cek == 1) {
                $buttons['cek'] = ['url' => ''];
            } else {
                $buttons['cek'] = null;
            }

            if ($hutang->bg == 1) {
                $buttons['bg'] = ['url' => ''];
            } else {
                $buttons['bg'] = null;
            }

            // return $buttons;
            $hutang->buttons = $buttons;

            foreach ($hutang->hutangs as $j => $pembayaran) {
                // return $pembayaran->lunas_cek;
                if ($pembayaran->lunas_cek != null && $pembayaran->lunas_cek == 0) $hutang->cek = 1;
                if ($pembayaran->lunas_bg != null && $pembayaran->lunas_bg == 0) $hutang->bg = 1;
            }
        }

        // return [$count, $hutangs];

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $hutangs,
        ]);
    }

    public function mdt2(Request $request)
    {
        $hutangs = TransaksiPembelian
            ::where('po', 0)
            ->where('status_hutang', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = TransaksiPembelian
            ::where('po', 0)
            ->where('status_hutang', 0)
            ->count();

        foreach ($hutangs as $i => $hutang) {
            $hutang->utang = Util::ewon($hutang->utang);
            $hutang->sisa_utang = Util::ewon($hutang->sisa_utang);

            $buttons['log'] = ['url' => url('hutang/show/'.$hutang->id)];

            // return $buttons;
            $hutang->buttons = $buttons;

            foreach ($hutang->hutangs as $j => $pembayaran) {
                // return $pembayaran->lunas_cek;
                if ($pembayaran->lunas_cek != null && $pembayaran->lunas_cek == 0) $hutang->cek = 1;
                if ($pembayaran->lunas_bg != null && $pembayaran->lunas_bg == 0) $hutang->bg = 1;
            }
        }

        // return [$count, $hutangs];

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $hutangs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $akun_kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $akun_hutang_dagang = Akun::where('kode', Akun::HutangDagang)->first();

        $transaksi_pembelian = TransaksiPembelian::find($request->id_transaksi);
        $transaksi_pembelian->sisa_utang = $transaksi_pembelian->sisa_utang - $request->jumlah_bayar + $request->nominal_cek + $request->nominal_bg;

        $selisih_utang = $transaksi_pembelian->sisa_utang;
        // return $selisih_utang;
        if ($transaksi_pembelian->sisa_utang == 0 
            || ($transaksi_pembelian->sisa_utang < 100 && $transaksi_pembelian->sisa_utang >= 0) 
            || ($transaksi_pembelian->sisa_utang <= 0 && $transaksi_pembelian->sisa_utang > -100 )) {
            $transaksi_pembelian->status_hutang = 0;
            $transaksi_pembelian->sisa_utang = 0;
            $request->jumlah_bayar += $selisih_utang;
        }

        $log_hutang = new Hutang();
        $log_hutang->transaksi_pembelian_id = $request->id_transaksi;
        $log_hutang->jumlah_bayar = $request->jumlah_bayar;
        $log_hutang->user_id = Auth::user()->id;

        $keterangan = 'Pembayaran Hutang Dagang Transaksi ';
        $akun_hutang_dagang->kredit -= $request->jumlah_bayar;
        $akun_hutang_dagang->update();
        Jurnal::create([
            'kode_akun' => $akun_hutang_dagang->kode,
            'referensi' => $request->kode_transaksi.'/H',
            'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
            'debet' => $request->jumlah_bayar
        ]);

        if($selisih_utang < 100 && $selisih_utang >= 0){
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                // 'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $transaksi_pembelian->kode_transaksi.'/H',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
                'kredit' => $selisih_utang
            ]);
        }elseif($selisih_utang <= 0 && $selisih_utang > -100 ){
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                // 'kode_akun' => Akun::PendapatanLain,
                'referensi' => $transaksi_pembelian->kode_transaksi.'/H',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
                'debet' => $selisih_utang * -1
            ]);
        }
        // TUNAI
        if ($request->nominal_tunai > 0) {
            $log_hutang->nominal_tunai = $request->nominal_tunai;
            $akun_kas_tunai->debet -= $request->nominal_tunai;

            Jurnal::create([
                'kode_akun' => $akun_kas_tunai->kode,
                'referensi' => $request->kode_transaksi.'/H',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
                'kredit' => $request->nominal_tunai
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $request->nominal_tunai,
            ]);

            if(Auth::user()->level_id == 5){
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 7;
                $log->nominal = $request->nominal_tunai;
                $log->save();

                $laci = Laci::find(1);
                $laci->gudang -= $request->nominal_tunai;
                $laci->update();
            }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
            {
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 7;
                $log->nominal = $request->nominal_tunai;
                $log->save();

                $laci = Laci::find(1);
                $laci->owner -= $request->nominal_tunai;
                $laci->update();
            }
        }

        // TRANSER DAN KARTU
        if ($request->nominal_transfer > 0 || $request->nominal_kartu > 0) {
            $keterangan_bank = $keterangan . $transaksi_pembelian->kode_transaksi;
            $nominal_bank = 0;
            if ($request->nominal_transfer > 0) {
                $log_hutang->no_transfer = $request->no_transfer;
                $log_hutang->bank_transfer = $request->bank_transfer;
                $log_hutang->nominal_transfer = $request->nominal_transfer;
                $akun_kas_bank->debet -= $request->nominal_transfer;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal -= $request->nominal_transfer;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - '.$request->nominal_transfer.' via '.$bank->nama_bank;
                $nominal_bank += $request->nominal_transfer;
            }

            if ($request->nominal_kartu > 0) {
                // $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;
                $log_hutang->no_kartu = $request->no_kartu;
                $log_hutang->bank_kartu = $request->bank_kartu;
                $log_hutang->jenis_kartu = $request->jenis_kartu;
                $log_hutang->nominal_kartu = $request->nominal_kartu;
                $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                if ($request->jenis_kartu == 'kredit') {
                    $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                    $akun_kartu->kredit += $request->nominal_kartu;
                    $akun_kartu->update();

                    $bank = Bank::find($request->bank_kartu);
                    $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                    Jurnal::create([
                        'kode_akun' => Akun::KartuKredit,
                        'referensi' => $request->kode_transaksi.'/H',
                        'keterangan' => $keterangan_bank,
                        'kredit' => $request->nominal_kartu
                    ]);
                } else {
                    $akun_kas_bank->debet -= $request->nominal_kartu;

                    $bank = Bank::find($request->bank_kartu);
                    $bank->nominal -= $request->nominal_kartu;
                    $bank->update();

                    $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                    $nominal_bank += $request->nominal_kartu;
                }
            }

            Jurnal::create([
                'kode_akun' => $akun_kas_bank->kode,
                'referensi' => $request->kode_transaksi.'/H',
                'keterangan' => $keterangan_bank,
                'kredit' => $nominal_bank
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $nominal_bank,
            ]);
        }

        // CEK
        if ($request->nominal_cek > 0) {
            if ($request->bank_cek == NULL) {
                // Pilih cek
                $log_hutang->no_cek = $request->no_cek;
                $log_hutang->nominal_cek = $request->nominal_cek;
                $akun_kas_cek->debet -= $request->nominal_cek;

                $cek = Cek::find($request->cek_id);
                $cek->aktif = 0;
                $cek->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_cek->kode,
                    'referensi' => $request->kode_transaksi.'/H',
                    'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
                    'kredit' => $request->nominal_cek
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $request->nominal_cek,
                ]);
            } else {
                // Cek baru
                $log_hutang->no_cek = $request->no_cek;
                $log_hutang->bank_cek = $request->bank_cek;
                $log_hutang->nominal_cek = $request->nominal_cek;
                $log_hutang->lunas_cek = 0;

                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $request->nominal_cek;
                $akun->update();

                $cek = new Cek();
                $cek->nomor = $request->no_cek;
                $cek->nominal = $request->nominal_cek;
                $cek->aktif = 0;
                $cek->save();

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $request->kode_transaksi.'/H',
                    'keterangan' => $keterangan.$transaksi_pembelian->kode_transaksi.' - via Cek '.$bank->nama_bank,
                    'kredit' => $request->nominal_cek
                ]);
            }
        }

        // BG
        if ($request->nominal_bg > 0) {
            if ($request->bank_bg == NULL) {
                // Pilih BG
                $log_hutang->no_bg = $request->no_bg;
                $log_hutang->nominal_bg = $request->nominal_bg;
                $akun_kas_bg->debet -= $request->nominal_bg;

                $bg = BG::find($request->bg_id);
                $bg->aktif = 0;
                $bg->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_bg->kode,
                    'referensi' => $request->kode_transaksi.'/H',
                    'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi,
                    'kredit' => $request->nominal_bg
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $request->nominal_bg,
                ]);
            } else {
                // BG Baru
                $log_hutang->no_bg = $request->no_bg;
                $log_hutang->bank_bg = $request->bank_bg;
                $log_hutang->nominal_bg = $request->nominal_bg;
                $log_hutang->lunas_bg = 0;

                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $request->nominal_bg;
                $akun->update();

                $bg = new BG();
                $bg->nomor = $request->no_bg;
                $bg->nominal = $request->nominal_bg;
                $bg->aktif = 0;
                $bg->save();

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $request->kode_transaksi.'/H',
                    'keterangan' => $keterangan.$transaksi_pembelian->kode_transaksi.' - via BG '.$bank->nama_bank,
                    'kredit' => $request->nominal_bg
                ]);
            }
        }

        if($selisih_utang <= 0 && $selisih_utang > -100){
            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_utang;
            $akun_laba_tahan->update();

            //jurnal laba rugi
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian->kode_transaksi.' L/R-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'debet' => $selisih_utang * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $transaksi_pembelian->kode_transaksi.' L/R-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'kredit' => $selisih_utang * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian->kode_transaksi.' LDT-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'debet' => $selisih_utang * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian->kode_transaksi.' LDT-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'kredit' => $selisih_utang * -1
            ]);
        }elseif($selisih_utang < 100 && $selisih_utang >= 0){
            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_utang;
            $akun_laba_tahan->update();

            //jurnal laba rugi
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $transaksi_pembelian->kode_transaksi.' L/R-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'debet' => $selisih_utang
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian->kode_transaksi.' L/R-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'kredit' => $selisih_utang
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian->kode_transaksi.' LDT-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'debet' => $selisih_utang
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian->kode_transaksi.' LDT-SB',
                'keterangan' => $keterangan . $transaksi_pembelian->kode_transaksi.' - Selisih Bayar',
                'kredit' => $selisih_utang
            ]);
        }

        // Update semua akun kas
        $log_hutang->save();
        $akun_kas_tunai->update();
        $akun_kas_bank->update();
        $akun_kas_cek->update();
        $akun_kas_bg->update();
        $transaksi_pembelian->update();

        return redirect()->back()->with('sukses', 'bayar');

        // if ($log_hutang->save()) {
        //     return redirect()->back()->with('sukses', 'bayar');
        // } else {
        //     return redirect()->back()->with('gagal', 'bayar');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_pembelian = TransaksiPembelian::find($id);
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();

        $hutangs = array();
        foreach ($transaksi_pembelian->hutangs as $i => $hutang) {
            if ($hutang->nominal_tunai > 0) {
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->nominal_tunai,
                    'metode' => 'tunai',
                    'user' => $hutang->user->nama
                ]);
            }

            if ($hutang->nominal_transfer > 0) {
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->nominal_transfer,
                    'metode' => 'transfer',
                    'user' => $hutang->user->nama
                ]);
            }

            if ($hutang->nominal_kartu > 0) {
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->nominal_kartu,
                    'metode' => 'kartu',
                    'user' => $hutang->user->nama
                ]);
            }

            if ($hutang->nominal_cek > 0) {
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->nominal_cek,
                    'metode' => 'cek',
                    'bank' => $hutang->bank_cek,
                    'lunas' => $hutang->lunas_cek,
                    'user' => $hutang->user->nama
                ]);
            }

            if ($hutang->nominal_bg > 0) {
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->nominal_bg,
                    'metode' => 'bg',
                    'bank' => $hutang->bank_bg,
                    'lunas' => $hutang->lunas_bg,
                    'user' => $hutang->user->nama
                ]);
            }

            if ($hutang->nominal_tunai == null &&$hutang->nominal_kartu == null &&$hutang->nominal_transfer == null &&$hutang->nominal_cek == null &&$hutang->nominal_bg == null &&$hutang->jumlah_bayar > 0){
                array_push($hutangs, [
                    'id' => $hutang->id,
                    'created_at' => $hutang->created_at,
                    'jumlah' => $hutang->jumlah_bayar,
                    'metode' => 'hutang',
                    'user' => $hutang->user->nama
                ]);
            }
        }

        $laci = Laci::find(1);
        return view('hutang_transaksi.show', compact('transaksi_pembelian', 'banks', 'ceks', 'bgs', 'hutangs', 'laci'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hutang_suplier()
    {
        $hutangs = TransaksiPembelian::selectRaw('sum(utang) as total_hutang, sum(sisa_utang) as sisa_hutang, suplier_id')->where('status_hutang', 1)->groupBy('suplier_id')->get();

        return view('hutang_suplier.index', compact('hutangs'));
    }

    public function show_suplier($id) 
    {
        $suplier = Suplier::find($id);
        $log_hutangs = Hutang::select('transaksi_pembelians.id', 
                'transaksi_pembelians.suplier_id',
                'transaksi_pembelians.utang',
                'hutangs.jumlah_bayar',
                'hutangs.created_at')
            ->join('transaksi_pembelians', 'hutangs.transaksi_pembelian_id', '=', 'transaksi_pembelians.id')
            ->where('transaksi_pembelians.suplier_id', $id)
            ->orderBy('transaksi_pembelians.id', 'asc')
            ->get();

        return view('hutang_suplier.show', compact('suplier', 'log_hutangs'));
    }

    public function store_suplier(Request $request)
    {
        $hutangs = TransaksiPembelian::where('suplier_id', $request->suplier_id)->where('status_hutang', 1)->get();
        $bayar = $request->jumlah_bayar;
        // echo $bayar;
        $i=0;
        foreach ($hutangs as $hutang) {
            $log = new Hutang();

            $log->transaksi_pembelian_id = $hutang->id;
            
            // echo $hutang->sisa_utang;

            if ($bayar > 0) {
                if ($bayar >= $hutang->sisa_utang) {
                    $bayar = $bayar-$hutang->sisa_utang;
                    $log->jumlah_bayar = $hutang->sisa_utang;
                    $hutang->sisa_utang = 0;
                    $hutang->status_hutang = 0;
                    $hutang->update();
                } else {
                    $log->jumlah_bayar = $bayar;
                    $hutang->sisa_utang = $hutang->sisa_utang-$bayar;
                    $hutang->update();
                    $bayar = 0;
                }
                $log->metode_pembayaran = $request->metode_pembayaran;
                $log->no_transfer = $request->no_transfer;
                if($log->save()){
                    $i = $i=1;
                }
            }
            if ($i > 0) {
                return redirect()->back()->with('sukses', 'bayar');
            } else {
                return redirect()->back()->with('gagal', 'bayar');
            }
        }
    }

    public function CairCek($id)
    {
        // return $id;
        $hutang = Hutang::find($id);
        $hutang->lunas_cek = 1;
        $hutang->lunas_cek = 1;
        $hutang->update();

        $transaksi_pembelian = TransaksiPembelian::find($hutang->transaksi_pembelian->id);
        $transaksi_pembelian->sisa_utang -= $hutang->nominal_cek;
        $transaksi_pembelian->update();

        $bank = Bank::find($hutang->bank_cek);
        $bank->nominal -= $hutang->nominal_cek;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $hutang->nominal_cek;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $hutang->nominal_cek;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $hutang->transaksi_pembelian->kode_transaksi.'/CEK - pembayaran hutang',
            'keterangan' => 'Penyesuaian Pembayaran Hutang Cek via Bank '.$bank->nama_bank,
            'debet' => $hutang->nominal_cek,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $hutang->transaksi_pembelian->kode_transaksi.'/CEK - pembayaran hutang',
            'keterangan' => 'Penyesuaian Pembayaran Hutang Cek via Bank '.$bank->nama_bank,
            'kredit' => $hutang->nominal_cek,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $hutang->nominal_cek,
        ]);

        return redirect('/hutang/show/'.$hutang->transaksi_pembelian->id)->with('sukses', 'cek');
    }

    public function CairBG($id)
    {
        // return $id;
        $hutang = Hutang::find($id);
        $hutang->lunas_bg = 1;
        $hutang->update();

        $transaksi_pembelian = TransaksiPembelian::find($hutang->transaksi_pembelian->id);
        $transaksi_pembelian->sisa_utang -= $hutang->nominal_bg;
        $transaksi_pembelian->update();

        $bank = Bank::find($hutang->bank_bg);
        $bank->nominal -= $hutang->nominal_bg;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $hutang->nominal_bg;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $hutang->nominal_bg;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $hutang->transaksi_pembelian->kode_transaksi.'/BG - pembayaran hutang',
            'keterangan' => 'Penyesuaian Pembayaran Hutang BG via Bank '.$bank->nama_bank,
            'debet' => $hutang->nominal_bg,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $hutang->transaksi_pembelian->kode_transaksi.'/BG - pembayaran hutang',
            'keterangan' => 'Penyesuaian Pembayaran Hutang BG via Bank '.$bank->nama_bank,
            'kredit' => $hutang->nominal_bg,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $hutang->nominal_bg,
        ]);

        return redirect('/hutang/show/'.$hutang->transaksi_pembelian->id)->with('sukses', 'cek');
    }
}
