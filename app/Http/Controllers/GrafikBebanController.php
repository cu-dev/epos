<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;

use App\Util;
use App\Akun;
use App\LabaRugi;
use App\Http\Controllers\LabaRugiController;
// use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class GrafikBebanController extends Controller
{
    public function index()
    {
        $beban_ex = [Akun::Beban, Akun::TaksiranUtangGaransi, Akun::BebanGaransi, Akun::PenjualanKredit];
        $bebans = Akun::where('kode', 'like', "5%")->whereNotIn('kode', $beban_ex)->orderBy('kode', 'asc')->get();

        return view('grafik.beban', compact('bebans', 'beban_ex'));
    }

    public function tahun($beban, $tahun)
    {
        $beban_list = self::beban_list();
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $datas = array();

        for($i = 1; $i <= 12; $i++){
            $awal_ = new DateTime($tahun.'-'.$i.'-01');
            $awal = $awal_->format('Y-m-d');

            if($i == 12){
                $akhir_ = new DateTime(($tahun+1).'-01-01');
                $akhir = $akhir_->format('Y-m-d');
            }else{
                $akhir_ = new DateTime($tahun.'-'.($i+1).'-01');
                $akhir = $akhir_->format('Y-m-d');
            }
            $cal = LabaRugiController::mhitung($awal, $akhir);
            $val = $cal[$beban_list[$beban]];
            $val = str_replace('.', '', $val);
            $val = str_replace(',', '.', $val);

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }

    public function bulan($beban, $tahun, $bulan)
    {
        $beban_list = self::beban_list();
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $datas = array();

        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            $awal_ = new DateTime($tahun.'-'.$bulan.'-'.$i);
            $awal = $awal_->format('Y-m-d');
                
            if($i == intval($last_day)){
                $akhir_ = (new DateTime($tahun.'-'.$bulan.'-'.$i))->modify('+1 day');
                $akhir = $akhir_->format('Y-m-d');
            }else{
                $akhir_ = new DateTime($tahun.'-'.$bulan.'-'.($i+1));
                $akhir = $akhir_->format('Y-m-d');
            }

            $cal = LabaRugiController::mhitung($awal, $akhir);
            $val = $cal[$beban_list[$beban]];
            $val = str_replace('.', '', $val);
            $val = str_replace(',', '.', $val);

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }

    public function rentang($beban, $awal, $akhir)
    {
        $beban_list = self::beban_list();
        $awal_ = (new DateTime($awal))->modify('+1 day');
        $add_awal = (new DateTime($awal))->modify('+2 day');
        $akhir = new DateTime($akhir);

        $interval = date_diff($akhir, $awal_);
        $labels = array();
        $datas = array();

        for ($i = 0; $i <= $interval->days; $i++) {
            $val = 0;

            if ($i == 0 ) {
                $start = $awal_->add(new DateInterval('P0D'));
                $end = $add_awal->add(new DateInterval('P0D'));
            } else {
                $start = $awal_->add(new DateInterval('P1D'));
                $end = $add_awal->add(new DateInterval('P1D'));
            }

            $tanggal = $start->format('d m Y');
            $start = $start->format('Y-m-d');
            $end = $end->format('Y-m-d');

            $cal = LabaRugiController::mhitung($start, $end);
            if($cal != NULL){
                $val = $cal[$beban_list[$beban]];
                $val = str_replace('.', '', $val);
                $val = str_replace(',', '.', $val);
            }

            array_push($labels, \App\Util::tanggal($tanggal));
            array_push($datas, $val);
        }
        return response()->json(compact('labels', 'datas'));
    }

    public function tanggal($beban, $tanggal)
    {
        $beban_list = self::beban_list();
        $labels = array();
        $datas = array();
        for ($i = 6; $i <= 22; $i++) {
            $val = 0;
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $jam = $label;
            $label .= '.00';
            array_push($labels, $label);

            $cal = LabaRugiController::todayHitung($tanggal, $jam);
            if($cal != NULL){
                $val = $cal[$beban_list[$beban]];
                $val = str_replace('.', '', $val);
                $val = str_replace(',', '.', $val);
            }

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }

    public static function beban_list(){
        $data = array();
        $data = [
            'semua' => 'total_beban',
            Akun::BebanOngkir => 'beban_ongkir',
            Akun::BebanIklan => 'beban_iklan',
            Akun::BebanSewa => 'beban_sewa',
            Akun::BebanPerlengkapan => 'beban_perlengkapan',
            Akun::BebanPerawatan => 'beban_rawat',
            Akun::BebanRugiAset => 'beban_rugi_aset',
            Akun::BebanKerugianPiutang => 'rugi_piutang',
            Akun::BebanDepKendaraan => 'beban_dep_kendaraan',
            Akun::BebanAsuransi => 'beban_asuransi',
            Akun::BebanGaji => 'beban_gaji',
            Akun::BebanGaji => 'beban_adm_bank',
            Akun::BebanAdministrasiBank => 'beban_ongkir',
            Akun::BebanUtilitas => 'beban_utilitas',
            Akun::BebanLain => 'beban_lain',
            Akun::BebanKerugianPersediaan => 'rugi_persediaan',
            Akun::BebanDepBangunan => 'dep_bangunan',
            Akun::BebanTransportasi => 'transportasi',
            Akun::BebanPajak => 'pajak',
            Akun::BebanBunga => 'bunga',
            Akun::BebanKonsumsi => 'konsumsi',
            Akun::BebanSosial => 'sosial',
            Akun::BebanListrik => 'beban_listrik',
        ];

        return $data;
    }
}
