<?php

namespace App\Http\Controllers;

use Auth;
use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Item;
use App\Laci;
use App\Stok;
use App\Util;
use App\Hutang;
use App\Jurnal;
use App\Satuan;
use App\Seller;
use App\Suplier;
use App\LogLaci;
use App\RelasiSatuan;
use App\TransaksiPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class KonsinyasiKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $konsinyasi_keluars = TransaksiPembelian::where('po', 0)
            ->where('kode_transaksi', 'like', '%TPMKP%')
            ->orderBy('kode_transaksi', 'desc')->get();
        return view('konsinyasi_keluar.index', compact('konsinyasi_keluars'));
    } */

    public function index()
    {
        return view('konsinyasi_keluar.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_pembelians.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $konsinyasi_keluars = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id',
                'transaksi_pembelians.created_at',
                'transaksi_pembelians.kode_transaksi',
                'transaksi_pembelians.harga_total',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 0)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPMKP/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 0)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPMKP/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($konsinyasi_keluars as $i => $konsinyasi_keluar) {

            $konsinyasi_keluar->harga_total = Util::duit($konsinyasi_keluar->harga_total);

            $buttons['detail'] = ['url' => url('konsinyasi-keluar/'.$konsinyasi_keluar->id)];
            $buttons['cek'] = null;
            $buttons['bg'] = null;

            if ($konsinyasi_keluar->aktif_cek == 1) {
                $buttons['cek'] = ['url' => ''];
            }

            if ($konsinyasi_keluar->aktif_bg == 1) {
                $buttons['bg'] = ['url' => ''];
            }

            // return $buttons;
            $konsinyasi_keluar->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $konsinyasi_keluars,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function lastJson()
    {
        $konsinyasi_keluar = TransaksiPembelian::where('kode_transaksi', 'like', '%TPMKP%')->get()->last();
        return response()->json([
            'konsinyasi_keluar' => $konsinyasi_keluar
        ]);
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_pembelian_terakhir = TransaksiPembelian::where('kode_transaksi', 'like', '%TPMKP%')->get()->last();
        if ($transaksi_pembelian_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPMKP/'.$tanggal;
        } else {
            $kode_transaksi_pembelian_terakhir = $transaksi_pembelian_terakhir->kode_transaksi;
            $tanggal_transaksi_pembelian_terakhir = substr($kode_transaksi_pembelian_terakhir, 11, 10);
            // return [$kode_transaksi_pembelian_terakhir, $tanggal_transaksi_pembelian_terakhir];
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_pembelian_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_pembelian_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPMKP/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPMKP/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supliers = Suplier::whereHas('items', function ($query) {
            $query->where('konsinyasi', 1);
        })->get();
        return view('konsinyasi_keluar.create', compact('supliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $kartu_kredit = Akun::where('kode', Akun::KartuKredit)->first();
        $hutang_konsinyasi = Akun::where('kode', Akun::HutangKonsinyasi)->first();
        $beban_ongkir = Akun::where('kode', Akun::BebanOngkir)->first();
        $kode_transaksi = self::kodeTransaksiBaru();

        $harga_total = $request->harga_total;
        $jum_bayar = $request->jumlah_bayar;
        $selisih = $harga_total - $jum_bayar;

        $temp_persediaan = 0;
        $temp_kas_tunai = 0;
        $temp_kas_bank = 0;
        $temp_hutang_konsinyasi = 0;
        $temp_beban_ongkir = 0;

        $data = array();
        foreach ($request->item_id as $i => $id) {
            $item = Item::find($id);
            $stoks = Stok::where('item_kode', $item->kode)
                    ->where('jumlah', '>=', 0)->where('transaksi_pembelian_id', $request->transaksi_pembelian_id)->get();

            // ngurangin stok tabel
            foreach ($stoks as $j => $stok) {
                $stok->jumlah = 0;
                $stok->update();
            }
            // $stok->jumlah -= $request->jumlah_sisa[$i];
            // $stok->update();

            // foreach ($stoks as $j => $stok) {
            //  $temp_stok = $stok->jumlah;
            //  if ($temp_stok <= $sisa_pengurangan) {
            //      $stok->jumlah -= $temp_stok;
            //      $sisa_pengurangan -= $temp_stok;
            //      $stok->update();
            //  }
            // }

            //ngurangin stoktotal tabel item
            $items = Item::where('kode_barang', $item->kode_barang)->get();
            foreach ($items as $j => $item) {
                $item->stoktotal -= $request->jumlah_sisa[$i];
                $item->update();
            }

            //variabel buat ngurangin persediaan dan utang
            $temp_persediaan += $request->harga[$i] * $request->jumlah_sisa[$i];

            $data[''.$id] = [
                'subtotal' => $request->subtotal[$i],
                'harga' => $request->harga[$i],
                'jumlah' => $request->jumlah_jual[$i]
            ];
        }


        // dd($temp_persediaan, $temp_hutang_konsinyasi);
        $temp_persediaan = number_format($temp_persediaan, 2, '.', '');
        $temp_persediaan = explode('.', $temp_persediaan);
        $temp_persediaan[1] = substr($temp_persediaan[1], 0, 2);
        $temp_persediaan = floatval($temp_persediaan[0].'.'.$temp_persediaan[1]);
        // return $temp_persediaan;
        $selisih_persediaan = 0;
        $total_persediaan = 0;
        $relasis_konsinyasi = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $request->transaksi_pembelian_id)->get();
        foreach ($relasis_konsinyasi as $j => $relasi) {
            $total_persediaan += $relasi->jumlah * $relasi->harga;
        }

        $total_persediaan = number_format($total_persediaan, 2, '.', '');
        $total_persediaan = explode('.', $total_persediaan);
        $total_persediaan[1] = substr($total_persediaan[1], 0, 2);
        $total_persediaan = floatval($total_persediaan[0].'.'.$total_persediaan[1]);

        $konsinyasi = TransaksiPembelian::find($request->transaksi_pembelian_id);
        $temp_hutang_konsinyasi += $konsinyasi->harga_total;
        
        $selisih_persediaan = $temp_hutang_konsinyasi - $total_persediaan;
        // dd($temp_hutang_konsinyasi, $total_persediaan);
        // return $selisih_persediaan;

        // Cari transaksi_pembelian yang kodenya KNM dan status hutangnya 1
        if ($request->kode_konsinyasi != null && $request->kode_konsinyasi != '') {
            $konsinyasi_masuk = TransaksiPembelian::where('kode_transaksi', $request->kode_konsinyasi)->first();
        } else {
            $konsinyasi_masuk = TransaksiPembelian::where('kode_transaksi', 'like', '%KNM%')
                                ->where('status_hutang', '1')
                                ->get()->last();
        }

        //input konsinyasi keluar
        $transaksi_pembelian = new TransaksiPembelian();
        $transaksi_pembelian->kode_transaksi = $kode_transaksi;
        $transaksi_pembelian->nota = $request->nota;
        $transaksi_pembelian->suplier_id = $request->suplier_id;
        $transaksi_pembelian->seller_id = $request->seller_id;
        $transaksi_pembelian->jumlah_bayar = $request->jumlah_bayar;
        $transaksi_pembelian->nominal_tunai = $request->nominal_tunai;

        $transaksi_pembelian->no_transfer = $request->no_transfer;
        $transaksi_pembelian->nominal_transfer = $request->nominal_transfer;
        $transaksi_pembelian->bank_transfer = $request->bank_transfer;

        $transaksi_pembelian->no_kartu = $request->no_kartu;
        $transaksi_pembelian->nominal_kartu = $request->nominal_kartu;
        $transaksi_pembelian->bank_kartu = $request->bank_kartu;
        $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;

        $transaksi_pembelian->no_cek = $request->no_cek;
        $transaksi_pembelian->nominal_cek = $request->nominal_cek;
        $transaksi_pembelian->bank_cek = $request->bank_cek;
        // $transaksi_pembelian->aktif_cek = $request->aktif_cek;

        $transaksi_pembelian->no_bg = $request->no_bg;
        $transaksi_pembelian->nominal_bg = $request->nominal_bg;
        $transaksi_pembelian->bank_bg = $request->bank_bg;
        // $transaksi_pembelian->aktif_bg = $request->aktif_bg;

        $transaksi_pembelian->user_id = Auth::user()->id;

        $konsinyasi_masuk->status_hutang = 0;
        $konsinyasi_masuk->sisa_utang = 0;
        $konsinyasi_masuk->update();

        $hutang_konsinyasi->kredit -= $temp_hutang_konsinyasi;
        Jurnal::create([
            'kode_akun' => $hutang_konsinyasi->kode,
            'referensi' => $kode_transaksi,
            'keterangan' => 'Pengambilan Konsinyasi',
            'debet' => $temp_hutang_konsinyasi
        ]);

        if ($request->ongkos_kirim > 0) {
            $beban_ongkir->debet += $request->ongkos_kirim;
            Jurnal::create([
                'kode_akun' => $beban_ongkir->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $request->ongkos_kirim
            ]);
        }

        if ($selisih_persediaan < 0) {
            Jurnal::create([
                'kode_akun' => Akun::BebanKerugianPersediaan,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih_persediaan * -1
            ]);
        }
        if ($selisih < 0) {
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih * -1
            ]);
        }
        if ($selisih > 0) {
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih
            ]);
        }
        if ($selisih_persediaan > 0) {
            Jurnal::create([
                'kode_akun' => Akun::UntungPersediaan,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih_persediaan
            ]);
        }

        if ($temp_persediaan > 0) {
            // $temp_persediaan = ceil($temp_persediaan / 100) * 100;
            $persediaan->debet -= $temp_persediaan;
            Jurnal::create([
                'kode_akun' => $persediaan->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $temp_persediaan
            ]);
        }

        if ($request->nominal_tunai > 0) {
            $kas_tunai->debet -= $request->nominal_tunai;
            Jurnal::create([
                'kode_akun' => $kas_tunai->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $request->nominal_tunai
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $request->nominal_tunai,
            ]);

            if(Auth::user()->level_id == 5){
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 16;
                $log->nominal = $request->nominal_tunai;
                $log->save();

                $laci = Laci::find(1);
                $laci->gudang -= $request->nominal_tunai;
                $laci->update();
            }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
            {
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 16;
                $log->nominal = $request->nominal_tunai;
                $log->save();

                $laci = Laci::find(1);
                $laci->owner -= $request->nominal_tunai;
                $laci->update();
            }
        }

        // if ($request->nominal_transfer > 0) {
        //  $bank = Bank::find($request->id_bank);
        //  $bank->nominal -= $request->nominal_transfer;
        //  $bank->update();
        //  $kas_bank->debet -= $request->nominal_transfer;
        //  Jurnal::create([
        //      'kode_akun' => $kas_bank->kode,
        //      'referensi' => $kode_transaksi,
        //      'keterangan' => 'Pengambilan Konsinyasi',
        //      'kredit' => $request->nominal_transfer
        //  ]);
        // }

        if ($request->nominal_transfer > 0 || $request->nominal_kartu > 0) {
            $keterangan_bank = 'Pengambilan Konsinyasi ';
            $nominal_bank = 0;
            if($request->nominal_transfer > 0){
                $kas_bank->debet -= $request->nominal_transfer;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal -= $request->nominal_transfer;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - '.$request->nominal_transfer.' via '.$bank->nama_bank;
                $nominal_bank += $request->nominal_transfer;
            }

            if($request->nominal_kartu > 0){
                $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;

                if($request->jenis_kartu == 'kredit'){
                    $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                    $akun_kartu->kredit += $request->nominal_kartu;
                    $akun_kartu->update();

                    Jurnal::create([
                        'kode_akun' => Akun::KartuKredit,
                        'referensi' => $kode_transaksi,
                        'keterangan' => 'Pengambilan Konsinyasi via kartu kredit',
                        'kredit' => $request->nominal_kartu
                    ]);
                }else{
                    $kas_bank->debet -= $request->nominal_kartu;

                    $bank = Bank::find($request->bank_kartu);
                    $bank->nominal -= $request->nominal_kartu;
                    $bank->update();

                    $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                    $nominal_bank += $request->nominal_kartu;
                }
            }

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $nominal_bank,
            ]);

            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $kode_transaksi,
                'keterangan' => $keterangan_bank,
                'kredit' => $nominal_bank
            ]);

            
        }

        if ($request->nominal_cek > 0) {
            if ($request->bank_cek == NULL) {
                $kas_cek->debet -= $request->nominal_cek;

                $cek_id = Cek::find($request->cek_id);
                $cek_id->aktif = 0;
                $cek_id->update();

                Jurnal::create([
                    'kode_akun' => $kas_cek->kode,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang',
                    'kredit' => $request->nominal_cek
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $request->nominal_cek,
                ]);
            } else {
                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $request->nominal_cek;
                $akun->update();

                $cek = new Cek();
                $cek->nomor = $request->no_cek;
                $cek->nominal = $request->nominal_cek;
                $cek->aktif = 0;
                $cek->save();

                $transaksi_pembelian->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $request->nominal_cek
                ]);
            }
        }

        if ($request->nominal_bg > 0) {
            if ($request->bank_bg == NULL) {
                $kas_bg->debet -= $request->nominal_bg;
                Jurnal::create([
                    'kode_akun' => $kas_bg->kode,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang',
                    'kredit' => $request->nominal_bg
                ]);

                $bg_id = BG::find($request->bg_id);
                $bg_id->aktif = 0;
                $bg_id->update();

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $request->nominal_bg,
                ]);
            } else {
                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $request->nominal_bg;
                $akun->update();

                $bg = new BG();
                $bg->nomor = $request->no_bg;
                $bg->nominal = $request->nominal_bg;
                $bg->aktif = 0;
                $bg->save();

                $transaksi_pembelian->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $request->nominal_bg
                ]);
            }
        }

        if ($selisih < 0) {
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih * -1
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih * -1
            ]);
        }
        if ($selisih > 0) {
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih
            ]);
        }

        if ($selisih_persediaan < 0) {
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih_persediaan * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanKerugianPersediaan,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih_persediaan * -1
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih_persediaan * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih_persediaan * -1
            ]);
        }
        if ($selisih_persediaan > 0) {
            Jurnal::create([
                'kode_akun' => Akun::UntungPersediaan,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih_persediaan
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - L/R',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih_persediaan
            ]);
            //LDT
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'debet' => $selisih_persediaan
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' - LDT',
                'keterangan' => 'Pengambilan Konsinyasi',
                'kredit' => $selisih_persediaan
            ]);
        }

        $akun_LDT = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_LDT->kredit += $selisih;
        $akun_LDT->update();

        $akun_LDT = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_LDT->kredit += $selisih_persediaan;
        $akun_LDT->update();


        if (
            $transaksi_pembelian->save() &&
            $hutang_konsinyasi->update() &&
            $persediaan->update() &&
            $kas_tunai->update() &&
            $kas_bank->update() &&
            $kas_cek->update() &&
            $kas_bg->update() &&
            $beban_ongkir->update()) {

            $id = TransaksiPembelian::get()->last()->id;
            foreach ($request->item_id as $x => $item) {
                $relasi_keluar = new RelasiTransaksiPembelian();
                $relasi_keluar->transaksi_pembelian_id = $id;
                $relasi_keluar->item_id = $request->item_id[$x];
                $relasi_keluar->jumlah = $request->jumlah_jual[$x];
                $relasi_keluar->harga = $request->harga[$x];
                $relasi_keluar->subtotal = $request->jumlah_jual[$x] * $request->harga[$x];
                $relasi_keluar->save();
            }

            return redirect('/konsinyasi-keluar')->with('sukses', 'tambah');
        } else {
            return redirect('/konsinyasi-keluar')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $konsinyasi_keluar = TransaksiPembelian::with('suplier', 'seller', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();

        if ($konsinyasi_keluar == null ||
            strtolower(explode('/', $konsinyasi_keluar->kode_transaksi)[1]) != 'tpmkp') {
            return redirect('/transaksi-pembelian')->with('gagal', '404');
        }

        $relasi_konsinyasi_keluar = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)->get();

        foreach ($relasi_konsinyasi_keluar as $i => $relasi) {
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $relasi->item->kode)
                ->orderBy('konversi', 'desc')->get();

            if (count($relasi_satuan) > 0) {
                $temp = $relasi->jumlah;
                $jumlah = array();
                foreach ($relasi_satuan as $j => $rs) {
                    $hasil = intval($temp / $rs->konversi);
                    if ($hasil > 0) {
                        $temp %= $rs->konversi;
                        array_push($jumlah, array(
                            'jumlah' => $hasil,
                            'satuan' => $rs->satuan
                        ));
                    }
                }
            } else {
                $jumlah = array();
                $jumlah[0] = [
                    'jumlah' => intval($relasi->jumlah),
                    'satuan' => Satuan::where('kode', 'PCS')->first(),
                ];
            }
            
            $relasi->jumlahs = $jumlah;
        }

        return view('konsinyasi_keluar.show', compact('konsinyasi_keluar', 'relasi_konsinyasi_keluar'));
    }

    public function showItemJson($id, $pembelian)
    {
        $item = Item::with('satuan')->find($id);
        $stok = Stok::where('item_kode',  $item->kode)->where('transaksi_pembelian_id', $pembelian)->get()->sum('jumlah');
        $pcs = Satuan::where('kode', 'PCS')->first();
        $relasi = RelasiTransaksiPembelian::where('item_id', $id)->where('transaksi_pembelian_id', $pembelian)->get()->last();
        return response()->json([
            'item' => $item,
            'stok' => $stok,
            'pcs' => $pcs,
            'relasi' => $relasi
        ]);
    }

    public function showSuplierItemsJson($id)
    {
        $items = Item::where('suplier_id', $id)->get();
        return response()->json([
            'items' => $items
        ]);
    }

    public function showSuplierSellersJson($id)
    {
        $sellers = Seller::where('suplier_id', $id)->get();
        return response()->json([
            'sellers' => $sellers
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cekLunas($id)
    {
        $konsinyasi = TransaksiPembelian::find($id);
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_pembayaran = Akun::where('kode', Akun::PembayaranProses)->first();
        $bank = Bank::find($konsinyasi->bank_cek)->first();
        //ngurangi nominal bank
        $bank->nominal -= $konsinyasi->nominal_cek;
        $bank->update();
        //ngurangi akun bank
        $akun_bank->debet -= $konsinyasi->nominal_cek;
        $akun_bank->update();
        //ngurangi akun pembayaran
        $akun_pembayaran->kredit -= $konsinyasi->nominal_cek;
        $akun_pembayaran->update();
        //update kolom akif cek
        $konsinyasi->aktif_cek = 0;
        $konsinyasi->update();
        //jurnal debet pembayaran
        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $konsinyasi->kode_transaksi.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$konsinyasi->kode_transaksi,
            'debet' => $konsinyasi->nominal_cek
        ]);
        //jurnal kredit bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $konsinyasi->kode_transaksi.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$konsinyasi->kode_transaksi,
            'kredit' => $konsinyasi->nominal_cek
        ]);
        //kolom arus kas
        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $konsinyasi->nominal_cek
        ]);

        return redirect('/konsinyasi-keluar/'.$id)->with('sukses', 'cek');
    }

    public function bgLunas($id)
    {
        $konsinyasi = TransaksiPembelian::find($id);
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_pembayaran = Akun::where('kode', Akun::PembayaranProses)->first();
        $bank = Bank::find($konsinyasi->bank_bg)->first();
        //ngurangi nominal bank
        $bank->nominal -= $konsinyasi->nominal_bg;
        $bank->update();
        //ngurangi akun bank
        $akun_bank->debet -= $konsinyasi->nominal_bg;
        $akun_bank->update();
        //ngurangi akun pembayaran
        $akun_pembayaran->kredit -= $konsinyasi->nominal_bg;
        $akun_pembayaran->update();
        //update kolom akif bg
        $konsinyasi->aktif_bg = 0;
        $konsinyasi->update();
        //jurnal debet pembayaran
        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $konsinyasi->kode_transaksi.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$konsinyasi->kode_transaksi,
            'debet' => $konsinyasi->nominal_bg
        ]);
        //jurnal kredit bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $konsinyasi->kode_transaksi.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$konsinyasi->kode_transaksi,
            'kredit' => $konsinyasi->nominal_bg
        ]);
        //kolom arus kas
        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $konsinyasi->nominal_bg
        ]);

        return redirect('/konsinyasi-keluar/'.$id)->with('sukses', 'bg');
    }
    
}
