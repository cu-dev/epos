<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Piutang;
use App\CashDrawer;
use App\MoneyLimit;
use App\RelasiSatuan;
use App\TransaksiPenjualan;
use App\RelasiTransaksiPenjualan;

class PrivePersediaanController extends Controller
{
    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->get()->first();
        $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode')->having('kode', '=', $kode)->get()->first();
        $harga = Stok::where('item_kode', $item->kode)
                    ->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
        // $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
        // return response()->json(['item' => $item, 'stok' => $stok]);
        return response()->json(['item' => $item, 'stok' => $stok, 'harga' => $harga]);
    }

    public function index()
    {
        $money_limit = MoneyLimit::find(1);
        $cash = CashDrawer::where('user_id', Auth::id())->first();
        $banks = Bank::all();
        $items = Item::distinct()->select('kode', 'nama')->get();
        $satuans = Satuan::all();

        return view('prive_persediaan.create', compact('items', 'satuans', 'banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
