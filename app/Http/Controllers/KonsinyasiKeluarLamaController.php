<?php

namespace App\Http\Controllers;

use App\Item;
use App\Suplier;
use App\ItemMasuk;
use App\ItemKeluar;

use Illuminate\Http\Request;

class KonsinyasiLamaKeluarController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	// public function index()
	// {
	//     $konsinyasi_keluars = ItemKeluar::with(['item_masuk' => function($query){
	//         $query->join('items', 'item_masuks.item_id', '=', 'items.id')->where('items.konsinyasi', '=', 1);
	//     },'item_masuk'])->get();
	//     return view('konsinyasi_keluar.index', compact('konsinyasi_keluars'));
	// }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create($id)
	{
		$konsinyasi_masuk = ItemMasuk::find($id);
		$items = Item::all();
		$supliers = Suplier::all();
		return view('konsinyasi_keluar.create', compact('konsinyasi_masuk', 'items', 'supliers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'keterangan' => 'required|string'
		]);

		$konsinyasi_keluar = new ItemKeluar();
		$konsinyasi_keluar->item_masuk_id = $request->item_masuk_id;
		$konsinyasi_keluar->jumlah = $request->jumlah;
		$konsinyasi_keluar->keterangan = $request->keterangan;
		if ($konsinyasi_keluar->save()) {
			$item = Item::find($konsinyasi_keluar->item_masuk->id);
			$item->stok -= $konsinyasi_keluar->jumlah;
			$item->save();

			return redirect('/konsinyasi-keluar')->with('sukses', 'tambah');
		} else {
			return redirect('/konsinyasi-keluar')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$konsinyasi_keluar = ItemKeluar::find($id);
		return view('konsinyasi_keluar.show', compact('konsinyasi_keluar'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$konsinyasi_keluar = ItemKeluar::find($id);
		$items = Item::all();
		$supliers = Suplier::all();
		return view('konsinyasi_keluar.edit', compact('konsinyasi_keluar', 'items', 'supliers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'keterangan' => 'required|string'
		]);

		$konsinyasi_keluar = ItemKeluar::find($id);
		$stok = $konsinyasi_keluar->jumlah;
		$konsinyasi_keluar->jumlah = $request->jumlah;
		$konsinyasi_keluar->keterangan = $request->keterangan;
		if ($konsinyasi_keluar->save()) {
			$item = Item::find($konsinyasi_keluar->item_masuk->id);
			$item->stok -= ($konsinyasi_keluar->jumlah - $stok);
			$item->save();

			return redirect('/konsinyasi-keluar')->with('sukses', 'ubah');
		} else {
			return redirect('/konsinyasi-keluar')->with('gagal', 'ubah');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	// public function destroy($id)
	// {
	//     blabla
	// }
}
