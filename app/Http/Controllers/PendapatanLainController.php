<?php

namespace App\Http\Controllers;

use App\Akun;
use App\Arus;
use App\Util;
use App\Bank;
use App\Jurnal;

use Illuminate\Http\Request;

class PendapatanLainController extends Controller
{
    public function lastJson() {
        $pendapatan = Jurnal::where('kode_akun', Akun::PendapatanLain)
                    ->where('kredit', '!=', null)
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['pendapatan' => $pendapatan]);
    }

    /* public function index() {
        $banks = Bank::all();
        $pendapatans = Jurnal::where('kode_akun', Akun::PendapatanLain)->where('kredit', '>', 0)->get();
        return view('pendapatan_lain.index', compact('banks', 'pendapatans'));
    } */

    public function index() {
        $banks = Bank::all();
        return view('pendapatan_lain.index', compact('banks'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'jurnals.' . $field;

        $kode_akuns = [
            Akun::PendapatanLain,
        ];

        $bebans = Jurnal
            ::select(
                'jurnals.id',
                'jurnals.kredit',
                'jurnals.referensi',
                'jurnals.keterangan'
            )
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.kredit', '>', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.kredit', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Jurnal
            ::select(
                'jurnals.id'
            )
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.kredit', '>', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.kredit', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($bebans as $i => $beban) {

            $beban->kredit = Util::dk($beban->kredit);

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bebans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request)
    {
        // return $request->all();
        //create jurnal pendapatan lain
        $jurnal_pendapatan = new Jurnal();
        $jurnal_pendapatan->kode_akun = Akun::PendapatanLain;
        $jurnal_pendapatan->referensi = $request->kode_transaksi;
        $jurnal_pendapatan->kredit = $request->nominal;

        //update akun pendapatan lain
        // $akun_pendapatan = Akun::where('kode', Akun::PendapatanLain)->first();
        // $akun_pendapatan->kredit += $request->nominal;
        // $akun_pendapatan->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit +=  $request->nominal;
        $akun_laba_ditahan->update();

        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet += $request->nominal;
            $akun_tunai->update();
           
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->keterangan;
            $tunai->debet = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
            $jurnal_pendapatan->keterangan = $request->keterangan;

        }else{
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet += $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal += $request->nominal;
            $bank->update();
            // create baris akun tunai
            $keterangan = $request->keterangan.' - masuk Bank '.$bank->nama_bank;
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode_transaksi;
            $jurnal_bank->keterangan = $keterangan;
            $jurnal_bank->debet = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
            $jurnal_pendapatan->keterangan = $keterangan;
        }

        $jurnal_pendapatan->save();

        Jurnal::create([
            'kode_akun' => Akun::PendapatanLain,
            'referensi' => $request->kode_transaksi.' - L/R',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode_transaksi.' - L/R',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode_transaksi.' - LDT',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $request->kode_transaksi.' - LDT',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Arus::create([
            'nama' => Arus::PendapatanLain,
            'nominal' => $request->nominal,
        ]);
        
        return redirect('/pendapatan_lain')->with('sukses', 'tambah');        
    }
}
