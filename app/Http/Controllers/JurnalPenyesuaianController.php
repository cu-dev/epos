<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Akun;
use App\Jurnal;
use App\Bank;

use Illuminate\Http\Request;

class JurnalPenyesuaianController extends Controller
{
	public function lastJson() {
        $jurnal = Jurnal::where('referensi', 'like', '%/JLP/%')
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['jurnal' => $jurnal]);
    }

    public function index() {
    	$akuns = Akun::where('kode', Akun::PiutangTakTertagih)
                    ->orWhere('kode', Akun::AsuransiDimuka)
                    ->orWhere('kode', Akun::SewaDimuka)
                    ->orWhere('kode', Akun::PPNMasukan)
                    ->orWhere('kode', Akun::KartuKredit)
                    ->orWhere('kode', Akun::PPNKeluaran)
                    ->orWhere('kode', Akun::BebanKerugianPiutang)
                    ->orWhere('kode', Akun::BebanAsuransi)
                    ->orWhere('kode', Akun::BebanSewa)
                    ->get();
    	$banks = Bank::all();
    	return view('jurnal_penyesuaian.index', compact('akuns', 'banks'));
    }

    public function store(Request $request){
    	// return $request->all();
    	foreach($request->akun as $nums => $akun){
    		// dd($request->akun[$nums]);
    		if($akun[0] == 1){
    			$nominal = ($request->debit[$nums] == 0 ? -$request->kredit[$nums] : $request->debit[$nums]);
				$akun = Akun::where('kode', $request->akun[$nums])->first();
				$akun->debet += $nominal;
				$akun->update();

				if($request->akun[$nums] == Akun::KasBank){
					$bank = Bank::find(2);
					$bank->nominal += $nominal;
					$bank->update();
					$keterangan = $request->keterangan.' - '.$bank->nama_bank;
				}else{
					$keterangan = $request->keterangan;
				}

				$jurnal = new Jurnal();
				$jurnal->kode_akun = $request->akun[$nums];
		        $jurnal->referensi = $request->kode_transaksi;
		        $jurnal->debet = $request->debit[$nums];
		        $jurnal->kredit = $request->kredit[$nums];
		        $jurnal->keterangan = $keterangan;
		        $jurnal->save();
    		}elseif($akun[0] == 2){
    			$nominal = ($request->debit[$nums] == 0 ? $request->kredit[$nums] : -$request->debit[$nums]);
				$akun = Akun::where('kode', $request->akun[$nums])->first();
				$akun->kredit += $nominal;
				$akun->update();

				$jurnal = new Jurnal();
				$jurnal->kode_akun = $request->akun[$nums];
		        $jurnal->referensi = $request->kode_transaksi;
		        $jurnal->debet = $request->debit[$nums];
		        $jurnal->kredit = $request->kredit[$nums];
		        $jurnal->keterangan = $request->keterangan;
		        $jurnal->save();
    		}elseif($akun[0] == 5){
    			$nominal = ($request->debit[$nums] == 0 ? -$request->kredit[$nums] : $request->debit[$nums]);
				$akun = Akun::where('kode', $request->akun[$nums])->first();
				$akun->debet += $nominal;
				$akun->update();

				$jurnal = new Jurnal();
				$jurnal->kode_akun = $request->akun[$nums];
		        $jurnal->referensi = $request->kode_transaksi;
		        $jurnal->debet = $request->debit[$nums];
		        $jurnal->kredit = $request->kredit[$nums];
		        $jurnal->keterangan = $request->keterangan;
		        $jurnal->save();
    		}
    	}
        
        return redirect('/jurnal_penyesuaian')->with('sukses', 'tambah');        
    }
}
