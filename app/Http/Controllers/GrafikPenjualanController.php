<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;

use App\Util;
use App\Pelanggan;
// use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class GrafikPenjualanController extends Controller
{
    public function index()
    {
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0) as harga'))
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        return view('grafik.penjualan', compact('transaksi_penjualans', 'pelanggans'));
    }

    public function tahun($tahun)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('MONTH(created_at) bulan'))
            ->groupBy('bulan')
            ->where('created_at', 'like', $tahun.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $data = array();
        for ($i = 1; $i <= 12; $i++) {
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->bulan == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function bulan($tahun, $bulan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('DAY(created_at) tanggal'))
            ->groupBy('tanggal')
            ->where('created_at', 'like', $tahun.'-'.$bulan.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $data = array();
        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->tanggal == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function rentang($awal, $akhir)
    {
        $awal_ = new DateTime($awal);
        $awal_ = $awal_->add(new DateInterval('P1D'));
        $awal = $awal_->format('Y-m-d');
        $akhir_ = new DateTime($akhir);
        $akhir = $akhir_->format('Y-m-d');

        $interval = date_diff($akhir_, $awal_);

        $labels = array();
        $data = array();
        for ($i = 0; $i <= $interval->days; $i++) {
            $val = 0;
            if ($i == 0 ) {
                $tanggal_ = $awal_->add(new DateInterval('P0D'));
            } else {
                $tanggal_ = $awal_->add(new DateInterval('P1D'));
            }

            $tanggal = $tanggal_->format('d m Y');
            $date = $tanggal_->format('Y-m-d');

            $transaksi_penjualans = DB::table('transaksi_penjualans')
                ->select(
                    DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                    DB::raw('DAY(created_at) tanggal'))
                ->groupBy('tanggal')
                ->whereDate('created_at', $date)
                ->whereIn('status', ['eceran', 'grosir', 'vip'])
                ->get();

            if (count($transaksi_penjualans) > 0 && $transaksi_penjualans[0]->harga != null) {
                $val = $transaksi_penjualans[0]->harga;
            }

            array_push($labels, \App\Util::tanggal($tanggal));
            array_push($data, $val);
        }

        return response()->json(compact('labels', 'data'));
    }

    public function tanggal($tanggal)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('HOUR(created_at) jam'))
            ->groupBy('jam')
            ->where('created_at', 'like', $tanggal.'%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = array();
        $data = array();
        for ($i = 6; $i <= 22; $i++) {
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $label .= '.00';
            array_push($labels, $label);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->jam == $i) {
                    $data[$i - 6] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 6] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function tahunPelanggan($tahun, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('MONTH(created_at) bulan'))
            ->groupBy('bulan')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tahun.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $data = array();
        for ($i = 1; $i <= 12; $i++) {
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->bulan == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function bulanPelanggan($tahun, $bulan, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('DAY(created_at) tanggal'))
            ->groupBy('tanggal')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tahun.'-'.$bulan.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $data = array();
        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->tanggal == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function rentangPelanggan($awal, $akhir, $pelanggan)
    {
        $awal_ = new DateTime($awal);
        $awal_ = $awal_->add(new DateInterval('P1D'));
        $awal = $awal_->format('Y-m-d');
        $akhir_ = new DateTime($akhir);
        $akhir = $akhir_->format('Y-m-d');

        $interval = date_diff($akhir_, $awal_);

        $labels = array();
        $data = array();
        for ($i = 0; $i <= $interval->days; $i++) {
            $val = 0;
            if ($i == 0 ) {
                $tanggal_ = $awal_->add(new DateInterval('P0D'));
            } else {
                $tanggal_ = $awal_->add(new DateInterval('P1D'));
            }

            $tanggal = $tanggal_->format('d m Y');
            $date = $tanggal_->format('Y-m-d');

            $transaksi_penjualans = DB::table('transaksi_penjualans')
                ->select(
                    DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                    DB::raw('DAY(created_at) tanggal'))
                ->groupBy('tanggal')
                ->where('pelanggan_id', $pelanggan)
                ->whereDate('created_at', $date)
                ->whereIn('status', ['eceran', 'grosir', 'vip'])
                ->get();

            if (count($transaksi_penjualans) > 0 && $transaksi_penjualans[0]->harga != null) {
                $val = $transaksi_penjualans[0]->harga;
            }

            array_push($labels, \App\Util::tanggal($tanggal));
            array_push($data, $val);
        }

        return response()->json(compact('labels', 'data'));
    }

    public function tanggalPelanggan($tanggal, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('HOUR(created_at) jam'))
            ->groupBy('jam')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tanggal.'%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = array();
        $data = array();
        for ($i = 6; $i <= 22; $i++) {
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $label .= '.00';
            array_push($labels, $label);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->jam == $i) {
                    $data[$i - 6] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 6] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    /*public function indexPelanggan($pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0) as harga'))
            ->where('pelanggan_id', $pelanggan)
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        return view('grafik.penjualan', compact('transaksi_penjualans'));
    }

    public function tahunPelanggan($tahun, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('MONTH(created_at) bulan'))
            ->groupBy('bulan')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tahun.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $data = array();
        for ($i = 1; $i <= 12; $i++) {
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->bulan == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function bulanPelanggan($tahun, $bulan, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('DAY(created_at) tanggal'))
            ->groupBy('tanggal')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tahun.'-'.$bulan.'-%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $data = array();
        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->tanggal == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function rentangPelanggan($awal, $akhir, $pelanggan)
    {
        $akhir = new DateTime($akhir);
        $akhir = $akhir->add(new DateInterval('P1D'));
        $akhir = $akhir->format('Y-m-d');
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('DAY(created_at) tanggal'))
            ->groupBy('tanggal')
            ->where('pelanggan_id', $pelanggan)
            ->whereBetween('created_at', [$awal, $akhir])
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $data = array();
        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->tanggal == $i) {
                    $data[$i - 1] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function tanggalPelanggan($tanggal, $pelanggan)
    {
        $transaksi_penjualans = DB::table('transaksi_penjualans')
            ->select(
                DB::raw('SUM(IFNULL(nego_total, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('HOUR(created_at) jam'))
            ->groupBy('jam')
            ->where('pelanggan_id', $pelanggan)
            ->where('created_at', 'like', $tanggal.'%')
            ->whereIn('status', ['eceran', 'grosir', 'vip'])
            ->get();
        $labels = array();
        $data = array();
        for ($i = 6; $i <= 22; $i++) {
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $label .= '.00';
            array_push($labels, $label);
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                if ($transaksi_penjualan->jam == $i) {
                    $data[$i - 6] = $transaksi_penjualan->harga;
                    break;
                } else {
                    $data[$i - 6] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }*/
}
