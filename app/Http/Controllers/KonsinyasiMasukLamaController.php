<?php

namespace App\Http\Controllers;

use App\Item;
use App\Suplier;
// use App\ItemMasuk;
use App\TransaksiPembelian;

use Illuminate\Http\Request;

class KonsinyasiMasukLamaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	// public function index()
	// {
	//     $konsinyasi_masuks = ItemMasuk::with(['item' => function($query){
	//         $query->where('konsinyasi', 1);
	//     }, 'suplier', 'item'])->get();
		
	//     return view('konsinyasi_masuk.index', compact('konsinyasi_masuks'));
	// }
	public function index()
	{
		$konsinyasi_masuks = TransaksiPembelian::whereHas('items', function ($query) {
			$query->where('konsinyasi', 1);
		})->orderBy('kode_transaksi', 'desc')->get();
		// dd($konsinyasi_masuks);
		return view('konsinyasi_masuk.index', compact('konsinyasi_masuks'));

		// $items = Item::with('transaksi_pembelians')->where('konsinyasi', 1)->get();
		// dd($items);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$items = Item::where('konsinyasi', 1)->get();
		$supliers = Suplier::all();
		return view('konsinyasi_masuk.create', compact('items', 'supliers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'harga_dasar' => 'required|integer|min:1'
		]);

		$item_masuk = new ItemMasuk();
		$item_masuk->item_id = $request->item_id;
		$item_masuk->suplier_id = $request->suplier_id;
		$item_masuk->jumlah = $request->jumlah;
		$item_masuk->harga_dasar = $request->harga_dasar;
		if ($item_masuk->save()) {
			$item = Item::find($item_masuk->item_id);
			$item->stok += $item_masuk->jumlah;
			$item->save();

			return redirect('/konsinyasi-masuk')->with('sukses', 'tambah');
		} else {
			return redirect('/konsinyasi-masuk')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$konsinyasi_masuk = ItemMasuk::find($id);
		return view('konsinyasi_masuk.show', compact('konsinyasi_masuk'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$konsinyasi_masuk = ItemMasuk::find($id);
		$items = Item::all();
		$supliers = Suplier::all();
		return view('konsinyasi_masuk.edit', compact('konsinyasi_masuk', 'items', 'supliers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'harga_dasar' => 'required|integer|min:1'
		]);

		$konsinyasi_masuk = ItemMasuk::find($id);
		$stok = $konsinyasi_masuk->jumlah;
		$konsinyasi_masuk->item_id = $request->item_id;
		$konsinyasi_masuk->suplier_id = $request->suplier_id;
		$konsinyasi_masuk->jumlah = $request->jumlah;
		$konsinyasi_masuk->harga_dasar = $request->harga_dasar;
		if ($konsinyasi_masuk->save()) {
			$item = Item::find($konsinyasi_masuk->item_id);
			$item->stok += $konsinyasi_masuk->jumlah - $stok;
			$item->save();

			return redirect('/konsinyasi-masuk')->with('sukses', 'ubah');
		} else {
			return redirect('/konsinyasi-masuk')->with('gagal', 'ubah');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
