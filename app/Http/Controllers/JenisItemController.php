<?php

namespace App\Http\Controllers;

use App\JenisItem;

use Illuminate\Http\Request;

class JenisItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index() {
        // return 'aw';
        $jenis_items = JenisItem::whereNotIn('id', [1])->get();
        return view('jenis_item.index', compact('jenis_items'));
    } */

    public function index() {
        return view('jenis_item.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'jenis_items.' . $field;

        $jenis_items = JenisItem
            ::select(
                'jenis_items.id',
                'jenis_items.kode',
                'jenis_items.nama'
            )
            ->whereNotIn('jenis_items.id', [1])
            ->where(function($query) use ($request) {
                $query
                ->where('jenis_items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = JenisItem
            ::select(
                'jenis_items.id'
            )
            ->whereNotIn('jenis_items.id', [1])
            ->where(function($query) use ($request) {
                $query
                ->where('jenis_items.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('jenis_items.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($jenis_items as $i => $jenis_item) {

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $jenis_item->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $jenis_items,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255'
        ]);

        $jenis_item_available = JenisItem::where('kode', $request->kode)->orWhere('nama', $request->nama)->get();
        // return sizeof($jenis_item_available);
        if(sizeof($jenis_item_available) <= 0){
            $jenis_item = new JenisItem();
            $jenis_item->kode = $request->kode;
            $jenis_item->nama = $request->nama;
            if ($jenis_item->save()) {
                return redirect('/jenisitem')->with('sukses', 'tambah');
            } else {
                return redirect('/jenisitem')->with('gagal', 'tambah');
            }
        }else{
            return redirect('/jenisitem')->with('gagal', 'tambah_kode');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255'
        ]);

        $jenis_item = JenisItem::find($id);
        $jenis_item->kode = $request->kode;
        $jenis_item->nama = $request->nama;
        if ($jenis_item->save()) {
            return redirect('/jenisitem')->with('sukses', 'ubah');
        } else {
            return redirect('/jenisitem')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {	
            $jenis_item = JenisItem::find($id);
            if ($jenis_item->delete()) {
                return redirect('/jenisitem')->with('sukses', 'hapus');
            } else {
                return redirect('/jenisitem')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/jenisitem')->with('gagal', 'hapus');
        }
    }
    
}
