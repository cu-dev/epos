<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\ReturRule;

class ReturRuleController extends Controller
{
    public function index()
    {
    	$rule = ReturRule::find(1);
    	return view('rule_retur.index', compact('rule'));
    }

    public function store(Request $request)
	{
		$rule = ReturRule::find(1);
		$rule->syarat = $request->jumlah;
		$rule->user_id = Auth::user()->id;
		$rule->update();

		return redirect('/rule_retur')->with('sukses', 'ubah');
	}
}
