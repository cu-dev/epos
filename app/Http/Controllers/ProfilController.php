<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Image;

use App\User;

use Illuminate\Http\Request;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('profil.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        return view('profil.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'nama' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|max:255',
            'tanggal_lahir' => 'required|max:255',
            'telepon' => 'max:255',
            'alamat' => 'max:255',
        ]);

        $user = Auth::user();

        $user->nama = $request->nama;
        $user->username = $request->username;
        // $user->password = $request->username;
        $user->email = $request->email;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->telepon = $request->telepon;
        $user->alamat = $request->alamat;

        if ($user->update()) {
            return redirect('/profil')->with('sukses', 'ubah');
        } else {
            return redirect('/profil')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'foto_profil' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();
        
        $file_name = time().'.'.$request->foto_profil->getClientOriginalExtension();
        $path = public_path('foto_profil/' . $file_name);

        // $path = str_replace('/', '\\', $path);
        $avatar = $request->file('foto_profil');
        Image::make($avatar->getRealPath())->save($path);
        // return [$file_name, $path]; 

        $user->foto = $file_name;

        if ($user->update()) {
            return back()->with('sukses', 'upload');
        } else {
            return back()->with('gagal', 'upload');
        }

    }

    public function password() {
        return view('profil.change_password');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6',
            'new_password' => 'required|min:6|confirmed',
        ]);

        $user = Auth::user();
        
        if (Hash::check($request->password, $user->password)) {
            if ($request->new_password == $request->new_password_confirmation){
                $user->password = Hash::make($request->new_password);
                if ($user->save()) {
                    return redirect('/profil')->with('sukses', 'password');
                } else {
                    return redirect('/profil')->with('gagal', 'password');
                }
            } else {
                return redirect('/profil/password')->with('unmatch', 'password');
            }
        } else {
            return redirect('/profil/password')->with('salah', 'password');
        }
    }
}
