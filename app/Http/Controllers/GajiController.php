<?php

namespace App\Http\Controllers;

use DB;
use Datetime;

use App\Arus;
use App\Akun;
use App\Bank;
use App\User;
use App\Util;
use App\Jurnal;
use App\Perkap;
use App\PiutangLain;
use App\BayarPiutangLain;

use Illuminate\Http\Request;

class GajiController extends Controller
{
    public function lastJson()
    {
        $beban = Jurnal::where('referensi', 'like', '%%%%/BGK/%%%%%%%')
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['beban' => $beban]);
    }

    public function piutangJson($id)
    {
        $piutangs = PiutangLain::where('user_id', $id)->where('sisa', '>', 0)->get();
        return response()->json(['piutangs' => $piutangs]);
    }

    public function sisaPiutangJson($id)
    {
        $sisa = PiutangLain::find($id)->sisa;
        return response()->json(['sisa' => $sisa]);
    }

    /* public function index()
    {
        // $users = user::whereIN('level_id', [3,4,5,6])->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $banks = Bank::all();
        $gaji_terbayar = Jurnal::where('kode_akun', Akun::BebanGaji)
                    ->where('referensi', 'like', '%%%%/BGK/%%%%%%%')
                    ->where('debet', '!=', NULL)
                    ->get();
        foreach ($gaji_terbayar as $key => $gaji) {
            $nama = explode('|', $gaji->keterangan);
            $gaji->nama = $nama[1];
            $gaji->keterangan = $nama[0];
        }
        return view('gaji.index', compact('users', 'banks', 'gaji_terbayar'));
    } */

    public function index()
    {
        // $users = user::whereIN('level_id', [3,4,5,6])->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $banks = Bank::all();
        return view('gaji.index', compact('users', 'banks'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'jurnals.' . $field;

        $kode_akuns = [
            Akun::BebanGaji,
        ];

        $gajis = Jurnal
            ::select(
                'jurnals.id',
                'jurnals.debet',
                'jurnals.referensi',
                'jurnals.keterangan'
            )
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.debet', '!=', null)
            ->where('referensi', 'like', '%/BGK/%')
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Jurnal
            ::select(
                'jurnals.id'
            )
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.debet', '!=', null)
            ->where('referensi', 'like', '%/BGK/%')
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.referensi', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($gajis as $i => $gaji) {

            $gaji->debet = Util::duit($gaji->debet);

            $keterangans = explode('|', $gaji->keterangan);
            $gaji->keterangan = $keterangans[0];
            $gaji->karyawan = $keterangans[1];

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $gajis,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request)
    {
        return $request->all();
        $nominal = $request->nominal_tunai + $request->nominal_transfer + $request->piutang_nominal;
        $nominal_kas = $request->nominal_tunai + $request->nominal_transfer;
        $keterangan = $request->keterangan.' '.$request->nama_karyawan;
        
        Jurnal::create([
            'kode_akun' => Akun::BebanGaji,
            'referensi' => $request->kode_transaksi,
            'keterangan' => $keterangan,
            'debet' => $nominal
        ]);

        if($request->nominal_tunai > 0){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->nominal_tunai;
            $akun_tunai->update();
            // create jurnal akun tunai
            Jurnal::create([
                'kode_akun' => Akun::KasTunai,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $request->nominal_tunai
            ]);
        }

        if($request->nominal_transfer > 0){
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet = $akun_bank->debet - $request->nominal_transfer;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::find($request->bank_transfer);
            $bank->nominal -= $request->nominal_transfer;
            $bank->update();
            //jurnal kas bank kredit
            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $request->nominal_transfer
            ]);
        }

        if($request->piutang_nominal > 0){
            //ngurangin piutang lain di AKun
            $akun_piutang = Akun::where('kode', Akun::PiutangKaryawan)->first();
            $akun_piutang->debet -= $request->piutang_nominal;
            $akun_piutang->update();

            // ngurangin sisa di piutang lain
            $piutang = PiutangLain::find($request->piutang_id);
            $piutang->sisa -= $request->piutang_nominal;
            $piutang->update();
            //jurnal piutang 
            Jurnal::create([
                'kode_akun' => Akun::PiutangKaryawan,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $request->piutang_nominal
            ]);

            $bayar_piutang_lain = new BayarPiutangLain();
            $bayar_piutang_lain->kode_transaksi = $request->kode_transaksi;
            $bayar_piutang_lain->piutang_lain_id = $piutang->id;
            $bayar_piutang_lain->nominal = $request->piutang_nominal;
            $bayar_piutang_lain->metode_pembayaran = 'gaji';
            $bayar_piutang_lain->save();
        }

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -=  $nominal ;
        $akun_laba_ditahan->update();

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode_transaksi.' - L/R',
            'keterangan' => $keterangan,
            'debet' => $nominal 
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanGaji,
            'referensi' => $request->kode_transaksi.' - L/R',
            'keterangan' => $keterangan,
            'kredit' => $nominal 
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $request->kode_transaksi.' - LDT',
            'keterangan' => $keterangan,
            'debet' => $nominal 
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode_transaksi.' - LDT',
            'keterangan' =>  $keterangan,
            'kredit' => $nominal 
        ]);

        Arus::create([
            'nama' => Arus::Beban,
            'nominal' => $nominal_kas
        ]);

        return redirect('gaji')->with('sukses', 'tambah');
    }
}
