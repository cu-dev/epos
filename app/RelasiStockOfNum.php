<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiStockOfNum extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'stock_of_num_id', 'item_id'
	];

	public function stock_of_num()
	{
		return $this->belongsTo('App\StockOfNum');
	}

	public function item()
	{
		return $this->belongsTo('App\Item');
	}
}
