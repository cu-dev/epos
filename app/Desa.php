<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $fillable = [
    	'nama', 'kodepos'
    ];

    public function kecamatan()
    {
    	return $this->belongsTo('App\Kecamatan');
    }

    public function pelanggans()
    {
        return $this->hasMany('App\Pelanggans');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
