<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashBackPembelian extends Model
{
    public function bank_transfers()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bank_kartus()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }
}
