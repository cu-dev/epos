<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyLimit extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nominal'
	];

	public function user()
    {
        return $this->belongsTo('App\User');
    }
}
