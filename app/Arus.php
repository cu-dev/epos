<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arus extends Model
{
    //masuk
    const Modal                     = 'Modal';//masuk
    const PendapatanLain            = 'Pendapatan Lain-lain';//masuk
    const Deposito                  = 'Deposito Pelanggan';//masuk
    const JBD                       = 'Penjualan Barang Dagang';//masuk
    const JualAset                  = 'Penjualan Aset';//masuk
    const ReturPembelian            = 'Retur Pembelian';//masuk
    const PembayaranPiutang         = 'Pembayaran Piutang';//masuk

    //keluar
    const BeliAset                  = 'Pembelian Aset';//keluar
    const Beban                     = 'Biaya Beban';//keluar
    const PBD                       = 'Pembelian Barang Dagang';//keluar
    const PiutangLain               = 'Piutang Lain-lain';//keluar
    const BagiHasil                 = 'Penarikan Bagi Hasil';//keluar
    const Prive                     = 'Prive';//keluar
    const ReturPenjualan            = 'Retur Penjualan';//keluar
    const Kewajiban                 = 'Kewajiban';//keluar

    protected $fillable = [
        'nama', 'nominal'
    ];
}
