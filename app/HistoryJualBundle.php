<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryJualBundle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'relasi_transaksi_penjualan_id', 'item_kode', 'jumlah', 'harga'
    ];

    public function relasi_transaksi_penjualan()
    {
        return $this->belongsTo('App\RelasiTransaksiPenjualan', 'relasi_transaksi_penjualan_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode');
    }
}
