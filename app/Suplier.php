<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suplier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'alamat',
    ];

    const notSuplier = '999';

    public function items()
    {
        return $this->hasMany('App\Item')->orderBy('nama', 'asc');
    }

    public function transaksi()
    {
    	return $this->hasMany('App\Transaksi');
    }

    public function sellers()
    {
        return $this->hasMany('App\Seller')->orderBy('nama', 'asc');
    }
}
