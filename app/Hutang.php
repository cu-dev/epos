<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hutang extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'transaksi_pembelian_id', 'jumlah_bayar', 'nominal_tunai', 'no_transfer', 'nominal_transfer', 'no_cek', 'nominal_cek', 'no_bg', 'nominal_bg'
	];

    public function transaksi_pembelian()
    {
    	return $this->belongsTo('App\TransaksiPembelian');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
