<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiBonus extends Model
{
    protected $fillable = [
        'item_kode', 'bonus_id', 'syarat', 'bonus_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function bonus()
    {
        return $this->belongsTo('App\Item');
    }
}
