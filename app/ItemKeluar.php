<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemKeluar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_masuk_id', 'keterangan', 'jumlah'
    ];

    public function item_masuk()
    {
    	return $this->belongsTo('App\ItemMasuk');
    }

    public function item()
    {
        return $this->belongsToThrough('App\Item', 'App\ItemMasuk');
    }
}
