<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashDrawer extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'nominal', 'transaksi'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
