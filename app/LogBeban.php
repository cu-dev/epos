<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogBeban extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nominal', 'jenis_modal', 'sumber', 'sisa_beban', 'status_beban'
	];

	public function modal()
	{
		return $this->belongsTo('App\Modal');
	}
}
