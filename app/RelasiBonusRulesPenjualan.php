<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiBonusRulesPenjualan extends Model
{
    protected $fillable = [
        'transaksi_penjualan_id', 'bonus_id', 'jumlah'
    ];

    public function transaksi_penjualan()
    {
        return $this->belongsTo('App\transaksi_penjualans');
    }

    public function bonus()
    {
        return $this->belongsTo('App\Item');
    }
}
