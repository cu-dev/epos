<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_bank',
        'no_rekening',
        'atas_nama',
        'nominal',
        'user_id',
    ];

    public function hutang_bank()
    {
        return $this->hasMany('App\HutangBank');
    }

    public function transaksi_penjualans()
    {
        return $this->hasMany('App\TransaksiPenjualan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }    

    public function bank_transfer()
    {
        return $this->hasMany('App\TransaksiPembelian', 'bank_transfer');
    }

    public function bank_cek()
    {
        return $this->hasMany('App\TransaksiPembelian', 'bank_cek');
    }

    public function bank_bg()
    {
        return $this->hasMany('App\TransaksiPembelian', 'bank_bg');
    }

    public function bank_kartu()
    {
        return $this->hasMany('App\TransaksiPembelian', 'bank_kartu');
    }

    public function bank_transferp()
    {
        return $this->hasMany('App\PembayaranReturPenjualan', 'bank_transfer');
    }

    public function bank_kartup()
    {
        return $this->hasMany('App\PembayaranReturPenjualan', 'bank_kartu');
    }

    public function bank_transferp_in()
    {
        return $this->hasMany('App\PembayaranReturPenjualan', 'bank_transfer_in');
    }

    public function bank_kartup_in()
    {
        return $this->hasMany('App\PembayaranReturPenjualan', 'bank_kartu_in');
    }

    public function bank_retur()
    {
        return $this->hasMany('App\ReturPembelian', 'bank_transfer');
    }

    public function kartu_retur()
    {
        return $this->hasMany('App\ReturPembelian', 'bank_kartu');
    }

    public function pembayaran_deposito()
    {
        return $this->hasMany('App\TransaksiPenjualan');
    }

    // public function bp_transfer()
    // {
    //     return $this->hasMany('App\BayarPiutangDagang', 'bank_transfer');
    // }

    // public function bp_kartu()
    // {
    //     return $this->hasMany('App\BayarPiutangDagang', 'bank_kartu');
    // }
}
