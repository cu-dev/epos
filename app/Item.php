<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode', 'kode_barang', 'nama', 'nama_pendek', 'suplier_id', 'jenis_item_id', 'stoktotal', 'konsinyasi', 'aktif', 'retur', 'bonus', 'limit_stok', 'limit_grosir', 'diskon', 'profit', 'profit_eceran', 'rentang', 'valid_konversi'
    ];

    protected $appends = [
        'satuans', 'satuan_pembelians'
    ];

    public function getSatuansAttribute()
    {
        return \App\RelasiSatuan::with('satuan')
            ->where('item_kode', $this->kode)
            ->orderBy('konversi', 'asc')->get();
    }

    public function getSatuanPembeliansAttribute()
    {
        return \App\RelasiSatuan::with('satuan')
            ->where('item_kode', $this->kode)
            ->orderBy('konversi', 'desc')->get();
    }

    public function suplier()
    {
        return $this->belongsTo('App\Suplier');
    }

    public function jenis_item()
    {
        return $this->belongsTo('App\JenisItem');
    }

    public function satuan()
    {
        return $this->belongsToMany('App\Satuan', 'relasi_satuans', 'item_kode', 'satuan_id');
    }

    public function satuan_pembelian()
    {
        return $this->belongsToMany('App\Satuan', 'relasi_satuans', 'item_kode', 'satuan_id')->orderBy('konversi', 'desc');
    }

    public function item_masuk()
    {
        return $this->hasMany('App\ItemMasuk');
    }

    public function item_keluar()
    {
        return $this->hasManyThrough('App\ItemKeluar', 'App\ItemMasuk');
    }

    public function harga()
    {
        return $this->hasMany('App\Harga', 'item_kode', 'kode');
    }

    public function stoks()
    {
        return $this->hasMany('App\Stok');
    }

    public function stok()
    {
        return $this->hasMany('App\Stok', 'item_kode', 'kode');
    }

    public function transaksi_penjualan()
    {
        return $this->belongsToMany('App\TransaksiPenjualan', 'relasi_transaksi_penjualans', 'item_id', 'transaksi_id');
    }

    public function transaksi_pembelians()
    {
        return $this->belongsToMany('App\TransaksiPembelian', 'relasi_transaksi_pembelians')
            ->withPivot('jumlah')
            ->withTimestamps();
    }

    public function retur_pembelians()
    {
        return $this->belongsToMany('App\ReturPembelian', 'relasi_retur_pembelians')
            ->withPivot('jumlah', 'subtotal', 'keterangan')
            ->withTimestamps();
    }

    public function stock_of_nums()
    {
        return $this->belongsToMany('App\StockOfNum', 'relasi_stock_of_nums')
            ->withPivot('stok_sistem', 'stok_fisik', 'keterangan')
            ->withTimestamps();
    }

    public function itemcc()
    {
        return $this->hasMany('App\RelasiBundle', 'item_child', 'kode');
        // return $this->hasMany('App\Harga', 'item_kode', 'kode');
    }
    
    public function isValidKonversi()
    {
        return $this->valid_konversi == 1 ? true : false;
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function valids()
    {
        return $this->belongsTo('App\User', 'valid_id');
    }

}
