<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiPenjualan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_transaksi',
        'harga_total',
        'nego_total',
        'check_nego_total',
        'potongan_penjualan',
        'ongkos_kirim',
        'jumlah_bayar',
        'status',

        'nominal_tunai',

        'no_transfer',
        'nominal_transfer',
        'bank_transfer',

        'no_kartu',
        'jenis_kartu',
        'nominal_kartu',
        'bank_kartu',

        'no_cek',
        'nominal_cek',
        'bank_cek',

        'no_bg',
        'nominal_bg',
        'bank_bg',

        'nominal_titipan',

        'jatuh_tempo',
        'pelanggan_id',
        'alamat',
        'pengirim',
        'user_id',
        'grosir_id',
        'can_retur'
    ];

    public function items()
    {
        $this->belongsToMany('App\Item', 'relasi_transaksi_penjualans', 'item_kode', 'kode')
            ->withPivot('jumlah', 'jumlah_bonus')
            ->withTimeStamps();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function grosir()
    {
        return $this->belongsTo('App\User');
    }

    public function piutang_dagang()
    {
        return $this->hasMany('App\PiutangDagang');
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }

    // public function bank()
    // {
    //     return $this->belongsTo('App\Bank');
    // }

    public function retur_penjualan()
    {
        return $this->hasMany('App\ReturPenjualan');
    }

    public function banktransfer()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bankkartu()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }

    public function bankcek()
    {
        return $this->belongsTo('App\Bank', 'bank_cek');
    }

    public function bankbg()
    {
        return $this->belongsTo('App\Bank', 'bank_bg');
    }
}
