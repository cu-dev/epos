<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiSatuan extends Model
{
    protected $fillable = [
        'item_kode', 'satuan_id', 'konversi',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Satuan');
    }
}
