<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BG extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bgs';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nomor', 'nominal', 'aktif'
	];
}
