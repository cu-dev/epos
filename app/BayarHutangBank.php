<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayarHutangBank extends Model
{
	protected $fillable = [
        'hutang_lain_id', 'nominal', 'metode_pembayaran', 'nomer_trasnfer'
    ];

    public function hutang_bank()
    {
        return $this->belongsTo('App\HutangBank');
    }    
}