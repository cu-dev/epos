<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode', 'nama',
    ];

    public function item()
    {
    	return $this->hasMany('App\Item', 'jenis_item_id');
    }
}
