<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturPembelian extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_retur',
        'nota',
        'transaksi_pembelian_id',
        'suplier_id',
        'status',
        'harga_total',
        'total_uang',

        'nominal_tunai',

        'no_transfer',
        'nominal_transfer',
        'bank_transfer',

        'no_cek',
        'nominal_cek',
        'bank_cek',
        'cek_lunas',

        'no_bg',
        'nominal_bg',
        'bank_bg',
        'bg_lunas',

        'no_kartu',
        'bank_kartu',
        'jenis_kartu',
        'nominal_kartu',

        'nominal_piutang',

        'hutang_id',
        'nominal_hutang',

        'user_id',
    ];

    public function transaksi_pembelian()
    {
    	return $this->belongsTo('App\TransaksiPembelian');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'relasi_retur_pembelians')
            ->withPivot('jumlah', 'subtotal', 'keterangan')
            ->withTimestamps();
    }

    public function bank_returs()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function kartu_returs()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
