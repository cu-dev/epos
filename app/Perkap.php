<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perkap extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode',
        'nama',
        'harga',
        'nominal',
        'residu',
        'umur',
        'status',
        'no_transaksi',
        'keterangan',
    ];

}
