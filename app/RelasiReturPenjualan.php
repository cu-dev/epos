<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiReturPenjualan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'retur_penjualan_id', 'item_kode', 'jumlah', 'subtotal', 'keterangan'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function retur_penjualan()
    {
        return $this->belongsTo('App\ReturPenjualan');
    }
}
