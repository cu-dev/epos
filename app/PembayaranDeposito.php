<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranDeposito extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function grosir()
    {
        return $this->belongsTo('App\User');
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }

    public function banktransfer()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bankkartu()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }

    public function bankcek()
    {
        return $this->belongsTo('App\Bank', 'bank_cek');
    }

    public function bankbg()
    {
        return $this->belongsTo('App\Bank', 'bank_bg');
    }
}
