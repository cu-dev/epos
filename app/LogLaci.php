<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogLaci extends Model
{
	protected $fillable = [
		'pengirim', 'penerima', 'approve', 'nominal', 'status', 'keterangan'
	];

    public function sender()
	{
		return $this->belongsTo('App\User', 'pengirim');
	}

	public function receiver()
	{
		return $this->belongsTo('App\User', 'penerima');
	}
}
