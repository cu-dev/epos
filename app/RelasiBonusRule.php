<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiBonusRule extends Model
{
    protected $fillable = [
        'bonus_rules_id', 'bonus_id', 'jumlah'
    ];

    public function bonus()
    {
        return $this->belongsTo('App\Item');
    }

    public function rule()
    {
        return $this->belongsTo('App\BonusRule');
    }
}
