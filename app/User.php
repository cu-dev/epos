<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const foto_default = 'default.png';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'nama',
        'email',
        'alamat',
        'telepon',
        'tanggal_lahir',
        'foto',
        'level_id',
        'status',
        'cetak_nota',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }

    public function bayar_piutang_dagang()
    {
        return $this->hasMany('App\BayarPiutangDagang');
    }

    public function transaksi()
    {
        return $this->hasMany('App\TransaksiPenjualan', 'user_id');
    }

    public function retur_penjualan()
    {
        return $this->hasMany('App\ReturPenjualan', 'user_id');
    }

    public function cash_drawer()
    {
        return $this->hasMany('App\CashDrawer');
    }

    public function setoran_kasir()
    {
        return $this->hasMany('App\SetoranKasir');
    }

    public function setoran_buka()
    {
        return $this->hasMany('App\SetoranBuka', 'user_id');
    }

    public function operator()
    {
        return $this->hasMany('App\SetoranBuka', 'operator', 'id');
    }

    public function penyesuaian_persediaan()
    {
        return $this->hasMany('App\PenyesuaianPersediaan', 'user_id');
    }

    public function money_limit()
    {
        return $this->hasMany('App\MoneyLimit', 'user_id');
    }

    public function periode()
    {
        return $this->hasMany('App\PeriodeOrder', 'user_id');
    }

    public function bank()
    {
        return $this->hasMany('App\Bank', 'user_id');
    }

    public function kredit()
    {
        return $this->hasMany('App\Kredit', 'user_id');
    }

    // public function laci()
    // {
    //  return $this->hasMany('App\LaciGrosir', 'user_id');
    // }

    public function stock_of_num()
    {
        return $this->hasMany('App\StockOfNum', 'user_id');
    }

    public function perprofits()
    {
        return $this->hasMany('App\Persentase', 'user_id');
    }
}
