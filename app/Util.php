<?php

namespace App;

use DB;
use Auth;
use Hash;
use DateTime;
use DateInterval;

use App\Akun;
use App\Stok;
use App\User;
use App\Util;
use App\Saldo;
use App\Config;
use App\Jurnal;
use App\Item;
use App\Perkap;
use App\Bangunan;
use App\Kendaraan;
use App\BukaTahun;
use App\Http\Requests;
use App\PersediaanAwal;

class Util
{
    public static function duit3($nominal)
    {
        if ($nominal == 0) return '-';
        return 'Rp'.number_format($nominal, 3, ',', '.');
    }

    public static function duit($nominal)
    {
        if ($nominal == 0) return '-';
        return 'Rp'.number_format($nominal, 2, ',', '.');
    }

    public static function duit2($nominal)
    {
        if ($nominal == 0) return 'Rp0,00';
        return 'Rp'.number_format($nominal, 2, ',', '.');
    }

    public static function duit0($nominal)
    {
        if ($nominal == 0) return 'Rp0';
        return 'Rp'.number_format($nominal, 0, ',', '.');
    }

    public static function ewon($nominal)
    {
        return 'Rp'.number_format($nominal, 0, ',', '.');
    }

    public static function angka($nominal)
    {
        return number_format($nominal, 0, ',', '.');
    }

    public static function angka2($nominal)
    {
        if ($nominal == 0) return '0,00';
        return number_format($nominal, 2, ',', '.');
    }

    //angka koma decimal to view, 1.950 -> 1,95
    public static function angka_koma($angka){
        $angka = $angka+0;
        $angka = $angka.'';

        $a = explode('.', $angka);

        if(sizeof($a) > 1){
            $result = $a[0].','.$a[1];
        }else{
            $result = $angka;
        }

        return $result;
    }

    public static function pembulatan($angka)
    {
        return round($angka, 2);
    }

    public static function DK($nominal)
    {
        if ($nominal > 0) return number_format($nominal, 2, ',', '.');
        else return '-';
    }

    public static function int4digit($i)
    {
        if ($i < 10) {
            return '000'.$i;
        } else if ($i < 100) {
            return '00'.$i;
        } else if ($i < 1000) {
            return '0'.$i;
        } else {
            return $i;
        }
    }

    public static function int6digit($i)
    {
        if ($i < 10) {
            return '00000'.$i;
        } else if ($i < 100) {
            return '0000'.$i;
        } else if ($i < 1000) {
            return '000'.$i;
        } else if ($i < 10000) {
            return '00'.$i;
        } else if ($i < 100000) {
            return '0'.$i;
        } else {
            return $i;
        }
    }

    public static function printBulanSekarang($format)
    {
        $today = date('Y-m-d');
        $mm = explode('-', $today)[1];
        $yyyy = explode('-', $today)[0];

        if ($format == 'mm/yyyy') {
            return $mm.'/'.$yyyy;
        }
    }

    //inputan "08 2017"
    public static function tanggalan($bulan_tahun)
    {
        $bulans = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        return $bulans[intval(explode(' ', $bulan_tahun)[0]) - 1] . ' '. explode(' ', $bulan_tahun)[1];
    }

    //inputan "1 08 2019"
    public static function tanggal($tanggal)
    {
        $bulans = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        return explode(' ', $tanggal)[0].' '.$bulans[intval(explode(' ', $tanggal)[1]) - 1] . ' '. explode(' ', $tanggal)[2];
    }

    // inputan '01-02-1995'
    public static function date($tanggal)
    {
        // return explode('-', $tanggal)[2].'-'.explode('-', $tanggal)[1].'-'explode('-', $tanggal)[0];
        if (count(explode('-', $tanggal)) < 3) return '';
        return explode('-', $tanggal)[2].'-'.explode('-', $tanggal)[1].'-'.explode('-', $tanggal)[0];
    }

    public static function akun(){
        //Aset
        $aset = Akun::where('kode', Akun::Aset)->first();
        //aset lancar
        $aset_lancar = Akun::where('kode', Akun::AsetLancar)->first();
        //kumpulan aset lancar
        $k_aset_lancar = Akun::where('kode', Akun::KasTunai)
                            ->orWhere('kode', Akun::KasKecil)
                            ->orWhere('kode', Akun::KasBank)
                            ->orWhere('kode', Akun::KasCek)
                            ->orWhere('kode', Akun::KasBG)
                            ->orWhere('kode', Akun::Persediaan)
                            ->orWhere('kode', Akun::Perlengkapan)
                            ->orWhere('kode', Akun::PiutangDagang)
                            ->orWhere('kode', Akun::PiutangKaryawan)
                            ->orWhere('kode', Akun::PiutangLain)
                            ->orWhere('kode', Akun::PiutangTakTertagih)
                            ->orWhere('kode', Akun::AsuransiDimuka)
                            ->orWhere('kode', Akun::SewaDimuka)
                            ->orWhere('kode', Akun::PPNMasukan)
                            ->get();
        $nilai_aset_lancar = 0;
        foreach ($k_aset_lancar as $key => $value) {
            $nilai_aset_lancar += $value->debet;
        }
        //update nilai aset lancar
        $aset_lancar->debet = $nilai_aset_lancar;
        $aset_lancar->update();

        //aset Tetap
        $aset_tetap = Akun::where('kode', Akun::AsetTetap)->first();
        //kumpulan aset Tetap debet
        $p_aset_tetap = Akun::where('kode', Akun::Tanah)
                            ->orWhere('kode', Akun::Bangunan)
                            ->orWhere('kode', Akun::Peralatan)
                            ->orWhere('kode', Akun::Kendaraan)
                            ->get();
        $nilai_p_tetap = 0;
        foreach ($p_aset_tetap as $key => $value) {
            $nilai_p_tetap += $value->debet;
        }

        //kumpulan aset Tetap kredit
        $k_aset_tetap = Akun::where('kode', Akun::ADBangunan)
                            ->orWhere('kode', Akun::ADPeralatan)
                            ->orWhere('kode', Akun::ADKendaraan)
                            ->get();
        $nilai_k_tetap = 0;
        foreach ($k_aset_tetap as $key => $value) {
            $nilai_k_tetap += $value->kredit;
        }

        //update nilai aset tetap
        $aset_tetap->debet = $nilai_p_tetap - $nilai_k_tetap;
        $aset_tetap->update();

        //update nilai aset
        $aset->debet = $aset_lancar->debet + $aset_tetap->debet;
        $aset->update();

        //akun kewajiban
        $kewajiban = Akun::where('kode', Akun::Kewajiban)->first();
        //akun hutang pendek
        $hutang_pendek = Akun::where('kode', Akun::HutangJangkaPendek)->first();
        //kumpulan hutang pendek
        $k_hutang_pendek = Akun::where('kode', Akun::KartuKredit)
                                ->orWhere('kode', Akun::HutangDagang)
                                ->orWhere('kode', Akun::HutangPajak)
                                ->orWhere('kode', Akun::TaksiranUtangGaransi)
                                ->orWhere('kode', Akun::HutangKonsinyasi)
                                ->orWhere('kode', Akun::PendapatanDimuka)
                                ->orWhere('kode', Akun::HutangPPN)
                                ->orWhere('kode', Akun::PPNKeluaran)
                                ->orWhere('kode', Akun::PendapatanDimuka)
                                ->orWhere('kode', Akun::PembayaranProses)
                                ->get();
        $nilai_k_hut_pendek = 0;
        foreach ($k_hutang_pendek as $key => $value) {
            $nilai_k_hut_pendek += $value->kredit;
        }
        //update hutang pendek
        $hutang_pendek->kredit = $nilai_k_hut_pendek;
        $hutang_pendek->update();
        //akun hutang panjang
        $hutang_panjang = Akun::where('kode', Akun::HutangJangkaPanjang)->first();
        //kumpulan hutang pendek
        $hutangBank = Akun::where('kode', Akun::HutangBank)->first();
        //update utang panjang
        $hutang_panjang->kredit = $hutangBank->kredit;
        $hutang_panjang->update();
        //update kewajiban
        $kewajiban->kredit = $hutang_pendek->kredit + $hutang_panjang->kredit;
        $kewajiban->update();

        //pendapatan
        $pendapatan = Akun::where('kode', Akun::Pendapatan)->first();
        //kumpulan pendapatan
        $k_pendapatan = Akun::where('kode', Akun::Penjualan)
                            ->orWhere('kode', Akun::PenjualanKredit)
                            ->orWhere('kode', Akun::PendapatanLain)
                            ->orWhere('kode', Akun::PotonganPembelian)
                            ->orWhere('kode', Akun::ReturPembelian)
                            ->orWhere('kode', Akun::UntungPersediaan)
                            ->get();
        $nilai_k_pendapatan = 0;
        foreach ($k_pendapatan as $key => $value) {
            $nilai_k_pendapatan += $value->kredit;
        }
        //kumpulan debet pendapatan
        $d_pendapatan = Akun::where('kode', Akun::PotonganPenjualan)
                            ->orWhere('kode', Akun::ReturPenjualan)
                            ->orWhere('kode', Akun::HPP)
                            ->get();

        $nilai_d_pendapatan = 0;
        foreach ($d_pendapatan as $key => $value) {
            $nilai_d_pendapatan += $value->debet;
        }

        $pendapatan->kredit = $nilai_k_pendapatan - $nilai_d_pendapatan;
        $pendapatan->update();
        //beban
        $beban = Akun::where('kode', Akun::Beban)->first();
        //kumpulan beban
        $k_beban = Akun::where('kode', Akun::BebanSewa)
                        ->orWhere('kode', Akun::BebanIklan)
                        ->orWhere('kode', Akun::BebanPerlengkapan)
                        ->orWhere('kode', Akun::BebanPerawatan)
                        ->orWhere('kode', Akun::BebanKerugianPiutang)
                        ->orWhere('kode', Akun::BebanKerugianPersediaan)
                        ->orWhere('kode', Akun::BebanRugiAset)
                        ->orWhere('kode', Akun::BebanDepBangunan)
                        ->orWhere('kode', Akun::BebanDepPeralatan)
                        ->orWhere('kode', Akun::BebanDepKendaraan)
                        ->orWhere('kode', Akun::BebanAsuransi)
                        ->orWhere('kode', Akun::BebanGaji)
                        ->orWhere('kode', Akun::BebanAdministrasiBank)
                        ->orWhere('kode', Akun::BebanDenda)
                        ->orWhere('kode', Akun::BebanUtilitas)
                        ->orWhere('kode', Akun::BebanOngkir)
                        ->orWhere('kode', Akun::BebanTransportasi)
                        ->orWhere('kode', Akun::BebanGaransi)
                        ->orWhere('kode', Akun::BebanPajak)
                        ->orWhere('kode', Akun::BebanLain)
                        ->orWhere('kode', Akun::BebanBunga)
                        ->get();
                        
        $kumpulan_beban = 0;
        foreach ($k_beban as $key => $value) {
            $kumpulan_beban += $value->debet;
        }
        //update beban
        $beban->debet = $kumpulan_beban;
        $beban->update();
    }

    public static function make_saldo(){
        //cek kapan modal awal masuk
        self::akun();
        $modal = Akun::where('kode', Akun::Modal)->first();
        $saat_ini = new DateTime();
        if($modal->created_at->format('m-Y') != $saat_ini->format('m-Y')){
            //untuk bikin saldo
            $saat_ini = new DateTime();
            $saat_ini->modify('-1 month');
            $month_s = $saat_ini->format('m');
            $year_s = $saat_ini->format('Y');
            $tanggal = $saat_ini->format('Y-m-t');

            $cek = Saldo::whereMonth('created_at', '=', $month_s)
                          ->whereYear('created_at', '=', $year_s)
                          ->first();
            $akuns = Akun::all();
            if($cek == NULL){
                foreach ($akuns as $value) {
                    $data = new Saldo();
                    $data->kode_akun = $value->kode;
                    $data->debet = $value->debet;
                    $data->kredit = $value->kredit;
                    $data->created_at = $tanggal;

                    $data->save();
                }
            }
        }
    }

    public static function fix_aset(){
        $sekarang = new Datetime();
        $tahun_ini = $sekarang->format('Y');

        $buka_tahun = BukaTahun::whereYear('created_at', '=', $tahun_ini)->first();

        // return $buka_tahun;
        $awal = new DateTime('first day of January');
        $awal_tahun = $awal->format('d/m/Y');
        $temp_tahun = $awal->format('Y');

        if($buka_tahun==NULL){
            $open = new BukaTahun();
            $open->save();
           
            //kurangi akun
            $a_bangunan = Akun::where('kode', Akun::Bangunan)->first();
            $a_peralatan = Akun::where('kode', Akun::Peralatan)->first();
            $a_kendaraan = Akun::where('kode', Akun::Kendaraan)->first();

            $ad_bangunan = Akun::where('kode', Akun::ADBangunan)->first();
            $ad_peralatan = Akun::where('kode', Akun::ADPeralatan)->first();
            $ad_kendaraan = Akun::where('kode', Akun::ADKendaraan)->first();

            //update nominal aset tetap
            $tabel_bangunan = Bangunan::find(1);
            $tabel_bangunan->nominal -= $ad_bangunan->kredit;
            $tabel_bangunan->update();

            $a_bangunan->debet -= $ad_bangunan->kredit;
            $a_bangunan->update(); 

            $a_kendaraan->debet -= $ad_kendaraan->kredit;
            $a_kendaraan->update();

            $a_peralatan->debet -= $ad_peralatan->kredit;
            $a_peralatan->update();

            //jurnal pengurangan
            if($ad_bangunan->kredit != NULL || $ad_bangunan->kredit != 0){
                $j_adbangunan = new Jurnal();
                $j_adbangunan->kode_akun = Akun::ADBangunan;
                $j_adbangunan->referensi = 'PNAB/'.$awal_tahun;
                $j_adbangunan->debet = $ad_bangunan->kredit;
                $j_adbangunan->keterangan = 'penyesuaian nominal aset';
                $j_adbangunan->created_at = $awal;
                $j_adbangunan->save();

                $j_bangunan = new Jurnal();
                $j_bangunan->kode_akun = Akun::Bangunan;
                $j_bangunan->referensi = 'PNAB/'.$awal_tahun;
                $j_bangunan->kredit = $ad_bangunan->kredit;
                $j_bangunan->keterangan = 'penyesuaian nominal aset';
                $j_bangunan->created_at = $awal;
                $j_bangunan->save();
            }

            $j_adperalatan = new Jurnal();
            $j_adperalatan->kode_akun = Akun::ADPeralatan;
            $j_adperalatan->referensi = 'PNAP/'.$awal_tahun;
            $j_adperalatan->debet = $ad_peralatan->kredit;
            $j_adperalatan->keterangan = 'penyesuaian nominal aset';
            $j_adperalatan->created_at = $awal;
            $j_adperalatan->save();

            $j_peralatan = new Jurnal();
            $j_peralatan->kode_akun = Akun::Peralatan;
            $j_peralatan->referensi = 'PNAP/'.$awal_tahun;
            $j_peralatan->kredit = $ad_peralatan->kredit;
            $j_peralatan->keterangan = 'penyesuaian nominal aset';
            $j_peralatan->created_at = $awal;
            $j_peralatan->save();

            $j_adkendaraan = new Jurnal();
            $j_adkendaraan->kode_akun = Akun::ADKendaraan;
            $j_adkendaraan->referensi = 'PNAK/'.$awal_tahun;
            $j_adkendaraan->debet = $ad_kendaraan->kredit;
            $j_adkendaraan->keterangan = 'penyesuaian nominal aset';
            $j_adkendaraan->created_at = $awal;
            $j_adkendaraan->save();

            $j_kendaraan = new Jurnal();
            $j_kendaraan->kode_akun = Akun::Kendaraan;
            $j_kendaraan->referensi = 'PNAK/'.$awal_tahun;
            $j_kendaraan->kredit = $ad_kendaraan->kredit;
            $j_kendaraan->keterangan = 'penyesuaian nominal aset';
            $j_kendaraan->created_at = $awal;
            $j_kendaraan->save();


            $ad_bangunan->kredit = 0;
            $ad_bangunan->update(); 

            $ad_kendaraan->kredit = 0;
            $ad_kendaraan->update();

            $ad_peralatan->kredit = 0;
            $ad_peralatan->update();

            $peralatans = Perkap::where('umur', '>', 0)->where('status' , 1)->get();
            $kendaraans = Kendaraan::where('rusak', 1)->get();            

            $saat_ini = new DateTime();
            $saat_ini->modify('-1 year');
            $year = $saat_ini->format('Y');

            foreach ($peralatans as $peralatan) {
                $jurnal = Jurnal::select(DB::raw('SUM(debet) as jumlah'))
                                ->where('referensi', $peralatan->kode)
                                ->whereYear('created_at', $year)
                                ->first()->jumlah;

                $peralatan->nominal -= $jurnal;
                $peralatan->update();
            }

            foreach ($kendaraans as $kendaraan) {
                $jurnal = Jurnal::select(DB::raw('SUM(debet) as jumlah'))
                                ->where('referensi', $kendaraan->nopol)
                                ->whereYear('created_at', $year)
                                ->first()->jumlah;

                $kendaraan->nominal -= $jurnal;
                $kendaraan->update();
            }

            $ppn_keluar = Akun::where('kode', Akun::PPNKeluaran)->first();
            $ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();

            $selisih = $ppn_keluar->kredit - $ppn_masuk->debet;
            if($selisih > 0){
                //jurnal pindah ke hutang pembayaran dan 0 kan tiap ppn
                Jurnal::create([
                    'kode_akun' => Akun::PPNKeluaran,
                    'referensi' => 'PPN/PENYESUAIAN '.$temp_tahun,
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'debet' => $ppn_keluar->kredit,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::PPNMasukan,
                    'referensi' => 'PPN/PENYESUAIAN '.$temp_tahun,
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $ppn_masuk->debet,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::HutangPPN,
                    'referensi' => 'PPN/PENYESUAIAN '.$temp_tahun,
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $selisih,
                ]);
                //jurnal Pendapatan Lain
                Jurnal::create([
                    'kode_akun' => Akun::HutangPPN,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - PLL',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'debet' => $selisih,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::PendapatanLain,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - PLL',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $selisih,
                ]);
                //jurnal LABA-RUGI
                Jurnal::create([
                    'kode_akun' => Akun::PendapatanLain,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - L/R',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'debet' => $selisih,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - L/R',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $selisih,
                ]);
                //JURNAL laba ditahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - LDT',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'debet' => $selisih,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => 'PPN/PENYESUAIAN/ '.$temp_tahun.' - LDT',
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $selisih,
                ]);

                //SESUAIKAN AKUN
                $ppn_keluar->kredit = 0;
                $ppn_masuk->debet = 0;
                $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $laba_tahan->kredit += $selisih;

                $ppn_keluar->update();
                $ppn_masuk->update();
                $laba_tahan->update();
            }else{
                Jurnal::create([
                    'kode_akun' => Akun::PPNKeluaran,
                    'referensi' => 'PPN/PENYESUAIAN '.$temp_tahun,
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'debet' => $ppn_keluar->kredit,
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::PPNMasukan,
                    'referensi' => 'PPN/PENYESUAIAN '.$temp_tahun,
                    'keterangan' => 'Penyesuaian PPN Tahun '.$temp_tahun,
                    'kredit' => $ppn_keluar->kredit,
                ]);

                $ppn_masuk->debet -= $ppn_keluar->kredit;
                $ppn_keluar->kredit -= $ppn_keluar->kredit;

                $ppn_keluar->update();
                $ppn_masuk->update();
            }
        }
    }

    public static function depresiasi(){
        $date = date('Y-m-d');
        $today = date('m');
        $timeEnd = strtotime("$date");

        $peralatans = Perkap::where('umur', '>', 0)->where('status' , 1)->get();
        $kendaraans = Kendaraan::where('rusak', 1)->get();
        $bangunan = Bangunan::find(1);

        //untuk depresiasi peralatan            
        for($i=0; $i<sizeof($peralatans); $i++){
            $timeStart = strtotime($peralatans[$i]->created_at); 
            // Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
            //menghitung selisih bulan
            $numBulan += date("m",$timeEnd)-date("m",$timeStart);
            
            //ngitung depresiasi per bulan
            $umur_alat = $peralatans[$i]->umur*12;
            $depresiasi = ($peralatans[$i]->harga - $peralatans[$i]->residu) / $umur_alat;

            $depresiasi = self::pembulatan($depresiasi);
            
            $x = 0;
            for($a=0; $a<$numBulan; $a++){
                $x += 1;
                if($x == $umur_alat){
                    $nilai_depresiasi_akhir = ($peralatans[$i]->harga - $peralatans[$i]->residu) - ($depresiasi * ($umur_alat - 1));
                    $depresiasi = self::pembulatan($nilai_depresiasi_akhir);
                }

                $awal = new DateTime($peralatans[$i]->created_at); 
                $interval = "P".$a."M";
                $awal->add(new DateInterval($interval));
                $star = $awal->format('Y-m-t');
                $tahun = $awal->format('Y');
                $bulan = $awal->format('m');

                $keterangan = "beban depresiasi alat ".$peralatans[$i]->nama." ".Util::tanggalan($awal->format('m Y'));

                $cek = Jurnal::where('kode_akun', Akun::BebanDepPeralatan)
                                ->where('referensi', $peralatans[$i]->kode)
                                ->whereMonth('updated_at', '=', $bulan)
                                ->whereYear('updated_at', '=', $tahun)
                                ->first();

                if($cek==NULL && $depresiasi > 0){
                    $jurnal_beban_depresiasi = new Jurnal();
                    $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepPeralatan;
                    $jurnal_beban_depresiasi->referensi = $peralatans[$i]->kode;
                    $jurnal_beban_depresiasi->debet = $depresiasi;
                    $jurnal_beban_depresiasi->keterangan = $keterangan;
                    $jurnal_beban_depresiasi->updated_at = $star;
                    $jurnal_beban_depresiasi->save();

                    $jurnal_akumulasi_depresiasi = new Jurnal();
                    $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADPeralatan;
                    $jurnal_akumulasi_depresiasi->referensi = $peralatans[$i]->kode;
                    $jurnal_akumulasi_depresiasi->kredit = $depresiasi;
                    $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                    $jurnal_akumulasi_depresiasi->updated_at = $star;
                    $jurnal_akumulasi_depresiasi->save();

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $peralatans[$i]->kode.' - L/R',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::BebanDepPeralatan,
                        'referensi' => $peralatans[$i]->kode.' - L/R',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $peralatans[$i]->kode.' - LDT',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $peralatans[$i]->kode.' - LDT',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi
                    ]);

                    $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                    $akun_laba_ditahan->kredit -=  $depresiasi;
                    $akun_laba_ditahan->update();

                    // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepPeralatan)->first();
                    // $akun_beban_depresiasi->debet += $depresiasi;
                    // $akun_beban_depresiasi->update();

                    $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADPeralatan)->first();
                    $akun_akumulasi_depresiasi->kredit += $depresiasi;
                    $akun_akumulasi_depresiasi->update();
                }

                if($x == $umur_alat){
                    break;
                }
            }
        }
        //untuk depresiasi Kendaraan
        for($i=0; $i<sizeof($kendaraans); $i++){
            $timeStart = strtotime($kendaraans[$i]->created_at); 
            // Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
            //menghitung selisih bulan
            $numBulan += date("m",$timeEnd)-date("m",$timeStart);
            //ngitung depresiasi per bulan
            $umur_kendaraan = $kendaraans[$i]->umur*12;
            $depresiasi = ($kendaraans[$i]->harga - $kendaraans[$i]->residu) / $umur_kendaraan;

            $depresiasi = self::pembulatan($depresiasi);

            $x = 0;
            for($a=0; $a<$numBulan; $a++){
                $x += 1;
                if($x == $umur_kendaraan){
                    // $beban = DB::table('jurnals')
                    //         ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                    //         ->where('kode_akun', Akun::BebanDepKendaraan)
                    //         ->where('referensi', $kendaraans[$i]->nopol)
                    //         ->first()->jumlah_beban;
                    $nilai_depresiasi_akhir = ($kendaraans[$i]->harga - $kendaraans[$i]->residu) - ($depresiasi * ($umur_kendaraan - 1));
                    $depresiasi = self::pembulatan($nilai_depresiasi_akhir);
                }

                $awal = new DateTime($kendaraans[$i]->created_at); 
                $interval = "P".$a."M";
                $awal->add(new DateInterval($interval));
                $star = $awal->format('Y-m-t');
                $tahun = $awal->format('Y');
                $bulan = $awal->format('m');

                $keterangan = "beban depresiasi kendaraan ".$kendaraans[$i]->nopol." ".Util::tanggalan($awal->format('m Y'));

                $cek = Jurnal::where('kode_akun', Akun::BebanDepKendaraan)
                                ->where('referensi', $kendaraans[$i]->nopol)
                                ->whereMonth('updated_at', '=', $bulan)
                                ->whereYear('updated_at', '=', $tahun)
                                ->first();

                if($cek==NULL && $depresiasi > 0){
                    $jurnal_beban_depresiasi = new Jurnal();
                    $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepKendaraan;
                    $jurnal_beban_depresiasi->referensi = $kendaraans[$i]->nopol;
                    $jurnal_beban_depresiasi->debet = $depresiasi;
                    $jurnal_beban_depresiasi->keterangan = $keterangan;
                    $jurnal_beban_depresiasi->updated_at = $star;
                    $jurnal_beban_depresiasi->save();

                    $jurnal_akumulasi_depresiasi = new Jurnal();
                    $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADKendaraan;
                    $jurnal_akumulasi_depresiasi->referensi = $kendaraans[$i]->nopol;
                    $jurnal_akumulasi_depresiasi->kredit = $depresiasi;
                    $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                    $jurnal_akumulasi_depresiasi->updated_at = $star;
                    $jurnal_akumulasi_depresiasi->save();

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kendaraans[$i]->nopol.' - L/R',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::BebanDepKendaraan,
                        'referensi' => $kendaraans[$i]->nopol.' - L/R',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kendaraans[$i]->nopol.' - LDT',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kendaraans[$i]->nopol.' - LDT',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi
                    ]);

                    $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                    $akun_laba_ditahan->kredit -=  $depresiasi;
                    $akun_laba_ditahan->update();

                    // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepKendaraan)->first();
                    // $akun_beban_depresiasi->debet += $depresiasi;
                    // $akun_beban_depresiasi->update();

                    $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADKendaraan)->first();
                    $akun_akumulasi_depresiasi->kredit += $depresiasi;
                    $akun_akumulasi_depresiasi->update();
                }

                if($x == $umur_kendaraan){
                    break;
                }
            }
        }

        //untuk depresiasi bangunan
        if($bangunan->umur > 0){
            $umur_bangunan = $bangunan->umur * 12;
            $depresiasi_bangunan = ($bangunan->harga - $bangunan->residu)  / $umur_bangunan;
            $depresiasi_bangunan = self::pembulatan($depresiasi_bangunan);
            $timeStart = strtotime($bangunan->created_at); 
            // Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
            //menghitung selisih bulan
            $numBulan += date("m",$timeEnd)-date("m",$timeStart);
            //ngitung depresiasi per bulan
            $x = 0;
            for($a=0; $a<$numBulan; $a++){
                $x += 1;
                if($x == $umur_bangunan - 1){
                    $beban = DB::table('jurnals')
                            ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                            ->where('kode_akun', Akun::BebanDepBangunan)
                            ->where('referensi', Akun::Bangunan)
                            ->first()->jumlah_beban;
                    $nilai_depresiasi_akhir = ($bangunan->harga - $bangunan->residu) - ($depresiasi_bangunan * ($umur_bangunan - 1));
                    $depresiasi_bangunan = self::pembulatan($depresiasi_bangunan);
                }
                $awal = new DateTime($bangunan->created_at); 
                $interval = "P".$a."M";
                $awal->add(new DateInterval($interval));
                $star = $awal->format('Y-m-t');
                $tahun = $awal->format('Y');
                $bulan = $awal->format('m');

                $keterangan = "beban depresiasi bangunan ".Util::tanggalan($awal->format('m Y'));

                $cek = Jurnal::where('kode_akun', Akun::BebanDepBangunan)
                                ->where('referensi', Akun::Bangunan)
                                ->whereMonth('updated_at', '=', $bulan)
                                ->whereYear('updated_at', '=', $tahun)
                                ->first();

                if($cek==NULL){
                    $jurnal_beban_depresiasi = new Jurnal();
                    $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepBangunan;
                    $jurnal_beban_depresiasi->referensi = Akun::Bangunan;
                    $jurnal_beban_depresiasi->debet = $depresiasi_bangunan;
                    $jurnal_beban_depresiasi->keterangan = $keterangan;
                    $jurnal_beban_depresiasi->updated_at = $star;
                    $jurnal_beban_depresiasi->save();

                    $jurnal_akumulasi_depresiasi = new Jurnal();
                    $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADBangunan;
                    $jurnal_akumulasi_depresiasi->referensi = Akun::Bangunan;
                    $jurnal_akumulasi_depresiasi->kredit = $depresiasi_bangunan;
                    $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                    $jurnal_akumulasi_depresiasi->updated_at = $star;
                    $jurnal_akumulasi_depresiasi->save();

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' =>  Akun::Bangunan.' - L/R',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::BebanDepBangunan,
                        'referensi' =>  Akun::Bangunan.' - L/R',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' =>  Akun::Bangunan.' - LDT',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' =>  Akun::Bangunan.' - LDT',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi_bangunan
                    ]);

                    $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                    $akun_laba_ditahan->kredit -=  $depresiasi_bangunan;
                    $akun_laba_ditahan->update();

                    // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepBangunan)->first();
                    // $akun_beban_depresiasi->debet += $depresiasi_bangunan;
                    // $akun_beban_depresiasi->update();

                    $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADBangunan)->first();
                    $akun_akumulasi_depresiasi->kredit += $depresiasi_bangunan;
                    $akun_akumulasi_depresiasi->update();
                }

                if($x == $umur_bangunan){
                    break;
                }
            }
        }
    }

    /*public static function depresiasi_akhir(){
        $awal = new Datetime();
        // $today = date('m');
        // $timeEnd = strtotime("$date");
        $tahun = $awal->format('Y');
        $bulan = $awal->format('m');

        $peralatans = Perkap::where('umur', '>', 0)->where('status' , 1)->get();
        $kendaraans = Kendaraan::where('rusak', 1)->get();
        $bangunan = Bangunan::find(1);

        //untuk depresiasi peralatan            
        for($i=0; $i<sizeof($peralatans); $i++){
            //ngitung depresiasi per bulan
            $umur_alat = $peralatans[$i]->umur*12;
            $depresiasi = ($peralatans[$i]->harga - $peralatans[$i]->residu) / $umur_alat;
            $depresiasi = self::pembulatan($depresiasi);       

            $count = DB::table('jurnals')
                            ->where('kode_akun', Akun::BebanDepPeralatan)
                            ->where('referensi', $peralatans[$i]->kode)
                            ->get();
            if($count->count() == ( $umur_alat-1 )){
                $beban = DB::table('jurnals')
                        ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                        ->where('kode_akun', Akun::BebanDepPeralatan)
                        ->where('referensi', $peralatans[$i]->kode)
                        ->first()->jumlah_beban;

                $depresiasi = ($peralatans[$i]->harga - $peralatans[$i]->residu) - $beban;
                $depresiasi = self::pembulatan($depresiasi);
            }

            $keterangan = "beban depresiasi alat ".$peralatans[$i]->nama." ".Util::tanggalan($awal->format('m Y'));

            $cek = Jurnal::where('kode_akun', Akun::BebanDepPeralatan)
                            ->where('referensi', $peralatans[$i]->kode)
                            ->whereMonth('updated_at', '=', $bulan)
                            ->whereYear('updated_at', '=', $tahun)
                            ->first();
            if($cek==NULL){
                $jurnal_beban_depresiasi = new Jurnal();
                $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepPeralatan;
                $jurnal_beban_depresiasi->referensi = $peralatans[$i]->kode;
                $jurnal_beban_depresiasi->debet = $depresiasi;
                $jurnal_beban_depresiasi->keterangan = $keterangan;
                $jurnal_beban_depresiasi->save();

                $jurnal_akumulasi_depresiasi = new Jurnal();
                $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADPeralatan;
                $jurnal_akumulasi_depresiasi->referensi = $peralatans[$i]->kode;
                $jurnal_akumulasi_depresiasi->kredit = $depresiasi;
                $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                $jurnal_akumulasi_depresiasi->save();

                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $peralatans[$i]->kode.' - L/R',
                    'keterangan' => $keterangan,
                    'debet' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::BebanDepPeralatan,
                    'referensi' => $peralatans[$i]->kode.' - L/R',
                    'keterangan' => $keterangan,
                    'kredit' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $peralatans[$i]->kode.' - LDT',
                    'keterangan' => $keterangan,
                    'debet' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $peralatans[$i]->kode.' - LDT',
                    'keterangan' => $keterangan,
                    'kredit' => $depresiasi
                ]);

                $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_ditahan->kredit -=  $depresiasi;
                $akun_laba_ditahan->update();

                // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepPeralatan)->first();
                // $akun_beban_depresiasi->debet += $depresiasi;
                // $akun_beban_depresiasi->update();

                $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADPeralatan)->first();
                $akun_akumulasi_depresiasi->kredit += $depresiasi;
                $akun_akumulasi_depresiasi->update();
            }
        }

        //untuk depresiasi Kendaraan
        for($i=0; $i<sizeof($kendaraans); $i++){
            //ngitung depresiasi per bulan
            $umur_kendaraan = $kendaraans[$i]->umur*12;
            $depresiasi = ($kendaraans[$i]->harga - $kendaraans[$i]->residu) / $umur_kendaraan;
            $depresiasi = self::pembulatan($depresiasi);

            $count = DB::table('jurnals')
                            ->where('kode_akun', Akun::BebanDepKendaraan)
                            ->where('referensi', $kendaraans[$i]->nopol)
                            ->get();
            if($count->count() == ( $umur_kendaraan-1 )){
                $beban = DB::table('jurnals')
                        ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                        ->where('kode_akun', Akun::BebanDepKendaraan)
                        ->where('referensi', $kendaraans[$i]->nopol)
                        ->first()->jumlah_beban;

                $depresiasi = ($kendaraans[$i]->harga - $kendaraans[$i]->residu)- $beban;
                $depresiasi = self::pembulatan($depresiasi);
            }
            $keterangan = "beban depresiasi kendaraan ".$kendaraans[$i]->nopol." ".Util::tanggalan($awal->format('m Y'));

            $cek = Jurnal::where('kode_akun', Akun::BebanDepKendaraan)
                            ->where('referensi', $kendaraans[$i]->nopol)
                            ->whereMonth('updated_at', '=', $bulan)
                            ->whereYear('updated_at', '=', $tahun)
                            ->first();

            if($cek==NULL){
                $jurnal_beban_depresiasi = new Jurnal();
                $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepKendaraan;
                $jurnal_beban_depresiasi->referensi = $kendaraans[$i]->nopol;
                $jurnal_beban_depresiasi->debet = $depresiasi;
                $jurnal_beban_depresiasi->keterangan = $keterangan;
                $jurnal_beban_depresiasi->save();

                $jurnal_akumulasi_depresiasi = new Jurnal();
                $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADKendaraan;
                $jurnal_akumulasi_depresiasi->referensi = $kendaraans[$i]->nopol;
                $jurnal_akumulasi_depresiasi->kredit = $depresiasi;
                $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                $jurnal_akumulasi_depresiasi->save();

                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kendaraans[$i]->nopol.' - L/R',
                    'keterangan' => $keterangan,
                    'debet' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::BebanDepKendaraan,
                    'referensi' => $kendaraans[$i]->nopol.' - L/R',
                    'keterangan' => $keterangan,
                    'kredit' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $kendaraans[$i]->nopol.' - LDT',
                    'keterangan' => $keterangan,
                    'debet' => $depresiasi
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kendaraans[$i]->nopol.' - LDT',
                    'keterangan' => $keterangan,
                    'kredit' => $depresiasi
                ]);

                $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_ditahan->kredit -=  $depresiasi;
                $akun_laba_ditahan->update();

                // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepKendaraan)->first();
                // $akun_beban_depresiasi->debet += $depresiasi;
                // $akun_beban_depresiasi->update();

                $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADKendaraan)->first();
                $akun_akumulasi_depresiasi->kredit += $depresiasi;
                $akun_akumulasi_depresiasi->update();
            }
        }

        //untuk depresiasi bangunan
        if($bangunan->umur > 0){
            $umur_bangunan = $bangunan->umur * 12;        
            $depresiasi_bangunan = ($bangunan->harga - $bangunan->residu)  / $umur_bangunan;
            $depresiasi_bangunan = self::pembulatan($depresiasi_bangunan);

            $count = DB::table('jurnals')
                            ->where('kode_akun', Akun::BebanDepBangunan)
                            ->where('referensi', Akun::Bangunan)
                            ->get();
            if($count->count() == ( $umur_bangunan-1 )){
                $beban = DB::table('jurnals')
                        ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                        ->where('kode_akun', Akun::BebanDepBangunan)
                        ->where('referensi', Akun::Bangunan)
                        ->first()->jumlah_beban;

                $depresiasi_bangunan = ($bangunan->harga - $bangunan->residu) - $beban;
                $depresiasi_bangunan = self::pembulatan($depresiasi_bangunan);
            }
            $timeStart = strtotime($bangunan->created_at); 
            // Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
            //menghitung selisih bulan
            $numBulan += date("m",$timeEnd)-date("m",$timeStart);
            //ngitung depresiasi per bulan


            $keterangan = "beban depresiasi bangunan ".Util::tanggalan($awal->format('m Y'));

            $cek = Jurnal::where('kode_akun', Akun::BebanDepBangunan)
                            ->where('referensi', Akun::Bangunan)
                            ->whereMonth('updated_at', '=', $bulan)
                            ->whereYear('updated_at', '=', $tahun)
                            ->first();

            if($cek==NULL){
                $jurnal_beban_depresiasi = new Jurnal();
                $jurnal_beban_depresiasi->kode_akun = Akun::BebanDepBangunan;
                $jurnal_beban_depresiasi->referensi = Akun::Bangunan;
                $jurnal_beban_depresiasi->debet = $depresiasi_bangunan;
                $jurnal_beban_depresiasi->keterangan = $keterangan;
                $jurnal_beban_depresiasi->save();

                $jurnal_akumulasi_depresiasi = new Jurnal();
                $jurnal_akumulasi_depresiasi->kode_akun = Akun::ADBangunan;
                $jurnal_akumulasi_depresiasi->referensi = Akun::Bangunan;
                $jurnal_akumulasi_depresiasi->kredit = $depresiasi_bangunan;
                $jurnal_akumulasi_depresiasi->keterangan = $keterangan;
                $jurnal_akumulasi_depresiasi->save();

                Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' =>  Akun::Bangunan.' - L/R',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::BebanDepBangunan,
                        'referensi' =>  Akun::Bangunan.' - L/R',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' =>  Akun::Bangunan.' - LDT',
                        'keterangan' => $keterangan,
                        'debet' => $depresiasi_bangunan
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' =>  Akun::Bangunan.' - LDT',
                        'keterangan' => $keterangan,
                        'kredit' => $depresiasi_bangunan
                    ]);

                    $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                    $akun_laba_ditahan->kredit -=  $depresiasi_bangunan;
                    $akun_laba_ditahan->update();

                // $akun_beban_depresiasi = Akun::where('kode', Akun::BebanDepBangunan)->first();
                // $akun_beban_depresiasi->debet += $depresiasi_bangunan;
                // $akun_beban_depresiasi->update();

                $akun_akumulasi_depresiasi = Akun::where('kode', Akun::ADBangunan)->first();
                $akun_akumulasi_depresiasi->kredit += $depresiasi_bangunan;
                $akun_akumulasi_depresiasi->update();
            }
        }
    }*/

    public static function down_pelanggan(){
        $periode = PeriodeOrder::find(1);
        $parameter_ = new Datetime();
        $parameter = $parameter_->sub(new DateInterval('P'.$periode->bulan_down.'M'));

        if($periode->bulan_down != 0){
            $transaksi_penjualan = TransaksiPenjualan::where('pelanggan_id', '!=', NULL)->groupBy('pelanggan_id')->get();
            foreach ($transaksi_penjualan as $key => $transaksi) {
                $transaksi = TransaksiPenjualan::where('pelanggan_id', $transaksi->pelanggan_id)->latest()->first();
                $pelanggan = Pelanggan::find($transaksi->pelanggan_id);
                if($pelanggan->level == 'grosir'){
                    if($transaksi->updated_at < $parameter){
                    	// return $pelanggan->nama;
                        $pelanggan->level = 'eceran';
                        $pelanggan->time_change = new Datetime();
                        $pelanggan->update();
                    }
                }
            }
        }
    }

    public static function down_pelanggan_transaksi($id){
        // return $id;
        $pelanggan = Pelanggan::find($id);
        $waktu_ganti = new Datetime($pelanggan->time_change);

        if($pelanggan->level == 'grosir'){
            $periode = PeriodeOrder::find(1);
            if($periode->jumlah_down != 0){

                $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $id)->where('updated_at', '>', $waktu_ganti)->orderBy('created_at', 'dsc')->get();
                // return $transaksi_penjualans;
                $eceran = 0;

                if(sizeof($transaksi_penjualans) >= $periode->jumlah_down){
                    for($i=0; $i < $periode->jumlah_down; $i++){
                        if($transaksi_penjualans[$i]->status == 'eceran'){
                            $eceran += 1;
                        }
                    }

                    if($eceran >= floor($periode->jumlah_down/2)+1 && $lolos = 1){
                        $pelanggan->level = 'eceran';
                        $pelanggan->time_change = new Datetime();
                        $pelanggan->update();
                    }
                }
            }
        }
    }

    public static function up_pelanggan($id){
        $pelanggan = Pelanggan::find($id);
        $waktu_ganti = new Datetime($pelanggan->time_change);
        if($pelanggan->level == 'eceran'){
            $periode = PeriodeOrder::find(1);
            $parameter_ = new Datetime($pelanggan->time_change);
            $parameter = $parameter_->sub(new DateInterval('P'.$periode->bulan_up.'M'));

            if($periode->bulan_up != 0 && $periode->jumlah_up != 0){
                $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $id)->where('updated_at', '>',  $waktu_ganti)->where('updated_at', '>', $parameter)->orderBy('created_at', 'dsc')->get();
                $grosir = 0;

                if(sizeof($transaksi_penjualans) >= $periode->jumlah_up){
                    for($i = (sizeof($transaksi_penjualans)-1); $i > (sizeof($transaksi_penjualans)-1-$periode->jumlah_up); $i--){
                        if($transaksi_penjualans[$i]->status == 'grosir'){
                            $grosir += 1;
                        }
                    }
                }

                if($grosir >= floor($periode->jumlah_up/2)+1){
                    $pelanggan->level = 'grosir';
                    $pelanggan->time_change = new Datetime();
                    $pelanggan->update();
                }
            }
        }
    }

    public static function terbilang($x) 
    { 
      $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
      if ($x < 12) 
        return " " . $abil[$x]; 
      elseif ($x < 20) 
        return self::terbilang($x - 10) . "belas"; 
      elseif ($x < 100) 
        return self::terbilang($x / 10) . " puluh" . self::terbilang($x % 10); 
      elseif ($x < 200) 
        return " seratus" . self::terbilang($x - 100); 
      elseif ($x < 1000) 
        return self::terbilang($x / 100) . " ratus" . self::terbilang($x % 100); 
      elseif ($x < 2000) 
        return " seribu" . self::terbilang($x - 1000); 
      elseif ($x < 1000000) 
        return self::terbilang($x / 1000) . " ribu" . self::terbilang($x % 1000); 
      elseif ($x < 1000000000) 
        return self::terbilang($x / 1000000) . " juta" . self::terbilang($x % 1000000);     
    }

    public static function generateRandomString($length = 10)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomNumber($length = 10)
    {
        $characters = '1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
