<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayarPiutangDagang extends Model
{
	public function piutang_dagang()
	{
		return $this->belongsTo('App\PiutangDagang');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	// public function bank()
	// {
	// 	return $this->belongsTo('App\Bank');
	// }

	public function bp_transfer()
    {
        return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bp_kartu()
    {
        return $this->belongsTo('App\Bank', 'bank_kartu');
    }
}
