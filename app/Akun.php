<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
	// Akun nomor 1
	const Aset 					= '100000';

	const AsetLancar			= '110000';
	
	const KasTunai				= '111100';
	const KasKecil				= '111200';
	const KasBank 				= '111310';
	const KasCek 				= '111320';
	const KasBG					= '111330';
	const Persediaan			= '112000';
	const Perlengkapan			= '113000';
	const PiutangDagang 		= '114100';
	const PiutangKaryawan 		= '114200';
	const PiutangLain			= '114300';
	const PiutangTakTertagih	= '114500';
	const AsuransiDimuka 		= '115100';
	const SewaDimuka			= '115200';
	const PPNMasukan 			= '116000';
	
	const AsetTetap				= '120000';
	
	const Tanah					= '121000';
	const Bangunan				= '122000';
	const Peralatan 			= '123000';
	const Kendaraan 			= '124000';
	const ADBangunan			= '125100';
	const ADPeralatan			= '125200';
	const ADKendaraan			= '125300';
	
	// Akun nomor 2
	const Kewajiban				= '200000';
	
	const HutangJangkaPendek 	= '210000';
	
	const KartuKredit			= '211000';
	const HutangDagang			= '212100';
	const HutangPajak			= '212300';
	const TaksiranUtangGaransi 	= '213000';	
	const HutangKonsinyasi		= '214000';
	const HutangPPN				= '215100';
	const PPNKeluaran			= '215200';
	const PendapatanDimuka		= '216000';
	const PembayaranProses		= '217000';
	
	const HutangJangkaPanjang 	= '220000';
	
	const HutangBank			= '221000';
	
	// Akun nomor 3
	const Modal 				= '300000';
	
	const LabaTahan				= '310000';
	const Prive 				= '320000';
	
	const BagiHasil				= '330000';
	
	// Akun nomor 4
	const Pendapatan 			= '400000';
	
	const Penjualan				= '411000';
	const PenjualanKredit		= '412000';
	const PendapatanLain		= '420000';
	const PotonganPembelian		= '430000';
	const ReturPembelian		= '431000';
	
	const PotonganPenjualan		= '440000';
	const ReturPenjualan		= '450000';	
	const HPP 					= '470000';
	
	const UntungPersediaan		= '480000';
	const BonusPenjualan		= '490000';

	// Akun nomor 5
	const Beban 				= '500000';
	
	const BebanIklan			= '511000';
	const BebanSewa				= '512000';
	const BebanPerlengkapan		= '531000';
	const BebanPerawatan 		= '532000';
	const BebanKerugianPiutang	= '541000';
	const BebanKerugianPersediaan	= '542000';
	const BebanRugiAset			= '543000';
	const BebanRugiBeli			= '544000';
	const BebanDepBangunan		= '551000';
	const BebanDepPeralatan		= '552000';
	const BebanDepKendaraan		= '553000';
	const BebanAsuransi			= '561000';
	const BebanGaji				= '562000';
	const BebanAdministrasiBank = '563000';
	const BebanBunga 			= '563100';
	const BebanDenda 			= '564000';
	const BebanUtilitas			= '571000';
	const BebanListrik			= '574000';
	const BebanOngkir			= '572000';
	const BebanTransportasi		= '573000';
	const BebanGaransi			= '575000';
	const BebanPajak			= '580000';
	const BebanKonsumsi			= '591000';
	const BebanSosial			= '592000';
	const BebanLain				= '593000';

	const LabaRugi				= '600000';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'kode', 'nama', 'debet', 'kredit'
	];

    public function jurnal()
    {
    	return $this->hasMany('App\Jurnal', 'kode_akun', 'kode_akun');
    }

    public function saldo()
    {
        return $this->hasMany('App\Saldo', 'kode_akun', 'kode_akun');
    }

    public function jurnal_penutup()
    {
    	return $this->hasMany('App\JurnalPenutup', 'kode_akun', 'kode_akun');
    }
}
