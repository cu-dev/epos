<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode', 'nama',
    ];

    public function item()
    {
        return $this->belongsToMany('App\Item', 'relasi_satuans', 'satuan_id', 'item_kode');
    }

    public function harga()
    {
        return $this->hasMany('App\Harga', 'satuan_id');
    }

    public function relasi_bonus()
    {
        return $this->hasMany('App\RelasiBonus');
    }
    // public function relasi_transaksi_penjualan()
    // {
    // 	return $this->hasMany('App\TransaksiPenjualan', 'satuan_id');
    // }
}
