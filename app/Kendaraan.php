<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nopol',
        'atas_nama',
        'umur',
        'tahun_perakitan',
        'rusak',
        'harga',
        'nominal',
        'residu',
        'keterangan',
    ];

}
