<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPerusahaan extends Model
{
    protected $fillable = [
        'nama',
        'alamat',
        'email',

        'telepon',
        'wa_perusahaan',
        'wa_penjualan',

        'npwp',
        'tanggal_npwp',

        'pkp',
        'tanggal_pkp',

        'logo'
    ];
}
