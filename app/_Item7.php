<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Item7 extends Model
{
    protected $fillable = [
        'item_kode', 'satuan_id', 'jumlah', 'eceran', 'grosir'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Satuan', 'satuan_id');
    }
}
