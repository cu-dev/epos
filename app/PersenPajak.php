<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersenPajak extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
