<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetoranKasir extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'nominal'
	];

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function operator()
	{
		return $this->belongsTo('App\User', 'operator');
	}
}
