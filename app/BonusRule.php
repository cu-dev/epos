<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonusRule extends Model
{
    /*
      Status
      0 => semua pelanggan
      1 => pelanggan eceran
      2 => pelanggan grosir
    */
    protected $fillable = [
        'nama',
        'nominal',
        'kelipatan',
        'profit',
        'jumlah',
        'status',
        'aktif',
        'user_id',
    ];

    protected $appends = [
        'relasi_bonuses'
    ];

    public function getRelasiBonusesAttribute()
    {
        $relasi_bonus_rules = array();
        $temp_relasi_bonus_rules = \App\RelasiBonusRule::with('bonus')->where('bonus_rules_id', $this->id)->get();
        foreach ($temp_relasi_bonus_rules as $i => $temp_relasi_bonus_rule) {
            $bonus = $temp_relasi_bonus_rule->bonus;
            $stoks = \App\Stok::where('item_kode', $bonus->kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
            $today = date('Y-m-d');
            $stoktotal = 0;
            foreach ($stoks as $j => $stok) {
                if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                    $stoktotal += $stok->jumlah;
                }
            }

            if ($stoktotal > 0) {
                $bonus->stoktotal = $stoktotal;
                array_push($relasi_bonus_rules, $temp_relasi_bonus_rule);
            }
        }

        return $relasi_bonus_rules;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // public function bonuses()
    // {
    //     return $this->belongsToMany('App\Item', 'relasi_bonus_rules', 'bonus_rules_id', 'item_id');
    // }
}
