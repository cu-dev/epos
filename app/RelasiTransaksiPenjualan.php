<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiTransaksiPenjualan extends Model
{
    protected $fillable = [
        'transaksi_penjualan_id', 'item_kode', 'jumlah', 'harga', 'subtotal', 'nego'
    ];

    protected $appends = [
        'relasi_bonus_penjualan', 'relasi_stok_penjualan', 'relasi_bundle'
    ];

    public function getRelasiBonusPenjualanAttribute()
    {
        return \App\RelasiBonusPenjualan::with('bonus')
            ->where('relasi_transaksi_penjualan_id', $this->id)
            ->get();
    }

    public function getRelasiStokPenjualanAttribute()
    {
        return \App\RelasiStokPenjualan::with('stok')
            ->where('relasi_transaksi_penjualan_id', $this->id)
            ->get();
    }

    public function getRelasiBundleAttribute()
    {
        return \App\RelasiBundle::with('itemxx')
            ->where('item_parent', $this->item_kode)
            ->get();
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function transaksi_penjualan() {
        return $this->belongsTo('App\TransaksiPenjualan');
    }
}
