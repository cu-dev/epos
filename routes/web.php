<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('beranda');
});

Route::post('/login', 'AuthController@postLogin');
Route::get('/login', 'AuthController@getLogin');
Route::get('/logout', 'AuthController@getLogout');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/beranda', 'BerandaController@get');
    Route::group(['middleware' => 'role:owner'], function() {
        // Untuk admin
    });
    Route::group(['middleware' => 'role:gudang'], function() {
        // Untuk gudang
    });
    Route::get('/jenisitem', 'JenisItemController@index');
    Route::get('/jenisitem/mdt1', 'JenisItemController@mdt1');
    Route::post('/jenisitem', 'JenisItemController@store');
    Route::put('/jenisitem/{jenisitem}', 'JenisItemController@update');
    Route::delete('/jenisitem/{jenisitem}', 'JenisItemController@destroy');

    Route::get('/satuan', 'SatuanController@index');
    Route::get('/satuan/mdt1', 'SatuanController@mdt1');
    Route::post('/satuan', 'SatuanController@store');
    Route::put('/satuan/{satuan}', 'SatuanController@update');
    Route::delete('/satuan/{satuan}', 'SatuanController@destroy');
    Route::get('/satuan/json', 'SatuanController@indexJson');

    Route::get('/level', 'LevelController@index');
    Route::post('/level', 'LevelController@store');
    Route::put('/level/{level}', 'LevelController@update');
    Route::delete('/level/{level}', 'LevelController@destroy');

    Route::get('/bonus', 'BonusController@index');
    Route::post('/bonus', 'BonusController@store');
    Route::put('/bonus/{bonus}', 'BonusController@update');
    Route::delete('/bonus/{bonus}', 'BonusController@destroy');

    Route::get('/item', 'ItemController@index');
    Route::get('/item/mdt1', 'ItemController@mdt1');
    Route::get('/item/mdt2', 'ItemController@mdt2');
    Route::get('/item/mdt3', 'ItemController@mdt3');
    Route::get('/item/mdt4', 'ItemController@mdt4');
    Route::get('/item/mdt5', 'ItemController@mdt5');
    Route::delete('/item/{item}', 'ItemController@delete');
    Route::post('/item/hapus/pemasok', 'ItemController@hapusPemasok');
    Route::delete('/item/{item}/kode', 'ItemController@deleteByKode');
    Route::post('/item/{item}/fix-konversi-satuan', 'ItemController@fixKonversiSatuan');
    Route::post('/item/{item}/valid-konversi', 'ItemController@validKonversi');
    Route::get('/item/json', 'ItemController@indexJson');
    Route::get('/item/create', 'ItemController@create');
    Route::post('/item', 'ItemController@store');
    Route::get('/item/show/{item}', 'ItemController@show');
    Route::get('/item-bundle/show/{item}', 'ItemController@showBundle');
    Route::get('/item-bundle/edit/{item}', 'ItemController@editBundle');
    Route::get('/item-history/show/{item}', 'ItemController@showHistory');
    Route::get('/item/show/{item}/json', 'ItemController@showJson');
    Route::get('/item/edit/{item}', 'ItemController@edit');
    Route::get('/item/reset/harga/{item}', 'ItemController@resetHarga');
    Route::get('/item/reset/harga-bundle/{item}', 'ItemController@resetHargaBundle');
    Route::get('/item/history-harga-beli/{item}', 'ItemController@historyHargaBeli');
    Route::get('/item/history-harga-jual/{item}', 'ItemController@historyHargaJual');
    Route::get('/item/tambah_suplier/{item}', 'ItemController@tambah_suplier');
    Route::post('/item/tambah_suplier/{item}', 'ItemController@store_suplier');
    Route::get('/item/tambah_barcode/{item}', 'ItemController@tambah_barcode');
    Route::post('/item/tambah_barcode/{item}', 'ItemController@store_barcode');
    Route::put('/item/{item}/ubah', 'ItemController@ubah');
    Route::get('/item/{item}/stok/json', 'ItemController@stokJson');
    Route::get('/item/{item}/konversi/json', 'ItemController@konversiJson');
    Route::post('/item/bonus/create', 'ItemController@store_bonus');
    Route::post('/item/bonus/{id}/update', 'ItemController@update_bonus');
    Route::post('/item/bonus/{id}/{kode}/delete', 'ItemController@delete_bonus');
    Route::post('/item/diskon', 'ItemController@update_diskon');
    Route::post('/item/limit-stok', 'ItemController@update_limit_stok');
    Route::post('/item/limit-grosir', 'ItemController@update_limit_grosir');
    Route::delete('/item/aktif/{item}', 'ItemController@re_aktif');
    // Route::get('/data_table_item', 'ItemController@item_data_json');

    Route::get('/item_bundle/{kode}/item/json', 'ItemController@SatuanJson');
    Route::post('/item/bundle/create', 'ItemController@BundleStore');
    Route::put('/item/bundle/{item}/update', 'ItemController@ubahBundle');
    Route::delete('/relasi_bundle/{id}/{kode}', 'ItemController@delete_bundle');

    Route::get('/item-konsinyasi', 'ItemKonsinyasiController@index');
    Route::get('/item-konsinyasi/json', 'ItemKonsinyasiController@indexJson');
    Route::get('/item-konsinyasi/create', 'ItemKonsinyasiController@create');
    Route::post('/item-konsinyasi', 'ItemKonsinyasiController@store');
    Route::get('/item-konsinyasi/show/{item}', 'ItemKonsinyasiController@show');
    Route::get('/item-konsinyasi/show/{item}/json', 'ItemKonsinyasiController@showJson');
    Route::get('/item-konsinyasi/edit/{item}', 'ItemKonsinyasiController@edit');
    Route::get('/item-konsinyasi/history-harga-beli/{item}', 'ItemKonsinyasiController@historyHargaBeli');
    Route::get('/item-konsinyasi/tambah_suplier/{item}', 'ItemKonsinyasiController@tambah_suplier');
    Route::post('/item-konsinyasi/tambah_suplier/{item}', 'ItemKonsinyasiController@store_suplier');
    // Route::put('/item-konsinyasi/{item}', 'ItemKonsinyasiController@update');
    Route::put('/item-konsinyasi/{item}/ubah', 'ItemKonsinyasiController@ubah');
    Route::delete('/item-konsinyasi/{item}', 'ItemKonsinyasiController@destroy');

    Route::get('/suplier', 'SuplierController@index');
    Route::get('/suplier/mdt1', 'SuplierController@mdt1');
    Route::get('/suplier/mdt2', 'SuplierController@mdt2');
    Route::post('/suplier', 'SuplierController@store');
    Route::get('/suplier/{suplier}/json', 'SuplierController@showJson');
    Route::put('/suplier/{suplier}', 'SuplierController@update');
    Route::delete('/suplier/{suplier}', 'SuplierController@destroy');
    Route::get('/suplier/aktif/{suplier}', 'SuplierController@aktif');

    Route::get('/item-masuk', 'ItemMasukController@index');
    Route::post('/item-masuk', 'ItemMasukController@store');
    Route::get('/item-masuk/create', 'ItemMasukController@create');
    Route::get('/item-masuk/{item}', 'ItemMasukController@show');
    Route::put('/item-masuk/{item}', 'ItemMasukController@update');
    // Route::delete('/item-masuk/{item}', 'ItemMasukController@destroy');
    Route::get('/item-masuk/{item}/edit', 'ItemMasukController@edit');

    Route::get('/transaksi-pembelian', 'TransaksiPembelianController@index');
    Route::get('/transaksi-pembelian/mdt1', 'TransaksiPembelianController@mdt1');
    Route::post('/transaksi-pembelian', 'TransaksiPembelianController@store');
    Route::post('/transaksi-pembelian/again/{id}', 'TransaksiPembelianController@storeAgain');
    Route::get('/transaksi-pembelian/edit/{id}', 'TransaksiPembelianController@Edit');
    Route::post('/transaksi-pembelian/edit/{id}', 'TransaksiPembelianController@Update');
    Route::get('/transaksi-pembelian/create', 'TransaksiPembelianController@create');
    Route::get('/transaksi-pembelian/create/{transaksi}', 'TransaksiPembelianController@createAgain');
    Route::get('/transaksi-pembelian/last.json', 'TransaksiPembelianController@lastJson');
    Route::get('/transaksi-pembelian/items/{item}/json', 'TransaksiPembelianController@showItemJson');
    Route::get('/transaksi-pembelian/{transaksi}', 'TransaksiPembelianController@show');
    Route::get('/transaksi-pembelian/{transaksi}/again', 'TransaksiPembelianController@showAgain');
    Route::get('/transaksi-pembelian/{transaksi}/hhb/{item}', 'TransaksiPembelianController@showHHB');
    Route::put('/transaksi-pembelian/{transaksi}', 'TransaksiPembelianController@update');
    // Route::delete('/transaksi-pembelian/{transaksi}', 'TransaksiPembelianController@destroy');
    Route::get('/transaksi-pembelian/{transaksi}/edit', 'TransaksiPembelianController@edit');
    Route::get('/transaksi-pembelian/{transaksi}/jatuh-tempo', 'TransaksiPembelianController@ubahJatuhTempo');
    Route::get('/transaksi-pembelian/{transaksi}/retur', 'ReturPembelianController@indexFromTransaksi');
    // Route::get('/transaksi-pembelian/{transaksi}/retur/transaksi/all', 'ReturPembelianController@indexFromTransaksiAll');
    Route::post('/transaksi-pembelian/{transaksi}/retur', 'ReturPembelianController@store');
    Route::get('/transaksi-pembelian/{transaksi}/retur/create', 'ReturPembelianController@create');
    Route::get('/transaksi-pembelian/{transaksi}/retur/{retur}', 'ReturPembelianController@showFromTransaksi');
    Route::get('/transaksi-pembelian/{transaksi}/retur/{retur}/retur', 'ReturPembelianController@showFromRetur');
    Route::get('/transaksi-pembelian/retur/last.json', 'ReturPembelianController@lastJson');
    Route::get('/transaksi-pembelian/{suplier}/items.json', 'TransaksiPembelianController@showSuplierItemsJson');
    Route::get('/transaksi-pembelian/{suplier}/sellers.json', 'TransaksiPembelianController@showSuplierSellersJson');
    Route::get('/transaksi-pembelian/{suplier}/suplier.json', 'TransaksiPembelianController@suplierJson');
    Route::get('/transaksi-pembelian/{id}/cairkan/cek', 'TransaksiPembelianController@CairCek');
    Route::get('/transaksi-pembelian/{id}/cairkan/bg', 'TransaksiPembelianController@CairBG');
    Route::get('/transaksi-pembelian/{suplier}/suplier_id.json', 'TransaksiPembelianController@SupplierIdJSON');

    Route::get('/retur-pembelian', 'ReturPembelianController@index');
    Route::get('/retur-pembelian/mdt1', 'ReturPembelianController@mdt1');
    Route::get('/retur-pembelian/{retur}', 'ReturPembelianController@show');
    Route::get('/retur-pembelian/{item}/item/{kode}/json', 'ReturPembelianController@itemJson');
    Route::get('/retur-pembelian/{transaksi}/konversi/json/{satuan}', 'ReturPembelianController@KonversiJson');
    Route::get('/retur-pembelian/{kode}/count/{jumlah}/json/{transaksi}', 'ReturPembelianController@countJson');
    Route::get('/retur-pembelian/{item}/item/json', 'ReturPembelianController@returItemJson');
    Route::get('/retur-pembelian/{kode}/item/{jumlah}/jumlah/{satuan}/satuan/{nilai}', 'ReturPembelianController@regaItemJson');
    Route::get('/retur-pembelian/{kode}/hutang', 'ReturPembelianController@hutangJson');
    Route::get('/retur-pembelian/{id}/cairkan/cek', 'ReturPembelianController@CekReturLunas');
    Route::get('/retur-pembelian/{id}/cairkan/bg', 'ReturPembelianController@BGReturLunas');

    // Route::get('/retur-pembelian/{transaksi}/json/item/retur', 'ReturPembelianController@returItemJson');

    Route::get('/hutang/{id}/cairkan/cek', 'HutangController@CairCek');
    Route::get('/hutang/{id}/cairkan/bg', 'HutangController@CairBG');

    Route::get('/konsinyasi-masuk', 'KonsinyasiMasukController@index');
    Route::get('/konsinyasi-masuk/mdt1', 'KonsinyasiMasukController@mdt1');
    Route::post('/konsinyasi-masuk', 'KonsinyasiMasukController@store');
    Route::get('/konsinyasi-masuk/create', 'KonsinyasiMasukController@create');
    Route::get('/konsinyasi-masuk/last.json', 'KonsinyasiMasukController@lastJson');
    Route::get('/konsinyasi-masuk/items/{item}/json', 'KonsinyasiMasukController@showItemJson');
    Route::get('/konsinyasi-masuk/{konsinyasi}', 'KonsinyasiMasukController@show');
    // Route::get('/konsinyasi-masuk/{konsinyasi}/hhb/{item}', 'KonsinyasiMasukController@showHHB');
    // Route::put('/konsinyasi-masuk/{konsinyasi}', 'KonsinyasiMasukController@update');
    // Route::delete('/konsinyasi-masuk/{konsinyasi}', 'KonsinyasiMasukController@destroy');
    Route::get('/konsinyasi-masuk/{konsinyasi}/keluar', 'KonsinyasiMasukController@keluar');
    // Route::get('/konsinyasi-masuk/{konsinyasi}/edit', 'KonsinyasiMasukController@edit');
    Route::get('/konsinyasi-masuk/{suplier}/items.json', 'KonsinyasiMasukController@showSuplierItemsJson');
    Route::get('/konsinyasi-masuk/{suplier}/sellers.json', 'KonsinyasiMasukController@showSuplierSellersJson');

    Route::get('/konsinyasi-keluar', 'KonsinyasiKeluarController@index');
    Route::get('/konsinyasi-keluar/mdt1', 'KonsinyasiKeluarController@mdt1');
    Route::post('/konsinyasi-keluar', 'KonsinyasiKeluarController@store');
    // Route::get('/konsinyasi-keluar/create', 'KonsinyasiKeluarController@create');
    Route::get('/konsinyasi-keluar/last.json', 'KonsinyasiKeluarController@lastJson');
    Route::get('/konsinyasi-keluar/items/{item}/json/{id}', 'KonsinyasiKeluarController@showItemJson');
    Route::get('/konsinyasi-keluar/{konsinyasi}', 'KonsinyasiKeluarController@show');
    Route::get('/konsinyasi-keluar/cek/{konsinyasi}', 'KonsinyasiKeluarController@cekLunas');
    Route::get('/konsinyasi-keluar/bg/{konsinyasi}', 'KonsinyasiKeluarController@bgLunas');
    // Route::get('/konsinyasi-keluar/{konsinyasi}/hhb/{item}', 'KonsinyasiKeluarController@showHHB');
    // Route::put('/konsinyasi-keluar/{konsinyasi}', 'KonsinyasiKeluarController@update');
    // Route::delete('/konsinyasi-keluar/{konsinyasi}', 'KonsinyasiKeluarController@destroy');
    // Route::get('/konsinyasi-keluar/{konsinyasi}/keluar', 'KonsinyasiKeluarController@keluar');
    // Route::get('/konsinyasi-keluar/{konsinyasi}/edit', 'KonsinyasiKeluarController@edit');
    Route::get('/konsinyasi-keluar/{suplier}/items.json', 'KonsinyasiKeluarController@showSuplierItemsJson');
    Route::get('/konsinyasi-keluar/{suplier}/sellers.json', 'KonsinyasiKeluarController@showSuplierSellersJson');

    Route::get('/po-pembelian', 'POPembelianController@index');
    Route::get('/po-pembelian/mdt1', 'POPembelianController@mdt1');
    Route::post('/po-pembelian', 'POPembelianController@store');
    Route::get('/po-pembelian/create', 'POPembelianController@create');
    Route::get('/po-pembelian/{po}', 'POPembelianController@show');
    Route::get('/po-pembelian/{po}/hapus', 'POPembelianController@hapus');
    Route::get('/po-pembelian/{po}/beli', 'POPembelianController@beli');

    Route::get('/item-keluar', 'ItemKeluarController@index');
    Route::post('/item-keluar/{item}', 'ItemKeluarController@store');
    Route::get('/item-keluar/{item}/create', 'ItemKeluarController@create');
    Route::get('/item-keluar/{item}', 'ItemKeluarController@show');
    Route::put('/item-keluar/{item}', 'ItemKeluarController@update');
    // Route::delete('/item-keluar/{item}', 'ItemKeluarController@destroy');
    Route::get('/item-keluar/{item}/edit', 'ItemKeluarController@edit');

    Route::post('/harga', 'HargaController@store');
    Route::post('/harga/{harga}/update', 'HargaController@ubah');
    Route::post('/harga/{harga}/delete', 'HargaController@hapus');

    Route::post('/relasi_satuan', 'RelasiSatuanController@store');
    Route::put('/relasi_satuan/{satuan}', 'RelasiSatuanController@update');
    Route::delete('/relasi_satuan/{satuan}', 'RelasiSatuanController@destroy');

    Route::get('/transaksi', 'TransaksiPenjualanController@index');
    Route::post('/transaksi', 'TransaksiPenjualanController@store');
    Route::get('/transaksi/create', 'TransaksiPenjualanController@create');
    Route::get('/transaksi/{transaksi}', 'TransaksiPenjualanController@show');
    Route::post('/transaksi/{transaksi}', 'TransaksiPenjualanController@piutang');
    Route::get('/transaksi/{transaksi}/item/json', 'TransaksiPenjualanController@itemJson');
    Route::get('/transaksi/last/json', 'TransaksiPenjualanController@lastJson');
    Route::get('/transaksi/{transaksi}/harga/json/{satuan}/{jumlah}', 'TransaksiPenjualanController@hargaJson');
    Route::get('/transaksi/{transaksi}/retur', 'ReturPenjualanController@index');
    Route::post('/transaksi/{transaksi}/retur', 'ReturPenjualanController@store');
    Route::get('/transaksi/{transaksi}/retur/create', 'ReturPenjualanController@create');
    Route::get('/transaksi/{transaksi}/retur/{retur}', 'ReturPenjualanController@show');
    Route::get('/transaksi/{transaksi}/retur/last/json', 'ReturPenjualanController@lastJson');
    // Route::get('/transaksi/{item}/retur/harga/json/{satuan}/{jumlah}', 'ReturPenjualanController@hargaJson');
    Route::get('/transaksi/{transaksi}/retur/harga/json/{item_kode}', 'ReturPenjualanController@hargaJson');
    Route::get('/retur-penjualan', 'ReturPenjualanController@all');
    Route::get('/retur-penjualan/mdt1', 'ReturPenjualanController@mdt1');
    Route::get('/retur-penjualan/{item}/item/json', 'ReturPenjualanController@itemJson');
    Route::get('/retur-penjualan/cek/{id}', 'ReturPenjualanController@cekLunas');
    Route::get('/retur-penjualan/bg/{id}', 'ReturPenjualanController@bgLunas');

    Route::get('/transaksi-grosir', 'TransaksiGrosirController@index');
    Route::get('/transaksi-grosir/mdt1', 'TransaksiGrosirController@mdt1');
    Route::post('/transaksi-grosir', 'TransaksiGrosirController@store');
    // Route::get('/transaksi-grosir/coba', 'TransaksiGrosirController@coba');
    Route::get('/transaksi-grosir/create', 'TransaksiGrosirController@create');
    Route::get('/transaksi-grosir/create/{transaksi}', 'TransaksiGrosirController@createAgain');
    Route::post('/transaksi-grosir/bayar/{transaksi}', 'TransaksiGrosirController@bayar');
//  Route::post('/transaksi-grosir/{transaksi}', 'TransaksiGrosirController@piutang');
    Route::get('/transaksi-grosir/last/json', 'TransaksiGrosirController@lastJson');
    Route::get('/transaksi-grosir/pelanggans/json', 'TransaksiGrosirController@pelanggansJson');
    Route::post('/transaksi-grosir/simpan-po/grosir', 'TransaksiGrosirController@simpanPOGrosir');
    Route::post('/transaksi-grosir/simpan-po/eceran', 'TransaksiGrosirController@simpanPOEceran');
    Route::post('/transaksi-grosir/simpan-po/pelanggan-baru', 'TransaksiGrosirController@simpanPOPelangganBaru');
    Route::put('/transaksi-grosir/simpan-po/{po}', 'TransaksiGrosirController@ubahPO');
    Route::put('/transaksi-grosir/bayar-po/{po}', 'TransaksiGrosirController@bayarPO');
    Route::post('/transaksi-grosir/bayar-po/', 'TransaksiGrosirController@bayarPONew');
    Route::get('/transaksi-grosir/{transaksi}', 'TransaksiGrosirController@show');
    Route::get('/transaksi-grosir/{transaksi}/cetak-nota', 'TransaksiGrosirController@cetak_nota');
    // Route::get('/transaksi-grosir/{transaksi}/cairkan-kredit', 'TransaksiGrosirController@formCairkanKredit');
    Route::post('/transaksi-grosir/{transaksi}/cairkan-kredit', 'TransaksiGrosirController@simpanCairkanKredit');
    Route::get('/transaksi-grosir/{transaksi}/retur', 'TransaksiGrosirController@returIndex');
    Route::post('/transaksi-grosir/{transaksi}/retur', 'TransaksiGrosirController@returSimpan');
    Route::get('/transaksi-grosir/{transaksi}/retur/create', 'TransaksiGrosirController@returCreate');
    Route::get('/transaksi-grosir/{transaksi}/retur/{item}/{item_parent}/json', 'TransaksiGrosirController@returItemJson');
    Route::get('/transaksi-grosir/{transaksi}/retur/{item}/{jumlah}/{satuan}/json', 'TransaksiGrosirController@returHargaJson');
    // Route::get('/transaksi-grosir/{transaksi}/retur/{item}/{satuan}/konversi/json', 'TransaksiGrosirController@konversiJson');
    // Route::get('/transaksi-grosir/{transaksi}/retur/{item}/count/{count}/json', 'TransaksiGrosirController@countJson');
    Route::get('/transaksi-grosir/{item}/item/json', 'TransaksiGrosirController@itemJson');
    Route::get('/transaksi-grosir/{item}/harga/json/{satuan1}/{jumlah1}/{satuan2}/{jumlah2}/{konversi1}/{konversi2}', 'TransaksiGrosirController@hargaJson');
    // Route::get('/transaksi-grosir/{item}/harga/json/{satuan}/{jumlah}', 'TransaksiGrosirController@hargaJson');
    Route::get('/transaksi-grosir/{item}/hpp/json', 'TransaksiGrosirController@hppJson');
    Route::get('/transaksi-grosir/{pelanggan}/pelanggan/json', 'TransaksiGrosirController@pelangganJson');
    Route::post('/transaksi-grosir/{id}/retur/aktif', 'TransaksiGrosirController@ReturAktif');

    Route::get('/transaksi-grosir-vip/create', 'TransaksiGrosirVIPController@create');
    Route::get('/transaksi-grosir-vip/create/{transaksi}', 'TransaksiGrosirVIPController@createAgain');
    Route::get('/transaksi-grosir-vip/{item}/item/json', 'TransaksiGrosirVIPController@itemJson');
    // Route::get('/transaksi-grosir-vip/{item}/harga/json/{satuan}/{jumlah}', 'TransaksiGrosirVIPController@hargaJson');
    Route::get('/transaksi-grosir-vip/{item}/harga/json/{satuan1}/{jumlah1}/{satuan2}/{jumlah2}/{konversi1}/{konversi2}', 'TransaksiGrosirController@hargaJson');
    Route::post('/transaksi-grosir-vip/simpan-po', 'TransaksiGrosirVIPController@simpanPO');
    Route::put('/transaksi-grosir-vip/simpan-po/{po}', 'TransaksiGrosirVIPController@ubahPO');

    Route::get('/transaksi-grosir/retur/user/{user}', 'TransaksiGrosirController@userJson');
    Route::get('/transaksi-grosir/tanggal/{piutang}', 'TransaksiGrosirController@tanggal');
    Route::post('/transaksi-grosir/tanggal', 'TransaksiGrosirController@tanggalStore');
    Route::post('/transaksi-grosir/{id}/jatuh-tempo', 'TransaksiGrosirController@jatuhTempoStore');

    Route::get('/po-penjualan', 'POPenjualanController@index');
    Route::get('/po-penjualan/mdt1', 'POPenjualanController@mdt1');
    Route::post('/po-penjualan', 'POPenjualanController@store');
    Route::get('/po-penjualan/create', 'POPenjualanController@create');
    Route::get('/po-penjualan/{po}', 'POPenjualanController@show');
    Route::post('/po-penjualan/{po}', 'POPenjualanController@updatePO');
    Route::get('/po-penjualan/{po}/ganti-status', 'POPenjualanController@gantiStatus');
    Route::get('/po-penjualan/{po}/hapus', 'POPenjualanController@hapus');
    Route::post('/po-penjualan/{po}/item/tambah', 'POPenjualanController@tambahItem');
    Route::put('/po-penjualan/{po}/item/update/{item}', 'POPenjualanController@updateItem');
    Route::delete('/po-penjualan/{po}/item/hapus/{item}', 'POPenjualanController@hapusItem');

    Route::get('/user', 'UserController@index');
    Route::get('/user/mdt1', 'UserController@mdt1');
    Route::get('/user/mdt2', 'UserController@mdt2');
    Route::get('/user/json', 'UserController@indexJson');
    Route::get('/user/create', 'UserController@create');
    Route::post('/user', 'UserController@store');
    Route::get('/user/show/{user}', 'UserController@show');
    Route::get('/user/show/{user}/json', 'UserController@showJson');
    Route::get('/user/edit/{user}', 'UserController@edit');
    Route::put('/user/{user}', 'UserController@update');
    Route::delete('/user/{user}', 'UserController@destroy');
    Route::get('/user/reset/{user}', 'UserController@reset');
    Route::post('/user/re_aktif/{user}', 'UserController@reAktif');

    // Route::get('/konsinyasi', 'KonsinyasiController@index');
    // Route::get('/konsinyasi/create', 'KonsinyasiController@create');
    // Route::post('/konsinyasi', 'KonsinyasiController@store');
    // Route::get('/konsinyasi/{konsinyasi}', 'KonsinyasiController@show');
    // Route::put('/konsinyasi/{konsinyasi}', 'KonsinyasiController@update');
    // Route::get('/konsinyasi/edit/{konsinyasi}', 'KonsinyasiController@edit');
    // Route::delete('/konsinyasi/{konsinyasi}', 'KonsinyasiController@destroy');

    // Route::get('/konsinyasi-masuk', 'KonsinyasiMasukController@index');
    // Route::get('/konsinyasi-masuk/create', 'KonsinyasiMasukController@create');
    // Route::post('/konsinyasi-masuk', 'KonsinyasiMasukController@store');
    // Route::get('/konsinyasi-masuk/{konsinyasi}', 'KonsinyasiMasukController@show');
    // Route::put('/konsinyasi-masuk/{konsinyasi}', 'KonsinyasiMasukController@update');
    // Route::get('/konsinyasi-masuk/edit/{konsinyasi}', 'KonsinyasiMasukController@edit');

    // Route::get('/konsinyasi-keluar/create/{konsinyasi}', 'KonsinyasiKeluarController@create');
    // Route::post('/konsinyasi-keluar/{konsinyasi}', 'KonsinyasiKeluarController@store');
    // Route::get('/konsinyasi-keluar/show/{konsinyasi}', 'KonsinyasiKeluarController@show');
    // Route::put('/konsinyasi-keluar/{konsinyasi}', 'KonsinyasiKeluarController@update');
    // Route::get('/konsinyasi-keluar/edit/{konsinyasi}', 'KonsinyasiKeluarController@edit');

    Route::get('/profil', 'ProfilController@index');
    Route::get('/profil/edit', 'ProfilController@edit');
    Route::post('/profil/upload', 'ProfilController@upload');
    Route::put('/profil', 'ProfilController@update');
    Route::get('/profil/password', 'ProfilController@password');
    Route::put('/profil/password', 'ProfilController@updatePassword');

    Route::get('/hutang', 'HutangController@index');
    Route::get('/hutang/mdt1', 'HutangController@mdt1');
    Route::get('/hutang/mdt2', 'HutangController@mdt2');
    Route::post('/hutang', 'HutangController@store');
    Route::get('/hutang/show/{hutang}', 'HutangController@show');

    Route::get('/hutang-suplier', 'HutangController@hutang_suplier');
    Route::post('/hutang-suplier', 'HutangController@store_suplier');
    Route::get('/hutang-suplier/show/{suplier}', 'HutangController@show_suplier');

    Route::get('/stock-of-num', 'StockOfNumController@index');
    Route::get('/stock-of-num/mdt1', 'StockOfNumController@mdt1');
    Route::post('/stock-of-num', 'StockOfNumController@store');
    Route::get('/stock-of-num/last.json', 'StockOfNumController@lastJson');
    Route::get('/stock-of-num/create', 'StockOfNumController@create');
    Route::get('/stock-of-num/{son}', 'StockOfNumController@show');
    Route::put('/stock-of-num/{son}', 'StockOfNumController@update');
    
    Route::get('/kas', 'KasController@index');
    Route::post('/transfer_kas', 'KasController@transfer');
    Route::get('/transfer_kas/last/json', 'KasController@lastJson');
    Route::get('/kas/cek/{cek}', 'KasController@cekJson');
    Route::get('/kas/bg/{bg}', 'KasController@bgJson');
    // Route::post('/setor', 'LogTransferBankController@store');

    Route::get('/pengeluaran', 'PengeluaranController@index');
    Route::get('/pengeluaran/create', 'PengeluaranController@create');
    Route::post('/pengeluaran/store', 'PengeluaranController@store');
    Route::post('/pengeluaran/tampil', 'PengeluaranController@tampil');
    Route::get('/pengeluaran/tampil', 'PengeluaranController@tampil');

    Route::get('/data_perusahaan', 'DataPerusahaanController@index');
    Route::post('/data_perusahaan/upload', 'DataPerusahaanController@upload');
    Route::get('/data_perusahaan/edit', 'DataPerusahaanController@edit');
    Route::put('/data_perusahaan', 'DataPerusahaanController@update');

    Route::get('/pelanggan', 'PelangganController@index');
    Route::get('/pelanggan/mdt1', 'PelangganController@mdt1');
    Route::get('/pelanggan/mdt2', 'PelangganController@mdt2');
    Route::get('/pelanggan/create', 'PelangganController@create');
    Route::get('/pelanggan/create/{po}', 'PelangganController@createWithPO');
    Route::post('/pelanggan/store', 'PelangganController@store');
    Route::get('/pelanggan/edit/{pelanggan}', 'PelangganController@edit');
    Route::get('/pelanggan/show/{pelanggan}', 'PelangganController@show');
    Route::get('/pelanggan/titipan/{pelanggan}', 'PelangganController@titipan');
    Route::get('/pelanggan/titipan/show/{titipan}', 'PelangganController@titipanShow');
    Route::post('/pelanggan/titipan', 'PelangganController@titipan_save');
    Route::put('/pelanggan/{pelanggan}', 'PelangganController@update');
    Route::delete('/pelanggan/delete/{pelanggan}', 'PelangganController@delete');
    Route::get('/pelanggan/{pelanggan}/pelanggan/json', 'PelangganController@pelangganJson');
    Route::get('/pelanggan/last/json', 'PelangganController@lastJson');
    Route::get('/pelanggan/{kode}/kodeJson', 'PelangganController@kodeJson');
    Route::get('/pelanggan/{kode}/ktpJson', 'PelangganController@ktpJson');
    Route::get('/pelanggan/{kode}/teleponJson', 'PelangganController@teleponJson');
    Route::get('/pelanggan/{kode}/{id}/kodeEditJson', 'PelangganController@kodeEditJson');
    Route::get('/pelanggan/{kode}/{id}/ktpEditJson', 'PelangganController@ktpEditJson');
    Route::get('/pelanggan/{kode}/{id}/teleponEditJson', 'PelangganController@teleponEditJson');
    Route::get('/pelanggan/aktif/{id}', 'PelangganController@reAktif');

    Route::get('/wilayah/{provinsi}/kabupaten.json', 'WilayahController@kabJson');
    Route::get('/wilayah/{kabupaten}/kecamatan.json', 'WilayahController@kecJson');
    Route::get('/wilayah/{kecamatan}/desa.json', 'WilayahController@desaJson');
    Route::get('/wilayah/{kecamatan}/camat.json', 'WilayahController@camatJson');
    Route::get('/wilayah/{kecamatan}/paten.json', 'WilayahController@patenJson');
    Route::get('/wilayah', 'WilayahController@index');
    Route::get('/wilayah/mdt1', 'WilayahController@mdt1');
    Route::get('/wilayah/mdt2', 'WilayahController@mdt2');
    Route::get('/wilayah/mdt3', 'WilayahController@mdt3');
    Route::get('/wilayah/mdt4', 'WilayahController@mdt4');

    Route::post('/wilayah/provinsi', 'WilayahController@storeProvinsi');
    Route::put('/wilayah/provinsi/{provinsi}', 'WilayahController@updateProvinsi');
    Route::delete('/wilayah/provinsi/{provinsi}', 'WilayahController@destroyProvinsi');

    Route::post('/wilayah/kabupaten', 'WilayahController@storeKabupaten');
    Route::put('/wilayah/kabupaten/{kabupaten}', 'WilayahController@updateKabupaten');
    Route::delete('/wilayah/kabupaten/{kabupaten}', 'WilayahController@destroyKabupaten');

    Route::post('/wilayah/kecamatan', 'WilayahController@storeKecamatan');
    Route::put('/wilayah/kecamatan/{kecamatan}', 'WilayahController@updateKecamatan');
    Route::delete('/wilayah/kecamatan/{kecamatan}', 'WilayahController@destroyKecamatan');

    Route::post('/wilayah/desa', 'WilayahController@storeDesa');
    Route::put('/wilayah/desa/{desa}', 'WilayahController@updateDesa');
    Route::delete('/wilayah/desa/{desa}', 'WilayahController@destroyDesa');

    Route::get('/seller/mdt1', 'SellerController@mdt1');
    Route::get('/seller/mdt2', 'SellerController@mdt2');
    Route::resource('/seller', 'SellerController');
    Route::get('/seller/aktif/{id}', 'SellerController@aktif');
    
    Route::get('/money-limit', 'MoneyLimitController@index');
    Route::post('/money-limit', 'MoneyLimitController@store');

    Route::get('/periode', 'PeriodeOrderController@index');
    Route::post('/periode', 'PeriodeOrderController@store');

    Route::get('/setoran-kasir', 'SetoranKasirController@index');
    Route::post('/setoran-kasir', 'SetoranKasirController@store');
    Route::post('/setoran-kasir/{id}', 'SetoranKasirController@setuju');
    
    // Route::post('/setoran_grosir', 'SetoranKasirController@storeGrosir');
    // Route::post('/setoran_grosir/{id}', 'SetoranKasirController@approveGrosir');
    // Route::post('/setoran_gudang/{id}', 'SetoranKasirController@approveGudang');

    Route::post('/setoranke_grosir', 'LaciController@setorGrosir');
    Route::post('/setoranke_gudang', 'LaciController@setorGudang');
    Route::post('/laci_owner', 'LaciController@storeOwner');
    Route::post('/transfer_bank_owner', 'LaciController@transferBank');
    Route::post('/transfer_bank_grosir', 'LaciController@transferBankGrosir');
    Route::post('/setoran_gudang/{id}', 'LaciController@approveGudang');
    Route::post('/setoran_ke_gudang/{id}', 'LaciController@approveOwner');
    Route::post('/setoran_gudang', 'LaciController@storeGudang');
    Route::post('/setoran_grosir', 'LaciController@storeGrosir');
    Route::post('/setoran_kasir', 'LaciController@storeEcerGrosir');
    Route::post('/setoran_ke_grosir/{id}', 'LaciController@approveGrosir');
    Route::post('/setoran_grosir_ecer/{id}', 'LaciController@approveGrosirEcer');
    Route::post('/setoran_grosir_gudang', 'LaciController@storeGrosirG');
    Route::post('/setoran_gudang_grosir', 'LaciController@storeGudangG');
    Route::post('/gudang_approve/{id}', 'LaciController@GudangApprove');
    Route::post('/grosir_approve/{id}', 'LaciController@GrosirApprove');

    /* --  Setoran dari tiap user -- */
    Route::post('/setoran_dari_grosir', 'LaciController@storeFromGrosir');
    Route::post('/setoran_dari_eceran', 'LaciController@storeFromEceran');
    Route::post('/setoran_dari_gudang', 'LaciController@storeFromGudang');
    Route::post('/setoran_dari_owner', 'LaciController@storeFromOwner');
    Route::post('/approve_eceran/{id}', 'LaciController@approveEcer');
    Route::post('/approve_owner/{id}', 'LaciController@approveOwner');



    Route::get('/setoran-buka', 'SetoranBukaController@index');
    Route::post('/setoran-buka', 'SetoranBukaController@store');

    Route::get('/modal', 'ModalController@index');
    Route::post('/modal', 'ModalController@store');
    // Route::put('/modal/{modal}', 'ModalController@update');
    
    Route::get('/prive', 'PriveController@index');
    Route::post('/prive', 'PriveController@store');
    // Route::put('/prive/{prive}', 'PriveController@update');
    
    Route::get('/cek', 'CekController@index');
    
    Route::get('/bg', 'BGController@index');

    Route::get('/hutang_bank', 'HutangBankController@index');
    Route::get('/hutang_bank/mdt1', 'HutangBankController@mdt1');
    Route::get('/hutang_bank/mdt2', 'HutangBankController@mdt2');
    Route::post('/hutang_bank', 'HutangBankController@store');
    Route::get('/hutang_bank/show/{hutang_bank}', 'HutangBankController@show');
    Route::get('/hutang_bank/last/json', 'HutangBankController@lastJson');
    Route::get('/bayar_hutang_bank/last/json', 'BayarHutangBankController@lastJson');
    Route::post('/bayar_hutang_bank', 'BayarHutangBankController@store');

    Route::get('/piutang_lain', 'PiutangLainController@index');
    Route::get('/piutang_lain/mdt1', 'PiutangLainController@mdt1');
    Route::get('/piutang_lain/mdt2', 'PiutangLainController@mdt2');
    Route::post('/piutang_lain', 'PiutangLainController@store');
    Route::get('/piutang_lain/show/{piutang_lain}', 'PiutangLainController@show');
    Route::get('/piutang_lain/last/json', 'PiutangLainController@lastJson');
    Route::get('/bayar_piutang_lain/last/json', 'BayarPiutangLainController@lastJson');
    Route::post('/bayar_piutang_lain', 'BayarPiutangLainController@store');
    Route::get('/transfer_piutang_lain/{id}', 'PiutangLainController@transfer_piutang');
    Route::post('/transfer_piutang_lain', 'PiutangLainController@store_transfer_piutang');

    Route::post('/piutang_dagang/tanggal', 'PiutangDagangController@tanggalStore');
    Route::get('/piutang-dagang', 'PiutangDagangController@index');
    Route::get('/piutang-dagang/mdt1', 'PiutangDagangController@mdt1');
    Route::get('/piutang-dagang/mdt2', 'PiutangDagangController@mdt2');
    Route::post('/piutang-dagang/{piutang}', 'PiutangDagangController@store');
    Route::get('/piutang-dagang/{piutang}', 'PiutangDagangController@show');
    Route::get('/piutang-dagang/transfer/{piutang}', 'PiutangDagangController@transfer');
    Route::get('/piutang-dagang/{piutang}/view-log/{bayar}', 'PiutangDagangController@viewLog');
    Route::get('/piutang-dagang/last/json', 'PiutangDagangController@lastJson');
    Route::post('/transfer_piutang_dagang', 'PiutangDagangController@transferStore');

    Route::get('/jurnal', 'JurnalController@index');
    Route::post('/jurnal/tampil', 'JurnalController@tampil');
    Route::get('/jurnal/tampil', 'JurnalController@tampil');

    Route::get('/akun', 'AkunController@index');
    
    Route::get('/bank', 'BankController@index');
    Route::get('/bank/mdt1', 'BankController@mdt1');
    Route::post('/bank', 'BankController@store');
    Route::put('/bank/{bank}', 'BankController@update');

    Route::get('/peralatan', 'PeralatanController@index');
    Route::get('/peralatan/mdt1', 'PeralatanController@mdt1');
    Route::get('/peralatan/mdt2', 'PeralatanController@mdt2');
    Route::post('/peralatan', 'PeralatanController@store');
    Route::get('/peralatan/show/{id}', 'PeralatanController@show');
    Route::put('/peralatan/{peralatan}', 'PeralatanController@update');
    Route::get('/peralatan/last/json', 'PeralatanController@lastJson');
    Route::post('/rawat_alat', 'PeralatanController@rawat_alat');
    Route::post('/peralatan/rusak', 'PeralatanController@rusak');
    Route::post('/peralatan/jual', 'PeralatanController@jual');
    
    Route::get('/perlengkapan/last/json', 'PerlengkapanController@lastJson');
    Route::get('/perlengkapan', 'PerlengkapanController@index');
    Route::get('/perlengkapan/mdt1', 'PerlengkapanController@mdt1');
    Route::get('/perlengkapan/mdt2', 'PerlengkapanController@mdt2');
    Route::post('/perlengkapan', 'PerlengkapanController@store');
    Route::put('/perlengkapan/off/{perlengkapan}', 'PerlengkapanController@update');
    Route::get('/perlengkapan/off', 'PerlengkapanController@off');

    Route::get('/beban', 'BebanController@index');
    Route::get('/beban/mdt1', 'BebanController@mdt1');
    Route::get('/beban/last/json', 'BebanController@lastJson');
    Route::post('/beban', 'BebanController@store');
    // Route::put('/perlengkapan/{perlengkapan}', 'PerlengkapanController@update');

    Route::get('/kendaraan', 'KendaraanController@index');
    Route::get('/kendaraan/mdt1', 'KendaraanController@mdt1');
    Route::get('/kendaraan/mdt2', 'KendaraanController@mdt2');
    Route::post('/kendaraan', 'KendaraanController@store');
    Route::get('/kendaraan/show/{id}', 'KendaraanController@show');
    Route::put('/kendaraan/{kendaraan}', 'KendaraanController@update');
    Route::get('/kendaraan/last/json', 'KendaraanController@lastJson');
    Route::post('/rawat_kendaraan', 'KendaraanController@rawat_kendaraan');
    Route::post('/kendaraan/rusak', 'KendaraanController@rusak');
    Route::post('/kendaraan/jual', 'KendaraanController@jual');

    Route::get('/event', 'EventController@index');
    Route::post('/event', 'EventController@store');

    Route::get('/harta_tetap', 'HartaTetapController@index');
    Route::post('/harta_tetap', 'HartaTetapController@store');
    Route::post('/tambah_aset', 'HartaTetapController@storeAset');
    Route::get('/harta_tetap/last/json', 'HartaTetapController@lastJson');

    Route::get('/laba_rugi', 'LabaRugiController@index');
    Route::post('/laba_rugi/tampil', 'LabaRugiController@tampil');    
    Route::get('/laba_rugi/tampil', 'LabaRugiController@tampil');    
    Route::get('/laba_rugi/tampil/sekarang', 'LabaRugiController@sekarang');    
    Route::post('/laba_rugi/tampil/pajak', 'LabaRugiController@pajak');    
    Route::get('/laba_rugi/tampil/pajak', 'LabaRugiController@pajak');    
    Route::post('/laba_rugi/cetak', 'LabaRugiController@cetak');    
    Route::get('/laba_rugi/cetak', 'LabaRugiController@cetak');    
    // Route::get('/jurnal_penutup', 'LabaRugiController@penutup');
    // Route::post('/jurnal_penutup', 'LabaRugiController@penutup');    
    // Route::post('/penutup', 'LabaRugiController@penutup_save');    

    Route::get('/neraca', 'NeracaController@index');
    Route::post('/neraca/tampil', 'NeracaController@tampil');    
    Route::get('/neraca/tampil', 'NeracaController@tampil');
    Route::post('/neraca/cetak', 'NeracaController@cetak');    
    Route::get('/neraca/cetak', 'NeracaController@cetak');

    Route::get('/prive-persediaan', 'PrivePersediaanController@index');
    Route::get('/prive/{item}/item/json', 'PrivePersediaanController@itemJson');

    Route::get('/laci', 'LaciController@index');
    // Route::post('/laci', 'LaciController@store');    

    Route::get('/pendapatan_lain', 'PendapatanLainController@index');
    Route::get('/pendapatan_lain/mdt1', 'PendapatanLainController@mdt1');
    Route::get('/pendapatan_lain/last/json', 'PendapatanLainController@lastJson');
    Route::post('/pendapatan_lain', 'PendapatanLainController@store');

    Route::get('/laba_ditahan', 'LabaDitahanController@index');
    Route::get('/laba_ditahan/last/json', 'LabaDitahanController@lastJson');
    Route::post('/laba_ditahan', 'LabaDitahanController@store');

    Route::get('/jurnal_penutup', 'JurnalPenutupController@index');
    Route::post('/jurnal_penutup', 'JurnalPenutupController@store');

    Route::get('/bagi_hasil', 'BagiHasilController@index');
    Route::get('/bagi_hasil/mdt1', 'BagiHasilController@mdt1');
    Route::get('/bagi_hasil/last/json', 'BagiHasilController@lastJson');
    Route::post('/bagi_hasil', 'BagiHasilController@store');

    Route::get('/jurnal_penyesuaian', 'JurnalPenyesuaianController@index');
    Route::get('/jurnal_penyesuaian/last/json', 'JurnalPenyesuaianController@lastJson');
    Route::post('/jurnal_penyesuaian', 'JurnalPenyesuaianController@store');

    Route::get('/penyesuaian_persediaan', 'PenyesuaianPersediaanController@index');
    Route::get('/penyesuaian_persediaan/mdt1', 'PenyesuaianPersediaanController@mdt1');
    Route::get('/penyesuaian_persediaan/last/json', 'PenyesuaianPersediaanController@lastJson');
    Route::post('/penyesuaian_persediaan', 'PenyesuaianPersediaanController@store');
    Route::get('/penyesuaian_persediaan/{item}/item/json', 'PenyesuaianPersediaanController@itemJson');
    Route::get('/penyesuaian_persediaan/{transaksi}/konversi/json/{satuan}', 'PenyesuaianPersediaanController@KonversiJson');
    Route::get('/penyesuaian_persediaan/{transaksi}/count/json/{count}', 'PenyesuaianPersediaanController@countJson');
    Route::get('/penyesuaian_persediaan/{id}/show', 'PenyesuaianPersediaanController@show');

    Route::get('/alert/stok/json/', 'AlertController@stokLimitJson');
    Route::get('/alert/jatuh_tempo/pembelian/', 'AlertController@JTPembelianJSON');
    Route::get('/alert/jatuh_tempo/penjualan/', 'AlertController@JTPenjualanJSON');
    Route::get('/penjualan/jatuh_tempo/', 'AlertController@JTPenjualan');
    Route::get('/pembelian/jatuh_tempo/', 'AlertController@JTPembelian');
    Route::get('/alert/notice/json/', 'AlertController@noticeJson');
    Route::get('/alert/kadaluarsa/json/', 'AlertController@kadaluarsaJson');
    Route::get('/alert/cash_grosir/json/', 'AlertController@cashGrosirJson');
    Route::get('/alert/cash_gudang/json/', 'AlertController@cashGudangJson');
    Route::get('/alert/cash_kasir/json/', 'AlertController@cashKasirJson');
    Route::get('/item/alert/', 'AlertController@alert');
    Route::get('/item/kadaluarsa/', 'AlertController@kadaluarsa');
    Route::get('/alert/laci_laci', 'AlertController@LaciLaci');
    Route::get('/alert/item/validasi/json', 'AlertController@ValidasiListJSON');
    Route::get('/alert/item/validasi', 'AlertController@ValidasiList');

    Route::get('/sidebar/setoran-buka/json/', 'SidebarController@setoranBuka');

    Route::get('/arus_kas', 'ArusController@index');
    Route::post('/arus_kas/tampil', 'ArusController@tampil');
    Route::get('/arus_kas/tampil', 'ArusController@tampil');
    Route::post('/arus_kas/cetak', 'ArusController@cetak');
    Route::get('/arus_kas/cetak', 'ArusController@cetak');

    Route::get('/kredit', 'KreditController@index');
    Route::post('/kredit', 'KreditController@store');
    Route::put('/kredit/{kredit}', 'KreditController@update');

    Route::get('penyesuaian_stok', 'StokController@index');
    Route::post('/penyesuaian_stok', 'StokController@store');
    Route::get('/stok/{id}/item/json', 'StokController@ItemJson');

    Route::get('gaji', 'GajiController@index');
    Route::get('gaji/mdt1', 'GajiController@mdt1');
    Route::get('/gaji/last/json', 'GajiController@lastJson');
    Route::get('/gaji/{piutang}/piutang/json', 'GajiController@piutangJson');
    Route::get('/gaji/{piutang}/sisa/json', 'GajiController@sisaPiutangJson');
    Route::post('/gaji', 'GajiController@store');

    Route::get('catatan', 'CatatanController@index');
    Route::post('/catatan', 'CatatanController@store');

    Route::get('persentase', 'PersentaseController@index');
    Route::post('/persentase', 'PersentaseController@store');
    // Route::get('/stok/{id}/item/json', 'StokController@ItemJson');

    Route::get('/cetak/pengambilan/{transaksi}', 'CetakController@pengambilan');
    Route::get('/cetak/eceran/{transaksi}', 'CetakController@eceran');
    Route::get('/cetak/grosir/{transaksi}', 'CetakController@grosir');
    Route::get('/cetak/nota/{transaksi}', 'CetakController@simpanCetakNota');
    Route::get('/cetak/titipan/{titipan}', 'CetakController@titipan');
    Route::get('/cetak/{transaksi}/retur/{titipan}', 'CetakController@retur');

    Route::get('/logout/json', 'AuthController@json');

    Route::get('/log_laci', 'LogLaciController@index');
    Route::post('/log_laci/tampil', 'LogLaciController@tampil');
    Route::get('/log_laci/tampil', 'LogLaciController@tampil');

    Route::get('/logo_perusahaan/json', 'AlertController@logoJson');

    Route::get('/grafik/wilayah', 'GrafikWilayahController@index');
    Route::get('/grafik/wilayah/provinsi/{provinsi}', 'GrafikWilayahController@Provinsi');
    Route::get('/grafik/wilayah/provinsi/', 'GrafikWilayahController@ProvinsiAll');
    Route::get('/grafik/wilayah/kabupaten/{kabupaten}', 'GrafikWilayahController@Kabupaten');
    Route::get('/grafik/wilayah/kecamatan/{kecamatan}', 'GrafikWilayahController@Kecamatan');

    Route::get('/grafik/pembelian', 'GrafikPembelianController@index');
    Route::get('/grafik/pembelian/tahun/{tahun}', 'GrafikPembelianController@tahun');
    Route::get('/grafik/pembelian/tahun/{tahun}/bulan/{bulan}', 'GrafikPembelianController@bulan');
    Route::get('/grafik/pembelian/rentang/{awal}/{akhir}', 'GrafikPembelianController@rentang');
    Route::get('/grafik/pembelian/tanggal/{tanggal}', 'GrafikPembelianController@tanggal');

    Route::get('/grafik/penjualan', 'GrafikPenjualanController@index');
    Route::get('/grafik/penjualan/tahun/{tahun}', 'GrafikPenjualanController@tahun');
    Route::get('/grafik/penjualan/tahun/{tahun}/bulan/{bulan}', 'GrafikPenjualanController@bulan');
    Route::get('/grafik/penjualan/rentang/{awal}/{akhir}', 'GrafikPenjualanController@rentang');
    Route::get('/grafik/penjualan/tanggal/{tanggal}', 'GrafikPenjualanController@tanggal');

    // Route::get('/grafik/penjualan/pelanggan/{pelanggan}', 'GrafikPenjualanController@indexPelanggan');
    Route::get('/grafik/penjualan/tahun/{tahun}/pelanggan/{pelanggan}', 'GrafikPenjualanController@tahunPelanggan');
    Route::get('/grafik/penjualan/tahun/{tahun}/bulan/{bulan}/pelanggan/{pelanggan}', 'GrafikPenjualanController@bulanPelanggan');
    Route::get('/grafik/penjualan/rentang/{awal}/{akhir}/pelanggan/{pelanggan}', 'GrafikPenjualanController@rentangPelanggan');
    Route::get('/grafik/penjualan/tanggal/{tanggal}/pelanggan/{pelanggan}', 'GrafikPenjualanController@tanggalPelanggan');

    Route::get('/grafik/beban', 'GrafikBebanController@index');
    Route::get('/grafik/beban/tahun/{beban}/{tahun}', 'GrafikBebanController@tahun');
    Route::get('/grafik/beban/tahun/{beban}/{tahun}/bulan/{bulan}', 'GrafikBebanController@bulan');
    Route::get('/grafik/beban/rentang/{beban}/{awal}/{akhir}', 'GrafikBebanController@rentang');
    Route::get('/grafik/beban/tanggal/{beban}/{tanggal}', 'GrafikBebanController@tanggal');

    Route::get('/grafik/laba', 'GrafikLabaController@index');
    Route::get('/grafik/laba/tahun/{tahun}', 'GrafikLabaController@tahun');
    Route::get('/grafik/laba/tahun/{tahun}/bulan/{bulan}', 'GrafikLabaController@bulan');
    Route::get('/grafik/laba/rentang/{awal}/{akhir}', 'GrafikLabaController@rentang');
    Route::get('/grafik/laba/tanggal/{tanggal}', 'GrafikLabaController@tanggal');

    Route::get('/rule_retur', 'ReturRuleController@index');
    Route::post('/rule_retur', 'ReturRuleController@store');

    Route::get('/rule-bonus', 'BonusRulesController@index');
    Route::get('/rule-bonus/mdt1', 'BonusRulesController@mdt1');
    Route::get('/rule-bonus/mdt2', 'BonusRulesController@mdt2');
    Route::post('/rule-bonus', 'BonusRulesController@store');
    Route::delete('/rule-bonus/{id}', 'BonusRulesController@delete');
    Route::put('/rule-bonus/{id}', 'BonusRulesController@update');
    Route::post('/rule-bonus/aktif/{id}', 'BonusRulesController@aktif');
    Route::get('/rule-bonus/{rule}/item/{id}/json', 'BonusRulesController@ItemJson');
    Route::get('/rule-bonus/{id}/show', 'BonusRulesController@show');
    Route::post('/rule-bonus/relasi', 'BonusRulesController@storeRelasi');
    Route::delete('/rule-bonus/relasi/{id}', 'BonusRulesController@deleteRelasi');
    Route::put('/rule-bonus/relasi/{id}', 'BonusRulesController@updateRelasi');

    Route::get('/cetak/po-pembelian/{id}', 'CetakController@po_pembelian');
    

    Route::get('/persen_pajak', 'PersenPajakController@index');
    Route::post('/persen_pajak', 'PersenPajakController@update');
});

Route::get('/coba', 'CobaController@coba');
Route::put('/coba/rusak', 'CobaController@rusak');
Route::get('/coba/cetak_pengambilan', 'CobaController@cetak_pengambilan');
Route::get('/coba/cetak_eceran', 'CobaController@cetak_eceran');
Route::get('/coba/cetak_grosir', 'CobaController@cetak_grosir');
Route::get('/coba/cetak_warna', 'CobaController@cetak_warna');

Route::get('/coba/transpose1', 'CobaController@transpose1');
Route::get('/coba/transpose2', 'CobaController@transpose2');
Route::get('/coba/migration1', 'CobaController@migration1');
Route::get('/coba/cek-satuan-kosong', 'CobaController@cekSatuanKosong');
Route::get('/coba/cek-harga-kosong', 'CobaController@cekHargaKosong');
Route::get('/coba/relasi_satuan_to_harga', 'CobaController@relasiToHarga');
Route::get('/coba/list-satuan', 'CobaController@listSatuan');
Route::get('/coba/get-item/{item}', 'CobaController@getItem');
